var elixir = require('laravel-elixir');

require('laravel-elixir-vueify');

elixir(function(mix) {
    mix.sass('app.scss')
        .styles([
            'libs/animate.css',
            'libs/font-awesome/css/font-awesome.min.css',
            'libs/bootstrap-social.css',
            'libs/carousel.css',
            'libs/ihover.min.css',
            'libs/jquery.mCustomScrollbar.min.css',
            'libs/select2.min.css',
            'libs/sweetalert.css',
            'aitisi.css',
            'excel.css',
            //'footer.css',
            'loader.css',
            'mystyles.css'
        ], 'public/css/libs.css')

        .styles([
            'libs/pe07icon.css',
            'libs/ct-navbar.css'
        ], 'public/css/app2.css')

        .copy('resources/assets/css/libs/font-pe07', 'public/build/fonts')

        .browserify('moria.js')
        .browserify('placements.js')
        .browserify('new_placements.js')
        .browserify('EpithimiaYperarithmias.js')
        .browserify('OrganikaKenaPleonasmata.js')
        .browserify('DilosiSxoleion.js')
        .browserify('DilosiSxoleionVeltiwsi.js')
        .browserify('OnomastikaYperarithmoi.js')

        .browserify('DilosiSxoleionYperarithmou.js')


        .browserify('notifications/forPysdeNotification.js')
        .browserify('notifications/forMisthodosiaNotification.js')
        .browserify('notifications/forSchoolNotification.js')
        .browserify('notifications/forTeacherNotification.js')
        .browserify('notifications/forOfaNotification.js')

        .browserify('ktel.js')

        .browserify('ofa/ofaSchool.js')
        .browserify('ofa/OfaSchoolGymnasiou.js')
        .browserify('ofa/ofaSchoolParticipations.js')


        .scripts([
            'libs/default/jquery.min.js',
            'libs/default/bootstrap.min.js',
            'libs/default/select2.min.js',
            'libs/default/sweetalert.min.js',
            'libs/default/jquery.printPage.js',
            'libs/default/jquery.mCustomScrollbar.concat.min.js',
            'libs/default/socket.io.min.js',
            'libs/default/global.js'
        ], 'public/js/libs-default.js')

        .scripts([
            'libs/forms/dataTables.min.js',
            'libs/forms/dataTables.bootstrap.min.js'
        ], 'public/js/datatables.js')

        .scripts([
            'libs/jquery.countdown.min.js'
        ], 'public/js/jquery.countdown.min.js')

        .scripts([
            'libs/ct-navbar.js'
        ], 'public/js/app2.js')


        .browserify('main.js')

        .copy('resources/assets/css/libs/font-awesome/fonts', 'public/build/fonts')
        .copy('resources/assets/css/libs/glyphicons/', 'public/build/fonts/bootstrap')

        .version([
            'css/app.css',
            'css/libs.css',
            'js/libs-default.js',
            'js/EpithimiaYperarithmias.js',
            'js/OrganikaKenaPleonasmata.js',
            'js/DilosiSxoleion.js',
            'js/DilosiSxoleionVeltiwsi.js',
            'js/OnomastikaYperarithmoi.js',
            'js/placements.js',
            'js/new_placements.js',

            'js/DilosiSxoleionYperarithmou.js',

            'css/app2.css',
            'js/app2.js',


            'js/forPysdeNotification.js',
            'js/forMisthodosiaNotification.js',
            'js/forSchoolNotification.js',
            'js/forTeacherNotification.js',
            'js/forOfaNotification.js',

            'js/ktel.js',

            'js/ofaSchool.js',
            'js/OfaSchoolGymnasiou.js',
            'js/ofaSchoolParticipations.js'
        ]);

});
