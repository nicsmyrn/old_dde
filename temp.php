<?php


$excel->sheet('Ομάδα1η', function($sheet) use($header1, $eidikotites, $styleArray, $date, $last_cell_row_Omada1){

$sheet->loadView('pysde.kena_pleonasmata.Excel.ORGANIKA_omada1_orderBy_eidikotites')
->setFontFamily('Arial')
->setFontSize(19)
->setFontBold(false)
->with(compact('header1','eidikotites'))
->cells('A1:AD1', function($cells){
$cells->setFontSize(16);
})

->cells('A2:A'.$last_cell_row_Omada1, function($cells){
$cells->setFontSize(14);
})
->cells('B2:B'.$last_cell_row_Omada1, function($cells){
$cells->setFontSize(9);
})
->getStyle('A1:AF1')->getAlignment()->setTextRotation(90)->setWrapText(true)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);

$sheet->cells('C1:AF1', function($cells) {
$cells->setFontWeight('bold');
$cells->setFontSize(16);
});

$sheet->setHeight(1, 123);
$sheet->setPageMargin(array(
0.55, 0.25, 0.25, 0.25
));

$sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΟΜΑΔΑ 1 - ΕΠΙΚΑΙΡΟΠΟΙΗΜΕΝΟΣ ΠΙΝΑΚΑΣ ΟΡΓΑΝΙΚΩΝ ΚΕΝΩΝ-ΠΛΕΟΝΑΣΜΑΤΩΝ: '. $date);
$sheet->getHeaderFooter()->setOddFooter(Carbon::now()->format('d-m-Y H:i'));

$sheet->getColumnDimension('A')->setWidth(6);
$sheet->getColumnDimension('B')->setWidth(11);

for($col='C'; $col !== 'AG'; $col++){
$sheet->setSize($col.'1',5,150);
}

for($row=2; $row <=$last_cell_row_Omada1 ;$row++){
$sheet->getRowDimension($row)->setRowHeight(30);
}

$sheet->getStyle('A1:AF'.$last_cell_row_Omada1)->applyFromArray($styleArray);
$sheet->getStyle('A2:AF'.$last_cell_row_Omada1)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
});