<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class MisthodosiaFake extends Model
{
    protected $table = 'misthodosiaFake';

    protected $primaryKey = 'fmid';

    protected $fillable = [
        'fmid',
        'at',
        'birth',
        'iban',
        'bank',
        'amka',
        'doy',
        'am_tsmede',
        'am_ika',
        'new',
        'mk',
        'family_situation',
        'teacher_id'
    ];

    protected $dates = ['birth'];

    public function tekna()
    {
        return $this->hasMany('App\TeknaFake', 'misthodosia_id', 'fmid');
    }

    public function teacher()
    {
        return $this->belongsTo('App\Teacher', 'teacher_id', 'id');
    }


    public function setBirthAttribute($date)
    {
        $this->attributes['birth'] = $date ?  Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getBirthAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }}
