<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    
    protected $table = 'roles';
    
    protected $fillable = [
        'name',
        'slug',
        'description',
        'can_access'
    ];
    
    public $timestamps = false;
    
    public function users()
    {
        return $this->hasMany('App\User');
    }
    
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'permissions_roles', 'role_id', 'permission_id');
    }
    
    public function givePermission($slug)
    {
        $permission = Permission::where('slug', $slug)->first();
        
        return $this->permissions()->save($permission);
    }
    
    public function hasPermission($slug)
    {
        foreach($this->permissions as $permission){
            if ($permission->slug == $slug){
                return true;
            }
        }

        return false;
    }
    
    public function takePermission($slug)
    {
        $permission = Permission::where('slug', $slug)->first();
        
        return $this->permissions()->detach($permission);
    }
    
    public static function checkAccess()
    {
        $role = Role::where('slug', 'school')->first();
        
        return $role->hasPermission('edit_school_kena');
    }
}
