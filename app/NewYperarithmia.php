<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewYperarithmia extends Model
{
    protected $table = 'new_yperarithmies';

    protected $fillable = [
        'school_id',
        'eidikotita_id',
        'number',
        'description'
    ];
}
