<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Edata extends Model
{
    protected $table = 'edata';

    protected $primaryKey = 'am';

    public $timestamps = false;

    protected $fillable = [
        'am',
        'special_disease',
        'special_many_children',
        'special_judiciary',
        'sum_moria',
        'entopiotita',
        'sinipiretisi',
        'dimos_entopiotitas',
        'dimos_sinipiretisis',
        'aitisi_veltiwsis',

        'ex_years',
        'ex_months',
        'ex_days',
        'moria_ypiresias',
        'moria_sinthikes',
        'moria_family_situation',
        'number_of_kids',
        'number_of_kids_spoudes',
        'sum_tekna',
        'moria_tekna',
        'special_situation'
    ];

    public function myschool()
    {
        return $this->belongsTo('App\MySchoolTeacher', 'am', 'am');
    }
}
