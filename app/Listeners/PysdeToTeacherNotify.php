<?php

namespace App\Listeners;

use App\Events\PysdeSendNotificationToTeacher;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notification;

class PysdeToTeacherNotify
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PysdeSendNotificationToTeacher  $event
     * @return void
     */
    public function handle(PysdeSendNotificationToTeacher $event)
    {
        Notification::create([
            'user_id'   => $event->pearson->user->id,
            'title'     => $event->title,
            'url'       => $event->url,
            'description' => $event->description,
            'type'          => $event->type,
            'forRole'       => 'teacher',
            'forUser'       => $event->pearson->user->id,
            'uniqueAction'  => $event->uniqueAction
        ]);
    }
}
