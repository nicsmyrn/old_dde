<?php

namespace App\Listeners;

use App\Events\SchoolConfirmToPysde;
use App\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SchoolSendNotificationConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SchoolConfirmToPysde  $event
     * @return void
     */
    public function handle(SchoolConfirmToPysde $event)
    {
        Notification::create([
            'user_id'   => $event->pearson->user->id,
            'title'     => $event->title,
            'description' => $event->description,
            'type'          => $event->type,
            'url'           => $event->url,
            'forRole'       => 'pysde_secretary',
            'forUser'       => null,
            'uniqueAction'  => $event->uniqueAction
        ]);
    }
}
