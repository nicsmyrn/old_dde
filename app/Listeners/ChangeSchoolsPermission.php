<?php

namespace App\Listeners;

use App\Events\SchoolGrantEditPermission;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Role;

class ChangeSchoolsPermission
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SchoolGrantEditPermission  $event
     * @return void
     */
    public function handle(SchoolGrantEditPermission $event)
    {
        $role_school = Role::where('slug', 'school')->first();
        
        if($event->noPermission){
            $role_school->takePermission($event->permission);
            flash()->overlayE('Προσοχή!', 'Αφαιρέσατε το δικαίωμα Επεξεργασίας από τα Σχολεία');
        }else{
            $role_school->givePermission($event->permission);
            flash()->overlayS('Συγχαρητήρια!', 'Δώσατε δικαιώματα Επεξεργασίας του πίνακα Κενά - Πλεονάσματα');
        }
    }
}
