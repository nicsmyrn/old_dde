<?php

namespace App\Listeners;

use App\Events\PysdeSendNotificationToSchool;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notification;

class PysdeChangedKenaValues
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PysdeSendNotificationToSchool  $event
     * @return void
     */
    public function handle(PysdeSendNotificationToSchool $event)
    {
//        Notification::create([
//            'user_id'   => $event->pearson->user->id,
//            'title'     => $event->title,
//            'url'       => $event->url,
//            'description' => $event->description,
//            'type'          => $event->type,
//            'forRole'       => 'school',
//            'forUser'       => $event->pearson->user->id,
//            'uniqueAction'  => $event->uniqueAction
//        ]);
    }
}
