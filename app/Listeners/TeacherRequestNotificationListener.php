<?php

namespace App\Listeners;

use App\Events\TeacherSendNotificationToPysde;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notification;

class TeacherRequestNotificationListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeacherSendNotificationToPysde  $event
     * @return void
     */
    public function handle(TeacherSendNotificationToPysde $event)
    {
        $notifictation = Notification::create([
            'user_id'   => $event->pearson->id,
            'title'     => $event->title,
            'url'       => $event->url,
            'description' => $event->description,
            'type'          => $event->type,
            'forRole'       => 'pysde_secretary',
            'forUser'       => null,
            'uniqueAction'  => $event->uniqueAction
        ]);

    }
}
