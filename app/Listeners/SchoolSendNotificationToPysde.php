<?php

namespace App\Listeners;

use App\Events\SchoolSaveChanges;
use App\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SchoolSendNotificationToPysde
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SchoolSaveChanges  $event
     * @return void
     */
    public function handle(SchoolSaveChanges $event)
    {
        //
        $notifictation = Notification::create([
            'user_id'   => $event->pearson->user->id,
            'title'     => $event->title,
            'description' => $event->description,
            'type'          => $event->type,
            'url'           => $event->url,
            'forRole'       => 'pysde_secretary',
            'forUser'       => null,
            'uniqueAction'  => $event->uniqueAction
        ]);
    }
}
