<?php

namespace App\Listeners;

use App\Events\TeacherRequestGrandDenyPermission;
use App\Role;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeTeacherRequestPermision
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeacherRequestGrandDenyPermission  $event
     * @return void
     */
    public function handle(TeacherRequestGrandDenyPermission $event)
    {
        $role_teacher = Role::where('slug', 'teacher')->first();
        if ($role_teacher->hasPermission($event->permission)){
            $role_teacher->takePermission($event->permission);
        }else{
            $role_teacher->givePermission($event->permission);
        }
    }
}
