<?php

namespace App\Listeners;

use App\Events\MisthodosiaSendNotificationToTeacher;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notification;

class MisthodosiaNotifyTeacherUnlock
{
    public function handle(MisthodosiaSendNotificationToTeacher $event)
    {
        Notification::create([
            'user_id'   => $event->pearson->user->id,
            'title'     => $event->title,
            'url'       => $event->url,
            'description' => $event->description,
            'type'          => $event->type,
            'forRole'       => 'teacher',
            'forUser'       => $event->pearson->user->id,
            'uniqueAction'  => $event->uniqueAction
        ]);
    }
}
