<?php

namespace App\Listeners;

use App\Events\SchoolToOfaTeamNotification;
use App\Notification;

class OfaNotifyCreation
{

    public function handle(SchoolToOfaTeamNotification $event)
    {
        //
        $notifictation = Notification::create([
            'user_id'   => $event->pearson->user->id,
            'title'     => $event->title,
            'description' => $event->description,
            'type'          => $event->type,
            'url'           => $event->url,
            'forRole'       => 'ofa',
            'forUser'       => null,
            'uniqueAction'  => $event->uniqueAction
        ]);
    }
}
