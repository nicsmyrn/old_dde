<?php

namespace App\Listeners;

use App\Events\TeacherChangeProfileNotification;
use App\Events\TeacherRequestForMoriodotisi;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notification;

class TeacherNotifyPysdeForMoriodotisi
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TeacherChangeProfileNotification  $event
     * @return void
     */
    public function handle(TeacherRequestForMoriodotisi $event)
    {
        Notification::create([
            'user_id'   => $event->pearson->id,
            'title'     => $event->title,
            'url'       => $event->url,
            'description' => $event->description,
            'type'          => $event->type,
            'forRole'       => 'pysde_secretary',
            'forUser'       => null,
            'uniqueAction'  => $event->uniqueAction
        ]);
    }
}
