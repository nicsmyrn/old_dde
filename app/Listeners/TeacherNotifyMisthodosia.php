<?php

namespace App\Listeners;

use App\Events\TeacherChangeMisthodosiaNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notification;

class TeacherNotifyMisthodosia
{

    public function handle(TeacherChangeMisthodosiaNotification $event)
    {
        Notification::create([
            'user_id'   => $event->pearson->id,
            'title'     => $event->title,
            'url'       => $event->url,
            'description' => $event->description,
            'type'          => $event->type,
            'forRole'       => 'misthodosia',
            'forUser'       => null,
            'uniqueAction'  => $event->uniqueAction
        ]);
    }
}
