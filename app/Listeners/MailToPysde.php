<?php

namespace App\Listeners;

use App\Events\MailsForKena;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\School;

class MailToPysde
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MailsForKena  $event
     * @return void
     */
    public function handle(MailsForKena $event)
    {
       $schools = School::all();

        $temp_array = array();

        foreach ($schools as $school){

            $eidikotites =  $school->eidikotites->filter(function($school){
                if ($school->pivot->value != 0){
                    if ($school->pivot->updated_at > "2015-12-01 00:00:00"){   // προσωρινή ώρα για αποστολή e-mail μόνο όσων έκαναν αλλαγές
                        return true;
                    }
                }
            });

            if (!$eidikotites->isEmpty()){
                $officer = $school->user->full_name;

                $new_eidikotites =  $school->eidikotites->filter(function($school){
                    if ($school->pivot->value != 0){
                            return true;    
                    }
                });
                
                if($school->description != ''){
                    \Mail::queue('emails.school_kena', compact('school', 'new_eidikotites', 'officer'), function($message) use ($school){
                        $message->from($school->user->email)// $school->user->email
                        ->to('pysde@dide.chan.sch.gr')
                            ->subject('Κενά - Πλεονάσματα: '.$school->name);
                    });                    
                }

                 \Mail::queue(['text'=>'emails.from_pysde_confirmation'], compact('school', 'new_eidikotites'), function($message) use ($school){
                     $message->from('mail3didechanion@gmail.com')
                         ->to($school->user->email) // $school->user->email
                         ->subject('Μήνυμα Επιβεβαίωσης');
                 });
            }
        }

    }
}
