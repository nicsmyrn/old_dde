<?php

namespace App\Listeners;

use App\Events\Event;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Auth;
use Request;

class UserEventListener
{
    /**
     * Create the event listener.

     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(User $user, $remember)
    {
        $user->ip = Request::getClientIp();
        $user->login_at = Carbon::now();
        $user->counter = $user->counter + 1;
        
        $user->save();
        
//        if ($user->email == 'mail@7gym-chanion.chan.sch.gr'){
/*            \Mail::queue('emails.login_at', ['user'=>$user], function($message) use ($user){
                $message->from($user->email)// $school->user->email
                ->to('nicsmyrn@gmail.com')
                ->subject(' Login at :'.$user->login_at . ' - '. $user->full_name);
            });
*/
            if ($user->userable_type == 'App\School' && $user->userable == null){
                session()->flash('error666school', 'is_true');
//                dd('Προσοχή! ο λογαριασμός που συνδεθήκατε έχει πρόβλημα. Επικοινωνήστε με τον διαχειριστή της ΠΑΣΙΦΑΗ Σμυρναίο Νικόλαο στο 6973009154.');
            }else{
                session()->flash('error666school', 'is_false');
            }
        }

//    }
}
