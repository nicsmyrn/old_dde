<?php

namespace App\Listeners;

use App\Events\SchoolSaveChanges;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SchoolReceiveEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SchoolSaveChanges  $event
     * @return void
     */
    public function handle(SchoolSaveChanges $event)
    {
        //
    }
}
