<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class NewEidikotita extends Model
{
    protected $table = 'eidikotites_new';

    protected $fillable = [
        'klados_name',
        'klados_slug',
        'eidikotita_name',
        'eidikotita_slug'
    ];

    public function getFullNameAttribute()
    {
        return "$this->eidikotita_slug  ($this->eidikotita_name)";
    }
}

