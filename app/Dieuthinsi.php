<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dieuthinsi extends Model
{
    //
    protected $table = 'dieuthinsis';

    protected $fillable = [
        'identifier',
        'title',
        'nomos',
        'name'
    ];
}
