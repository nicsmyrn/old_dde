<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Placement extends Model
{
    //
    use SoftDeletes;

    protected $table = 'placements';

    protected $fillable = [
        'klados',
        'praxi_id',
        'teacher_name',
        'hours',
        'from',
        'to',
        'description',
        'from_id',
        'to_id',
        'klados_id',
        'teacherable_id',
        'teacherable_type',
        'placements_type',
        'days',
        'me_aitisi',
        'starts_at',
        'afm'
    ];

    public function teacher()
    {
        return $this->belongsTo(MySchoolTeacher::class,'afm','afm');
    }

    protected $dates = ['deleted_at', 'starts_at','ends_at'];

    public function praxi()
    {
        return $this->belongsTo(\App\Praxi::class);
    }

    public function teacherable()
    {
        return $this->morphTo();
    }

    public function getSystemAttribute()
    {
        return "Hello World!!";
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d-m-Y');
    }

    public function setStartsAtAttribute($date){
        $this->attributes['p_date'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getStartsAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    public function setEndsAtAttribute($date){
        $this->attributes['p_date'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getEndsAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }
}
