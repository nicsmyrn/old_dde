<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //
    
    protected $table = 'permissions';
    
    protected $fillable = [
        'name',
        'slug',
        'description'
    ];
    
    public $timestamps = false;
    
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'permissions_roles', 'permission_id','role_id');
    }
}
