<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anaplirotis extends Model
{
    protected $table = 'anaplirotes';

    protected $fillable = [
        'afm',
        'moria',
        'seira_topothetisis',
        'type',
        'orario'
    ];

    public function teacher()
    {
        return $this->morphOne(\App\Teacher::class, 'teacherable');
    }
}
