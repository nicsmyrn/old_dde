<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Praxi extends Model
{

    protected $table = 'praxeis';

    protected $fillable = [
        'decision_number',
        'decision_date',
        'description',
        'dde_protocol',
        'dde_protocol_date',
        'praxi_type',
        'ada',
        'url'
    ];

    protected $appends = [
        'current_year',
        'year_name'
    ];

    public function getYearNameAttribute()
    {
        $year = Year::find($this->year_id);

        return $year->name;
    }

    public function getCurrentYearAttribute()
    {
        $year = Year::find($this->year_id);

        return $year->current;
    }

    public function placements()
    {
        return $this->hasMany(\App\Placement::class);
    }

    public function year()
    {
        return $this->belongsTo(\App\Year::class);
    }

    public function getDecisionDateAttribute($date)
    {
        return Carbon::parse($date)->format('d-m-Y');
    }

    public function getDdeProtocolDateAttribute($date)
    {
        return Carbon::parse($date)->format('d-m-Y');
    }

}
