<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teachers';

    protected $fillable = [
        'middle_name',
        'klados_id',
        'phone',
        'mobile',
        'family_situation',
        'childs',
        'special_situation',
        'address',
        'city',
        'tk',
        'dimos_sinipiretisis',
        'dimos_entopiotitas',
        'years_experience',
        'activation_for_register',
        'is_checked',
        'teacherable_id',
        'teacherable_type',
        'request_to_change',
        'fid',
        'afm',
        'ex_years',
        'ex_months',
        'ex_days',
        'sex',
        'logoi_ygeias_idiou',
        'logoi_ygeias_sizigou',
        'logoi_ygeias_paidiwn',
        'logoi_ygeias_goneon',
        'logoi_ygeias_aderfon',
        'logoi_ygeias_exosomatiki',
        'fmid',
        'misthodosia_is_checked',
        'request_to_change_misthodosia',
        'misthodosia_counter',
        'misthodosia_new_request',
        'request_to_change_profile',
        'profile_new_request',
        'profile_counter',
        'new_eidikotita'
    ];

    public function user()
    {
        return $this->morphOne(\App\User::class,'userable');
    }

    public function fake()
    {
        return $this->hasOne('App\FakeTeacher','fid','fid');
    }

    public function fake_misthodosia()
    {
        return $this->hasOne('App\MisthodosiaFake', 'teacher_id', 'id');
    }

    public function yperarithmia_request()
    {
        return $this->hasMany('App\RequestTeacherYperarithmia', 'teacher_id', 'id');
    }

    public function myschool()
    {
        return $this->belongsTo('App\MySchoolTeacher', 'afm', 'afm');
    }

    public function teacherable()
    {
        return $this->morphTo();
    }

    public function placements()
    {
        return $this->hasMany(Placement::class, 'teacherable_id', 'id');
    }

    public function requests()
    {
        return $this->hasMany(RequestTeacher::class, 'teacher_id', 'id');
    }

    public function getKladosNameAttribute()
    {
        $eidikotita =  Eidikotita::find($this->klados_id);

        return $eidikotita->slug_name;
    }

    public function getKladosUnitedAttribute()
    {
        $eidikotita =  Eidikotita::find($this->klados_id);

        return $eidikotita->united;
    }

    public function getEidikotitaNameAttribute()
    {
        $eidikotita =  Eidikotita::find($this->klados_id);

        return $eidikotita->name;
    }


}
