<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class Eidiki extends Model
{
    //
    protected $table = 'eidiki_agogi';


    protected $fillable = [
        'name',
        'slug',
        'slug_name',
        'order',
        'sch_type'
    ];

    public $timestamps = false;

    public function schools()
    {
        return $this->belongsToMany('App\School', 'kena_eidikis', 'eid_id', 'sch_id')
            ->withPivot('value','last_user_login_id', 'description')
            ->withTimestamps();
    }

    public function getFullNameAttribute (){
        return "$this->slug_name  ($this->name)";
    }

    public function getLastUserAttribute()
    {
        $user = User::find($this->pivot->last_user_login_id);
        return "$user->last_name  $user->first_name";
    }

    public function getDateModifiedAttribute()
    {
        return Carbon::parse($this->pivot->updated_at)->format('d-m-Y h:i:s');
    }

    public function notZeroCellValues()
    {
        if ($this->schools->first(function($key, $value){
                return (($value->pivot->value > 0) || ($value->pivot->value < 0));
            }) == '')
            return 'NULL';
        return 'NOT NULL';
    }
}
