<?php

namespace App\Models\KTEL;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'ktel_period_school_comments';

    protected $appends = [
        'edit_cell'
    ];

    public function getEditCellAttribute()
    {
        return false;  //????
    }

    protected $fillable = [
        'school_id',
        'period_id',
        'disabled',
        'comment',
        'edit_cell'
    ];
}
