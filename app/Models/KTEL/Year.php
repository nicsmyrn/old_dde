<?php

namespace App\Models\KTEL;


use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $table = 'ktel_years';

    protected $fillable = [
        'name'
    ];

    public function months()
    {
        return $this->belongsToMany(\App\Models\KTEL\Month::class, 'ktel_period', 'year_id', 'month_id')
            ->withPivot(['id','disabled']);
    }
}
