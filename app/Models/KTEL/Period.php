<?php

namespace App\Models\KTEL;


use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $table = 'ktel_period';

    protected $fillable = [
        'year_id',
        'month_id',
        'disabled'
    ];

    public function getPeriodNameAttribute()
    {
        $month = Month::find($this->month_id);
        $year = Year::find($this->year_id);

        return "Σχολική χρονιά:$year->name  -  Μήνας:$month->name";
    }

    public function getYearAttribute()
    {
        return Year::find($this->year_id)->name;
    }

    public function getMonthAttribute()
    {
        return Month::find($this->month_id)->name;
    }
}
