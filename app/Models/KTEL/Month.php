<?php

namespace App\Models\KTEL;

use Illuminate\Database\Eloquent\Model;

class Month extends Model
{
    protected $table = 'ktel_months';

    protected $fillable = [
        'number',
        'name'
    ];

    public function years()
    {
        return $this->belongsToMany(\App\Models\KTEL\Year::class, 'ktel_period', 'month_id', 'year_id')
            ->withPivot('disabled');
    }
}
