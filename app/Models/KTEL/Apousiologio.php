<?php

namespace App\Models\KTEL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Apousiologio extends Model
{
    use SoftDeletes;

    protected $table = 'ktel_apousiologio';

    protected $fillable = [
        'periodroute_id',
        'days_apousias',
        'disabled'
    ];

    protected $dates = ['deleted_at'];

    public function periodroute()
    {
        return $this->belongsTo(\App\Models\KTEL\PeriodRoute::class, 'periodroute_id', 'id');
    }
}
