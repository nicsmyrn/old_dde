<?php

namespace App\Models\KTEL;

use Illuminate\Database\Eloquent\Model;

class PeriodSchoolComment extends Model
{
    protected $table = 'ktel_period_school_comments';


    protected $fillable = [
        'sch_rt_id',
        'period_id',
        'disabled',
        'comment'
    ];


    public function schoolroute()
    {
        return $this->belongsTo(\App\Models\KTEL\SchoolRoute::class, 'sch_rt_id', 'id');
    }

    public function getRouteTypeAttribute()
    {
        $schoolRoute = SchoolRoute::find($this->sch_rt_id);

        return Busroute::find($schoolRoute->route_id)->routetype_id;
    }

    public function getRouteContractAttribute()
    {
        $schoolRoute = SchoolRoute::find($this->sch_rt_id);

        return Busroute::find($schoolRoute->route_id)->aa_contract;
    }

    public function getRouteNameAttribute()
    {
        $schoolRoute = SchoolRoute::find($this->sch_rt_id);

        return Busroute::find($schoolRoute->route_id)->name;
    }
}
