<?php

namespace App\Models\KTEL;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Busroute extends Model
{
    use SoftDeletes;

    protected $table = 'ktel_busroute';

    protected $fillable = [
        'name',
        'municipality_id',
        'routetype_id',
        'aa_contract',
        'name_type'
    ];

    protected $dates = ['deleted_at'];

    public function municipality()
    {
        return \Config::get('requests.dimos')[$this->id];
    }

//    public function getTypeAttribute()
//    {
//        return \Config::get('ktel.routetype')[$this->id];
//    }

    public function schools()
    {
        return $this->hasMany(\App\Models\KTEL\SchoolRoute::class, 'route_id', 'id');
    }


    public function getRoutetypeIdAttribute($value)
    {
        return \Config::get('ktel.routetype')[$value];
    }
//
//    public function setRoutetypeIdAttribute($value)
//    {
//        $this->attributes['routetype_id'] = array_search($value, \Config::get('ktel.routetype'));
//    }
}
