<?php

namespace App\Models\KTEL;


use Illuminate\Database\Eloquent\Model;

class SchoolRoute extends Model
{
    protected $table = 'ktel_schoolroutes';

    protected $fillable = [
        'school_id',
        'route_id',
        'checked',
        'locked'
    ];

    public function period()
    {
        return $this->hasMany(\App\Models\KTEL\PeriodRoute::class, 'schoolroute_id', 'id');
    }

    public function getRouteNameAttribute()
    {
        return Busroute::where('id', $this->route_id)->first()->name;
    }

    public function getContractAttribute()
    {
        return Busroute::where('id', $this->route_id)->first()->aa_contract;
    }
}
