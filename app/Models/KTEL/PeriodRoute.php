<?php

namespace App\Models\KTEL;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Models\KTEL\Busroute;
use Carbon\Carbon;

class PeriodRoute extends Model
{
    use SoftDeletes;

    protected $table = 'ktel_period_routes';


    protected $fillable = [
        'schoolroute_id',
        'period_id',
        'kids_number',
        'routes_number',
        'description',
        'disabled',
        'starts_at',
        'new'
    ];


    protected $dates = ['deleted_at'];      // starts_at removed

    public function setStartsAtAttribute($date){
        $this->attributes['starts_at'] = Carbon::createFromFormat('H:i', $date);
    }

    public function getStartsAtAttribute($date)
    {
        return Carbon::parse($date)->format('H:i');
    }

    public function schoolroute()
    {
        return $this->belongsTo(\App\Models\KTEL\SchoolRoute::class, 'schoolroute_id', 'id');
    }

    public function absences()
    {
        return $this->hasMany(\App\Models\KTEL\Apousiologio::class, 'periodroute_id', 'id');
    }

    public function getPeriodNameAttribute()
    {
        return 'Σχολική χρονιά bla bla bla';
    }

    public function getRouteTypeAttribute()
    {
        $schoolRoute = SchoolRoute::find($this->schoolroute_id);

        return Busroute::find($schoolRoute->route_id)->routetype_id;
    }

    public function getRouteContractAttribute()
    {
        $schoolRoute = SchoolRoute::find($this->schoolroute_id);

        return Busroute::find($schoolRoute->route_id)->aa_contract;
    }

    public function getRouteNameAttribute()
    {
        $schoolRoute = SchoolRoute::find($this->schoolroute_id);

        return Busroute::find($schoolRoute->route_id)->name;
    }
}
