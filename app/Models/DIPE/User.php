<?php

namespace App\Models\DIPE;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $connection = 'dipe';

    protected $table = 'users';

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'role_id',
        'userable_id',
        'userable_type',
        'provider',
        'provider_id',
        'login_at',
        'sex',
        'second_mail'
    ];


    protected $hidden = ['password', 'remember_token'];

    protected $dates = ['login_at'];


    public function getFullNameAttribute(){
        return $this->last_name .' '. $this->first_name;
    }

    public function getLoginAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }


}
