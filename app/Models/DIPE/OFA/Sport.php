<?php

namespace App\Models\OFA;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sport extends Model
{
    use SoftDeletes;

    protected $connection = 'dipe';

    protected $table = 'ofa_sports';

    protected $fillable = [
        'name',
        'individual',
        'list_limit',
        'identifier'
    ];

    protected $dates = ['deleted_at'];

    public function special()
    {
        return $this->hasMany(SpecialSport::class, 'sport_id', 'id');
    }

}
