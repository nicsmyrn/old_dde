<?php

namespace App\Models\OFA;


use App\School;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Student extends Model
{
    use SoftDeletes;

    protected $connection = 'dipe';


    protected $table = 'ofa_students';
    
    protected $fillable = [
        'am',
        'last_name',
        'first_name',
        'middle_name',
        'birth',
        'class',
        'sex',
        'school_id',
        'year_birth',
        'mothers_name'

    ];

    protected $dates = ['deleted_at'];

    public function setBirthAttribute($date){
        $this->attributes['birth'] = Carbon::createFromFormat('d/m/Y', $date);
    }

    public function getBirthAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id', 'id');
    }
}
