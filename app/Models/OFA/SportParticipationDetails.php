<?php

namespace App\Models\OFA;


use Illuminate\Database\Eloquent\Model;

class SportParticipationDetails extends Model
{
    protected $table = 'ofa_p_status_details';

    protected $fillable = [
        'p_status_id',
        'list_details_id'
    ];


    protected $appends = [
        'student_id'
    ];

    public function getStudentIdAttribute()
    {
        return SportListDetails::where('id', $this->list_details_id)->first()->student_id;
    }

//    public function getStudentAttribute()
//    {
//        return Student::find($this->getStudentIdAttribute());
//    }


    public function p_status()
    {
        return $this->belongsTo(SportParticipation::class, 'p_status_id', 'id');
    }

    public function student_in_list()
    {
        return $this->belongsTo(SportListDetails::class, 'list_details_id', 'id');
    }
}
