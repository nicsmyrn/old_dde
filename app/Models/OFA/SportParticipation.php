<?php

namespace App\Models\OFA;


use Illuminate\Database\Eloquent\Model;

class   SportParticipation extends Model
{
    protected $table = 'ofa_participation_status';

    protected $fillable = [
        'list_id',
        'description',
        'phase'
    ];

    public function sport_list()
    {
        return $this->belongsTo(SportList::class, 'list_id', 'id');
    }

    public function details()
    {
        return $this->hasMany(SportParticipationDetails::class, 'p_status_id', 'id');
    }
}
