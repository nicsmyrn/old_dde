<?php

namespace App\Models\OFA;


use Illuminate\Database\Eloquent\Model;

class SportListDetails extends Model
{
    protected $table = 'ofa_list_details';

    protected $fillable = [
        'list_id',
        'student_id',
        'statement',
        'sport_id_special'
    ];

    protected $appends = [
        'special_name'
    ];

    public function sport_list()
    {
        return $this->belongsTo(SportList::class, 'list_id', 'id');
    }

    public function getStudentAttribute()
    {
        return Student::where('am', $this->student_id)->where('school_id', $this->sport_list->school_id)->first();
    }
//
//    public function student()
//    {
//        return $this->belongsTo(Student::class, 'student_id', 'am');
//    }

    public function special()
    {
        return $this->belongsTo(SpecialSport::class, 'sport_id_special', 'id');
    }

    public function getSpecialNameAttribute()
    {
        if($this->sport_id_special != 0){
            return SpecialSport::find($this->sport_id_special)->name;
        }else{
            return 'nick_sport_name';
        }
    }
}
