<?php

namespace App;


use Laravel\Socialite\Contracts\Factory as Socialite;
use Illuminate\Contracts\Auth\Guard;
use App\Repositories\UserRepository;

class AuthenticateUser{
    
    protected $auth;
    protected $users;
    protected $socialite;
    
    public function __construct(Socialite $socialite, UserRepository $users, Guard $auth)
    {
        $this->socialite = $socialite;
        $this->users = $users;
        $this->auth = $auth;
    }
    
    public function execute($hasCode, $listener,  $provider)
    {
        if (!$hasCode) return $this->getAuthorizationFirst($provider);

        $user = $this->users->findByUsernameOrCreate($this->getSocialUser($provider), $provider);
//        return $user;
//        return 'Teachers are not Allowed to login';

        $this->auth->login($user,true);

        $lockout = [
            'pysdechanion@gmail.com',
            'ofachania@gmail.com',
            'nicsmyrn@gmail.com'
        ];

        if(!in_array($user->email, $lockout)){
            dd('MΗ ΔΙΑΘΕΣΙΜΟ. Παρακαλώ συνδεθείτε με το Πανελλήνιο Σχολικό Δίκτυο');
        }


        return $listener->userHasLoggedIn($user);
        
    }
    
    private function getAuthorizationFirst($provider)
    {
        return $this->socialite->driver($provider)->redirect();
    }
    
    private function getSocialUser($provider)
    {
        return $this->socialite->driver($provider)->user();
    }
}