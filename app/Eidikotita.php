<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Carbon\Carbon;

class Eidikotita extends Model
{
    protected $table = 'eidikotita';

    protected $fillable = [
        'name',
        'slug',
        'slug_name',
        'united',
        'order',
        'sch_type'
    ];

    public $timestamps = false;

    public function yperarithmies()
    {
        return $this->belongsToMany(School::class, 'yperarithmies', 'eidikotita_id', 'school_id')
            ->withPivot('number', 'description');
    }

    public function schools()
    {
        return $this->belongsToMany('App\School', 'kena', 'eid_id', 'sch_id')
            ->withPivot('value', 'last_user_login_id', 'description')
            ->withTimestamps();
    }

    public function getFullNameAttribute()
    {
        return "$this->slug_name  ($this->name)";
    }

    public function getLastUserAttribute()
    {
        $user = User::find($this->pivot->last_user_login_id);
        if ($user->userable_type == 'App\School') {
            return "$user->last_name  $user->first_name";
        } else {
            return "ΠΥΣΔΕ";
        }
    }

    public function getUnitedFullName()
    {
        return "$this->name [$this->united]";
    }

    public function getDateModifiedAttribute()
    {
        return Carbon::parse($this->pivot->updated_at)->format('d-m-Y h:i:s');
    }

    public function notZeroCellValues()
    {
        if ($this->schools->first(function ($key, $value) {
                return (($value->pivot->value > 0) || ($value->pivot->value < 0));
            }) == ''
        )
            return 'NULL';
        return 'NOT NULL';
    }

    public function notZeroOrganikaOmada1()
    {
        if ($this->yperarithmies->first(function($key, $value){
                if ($value->group == 'Ομάδα 1'){
                    return (($value->pivot->number > 0) || ($value->pivot->number < 0));
                }
            }) == ''
        ) return 'NULL';
        return 'NOT NULL';
    }

    public function notZeroOrganikaOmada2345()
    {
        if ($this->yperarithmies->first(function($key, $value){
                if ($value->group != 'Ομάδα 1'){
                    return (($value->pivot->number > 0) || ($value->pivot->number < 0));
                }
            }) == ''
        ) return 'NULL';
        return 'NOT NULL';
    }

    static function countPE04()
    {
        $all = Eidikotita::with('schools')->where('united', 'pe04')->get();

        $counter = 0;

        foreach ($all as $eidikotita){
            $numberOfSchoolsThatHasNotZeroValue = $eidikotita->schools->filter(function($school){
                if ($school->pivot->value != 0){
                    return true;
                }
            })->count();

            if ($numberOfSchoolsThatHasNotZeroValue != 0){
                $counter = $counter + 1;
            }
        }

        return $counter;

    }
}

