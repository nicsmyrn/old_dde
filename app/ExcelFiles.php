<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExcelFiles extends Model
{
    protected $table = 'excel_files';

    protected $fillable = [
        'file_name',
        'file_date',
        'description'
    ];

}
