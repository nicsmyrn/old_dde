<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FakeTeacher extends Model
{
    //

    protected $table = 'fake_teachers';

    protected $primaryKey = 'fid';

    protected $fillable = [
        'middle_name',
        'klados_id',
        'phone',
        'mobile',
        'family_situation',
        'childs',
        'special_situation',
        'address',
        'city',
        'tk',
        'dimos_sinipiretisis',
        'dimos_entopiotitas',
        'years_experience',
        'activation_for_register',
        'is_checked',
        'teacherable_id',
        'teacherable_type',
        'ex_years',
        'ex_months',
        'ex_days',
        'sex',
        'request_to_change',
        'logoi_ygeias_idiou',
        'logoi_ygeias_sizigou',
        'logoi_ygeias_paidiwn',
        'logoi_ygeias_goneon',
        'logoi_ygeias_aderfon',
        'logoi_ygeias_exosomatiki'
    ];

    public function teacher()
    {
        return $this->belongsTo('App\Teacher', 'fid','fid');
    }

    public function teacherable()
    {
        return $this->morphTo();
    }

    public function getKladosNameAttribute()
    {
        $eidikotita =  Eidikotita::find($this->klados_id);

        return $eidikotita->slug_name;
    }

    public function getKladosUnitedAttribute()
    {
        $eidikotita =  Eidikotita::find($this->klados_id);

        return $eidikotita->united;
    }

    public function getEidikotitaNameAttribute()
    {
        $eidikotita =  Eidikotita::find($this->klados_id);

        return $eidikotita->name;
    }
}
