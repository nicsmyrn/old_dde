<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Notification extends Model
{
    //
    protected $table = 'notifications';

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'type',
        'url',
        'unread',
        'forRole',
        'forUser',
        'uniqueAction'
    ];

    protected $appends = [
        'hour_time',
        'minute_time',
        'time'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id', 'id');
    }

    public function getTimestampAttribute()
    {
        return Carbon::parse($this->created_at)->format('d/m/Y και ώρα H:i:s');
    }

    public function getHourTimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('H');
    }

    public function getMinuteTimeAttribute()
    {
        return Carbon::parse($this->created_at)->format('i');
    }

    public function getTimeAttribute()
    {
        return Carbon::parse($this->created_at)->toIso8601String();
    }
}
