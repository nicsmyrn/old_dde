<?php

namespace App\Console\Commands;

use App\Edata;
use App\Misthodosia;
use App\MySchoolTeacher;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class EdataUpdateMoria extends Command
{

    protected $signature = 'edata:moria';

    //
    /*
     * Για να λειτουργήσει σωστά, θα πρέπει από έναν υπολογιστή με Windows να μπω στο myschool
     * και να εξάγω τους μόνιμους καθηγητές. Άνοιγμα από Excel και εξαγωγή ως csv νέο.
     *  Το στέλνω E-mail και αντιγράφω από την προεπισκόπιση
     * του gmail το csv αρχείο στο αρχείο του server.
     */

    private $teachers = array();


    protected $description = 'Update Moria From E-Data';
    protected $path = 'seeds/data/Edata/edata_all_teachers_MORIA.csv';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfTeachers();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);

            foreach($this->teachers as $teacher){
                $t = Edata::where('am', $teacher['registryno'])->first();

                if ($t == null){
                    $this->createTeacher($teacher);
                }else{
                    //nothing
                }
                $bar->advance();
            }

            $bar->finish();
            $this->comment("\n Script Completed...\n");
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names, $total)
    {
        while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        $this->comment("\nCSV file  loaded $total teachers...");

        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ',');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }


    /**
     * @param $teacher
     */
    private function createTeacher($teacher)
    {
        Edata::create([
            'am' => $teacher['registryno'],
            'special_disease' => 0,
            'special_many_children' => 0,
            'special_judiciary' => 0,
            'sum_moria' => $teacher['textbox59'],
            'entopiotita' => 0,
            'sinipiretisi' => 0,
            'dimos_entopiotitas' => 0,
            'dimos_sinipiretisis' => 0,
        ]);
    }
}
