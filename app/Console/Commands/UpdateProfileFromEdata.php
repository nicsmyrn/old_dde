<?php

namespace App\Console\Commands;

use App\Edata;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\MySchoolTeacher;
use Carbon\Carbon;

class UpdateProfileFromEdata extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'edata:profile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Pasifai Teacher Profile from E-Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $edataCollection = Edata::where('aitisi_veltiwsis', true)->get();

        $counterHasUser = 0;
        $counterNotHaveUser = 0;

        foreach($edataCollection as $edataProfile){
            $mySchool = MySchoolTeacher::where('am', $edataProfile->am)->first();
            $message = 'Επώνυμο:'.$mySchool->last_name . ' - ΑΜ:'.$mySchool->am;
            if($mySchool->teacher != null){
                    $counterHasUser += 1;
                    \DB::beginTransaction();
                    $mySchool->teacher->update([
                        'dimos_sinipiretisis'   => $edataProfile->dimos_sinipiretisis,
                        'dimos_entopiotitas'    => $edataProfile->dimos_entopiotitas,
                        'special_situation'     => $edataProfile->special_situation,
                        'ex_years'              => $edataProfile->ex_years,
                        'ex_months'              => $edataProfile->ex_months,
                        'ex_days'              => $edataProfile->ex_days,
                        'childs'                  => $edataProfile->sum_tekna,
                        'family_situation'      => $edataProfile->moria_family_situation > 0 ? 1 : 0
                    ]);

                    $mySchool->teacher->teacherable->update([
                        'moria' => $edataProfile->sum_moria
                    ]);
                    \DB::commit();
            }else{
                $counterNotHaveUser += 1;
                $this->comment($message . ' του '. $mySchool->middle_name);
            }
        }

        $this->comment('Users:' . $counterHasUser . ' --- Not with User:'. $counterNotHaveUser);
    }
}
