<?php

namespace App\Console\Commands;

use App\Misthodosia;
use App\MySchoolTeacher;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class MisthodosiaDataInsert extends Command
{

    private $teachers = array();
    private $updatedTeachers = 0;
    private $createdTeachers = 0;
    private $untouchedTeachers = 0;

    protected $path = 'seeds/data/DIAS_v1.csv';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'misthodosia:insert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Data from DIAS platform';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfTeachers();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);

            foreach($this->teachers as $count=>$misthodosia){
                $myschoolTeacher = MySchoolTeacher::find($misthodosia['afm']);
//                $this->comment('ΑΦΜ1:'.$teacher['afm']. ' - ΑΦΜ2:'.$t->afm);

                if($myschoolTeacher != null){
                    if ($myschoolTeacher->misthodosia == null){
                        $this->createTeacher($misthodosia, $myschoolTeacher);
                    }else{
                        $this->comment('Teacher with afm: '. $misthodosia['afm'] .' not exist');
//                    $this->updateOrNothing($t, $teacher);
                    }
                }else{
                    $this->comment('Teacher with afm: '. $misthodosia['afm'] .' not exist');
                }

                $bar->advance();
            }

            $bar->finish();
            $this->comment("\n Updated: $this->updatedTeachers - Created: $this->createdTeachers - Untouched: $this->untouchedTeachers\n");
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names, $total)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        $this->comment("\nCSV file  loaded $total teachers...");

        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }

    /**
     * @param $t
     * @param $teacher
     */
    private function updateOrNothing($t, $teacher)
    {
        $updated = false;
        if ($t->am != $teacher['am']) {
            $t->am = $teacher['am'];
            $updated = true;
        }
        if ($t->last_name != $teacher['epwnymo']) {
            $t->last_name = $teacher['epwnymo'];
            $updated = true;
        }
        if ($t->first_name != $teacher['onoma']) {
            $t->first_name = $teacher['onoma'];
            $updated = true;
        }
        if ($t->middle_name != $teacher['patrwnymo']) {
            $t->middle_name = $teacher['patrwnymo'];
            $updated = true;
        }
        if ($t->mothers_name != $teacher['mhtrwnymo']) {
            $t->mothers_name = $teacher['mhtrwnymo'];
            $updated = true;
        }
        if ($t->phone != $teacher['thlefwno-ekpaideytikoy']) {
            $t->phone = $teacher['thlefwno-ekpaideytikoy'];
            $updated = true;
        }
        if ($t->mobile != $teacher['kinhto-thlefwno-ekpaideytikoy']) {
            $t->mobile = $teacher['kinhto-thlefwno-ekpaideytikoy'];
            $updated = true;
        }
        if ($t->eidikotita != $teacher['kwd-eidikothtas']) {
            $t->eidikotita = $teacher['kwd-eidikothtas'];
            $updated = true;
        }
        if ($t->vathmos != $teacher['baomos']) {
            $t->vathmos = $teacher['baomos'];
            $updated = true;
        }
        if ($t->ypoxreotiko != $teacher['ypox-wrario']) {
            $t->ypoxreotiko = $teacher['ypox-wrario'];
            $updated = true;
        }
        if ($t->organiki_prosorini != $teacher['monada-organikhsproswrinhs-topooethshs']) {
            $t->organiki_prosorini = $teacher['monada-organikhsproswrinhs-topooethshs'];
            $updated = true;
        }
        if ($t->topothetisi != $teacher['sxesh-topooethshs']) {
            $t->topothetisi = $teacher['sxesh-topooethshs'];
            $updated = true;
        }
        if ($t->dieuthinsi != array_search($teacher['perioxh-metaoeshs-ekp'],\Config::get('requests.perioxi'))) {
            $t->dieuthinsi =  array_search($teacher['perioxh-metaoeshs-ekp'],\Config::get('requests.perioxi'));
            $updated = true;
        }

        if ($updated) {
            $t->save();
            $this->updatedTeachers++;
        } else $this->untouchedTeachers++;
    }

    /**
     * @param $teacher
     */
    private function createTeacher($data, $myschoolTeacher)
    {
        $mis = new Misthodosia([
            'at'        => $data['at'],
            'birth'     => $data['birth'] == 0 ? null : $data['birth'],
            'family_situation'  => array_search($data['family-situation'], \Config::get('misthodosia.family_situation_list')) ? array_search($data['family-situation'], \Config::get('misthodosia.family_situation_list')) : 666,
            'iban'              => $data['iban'],
            'bank'              => array_search($data['bank'], \Config::get('misthodosia.banks')) ? array_search($data['bank'], \Config::get('misthodosia.banks')) : 666,
            'amka'              => $data['amka'],
            'doy'               => array_search($data['doy'], \Config::get('misthodosia.list_doy')) ? array_search($data['doy'], \Config::get('misthodosia.list_doy')) : 666,
            'am_tsmede'         => $data['am-tsmede'],
            'am_ika'            => $data['am-ika'],
            'new'               => $data['new'] == 'Νέος Ασφαλισμένος' ? 1 : 0,
            'mk'                => $data['mk']
        ]);

        $myschoolTeacher->misthodosia()->save($mis);

        if($mis == null){
            $this->comment('Errror');
        }

        $this->createdTeachers++;
    }
}
