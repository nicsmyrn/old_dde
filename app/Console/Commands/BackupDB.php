<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

class BackupDB extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backing up all the data of database tables';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $cmd = 'mysqldump --user='.env('DB_USERNAME').' --password='.env('DB_PASSWORD').' '.env('DB_DATABASE').' > '.storage_path('app/backups/').env('DB_DATABASE').'_'.date('d_m_Y_h_i_s').'.sql';
        $process = new Process($cmd);
        $process->run();

        $bar = $this->output->createProgressBar(1000);

        for($i=0;$i<1000;$i++){
            $bar->advance();
        }

        $bar->finish();

        $this->comment(' --> Database backed up with success at:'. date('d-m-Y h:i:s'));

        //        $builder = new ProcessBuilder();
        //        $builder->setPrefix('mysqldump');
        //        $builder->setArguments([
        //            '--user='.env('DB_USERNAME'),
        //            '--password='.env('DB_PASSWORD'),
        //            env('DB_DATABASE'),
        //        ]);
        //
        //        $builder->getProcess()->run();
    }
}
