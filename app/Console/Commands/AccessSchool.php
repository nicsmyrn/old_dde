<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Role;

class AccessSchool extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'school:access {permission}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Access Rights of School Role (Edit/Read) {permission: The permission of the School Role}';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $role_school = Role::where('slug', 'school')->first();
        
        if($role_school->hasPermission($this->argument('permission'))){
            $role_school->takePermission($this->argument('permission'));
            
            \Event::fire(new \App\Events\MailsForKena());
            
            $message = 'Αφαιρέθηκε το δικαίωμα '.$this->argument('permission').' από τα Σχολεία';
        }else{
            $role_school->givePermission($this->argument('permission'));
            $message = 'Τα Σχολεία έχουν το Δικαίωμα :'.$this->argument('permission');
        }
        $this->info($message);
    }
}
