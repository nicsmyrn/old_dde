<?php

namespace App\Console\Commands;

use App\MySchoolTeacher;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class MySchoolUpdateApospasmenous extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'myschool:apospasmenoi';

    private $teachers = array();
    private $updatedTeachers = 0;
    private $createdTeachers = 0;
    private $untouchedTeachers = 0;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'insert or update apospasmenous to my school table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();


        $total = $this->getNumberOfTeachers();

        if ( ($handle = fopen(database_path('seeds/data/myschool_apospasmenoi2.csv'),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);

            foreach($this->teachers as $teacher){
                $t = MySchoolTeacher::find($teacher['afm']);
                if ($t == null){
                    $this->createTeacher($teacher);
                }else{
                    $this->updateOrNothing($t, $teacher);
                }
                $bar->advance();
            }

            $bar->finish();
            $this->comment("\n Updated: $this->updatedTeachers - Created: $this->createdTeachers - Untouched: $this->untouchedTeachers\n");
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names, $total)
    {
        ini_set('auto_detect_line_endings', true);

        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;

            foreach ($columns_names as $key) {
                if($key == 'afm'){
                    $record[$key] = $this->getIntegerFromString($data[$i++]);
                }else{
                    $record[$key] = $data[$i++];
                }
            }
            $this->teachers[] = $record;

        }
        $this->comment("\nCSV file  loaded $total teachers...");

        fclose($handle);
    }

    private function getIntegerFromString($string)
    {
        return preg_replace('/[^0-9]+/', '', $string);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        ini_set('auto_detect_line_endings', true);
        $fp = file(database_path('seeds/data/myschool_apospasmenoi.csv'));
        return  count($fp) - 1;
    }

    /**
     * @param $t
     * @param $teacher
     */
    private function updateOrNothing($t, $teacher)
    {
        $updated = false;
        if ($t->am != $teacher['am']) {
            $t->am = $teacher['am'];
            $updated = true;
        }
        if ($t->last_name != $teacher['epwnymo']) {
            $t->last_name = $teacher['epwnymo'];
            $updated = true;
        }
        if ($t->first_name != $teacher['onoma']) {
            $t->first_name = $teacher['onoma'];
            $updated = true;
        }
        if ($t->middle_name != $teacher['patrwnymo']) {
            $t->middle_name = $teacher['patrwnymo'];
            $updated = true;
        }
        if ($t->mothers_name != $teacher['mhtrwnymo']) {
            $t->mothers_name = $teacher['mhtrwnymo'];
            $updated = true;
        }
        if ($t->phone != $teacher['thlefwno-ekpaideytikoy']) {
            $t->phone = $teacher['thlefwno-ekpaideytikoy'];
            $updated = true;
        }
        if ($t->mobile != $teacher['kinhto-thlefwno-ekpaideytikoy']) {
            $t->mobile = $teacher['kinhto-thlefwno-ekpaideytikoy'];
            $updated = true;
        }
        if ($t->eidikotita != $teacher['kwd-eidikothtas']) {
            $t->eidikotita = $teacher['kwd-eidikothtas'];
            $updated = true;
        }
        if ($t->vathmos != $teacher['baomos']) {
            $t->vathmos = $teacher['baomos'];
            $updated = true;
        }
        if ($t->ypoxreotiko != $teacher['ypox-wrario']) {
            $t->ypoxreotiko = $teacher['ypox-wrario'];
            $updated = true;
        }
        if ($t->organiki_prosorini != $teacher['monada-apospashs']) {
            $t->organiki_prosorini = $teacher['monada-apospashs'];
            $updated = true;
        }
        if ($t->topothetisi != $teacher['sxesh-topooethshs']) {
            $t->topothetisi = $teacher['sxesh-topooethshs'];
            $updated = true;
        }

        if ($t->dieuthinsi != array_search($teacher['perioxh-metaoeshs-ekp'],\Config::get('requests.perioxi'))) {
            $t->dieuthinsi =  array_search($teacher['perioxh-metaoeshs-ekp'],\Config::get('requests.perioxi'));
            $updated = true;
        }

        if ($updated) {
            $t->save();
            $this->updatedTeachers++;
        } else $this->untouchedTeachers++;
    }

    /**
     * @param $teacher
     */
    private function createTeacher($teacher)
    {
        MySchoolTeacher::create([
            'afm' => $teacher['afm'],
            'am' => isset($teacher['am']) ? $teacher['am'] : '',
            'sex' => isset($teacher['fylo']) ?  $teacher['fylo'] == 'Θ' ? 0 : 1 : 0,
            'last_name' => isset($teacher['epwnymo']) ? $teacher['epwnymo'] : '',
            'first_name' => isset($teacher['onoma']) ? $teacher['onoma'] : '',
            'middle_name' => isset($teacher['patrwnymo']) ? $teacher['patrwnymo'] : '',
            'mothers_name' => isset($teacher['mhtrwnymo']) ? $teacher['mhtrwnymo'] : '',
            'phone' => isset($teacher['thlefwno-ekpaideytikoy']) ? $teacher['thlefwno-ekpaideytikoy'] : '',
            'mobile' => isset($teacher['kinhto-thlefwno-ekpaideytikoy']) ? $teacher['kinhto-thlefwno-ekpaideytikoy'] : '',
            'eidikotita' => isset($teacher['kwd-eidikothtas']) ? $teacher['kwd-eidikothtas'] : '',
            'vathmos' => isset($teacher['baomos']) ? $teacher['baomos'] : '',
            'ypoxreotiko' => isset($teacher['ypox-wrario']) ? $teacher['ypox-wrario'] : '',
            'organiki_prosorini' => isset($teacher['monada-apospashs']) ? $teacher['monada-apospashs'] : '',
            'topothetisi' => isset($teacher['sxesh-topooethshs']) ? $teacher['sxesh-topooethshs'] : '',
            'birth' =>  '0000-00-00',
            'dieuthinsi' => array_search($teacher['perioxh-metaoeshs-ekp'],\Config::get('requests.perioxi'))
        ]);
        $this->createdTeachers++;
    }
}
