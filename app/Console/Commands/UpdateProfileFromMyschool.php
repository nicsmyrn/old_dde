<?php

namespace App\Console\Commands;

use App\Edata;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use App\MySchoolTeacher;
use Carbon\Carbon;

class UpdateProfileFromMyschool extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'myschool:profile';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Pasifai Teacher Profile from MySchool';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mySchoolCollection = MySchoolTeacher::all();

        $counterHasUser = 0;
        $counterNotHaveUser = 0;

        foreach($mySchoolCollection as $mySchoolProfile){


            if($mySchoolProfile->teacher != null){
                if($mySchoolProfile->am != ''){
                    if($mySchoolProfile->dieuthinsi == '5001a'){
                        $mySchoolProfile->teacher->teacherable->update([
                            'organiki'  => $mySchoolProfile->organiki_id,
                            'county'    => 49,
                            'orario'    => $mySchoolProfile->ypoxreotiko,
                            'dieuthinsi'    => $mySchoolProfile->dieuthinsi
                        ]);
                        $counterHasUser += 1;
                    }
                }
            }else{
                $counterNotHaveUser += 1;
            }

        }

        $this->comment('Users:' . $counterHasUser . ' --- Not with User:'. $counterNotHaveUser);
    }
}
