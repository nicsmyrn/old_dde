<?php

namespace App\Console\Commands;

use App\Misthodosia;
use App\Tekna;
use Illuminate\Console\Command;

class MisthodosiaTekna extends Command
{
    private $tekna = array();
    private $updatedTeachers = 0;
    private $createdTekna = 0;
    private $untouchedTeachers = 0;

    protected $path = 'seeds/data/DIAS_tekna_v1.csv';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'misthodosia:tekna';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert Tekna from DIAS platform';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfTekna();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTekna($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);

            foreach($this->tekna as $count=>$tekna){
                $misthodosiaProfile = Misthodosia::where('afm', $tekna['afm'])->first();

                if($misthodosiaProfile != null){
                        $this->createTekno($tekna, $misthodosiaProfile);
                }else{
                    $this->comment('Teacher with afm: '. $tekna['afm'] .' not exist');
                }

                $bar->advance();
            }

            $bar->finish();
            $this->comment("\n Updated: $this->updatedTeachers - Created: $this->createdTekna - Untouched: $this->untouchedTeachers\n");
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTekna($handle, $columns_names, $total)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->tekna[] = $record;
        }
        $this->comment("\nCSV file  loaded $total ΤΕΚΝΑ...");

        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTekna()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }

    /**
     * @param $teacher
     */
    private function createTekno($data, $misthodosiaProfile)
    {
        $tekno = new Tekna([
            'birth'     => $data['birth'] == 0 ? null : $data['birth'],
            'college_start_date'     => $data['college-start-date'] == 0 ? null : $data['college-start-date'],
            'college_end_date'     => $data['college-end-date'] == 0 ? null : $data['college-end-date'],
            'description'     => $data['description']
        ]);

        $misthodosiaProfile->tekna()->save($tekno);

        if($tekno == null){
            $this->comment('Errror');
        }

        $this->createdTekna++;
    }
}
