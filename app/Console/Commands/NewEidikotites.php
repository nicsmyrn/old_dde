<?php

namespace App\Console\Commands;

use App\MySchoolTeacher;
use App\NewEidikotita;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class NewEidikotites extends Command
{

    protected $signature = 'myschool:new';

    //
    /*
     * Για να λειτουργήσει σωστά, θα πρέπει από έναν υπολογιστή με Windows να μπω στο myschool
     * και να εξάγω τους μόνιμους καθηγητές. Άνοιγμα από Excel και εξαγωγή ως csv νέο.
     *  Το στέλνω E-mail και αντιγράφω από την προεπισκόπιση
     * του gmail το csv αρχείο στο αρχείο του server.
     */

    private $teachers = array();
    private $updatedTeachers = 0;
    private $createdTeachers = 0;
    private $untouchedTeachers = 0;

    protected $description = 'Insert New Eidikotites to all teachers';
    protected $path = 'seeds/data/teachers/tsouroupakis_anaplirotes_apospasmenoi.csv';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfTeachers();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);

            foreach($this->teachers as $teacher){
                $t = MySchoolTeacher::find($teacher['afm']);
//                $this->comment('ΑΦΜ1:'.$teacher['afm']. ' - ΑΦΜ2:'.$t->afm);

                if ($t == null){
//                    $this->createTeacher($teacher);
                }else{

                    $new = NewEidikotita::where('eidikotita_slug', $teacher['klados'])->first();


                    if($new != null){
                        $t->new_eidikotita = $new->id;
                        $t->save();
                        $this->comment("\n User with name : $t->last_name and afm $t->afm is ΠΕ04.05 ... \n");
                    }
//                    $t->new_eidikotita = isset($teacher['epwnymo']) ? $teacher['epwnymo'] : '',
//                    $this->updateOrNothing($t, $teacher);
                }
                $bar->advance();
            }

            $bar->finish();
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names, $total)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        $this->comment("\nCSV file  loaded $total teachers...");

        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }


}
