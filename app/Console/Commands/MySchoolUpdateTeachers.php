<?php

namespace App\Console\Commands;

use App\Misthodosia;
use App\MySchoolTeacher;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class MySchoolUpdateTeachers extends Command
{

    protected $signature = 'myschool:entos';

    //
    /*
     * Για να λειτουργήσει σωστά, θα πρέπει από έναν υπολογιστή με Windows να μπω στο myschool
     * και να εξάγω τους μόνιμους καθηγητές. Άνοιγμα από Excel και εξαγωγή ως csv νέο.
     *  Το στέλνω E-mail και αντιγράφω από την προεπισκόπιση
     * του gmail το csv αρχείο στο αρχείο του server.
     */

    private $teachers = array();
    private $updatedTeachers = 0;
    private $createdTeachers = 0;
    private $untouchedTeachers = 0;

    protected $description = 'Insert Or Update Teachers to database';
    protected $path = 'seeds/data/MySchool/myschool_teachers_03_05_2018.csv';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfTeachers();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);

            foreach($this->teachers as $teacher){
                $t = Misthodosia::where('afm', $teacher['afm'])->first();

                if ($t == null){
                    $this->createTeacher($teacher);
                }else{
                    $this->updateOrNothing($t, $teacher);
                }
                $bar->advance();
            }

            $bar->finish();
            $this->comment("\n Updated: $this->updatedTeachers - Created: $this->createdTeachers - Untouched: $this->untouchedTeachers\n");
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names, $total)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        $this->comment("\nCSV file  loaded $total teachers...");

        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }

    /**
     * @param $t
     * @param $teacher
     */
    private function updateOrNothing($t, $teacher)
    {
        $updated = false;
        if ($t->am != $teacher['am']) {
            $t->am = $teacher['am'];
            $updated = true;
        }
        if ($t->last_name != $teacher['epwnymo']) {
            $t->last_name = $teacher['epwnymo'];
            $updated = true;
        }
        if ($t->first_name != $teacher['onoma']) {
            $t->first_name = $teacher['onoma'];
            $updated = true;
        }
        if ($t->middle_name != $teacher['patrwnymo']) {
            $t->middle_name = $teacher['patrwnymo'];
            $updated = true;
        }
        if ($t->mothers_name != $teacher['mhtrwnymo']) {
            $t->mothers_name = $teacher['mhtrwnymo'];
            $updated = true;
        }
        if ($t->phone != $teacher['thlefwno-ekpaideytikoy']) {
            $t->phone = $teacher['thlefwno-ekpaideytikoy'];
            $updated = true;
        }
        if ($t->mobile != $teacher['kinhto-thlefwno-ekpaideytikoy']) {
            $t->mobile = $teacher['kinhto-thlefwno-ekpaideytikoy'];
            $updated = true;
        }
        if ($t->eidikotita != $teacher['kwd-eidikothtas']) {
            $t->eidikotita = $teacher['kwd-eidikothtas'];
            $updated = true;
        }
        if ($t->vathmos != $teacher['baomos']) {
            $t->vathmos = $teacher['baomos'];
            $updated = true;
        }
        if ($t->ypoxreotiko != $teacher['ypox-wrario']) {
            $t->ypoxreotiko = $teacher['ypox-wrario'];
            $updated = true;
        }
        if ($t->organiki_prosorini != $teacher['monada-organikhsproswrinhs-topooethshs']) {
            $t->organiki_prosorini = $teacher['monada-organikhsproswrinhs-topooethshs'];
            $updated = true;
        }
        if ($t->topothetisi != $teacher['sxesh-topooethshs']) {
            $t->topothetisi = $teacher['sxesh-topooethshs'];
            $updated = true;
        }
        if ($t->dieuthinsi != array_search($teacher['perioxh-metaoeshs-ekp'],\Config::get('requests.perioxi'))) {
            $t->dieuthinsi =  array_search($teacher['perioxh-metaoeshs-ekp'],\Config::get('requests.perioxi'));
            $updated = true;
        }

        if ($updated) {
            $t->save();
            $this->updatedTeachers++;
        } else $this->untouchedTeachers++;
    }

    /**
     * @param $teacher
     */
    private function createTeacher($teacher)
    {
        Misthodosia::create([
            'at' => $teacher['at'],
            'birth' => isset($teacher['am']) ? $teacher['am'] : '',
            'iban' => isset($teacher['fylo']) ?  $teacher['fylo'] == 'Θ' ? 0 : 1 : 0,
            'bank' => isset($teacher['epwnymo']) ? $teacher['epwnymo'] : '',
            'amka' => isset($teacher['onoma']) ? $teacher['onoma'] : '',
            'doy' => isset($teacher['patrwnymo']) ? $teacher['patrwnymo'] : '',
            'am_tsmede' => isset($teacher['mhtrwnymo']) ? $teacher['mhtrwnymo'] : '',
            'am_ika' => isset($teacher['thlefwno-ekpaideytikoy']) ? $teacher['thlefwno-ekpaideytikoy'] : '',
            'new' => isset($teacher['kinhto-thlefwno-ekpaideytikoy']) ? $teacher['kinhto-thlefwno-ekpaideytikoy'] : '',
            'mk' => isset($teacher['kwd-eidikothtas']) ? $teacher['kwd-eidikothtas'] : '',
            'teacher_id' => isset($teacher['baomos']) ? $teacher['baomos'] : '',
            'family_situation' => isset($teacher['ypox-wrario']) ? $teacher['ypox-wrario'] : ''
        ]);
        $this->createdTeachers++;
    }
}
