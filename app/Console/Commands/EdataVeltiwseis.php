<?php

namespace App\Console\Commands;

use App\Edata;
use App\Misthodosia;
use App\MySchoolTeacher;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class EdataVeltiwseis extends Command
{

    protected $signature = 'edata:veltiwseis';

    private $teachers = array();
    private $updatedTeachers = 0;

    protected $description = 'Update Teacher Profile for Veltiwseis';
    protected $path = 'seeds/data/Edata/veltiwseis2018_anakoinopoiisi.csv';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfTeachers();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);

            foreach($this->teachers as $key=>$teacher){
                $t = Edata::where('am', $teacher['am'])->first();

                if ($t == null){
                    $this->comment("\n Teacher with AM : {$teacher['am']} not Exist to database\n");
                }else{
                    $this->updateTeacher($t, $teacher);
                }
                $bar->advance();
            }

            $bar->finish();
            $this->comment("\n Edata Updated  {$this->updatedTeachers} teachers...");
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names, $total)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        $this->comment("\nCSV file  loaded $total teachers...");

        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }

    private function updateTeacher($t, $teacher)
    {
        $t->ex_years = $teacher['ex-years'];
        $t->ex_months = $teacher['ex-months'];
        $t->ex_days = $teacher['ex-days'];

        $t->moria_ypiresias = $teacher['moria-ypiresias'];
        $t->moria_sinthikes = $teacher['moria-sinthikes'];
        $t->moria_family_situation = $teacher['moria-family-situation'];
        $t->number_of_kids = $teacher['number-of-kids'];
        $t->number_of_kids_spoudes = $teacher['number-of-kids-spoudes'];
        $t->sum_tekna = $teacher['sum-tekna'];
        $t->moria_tekna = $teacher['moria-tekna'];
        $t->sum_moria = $teacher['sum-moria'];
        $t->dimos_entopiotitas = $teacher['dimos-entopiotitas'];
        $t->dimos_sinipiretisis = $teacher['dimos-sinipiretisis'];
        $t->special_situation = $teacher['special-situation'] == 'TRUE' ? 1 : 0;


        $t->aitisi_veltiwsis = true;

        $t->save();
        $this->updatedTeachers++;

    }


}
