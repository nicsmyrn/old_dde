<?php

namespace App\Console\Commands;

use App\Misthodosia;
use App\MySchoolTeacher;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class MisthodosiaNextMk extends Command
{

    private $teachers = array();
    private $updatedTeachers = 0;
    private $untouchedTeachers = 0;

    protected $path = 'seeds/data/DIAS_next_mk.csv';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'misthodosia:next_mk';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update employees with next mk date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $columns_names = array();

        $total = $this->getNumberOfTeachers();

        if ( ($handle = fopen(database_path($this->path),'r')) !== FALSE){

            $columns_names = $this->getHeaders($handle, $columns_names);

            $this->getTeachers($handle, $columns_names, $total);

            $bar = $this->output->createProgressBar($total);



            foreach($this->teachers as $count=>$mk_record){
                $myschoolTeacher = MySchoolTeacher::where('am', $mk_record['am'])->first();

//                $this->comment('ΑΦΜ1:'.$teacher['afm']. ' - ΑΦΜ2:'.$t->afm);

                if($myschoolTeacher != null){
                        $misthodosiaOfTeacher = $myschoolTeacher->misthodosia;

                        if($misthodosiaOfTeacher != null){
                            $misthodosiaOfTeacher->next_mk = $mk_record['next-mk'];
                            $misthodosiaOfTeacher->save();

                            $this->updatedTeachers++;
                        }else{
                            $this->untouchedTeachers++;
                        }

                }else{
                    $this->comment('Teacher with am: '. $mk_record['am'] .' not exist');
                    $this->untouchedTeachers++;
                }

                $bar->advance();
            }

            $bar->finish();
            $this->comment("\n Updated: $this->updatedTeachers  - Untouched: $this->untouchedTeachers\n");
        }else{
            $this->comment('Error Opening File');
        }
    }

    /**
     * @param $handle
     * @param $columns_names
     */
    private function getTeachers($handle, $columns_names, $total)
    {
        while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER
            $record = array();
            $i = 0;
            foreach ($columns_names as $key) {
                $record[$key] = $data[$i++];
            }
            $this->teachers[] = $record;
        }
        $this->comment("\nCSV file  loaded $total teachers...");

        fclose($handle);
    }

    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = str_slug($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }

}
