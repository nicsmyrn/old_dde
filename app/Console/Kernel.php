<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\AccessSchool::class,
        \App\Console\Commands\BackupDB::class,
        \App\Console\Commands\MySchoolUpdateTeachers::class,
        \App\Console\Commands\MySchoolUpdateApospasmenous::class,
        \App\Console\Commands\UpdateProfileFromEdata::class,
        \App\Console\Commands\UpdateProfileFromMyschool::class,
        \App\Console\Commands\MySchoolManual::class,
        \App\Console\Commands\MisthodosiaDataInsert::class,
        \App\Console\Commands\MisthodosiaTekna::class,
        \App\Console\Commands\InsertStudents::class,
        \App\Console\Commands\MisthodosiaNextMk::class,
        \App\Console\Commands\NewEidikotites::class,
        \App\Console\Commands\EdataUpdateMoria::class,
        \App\Console\Commands\EdataVeltiwseis::class,


    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();


        /*
         * sudo crontab -e
         *
         * add the line : * * * * * php /var/www/larry/artisan schedule:run >> /dev/null 2>&1
         */

//         $schedule->command('school:access edit_school_kena')
//                 ->dailyAt('12:00');
    
//         $schedule->call(function(){
//             \DB::table('f')->insert(['name'=>'hello', 'description'=>'hello '.date("Y-m-d H:i:s").' description']);
//         })->everyThirtyMinutes();
    }
}
