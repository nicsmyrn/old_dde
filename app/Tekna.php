<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Tekna extends Model
{
    protected $table = 'misthodosia_tekna';

    protected $fillable = [
        'birth',
        'college_start_date',
        'college_end_date',
        'misthodosia_id',
        'description'
    ];

    protected $dates = ['birth', 'college_start_date', 'college_end_date'];

    public function misthodosia()
    {
           return $this->belongsTo('App\Misthodosia', 'misthodosia_id', 'id');
    }

    public function setBirthAttribute($date)
    {
        $this->attributes['birth'] = $date ?  Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getBirthAttribute($date)
    {
        return $date ? Carbon::parse($date)->format('d/m/Y') : null;
    }

    public function setCollegeStartDateAttribute($date)
{
    $this->attributes['college_start_date'] = $date ?  Carbon::createFromFormat('d/m/Y', $date) : null;
}

    public function getCollegeStartDateAttribute($date)
    {
        return $date ? Carbon::parse($date)->format('d/m/Y') : null;
    }

    public function setCollegeEndDateAttribute($date)
    {
        $this->attributes['college_end_date'] = $date ?  Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getCollegeEndDateAttribute($date)
    {
        return $date ? Carbon::parse($date)->format('d/m/Y') : null;
    }
}
