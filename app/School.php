<?php

namespace App;

use App\Models\OFA\Student;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class School extends Model
{
    //
        
    protected $table = 'schools';
    
    protected $fillable = [
        'name',
        'type',
        'group',
        'order',
        'description',
        'confirmation_date',
        'municipality_id',
        'work_phone',
        'mobile_phone',
        'area',
        'sex'
    ];
    
    public $timestamps = false;

    public function students()
    {
        return $this->hasMany(Student::class, 'school_id', 'id');
    }

    public function routes()
    {
        return $this->hasMany(\App\Models\KTEL\SchoolRoute::class, 'school_id', 'id');
    }

    public function new_yperarithmies()
    {
        return $this->belongsToMany(NewEidikotita::class, 'new_yperarithmies', 'school_id', 'eidikotita_id')
            ->withPivot('number', 'description');
    }

    public function yperarithmies()
    {
        return $this->belongsToMany(Eidikotita::class, 'yperarithmies', 'school_id', 'eidikotita_id')
            ->withPivot('number', 'description');
    }
    
    public function eidikotites()
    {
        return $this->belongsToMany(\App\Eidikotita::class, 'kena', 'sch_id', 'eid_id')
            ->withPivot('value','last_user_login_id', 'description')
            ->withTimestamps();        
    }

    public function eidikis()
    {
        return $this->belongsToMany(\App\Eidiki::class, 'kena_eidikis', 'sch_id', 'eid_id')
            ->withPivot('value','last_user_login_id', 'description')
            ->withTimestamps();
    }

    public function kanonikes_eidikotites()
    {
        return $this->eidikotites->where('sch_type', 'all');
    }


    public function eeeek()
    {
        return $this->eidikotites->where('sch_type', 'eidiki');
    }

    public function notUnitedEidikotites()
    {
        return $this->eidikotites->where('united', null)->where('sch_type', 'all');
    }

    public function gymnasiumEidikotites()
    {
        return $this->eidikotites->where('sch_type', 'all')->where('plus', 'general');
    }

    public function pe04()
    {
        return $this->eidikotites->where('united', 'pe04');
    }

    public function mathimata()
    {
        return $this->belongsToMany(\App\Lesson::class, 'kena-mathimaton', 'sch_id', 'lesson_id')
            ->withPivot('value', 'last_user_login_id', 'description')
            ->withTimestamps();
    }
    
    
//    public function user()
//    {
//        return $this->hasOne('App\User', 'sch_id', 'id');
//    }
    public function user()
    {
        return $this->morphOne(\App\User::class,'userable');
    }

    public function getOrganikaAttribute()
    {
        return MySchoolTeacher::where('organiki_prosorini', $this->identifier)
                    ->whereIn('topothetisi', [
                        'Οργανικά',
                        'Οργανικά από Αμοιβαία Μετάθεση',
                        'Οργανικά από Αρση Υπεραριθμίας'
                    ])
                    ->orderBy('eidikotita')
                    ->get();
    }

    public function survey()
    {
        return $this->hasOne('App\Survey', 'sch_id', 'id');
    }

    public function getSlugNameAttribute()
    {
        return str_replace(' ','-', $this->name);
    }
    
    public function getLastUpdateAttribute()
    {
        $date =  \DB::table('kena')
            ->where('sch_id', $this->id)
            ->max('updated_at');
            
        return Carbon::parse($date)->format('d/m/Y και ώρα h:i:s');
    }

    public function getConfirmationAttribute()
    {
        if ($this->confirmation_date == '0000-00-00 00:00:00'){
            return '';
        }else{
            return Carbon::parse($this->confirmation_date)->format('d/m/Y και ώρα h:i:s');
        }
    }


    public function getHasProjectAttribute()
    {
        if($this->mathimata->find(28)){
            return $this->mathimata->find(28)->pivot->value != 0 ? $this->mathimata->find(28)->pivot->value : '';
        }
        return '';
    }

    public function getHasTechnologyAttribute()
    {
        if($this->mathimata->find(29)){
            return $this->mathimata->find(29)->pivot->value != 0 ? $this->mathimata->find(29)->pivot->value : '';
        }
        return '';
    }

    public function getHasSxedioAttribute()
    {
        if($this->mathimata->find(31)){
            return $this->mathimata->find(31)->pivot->value != 0 ? $this->mathimata->find(31)->pivot->value : '';
        }
        return '';
    }

    public function getHasPe091013Attribute()
    {
        if($this->mathimata->find(30)){
            return $this->mathimata->find(30)->pivot->value != 0 ? $this->mathimata->find(30)->pivot->value : '';
        }
        return '';
    }

    public function cellValue($id, $eidikotita)
    {
        if ($this->eidikotites->find($id)){
            if ($this->eidikotites->find($id)->pivot->value != 0)
                return $this->eidikotites->find($id)->pivot->value;
            else
                return '';
        }else{
            return $this->isLesson($eidikotita);
        }
    }

    public function cellValueOrganika($id, $eidikotita)
    {
        if ($this->yperarithmies->find($id)){
            if ($this->yperarithmies->find($id)->pivot->number != 0)
                return $this->yperarithmies->find($id)->pivot->number;
            else
                return '';
        }
        return '';
    }
    
    public function cellValueEidikis($id, $eidikotita)
    {
        if ($this->eidikis->find($id)){
            if ($this->eidikis->find($id)->pivot->value != 0)
                return $this->eidikis->find($id)->pivot->value;
            else
                return '';
        }
        return '';
    }

    public function isLesson($eidikotita)
    {
        if ($eidikotita == 'pe05') {
            if ($this->type == 'Γυμνάσιο' && $this->mathimata->find(26) && $this->mathimata->find(27)) {
                if ($this->mathimata->find(26)->pivot->value == 0 && $this->mathimata->find(27)->pivot->value == 0)
                    return '';
                elseif ($this->mathimata->find(26)->pivot->value == 0 && $this->mathimata->find(27)->pivot->value != 0)
                    return $this->mathimata->find(27)->pivot->value;
                elseif ($this->mathimata->find(26)->pivot->value != 0 && $this->mathimata->find(27)->pivot->value == 0)
                    return $this->mathimata->find(26)->pivot->value;
                else
                    return '[' . $this->mathimata->find(26)->pivot->value . ',' . $this->mathimata->find(27)->pivot->value . ']';
            }
        }
        if ($eidikotita == 'pe06') {
            if ($this->type == 'Γυμνάσιο' && $this->mathimata->find(22) && $this->mathimata->find(23)) {
                if ($this->mathimata->find(22)->pivot->value == 0 && $this->mathimata->find(23)->pivot->value == 0)
                    return '';
                elseif ($this->mathimata->find(22)->pivot->value == 0 && $this->mathimata->find(23)->pivot->value != 0)
                    return $this->mathimata->find(23)->pivot->value;
                elseif ($this->mathimata->find(22)->pivot->value != 0 && $this->mathimata->find(23)->pivot->value == 0)
                    return $this->mathimata->find(22)->pivot->value;
                else
                    return '[' . $this->mathimata->find(22)->pivot->value . ',' . $this->mathimata->find(23)->pivot->value . ']';
            }
        }
        if ($eidikotita == 'pe07') {
            if ($this->type == 'Γυμνάσιο' && $this->mathimata->find(24) && $this->mathimata->find(25)) {
                if ($this->mathimata->find(24)->pivot->value == 0 && $this->mathimata->find(25)->pivot->value == 0)
                    return '';
                elseif ($this->mathimata->find(24)->pivot->value == 0 && $this->mathimata->find(25)->pivot->value != 0)
                    return $this->mathimata->find(25)->pivot->value;
                elseif ($this->mathimata->find(24)->pivot->value != 0 && $this->mathimata->find(25)->pivot->value == 0)
                    return $this->mathimata->find(24)->pivot->value;
                else
                    return '[' . $this->mathimata->find(24)->pivot->value . ',' . $this->mathimata->find(25)->pivot->value . ']';
            }
        }
    }

    public function notZeroParalliliCell()
    {
        if ($this->eidikotites->first(function($key, $value){
            return (($value->pivot->value > 0) || ($value->pivot->value < 0));
        }) == '')
            return 'NULL';
        return 'NOT NULL';
    }

    public function getTypeAttribute($type)
    {
        switch ($type){
            case 'gym':
                return 'Γυμνάσιο';
                break;
            case 'lyk':
                return 'Λύκειο';
                break;
            case 'epal':
                return 'ΕΠΑΛ';
                break;
            default:
                return 'Ειδικό';
                break;
        }
    }

    public function setTypeAttribute($type)
    {
        switch($type){
            case 'Γυμνάσιο':
                $this->attributes['type'] = 'gym';
                break;
            case 'Λύκειο':
                $this->attributes['type'] = 'lyk';
                break;
            case 'ΕΠΑΛ':
                $this->attributes['type'] = 'epal';
                break;
            case 'Ειδικό':
                $this->attributes['type'] = 'eidiko';
                break;
            default:
                $this->attributes['type'] = 'eidiko';
                break;
        }
    }

    public function getGroupAttribute($group)
    {
        switch ($group){
            case 1:
                return 'Ομάδα 1';
                break;
            case 2:
                return 'Ομάδα 2';
                break;
            case 3:
                return 'Ομάδα 3';
                break;
            case 4:
                return 'Ομάδα 4';
                break;
            case 5:
                return 'Ομάδα 5';
                break;
            case 6:
                return 'Ομάδα 6';
                break;
        }
    }

    public function setGroupAttribute($group)
    {
        switch($group){
            case 'Ομάδα 1':
                $this->attributes['group'] = 1;
                break;
            case 'Ομάδα 2':
                $this->attributes['group'] = 2;
                break;
            case 'Ομάδα 3':
                $this->attributes['group'] = 3;
                break;
            case 'Ομάδα 4':
                $this->attributes['group'] = 4;
                break;
            case 'Ομάδα 5':
                $this->attributes['group'] = 5;
                break;
            case 'Ομάδα 6':
                $this->attributes['group'] = 6;
                break;
            default:
                $this->attributes['group'] = 1;
        }
    }
    //    public function setPDateAttribute($date){
//        $this->attributes['p_date'] = Carbon::createFromFormat('d/m/Y', $date);
//    }

}

