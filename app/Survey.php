<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    //
    protected $table = 'survey';

    protected $fillable = [
        'sch_id',
        'double_hours',
        'earthquake_check',
        'ergP',
        'ergP_year',
        'ergP_status',
        'ergX',
        'ergX_year',
        'ergX_status',
        'ergL',
        'ergL_year',
        'ergL_status',
        'ergG',
        'ergG_year',
        'ergG_status',
        'ergE',
        'ergE_year',
        'ergE_status',
        'ergT',
        'ergT_year',
        'ergT_status',
        'faucet',
        'fire_check',
        'studentpermeters',
        'total',
        'wc',
        'yard_check'
    ];

    public function school()
    {
        return $this->belongsTo(\App\School::class);
    }
}
