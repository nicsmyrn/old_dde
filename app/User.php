<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
        'role_id',
        'userable_id',
        'userable_type',
        'provider',
        'provider_id',
        'login_at',
        'sex',
        'second_mail'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    protected $dates = ['login_at'];
    

    public function notifications()
    {
        if ($this->role->slug == 'pysde_secretary'){
            return Notification::where('forRole', 'pysde_secretary')
                ->where('unread', 'is_true')
                ->orWhere(function($query){
                    $query->where('forRole','=','self')
                        ->where('user_id','=',$this->id);
                })
                ->get()->sortByDesc('created_at');
        }elseif($this->role->slug == 'ofa'){
            return Notification::where('forRole', 'ofa')
                ->where('unread', 'is_true')
                ->get()->sortByDesc('created_at');
        }else{
            return Notification::where(function($query){
                $query->where('forRole','=',$this->role->slug)
                    ->where('forUser', '=', $this->id);
            })
                ->where('unread', '=', 'is_true')
                ->orWhere('forRole', $this->role->slug.'s')
                ->orWhere(function($query){
                    $query->where('forRole','=','self')
                        ->where('user_id','=',$this->id);
                })
                ->get()->sortByDesc('created_at');
        }
    }


    public function readedNotifications()
    {
        if ($this->role->slug == 'pysde_secretary'){
            return Notification::where('forRole', 'pysde_secretary')
                ->where('unread', 'is_false')
                ->orWhere(function($query){
                    $query->where('forRole','=','self')
                        ->where('user_id','=',$this->id);
                })
                ->get()->sortByDesc('created_at');
        }elseif($this->role->slug == 'ofa') {
            return Notification::where('forRole', 'ofa')
                ->where('unread', 'is_false')
                ->get()->sortByDesc('created_at');
        }else{
            return Notification::where(function($query){
                $query->where('forRole','=',$this->role->slug)
                    ->where('forUser', '=', $this->id);
            })
                ->where('unread', '=', 'is_false')
                ->orWhere('forRole', $this->role->slug.'s')
                ->orWhere(function($query){
                    $query->where('forRole','=','self')
                        ->where('user_id','=',$this->id);
                })
                ->get()->sortByDesc('created_at');
        }
    }
    
    public function role(){
        return $this->belongsTo(\App\Role::class,'role_id', 'id');
    }

    public function isRole($role){
        if (is_string($role)){
            if ($this->role->slug == $role){
                return true;
            }   
            return false;
        }
        
        foreach($role as $r){
            if ($this->isRole($r->slug)){
                return true;
            }
        }
        
        return false;
    }
    
    public function userable()
    {
        return $this->morphTo();
    }
    
    
    public function getFullNameAttribute(){
        return $this->last_name .' '. $this->first_name;
    }

    public function getLoginAtAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }


}
