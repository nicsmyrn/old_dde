<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use PhpParser\Node\Scalar\MagicConst\File;
use Storage;
use Dropbox\Client as DropboxClient;
use League\Flysystem\Dropbox\DropboxAdapter;
use League\Flysystem\Filesystem;


class DropboxFilesystemServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Storage::extend('dropbox', function($app, $config){
            $client = new DropboxClient(
                $config['appKey'], $config['appSecret']
            );

            return new Filesystem(new DropboxAdapter($client));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
