<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SchoolGrantEditPermission' => [
            'App\Listeners\ChangeSchoolsPermission',
            
        ],
        \App\Events\TeacherRequestGrandDenyPermission::class => [
            \App\Listeners\ChangeTeacherRequestPermision::class
        ],        
        'auth.login' => [
            \App\Listeners\UserEventListener::class
        ],  
        \App\Events\MailsForKena::class => [
            \App\Listeners\MailToPysde::class
        ],
        \App\Events\TestEvent::class => [
            \App\Listeners\TestListener::class
        ],

        \App\Events\SchoolSaveChanges::class => [
            \App\Listeners\SchoolSendNotificationToPysde::class
        ],

        \App\Events\TeacherSendNotificationToPysde::class => [
            \App\Listeners\TeacherRequestNotificationListener::class
        ],

        \App\Events\SchoolConfirmToPysde::class => [
            \App\Listeners\SchoolSendNotificationConfirmation::class
        ],

        \App\Events\TeacherChangeProfileNotification::class => [
            \App\Listeners\TeacherNotifyPysdeForProfile::class
        ],

        \App\Events\PysdeSendNotificationToTeacher::class => [
            \App\Listeners\PysdeToTeacherNotify::class
        ],
        \App\Events\PysdeSendNotificationToSchool::class => [
            \App\Listeners\PysdeChangedKenaValues::class
        ],
        \App\Events\TeacherChangeMisthodosiaNotification::class => [
            \App\Listeners\TeacherNotifyMisthodosia::class
        ],
        \App\Events\MisthodosiaSendNotificationToTeacher::class => [
            \App\Listeners\MisthodosiaNotifyTeacherUnlock::class
        ],
        \App\Events\SchoolToOfaTeamNotification::class => [
            \App\Listeners\OfaNotifyCreation::class
        ],
        \App\Events\TeacherRequestForMoriodotisi::class => [
            \App\Listeners\TeacherNotifyPysdeForMoriodotisi::class
        ]
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //

    }
}
