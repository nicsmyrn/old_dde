<?php

namespace App\Providers;

use App\Sch_gr\SchAuth;
use App\Sch_gr\Smail;
use Illuminate\Support\ServiceProvider;

class SchServiceProvider extends ServiceProvider
{


    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Sch', function(){
            return new SchAuth();
        });

        $this->app->bind('Smail', function(){
            return new Smail();
        });
    }

}
