<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class RecaptchaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('checkcaptcha', function($attribute, $value, $parameters, $validator){

            $parameters = http_build_query([
                'secret' => config('recaptcha.secret_key'),
                'response' => $value
            ]);

            $result = json_decode(file_get_contents(config('recaptcha.url').$parameters),true);

            return $result['success'];
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
