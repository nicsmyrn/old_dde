<?php

namespace App\Providers;

use App\Models\KTEL\Busroute;
use App\Models\OFA\Student;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\School;
use Auth;
use App\Models\KTEL\Month;
use App\Models\KTEL\Year;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     * 
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
        

        $router->bind('for', function($name){
            return $this->getSchoolModelbyName($name);
        });

        $router->bind('forSchool', function($name){
           return $this->getSchoolModelbyNameOfa($name) ;
        });


        $router->bind('school', function($name){
            return $this->getSchoolModelbyName($name);
        });

        $router->bind('year', function ($year){
            return $this->getYearModel($year);
        });

        $router->bind('month', function($month){
            return $this->getMonthModel($month);
        });

        $router->bind('route', function($route){
            return  Busroute::where('id', $route)->first();
        });

        $router->bind('student', function($student){
            return Student::where('school_id', Auth::user()->userable->id)->where('am', $student)->first();
        });

    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
    
    private function getSchoolModelbyName($name)
    {
        $name = str_replace('-', ' ', $name);
        return School::with(['eidikotites'=> function($query){
            $query->orderBy('order');
        },'mathimata','eidikis'])
            ->where('name', $name)

            ->first();
    }


    private function getSchoolModelbyNameOfa($name)
    {
        $name = str_replace('-', ' ', $name);
        return School::where('name', $name)
            ->first();
    }
    private function getMonthModel($month)
    {
        return Month::where('name', $month)->first();
    }

    private function getYearModel($year)
    {
        return Year::where('name', $year)->first();
    }
}

