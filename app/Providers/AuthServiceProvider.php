<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Permission;
use App\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \App\School::class => \App\Policies\SchoolPolicy::class    
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

        foreach($this->getPermissions() as $permission){
            $gate->define($permission->slug, function($user) use($permission){
                return $user->isRole($permission->roles);
            });
        }
        
        // $gate->define('update', function($user, $school){
        //     return $school->id == $user->sch_id; 
        // });
        
        $gate->define('edit_kena', function($user, $school){
             return $user->sch_id == $school->id;
        });

        // $gate->define('edit-school-kena', function($user, $school){
        //     return $user->sch_id == $school->id;
        // });
    }

    protected function getPermissions()
    {
        return Permission::with('roles')->get();
    }
}
