<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('partials.nav.forPysde', 'App\Http\Composers\NavigationComposer@compose');
        view()->composer('partials.nav2.forPysde', 'App\Http\Composers\NavigationComposer@compose');
        view()->composer('partials.nav.forAdmin', 'App\Http\Composers\NavigationComposer@compose');
        view()->composer('partials.nav2.forAdmin', 'App\Http\Composers\NavigationComposer@compose');
        view()->composer('partials.nav.forMisthodosia', 'App\Http\Composers\NavigationMisthodosiaComposer@compose');
        view()->composer('partials.nav2.forMisthodosia', 'App\Http\Composers\NavigationMisthodosiaComposer@compose');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
