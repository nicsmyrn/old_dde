<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class F extends Model
{
    protected $table = 'f';

    protected $fillable = [
        'name',
        'description'
    ];

    public $timestamps = false;

    public function protocols()
    {
        return $this->hasMany('App\Protocol','f_id', 'id');
    }

    public function setFListAttribute()
    {
        return "$this->name - $this->description";
    }

}
