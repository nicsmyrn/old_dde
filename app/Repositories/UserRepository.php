<?php

namespace App\Repositories;

use App\Eidikotita;
use App\Http\Requests\UserRequest;
use App\Monimos;
use App\MySchoolTeacher;
use App\School;
use App\Teacher;
use App\User;
use Illuminate\Http\Request;


class UserRepository{

    private $roles = array(
        'school'                => 1,
        'pysde_secretary'       => 2,
        'pysde_admin'           => 5,
        'manager'               => 4,
        'ekpaideutikon_thematon'=> 7,
        'council'               => 3,
        'teacher'               => 6, // some teachers,
        'misthodosia'           => 11
    );

    protected $userable_type = '';

    protected $pysde_secretary = array(
        'vikizouridaki@gmail.com'
    );

    protected $pysde_admin = array(

        ''
    );

    protected $ekpaideutikon_thematon = array(
        'papagrigorakism@gmail.com'
    );

    protected $manager = array(
        ''
    );

    protected $council = array(
        ''
    );

    protected $teacher = array(
        ''
    );

    public function SCH_GRfindByUsernameOrCreate($userData, $provider)
    {
//        dd($userData);

        if(is_array($userData['businesscategory'])){
            if(in_array('ΕΚΠΑΙΔΕΥΤΙΚΗ ΜΟΝΑΔΑ - ΕΜ', $userData['businesscategory'])) {
                $role_id = $this->roles['school'];
                $this->userable_type = 'App\School';
            }elseif(in_array('ΔΙΟΙΚΗΤΙΚΗ ΜΟΝΑΔΑ - ΔΜ', $userData['businesscategory'])){
                $role_id = $this->roles['misthodosia'];
                $this->userable_type = 'App\Misthodosia';
            }else {
                $role_id = $this->roles['teacher'];
                $this->userable_type = 'App\Teacher';
            }
        }else{
            if($userData['businesscategory'] == 'ΕΚΠΑΙΔΕΥΤΙΚΗ ΜΟΝΑΔΑ - ΕΜ') {
                $role_id = $this->roles['school'];
                $this->userable_type = 'App\School';
            }elseif($userData['businesscategory'] == 'ΔΙΟΙΚΗΤΙΚΗ ΜΟΝΑΔΑ - ΔΜ'){
                $role_id = $this->roles['misthodosia'];
                $this->userable_type = 'App\Misthodosia';
            }else{
                $role_id = $this->roles['teacher'];
                $this->userable_type = 'App\Teacher';
            }
        }

        $mySchool = User::where('second_mail', $userData['mail'])->first(['email']);

        if($mySchool != null){
            $user = User::where('email', $mySchool->email)->where('provider_id', base64_encode($mySchool->email))->first();
        }else{
            $user = User::where('email', $userData['mail'])->orWhere('second_mail', $userData['mail'])->where('provider_id', base64_encode($userData['mail']))->first();
        }

        $cn = explode(" ",$userData['cn'],2);

        if ($user === null){
            if($this->userable_type == 'App\School'){
                return $this->createSchUserSchool($userData, $provider, $cn, $role_id);
            }elseif($this->userable_type == 'App\Teacher'){
                return $this->createSchUserTeacher($userData, $provider, $cn, $role_id);
            }elseif($this->userable_type == 'App\Misthodosia'){
                return $this->createSchUserMisthodosia($userData, $provider, $cn, $role_id);
            }
        }else{
            if($this->userable_type == 'App\School'){                                   // CHANGED 27 - 06 - 2017
                return $user;
            }elseif($this->userable_type == 'App\Teacher'){
                return $this->createSchUserTeacher($userData, $provider, $cn, $role_id);
            }elseif($this->userable_type == 'App\Misthodosia'){
                return $user;
            }
        }
    }

    public function findByUsernameOrCreate($userData, $provider)
    {
        if (in_array($userData->email, $this->pysde_secretary)){
            $role_id = $this->roles['pysde_secretary'];
        }elseif(in_array($userData->email, $this->pysde_admin)){
            $role_id = $this->roles['pysde_admin'];
        }elseif(in_array($userData->email, $this->manager)){
            $role_id = $this->roles['manager'];
        }elseif(in_array($userData->email, $this->ekpaideutikon_thematon)){
            $role_id = $this->roles['ekpaideutikon_thematon'];
        }elseif(in_array($userData->email, $this->council)){
            $role_id = $this->roles['council'];
        }else{
            $role_id = $this->roles['teacher'];
            $this->userable_type = 'App\Teacher';
        }


        $user = User::where('email', $userData->email)->first();

        if ($user === null){
            return User::create([
                'first_name'    => $userData->user['name']['givenName'],
                'last_name'     => $userData->user['name']['familyName'],
                'email'         => $userData->email,
                'userable_type' => $this->userable_type,
                'role_id'       => $role_id,
                'provider'      => $provider,
                'provider_id'   => $userData->id
            ]);
        }else{
            return $user;
        }

    }

    public function createUser(UserRequest $request)
    {
        User::create([
            'first_name'=> $request->get('first_name'),
            'last_name'=> $request->get('last_name'),
            'email'=> $request->get('email'),
            'password'=> bcrypt($request->get('password')),
            'role_id'=> 6,      // teacher ONLY
            'userable_id' => 0,
            'userable_type' => 'App\Teacher'
        ]);
    }

    private function createTeacherSchAccountProfile()
    {

    }

    /**
     * @param $userData
     * @param $provider
     * @param $cn
     * @param $role_id
     * @return static
     */
    private function createSchUserSchool($userData, $provider, $cn, $role_id)
    {
        $user = User::create([
            'first_name' => $cn[1],
            'last_name' => $cn[0],
            'email' => $userData['mail'],
            'userable_type' => $this->userable_type,
            'role_id' => $role_id,
            'provider' => $provider,
            'provider_id' => base64_encode($userData['mail'])
        ]);
        return $user;
    }

    private function createSchUserTeacher($userData, $provider, $cn, $role_id)
    {
        $monimos_exists = Monimos::where('am', $userData['employeenumber'])->first();
        $myschoolProfile = MySchoolTeacher::where('am', $userData['employeenumber'])->first();

        if($myschoolProfile == null){
            return 'other';
        }

        if ($monimos_exists == null){

            $organikiId = $myschoolProfile->organiki_id;

            $newMonimos = Monimos::create([
                'am'        => $myschoolProfile->am,
                'organiki'  => $organikiId,
                'county'    => 49,              // TEMPORARY αποσπασμένοι NOT INCLUDED
                'orario'    => $myschoolProfile->ypoxreotiko,
                'dieuthinsi'=> '5001a',
                'moria'     => $myschoolProfile->edata != null ? $myschoolProfile->edata->sum_moria : 0
            ]);

            $newTeacher = new Teacher();
            $newTeacher->middle_name = $myschoolProfile->middle_name;
            $newTeacher->klados_id = Eidikotita::where('slug_name', $myschoolProfile->eidikotita)->first()->id;
            $newTeacher->mobile = $myschoolProfile->mobile;

            $newTeacher->address = '';
            $newTeacher->city = '';
            $newTeacher->tk = '73100';
            $newTeacher->activation_for_register = true;
            $newTeacher->is_checked = true;
            $newTeacher->sex = $myschoolProfile->sex;
            $newTeacher->request_to_change = 0;
            $newTeacher->afm = $myschoolProfile->afm;

            if($myschoolProfile->edata != null){
                $newTeacher->dimos_sinipiretisis = $myschoolProfile->edata->dimos_sinipiretisis;
                $newTeacher->dimos_entopiotitas = $myschoolProfile->edata->dimos_entopiotitas;
                $newTeacher->special_situation = $myschoolProfile->edata->special_situation;
                $newTeacher->family_situation = $myschoolProfile->edata->moria_family_situation > 0 ? 1 : 0;
                $newTeacher->childs = $myschoolProfile->edata->sum_tekna;
            }else{
                $newTeacher->special_situation = 0;
                $newTeacher->dimos_sinipiretisis = 0;
                $newTeacher->dimos_entopiotitas = 0;
                $newTeacher->family_situation = 0;
                $newTeacher->childs = 0;
            }

            $newMonimos->teacher()->save($newTeacher);

            $newUser = new User();
            $newUser->first_name = $myschoolProfile->first_name;
            $newUser->last_name = $myschoolProfile->last_name;
            $newUser->email = $userData['mail'];
            $newUser->role_id = $role_id;
            $newUser->provider = $provider;
            $newUser->sex = $myschoolProfile->sex;
            $newUser->provider_id = base64_encode($userData['mail']);

            $newTeacher->user()->save($newUser);

            return $newUser;

        }else{
            \DB::beginTransaction();
                $second_mail = $monimos_exists->teacher->user->email;

                $monimos_exists->update([
                    'organiki'      => $myschoolProfile->organiki_id,
                    'orario'        => $myschoolProfile->ypoxreotiko,
                ]);

                $monimos_exists->teacher->update([
                    'middle_name'       => $myschoolProfile->middle_name,
                    'klados_id'         => $myschoolProfile->klados_id,
                    'afm'               => $myschoolProfile->afm,
                    'is_checked'        => true
                ]);

                if($myschoolProfile->edata != null) {
                    $monimos_exists->update([
                        'moria'     => $myschoolProfile->edata->sum_moria
                    ]);
                    $monimos_exists->teacher->update([
                        'special_situation'        => $myschoolProfile->edata->special_disease,
                        'dimos_entopiotitas'        => $myschoolProfile->edata->dimos_entopiotitas,
                        'dimos_sinipiretisis'        => $myschoolProfile->edata->dimos_sinipiretisis,
                        'activation_for_register'   => true
                    ]);
                }

                $monimos_exists->teacher->user->update([
                    'first_name'        => $myschoolProfile->first_name,
                    'last_name'         => $myschoolProfile->last_name,
                    'email'             => $userData['mail'],
                    'provider'          => $provider,
                    'provider_id'       => base64_encode($userData['mail']),
                    'second_mail'       => $second_mail
                ]);
            \DB::commit();
            return $monimos_exists->teacher->user;
        }
    }

    private function createSchUserMisthodosia($userData, $provider, $cn, $role_id)
    {
            $newUser = new User();
            $newUser->first_name = 'Μισθοδοσία';
            $newUser->last_name = 'ΔΔΕ Χανίων';
            $newUser->email = $userData['mail'];
            $newUser->role_id = $role_id;
            $newUser->provider = $provider;
            $newUser->provider_id = base64_encode($userData['mail']);

            return $newUser;

    }
}