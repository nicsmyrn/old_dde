<?php

namespace App\Repositories\Pysde;

use App\MySchoolTeacher;
use App\NewEidikotita;
use App\School;
use App\Year;
use Illuminate\Http\Request;
use App\Eidikotita;
use App\Praxi;
use App\Placement;
use DB;
use Carbon\Carbon;
use App\Teacher;
use Illuminate\Support\Facades\Config;


class PlacementRepository{

    public function allEidikotitesUnited()
    {
        return   Eidikotita::groupBy('united')
            ->with(['schools'=> function($query){
                $query->where('value', '<>', 0);
            }])
            ->where('sch_type', 'all')
            ->orderBy('order')
            ->get(['name', 'united', 'id']);
    }

    public function allNewKladoi()
    {
        return NewEidikotita::groupBy('klados_slug')
            ->orderBy('id')
            ->get(['klados_name', 'klados_slug', 'id']);
    }
    
    public function notZeroEidikotites()
    {
        $eidikotites = Eidikotita::with(['schools'=> function($query){
            $query->where('value', '<>', 0);
        }])->get()
            ->filter(function($item){
    
            if (count($item['schools']) !=0 ){
                return true;
            }
        })->lists('full_name', 'id');      
        
        return $eidikotites;
    }

    public function initializeDataCreate(Request $request, $teacher_id = null)
    {
        if ($teacher_id != null){
            $teacher = MySchoolTeacher::find($teacher_id);
        }else{
            $teacher = MySchoolTeacher::find($request->get('teachers'));
        }

        $data = array(
            'klados' => Eidikotita::find($request->get('klados_id'))->slug_name,
            'praxi_id'  => $request->get('praxi_id'),
            'teacher_name'  => $teacher->last_name . ' ' . $teacher->first_name,
            'hours'         => $request->get('hours'),
            'from'          => School::find($request->get('from_id')) != null? School::find($request->get('from_id'))->name : \Config::get('requests.teacher_types')[$request->get('from_id')],
            'to'            => School::find($request->get('to_id')) != null ? School::find($request->get('to_id'))->name : \Config::get('requests.special_placements')[$request->get('to_id')],
            'description'   => $request->get('description'),
            'from_id'       => $request->get('from_id'),
            'to_id'         => School::find($request->get('to_id')) != null ? School::find($request->get('to_id'))->id : $request->get('to_id'),
            'klados_id'     => Eidikotita::find($request->get('klados_id'))->id,
            'afm'               => $teacher->afm,
            'teacherable_id'    => 0,
            'teacherable_type'  => 'App\Myschool',
            'placements_type'   => $request->get('placements_type'),
            'days'              => $request->get('days'),
            'me_aitisi'         => $request->has('me_aitisi') ? true : false
        );

        return $data;
    }

    public function storeNewPlacement(Request $request)
    {
        $decision_date = $request->get('decision_date');
        $decision_number = $request->get('decision_number');
        $placements = $request->get('placements');
        $schools = $request->get('schools');


        DB::beginTransaction();
        $year_id = Year::where('current', true)->first()->id;

        $p = Praxi::where('decision_number', $decision_number)
            ->where('year_id', $year_id)
            ->first();

        if ($p == null){
            $praxi = Praxi::create([
                'decision_number' => $decision_number,
                'decision_date' => Carbon::createFromFormat('d/m/Y', $decision_date),
                'praxi_type'    => 11,
                'year_id'       => $year_id
            ]);
            $praxi_id = $praxi->id;
        }else{
            $praxi_id = $p->id;
        }

        foreach ($placements as $teacher){
            foreach($teacher['placements'] as $placement) {
                Placement::create([
                    'klados' => $teacher['klados'],
                    'praxi_id' => $praxi_id,
                    'teacher_name' => $teacher['full_name'],  //$placement['name'],
                    'hours' => $placement['hours'],
                    'from' => $teacher['organiki'],
                    'to' => $placement['to']['name'],
                    'description' => $placement['description'],
//                        'teacher_id' => $teacher['id'],
                    'from_id' => $teacher['organiki_id'],
                    'to_id' => $placement['to']['id'],
                    'klados_id' => $teacher['klados_id'],
                    'teacherable_id' => $teacher['id'],
                    'teacherable_type' => $teacher['teacherable_type'],
                    'placements_type'   => $placement['type'],
                    'days'  => $placement['days'],
                    'me_aitisi' => $placement['aitisi'],
                    'afm'   => $teacher['afm']
                ]);
            }
        }

//            foreach ($schools as $school){
//                $s = School::find($school['id']);
//                $s->eidikotites->find($eid_id)->pivot->value = $school['pivot']['value'];
//                $s->eidikotites->find($eid_id)->pivot->save();
//            }
        DB::commit();

        return 'true';
    }
    
    public function storePlacement(Request $request)
    {
        $decision_date = $request->get('decision_date');
        $decision_number = $request->get('decision_number');
        $placements = $request->get('placements');
        $eid_id = $request->get('eid_id');
        $schools = $request->get('schools');


        DB::beginTransaction();
            $year_id = Year::where('current', true)->first()->id;

            $p = Praxi::where('decision_number', $decision_number)
                ->where('year_id', $year_id)
                ->first();

            if ($p == null){
                $praxi = Praxi::create([
                    'decision_number' => $decision_number,
                    'decision_date' => Carbon::createFromFormat('d/m/Y', $decision_date),
                    'praxi_type'    => 11,
                    'year_id'       => $year_id
                ]);
                $praxi_id = $praxi->id;
            }else{
                $praxi_id = $p->id;
            }

            foreach ($placements as $teacher){
                foreach($teacher['placements'] as $placement) {
                    Placement::create([
                        'klados' => $teacher['klados'],
                        'praxi_id' => $praxi_id,
                        'teacher_name' => $teacher['full_name'],  //$placement['name'],
                        'hours' => $placement['hours'],
                        'from' => $teacher['organiki'],
                        'to' => $placement['to']['name'],
                        'description' => $placement['description'],
//                        'teacher_id' => $teacher['id'],
                        'from_id' => $teacher['organiki_id'],
                        'to_id' => $placement['to']['id'],
                        'klados_id' => $eid_id,
                        'teacherable_id' => $teacher['id'],
                        'teacherable_type' => $teacher['teacherable_type'],
                        'placements_type'   => $placement['type'],
                        'days'  => $placement['days'],
                        'me_aitisi' => $placement['aitisi'],
                        'afm'   => $teacher['afm']
                    ]);
                }
            }

//            foreach ($schools as $school){
//                $s = School::find($school['id']);
//                $s->eidikotites->find($eid_id)->pivot->value = $school['pivot']['value'];
//                $s->eidikotites->find($eid_id)->pivot->save();
//            }
        DB::commit();

        return 'true';
    }
    
    public function jsonNotZeroSchools($eidikotita)
    {
        $eidikotita = Eidikotita::with(['schools'=> function ($query){
            $query->where('value', '<>', 0);
        }])->find($eidikotita);
        
        return $eidikotita->schools;  
    }
    
}