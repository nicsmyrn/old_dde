<?php

namespace App\Repositories\OFA;


use App\Http\Controllers\Controller;
use App\Models\OFA\Student;

class StudentRepository{
    private $school_id;

    private $headersCounter;
    private $updatedStudent = 0;
    private $untouchedStudent = 0;
    private $newStudent = 0;
    private $students = array();
    private $class_name = '';
    private $header = [
        'Α/Α'               => 'id',
        'Αριθμός μητρώου'   => 'am',
        'Επώνυμο μαθητή'    => 'last_name',
        'Όνομα μαθητή'      => 'first_name',
        'Όνομα πατέρα'      => 'middle_name',
        'Όνομα μητέρας'     => 'mothers_name',
        'Ημ/νία γέννησης'   => 'year_birth',
        'Φύλο μαθητή'       => 'sex'
    ];

    protected $path;

    public function __construct()
    {
        if(\Auth::check()){
            $this->school_id = \Auth::user()->userable->id;
            $this->path = storage_path('app/schools/'.str_slug(\Auth::user()->userable->name).'/myschool.csv');
        }else{
            dd('Error: Παρακαλώ συνδεθείτε...');
        }
    }

    public function execute()
    {
        $students = array();
        $columns_names = array();

        $counter = 0;

        if ( ($handle = fopen($this->path,'r')) !== FALSE) {
            $temp = 1;
            while (($data = fgetcsv($handle, 0, ';')) !== FALSE) {     // FOR EACH TEACHER

                $students = $this->getStudent($counter, $data, $students, $columns_names);

                if($this->convert($data[0]) == "Τάξη Εγγραφής: "){
                    $counter = 1;

                    $this->findClassName($data);

                    $columns_names = [];
                    $columns_names = $this->getHeaders($handle, $columns_names);
                }
                $temp += 1;
            }
            \DB::beginTransaction();
            foreach($this->students as $student){
                $modelStudent = Student::where('school_id', $this->school_id)
                    ->where('am', $student['am'])
                    ->first();
                if($modelStudent == null){
                    $this->createStudent($student, $this->school_id);
                }else{
                    $this->updateOrNothing($modelStudent, $student, $this->school_id);
                }
            }
            \DB::commit();

            \Log::alert("\n Updated: $this->updatedStudent - Created: $this->newStudent - Untouched: $this->untouchedStudent\n");
            flash()->overlayS('', "Ενημερώθηκαν: $this->updatedStudent - Δημιουργήθηκαν: $this->newStudent - Καμία ενέργεια σε: $this->untouchedStudent μαθητές");

        }else{
            flash()->error('Error');

            \Log::alert('Error Opening File');
        }

    }



    /**
     * @param $handle
     * @param $columns_names
     * @return array
     */
    private function getHeaders($handle, $columns_names)
    {
        $headers = fgetcsv($handle, 0, ';');
        foreach ($headers as $column) {
            $columns_names[] = $this->convert($column);
        }
        return $columns_names;
    }

    private function getNumberOfTeachers()
    {
        $fp = file(database_path($this->path));
        return  count($fp) - 1;
    }

    /**
     * @param $t
     * @param $teacher
     */
    private function updateOrNothing($modelStudent, $student, $school_id)
    {
        $updateColumns = [];

        if ($modelStudent->am != $student['am']) {
            $updateColumns['am'] = $student['am'];
        }
        if ($modelStudent->last_name != $student['last_name']) {
            $updateColumns['last_name'] = $student['last_name'];
        }
        if ($modelStudent->first_name != $student['first_name']) {
            $updateColumns['first_name'] = $student['first_name'];
        }
        if ($modelStudent->middle_name != $student['middle_name']) {
            $updateColumns['middle_name'] = $student['middle_name'];
        }
        if ($modelStudent->mothers_name != $student['mothers_name']) {
            $updateColumns['mothers_name'] = $student['mothers_name'];
        }

        $year = $this->getYear($student['year_birth']);
        if ($modelStudent->year_birth != $year) {
            $updateColumns['year_birth'] = $year;
        }

        $class = $this->getClassName($student['class']);
        if ($modelStudent->class != $class) {
            $updateColumns['class'] = $class;
        }

        $sex = $this->getSex($student['sex']);
        if ($modelStudent->sex != $sex) {
            $updateColumns['sex'] = $sex;
        }

        if (count($updateColumns) != 0) {

            \DB::table('ofa_students')
                ->where('school_id', $school_id)
                ->where('am', $student['am'])
                ->update($updateColumns);

            $this->updatedStudent++;
        } else $this->untouchedStudent++;
    }

    /**
     * @param $teacher
     */
    private function createStudent($student, $school_id)
    {
        Student::create([
            'am' => $student['am'],
            'last_name' => $student['last_name'],
            'first_name' => $student['first_name'],
            'middle_name' => $student['middle_name'],
            'mothers_name' => $student['mothers_name'],
            'class' => $this->getClassName($student['class']),
            'sex' => $this->getSex($student['sex']),
            'school_id' => $school_id,
            'year_birth' => $this->getYear($student['year_birth'])
        ]);

        $this->newStudent++;
    }

    /**
     * @param $counter
     * @param $data
     * @param $students
     * @param $columns_names
     * @return array
     */
    private function getStudent($counter, $data, $students, $columns_names)
    {
        if ($counter > 0 && $this->convert($data[0]) != "Τάξη Εγγραφής: ") {

            $students[] = $data;

            $record = array();
            $i = 0;
            $this->headersCounter = 0;

            foreach ($columns_names as $key) {
                if(array_key_exists($key, $this->header)){
                    $record[$this->header[$key]] = $this->convert($data[$i++]);
                    $this->headersCounter++;
                }else{
                    $i++;
                }
            }

            $record['class'] = $this->class_name;

            if($this->headersCounter == count($this->header)){
                $this->students[] = $record;
            }else{
                dd('Σφάλμα 342.4938 : δεν έχετε προσθέσει στην αναφορά τα απαραίτητα πεδία');
            }

            $counter += 1;
            return $students;
        }
        return $students;
    }

    private function convert($text)
    {
        return iconv("greek", "UTF-8", $text);
    }

    private function getClassName($class)
    {
        $value = array_search($class, \Config::get('requests.class'));
        if($value == false){
            return 1;
        }else{
            return $value;
        }
    }

    private function getSex($sex)
    {
        return $sex == 'Θ' ? 0 : 1;
    }

    private function getYear($year)
    {
        return substr($year, -4);
    }

    /**
     * @param $data
     */
    private function findClassName($data)
    {
        $i = 1;
        $found = false;
        while ($i <= (count($data) - 1) && !$found) {
            if ($this->convert($data[$i] != '')) {
                $this->class_name = $this->convert($data[$i]);
                $found = true;
            }
            $i++;
        }
        if (!$found) $this->class_name = 'ΣΤ';
    }
}