<?php

namespace App\Repositories\School;

use App\School;
use App\Http\Requests\SchoolRequest;


class SchoolRepository{

    public function eidikotitesNotZero(School $school)
    {
        return $school->eidikotites->filter(function($school){
            if($school->sch_type != 'parallili'){
                if ($school->pivot->value != 0){
                    return true;
                }
            }
        });
    }

    public function paralliliNotZero(School $school)
    {
        return $school->eidikotites->filter(function($school){
            if($school->sch_type == 'parallili'){
                if ($school->pivot->value != 0){
                    return true;
                }
            }
        });
    }

    public function mathimataNotZero(School $school)
    {
        return $school->mathimata->filter(function($school){
            if ($school->pivot->value != 0){
                return true;
            }
        });
    }

    public function eidikisNotZero(School $school)
    {
        return $school->eidikis->filter(function($school){
            if ($school->pivot->value != 0){
                return true;
            }
        });
    }
    
    public function saveValueOfEidikotita(SchoolRequest $request, School $school)
    {
        $eidikotita_input = $request->get('eidikot');
        $eidiki_input = $request->get('eidikis');
        $enopiimenoPE04 = $request->get('eidikotitaPE04');

        if($school->type == 'Γυμνάσιο'){
            if ($enopiimenoPE04 > 0) {
                $enopiimenoPE04 = 0;
            }

            foreach($school->eidikotites->where('united', 'pe04') as $pe04){
                if($eidikotita_input[$pe04->id] < 0){
                    $eidikotita_input[$pe04->id] = 0;
                }
                if ($enopiimenoPE04 < 0){
                    $eidikotita_input[$pe04->id] = 0;
                    if($pe04->slug == 'pe0401'){
                        $eidikotita_input[$pe04->id] = $enopiimenoPE04;
                    }
                }

            }
        }


        $changed = false;

        $changed = $this->saveValueOfMathimata($request, $school);

        foreach($school->eidikotites as $eidikotita){
            if($eidikotita->pivot->value != $eidikotita_input[$eidikotita->id]){
                if($eidikotita->sch_type == 'parallili'){
                    $eidikotita->pivot->value =  $eidikotita_input[$eidikotita->id] > 0 ? $eidikotita_input[$eidikotita->id]*(-1): $eidikotita_input[$eidikotita->id] ;
                }else{
                    if ($eidikotita->plus == 'extra' && $school->type == 'Γυμνάσιο'){
                        $eidikotita->pivot->value =  $eidikotita_input[$eidikotita->id] > 0 ? $eidikotita_input[$eidikotita->id] : $eidikotita_input[$eidikotita->id]*(-1) ;
                    }else{
                        $eidikotita->pivot->value =  $eidikotita_input[$eidikotita->id] ;
                    }
                }

                $eidikotita->pivot->last_user_login_id = \Auth::id();
                $eidikotita->pivot->save();   
                $changed = true;
            }
        }

        foreach($school->eidikis as $eidikotita_eidikis){
            if($eidikotita_eidikis->pivot->value != $eidiki_input[$eidikotita_eidikis->id]){

                $eidikotita_eidikis->pivot->value =  $eidiki_input[$eidikotita_eidikis->id] < 0 ? $eidiki_input[$eidikotita_eidikis->id] : $eidiki_input[$eidikotita_eidikis->id]*(-1) ;

                $eidikotita_eidikis->pivot->last_user_login_id = \Auth::id();
                $eidikotita_eidikis->pivot->save();

                $changed = true;
            }
        }

        if ($school->description != $request->get('description')){
            $school->description = $request->get('description');
            $changed = true;
            $school->save();
        }
        
        if ($changed){
            flash()->overlayS('Συγχαρητήρια', 'Ο πίνακας ενημερώθηκε με επιτυχία. Έχει σταλεί E-mail επιβεβαίωσης στο Σχολείο');
        }else{
            flash()->info('Προσοχή!', 'Δεν έγινε καμία αλλαγή στον πίνακα');
        }        
    }

    private function saveValueOfMathimata(SchoolRequest $request, School $school)
    {
        $mathimata_input = $request->get('lesson');
        $changed = false;

        foreach($school->mathimata as $mathimata){
            if($mathimata->pivot->value != $mathimata_input[$mathimata->id]){
                if (in_array($mathimata->name, ['english+','german+', 'french+'])){
                    $mathimata->pivot->value = $mathimata_input[$mathimata->id] < 0 ? $mathimata_input[$mathimata->id] *(-1): $mathimata_input[$mathimata->id] ;
                }else{
                    $mathimata->pivot->value = $mathimata_input[$mathimata->id] > 0 ? $mathimata_input[$mathimata->id] *(-1): $mathimata_input[$mathimata->id] ;
                }
                $mathimata->pivot->last_user_login_id = \Auth::id();
                $mathimata->pivot->save();
                $changed = true;
            }
        }

        return $changed;
    }
}
