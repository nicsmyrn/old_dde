<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeacherRights extends Model
{
    protected $table = 'teacher_rights';

    protected $primaryKey = 'afm';

    public $timestamps = false;

    protected $fillable = [
        'year',
        'afm',
        'onomastika_yperarithmos',
        'dikaiwma_veltiwsis'
    ];

    public function myschool()
    {
        return $this->belongsTo('App\MySchoolTeacher', 'afm', 'afm');
    }


}
