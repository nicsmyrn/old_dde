<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Misthodosia extends Model
{
    protected $table = 'misthodosia';

    protected $fillable = [
        'at',
        'birth',
        'iban',
        'bank',
        'amka',
        'doy',
        'am_tsmede',
        'am_ika',
        'new',
        'mk',
        'afm',
        'family_situation',
        'next_mk'
    ];

    protected $dates = ['birth', 'next_mk'];

    public function tekna()
    {
        return $this->hasMany('App\Tekna', 'misthodosia_id', 'id');
    }

    public function myschool()
    {
        return $this->belongsTo(MySchoolTeacher::class, 'afm', 'afm');
    }

    public function setBirthAttribute($date)
    {
        $this->attributes['birth'] = $date ?  Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getBirthAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }
    public function setNextMkAttribute($date)
    {
        $this->attributes['next_mk'] = $date ?  Carbon::createFromFormat('d/m/Y', $date) : null;
    }

    public function getNextMkAttribute($date)
    {
        return Carbon::parse($date)->format('d/m/Y');
    }
}
