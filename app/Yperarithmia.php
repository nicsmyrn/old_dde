<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Yperarithmia extends Model
{
    protected $table = 'yperarithmies';

    protected $fillable = [
        'school_id',
        'eidikotita_id',
        'number',
        'description'
    ];
}
