<?php

namespace App\Http\Controllers\traits;

use App\Eidikotita;
use App\MySchoolTeacher;
use App\Year;
use Auth;
use App\Teacher;
use App\Praxi;
use App\Placement;
use Carbon\Carbon;
use App\School;

trait PlacementsTrait
{
    public function getIndexPlacementsOfTeacher($teacher_id = null)
    {
//        $teacher = Teacher::find($this->checkUser($teacher_id));

        $myschool = MySchoolTeacher::findOrFail($this->checkUser($teacher_id));

        $placements = Placement::withTrashed()
            ->with('praxi')
            ->where('afm', $myschool->afm)
            ->get();

        $old_placements = $placements->where('praxi.current_year', 0);
        $current_placements = $placements->where('praxi.current_year', 1);

//        $placements = Placement::withTrashed()
//            ->with('praxi')
//            ->where('teacherable_id', $teacher->id)
//            ->where('teacherable_type', 'App\Teacher')
//            ->get();

//        $total = Placement::where('teacherable_id', $teacher->id)
//            ->where('teacherable_type', 'App\Teacher')
//            ->sum('hours');

        $total = Placement::with(['praxi'=> function($q){
            $q->where('year_id', Year::where('current', true)->first()->id);
        }])->where('afm', $myschool->afm)
            ->get()
            ->filter(function($p){
                if($p->praxi != null) return true;
            })
            ->sum('hours');

//        $total = Placement::where('afm', $myschool->afm)
//            ->sum('hours');


        $base_placement = Placement::whereIn('placements_type', [1,6])     //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
        ->where('afm', $myschool->afm)
            ->orderBy('created_at','desc')          // ΑΥΤΟ ΕΔΩ ΠΡΕΠΕΙ ΝΑ ΤΟ ΞΑΝΑΔΩ ....<<<<<<<<<<<<<<<<<<<<<<<<
            ->first();
//
        $schools = School::lists('name', 'id');
        $kladoi = Eidikotita::lists('name', 'id');

        $current_year = Year::where('current', true)->first()->name;

        return view('teacher.placements.index', compact('old_placements', 'current_placements', 'total', 'base_placement', 'myschool', 'schools','kladoi', 'current_year'));
    }

    public function allPlacementsPDF($teacher_id = null, $year_list)
    {
//        $teacher_id = $this->checkUser($teacher_id);
//        $teacher = Teacher::find($teacher_id);
//        $pr = Praxi::with(['placements' => function($query) use($teacher_id){
//            $query->where('teacherable_id', $teacher_id)
//                ->where('teacherable_type', 'App\Teacher')
////                ->whereNotIn('placements_type', [1,6]) //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
//                ->withTrashed();
//        }])->whereIn('praxi_type', [11,12])->get();

        $myschool = MySchoolTeacher::find($this->checkUser($teacher_id));

        $pr = Praxi::with(['placements' => function($query) use ($myschool){
            $query->where('afm', $myschool->afm)
                //                ->whereNotIn('placements_type', [1,6]) //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
                ->withTrashed();
        }])->whereIn('praxi_type', [11,12])
            ->whereIn('year_id', $year_list)
            ->get();

        $temp = array();

        foreach($pr as $praxi){
            if (!$praxi->placements->isEmpty()){
                // ************** START TEST  *****************
                $last_BASE_placement = Placement::with(['praxi' => function($q) use ($praxi, $year_list){
                    $q->where('id', '<=', $praxi->id)
                        ->whereIn('year_id', $year_list);   // *******<<<<<<<<<<<<<<  $praxi->decision_number
                }])
                    ->whereIn('placements_type', [1,6])     //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
//                    ->where('teacherable_id', $teacher_id)
                    ->where('afm', $myschool->afm)
                    ->withTrashed()
                    ->orderBy('created_at','desc')          // ΑΥΤΟ ΕΔΩ ΠΡΕΠΕΙ ΝΑ ΤΟ ΞΑΝΑΔΩ ....<<<<<<<<<<<<<<<<<<<<<<<<
                    ->get();

                $new_placements = Placement::with(['praxi' => function($q) use ($praxi, $year_list){
                    $q->where('decision_number', $praxi->decision_number)->whereIn('year_id', $year_list);                      // ****************CHANGES from id to decision_numbers green words
                }])
                    ->whereNotIn('placements_type', [1,6])
//                    ->where('teacherable_id', $teacher_id)
                    ->where('afm', $myschool->afm)
                    ->withTrashed()
                    ->get();

                $temp0 = collect();
                $temp_new_placements = array();

                foreach($last_BASE_placement as $base){
                    if($base->praxi != null){
                        $temp0->push($base);
                    }
                }

//                if($praxi->id == 70){

                    $max = $temp0->max('praxi_id');
                    $praxi->last_BASE_placement = $temp0->where('praxi_id', $max)->first();

//                    dd($praxi->last_BASE_placement);
//                }



                foreach($new_placements as $new_placement){
                    if($new_placement->praxi != null){
                        if($praxi->last_BASE_placement != null){
                            if($new_placement->id != $praxi->last_BASE_placement->id){
                                $temp_new_placements[] = $new_placement;
                            }
                        }else{
                            $temp_new_placements[] = $new_placement;
                        }
                    }
                }
                $praxi->new_placements = collect($temp_new_placements);


                $old_placements = Placement::with(['praxi' => function($q) use($praxi, $year_list){
                    $q->whereIn('year_id', $year_list)->where('decision_date', '<', Carbon::parse($praxi->decision_date)->format('Y-m-d'));
//                }])->where('teacherable_id', $teacher_id)
                }])->where('afm', $myschool->afm)
                    ->whereNotIn('placements_type', [1,6]) //ΠΡΟΣΩΡΙΝΕΣ ΤΟΠΟΘΕΤΗΣΕΙΣ
                    ->get();
                $temp2 = array();
                foreach($old_placements as $old){
                    if ($old->praxi != null){
                        $temp2[] = $old;
                    }
                }
                $praxi->old_placements = collect($temp2);
                // ************** END TEST  *******************
                if($praxi->praxi_type == 11 ){
                    $temp[] = $praxi;
                }
            }
        }

        $praxeis = collect($temp);

        foreach($praxeis as $praxi){
            $this->aitisiTypeF($praxi);
            $this->oldAitisiTypeF($praxi);
//            $this->createTitle($praxi, $teacher);
            $this->createTitle($praxi, $myschool);
            $this->baseAitisiTypeF($praxi);
        }

//        return $teacher->teacherable->organiki;

//        return $praxeis;

//        $pdf = \PDF::loadView('teacher.placements.all_topothetiria',compact('praxeis', 'teacher'));

//        return $praxeis;

        $pdf = \PDF::loadView('teacher.placements.all_new_topothetiria',compact('praxeis', 'myschool'));

        return $pdf->setOrientation('portrait')->stream();

    }

    public function allPlacementsPDFbyLetter($teacher_first_letter_list, $year_list)
    {
        $praxis_list = Praxi::whereIn('year_id', $year_list)->lists('id');

        $teachers = MySchoolTeacher::with(['placements' => function($q) use($praxis_list, $teacher_first_letter_list){
            $q->whereIn('praxi_id', $praxis_list)
                ->withTrashed();
        }])
            ->where('last_name', 'like', $teacher_first_letter_list.'%')
            ->orderBy('last_name')
            ->get()
            ->filter(function($p){
                if(count($p->placements) != 0) return true;
            });

        $pages = collect();

        foreach($teachers as $teacher){
            if (!$teacher->placements->isEmpty()){

                $praxeis = $teacher->placements->unique('praxi_id')->pluck('praxi_id')->values()->all();

                foreach($praxeis as $praxi){
                    $page = Praxi::find($praxi);
                    $page->teacher = $teacher;
                    $page->placements = $teacher->placements;

                    $last_BASE_placement = $teacher->placements->filter(function($p) use ($praxi){
                        if(in_array($p->placements_type, [1,6])){
                            if($p->praxi_id == $praxi) return true;
                        }
                    })->sortByDesc('praxi_id')->first();

                    $new_placements = $teacher->placements->filter(function($p) use ($praxi){
                        if(!in_array($p->placements_type, [1,6])){
                            if($p->praxi_id == $praxi) return true;
                        }
                    })->sortByDesc('praxi_id')->values();
                    $page->last_BASE_placement = $last_BASE_placement;

                    $page->new_placements = $new_placements;

                    $this->aitisiTypeF($page);
                    $this->createTitle_for_leter($page, $page->teacher);
                    $this->baseAitisiTypeF_for_letter($page);

                    $pages->push($page);
                }
            }
        }

        $pdf = \PDF::loadView('teacher.placements.all_topothetiria_by_letter',compact('pages'));
        return $pdf->setOrientation('portrait')->stream();

    }

    public function allPlacementsPDFbyPraxi($praxi)
    {
        $teachers = MySchoolTeacher::with(['placements'=> function($query) use($praxi){
            $query->withTrashed()->where('praxi_id', $praxi->id);
        }])
//            ->whereHas('placements', function($query) use($praxi){
//                $query->withTrashed()->where('praxi_id', $praxi->id);  // IN LARAVEL 5.2 Works
//            })
            ->orderBy('last_name')
            ->get()
            ->filter(function($p){
                if(count($p->placements) != 0) return true;
            })
            ->values();

//        $teachers = MySchoolTeacher::with(['placements' => function($q) use($praxi){
//                        $q->where('praxi_id', $praxi->id)
//                            ->withTrashed();
//                    }])
//                    ->orderBy('last_name')
//                    ->get()
//                    ->filter(function($p){
//                        if(count($p->placements) != 0) return true;
//                    });

        $pages = collect();

        foreach($teachers as $teacher){
            if (!$teacher->placements->isEmpty()){
//                $page = $praxi;
                $page = null;
                $page = Praxi::find($praxi->id);

                $page->teacher = $teacher;
                $page->placements = $teacher->placements;

                $last_BASE_placement = $teacher->placements->filter(function($p) use ($praxi){
                    if(in_array($p->placements_type, [1,6])) return true;
                })->sortByDesc('praxi_id')->first();

                $new_placements = $teacher->placements->filter(function($p) use ($praxi){
                    if(!in_array($p->placements_type, [1,6])) return true;
                })->sortByDesc('praxi_id')->values();
                $page->last_BASE_placement = $last_BASE_placement;

                $page->new_placements = $new_placements;

                $this->aitisiTypeF($page);
                $this->createTitle_for_leter($page, $page->teacher);
                $this->baseAitisiTypeF_for_letter($page);

                $pages->push($page);

            }
        }

        $pdf = \PDF::loadView('teacher.placements.all_topothetiria_by_letter',compact('pages'));
        return $pdf->setOrientation('portrait')->stream();

    }


    private  function baseAitisiTypeF($praxi)
    {
        if ($praxi->last_BASE_placement != null){
            if($praxi->last_BASE_placement->me_aitisi){
                $aitisis = collect([
                    'value' => 'x-true',
                    'text'  => 'με αίτησή'
                ]);
            }else{
                $aitisis = collect([
                    'value' => 'x-false',
                    'text' => 'χωρίς αίτησή'
                ]);
            }
            return $praxi->base_aitisis = $aitisis;
        }
        return $praxi->base_aitisis = null;
    }

    private  function baseAitisiTypeF_for_letter($praxi)
    {
        if (count($praxi->last_BASE_placement) > 0){
            if($praxi->last_BASE_placement['me_aitisi'] == 1){
                $aitisis = collect([
                    'value' => 'x-true',
                    'text'  => 'με αίτησή'
                ]);
            }else{
                $aitisis = collect([
                    'value' => 'x-false',
                    'text' => 'χωρίς αίτησή'
                ]);
            }
            return $praxi->base_aitisis = $aitisis;
        }
        return $praxi->base_aitisis = null;
    }

    private function oldAitisiTypeF($praxi)
    {
        if (!$praxi->old_placements->isEmpty()){
            if (!$praxi->old_placements->contains('me_aitisi', false)) {
                $aitisis = collect([
                    'value' => 'x-true',
                    'text' => 'με αίτησή'
                ]);
            } elseif (!$praxi->old_placements->contains('me_aitisi', true)) {
                $aitisis = collect([
                    'value' => 'x-false',
                    'text' => 'χωρίς αίτησή'
                ]);
            } else {
                $aitisis = collect([
                    'value' => 'true&false',
                    'text' => ''
                ]);
            }

            $praxi->old_aitisis =  $aitisis;
        }else{
            $praxi->old_aitisis = null;
        }
    }

    private function aitisiTypeF($praxi)
    {
        if (!$praxi->placements->contains('me_aitisi', false)) {
            $aitisis = collect([
                'value' => 'x-true',
                'text' => 'με αίτησή'
            ]);
        } elseif (!$praxi->placements->contains('me_aitisi', true)) {
            $aitisis = collect([
                'value' => 'x-false',
                'text' => 'χωρίς αίτησή'
            ]);
        } else {
            $aitisis = collect([
                'value' => 'true&false',
                'text' => ''
            ]);
        }

        $praxi->aitisis =  $aitisis;
    }

    private function createTitle($praxi, $teacher)
    {
//        if($teacher->teacherable->organiki == 2000){
//            $teacher_title = ' αποσπασμένου εκπαιδευτικού από άλλο ΠΥΣΔΕ ';
//        }else{
//            $teacher_title = ' εκπαιδευτικού ';
//        }

        if($teacher->dieuthinsi == \Config::get('requests.my_dieuthinsi')){
            $teacher_title = ' εκπαιδευτικού ';
        }else{
            $teacher_title = ' αποσπασμένου εκπαιδευτικού από άλλο ΠΥΣΔΕ ';
        }

        if(!$praxi->placements->where('placements_type', 2)->isEmpty()){
            $placement_title = 'απόσπασης';
        }elseif(!$praxi->placements->where('placements_type', 7)->isEmpty()) {
            $placement_title = 'τροποποίησης απόσπασης';
        }elseif(!$praxi->placements->where('placements_type', 9)->isEmpty()){
            $placement_title = 'ανάκλησης τοποθέτησης';
        }else{
            $placement_title = '';
        }

        if (!$praxi->placements->where('placements_type', 4)->isEmpty()) {
            $pvthmia = 'ολικής διάθεσης';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';
        }elseif(!$praxi->placements->where('placements_type', 8)->isEmpty()){
            $pvthmia = 'διάθεσης';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';

        }else{
            $pvthmia = '';
            $pvthmia2 = '';
        }
        if($praxi->last_BASE_placement != null){
            if($praxi->last_BASE_placement->placements_type == 6 ){
                $base_placement_tile = 'τροποποίησης προσωρινής τοποθέτησης ';
            }elseif($praxi->last_BASE_placement->placements_type == 1   && $praxi->last_BASE_placement->praxi->decision_number == $praxi->decision_number){
                $base_placement_tile = 'προσωρινής τοποθέτησης ';
            }else{
                $base_placement_tile = '';
            }
        }else{
            $base_placement_tile = '';
        }
        if(!$praxi->placements->where('placements_type', 3)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'διάθεσης';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }elseif(!$praxi->placements->where('placements_type', 5)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'τροποποίησης διάθεσης';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }else{
            $new_placement_operator = '';
            $new_placements_title = '';
            $new_placements_title2 = '';
        }

        $praxi->title =   $base_placement_tile .$new_placement_operator. $new_placements_title . $placement_title . $pvthmia . $teacher_title . $new_placements_title2 . $pvthmia2;

    }

    private function createTitle_for_leter($praxi, $teacher)
    {
        $praxi->placements = $praxi->placements->filter(function($p) use($praxi){
            if ($p->praxi_id == $praxi->id) return true;;
        });

//        if($teacher->teacherable->organiki == 2000){
//            $teacher_title = ' αποσπασμένου εκπαιδευτικού από άλλο ΠΥΣΔΕ ';
//        }else{
//            $teacher_title = ' εκπαιδευτικού ';
//        }

        if($teacher->dieuthinsi == \Config::get('requests.my_dieuthinsi')){
            $teacher_title = ' εκπαιδευτικού ';
        }else{
            $teacher_title = ' αποσπασμένου εκπαιδευτικού από άλλο ΠΥΣΔΕ ';
        }

        if(!$praxi->placements->where('placements_type', 2)->isEmpty()){
            $placement_title = 'απόσπασης';
        }elseif(!$praxi->placements->where('placements_type', 7)->isEmpty()) {
            $placement_title = 'τροποποίησης απόσπασης';
        }elseif(!$praxi->placements->where('placements_type', 9)->isEmpty()){
            $placement_title = 'ανάκλησης τοποθέτησης';
        }else{
            $placement_title = '';
        }

        if (!$praxi->placements->where('placements_type', 4)->isEmpty()) {
            $pvthmia = 'ολικής διάθεσης';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';
        }elseif(!$praxi->placements->where('placements_type', 8)->isEmpty()){
            $pvthmia = 'διάθεσης';
            $pvthmia2 = 'στην Π/θμια Εκπαίδευση';

        }else{
            $pvthmia = '';
            $pvthmia2 = '';
        }
        if(count($praxi->last_BASE_placement) > 0){
            if($praxi->last_BASE_placement['placements_type'] == 6 ){
                $base_placement_tile = 'τροποποίησης προσωρινής τοποθέτησης ';
            }elseif($praxi->last_BASE_placement['placements_type'] == 1   && $praxi->last_BASE_placement['praxi_id'] == $praxi->id){
                $base_placement_tile = 'προσωρινής τοποθέτησης ';
            }else{
                $base_placement_tile = '';
            }
        }else{
            $base_placement_tile = '';
        }
        if(!$praxi->placements->where('placements_type', 3)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'διάθεσης';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }elseif(!$praxi->placements->where('placements_type', 5)->isEmpty()){
            $new_placement_operator = $base_placement_tile == '' ? '' : ' και ';
            $new_placements_title = 'τροποποίησης διάθεσης';
            $new_placements_title2 = 'για συμπλήρωση του υποχρεωτικού του ωραρίου';
        }else{
            $new_placement_operator = '';
            $new_placements_title = '';
            $new_placements_title2 = '';
        }

        $praxi->title =   $base_placement_tile .$new_placement_operator. $new_placements_title . $placement_title . $pvthmia . $teacher_title . $new_placements_title2 . $pvthmia2;

    }


    private function checkUser($teacher_id)
    {
        if ($teacher_id == null) {
//            $teacher_id = Auth::user()->userable->id;
            $teacher_id = Auth::user()->userable->afm;
        }
        return $teacher_id;
    }

}