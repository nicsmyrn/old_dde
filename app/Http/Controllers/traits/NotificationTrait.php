<?php

namespace App\Http\Controllers\traits;

use App\Notification;

trait NotificationTrait
{
    public function makeNotificationRead($action)
    {
        if($action != null){
            Notification::where('uniqueAction', $action)
                ->update([
                    'unread' => 'is_false'
                ]);
        }
    }
}