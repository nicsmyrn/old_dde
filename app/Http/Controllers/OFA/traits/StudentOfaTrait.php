<?php

namespace App\Http\Controllers\OFA\traits;



use App\Http\Requests\StudentRequest;
use Illuminate\Http\Request;
use App\Models\OFA\Student;
use Storage;

use App\Repositories\OFA\StudentRepository;


trait StudentOfaTrait {


    protected $repo;

    public function __construct(StudentRepository $repository)
    {
        $this->repo = $repository;
    }


    protected function getStudents()
    {
        $students = Student::where('school_id', \Auth::user()->userable->id)->get();

        return view('OFA.school.tables.index-students', compact('students'));
    }

    protected function edit(Student $student)
    {
        $type = \Auth::user()->userable->type;

        return view('OFA.school.edit-student', compact('student', 'type'));

    }

    protected function deleteStudent(Request $request)
    {
        if($request->ajax()){
            $am = $request->get('am');

            \DB::table('ofa_students')
                ->where('school_id', \Auth::user()->userable->id)
                ->where('am', $am)
                ->delete();

            flash()->success('', 'ο μαθητής με Αριθμό Μητρώου: '. $am . ' ΔΙΕΓΡΑΦΗ');
        }
    }

    protected function insertFromMySchool(Request $request)
    {
        if($request->hasFile('csv_file')){
            $real_path =  $request->file('csv_file')->getRealPath();
            $mime =  $request->file('csv_file')->getMimeType();

            Storage::put('schools/'.str_slug(\Auth::user()->userable->name).'/myschool.csv', file_get_contents($real_path));

            $this->repo->execute();
        }else{
            flash()->error('Δεν έχετε επιλέξει το σωστό csv αρχείο');
        }
        return redirect()->back();
    }

    protected function updateS($student, $request)
    {
        \DB::table('ofa_students')
            ->where('school_id', \Auth::user()->userable->id)
            ->where('am', $student->am)
            ->update([
                'am'    => $request->get('am'),
                'last_name'    => $request->get('last_name'),
                'first_name'    => $request->get('first_name'),
                'middle_name'    => $request->get('middle_name'),
                'mothers_name'    => $request->get('mothers_name'),
                'class'    => $request->get('class'),
                'sex'    => $request->get('sex'),
                'year_birth'    => $request->get('year_birth'),
            ]);

        flash()->success('Συγχαρητήρια', 'η αποθήκευση των στοιχεών του μαθητή: ['. $student->last_name . ' '. $student->first_name . '] έγινε με επιτυχία');
    }

    protected  function insertNewStudent(StudentRequest $request)
    {
        if(\Request::ajax()){
            $new_student = $request->get('new_student');

            $student = Student::create($new_student + [
                    'school_id'     => \Auth::user()->userable->id
                ]);
            return $student;
        }else{
            abort(404);
        }
    }
}