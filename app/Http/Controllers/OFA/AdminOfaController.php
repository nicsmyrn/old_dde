<?php

namespace App\Http\Controllers\OFA;

use App\Http\Controllers\Controller;


use App\Http\Requests\StudentRequest;
use App\Models\OFA\SportList;
use App\Models\OFA\SportListDetails;
use App\Models\OFA\SportParticipation;
use App\Models\OFA\SportParticipationDetails;
use App\Models\OFA\Student;
use App\Notification;
use App\School;
use Illuminate\Http\Request;

use App\Models\OFA\Sport;
use App\Year;
use App\Http\Controllers\OFA\traits\OutputOfaTrait;

class AdminOfaController extends Controller
{

    use OutputOfaTrait;

    public function __construct()
    {
//        $this->middleware('ofaAdmin');

//        \Auth::loginUsingId(1109); //16 1ο Γυμνάσιο, 57 1 ΕΠΑΛ ΧΑΝΙΩΝ  | 23 4 Γυνάσιο | 42  1 ΓΕΛ
    }

    public function statistics(Request $request)
    {
        $gym = School::where('type', 'gym')->lists('id');
        $lykeia = School::where('type', 'lyk')->orWhere('type', 'epal')->lists('id');
        $dimotika = School::where('type', 'dimotika')->lists('id');



        if($request->get('first') == 'high-school'){
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('first') == 'secondary-school'){
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $gym)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('first') == 'primary-school'){
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $dimotika)->where('locked', true)->get()->groupBy('sport_id');
        }else{
            $first_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }

        if($request->get('second') == 'high-school'){
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('second') == 'secondary-school'){
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $gym)->where('locked', true)->get()->groupBy('sport_id');
        }elseif($request->get('second') == 'primary-school'){
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $dimotika)->where('locked', true)->get()->groupBy('sport_id');
        }else{
            $second_statistic_array = SportList::with('details')->whereIn('school_id', $lykeia)->where('locked', true)->get()->groupBy('sport_id');
        }


        $teams = collect();
        $students = collect();


        foreach($first_statistic_array as $key=>$sportLists){
            $teams->push([
                'sport_name'    => Sport::find($key)->name,
                'female'        => $sportLists->where('gender', 0)->count(),
                'male'          => $sportLists->where('gender', 1)->count(),
                'sum'           => $sportLists->count()
            ]);

        }


        foreach($second_statistic_array as $key=>$sportLists){
            $sumMale = 0;
            $sumFemale = 0;

            foreach($sportLists as $sportList){
                if($sportList->gender == 0){
                    $sumFemale += $sportList->details->count();
                }else{
                    $sumMale += $sportList->details->count();
                }
            }

            $students->push([
                'sport_name'    => Sport::find($key)->name,
                'female_counter'        => $sumFemale,
                'male_counter'          => $sumMale,
                'sum'           => ($sumFemale + $sumMale)
            ]);
        }

        $school_types = [
            'high-school' => 'ΛΥΚΕΙΑ - ΕΠΑΛ',
            'secondary-school' => 'ΓΥΜΝΑΣΙΑ',
            'primary-school'    => 'ΔΗΜΟΤΙΚΑ'
        ];


        return view('OFA.admin.statistics', compact('teams', 'students', 'school_types'));
    }

    public function getLists()
    {
        $schools = School::where('type', 'lyk')->orWhere('type', 'epal')->get();

        return view('OFA.admin.show-all-lists', compact('schools'));
    }

    public function getGlobalLists()
    {
        $lists = SportList::with(['year', 'school'])->whereNotNull('filename')->get();

        return view('OFA.admin.all-lists-by-filter', compact('lists'));
    }

    public function toggleLock($list_id)
    {
        $list = SportList::find($list_id);

        $list->locked = $list->locked ? false : true;

        $list->save();

        flash()->success('Εντάξει...');

        return redirect()->back();
    }

    public function showLists(School $school, Request $request)
    {

        $action = $request->get('action');
        if($action != null){
            Notification::where('uniqueAction', $action)->update([
                'unread' => 'is_false'
            ]);
        }

        $name = $school->name;

        $schools = School::where('type', 'lyk')->orWhere('type', 'epal')->orWhere('type', 'gym')->get();

        $lists = SportList::where('school_id', $school->id)->get();

        $parts = collect();


        foreach($lists as $list){
            foreach($list->participations as $participation){
                $parts->push([
                    'year_name' => $list->year->name,
                    'year_id'   => $list->year_id,
                    'individual'   => $list->sport->individual,
                    'sport_name'    => $list->sport->name,
                    'gender'        => $list->gender,
                    'synodos'       => $list->synodos,
                    'phase'         => $participation->phase,
                    'part_id'       => $participation->id,
                    'checkedStudents'  => $participation->details->pluck('student_id')->all()
                ]);
            }
        }

        $school_id = $school->id;

        return view('OFA.admin.show-school-list', compact('schools', 'name', 'lists', 'parts', 'school_id'));

    }
}