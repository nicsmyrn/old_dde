<?php

namespace App\Http\Controllers\OFA;

use App\Events\SchoolToOfaTeamNotification;
use App\Http\Controllers\Controller;


use App\Http\Requests\StudentRequest;
use App\Models\OFA\SportList;
use App\Models\OFA\SportListDetails;
use App\Models\OFA\SportParticipation;
use App\Models\OFA\SportParticipationDetails;
use App\Models\OFA\Student;
use Illuminate\Http\Request;

use App\Models\OFA\Sport;
use App\Year;
use App\Http\Controllers\OFA\traits\OutputOfaTrait;

class SchoolOfaParticipationsController extends Controller
{

    use OutputOfaTrait;

    protected $school;

    public function __construct()
    {
        $this->middleware('isLykeio');
        $this->school = \Auth::user()->userable;
    }

    /*
     * Participation Status S T A R T S
     */
    public function getParticipationStatus()
    {
        return view('OFA.school.participation-status');
    }

    public function fetchDataForParticipationStatus(Request $request)
    {
        if ($request->ajax()){
            $data = array();

            $data['sports'] = Sport::all();
            $data['years'] = Year::all();
            $data['students'] = $this->school->students;

            return $data;
        }else{
            abort(401);
        }
    }

    public function RemoveFromStatus(Request $request)
    {
        if($request->ajax()){

            $am = $request->get('am');
            $p_status = SportParticipation::find($request->get('p_status_id'));

            if($p_status != null){
                foreach($p_status->details as $detail){
                    if($detail->student_id == $am) $detail->delete();
                }
            }

            return 'ok student :  removed...';
        }else{
            abort(401);
        }
    }

    public function AddToStudents(Request $request)
    {
        if($request->ajax()){

            $am = $request->get('am');
            $p_status = SportParticipation::find($request->get('p_status_id'));

            if($p_status == null){
                $p_status = SportParticipation::create([
                    'list_id'   => $request->get('list_id'),
                    'phase'     => $request->get('phase')
                ]);
            }

            $list_details_id = SportListDetails::where('list_id', $request->get('list_id'))
                ->where('student_id', $am)
                ->first()
                ->id;

            $p_status_new_detail = new SportParticipationDetails();
            $p_status_new_detail->list_details_id = $list_details_id;

            $p_status->details()->save($p_status_new_detail);

            return $p_status->id;
            return 'add to students';
        }else{
            abort(401);
        }
    }

    public function fetchStudentsFromParticipationStatus(Request $request)
    {
        if($request->ajax()) {
            $data = array();

            $data['p_status_id'] = null;
            $data['p_status_students_ids'] = array();


            $phase = $request->get('phase');
            $list_id = $request->get('list_id');

            $participateStatus = SportParticipation::with('details')
                ->where('list_id', $list_id)
                ->where('phase', $phase)
                ->first();

            if($participateStatus != null){
                $data['p_status_id'] = $participateStatus->id;

                foreach($participateStatus->details as $detail){
                    $data['p_status_students_ids'][] = $detail->student_in_list->student_id;
                }
            }


            return $data;
        }else{
            abort(401);
        }
    }

    public function checkIfListAlreadyExistsForParticipationStatus(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $gender = $request->get('gender');
            $school_id = $this->school->id;

            $list = SportList::with('details')
                ->where('year_id', $year_id)
                ->where('school_id', $school_id)
                ->where('sport_id', $sport['id'])
                ->where('gender', $gender)
                ->where('locked', true)                 // Last change 01-02-2018
                ->first();

            if($list == null){
                $data['message'] = 'listDoesNotExist';
            }else{
                $data['message'] = 'listExistsAndCanMakeParticipationStatus';
                $data['list'] = $list->details->pluck('student_id');
                $data['list_id'] = $list->id;
                $data['synodos'] = $list->synodos;
            }

            return $data;
        }else{
            abort(401);
        }
    }

    /*
     * Participation Status E N D S
     */

}