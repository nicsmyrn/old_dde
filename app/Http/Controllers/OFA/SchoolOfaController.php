<?php

namespace App\Http\Controllers\OFA;

use App\Events\SchoolToOfaTeamNotification;
use App\Http\Controllers\Controller;


use App\Http\Requests\StudentFormRequest;
use App\Http\Requests\StudentRequest;
use App\Models\OFA\SportList;
use App\Models\OFA\SportListDetails;
use App\Models\OFA\SportParticipation;
use App\Models\OFA\SportParticipationDetails;
use App\Models\OFA\Student;
use Illuminate\Http\Request;

use App\Models\OFA\Sport;
use App\Year;
use App\Http\Controllers\OFA\traits\OutputOfaTrait;
use App\Http\Controllers\OFA\traits\StudentOfaTrait;
use App\Repositories\OFA\StudentRepository;


use App\Http\Controllers\OFA\traits\StudentOfaAbstract;


class SchoolOfaController extends Controller
{

    use OutputOfaTrait, StudentOfaTrait;

    protected $repo;

    public function __construct(StudentRepository $repository)
    {
        $this->middleware('isLykeio');
        $this->repo = $repository;
    }

    public function insertStudentsFromMySchool(Request $request)
    {
        return $this->insertFromMySchool($request);
    }

    public function listOfStudents()
    {
        return $this->getStudents();
    }

    public function editStudent(Student $student)
    {
        return $this->edit($student);
    }

    public function deleteStudentFromSpecificSchool(Request $request)
    {
        $this->deleteStudent($request);

        return 'ok2';
    }

    public function updateStudent(Student $student, StudentFormRequest $request)
    {
        $this->updateS($student, $request);

        return redirect()->route('OFA::listOfStudents');
    }

    public function getLists()
    {
        return view('OFA.school.create-list-high-school');
    }

    public function getListsIndividual()
    {
        abort(401);
        return view('OFA.school.create-list-high-school-individual');
    }

    public function archivesStatementsHighSchool()
    {
        $lists = SportList::where('school_id', \Auth::user()->userable->id)
                            ->get();

        $parts = collect();

        foreach($lists as $list){
            foreach($list->participations as $participation){
                $parts->push([
                    'year_name' => $list->year->name,
                    'year_id'   => $list->year_id,
                    'individual'   => $list->sport->individual,
                    'sport_name'    => $list->sport->name,
                    'gender'        => $list->gender,
                    'synodos'       => $list->synodos,
                    'phase'         => $participation->phase,
                    'part_id'       => $participation->id,
                    'checkedStudents'  => $participation->details->pluck('student_id')->all()
                ]);
            }
        }

        return view('OFA.school.tables.index-lists', compact('lists', 'parts'));
    }


    public function fetchDataForLists()
    {
        if(\Request::ajax()){
            $data = array();

            $data['sports'] = Sport::where('individual', false)->get();
            $data['year'] = Year::where('current', true)->first();
            $data['students'] = \Auth::user()->userable->students;

            return $data;
        }else{
            abort(404);
        }
    }

    public function fetchDataForListsIndividual()
    {
        if(\Request::ajax()){
            $data = array();

            $data['sports'] = Sport::where('individual', true)->get();
            $data['year'] = Year::where('current', true)->first();
            $data['students'] = \Auth::user()->userable->students;

            return $data;
        }else{
            abort(404);
        }
    }




    public function temporarySaveNewList(Request $request)
    {
        if($request->ajax()){
            $list_id = $request->get('existedListId');
            $attr = [
                'studentsList'  => $request->get('studentsList'),
                'gender'   => $request->get('current_sex'),
                'sport' => $request->get('current_sport'),
                'year'       => $request->get('year'),
                'school'        => \Auth::user()->userable,
                'teacher_name'  => $request->get('teacher_name'),
                'synodos'       => $request->get('synodos')
            ];

            if($list_id == 0){
                $newList = $this->createTablesInDatabase($attr);
            }else{
                $newList = $this->updateTablesInDatabase($attr, $list_id);
            }
            return $newList->id;

        }else{
            abort(401);
        }
    }



    public function insertNewList(Request $request)
    {
        if($request->ajax()){
            $list_id = $request->get('existedListId');

            $attr = [
                'studentsList'  => $request->get('studentsList'),
                'gender'   => $request->get('current_sex'),
                'sport' => $request->get('current_sport'),
                'year'       => $request->get('year'),
                'school'        => \Auth::user()->userable,
                'teacher_name'  => $request->get('teacher_name'),
                'synodos'       => $request->get('synodos')
            ];

            $school_folder = str_slug($attr['school']->name). '/' . $attr['year']['name'] . '/OfaLists/';
            $sport_name = $attr['sport']['name'];

            $gender = $attr['gender'] == 0 ? 'ΚΟΡΙΤΣΙΩΝ' : 'ΑΓΟΡΙΩΝ';

            $file_path = $school_folder .str_slug($sport_name).'-'.str_slug($gender)  . '.pdf';


            if($list_id == 0){
                $newList = $this->createTablesInDatabase($attr, $file_path);
            }else{
                $newList = $this->updateTablesInDatabase($attr, $list_id, $file_path);
            }


            $this->sportListToPDF($attr, $school_folder,  $file_path, 'OFA.school.PDF.sport-list');

            flash()->overlayS('Συγχαρητήρια!', 'Η λίστα αγώνων δημιουργήθηκε με επιτυχία και το έγγραφο PDF στάλθηκε στο γραφείο Φυσικής Αγωγής. Για να δείτε το έγγραφο από το μενού Σχολικά Πρωταθλήματα -> Αρχείο Δηλώσεων του Σχολείου');

            event(new SchoolToOfaTeamNotification($attr['school'],[
                'action' => 'OFA\AdminOfaController@showLists',
                'title' => 'Δημιουργία Λίστας Αγώνων',
                'description' => "Η Σχολική Μονάδα <span class='notification_full_name'>&laquo;{$attr['school']['name']}&raquo;</span> δημιούργησε με επιτυχία τη λίστα αγώνων στο άθλημα <span class='notification_full_name'>{$sport_name}</span>",
                'type' => 'success'
            ]));

            return route('OFA::sportList');
        }else{
            abort(401);
        }
    }

    public function checkIfListAlreadyExists(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $gender = $request->get('gender');
            $school_id = \Auth::user()->userable->id;

            $list = SportList::where('year_id', $year_id)
                        ->where('school_id', $school_id)
                        ->where('sport_id', $sport['id'])
                        ->where('gender', $gender)
                        ->first();

            if($list == null){
                $data['response'] = 'canMakeSchoolList';
            }else{
                if($list->locked){
                    $data['response'] = 'isLocked';
                    $data['teacher_name'] = $list->teacher_name;
                }else{
                    $data['response'] = 'temporaryListExists';
                    $data['list'] = $list->details->pluck('student_id')->all();
                    $data['list_id'] = $list->id;
                    $data['teacher_name'] = $list->teacher_name;
                }
            }
            return $data;
        }else{
            abort(401);
        }
    }

    public function checkIfListAlreadyExistsForIndividual(Request $request)
    {
        if($request->ajax()){
            $data = array();

            $sport = $request->get('sport');
            $year_id = $request->get('year_id');
            $school_id = \Auth::user()->userable->id;

            $data['special_sports'] = Sport::find($sport['id'])->special;

            $list = SportList::where('year_id', $year_id)
                ->where('school_id', $school_id)
                ->where('sport_id', $sport['id'])
                ->first();

            if($list == null){
                $data['response'] = 'canMakeSchoolListIndividual';
            }else{
                if($list->locked){
                    $data['response'] = 'isLockedIndividual';
                    $data['synodos'] = $list->synodos;
                }else{
                    $data['response'] = 'temporaryListExistsIndividual';
                    $data['list'] = $list->details->pluck('student_id')->all();
                    $data['studentListIndividual'] = $list->details;
                    $data['list_id'] = $list->id;
                    $data['synodos'] = $list->synodos;
                }
            }
            return $data;
        }else{
            abort(401);
        }
    }

}