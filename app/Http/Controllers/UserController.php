<?php

namespace App\Http\Controllers;

use App\Teacher;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use App\School;
use App\Http\Requests\UserRequest;

use App\Repositories\UserRepository;
use App\Events\PysdeSendNotificationToSchool;

use Illuminate\Contracts\Filesystem\Factory as Filesystem;
use Gate;

class UserController extends Controller
{
    protected $repo;
    
    public function __construct(UserRepository $userRepo)
    {
        $this->middleware('auth');
        
        $this->repo = $userRepo;
    }

    public function create()
    {
        $this->denies('create_user');

        $role_list = Role::lists('name','id');
        $school_list = [''=>''] + School::lists('name', 'id')->all();
        
        return view('pysde.user.create', compact('role_list', 'school_list'));
    }


    public function store(UserRequest $request)
    {
        $this->denies('create_user');

        $this->repo->createUser($request);
        
        flash()->success('Συγχαρητήρια.','Δημιουργήσατε με επιτυχία τον χρήστη '.$request->get('email'));
        
        return redirect()->back();
    }

    public function indexSchools()
    {
        $this->denies('user_school_admin');

        $schools = School::with('user')->get();

        return view('pysde.user.indexSchools', compact('schools'));
    }

    public function indexTeachers()
    {
        $this->denies('user_teacher_admin');

        $teachers = Teacher::with(['user', 'teacherable'])->get();
        $schools = School::lists('name', 'id');

        return view('pysde.user.indexTeachers', compact('teachers', 'schools'));
    }

    public function ajaxIndex()
    {

    }

    public function ajaxModalData(Request $request)
    {
        if($request->ajax()){
            $id = $request->get('id');
            $school = School::with('user')->find($id);

            return $school;
        }
        abort(403);
    }

    public function ajaxTeacherModalData(Request $request)
    {
        $this->denies('user_teacher_admin');

        if($request->ajax()){
            $id = $request->get('id');
            $teacher = Teacher::with(['user','teacherable'])->find($id);

            $teacher->full_name = $teacher->user->full_name;


            $teacher->relation = $teacher->teacherable_type == 'App\Monimos' ? 'Μόνιμος' : 'Αναπληρωτής';
            $teacher->teacherable->organiki = $teacher->teacherable->organiki;
            $teacher->klados = $teacher->klados_name;
            $teacher->family_s = $teacher->family_situation;
            $teacher->special_s = $teacher->special_situation;
            $teacher->dimos_e = $teacher->dimos_entopiotitas;
            $teacher->dimos_s = $teacher->dimos_sinipiretisis;

            return $teacher;
        }
        abort(403);
    }

    public function ajaxSaveSchoolModalDetails(Request $request)
    {
        $this->denies('user_school_admin');

        $data = array();

        $rules = [
            'last_name' =>  'required|min:3|alpha',
            'first_name' =>  'required|min:3|alpha',
            'email' => 'email',
            'type' => 'in:Λύκειο,Γυμνάσιο,ΕΠΑΛ,Ειδικό',
            'group' => 'in:Ομάδα 1,Ομάδα 2, Ομάδα 3,Ομάδα 4,Ομάδα 5,Ομάδα 6'
        ];

        $messages = [
            'last_name.required'    => 'Το Επώνυμο είναι υποχρεωτικό',
            'last_name.min'         => 'Το Επώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'last_name.alpha'       => 'Το Επώνυμο πρέπει να είναι γράμματα',
            'first_name.required'   => 'Το Όνομα είναι υποχρεωτικό',
            'first_name.min'        => 'Το Όνομα πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'first_name.alpha'      => 'Το Όνομα πρέπει να είναι γράμματα',
            'email.email'           => 'Το E-mail δεν είναι έγκυρο',
            'type.in'               => 'Ο τύπος Σχολείου δεν είναι σωστός',
            'group.in'              => 'Η ομάδα Σχολείων δεν είναι σωστή'
        ];

        if ($request->ajax()){
            $validator = \Validator::make($request->all(), $rules,$messages);

            if ($validator->fails()){
                return \Response::json($validator->errors()->all(), 422);
//                return 'false';
            }else{
                $id = $request->get('schoolID');
                $school = School::find($id);

                $school->user->update($request->all());
                $school->update($request->all());

                event(new PysdeSendNotificationToSchool($school, [
                    'title' => 'Αλλαγή Στοιχείων',
                    'description' => "Το ΠΥΣΔΕ <span class='notification_full_name'>άλλαξε</span> τα στοιχεία του προφίλ σας",
                    'type' => 'warning',
                    'action' => 'ProfileController@getProfile'
                ]));

                $data['return'] = 'query_success';

                $data['school'] = [
                    'name' => $school->name,
                    'type' => $school->type,
                    'group' => $school->group,
                    'full_name' => $school->user->full_name
                ];
                return $data;
            }
        }
        abort(403);
    }

    public function ajaxSaveTeacherModalDetails(Request $request)
    {
        $this->denies('user_teacher_admin');

        $data = array();

        $rules = [
            'moria' => 'numeric|between:0,400',
            'moria_apospasis' => 'numeric|between:0,400',
            'teacherID'    => 'required|int',
            'ex_years'      => 'int|between:0,40',
            'ex_months'      => 'int|between:0,11',
            'ex_days'      => 'int|between:0,30',
        ];

        $messages = [
            'moria.numeric'    => 'Τα μόρια πρέπει να είναι αριθμός',
            'moria.between'    => 'Τα μόρια πρέπει να είναι μεταξύ 0 και 400',
            'moria_apospasis.numeric'    => 'Τα μόρια πρέπει να είναι αριθμός',
            'moria_apospasis.between'    => 'Τα μόρια πρέπει να είναι μεταξύ 0 και 400',
            'teacherID.required'    => 'Το Id του καθηγητή είναι υποχρεωτικό',
            'teacherID.int'    => 'Το Id πρέπει να είναι ακέραιος',
            'ex_years.int'    => 'Τα χρόνια προϋπηρεσίας πρέπει να είναι ακέραιος',
            'ex_months.int'    => 'Οι μήνες προϋπηρεσίας πρέπει να είναι ακέραιος',
            'ex_days.int'    => 'Οι ημέρες προϋπηρεσίας πρέπει να είναι ακέραιος',
            'ex_years.between'    => 'Τα χρόνια προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 40',
            'ex_months.between'    => 'Οι μήνες προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 11',
            'ex_days.between'    => 'Οι ημέρες προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 30',

        ];

        if ($request->ajax()){
            $validator = \Validator::make($request->all(), $rules,$messages);

            if ($validator->fails()){
                return \Response::json($validator->errors()->all(), 422);
//                return 'false';
            }else{
                $id = $request->get('teacherID');
                $teacher = Teacher::find($id);

                $teacher->update([
                    'ex_years'       => $request->get('ex_years'),
                    'ex_months'      => $request->get('ex_months'),
                    'ex_days'        => $request->get('ex_days'),
                    'middle_name'    => $request->get('middle-name'),
                    'childs'         => $request->get('children'),
                    'dimos_sinipiretisis' => $request->get('dimos_sinipiretisis'),
                    'dimos_entopiotitas'  => $request->get('dimos_entopiotitas'),
                    'family_situation'  => $request->get('family_situation'),
                    'special_situation' => $request->get('special_situation')
                ]);

                $teacher->user->update([
                    'sex'   => $request->get('sex'),
                    'last_name' => $request->get('last_name'),
                    'first_name'=> $request->get('first_name')
                ]);

                $teacher->teacherable->update([
                    'moria'              => $request->get('moria'),
                    'moria_apospasis'    => $request->get('moria_apospasis'),
                    'am'                 => $request->get('am'),
                    'orario'             => $request->get('orario'),
                    'organiki'           => $request->get('organiki')
                ]);



                // --->>>>>>   notification to Teacher  <<<<<<<<<------------

                $data['return'] = 'query_success';

                $data['moria'] = $teacher->teacherable->moria;
                $data['moria_apospasis'] = $teacher->teacherable->moria_apospasis;

                $data['id'] = $teacher->id;

                return $data;
            }
        }
        abort(403);
    }

    public function ajaxSaveTeacherModalLittleDetails(Request $request)
    {
        $this->denies('user_teacher_admin');

        $data = array();

        $rules = [
            'teacherID'    => 'required|int',
        ];

        $messages = [
            'ex_days.between'    => 'Οι ημέρες προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 30',
        ];

        if ($request->ajax()){
            $validator = \Validator::make($request->all(), $rules,$messages);

            if ($validator->fails()){
                return \Response::json($validator->errors()->all(), 422);
//                return 'false';
            }else{
                $id = $request->get('teacherID');
                $teacher = Teacher::find($id);

                $teacher->update([
                    'middle_name'    => $request->get('middle-name'),
                    'klados_id'      => $request->get('klados')
                ]);

                $teacher->user->update([
                    'sex'   => $request->get('sex'),
                    'last_name' => $request->get('last_name'),
                    'first_name'=> $request->get('first_name')
                ]);

                $teacher->teacherable->update([
                    'am'                 => $request->get('am'),
                    'orario'             => $request->get('orario'),
                    'organiki'           => $request->get('organiki')
                ]);

                // --->>>>>>   notification to Teacher  <<<<<<<<<------------

                flash()->success('', 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία');
                return 'true';
            }
        }
        abort(403);
    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }

}
