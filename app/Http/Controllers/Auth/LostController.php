<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\traits\ResetPassword;


class LostController extends Controller
{

    use ResetPassword;

    public function __construct()
    {
        $this->middleware('guest');
    }
}
