<?php

namespace App\Http\Controllers\Auth\traits;

use App\Reset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait ResetPassword{
    public function getEmail()
    {
        if (\Auth::check()){
            return view('user.change_password');
        }else{
            return view('auth.password');
        }
    }

    public function postEmail(Request $request)
    {
        $this->validate($request,
            ['email' => 'required|email|exists:users'],
            ['email.required' => 'Το E-mail είναι υποχρεωτικό',
             'email.email'    => 'Το E-mail που γράψατε δεν είναι σωστό',
             'email.exists'   => 'Το E-mail δεν υπάρχει στη βάση δεδομένων'
            ]);
            
        if (\Auth::check()){
            if (\Auth::user()->email != $request->get('email')){
                flash()->error('Προσοχή!', 'Το E-mail που γράψατε δεν σας ανήκει.');
                return redirect()->back();
            }

            if(\Auth::user()->provider == 'google'){
                flash()->error('Σημείωση!', 'Ο λογαριασμός σας ανήκει στη Google.');
                return redirect()->back();
            }elseif(Auth::user()->provider == 'sch.gr'){
                flash()->error('Σημείωση!', 'Ο λογαριασμός σας ανήκει στο Πανελλήνιο Σχολικό Δίκτυο.');
                return redirect()->back();
            }

            $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject('Αλλαγή κωδικού')
                    ->from('mail3didechanion@gmail.com');
            });

        }else{
            $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject('Ανάκτηση Κωδικών')
                    ->from('mail3didechanion@gmail.com');
            });
        }

        switch ($response) {
            case Password::RESET_LINK_SENT:
//                flash('Συγχαρητήρια', trans($response));
                return redirect()->back()->with('status', trans($response));

            case Password::INVALID_USER:
//                flash()->error('Προσοχή', trans($response));
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }
    }

    public function getReset($token = null)
    {
        if (is_null($token)) {
            abort(404);
        }

        $reset = Reset::where('token', $token)->first();

        if (!$reset){
            abort(404);
        }else{
            return view('auth.reset', compact('reset'));
        }
    }

    public function postReset(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ],[
            'token.required' => 'Το token είναι υποχρεωτικό',
            'email.required' => 'Το E-mail είναι υποχρεωτικό',
            'email.email'    => 'Το E-mail δεν είναι έγκυρο',
            'password.required' => 'Ο κωδικός είναι υποχρεωτικός',
            'password.confirmed'=> 'Οι κωδικοί δεν ταιριάζουν',
            'password.min'      => 'Ο κωδικος πρέπει να είναι τουλάχιστον 6 χαρακτήρων'
        ]);

        $credentials = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );


        $response = Password::reset($credentials, function ($user, $password) {
            $this->resetPassword($user, $password);
        });

            switch ($response) {
                case Password::PASSWORD_RESET:
                    flash('Συγχαρητήρια!', trans($response));
                    return redirect($this->redirectPath())->with('status', trans($response));

                default:
                    return redirect()->back()
                        ->withInput($request->only('email'))
                        ->withErrors(['email' => trans($response)]);
            }
    }

    protected function resetPassword($user, $password)
    {
        $user->password = bcrypt($password);

        $user->save();

//        Auth::login($user);
    }

    protected function getEmailSubject()
    {
        return property_exists($this, 'subject') ? $this->subject : 'Your Password Reset Link';
    }

    public function redirectPath()
    {
        if (property_exists($this, 'redirectPath')) {
            return $this->redirectPath;
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/';
    }
}
