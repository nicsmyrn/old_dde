<?php

namespace App\Http\Controllers\Auth\traits;

use App\Http\Requests\TeacherRegistrationRequest;
use App\Register;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

trait RegisterTeachers {
    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        dd('ΜΗ ΔΙΑΘΕΣΙΜΟ');
        return view('auth.register');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(TeacherRegistrationRequest $request) //TeacherRegistrationRequest
    {
        dd('ΜΗ ΔΙΑΘΕΣΙΜΟ');

        $token = Str::random(128);

        $new_user = Register::firstOrNew(['email' => $request->get('email')]);

        if ($new_user->exists){
            $new_user->update([
                'last_name' => $request->get('last_name'),
                'first_name' => $request->get('first_name'),
                'email'      => $request->get('email'),
                'password'   => bcrypt($request->get('password')),
                'token'      => $token
            ]);
        }else{
            $new_user = Register::create([
                'last_name' => $request->get('last_name'),
                'first_name' => $request->get('first_name'),
                'email'      => $request->get('email'),
                'password'   => bcrypt($request->get('password')),
                'token'      => $token
            ]);
        }


        \Mail::send('emails.activate_register',[
            'token' => $token,
            'last_name' => $new_user->last_name,
            'first_name' => $new_user->first_name
        ], function($m) use ($new_user) {
            $m->from('mail3didechanion@gmail.com', 'Δ.Δ.Ε. Χανίων')
                ->to($new_user->email, $new_user->last_name .' '. $new_user->first_name)
                ->subject('Επιβεβαίωση εγγραφής');
        });

        return view('auth.registration_success');
    }

    public function getActivate($token)
    {
        $register_user = Register::where('token', $token)->first();

        if($register_user == null){
            abort(403);
        }else{
            \DB::beginTransaction();
                $new_user = User::create([
                    'first_name'    => $register_user->first_name,
                    'last_name'     => $register_user->last_name,
                    'email'         => $register_user->email,
                    'userable_type' => 'App\Teacher',
                    'password'      => $register_user->password,
                    'role_id'       => 6,   // TEACHER
                ]);

                $register_user->delete();
            \DB::commit();

            return view('auth.confirm_activation');
        }
    }
}