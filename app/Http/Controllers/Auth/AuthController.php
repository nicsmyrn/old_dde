<?php

namespace App\Http\Controllers\Auth;

use App\Sch_gr\SchAuth;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\traits\RegisterTeachers;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

use App\AuthenticateUser;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

//    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    use AuthenticatesUsers, RegisterTeachers, ThrottlesLogins;
    
        protected $redirectTo = '/';
        protected $redirectAfterLogout = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    public function ddeLogin()
    {
        return view('auth.loginDDE');
    }

    public function postDdeLogin(Request $request)
    {
        $lockout = [
            'oikonomikachanion@sch.gr'
        ];

        if(!in_array($request->get('email'), $lockout)){
            dd('MΗ ΔΙΑΘΕΣΙΜΟ. Παρακαλώ συνδεθείτε με το Πανελλήνιο Σχολικό Δίκτυο');
        }

        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();
//
        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        Log::warning('Αποτυχημένη προσπάθεια σύνδεσης από το χρήστη:'.$request->get('email'));

        return redirect('auth/only-for-dde')
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }
    
    public function socialLogin(AuthenticateUser $authenticateUser, Request $request, $provider = null)
    {
        return $authenticateUser->execute($request->has('code'), $this,  $provider);
    }

    public function schGrLogin(SchAuth $schAuth)
    {
        return $schAuth->execute($this);
    }
    
    public function userHasLoggedIn($user)
    {
        Log::info('Ο χρήστης:'.$user->id.' με Ονομετεπωνυμο:'.$user->full_name.' ΣΥΝΔΕΘΗΚΕ ΜΕ ΕΠΙΤΥΧΙΑ.');

        if(session('error666school') == 'is_true'){
            if (\Auth::check()) \Auth::logout();

            flash()->overlayS('Προσοχή!', 'ο λογαριασμός που συνδεθήκατε έχει πρόβλημα. Επικοινωνήστε με τον διαχειριστή της ΠΑΣΙΦΑΗ Σμυρναίο Νικόλαο στο 6973009154.');
        }else{
            flash()->info('Καλώς ήρθες', $user->first_name);
        }

        return redirect('/');
    }

    public function userHasPushedOut($email)
    {
        Log::info('Ο χρήστης με E-mail : '. $email . ' έκανε απόπειρα σύνδεσης στην ΠΑΣΙΦΑΗ');
        \phpCAS::logoutWithRedirectService(config('school_gr.redirect_path_suspended'));
    }

    public function postLogin(Request $request){
        dd('MΗ ΔΙΑΘΕΣΙΜΟ. Παρακαλώ συνδεθείτε με το Πανελλήνιο Σχολικό Δίκτυο');

//        dd('ΜΗ ΔΙΑΘΕΣΙΜΟ');

        $this->validate($request, [
            $this->loginUsername() => 'required', 'password' => 'required',
        ]);

        $throttles = $this->isUsingThrottlesLoginsTrait();
//
        if ($throttles && $this->hasTooManyLoginAttempts($request)) {
            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->getCredentials($request);

        if (Auth::attempt($credentials, $request->has('remember'))) {
            return $this->handleUserWasAuthenticated($request, $throttles);
        }

        if ($throttles) {
            $this->incrementLoginAttempts($request);
        }

        Log::warning('Αποτυχημένη προσπάθεια σύνδεσης από το χρήστη:'.$request->get('email'));

        return redirect($this->loginPath())
            ->withInput($request->only($this->loginUsername(), 'remember'))
            ->withErrors([
                $this->loginUsername() => $this->getFailedLoginMessage(),
            ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role_id' => $data['role_id']
        ],[
            
        ]);
    }

    public function getLogout(SchAuth $schAuth)
    {

        if(Auth::check()){

            Log::info('Ο χρήστης με ID:'.Auth::user()->id.' και Ονοματεπώνυμο: '.Auth::user()->full_name.' ΑΠΟΣΥΝΔΕΘΗΚΕ');

            if (Auth::user()->provider == 'sch.gr'){
                Auth::logout();

                $schAuth->logout();

            }else{
                Auth::logout();
            }
            flash('Επιτυχής αποσύνδεση');
        }

        return redirect('/');
    }
    
    
}

