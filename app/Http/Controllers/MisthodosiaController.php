<?php

namespace App\Http\Controllers;

use App\Misthodosia;
use App\MisthodosiaFake;
use App\TeknaFake;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notification;
use App\Teacher;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\MisthodosiaRequest;
use Smail;
use App\Events\TeacherChangeMisthodosiaNotification;
use App\Events\TeacherSentNewMisthodosiaDetails;

class MisthodosiaController extends Controller
{
    protected $user;

    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->user = $auth->user();
    }

    public function checkMisthodosia()
    {
        return 'notification here...';
    }


    public function getMisthodosia(Request $request)
    {
        $action = $request->get('action');
        if($action != null){
            Notification::where('uniqueAction', $action)->update([
                'unread' => 'is_false'
            ]);
        }
        if ($this->user->userable_type == 'App\Teacher') {
            if ($this->user->userable == null) {
                flash()->overlayE('Προσοχή!', 'πρέπει να συμπληρώστε πρώτα τα στοιχεία του Προφίλ σας και στη συνέχεια τα μισθολογικά');
                return redirect()->action('ProfileController@getProfile');
            } else {
                $newMisthodosia = false;
                $teacher = $this->user->userable;

                if($teacher->myschool->misthodosia == null){
                    return 'Error 87391';
                }else{
                    if($teacher->fmid != null){
                        $misthodosia = MisthodosiaFake::find($teacher->fmid);
                    }else{
                        $misthodosia = $teacher->myschool->misthodosia;
                    }
                }
                return view('user.misthodosia', compact('teacher', 'misthodosia', 'newProfile'));
            }

        }elseif($this->user->userable_type == 'App\School') {
            return view('user.profile');
        }
    }

    public function postMisthodosia(MisthodosiaRequest $request)
    {
        if($this->user->userable->misthodosia_counter >=3){
            flash()->overlayE('Προσοχή!', 'Δεν έγινε ενημέρωση των στοιχείων σας. Μέχρι τον επόμενο έλεγχο, δεν μπορείτε να στείλετε νέα στοιχεία στο Τμήμα Μισθοδοσίας');
            return redirect()->back();
        }

        $attachments = array();

        for($i=1; $i<=3; $i++){
            if($request->hasFile('attachment'.$i)){
                $attachments[] = [
                    'real_path' => $request->file('attachment'.$i)->getRealPath(),
                    'options' =>
                        [
                            'as'  => 'Δικαιολογητικό_No'.$i,
                            'mime' => $request->file('attachment'.$i)->getMimeType()
                        ]
                ];
            }
        }


        $misthodosia = MisthodosiaFake::where('teacher_id', $this->user->userable->id)->firstOrFail();

        \DB::beginTransaction();

        $misthodosia->update($request->all());


        TeknaFake::where('misthodosia_id', $misthodosia->fmid)->delete();

        if(count($request->get('tekna')) > 0){
            foreach($request->get('tekna') as $tekna){
                TeknaFake::create([
                    'birth'         =>  $tekna['birth'],
                    'college_start_date'    => $tekna['StartCollege'],
                    'college_end_date'      => $tekna['EndCollege'],
                    'description'           => $tekna['description'],
                    'misthodosia_id'        => $misthodosia->fmid
                ]);
            }
        }

        $this->user->userable->update([
            'misthodosia_counter'   => $this->user->userable->misthodosia_counter + 1
        ]);

        \DB::commit();

        event(new TeacherChangeMisthodosiaNotification($this->user, [
            'title' => 'Αποστολή νέων στοιχείων',
            'description' => "Ο καθηγητής <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> έστειλε νέα στοιχεία Μισθοδοσίας.",
            'type' => 'primary',
            'teacherID' => $this->user->userable->afm
        ]));

        if(\Config::get('requests.sent_mail')) {
            Smail::sentTeacherToMisthodosiaRequest($attachments, $this->user->full_name, $this->user->userable->afm);
        }

        flash()->overlayS('Συγχαρητήρια!','έγινε αποστολή των στοιχείων σας στο τμήμα Μισθοδοσίας. Θα ακολουθήσει έλεγχος και ενημέρωσή σας με σχετικό E-mail');

        return redirect()->back();
    }

    public function ajaxRequestChangeMisthodosia(Request $request)
    {
        if($request->ajax()){

            $teacher = $this->user->userable;

            $teacher->request_to_change_misthodosia = true;

            $teacher->save();

            $data = array();

            flash()->success('','Το Αίτημά σας στάλθηκε με επιτυχία...');

            if(\Config::get('requests.sent_mail')) {

                Smail::sentTeacherMisthodosia($teacher, 'Αίτημα αλλαγής στοιχείων Μισθοδοσίας', [
                    'type' => 'changeMisthodosiaRequest',
                    'full_name' => $teacher->user->full_name,
                    'afm'       => $teacher->afm
                ]);
            }
            $data['return'] = 'query_success';

            event(new TeacherChangeMisthodosiaNotification($this->user, [
                'title' => 'Αίτημα αλλαγής Μισθοδοσίας',
                'description' => "Ο καθηγητής <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> αιτείται να αλλάξει τα στοιχεία της Μισθοδοσίας.",
                'type' => 'danger',
                'teacherID' => $teacher->afm
            ]));

            return $data;
        }else{
            abort(404);
        }
    }

    public function undoRequestToChangeMisthodosia()
    {
        $teacher = $this->user->userable;

        $teacher->request_to_change_misthodosia = false;

        $teacher->save();

        flash()->success('Το αίτημα για αλλαγή στοιχείων αναιρέθηκε');

        return redirect()->back();
    }


}
