<?php

namespace App\Http\Controllers;

use App\Http\Controllers\traits\NotificationTrait;
use App\NewEidikotita;
use App\NewYperarithmia;
use App\Sch_gr\Facade\Sch;
use App\Yperarithmia;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\School;
use Auth;
use App\ExcelFiles;
use App\Eidikotita;
use App\Role;
use App\Http\Requests\KenaRequest;
use App\Http\Requests\SchoolRequest;

use App\Events\SchoolGrantEditPermission;
use Gate;
use App\Notification;

use App\Repositories\Pysde\KenaRepository;

class KenaController extends Controller
{
    use NotificationTrait;

    protected $repo;
    
    public function __construct(KenaRepository $kenaRepo)
    {
        $this->middleware('auth');
        
        $this->repo = $kenaRepo;
    }

    public function editOrganikaKenaPleonasmata()
    {
        return view('pysde.kena_pleonasmata.organika-kena-pleonasmata');
    }

    public function getOrganikaKenaSchools(Request $request)
    {
        if($request->ajax()){
            return School::all()->lists('name', 'id');
        }else{
            abort(404);
        }
    }


    public function getOrganikaKenaYperarithmonSchools(Request $request)
    {
        if($request->ajax()){
            return School::where('type', '<>', 'eidiko')->get(['name', 'id', 'order']);
        }else{
            abort(404);
        }
    }


    public function postEidikotitesForSchool(Request $request)
    {
        if ($request->ajax()){
            $currentEidikotites = array();

            $schoolId = $request->get('schoolId');

//            $eidikotites = Eidikotita::all();
            $eidikotites = NewEidikotita::all();

            foreach($eidikotites as $eidikotita){
//                $hasKenoPleonasma = Yperarithmia::where('school_id', $schoolId)->where('eidikotita_id', $eidikotita->id)->first();
                $hasKenoPleonasma = NewYperarithmia::where('school_id', $schoolId)->where('eidikotita_id', $eidikotita->id)->first();
                if($hasKenoPleonasma == null){
                    $number = 0;
                }else{
                    $number = $hasKenoPleonasma->number;
                }
                $currentEidikotites[] = [
                    'id'    => $eidikotita->id,
                    'name'  => $eidikotita->full_name,
                    'number'=> $number
                ];
            }
            return $currentEidikotites;
        }
        abort(404);
    }

    public function saveEidikotitaForSchool(Request $request)
    {
        $message = '';
        if ($request->ajax()){
            $school = School::find($request->get('schoolId'));
            $eidikotitaId = $request->get('eidikotitaId');
            $number = $request->get('number');

            if ($school->new_yperarithmies->where('id', $eidikotitaId)->first() == null){
                $school->new_yperarithmies()->attach($eidikotitaId, ['number' => $number]);
                $message = 'Νέα καταχώρηση με επιτυχία';
            }else{
                $database_Number = $school->new_yperarithmies->where('id', $eidikotitaId)->first()->pivot->number;
                if ($database_Number != $number){
                    $school->new_yperarithmies()->detach($eidikotitaId);
                    $school->new_yperarithmies()->attach($eidikotitaId, ['number' => $number]);
                    $message = 'Ενημερώθηκε με επιτυχία';
                }
            }

            return $message;
        }
    }

    public function index()
    {
        $this->denies('edit_school_kena');
        
        $schools = School::lists('name');
        $schools_can_edit = Role::checkAccess();
        
        return view('pysde.kena_pleonasmata.create', compact('schools',  'schools_can_edit'));
    }
    
    
    public function indexView()
    {
        $this->denies('read_All_Schools_Kena');
        
        $schools = School::lists('name', 'id')->all();
        
        return view('pysde.kena_pleonasmata.view', compact('schools'));  
    }
    
    
    public function all()
    {
        $this->denies('read_All_Schools_Kena');
        
        $eidikotites = Eidikotita::all();
        $schools = School::with('eidikotites')->get();
        
        return view('pysde.kena_pleonasmata.all', compact('schools', 'eidikotites'));
    }
    
    public function show(School $school)
    {
        $this->denies('read_All_Schools_Kena');
        
        $schools = School::lists('name');
        $name = $school->name;
        $last_update = $school->last_update;
        
        $eidikotites =  $this->repo->eidikotitesNotZeroSchools($school);
        $parallili = $this->repo->paralliliNotZeroSchools($school);
        $mathimata = $this->repo->mathimataNotZeroSchools($school);
        $eidikis = $this->repo->eidikisNotZeroSchools($school);
        
        return view('school.show', compact('schools','name', 'last_update', 'eidikotites', 'parallili', 'mathimata', 'eidikis'));
    }

    public function printShow(School $school, Request $request)
    {
        $this->denies('read_All_Schools_Kena');

        $name = $school->name;
        $last_update = $school->last_update;

        $eidikotites =  $this->repo->eidikotitesNotZeroSchools($school);
        $parallili = $this->repo->paralliliNotZeroSchools($school);
        $mathimata = $this->repo->mathimataNotZeroSchools($school);
        $eidikis = $this->repo->eidikisNotZeroSchools($school);

        return view('school.printShow', compact('name', 'last_update', 'eidikotites', 'parallili', 'mathimata', 'eidikis'));

    }

    public function edit(School $school, Request $request)
    {
        $this->denies('edit_school_kena');

        $this->makeNotificationRead($request->get('action'));

        $schools_list = School::lists('name', 'id')->all();
        $schools_can_edit = Role::checkAccess();

        return view('pysde.kena_pleonasmata.edit', compact('school', 'schools_list', 'schools_can_edit'));
    }


    public function update(SchoolRequest $request, School $school)
    {
        $this->denies('edit_school_kena');

        $this->repo->updateKenaOfSchools($request, $school);
            
        return redirect()->back();
    }
    
    
    public function changeSchoolAccess()
    {
        $this->denies('edit_school_kena');
        
        event(new SchoolGrantEditPermission('edit_school_kena'));
        
        return redirect()->back();
    }

    public function confirmationSchools(Request $request)
    {
        $this->denies('confirmation_for_schools');

        $this->makeNotificationRead($request->get('action'));

        $schools = School::orderBy('confirmation_date','desc')->get();

        return view('pysde.confirmation.index', compact('schools'));
    }

    public function printDescriptions()
    {
        $this->denies('confirmation_for_schools');

        $schools = School::where('description', '<>', '')->orderBy('confirmation_date','desc')->get();

        return view('pysde.confirmation.index', compact('schools'));
    }

    public function calculateMoria()
    {
        return view('pysde.moria.moria_apospasis');
    }
    
    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }    

}
