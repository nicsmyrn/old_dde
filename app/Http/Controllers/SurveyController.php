<?php

namespace App\Http\Controllers;

use App\School;
use App\Survey;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SureyRequest;

class SurveyController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('role:school');
    }
    
    public function getCreateSurvey()
    {
        $user = \Auth::user();
        
        $sch_id = $user->school->id;
        $sch_name = $user->school->name;
        
        $found = Survey::where('sch_id', $sch_id)->first();

        if (!$found){
            return view('school.survey.create', compact('sch_id', 'sch_name'));
        }else{
            return view('school.survey.survey_exists');
        }
    }

    public function postStoreSurvey(SureyRequest $request)
    {
        Survey::create($request->all());

        return view('school.survey.success');//
    }
}
