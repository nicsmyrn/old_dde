<?php

namespace App\Http\Controllers;

use App\Eidikotita;
use App\Events\TeacherRequestForMoriodotisi;
use App\Events\TeacherSendNotificationToPysde;
use App\NewEidikotita;
use App\OrganikiPrefrences;
use App\OrganikiRequest;
use App\Prefrences;
use App\ProtocolTeacher;
use App\RequestTeacherYperarithmia;
use App\School;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use DB;
use App\RequestTeacher;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gate;
use Illuminate\Support\Facades\Auth;
use Smail;
use App\Events\TeacherChangeProfileNotification;
class RequestController extends Controller
{
    private $user;

    private $schoolType = [
        'Δημοτικό' => 'dimotiko',
        'Νηπιαγωγείο' => 'nipiagogeio'
    ];

    private $types = [
        'Αίτηση-Υπεραριθμίας'   => 'Υπεραρ',
        'Αίτηση-Οργανικής-Υπεράριθμου'  => 'ΟργανικήΥπ',
        'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης'    => 'Βελτίωση',
        'Αίτηση-Μοριοδότησης-Μετάθεσης' => 'Μοριοδ',
        'Αίτηση-Μοριοδότησης-Απόσπασης' => 'ΜοριοδΑ',
    ];

    private $moriodotisi = [
        'Αίτηση-Μοριοδότησης-Μετάθεσης' => 'ΜΕΤΑΘΕΣΗΣ',
        'Αίτηση-Μοριοδότησης-Απόσπασης' => 'ΑΠΟΣΠΑΣΗΣ'
    ];

    public function __construct(Guard $auth)
    {
        $this->middleware('teacher_activation_ckeck');
        $this->user = $auth->user();
    }

    public function ajaxPermanentlyDeleteRequestOrganika(Request $request)
    {
        if ($request->ajax()){
            $type = $request->get('type');
            $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                            ->where('aitisi_type', $type)
                            ->where('protocol_number', null)
                            ->first();
            $request->delete();

            flash()->overlayE('Προσοχή!!!', 'Η Αίτηση διαγράφηκε Οριστικά και ΔΕΝ θα ληφθεί υπόψη');

            return route('Aitisi::Prepare');

        }
    }


    public function ajaxFetchSchoolsVeltiwsi(Request $request)
    {
        if($request->ajax()){
            $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
//                ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                ->where(DB::raw('TIMEDIFF("2018-05-24 18:00:00", date_request)'), '<', 0)
                ->where('aitisi_type', 'Βελτιώση - Οριστική τοποθέτηση')
                ->first();

            $selectedSchoolsList = [];
            $selectedSchools = array();

            if($request != null){
                if(is_integer($request->protocol_number)){
                    return 'is_made';
                }
                foreach($request->prefrences->sortBy('order_number') as $prefrence){
                    $selectedSchoolsList[] = $prefrence->school_id;
                    $selectedSchools[] = [
                        'id'    => $prefrence->school_id,
                        'name'  => $prefrence->OrganikiName,
                        'order' => $prefrence->OrganikiOrder
                    ];
                }
            }

            $allSchools =  School::where('municipality_id', 0)
                ->whereNotIn('id', $selectedSchoolsList)
                ->orderBy('order')
                ->get(['name', 'id', 'order']);

            return compact('allSchools', 'selectedSchools');
        }else{
            abort(404);
        }
    }

    public function ajaxFetchSchools(Request $request)
    {
        if($request->ajax()){
            $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                ->where('aitisi_type', 'Ονομαστικά Υπεράριθμος')
                ->first();

            $selectedSchoolsList = [];
            $selectedSchools = array();

            if($request != null){
                if(is_integer($request->protocol_number)){
                    return 'is_made';
                }
                foreach($request->prefrences->sortBy('order_number') as $prefrence){
                    $selectedSchoolsList[] = $prefrence->school_id;
                    $selectedSchools[] = [
                        'id'    => $prefrence->school_id,
                        'name'  => $prefrence->OrganikiName,
                        'order' => $prefrence->OrganikiOrder
                    ];
                }
            }

            $allSchools =  School::where('municipality_id', 0)
                            ->whereNotIn('id', $selectedSchoolsList)
                            ->orderBy('order')
                            ->get(['name', 'id', 'order']);

            return compact('allSchools', 'selectedSchools');
        }else{
            abort(404);
        }
    }

    public function ajaxSaveOrganikaRequest(Request $request)
    {
        if ($request->ajax()){
            $message = $this->saveOrganikiRequest($request);

            return $message;
        }
    }

    public function ajaxSaveOrganikaRequestVeltiwsi(Request $request)
    {
        if ($request->ajax()){
            $message = $this->saveOrganikiRequestVeltiwsi($request);

            return $message;
        }
    }

    public function ajaxSentOrganikaRequestForProtocol(Request $request)
    {
        $message = $this->saveOrganikiRequest($request);

        $this->giveOrganikiProtocol($request);

        return 'all ok for Organika Yperarithmous';
    }

    public function ajaxSentOrganikaRequestForProtocolVeltiwsi(Request $request)
    {
        $message = $this->saveOrganikiRequestVeltiwsi($request);

        $this->giveOrganikiProtocolVeltiwsi($request);

        return 'all ok';
    }

    public function ajaxCheckIfYperarithmiaExists(Request $request)
    {
        return 'ok';
    }

    public function ajaxFetchSchoolsYperarithmou(Request $request)
    {
        if($request->ajax()){
            $aitisi_type = $request->get('aitisi_type');

            $schoolType = $request->get('schoolType');

            $request = OrganikiRequest::with(['prefrences' => function($query){
                $query->orderBy('order_number');
            }])
                ->where('teacher_id', $this->user->userable->id)
                ->where('groupType', 'Υπεράριθμος')
                ->get();

            return $request;

            $selectedSchoolsList = [];
            $selectedSchoolsListIdiaOmada = [];
            $selectedSchoolsListOmores = [];

            $selectedSchools = array();
            $selectedSchoolsIdiaOmada = array();
            $selectedSchoolsOmores = array();

            $stay_to_organiki = false;
            $return_to_organiki = false;

            $isMadeValue = 'not_protocol_given';
            $isMadeValueApospasi = false;

            if(!$request->isEmpty()){
                if($request->where('situation', 'yperarithmos')->first() != null && is_integer($request->where('situation', 'yperarithmos')->first()->protocol_number)){
                    $isMadeValue = 'protocol_given';
                    return compact('isMadeValue');
                }
                if($request->where('aitisi_type', 'Απόσπαση')->first() != null && is_integer($request->where('aitisi_type', 'Απόσπαση')->first()->protocol_number)){
                    $isMadeValueApospasi = true;
                }
                foreach($request as $r){
                    if($r->aitisi_type == 'ΥπεράριθμοςΊδιαΟμάδα'){
                        $stay_to_organiki = $r->stay_to_organiki;
                        $return_to_organiki = $r->return_to_organiki;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListIdiaOmada[] = $prefrence->school_id;
                            $selectedSchoolsIdiaOmada[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder
                            ];
                        }
                    }
                    if($r->aitisi_type == 'ΥπεράριθμοςΌμορες'){
                        $stay_to_organiki = $r->stay_to_organiki;
                        $return_to_organiki = $r->return_to_organiki;
                        foreach($r->prefrences as $prefrence){
                            $selectedSchoolsListOmores[] = $prefrence->school_id;
                            $selectedSchoolsOmores[] = [
                                'id'    => $prefrence->school_id,
                                'name'  => $prefrence->OrganikiName,
                                'order' => $prefrence->OrganikiOrder
                            ];
                        }
                    }
                    if(!$isMadeValueApospasi){
                        if($r->aitisi_type == 'Απόσπαση'){
                            foreach($r->prefrences as $prefrence){
                                $selectedSchoolsList[] = $prefrence->school_id;
                                $selectedSchools[] = [
                                    'id'    => $prefrence->school_id,
                                    'name'  => $prefrence->OrganikiName,
                                    'order' => $prefrence->OrganikiOrder
                                ];
                            }
                        }
                    }
                }
            }else{
                $searchForApospasi = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                    ->where('groupType', 'Απόσπαση')
                    ->first();

                if($searchForApospasi != null){
                    $isMadeValueApospasi = true;
                }
            }

            $organiki = School::find($this->user->userable->teacherable->organiki);
            $allSchoolsIdiasOmadas =  $organiki->idiaomada->schools;
            $allSchoolsIdiasOmadas = $allSchoolsIdiasOmadas->where('type', $organiki->type)->values();

            $allSchoolsIdiasOmadas = $allSchoolsIdiasOmadas->reject(function($value) use($selectedSchoolsListIdiaOmada){
                return in_array($value->id, $selectedSchoolsListIdiaOmada);
            })->sortBy('order')->values();

            if(!$isMadeValueApospasi){
                $allSchools =  School::where('type', $this->schoolType[$organiki->type])
                    ->whereNotIn('id', $selectedSchoolsList)
                    ->orderBy('order')
                    ->get();
            }else{
                $allSchools = [];
            }


            $idiaomada = $organiki->idiaomada;
            $omores =  $organiki->idiaomada->omores;
            $omoresArray = [];

            $allSchoolsOmoron = collect();

            foreach($omores as $omori){
                $omoresArray[] = [
                    'name'  => $omori->name,
                    'description' => $omori->description
                ];
                foreach($omori->schools as $school){
                    if($school->type == $organiki->type){
                        $allSchoolsOmoron->push($school);
                    }
                }
            }

            $allSchoolsOmoron = $allSchoolsOmoron->reject(function($value) use($selectedSchoolsListOmores){
                return in_array($value->id, $selectedSchoolsListOmores);
            })->sortBy('order')->values();

            return compact(
                'allSchoolsIdiasOmadas',
                'allSchoolsOmoron',
                'allSchools',
                'selectedSchools',
                'selectedSchoolsIdiaOmada',
                'selectedSchoolsOmores',
                'schoolType',
                'isMadeValue',
                'idiaomada',
                'omoresArray',
                'isMadeValueApospasi',
                'stay_to_organiki',
                'return_to_organiki'
            );
        }else{
            abort(404);
        }
    }

    public function ajaxCreateYperarithmia(Request $request)
    {
        $unique_id = uniqid(Auth::id());

        if ($request->ajax()) {
            $teacher_folder = md5(\Auth::user()->userable->id);
            $file_path = $teacher_folder . '/' . $unique_id . '.pdf';
            \Storage::makeDirectory('teachers/'.$teacher_folder);

            DB::beginTransaction();
                $protocol = ProtocolTeacher::create([
                    'p_date'    =>  date('d/m/Y'),
                    'from'      => \Auth::user()->full_name . ' του '. \Auth::user()->userable->middle_name,
                    'type'      => 'Επιθυμία Υπεραριθμίας',
                    'subject'   => 'Αίτηση Επιθυμίας Υπεραριθμίας',
                    'f_id'      => 2,   // Φ.2.1
                ]);

                $request_teacher = RequestTeacherYperarithmia::create([
                    'protocol_number'       => $protocol->id,
                    'year'                  => date('Y'),
                    'unique_id'             => $unique_id,
                    'teacher_id'            => \Auth::user()->userable->id,
                    'want_yperarithmia'     => $request->get('want_yperarithmia'),
                    'school_organiki'       => \Auth::user()->userable->myschool->organiki_name,
                    'file_name'             => $file_path,
                    'date_request'          => Carbon::now()
                ]);
            DB::commit();

            $pdf = \PDF::loadView('teacher.aitisis.organikes.YP_PDF', compact('request_teacher','protocol'));

            $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

            event(new TeacherSendNotificationToPysde(\Auth::user(), $request['a_type'], [
                'title' => 'Αίτηση ΥΠΕΡΑΡΙΘΜΙΑΣ Εκπαιδευτικού',
                'description' => "Ο <span class='notification_full_name'>&laquo;{$this->user->userable->myschool->full_name}&raquo;</span> έχει στείλει στο ΠΥΣΔΕ αίτηση <strong>ΕΠΙΘΥΜΙΑΣ ΥΠΕΡΑΡΙΘΜΙΑΣ</strong> με αριθμό αίτησης: {$protocol->protocol_name}",
                'type' => 'primary'
            ]));
            flash()->overlayS('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');


            if(\Config::get('requests.sent_mail')) {
                Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $this->user);
                Smail::sentAitisiToPysde($protocol, $request_teacher, $file_path, $this->user);
            }
        }
    }

    public function createPreparation()
    {
        return view('teacher.prepare_create');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requests = RequestTeacher::where('teacher_id', \Auth::user()->userable->id)->get();

        $title = 'Αρχείο Αιτήσεων - Ενστάσεων - Μοριοδότησης - Ε1, Ε2, Ε3, Ε4, Ε5, Ε5';
        $type = 'simple';

        return view('teacher.index', compact('requests','title', 'type'));

    }

    public function indexOrganiki()
    {
        $requests = OrganikiRequest::where('teacher_id', \Auth::user()->userable->id)->get();

        $title = 'Αρχείο Αιτήσεων για Οργανική Θέση';
        $type = 'organiki';

        return view('teacher.index', compact('requests','title', 'type'));
    }

    public function indexYperarithmia()
    {
        $requests = RequestTeacherYperarithmia::where('teacher_id', \Auth::user()->userable->id)->get();

        $title = 'Αρχείο Αιτήσεων Επιθυμίας Υπεραριθμίας';
        $type = 'yperarithmia';

        return view('teacher.index', compact('requests','title','type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type)
    {
        $this->checkAitisisAvailability($type);

        $this->denies('create_'.$type.'_request');

        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();

        return view('teacher.create', compact('type', 'eidikotita', 'organiki'));
    }

    public function organikiCreate($type)
    {
        $new_type = $this->types[$type];

        $this->denies('create_'.$new_type.'_request');

        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();

        if($new_type == 'Υπεραρ'){
            return view('teacher.aitisis.organikes.epithimia-yperarithmias', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'ΟργανικήΥπ'){
            return view('teacher.aitisis.organikes.aitisi-yperarithmou', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'Βελτίωση'){
            return view('teacher.aitisis.organikes.aitisi-gia-veltiosi', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'Μοριοδ'){
            return view('teacher.aitisis.moriodotisis', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'ΜοριοδΑ'){
            return view('teacher.aitisis.moriodotisis', compact('type', 'eidikotita', 'organiki'));
        }else{
            return 'wrong view';
        }
    }

    public function organikiCreateTest($type)
    {
        $new_type = $this->types[$type];

//        $this->denies('create_'.$new_type.'_request');

        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();


        if($new_type == 'Υπεραρ'){
            return view('teacher.aitisis.organikes.epithimia-yperarithmias', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'ΟργανικήΥπ'){
            return view('teacher.aitisis.organikes.aitisi-yperarithmou', compact('type', 'eidikotita', 'organiki'));
        }elseif($new_type == 'Βελτίωση'){
            return view('teacher.aitisis.organikes.aitisi-gia-veltiosi', compact('type', 'eidikotita', 'organiki'));
        }else{
            return 'wrong view';
        }
    }

    public function sentAttachementsMoriodotisi(Request $request)
    {
//        return $request->all();

        $unique_id = uniqid(Auth::id());



        $teacher_folder = md5(\Auth::user()->userable->id);
        $file_path = $teacher_folder . '/'. $unique_id . '.pdf';
        \Storage::makeDirectory('teachers/'.$teacher_folder);

        DB::beginTransaction();
        $protocol = ProtocolTeacher::create([
            'p_date'    =>  date('d/m/Y'),
            'from'      => \Auth::user()->full_name . ' του '. \Auth::user()->userable->middle_name,
            'type'      => 'Μοριοδότηση '. $this->moriodotisi[$request->get('type')],
            'subject'   => 'Αίτηση Μοριοδότησης '. $this->moriodotisi[$request->get('type')],
            'f_id'      => 1,   // Φ.2.1
        ]);

        $description = '';

        if($this->moriodotisi[$request->get('type')] == 'ΜΕΤΑΘΕΣΗΣ'){
            $description = 'όπως υπολογιστούν τα μόρια μετάθεσης για τις λειτουργικές τοποθετήσεις τον Σεπτέμβρη του 2018';
        }elseif($this->moriodotisi[$request->get('type')] == 'ΑΠΟΣΠΑΣΗΣ'){
            $description = 'όπως υπολογιστούν τα μόρια απόσπασης';
        }

        $request_teacher = RequestTeacher::create([
            'unique_id'             => $unique_id,
            'protocol_number'       => $protocol->id,
            'teacher_id'            => \Auth::user()->userable->id,
            'file_name'             => $file_path,
            'aitisi_type'           => 'ΜΟΡΙΑ '. $this->moriodotisi[$request->get('type')],
            'date_request'          => Carbon::now(),
            'subject'               => 'ΑΙΤΗΣΗ ΜΟΡΙΟΔΟΤΗΣΗΣ '. $this->moriodotisi[$request->get('type')],
            'description'           => $description
        ]);
        DB::commit();

        $pdf = \PDF::loadView('teacher.aitisis.moriodotisi', compact('request_teacher','protocol', 'type'));

        $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

        $attachments = array();

        $attachments[] = [
            'real_path' => storage_path('app/teachers/'.$file_path),
            'options' =>
                [
                    'as'  => 'ΑΙΤΗΣΗ ΜΟΡΙΟΔΟΤΗΣΗΣ '.$this->moriodotisi[$request->get('type')],
                    'mime' => 'application/pdf'
                ]
            ];

        for($i=1; $i<=3; $i++){
            if($request->hasFile('attachment'.$i)){
                $attachments[] = [
                    'real_path' => $request->file('attachment'.$i)->getRealPath(),
                    'options' =>
                        [
                            'as'  => 'Δικαιολογητικό_No'.$i,
                            'mime' => $request->file('attachment'.$i)->getMimeType()
                        ]
                ];
            }
        }


        if(\Config::get('requests.sent_mail') && count($attachments) > 0) {
            Smail::sentTeacherToProswpikoForMoriodotisi($attachments, $this->user->full_name, $this->user->userable->myschool->afm, $this->user->userable->myschool->am, $this->user->userable->id, $this->moriodotisi[$request->get('type')]);
            Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $this->user);
        }

        event(new TeacherRequestForMoriodotisi($this->user, [
            'title' => 'Αίτημα Μοριοδότησης',
            'description' => "Ο Εκπαιδευτικός <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> του {$this->user->userable->middle_name} έχει αιτηθεί να υπολογιστούν τα <span class='notification_full_name'> μόρια {$this->moriodotisi[$request->get('type')]} </span>",
            'type' => 'danger',
            'teacherID' => $this->user->userable->fid != null ? $this->user->userable->teacher->id : $this->user->userable->id
        ]));

        flash()->overlayS('Συγχαρητήρια '.$this->user->first_name,'Η Αίτηση '.$this->moriodotisi[$request->get('type')].' / Δικαιολογητικά στάλθηκε στο τμήμα Γ Προσωπικού. Ο αριθμός της Αίτησης είναι: ' . $protocol->protocol_name);

        return redirect()->route('welcome');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aitisi = $this->validateTeacherRequest($request);

        $this->denies('create_'.$aitisi['aitisi_type'].'_request');

        if ($request->ajax()){      //if not a simple aitisi then
            DB::beginTransaction();
                $new_request = RequestTeacher::create($aitisi);

                $i = 1;
                foreach($aitisi['prefrences'] as $prefrence){
                    Prefrences::create([
                        'request_id'    => $new_request->id,
                        'order_number'  => $i,
                        'school_name'   => $prefrence['school_name']
                    ]);
                    $i = $i + 1;
                }
                $this->giveProtocol($request, $new_request->unique_id);
            DB::commit();
//            return $request->all();
        }else{  //if simple aitisi
            DB::beginTransaction();
                $new_request = RequestTeacher::create($aitisi);

                $this->giveProtocol($request, $new_request->unique_id);
            DB::commit();

            return redirect()->route('Aitisi::archives');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($unique_id)
    {
        $request = RequestTeacher::where('unique_id', $unique_id)->first();
        $eidikotita = $this->findEidikotita();
        $organiki = $this->findOrganiki();

        if($request->protocol_number == null){
            return view('teacher.edit', compact('unique_id', 'request', 'eidikotita', 'organiki'));
        }

        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aitisi = $this->validateTeacherRequest($request);

        $request_teacher = RequestTeacher::where('unique_id', $id)->first();

        if ($request->ajax()){ // if not a simple aitisi
            DB::beginTransaction();
                $request_teacher->description = $aitisi['description'];
                $request_teacher->date_request = $aitisi['date_request'];
                $request_teacher->hours_for_request = $aitisi['hours_for_request'];
                $request_teacher->reason = $aitisi['reason'];
                $request_teacher->stay_to_organiki = $aitisi['stay_to_organiki'];
                $request_teacher->save();

                $delete_rows = Prefrences::where('request_id', $request_teacher->id)->delete();

                $i = 1;
                foreach($aitisi['prefrences'] as $prefrence){
                    Prefrences::create([
                        'request_id'    => $request_teacher->id,
                        'order_number'  => $i,
                        'school_name'   => $prefrence['school_name']
                    ]);
                    $i = $i + 1;
                }
                $this->giveProtocol($request, $id);
            DB::commit();

        }else{  //if simple aitisi
            DB::beginTransaction();
                $request_teacher->update([
                    'subject' => $aitisi['subject'],
                    'schools_that_is' => $aitisi['schools_that_is'],
                    'description' => $aitisi['description'],
                    'date_request' => $aitisi['date_request'],
                ]);

                $this->giveProtocol($request,$request_teacher->unique_id);
            DB::commit();

            return redirect()->route('Aitisi::archives');
        }

    }

    private function giveOrganikiProtocol(Request $request)
    {
        if($request->ajax()){
            $request_teacher = OrganikiRequest::where('teacher_id', $this->user->userable->id)
                                ->where('aitisi_type', 'Ονομαστικά Υπεράριθμος')
                                ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                                ->first();

            if ($request_teacher != null){
                $user = $request_teacher->teacher->user;
                $teacher = $request_teacher->teacher;

                if ($request_teacher->protocol_number == null && $request_teacher->file_name == null){
                    $protocolRequest = array(
                        'p_date'    => date('d/m/Y'),
                        'from'      => $user->full_name . ' του '. $teacher->middle_name,
                        'type'      => $request_teacher->aitisi_type,
                        'subject'   => 'Αίτηση Ονομαστικά Υπεράριθμου για Οργανική',
                        'f_id'      => 2
                    );
                    $teacher_folder = md5($teacher->id);
                    $file_path = $teacher_folder . '/check_' . $request_teacher->unique_id . '.pdf';
                    \Storage::makeDirectory('teachers/'.$teacher_folder);

                    DB::beginTransaction();
                    $protocol = ProtocolTeacher::create($protocolRequest);
                    $request_teacher->update([
                        'protocol_number'   => $protocol->id,
                        'file_name'         => $file_path
                    ]);
                    DB::commit();

                    $pdf = \PDF::loadView('teacher.aitisis.organikes.PDF-Organika-Yperarithmos', compact('request_teacher','protocol'));

                    $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

                    event(new TeacherSendNotificationToPysde(\Auth::user(), $request['a_type'], [
                        'title' => 'Αίτηση για Οργανική',
                        'description' => "Ο <span class='notification_full_name'>&laquo;{$user->full_name}&raquo;</span> έχει στείλει στο ΠΥΣΔΕ αίτηση ως ονομαστικά Υπεράριθμος για Οργανική <strong>{$request_teacher->aitisi_type}</strong> με αριθμό αίτησης: {$protocol->protocol_name}",
                        'type' => 'primary'
                    ]));
                    flash()->overlayS('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλθεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');
                    if(\Config::get('requests.sent_mail')) {
                        Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $user);
                        Smail::sentAitisiToPysde($protocol, $request_teacher, $file_path, $user);
                    }
                }else{
                    flash()->overlayE('Σφάλμα!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
                }
            }else{
                flash()->overlayE('Σφάλμα!!!', 'Error code : 1141');

            }
        }

            return redirect()->back();


    }

    private function giveOrganikiProtocolVeltiwsi(Request $request)
    {
        if($request->ajax()){
            $request_teacher = OrganikiRequest::where('teacher_id', $this->user->userable->id)
//                ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
                ->where(DB::raw('TIMEDIFF("2018-05-24 18:00:00", date_request)'), '<', 0)
                ->where('aitisi_type', 'Βελτιώση - Οριστική τοποθέτηση')
                ->first();

            if ($request_teacher != null){
                $user = $request_teacher->teacher->user;
                $teacher = $request_teacher->teacher;

                if ($request_teacher->protocol_number == null && $request_teacher->file_name == null){
                    $protocolRequest = array(
                        'p_date'    => date('d/m/Y'),
                        'from'      => $user->full_name . ' του '. $teacher->middle_name,
                        'type'      => $request_teacher->aitisi_type,
                        'subject'   => 'Αίτηση Δήλωση για Βελτίωση - Οριστική τοποθέτηση',
                        'f_id'      => 3
                    );
                    $teacher_folder = md5($teacher->id);
                    $file_path = $teacher_folder . '/check_' . $request_teacher->unique_id . '.pdf';
                    \Storage::makeDirectory('teachers/'.$teacher_folder);

                    DB::beginTransaction();
                    $protocol = ProtocolTeacher::create($protocolRequest);
                    $request_teacher->update([
                        'protocol_number'   => $protocol->id,
                        'file_name'         => $file_path
                    ]);
                    DB::commit();

                    $pdf = \PDF::loadView('teacher.aitisis.organikes.PDF-Veltiwsi', compact('request_teacher','protocol'));

                    $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

                    event(new TeacherSendNotificationToPysde(\Auth::user(), $request['a_type'], [
                        'title' => 'Αίτηση για Οργανική',
                        'description' => "Ο <span class='notification_full_name'>&laquo;$user->full_name&raquo;</span> έχει στείλει στο ΠΥΣΔΕ αίτηση Βελτίωσης - Οριστικής Τοποθέτησης <strong>{$request_teacher->aitisi_type}</strong> με αριθμό αίτησης: {$protocol->protocol_name}",
                        'type' => 'primary'
                    ]));
                    flash()->overlayS('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλθεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');
                    if(\Config::get('requests.sent_mail')) {
                        Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $user);
                        Smail::sentAitisiToPysde($protocol, $request_teacher, $file_path, $user);
                    }
                }else{
                    flash()->overlayE('Σφάλμα!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
                }
            }
        }



        return redirect()->back();


    }

    private function giveProtocol(Request $request, $uid = null)
    {
        if($request->has('sent_request')){
            $request_teacher = RequestTeacher::where('unique_id', $uid)->first();
            if ($request_teacher == null){
                abort(404);
            }
            $user = $request_teacher->teacher->user;
            $teacher = $request_teacher->teacher;

            if ($request_teacher->protocol_number == null && $request_teacher->file_name == null){
                $protocolRequest = array(
                    'p_date'    => date('d/m/Y'),
                    'from'      => $user->full_name,
                    'type'      => $request_teacher->aitisi_type,
                    'subject'   => 'Αίτηση '. $request_teacher->aitisi_type,
                    'f_id'      => $request_teacher->type
                );
//3#####################################################33###########################
                $teacher_folder = md5($teacher->id);
                $file_path = $teacher_folder . '/' . $uid . '.pdf';
                \Storage::makeDirectory('teachers/'.$teacher_folder);

                DB::beginTransaction();
                    $protocol = ProtocolTeacher::create($protocolRequest);
                    $request_teacher->update([
                        'protocol_number'   => $protocol->id,
                        'file_name'         => $file_path
                    ]);
                DB::commit();

                if($request_teacher->aitisi_type == 'ΑΠΛΗ'){
                    $pdf = \PDF::loadView('teacher.simpleAitisiPDF', compact('request_teacher', 'protocol'));
                }else{
                    $pdf = \PDF::loadView('teacher.aitisiPDF', compact('request_teacher','protocol'));
                }

                $pdf->setOrientation('portrait')->save(storage_path('app/teachers/'.$file_path));

                event(new TeacherSendNotificationToPysde(\Auth::user(), $request['a_type'], [
                    'title' => 'Αίτηση Εκπαιδευτικού',
                    'description' => "Ο <span class='notification_full_name'>&laquo;{$user->full_name}&raquo;</span> έχει στείλει στο ΠΥΣΔΕ αίτηση <strong>{$request_teacher->aitisi_type}</strong> με αριθμό αίτησης: {$protocol->protocol_name}",
                    'type' => 'primary'
                ]));
                flash()->overlayS('Συγχαρητήρια', 'Η αίτησή σας έχει αριθμό : <strong>'.$protocol->protocol_name.'</strong>. Αντίγραφο της αίτησης έχει σταλθεί στη γραμματεία του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο.');
                if(\Config::get('requests.sent_mail')) {
                    Smail::sentAitisiToTeacher($protocol, $request_teacher, $file_path, $user);
                    Smail::sentAitisiToPysde($protocol, $request_teacher, $file_path, $user);
                }
            }else{
                flash()->overlayE('Σφάλμα!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
            }
            return redirect()->back();

        }elseif($request->has('save_request')){
            flash()->success('Συγχαρητήρια','Η αίτηση αποθηκεύτηκε με επιτυχία');
        }elseif($request->has('update_request')){
            flash()->success('', 'Η αίτησή ενημερώθηκε με επιτυχία');
        }else{
            flash()->overlayE('Σφάλμα22!!!', 'Δε δόθηκε κανένας αριθμός  στην αίτηση');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        return 'deleted';
    }

    public function ajaxEdit($unique_id){
        $requestTeacher = RequestTeacher::with('prefrences')->where('unique_id', $unique_id)->first();

        return $requestTeacher;
    }

    public function deleteRowRequest($unique_id, $type)
    {
        if($type == 'organiki'){
            $requestTeacher = OrganikiRequest::where('unique_id', $unique_id)->first();
        }elseif($type == 'simple'){
            $requestTeacher = RequestTeacher::where('unique_id', $unique_id)->first();
        }elseif($type == 'yperarithmia'){
            $requestTeacher = RequestTeacherYperarithmia::where('unique_id', $unique_id)->first();
        }else{
            abort(403);
        }

        if($requestTeacher->protocol_number == null){
            $requestTeacher->delete();

            flash()->success('','Η αίτηση διεγράφη με επιτυχία');

            return 'success';
        }else{
            flash()->overlayE('Προσοχή!','Η ενέργεια που κάνατε δεν είναι αποδεκτή. Ο λογαριασμός σας θα είναι υπό επιτήρηση');

            return 'error';
        }

    }


    public function downloadPDF($unique_id)
    {
        $teacher_folder = md5(\Auth::user()->userable->id);

        $file = $unique_id.'.pdf';
        $file_path = storage_path('app/teachers/'.$teacher_folder).'/'.$file;


        if (file_exists($file_path)){
            return response()->download($file_path, $file, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            $file = 'check_' . $file;
            $organiki_file = storage_path('app/teachers/'.$teacher_folder).'/'.$file;

            if(file_exists($organiki_file)){
                return response()->download($organiki_file, $file, [
                    'Content-Length: '. filesize($organiki_file)
                ]);
            }

            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
        }
    }


    public function temptoHTML()
    {
        return view('teacher.simpleAitisiPDF');
    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }

    /**
     * @param Request $request
     */
    private function validateTeacherRequest(Request $request)
    {
        if ($request->ajax()) {

        } else { // if simple request - ΑΠΛΗ Αίτηση
            $this->validate($request, [
                'subject' => 'required',
                'schools_that_is' => 'required',
                'description' => 'required'
            ], [
                'subject.required' => 'Το Θέμα είναι υποχρεωτικό',
                'schools_that_is.required' => 'Το/τα σχολείο/α που υπηρετείς είναι υποχρεωτικό/ά',
                'description.required' => 'Η περιγραφή της αίτησης είναι υποχρεωτική'
            ]);
        }

        return $aitisi = [
            'subject'           => $request->get('subject'),
            'aitisi_type'       => $request->get('a_type'),
            'description'       => $request->get('description'),
            'schools_that_is'   => $request->get('schools_that_is'),
            'prefrences'        => $request->get('prefrences'),
            'hours_for_request' => $request->get('hours_for_request'),
            'stay_to_organiki'  => $request->get('stay_to_organiki'),
            'reason'            => $request->get('reason'),
            'unique_id'         => uniqid(\Auth::id()),
            'teacher_id'        => \Auth::user()->userable->id,
            'date_request'      => Carbon::now()
        ];
    }

    /**
     * @param $type
     */
    private function checkAitisisAvailability($type)
    {
        if (!in_array($type, \Config::get('requests.aitisis_type'))) {
            abort(404);
        } else {
            if (\Auth::user()->userable->teacherable_type == 'App\Monimos' && $type == 'E5') {
                abort(404);
            } elseif (\Auth::user()->userable->teacherable_type == 'App\Anaplirotis' && in_array($type, ['E1', 'E3', 'E4', 'E6', 'E7', 'Οργανικές', 'Βελτιώσεις'])) {
                abort(404);
            }
        }
    }

    /**
     * @return string
     */
    private function findOrganiki()
    {
        $organiki = School::find(\Auth::user()->userable->teacherable->organiki);

        if ($organiki == null) {
            if (\Auth::user()->userable->teacherable->organiki == 2000) {
                $organiki = 'Νομός ' . \Config::get('requests.county')[\Auth::user()->userable->teacherable->county];
                return $organiki;
            } elseif (\Auth::user()->userable->teacherable->organiki == 1000) {
                $organiki = 'Διάθεση';
                return $organiki;
            }
            return $organiki;
        } else {
            $organiki = $organiki->name;
            return $organiki;
        }
    }

    /**
     * @return mixed
     */
    private function findEidikotita()
    {
//        $eidikotita = Eidikotita::find(\Auth::user()->userable->klados_id)->full_name;
        $new = NewEidikotita::find(Auth::user()->userable->myschool->new_eidikotita);

        return $new->eidikotita_slug . ' ('. $new->eidikotita_name . ')';
    }

    /**
     * @param $selectedSchools
     * @param $new_request
     */
    private function createOrganikesPrefrences($selectedSchools, $new_request)
    {
        $order_number = 1;
        foreach ($selectedSchools as $school) {
            $prefrences = OrganikiPrefrences::create([
                'request_id' => $new_request->id,
                'school_id' => $school['id'],
                'order_number' => $order_number
            ]);
            $order_number = $order_number + 1;
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    private function saveOrganikiRequest(Request $request)
    {
        $selectedSchools = $request->get('selectedSchools');

        DB::beginTransaction();

        $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
            ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
            ->where('aitisi_type', 'Ονομαστικά Υπεράριθμος')
            ->first();

        if ($request == null) {
            $unique_id = uniqid(Auth::id());

            $new_request = OrganikiRequest::create([
                'unique_id' => $unique_id,
                'teacher_id' => $this->user->userable->id,
                'school_organiki' => \Auth::user()->userable->myschool->organiki_name,
                'aitisi_type' => 'Ονομαστικά Υπεράριθμος',
                'date_request' => Carbon::now(),
                'situation' => 'saved'
            ]);

            $this->createOrganikesPrefrences($selectedSchools, $new_request);
            $message = 'Η Αίτηση σας αποθηκεύτηκε με επιτυχία.';
        } else {
            OrganikiPrefrences::where('request_id', $request->id)->delete();
            $this->createOrganikesPrefrences($selectedSchools, $request);
            $request->updated_at = Carbon::now();
            $request->save();
            $message = 'Η Αίτηση σας ενημερώθηκε με επιτυχία.';
        }

        DB::commit();
        return $message;
    }

    private function saveOrganikiRequestVeltiwsi(Request $request)
    {
        $selectedSchools = $request->get('selectedSchools');

        DB::beginTransaction();

        $request = OrganikiRequest::where('teacher_id', $this->user->userable->id)
//            ->where(DB::raw('YEAR(date_request)'), Carbon::now()->year)
            ->where(DB::raw('TIMEDIFF("2018-05-24 18:00:00", date_request)'), '<', 0)
            ->where('aitisi_type', 'Βελτιώση - Οριστική τοποθέτηση')
            ->first();

        if ($request == null) {
            $unique_id = uniqid(Auth::id());

            $new_request = OrganikiRequest::create([
                'unique_id' => $unique_id,
                'teacher_id' => $this->user->userable->id,
                'school_organiki' => \Auth::user()->userable->myschool->organiki_name,
                'aitisi_type' => 'Βελτιώση - Οριστική τοποθέτηση',
                'date_request' => Carbon::now(),
                'situation' => 'saved'
            ]);

            $this->createOrganikesPrefrences($selectedSchools, $new_request);
            $message = 'Η Αίτηση σας αποθηκεύτηκε με επιτυχία.';
        } else {
            OrganikiPrefrences::where('request_id', $request->id)->delete();
            $this->createOrganikesPrefrences($selectedSchools, $request);
            $request->updated_at = Carbon::now();
            $request->save();
            $message = 'Η Αίτηση σας ενημερώθηκε με επιτυχία.';
        }

        DB::commit();
        return $message;
    }


}
