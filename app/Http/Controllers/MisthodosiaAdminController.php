<?php

namespace App\Http\Controllers;

use App\Misthodosia;
use App\MisthodosiaFake;
use App\MySchoolTeacher;
use App\Tekna;
use App\TeknaFake;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Notification;
use App\Teacher;
use Illuminate\Contracts\Auth\Guard;
use App\Http\Requests\MisthodosiaRequest;
use Smail;
use App\Events\TeacherChangeMisthodosiaNotification;
use App\Events\MisthodosiaSendNotificationToTeacher;
use Carbon\Carbon;

class MisthodosiaAdminController extends Controller
{

    public function __construct(Guard $auth)
    {
        $this->middleware('misthodosia');
    }

    public function excelMisthodosia()
    {
        $employees = MySchoolTeacher::with(['teacher', 'misthodosia'])
            ->get()
            ->filter(function($e){
                if($e->misthodosia != null) return true;
            })->values();

        \Excel::create('ΜΙΣΘΟΔΟΣΙΑ'.Carbon::now()->format('_Ymdhi_'), function($excel) use($employees){
                    $excel->sheet('Εργαζόμενοι', function($sheet) use($employees){
                        $sheet->loadView('misthodosia.excelMisthodosia')
                            ->setFontFamily('Arial')
                            ->setFontSize(13)
                            ->setFontBold(false)
                            ->with(compact('employees'));

                        $sheet->setPageMargin(array(
                            0.55, 0.25, 0.25, 0.25
                        ));

                        $sheet->setFreeze('E2');
                    });
        })->download('xls');

        return 'ok';
    }

    public function allErgazomenoi()
    {
        $employees = MySchoolTeacher::with('misthodosia')
            ->get()
            ->filter(function($e){
                if($e->misthodosia != null) return true;
            })->values();

        $sum = $employees->count();

        $header = 'Διαχείριση όλων των Εργαζομένων';

        return view('misthodosia.all_employees', compact('employees', 'sum', 'header'));
    }

    public function allWithAccount()
    {
        $employees = MySchoolTeacher::with(['teacher', 'misthodosia'])
            ->get()
            ->filter(function($e){
                if($e->teacher != null && $e->misthodosia != null) return true;
            })->values();

        $sum = $employees->count();

        $header = 'Διαχείριση εργαζομένων με λογαριασμό στην ΠΑΣΙΦΑΗ';

        return view('misthodosia.all_employees', compact('employees', 'sum', 'header'));
    }

    public function notChecked()
    {
        $employees = MySchoolTeacher::with(['teacher' => function($q){
            $q->where('misthodosia_is_checked', false);
        }, 'misthodosia'])
            ->get()
            ->filter(function($e){
                if($e->teacher != null && $e->misthodosia != null) return true;
            })->values();

        $sum = $employees->count();

        $header = 'για Έλεγχο';

        return view('misthodosia.all_employees', compact('employees', 'sum', 'header'));
    }

    public function allWithOutAccount()
    {
        $employees = MySchoolTeacher::with(['teacher', 'misthodosia'])
            ->get()
            ->filter(function($e){
                if($e->teacher == null && $e->misthodosia != null) return true;
            })->values();

        $sum = $employees->count();

        $header = 'Διαχείριση εργαζομένων ΧΩΡΙΣ λογαριασμό';

        return view('misthodosia.all_employees', compact('employees', 'sum', 'header'));
    }

    public function checkMisthodosia($afm, Request $request)
    {
        $action = $request->get('action');
        if ($action != null){
            Notification::where('uniqueAction', $action)
                ->update([
                    'unread' => 'is_false'
                ]);
        }
        $myschoolProfile =  MySchoolTeacher::findOrFail($afm);

        if($myschoolProfile->teacher != null){
            $teacher = $myschoolProfile->teacher;

            $hasFakeMisthodosia = $teacher->fake_misthodosia == null ? false : true;
        }else{
            $teacher = null;

            $hasFakeMisthodosia = false;
        }


        return view('misthodosia.checkTeacherMisthodosia', compact('teacher', 'myschoolProfile', 'hasFakeMisthodosia'));
    }

    public function unlock($afm)
    {
        $misthodosia = Misthodosia::where('afm', $afm)->firstOrFail();

        \DB::beginTransaction();
        $fakeMisthodosia = MisthodosiaFake::create([
            'at'    => $misthodosia->at,
            'birth' => $misthodosia->birth,
            'family_situation'  => $misthodosia->family_situation,
            'iban'              => $misthodosia->iban,
            'bank'              => $misthodosia->bank,
            'amka'              => $misthodosia->amka,
            'doy'               => $misthodosia->doy,
            'am_tsmede'         => $misthodosia->am_tsmede,
            'am_ika'            => $misthodosia->am_ika,
            'new'               => $misthodosia->new,
            'mk'                => $misthodosia->mk,
            'teacher_id'        => $misthodosia->myschool->teacher->id
        ]);

        foreach($misthodosia->tekna as $tekno){
            $fakeTekna = TeknaFake::create([
                'birth' => $tekno->birth,
                'college_start_date' => $tekno->college_start_date,
                'college_end_date'  => $tekno->college_end_date,
                'description'       => $tekno->description,
                'misthodosia_id'    => $fakeMisthodosia->fmid
            ]);
        }

        $misthodosia->myschool->teacher->update([
            'fmid' => $fakeMisthodosia->fmid,
            'misthodosia_is_checked'    => false,
            'request_to_change_misthodosia' => false
        ]);
        \DB::commit();

        if(\Config::get('requests.sent_mail')) {
            Smail::sentTeacherMisthodosia($misthodosia->myschool->teacher, 'ΞΕΚΛΕΙΔΩΜΑ Καρτέλας Μισθοδοσίας', [
                'type' => 'unlockMisthodosia',
                'full_name' => $misthodosia->myschool->full_name,
                'afm'       => $misthodosia->afm
            ]);
        }
        event(new MisthodosiaSendNotificationToTeacher($misthodosia->myschool->teacher,[
            'url' => 'MisthodosiaController@getMisthodosia',
            'title' => 'ΞΕΚΛΕΙΔΩΜΑ ΜΙΣΘΟΔΟΣΙΑΣ',
            'description' => "Τα στοιχεία στην καρτέλα Μισθοδοσίας <span class='notification_full_name'>είναι επεξεργάσιμα</span>. Μπορείς να πατήσεις εδώ για να πραγματοποιήσεις τις αλλαγές",
            'type' => 'danger'
        ]));

        flash()->success('', 'Η καρτέλα μισθοδοσίας ξεκλειδώθηκε');

        return redirect()->back();
    }

    public function noChanges($afm)
    {
        $mySchoolProfile = MySchoolTeacher::findOrFail($afm);

        $mySchoolProfile->teacher->update([
            'fmid'  => null,
            'misthodosia_is_checked'    => true,
            'request_to_change_misthodosia' => false,
            'misthodosia_new_request'   => false,
            'misthodosia_counter'   => 0
        ]);

        event(new MisthodosiaSendNotificationToTeacher($mySchoolProfile->teacher,[
            'url' => 'MisthodosiaController@getMisthodosia',
            'title' => 'Ακύρωση Αιτήματος',
            'description' => "Το τμήμα Μισθοδοσίας <span class='notification_full_name'>απέρριψε το αίτημά σας</span>για αλλαγή των στοιχείων Μισθοδοσίας διότι είναι ορθά",
            'type' => 'warning'
        ]));

        flash()->info('Καμία αλλαγή', 'Ο εργαζόμενος δεν μπορεί να αλλάξει τα στοιχεία της Μισθοδοσίας.');

        return redirect()->back();
    }

    public function confirmTeacherMisthodosia($afm, MisthodosiaRequest $request)
    {
        $mySchoolProfile = MySchoolTeacher::findOrFail($afm);

        $this->updateMisthodosiaProfile($mySchoolProfile->misthodosia, $request);

        if($mySchoolProfile->teacher->fmid != null){
            $mySchoolProfile->teacher->fake_misthodosia->delete();

            $mySchoolProfile->teacher->update([
                'fmid'  => null,
                'misthodosia_is_checked'    => true,
                'request_to_change_misthodosia' => false,
                'misthodosia_new_request'   => false,
                'misthodosia_counter'   => 0
            ]);
        }

        flash()->success('Ελέγχθηκε', 'Ο έλεγχος των στοιχείων Μισθοδοσίας ολοκληρώθηκε');

            if(\Config::get('requests.sent_mail')) {
                Smail::sentTeacherConfirmMisthodosia($mySchoolProfile->teacher, 'Ολοκλήρωση ελέγχου στοιχείων Μισθοδοσίας', [
                    'type' => 'confirm',
                    'full_name' => $mySchoolProfile->teacher->user->full_name,
                    'afm'       => $mySchoolProfile->afm
                ]);
            }
            event(new MisthodosiaSendNotificationToTeacher($mySchoolProfile->teacher,[
                'url' => 'MisthodosiaController@getMisthodosia',
                'title' => 'Ολοκλήρωση ελέγχου Μισθοδοσίας',
                'description' => "Έπειτα από έλεγχο της υπηρεσίας <span class='notification_full_name'>διορθώθηκαν</span> τα στοιχεία της Μισθδοδοσίας σου.",
                'type' => 'success'
            ]));

        return redirect()->route('Misthodosia::checkMisthodosia', $afm);

    }

    public function lockWithOutChanges($afm)
    {
        $mySchoolProfile = MySchoolTeacher::findOrFail($afm);

        if($mySchoolProfile->teacher->fmid != null){
            $m = MisthodosiaFake::findOrFail($mySchoolProfile->teacher->fmid);

            $m->delete();
        }

        $mySchoolProfile->teacher->update([
            'fmid'  => null,
            'misthodosia_is_checked'    => true,
            'request_to_change_misthodosia' => false,
            'misthodosia_new_request'   => false,
            'misthodosia_counter'   => 0
        ]);

        event(new MisthodosiaSendNotificationToTeacher($mySchoolProfile->teacher,[
            'url' => 'MisthodosiaController@getMisthodosia',
            'title' => 'Καμία αλλαγή',
            'description' => "Το τμήμα Μισθοδοσίας <span class='notification_full_name'>ΔΕΝ</span> πραγματοποίησε καμία αλλαγή - με βάση τις προτάσεις σας για τα στοιχεία μισθοδοσίας - <span class='notification_full_name'>διότι είναι ορθά.</span>",
            'type' => 'warning'
        ]));

        flash()->info('Κλείδωμα χωρίς αλλαγές');

        return redirect()->back();

    }

    public function updateMisthodosiaDetails(MisthodosiaRequest $request)
    {
        $misthodosia = Misthodosia::where('afm', $request->get('afm'))->firstOrFail();

        $this->updateMisthodosiaProfile($misthodosia, $request);

        flash()->success('','Η ενημέρωση έγινε με επιτυχία');

        return redirect()->back();
    }

    /**
     * @param MisthodosiaRequest $request
     */
    private function updateMisthodosiaProfile(Misthodosia $misthodosia, MisthodosiaRequest $request)
    {
        $misthodosia->update($request->all());

        Tekna::where('misthodosia_id', $misthodosia->id)->delete();

        if (count($request->get('tekna')) > 0) {
            foreach ($request->get('tekna') as $tekna) {
                Tekna::create([
                    'birth' => $tekna['birth'],
                    'college_start_date' => $tekna['StartCollege'],
                    'college_end_date' => $tekna['EndCollege'],
                    'description' => $tekna['description'],
                    'misthodosia_id' => $misthodosia->id
                ]);
            }
        }
    }

}
