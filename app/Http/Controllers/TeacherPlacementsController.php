<?php

namespace App\Http\Controllers;

use App\Http\Controllers\traits\PlacementsTrait;
use App\Placement;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Gate;
use App\Teacher;
use App\Praxi;
use Carbon\Carbon;

class TeacherPlacementsController extends Controller
{
    use PlacementsTrait;

    public function  __construct()
    {
        $this->middleware('role:teacher');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->denies('teacher_view_placements');

        return $this->getIndexPlacementsOfTeacher();
    }

    public function getAllTeacherPlacements(){
        $this->denies('teacher_view_placements');

        $this->allPlacementsPDF();
    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }

}
