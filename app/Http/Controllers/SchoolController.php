<?php

namespace App\Http\Controllers;

use App\Events\SchoolConfirmToPysde;
use App\Events\SchoolSaveChanges;
use App\Http\Controllers\traits\NotificationTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\SchoolRequest;
use App\School;
use Auth;
use Gate;
use Mail;
use Illuminate\Auth\Guard;
use App\Http\Controllers\Controller;
use App\Repositories\School\SchoolRepository;
use App\Notification;
use App\Sch_gr\Facade\Smail;
class SchoolController extends Controller
{
    use NotificationTrait;
    protected $repo;

    public function __construct(SchoolRepository $schoolRepo)
    {
        // Auth::loginUsingId(16);  //23->4 Γ/σιο  42->1-ΓΕΛ-ΧΑΝΙΩΝ | 57->gia epal  |  16->gia gymnasio  | 62-> επαλ καντάνου
        $this->middleware('role:school');
        $this->repo = $schoolRepo;
    }
    

    public function show(School $school)
    {
        if (Gate::allows('isMySchool', $school)){
            $this->denies('read_school_kena');
            
                $eidikotites =  $this->repo->eidikotitesNotZero($school);
                $parallili = $this->repo->paralliliNotZero($school);
                $mathimata = $this->repo->mathimataNotZero($school);
                $eidikis = $this->repo->eidikisNotZero($school);

                $schools = [];
                $name = $school->name;
                $last_update = $school->last_update;
                return view('school.show', compact('schools','name', 'last_update', 'eidikotites', 'parallili','mathimata','eidikis'));
        }
        abort(403);
    }

    public function printShow(School $school)
    {
        if (Gate::allows('isMySchool', $school)){
            $this->denies('read_school_kena');

            $eidikotites =  $this->repo->eidikotitesNotZero($school);
            $parallili = $this->repo->paralliliNotZero($school);
            $mathimata = $this->repo->mathimataNotZero($school);
            $eidikis = $this->repo->eidikisNotZero($school);

            $name = $school->name;
            $last_update = $school->last_update;
            return view('school.printShow', compact('name', 'last_update', 'eidikotites', 'parallili','mathimata','eidikis'));
        }
        abort(403);
    }
    
    public function edit(School $school, Request $request)
    {
        if (Gate::allows('isMySchool', $school)){
            if (Gate::allows('edit_school_kena')){
                $this->makeNotificationRead($request->get('action'));
                return view('school.edit',  compact('school'));
            }
            return redirect()->action('SchoolController@show', str_replace(' ','-',$school->name));
        }
        abort(403);
    }
    
    public function update(SchoolRequest $request, School $school)
    {
        $this->schoolDenyAccess($school, 'edit_school_kena');

        $old_description = $school->description;

        $this->repo->saveValueOfEidikotita($request, $school);

        $date_request = Carbon::now();
        $uniqueAction = md5(\Auth::id(). date_timestamp_get($date_request));

        $this->confirmation($school,'internal');

        event(new SchoolSaveChanges($school, [
        'title' => 'Ενημέρωση Κενών-Πλεονασμάτων',
        'description' => "Το <span class='notification_full_name'>&laquo;{$school->name}&raquo;</span> ενημέρωσε με επιτυχία τα κενά και τα πλεονάσματα των καθηγητών του",
        'type' => 'warning'
    ]));

        $new_eidikotites =  $school->eidikotites->filter(function($school){
            if ($school->pivot->value != 0){
                return true;
            }
        });

        $officer = $school->user->full_name;

        if(\Config::get('requests.sent_mail')){
            Smail::sentConfirmationToSchool($school);
            Smail::sentSchoolDescriptionToPysde($school, $old_description , $request->get('description'));
        }

        return redirect()->back();
    }

    public function confirmation(School $school, $action = 'not_internal')
    {
        if (Gate::allows('isMySchool', $school)){
            $this->updateConfirmationDate();
            if ($action == 'internal'){
                return true;
            }
            event(new SchoolConfirmToPysde($school,[
                'title' => 'Επικαιροποίηση Σχολικής Μονάδας',
                'description' => "Το <span class='notification_full_name'>&laquo;{$school->name}&raquo;</span> έκανε επιβεβαίωση εισόδου στην ηλεκτρονική πλατφόρμα",
                'type' => 'default'
            ]));
            flash()->success('Συγχαρητήρια', 'η επικαιροποίηση έγινε με επιτυχία');
            return redirect()->back();
        }
        abort(403);
    }
    
    private function denies($permision)
    {

        if (Gate::denies($permision)){
            abort(403);
        }
    }  
    
    private function schoolDenyAccess(School $school, $permision)
    {
        if (Gate::denies('isMySchool', $school)){
            abort(403);
        }
        $this->denies($permision);
    }

    /**
     * @param $date_request
     */
    private function updateConfirmationDate()
    {
        Auth::user()->userable->update([
            'confirmation_date' => Carbon::now()
        ]);
    }

}

