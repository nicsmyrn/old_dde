<?php

namespace App\Http\Controllers\KTEL\traits;


use Carbon\Carbon;
use App\Sch_gr\Facade\Smail;

trait OutputKtelTrait {


    public function periodRouteToPDF($attr)
    {
        $school_folder = str_slug($attr['school']->name);

        $file_path = $school_folder . '/' . $attr['year'] . '-' . str_slug($attr['month']) . '.pdf';
        \Storage::makeDirectory('schools/'.$school_folder);

        $FirstDate = $this->getFirstWorkDateOfMonth($attr);

        $pdf = \PDF::loadView('KTEL.PDF.period-routes',compact('attr', 'FirstDate'));
        $pdf->setOrientation('landscape')->save(storage_path('app/schools/'.$file_path));

        return $pdf->setOrientation('landscape')
            ->stream();
    }

    /**
     * @param $attr
     * @return string
     */
    private function getFirstWorkDateOfMonth($attr)
    {
        $year_period = $attr['year'];
        $month = $attr['month_number'];
        if($month != 12){
            $month += 1;
        }
        $years = explode("-", $year_period);

        if (in_array(intval($month), [9, 10, 11, 12])) $current_year = $years[0];
        else $current_year = $years[1];

        $date = Carbon::parse($current_year . '-' . $month);

        $date = $date->firstOfMonth();

        $numberOfFirstDayOfMonth = $date->dayOfWeek;

        if (in_array($numberOfFirstDayOfMonth, [0, 6])) {
            $FirstDate = $date->firstOfMonth(1)->format('d/m/Y');
            return $FirstDate;
        } else {
            $FirstDate = $date->format('d/m/Y');
            return $FirstDate;
        }
    }
}