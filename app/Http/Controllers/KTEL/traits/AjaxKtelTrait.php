<?php

namespace App\Http\Controllers\KTEL\traits;


use Illuminate\Http\Request;
use App\Models\KTEL\Busroute;
use App\Models\KTEL\Year;


trait AjaxKtelTrait {

    public function getAllYears(Request $request)
    {
        $this->checkAjaxCall($request);

        return Year::with('months')->get();
    }

    public function allMunicipalities(Request $request)
    {
        $this->checkAjaxCall($request);

        return \Config::get('requests.dimos');
    }

    public function getRouteTypes(Request $request)
    {
        $this->checkAjaxCall($request);

        return \Config::get('ktel.routetype');
    }

    private function checkAjaxCall(Request $request)
    {
        if ($request->ajax()) return true;
        else abort(403);
    }

    public function getBusRoute($m_id, Request $request)
    {
        $this->checkAjaxCall($request);

        return Busroute::where('municipality_id', $m_id)->get();
    }

}