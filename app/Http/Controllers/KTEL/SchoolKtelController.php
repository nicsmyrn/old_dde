<?php

namespace App\Http\Controllers\KTEL;

use App\Http\Controllers\KTEL\traits\OutputKtelTrait;
use App\Models\KTEL\Busroute;
use App\Models\KTEL\Comment;
use App\Models\KTEL\Month;
use App\Models\KTEL\Period;
use App\Models\KTEL\PeriodRoute;
use App\Models\KTEL\Year;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\School;
use App\Models\KTEL\SchoolRoute;


use App\Http\Requests;

class SchoolKtelController extends Controller
{
    use OutputKtelTrait;

    private $school;

     public function __construct()
     {
         $this->middleware('role:school');
     }

    public function listPdf()
    {
        $this->school = \Auth::user()->userable;

        $files =  \Storage::files('schools/'.str_slug($this->school->name).'/ktel');

        $arrayFiles = array();
        foreach ($files as $file)
        {
            $arrayFiles[] =  str_replace('schools/'.str_slug($this->school->name).'/ktel/', '', $file);
        }

        $fileCollection = collect($arrayFiles);

        return view('KTEL.schools.pdf-index', compact('fileCollection'));
    }

    public function downloadPDF($file_name)
    {
        $this->school = \Auth::user()->userable;

        $school_folder = str_slug($this->school->name);

        $file = $file_name;
        $file_path = storage_path('app/schools/'.$school_folder).'/ktel/'.$file;

        if (file_exists($file_path)){
            return response()->download($file_path, $file, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');
        }
    }

    public function showPDFperiodRoute(Year $year, Month $month)
    {
        $this->school = \Auth::user()->userable;

        $period =  Period::where('year_id', $year->id)->where('month_id', $month->id)->first();

        $p =  PeriodRoute::with(['schoolroute' => function($q){
            $q->where('school_id',$this->school->id);                   // *********** CHANGE --> SCHOOL ID AUTH
        }])->where('period_id', $period->id)->get()
            ->filter(function($periodroute){
                if($periodroute->schoolroute != null){
                    return true;
                }
            });

        $c = Comment::where('school_id', $this->school->id)
                ->where('period_id', $period->id)
                ->where('disabled', true)
                ->get();

//        return $p->max('routes_number');

        $attr = [
            'year'  => $year->name,
            'month' => $month->name,
            'month_number' => $month->number,
            'periodRouteCollection' => $p,
            'commentsCollection'  => $c,
            'school'    => $this->school,
            'full_name'     => $this->school->user->full_name,
            'sex'       => $this->school->sex,
            'work_phone'    => $this->school->work_phone,
            'mobile_phone'  => $this->school->mobile_phone,
            'area'          => $this->school->area
        ];

        if ($attr['periodRouteCollection']->isEmpty()){
            abort(404);
        }
        return $this->periodRouteToPDF($attr);
    }

    public function chooseRoutes()
    {
        $this->school = \Auth::user()->userable;

        $municipalityId = $this->school->municipality_id;

        return view('KTEL.schools.choose-routes', compact('municipalityId'));
    }

    public function postChosenRoutes(Request $request)
    {
        $this->school = \Auth::user()->userable;

        \DB::beginTransaction();

        if($this->school->municipality_id != $request->get('municipality')){
            $this->school->update(['municipality_id' => $request->get('municipality')]);
        }

        $routeCollection = Busroute::where('municipality_id', $this->school->municipality_id)->get(['id']);


        foreach ($routeCollection as $route){
            $first = SchoolRoute::where('school_id', $this->school->id)->where('route_id', $route->id)->first();
            if ($first != null){
                $first->update([
                    'checked'   => array_has($request->get('routes'), $route->id) ? true : false,
                    'locked'    => true
                ]);
            }else{
                SchoolRoute::create([
                    'school_id' => $this->school->id,
                    'route_id'  => $route->id,
                    'checked'   => array_has($request->get('routes'), $route->id) ? true : false,
                    'locked'    => true
                ]);
            }
        }

        \DB::commit();

        flash()->success('Συγχαρητήρια', 'Τα δρομολόγια της Σχολικής Μονάδας ενημερώθηκαν με επιτυχία...');


        return redirect()->back();
    }

    public function editRoutePeriods()
    {
        $this->school = \Auth::user()->userable;

        if(!in_array($this->school->sex,[0,1]) || $this->school->area == null || $this->school->work_phone == null || $this->school->mobile_phone == null){
            flash()->overlayE('Προσοχή!', 'Για να εισέλθετε στη Διαχείριση Δρομολογίων πρέπει πρώτα να συμπληρώσετε το Προφίλ του Σχολείου');
            return redirect()->route('User::getProfile');
        }

        return view('KTEL.schools.edit_period_of_routes');
    }
}
