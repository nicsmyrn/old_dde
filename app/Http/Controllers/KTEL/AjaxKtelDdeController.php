<?php

namespace App\Http\Controllers\KTEL;

use App\Http\Controllers\KTEL\traits\AjaxKtelTrait;
use App\Models\KTEL\Apousiologio;
use App\Models\KTEL\Period;
use App\Models\KTEL\PeriodRoute;
use App\School;
use App\Models\KTEL\SchoolRoute;
use App\Models\KTEL\Year;
use App\Models\KTEL\Busroute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PeriodRouteRequest;
use App\Http\Requests\ApousiologioRequest;

use App\Http\Requests;

class AjaxKtelDdeController extends Controller
{
    use AjaxKtelTrait;

    public function getPeriodCollection(Request $request)
    {
        $this->checkAjaxCall($request);

        return Year::with('months')->get();
    }

    public function deleteRoute(Busroute $route, Request $request)
    {
        $this->checkAjaxCall($request);

        $route->delete();
    }

    public function updateBusRoute(Request $request)
    {
        $this->checkAjaxCall($request);

        Busroute::find($request->get('id'))->update($request->all());
    }

    public function addRoute(Request $request)
    {
        $this->checkAjaxCall($request);

        $route = Busroute::create($request->all() + [
                'name_type' => \Config::get('ktel.routetype')[$request->get('routetype_id')]
            ]);
//        $route->type;
        return $route;
    }

    public function changePermission(Request $request)
    {
        $this->checkAjaxCall($request);

        $period = Period::find($request->get('id'));


        $period->update([
            'disabled'  => !$period->disabled
        ]);

        return $period;
    }

}
