<?php

namespace App\Http\Controllers\KTEL;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Http\Requests;
use Gate;

class AdminKtelController extends Controller
{
    public function __construct()
    {
//        \Auth::loginUsingId(15);
        $this->middleware('auth');
    }
    public function routesAdmin()
    {
        $this->denies('ktel_routes');

        return view('KTEL.dde.routes-controller');
    }

    public function managePeriod()
    {
        $this->denies('ktel_period');

        return view('KTEL.dde.period-manager');
    }

    public function statistics()
    {
        return view('KTEL.dde.statistics');
    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(401);
        }
    }
}
