<?php

namespace App\Http\Controllers\KTEL;

use App\Http\Controllers\KTEL\traits\AjaxKtelTrait;
use App\Http\Controllers\KTEL\traits\OutputKtelTrait;
use App\Models\KTEL\Apousiologio;
use App\Models\KTEL\Comment;
use App\Models\KTEL\Month;
use App\Models\KTEL\Period;
use App\Models\KTEL\PeriodRoute;
use App\School;
use App\Models\KTEL\SchoolRoute;
use App\Models\KTEL\Year;
use App\Models\KTEL\Busroute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\PeriodRouteRequest;
use App\Http\Requests\ApousiologioRequest;

use App\Http\Requests;
use App\Sch_gr\Facade\Smail;

class AjaxKtelSchoolController extends Controller
{
    use AjaxKtelTrait, OutputKtelTrait;

    private $school;

    public function __construct()
    {
        $this->school = \Auth::user()->userable;
        $this->middleware('role:school');
    }

    public function getPeriodRoutes($periodId, Request $request)
    {
        $this->checkAjaxCall($request);
        $periodId =  intval($periodId);

        $period = Period::find($periodId);
        $school = $this->school;

        $data = array();
        $data['periodRoutes'] = array();
        $oneEnableFLAG = false;
        $isLocked = true;

        foreach ($school->routes as $route){
            if (!$route->period->where('period_id', $periodId)->isEmpty()){
                $isLocked = false;
                if(!$route->period->where('period_id', $periodId)->first()->disabled) $oneEnableFLAG = true;
                $data['periodRoutes'][] = [
                    'id'                => $route->period->where('period_id', $periodId)->first()->id,
                    'schoolroute_id'    => $route->period->where('period_id', $periodId)->first()->schoolroute_id,
                    'period_id'         => $route->period->where('period_id', $periodId)->first()->period_id,
                    'kids_number'       => $route->period->where('period_id', $periodId)->first()->kids_number,
                    'routes_number'     => $route->period->where('period_id', $periodId)->first()->routes_number,
                    'description'       => $route->period->where('period_id', $periodId)->first()->description,
                    'disabled'          => $route->period->where('period_id', $periodId)->first()->disabled,
                    'name'              => $route->period->where('period_id', $periodId)->first()->route_name,
                    'type'              => $route->period->where('period_id', $periodId)->first()->route_type,
                    'contract'          => $route->period->where('period_id', $periodId)->first()->route_contract,
                    'starts_at'         => $route->period->where('period_id', $periodId)->first()->starts_at,
                    'new'               => $route->period->where('period_id', $periodId)->first()->new,
                ];
            }
        }

        $data['comments'] = Comment::where('period_id', $periodId)->where('school_id',$school->id)->get();

        if(!$oneEnableFLAG && !$isLocked) $data['url'] = action('KTEL\SchoolKtelController@showPDFperiodRoute', [
                                                                $period->year,
                                                                $period->month
                                                            ]);
        else $data['url'] = '';

        return $data;
    }

    public function insertPeriodRoutes(PeriodRouteRequest $request)
    {
        $this->checkAjaxCall($request);

        $data = array();
        $data['periodCollection'] = array();
        $data['comments']   = array();

        foreach($request->get('periodCollection') as $record){
            $model_instance = PeriodRoute::create($record);
            $data['periodCollection'][] = collect($model_instance)
                        ->put('type', $model_instance->route_type)
                        ->put('name', $model_instance->route_name)
                        ->put('contract', $model_instance->route_contract);
        }

        foreach($request->get('comments') as $comment){
            $comment_instance = Comment::create([
                'school_id'     => $comment['school_id'],
                'period_id'     => $comment['period_id'],
                'disabled'      => $comment['disabled'],
                'comment'      => $comment['comment']
            ]);
            $data['comments'][] = collect($comment_instance);
        }

        return $data;
    }

    public function updatePeriodRoutes(PeriodRouteRequest $request)
    {
        $this->checkAjaxCall($request);


        foreach($request->get('periodCollection') as $record){
            PeriodRoute::find($record['id'])->update($record);
        }

//        $school_id = last($request->get('comments'))['school_id'];
//        $period_id = last($request->get('comments'))['period_id'];
        $year = Year::find($request->get('yearId'));
        $month = Month::find($request->get('monthId'));
        $periodId = $request->get('periodId');

        Comment::where('school_id', $this->school->id)->where('period_id', $periodId)->delete();
        foreach($request->get('comments') as $comment){
            Comment::create([
                'school_id'     => $comment['school_id'],
                'period_id'     => $comment['period_id'],
                'disabled'      => $comment['disabled'],
                'comment'       => $comment['comment']
            ]);
        }

        if ($request->get('createPDF') == 'canCreatePDF'){
            $p =  PeriodRoute::with(['schoolroute' => function($q){
                $q->where('school_id',$this->school->id);                   // *********** CHANGE --> SCHOOL ID AUTH
            }])->where('period_id', $periodId)->get()
                ->filter(function($periodroute){
                    if($periodroute->schoolroute != null){
                        return true;
                    }
                });

            $c = Comment::where('school_id', $this->school->id)
                ->where('period_id', $periodId)
                ->where('disabled', true)
                ->get();


            $attr = [
                'year'  => $year->name,
                'month' => $month->name,
                'month_number' => $month->number,
                'periodRouteCollection' => $p,
                'commentsCollection'  => $c,
                'school'    => $this->school,
                'full_name'     => $this->school->user->full_name,
                'sex'       => $this->school->sex,
                'work_phone'    => $this->school->work_phone,
                'mobile_phone'  => $this->school->mobile_phone,
                'area'          => $this->school->area
            ];

            $school_folder = str_slug($attr['school']->name) . '/ktel/';

            $file_path = $school_folder . $attr['year'] . '_' . str_slug($attr['month']) . '.pdf';

            \Storage::makeDirectory('schools/'.$school_folder);

            $FirstDate = $this->getFirstWorkDateOfMonth($attr);

            $pdf = \PDF::loadView('KTEL.PDF.period-routes',compact('attr', 'FirstDate'));
            $pdf->setOrientation('landscape')->save(storage_path('app/schools/'.$file_path));

            Smail::sentKtelToSchool($attr['school'], $file_path, $attr['month'], $attr['year']);

        }

        $period = Period::find($request->get('periodId'));

        return action('KTEL\SchoolKtelController@showPDFperiodRoute', [
            $period->year,
            $period->month
        ]);
    }

    public function getSpecificAbsences(PeriodRoute $periodRoute, Request $request)
    {
        $data = array();

        $this->checkAjaxCall($request);

        $periodRoute->absences;

        $data['object'] =  $periodRoute;
        $data['nameAttribute'] = $periodRoute->route_name;
        $data['periodAttribute'] = $periodRoute->period_name;

        return $data;
    }

    public function updateAbsences(ApousiologioRequest $request)
    {
        $this->checkAjaxCall($request);

        foreach($request->get('absencesCollection') as $record){
            Apousiologio::find($record['id'])->update($record);
        }

        return 'update success';
    }

    public function insertAbsences(ApousiologioRequest $request)
    {
        $this->checkAjaxCall($request);

        $data = array();

        foreach($request->get('absencesCollection') as $record){
            $data[] = Apousiologio::create($record);
        }

        return $data;
    }

    public function getExistingRoutes(Request $request)
    {
        $school = $this->school;

//        $this->checkAjaxCall($request);
        $busRouteCollection = Busroute::where('municipality_id', $school->municipality_id)->get();

        // χωρίς τα softDeletes

        $collection = \DB::table('ktel_schoolroutes')
            ->where('school_id', $school->id)
            ->whereExists(function ($query) use ($school) {
                $query->select(\DB::raw('*'))
                    ->from('ktel_busroute')
                    ->whereRaw('ktel_busroute.id = ktel_schoolroutes.route_id')
                    ->whereRaw('ktel_busroute.municipality_id = ' . $school->municipality_id);
            })
            ->get(['route_id', 'checked', 'locked']);
//        return is_object(array_first($collection, function($key, $object) use($sss){
//            return $object->route_id == $sss;
//        })) ? 'is set' : 'not set';



        /*       array of 3 objects
                 * checked : 1,
         *          route_id : 17
                 */

        $data = array();

        foreach ($busRouteCollection as $busroute) {
            $data[] = [
                'id' => $busroute->id,
                'checked' => $this->getCheckedValue($collection, $busroute->id),
                'name' => $busroute->name,
                'locked' => $this->getLockedValue($collection, $busroute->id),
                'aa_contract'  => $busroute->aa_contract,
                'routetype_id' => $busroute->routetype_id
            ];
        }

//        $array = array_values(array_sort($array, function ($value) {
//            return $value['name'];
//        }));

        return $data = array_reverse(array_sort($data, function($value){
            return $value['checked'];
        }));
    }

    private function getCheckedValue($array, $searchKey){
        $object = array_first($array, function($key, $object) use($searchKey){
            return $object->route_id == $searchKey;
        });

        return is_object($object) ? $object->checked : 0;
    }

    private function getLockedValue($array, $searchKey)
    {
        $object = array_first($array, function($key, $object) use($searchKey){
            return $object->route_id == $searchKey;
        });

        return is_object($object) ? $object->locked : 0;
    }

    public function getExistingRoutes_ENABLED(Request $request)
    {
        $this->checkAjaxCall($request);

        $collection = \DB::table('ktel_schoolroutes')
            ->where('school_id', $this->school->id)
            ->whereExists(function ($query){
                $query->select(\DB::raw('*'))
                    ->from('ktel_busroute')
                    ->whereRaw('ktel_busroute.id = ktel_schoolroutes.route_id')
                    ->whereRaw('ktel_busroute.municipality_id = ' . $this->school->municipality_id);
            })
            ->where('checked', true)
            ->get(['id', 'route_id']);

        $data = array();

        foreach ($collection as $schoolRouteRecord) {
            $data[] = [
                'schoolroute_id' => $schoolRouteRecord->id,
                'name' => Busroute::find($schoolRouteRecord->route_id)->name,
                'type' => Busroute::find($schoolRouteRecord->route_id)->routetype_id,
                'contract' => Busroute::find($schoolRouteRecord->route_id)->aa_contract
            ];
        }

        return $data;
    }

}
