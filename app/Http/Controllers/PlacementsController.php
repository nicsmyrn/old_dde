<?php

namespace App\Http\Controllers;

use App\Allteacher;
use App\Http\Controllers\traits\PlacementsTrait;
use App\Monimos;
use App\MySchoolTeacher;
use App\NewEidikotita;
use App\Placement;
use App\Sch_gr\Facade\Sch;
use App\User;
use App\Year;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\School;
use App\Eidikotita;
use App\Praxi;
use App\Repositories\Pysde\PlacementRepository;
use App\Teacher;
use DB;
use Gate;

class PlacementsController extends Controller
{
    use PlacementsTrait;
    protected $repo;
    
    public function __construct(PlacementRepository $placementRepo)
    {
//        $this->middleware('pysde');

        $this->repo = $placementRepo;
        // parent::__construct();
    }

    public function allTeachers()
    {
        $this->denies('view_placements');

        $message = 'Καθηγητές με τοποθετήρια';

        $teachers = MySchoolTeacher::whereHas('placements', function($query){
                $query->withTrashed();
        })->get();

        return view('pysde.placements.all_teachers', compact('teachers', 'message'));
    }

    public function teacherPlacementByPraxi($teacher_id, $praxi_id)
    {
        $this->denies('view_placements');

        $teacher = Teacher::with(['placements' => function($q) use ($praxi_id){
            return $q->where('praxi_id', $praxi_id)->withTrashed();
        }])->find($teacher_id);

        $praxi = Praxi::find($praxi_id);

        return view('pysde.placements.teacher_by_praxi', compact('teacher', 'praxi'));
    }

    public function create()
    {
        $this->denies('manage_placements');

        $allSchools = School::all();

//        $eidikotites = $this->repo->notZeroEidikotites();
        $eidikotites = $this->repo->allEidikotitesUnited();

        return view('pysde.placements.topo2',compact('eidikotites', 'allSchools'));
    }

    public function createWithNewEidikotita()
    {
//        $this->denies('manage_placements');

        $allSchools = School::all();

        $eidikotites = $this->repo->allNewKladoi();

        return view('pysde.placements.topo3',compact('eidikotites', 'allSchools'));
    }

    public function manualCreate()
    {
        $this->denies('manage_placements');

//        $teachers = Teacher::with('teacherable')->get();
        $myschool = MySchoolTeacher::all();
        $schools = School::all();
        $year = Year::where('current', true)->first();

        $praxeis = $year->praxeis;

//        return $teachers;

        return view('pysde.placements.manual_insert', compact('myschool', 'schools', 'praxeis'));
    }

    public function saveManualCreate(Request $request)
    {
        $this->denies('manage_placements');

        Placement::create($this->repo->initializeDataCreate($request));

        flash()->overlayS('Συγχαρητήρια', 'Η τοποθέτηση περάστηκε με επιτυχία...');
        return redirect()->back();
    }

    public function store(Request $request)
    {
        return $this->repo->storePlacement($request);
    }

    public function saveWithNewEidikotita(Request $request)
    {
        return $this->repo->storeNewPlacement($request);
    }

    public function updatePlacement($teacher_id, $placement_id)
    {
        $this->denies('manage_placements');

        $placement = Placement::where('afm', $teacher_id)->whereId($placement_id)->withTrashed()->firstOrFail();
//        $praxeis = Praxi::all();
        $praxeis = Praxi::where('praxi_type', 11)
//                            ->where('year_id', Year::where('current', true)->first()->id)
                            ->get();
        $schools = School::all();
        $teacher = MySchoolTeacher::find($teacher_id);

        return view('pysde.placements.edit-teacher-placement', compact('placement','praxeis','schools', 'teacher'));
    }

    public function storePlacement(Request $request, $teacher_afm, $placement_id)
    {
        $this->denies('manage_placements');

        Placement::withTrashed()->find($placement_id)->update($this->repo->initializeDataCreate($request,$teacher_afm));

        flash()->overlayS('Συγχαρητήρια', 'Η τοποθέτηση αποθηκεύτηκε με επιτυχία...');
        return redirect()->action('PlacementsController@placementsDetailsOfTeacher', $teacher_afm);
    }

    public function byPraxi(Request $request)
    {
        $this->denies('view_placements');

        $year_id =  Year::where('current', true)->first()->id;

        $praxi =  Praxi::where('year_id', $year_id)->where('decision_number', $request->get('αριθμός'))->first();

        $praxeis = Praxi::where('year_id', $year_id)->get();

        $message = '';
        $teachers = collect();

        if($praxi != null){
            $teachers = MySchoolTeacher::with(['placements'=> function($query) use($praxi){
                $query->withTrashed()->where('praxi_id', $praxi->id);
            }])
//            ->whereHas('placements', function($query) use($praxi){
//                $query->withTrashed()->where('praxi_id', $praxi->id);  // IN LARAVEL 5.2 Works
//            })
            ->get()
            ->filter(function($q){
                return count($q->placements) > 0;
            })
            ->values();
        }

//        $teacherList =  DB::table('placements')->distinct()->where('praxi_id', $praxiId)->lists('teacherable_id');
//
//        $teachers =  Teacher::find($teacherList);

        return view('pysde.placements.all_teachers_by_praxi', compact('teachers', 'message', 'praxeis'));
    }

    public function allByPraxi($number)
    {
        $this->denies('view_placements');

        $year_id =  Year::where('current', true)->first()->id;

        $praxi = Praxi::where('decision_number', $number)
            ->where('year_id',  $year_id)
            ->first();

        return $this->allPlacementsPDFbyPraxi($praxi);

//        $teachers =  Teacher::with(['placements' => function($query) use ($number){
//            $query->where('praxi_id', $number)
//                    ->withTrashed();
//        }])->whereHas('placements', function($query) use ($number){
//            $query->where('praxi_id', $number)
//                ->withTrashed();
//        })->get();
//
//        foreach($teachers as $teacher){
//            $teacher->aitisis = $this->aitisiTypeF($teacher);
//        }
//
//
//        $pdf = \PDF::loadView('teacher.placements.all_by_praxi',compact('teachers', 'praxi'));
//
//        return $pdf->setOrientation('portrait')->download();
    }

    public function allByTeacher($teacher_id, $year_name)
    {
        $this->denies('view_placements');

        $year_list = array();
        $yearModel = Year::where('name', $year_name)->first();


        if($yearModel != null){
            if($yearModel->current) $year_list[] = $yearModel->id;
        }else{
            $year_list = Year::where('current', false)->lists('id');
        }

        return $this->allPlacementsPDF($teacher_id, $year_list);
    }

    public function allByTeacherByLetter($teacher_first_letter_list, $year_name)
    {
        $this->denies('view_placements');

        $year_list = array();
        $yearModel = Year::where('name', $year_name)->first();
        if($yearModel != null){
            if($yearModel->current) $year_list[] = $yearModel->id;
        }else{
            $year_list = Year::where('current', false)->lists('id');
        }

        return $this->allPlacementsPDFbyLetter($teacher_first_letter_list, $year_list);
    }

    public function placementsDetailsOfTeacher($teacher_id)
    {
        $this->denies('view_placements');

        return $this->getIndexPlacementsOfTeacher($teacher_id);
    }

    public function createPDF($id, $praxiId)
    {
        $this->denies('view_placements');

        $teacher = Teacher::find($id);

        $praxi = Praxi::with(['placements'=> function($query) use ($id){
            $query->where('teacherable_id', $id)
                  ->where('teacherable_type', 'App\Teacher')
                  ->withTrashed();
        }])->where('praxi_type', 11)->find($praxiId);

        $praxi->aitisis = $this->aitisiTypeF($praxi);

        $pdf = \PDF::loadView('teacher.placements.topothetiria',compact(['teacher', 'praxi']));
        return $pdf->setOrientation('portrait')->stream();

//        return view('teacher.placements.topothetiria');
    }

    public function jsonSchools($eidikotita)
    {
        $this->denies('manage_placements');

        return $this->repo->jsonNotZeroSchools($eidikotita);      
    }
    
    public function jsonAllSchools($eidikotita)
    {
        $this->denies('manage_placements');

        $eidikotita = Eidikotita::with(['schools'])->find($eidikotita);
        return $eidikotita->schools;
    }

    public function jsonTemporarySchools()
    {
        $this->denies('manage_placements');

        return School::all();
    }

    public function jsonTeachersOfEidikotita($eidikotita)
    {
        $this->denies('manage_placements');

        $e = $this->getUnitedEidikotitaArray($eidikotita);

//        $teacherCollection =  Teacher::with(['user', 'teacherable'])->whereIn('klados_id', $e)->get();

        $teacherCollection =  Teacher::with('user', 'teacherable')
                                        ->whereIn('klados_id', $e)
                                        ->get()
                                        ->sortBy(function($teacher, $key){
                                            return $teacher['user']['last_name'];
                                        });

        $teacherCollection =  $teacherCollection->values()->all();
//        return $teacherCollection;

        $teachers = collect();

        foreach($teacherCollection as $teacher){
            if($teacher->teacherable_type == 'App\Monimos'){
                $tempCollection = collect([
                    'source'        => 'users',
                    'totalPlacements' => 0,
                    'totalTopothetisis' => 0,
                    'sxesi'         => 'ΜΟΝΙΜΟΣ',
                    'id'            => $teacher->id,
                    'teacherable_type' => 'App\Teacher',
                    'full_name'     => $teacher->user->full_name,
                    'middle_name'   => $teacher->middle_name,
                    'moria'         => $teacher->teacherable->moria,
                    'organiki'      => $teacher->teacherable->organiki_name,
                    'organikiMySchool'  => $teacher->myschool->organiki_name,
                    'organiki_id'   => $teacher->teacherable->organiki,
                    'klados'        => $teacher->klados_name,
                    'entopiotita'   => \Config::get('requests.dimos')[$teacher->dimos_entopiotitas],
                    'sinipiretisi'  => \Config::get('requests.dimos')[$teacher->dimos_sinipiretisis],
                    'special'       => $teacher->special_situation == 1 ? 'ΝΑΙ': 'ΟΧΙ',
                    'ypoxreotiko'   => $teacher->teacherable->orario,
                    'ypoxreotikoMySchool' => $teacher->myschool->ypoxreotiko,
                    'topothetisis'  => Placement::with(['praxi' => function($q){
                        $q->where('year_id', Year::where('current', true)->first()->id);
                    }])
                        ->where('afm', $teacher->myschool->afm)
                        ->get()
                        ->filter(function($p){
                            if($p->praxi != null) return true;
                        })->values()->all(),
                    'afm'           => $teacher->myschool->afm,
                    'placements'    => collect()
                ]);
                if(!empty($tempCollection["topothetisis"])){
                    foreach($tempCollection["topothetisis"] as $topothetisi){
                        $topothetisi->edit_cell = false;
                    }
                }

                $teachers->push($tempCollection);
            }else{
                $tempCollection = collect([
                    'source'        => 'users',
                    'totalPlacements' => 0,
                    'totalTopothetisis' => 0,
                    'sxesi'         => 'ΑΝΑΠΛΗΡΩΤΗΣ',
                    'id'            => $teacher->id,
                    'teacherable_type' => 'App\Teacher',
                    'full_name'     => $teacher->user->full_name,
                    'middle_name'   => $teacher->middle_name,
                    'moria'         => $teacher->teacherable->moria,
                    'organiki'      => 'ΑΝΑΠΛΗΡΩΤΗΣ',
                    'organiki_id'   => 3000,
                    'klados'        => $teacher->klados_name,
                    'entopiotita'   => \Config::get('requests.dimos')[$teacher->dimos_entopiotitas],
                    'sinipiretisi'  => \Config::get('requests.dimos')[$teacher->dimos_sinipiretisis],
                    'special'       => $teacher->special_situation == 1 ? 'ΝΑΙ': 'ΟΧΙ',
                    'ypoxreotiko'   => $teacher->teacherable->orario,
                    'ypoxreotikoMySchool' => $teacher->myschool->ypoxreotiko,
                    'topothetisis'  => Placement::with(['praxi' => function($q){
                        $q->where('year_id', Year::where('current', true)->first()->id);
                    }])
                        ->where('afm', $teacher->myschool->afm)
                        ->get()
                        ->filter(function($p){
                            if($p->praxi != null) return true;
                        })->values()->all(),
                    'afm'           => $teacher->myschool->afm,
                    'placements'    => collect()
                ]);
                if(!empty($tempCollection["topothetisis"])){
                    foreach($tempCollection["topothetisis"] as $topothetisi){
                        $topothetisi->edit_cell = false;
                    }
                }

                $teachers->push($tempCollection);
            }
        }

        return $teachers;
    }

    public function jsonTeachersOfEidikotitaPleonazon($eidikotita)
    {
//        $this->denies('manage_placements');
        $united = Eidikotita::find($eidikotita)->united;

        $e = Eidikotita::with(['schools' => function($q){
            $q->where('value', '>', 0);
        }])->where('united', $united)
            ->get()
            ->filter(function($eidikotita){
                if(count($eidikotita['schools']) > 0) return true;
            });

//        $eidikotites =  $e->lists('id');
//
//return         $schools = $this->getSchoolsPleonazonta($e);

        $teachers = collect();
        foreach($e as $eid){
            foreach($eid->schools as $school){
                $myschoolTeachers = MySchoolTeacher::where('organiki_prosorini', $school->identifier)
                    ->where('eidikotita', $eid->slug_name)
                    ->where('topothetisi', 'Οργανικά')
                    ->get();

                foreach($myschoolTeachers as $teacher){
                    $tempCollection = collect([
                        'source'        => 'pleonazon',
                        'totalPlacements' => 0,
                        'totalTopothetisis' => 0,
                        'sxesi'         => 'ΜΟΝΙΜΟΣ',
                        'id'            => $teacher->afm,
                        'teacherable_type' => 'App\MySchool',
                        'full_name'     => $teacher->last_name . ' ' . $teacher->first_name,
                        'middle_name'   => $teacher->middle_name,
                        'moria'         => '-',
                        'organiki'      => $teacher->organiki_name,
                        'organiki_id'   => $teacher->organiki_id,
                        'klados'        => $teacher->eidikotita,
                        'entopiotita'   => 'Χωρίς στοιχεία',
                        'sinipiretisi'  => 'Χωρίς στοιχεία',
                        'special'       => 'Χωρίς στοιχεία',
                        'ypoxreotiko'   => $teacher->ypoxreotiko,
                        'topothetisis'  => Placement::with(['praxi' => function($q){
                            $q->where('year_id', Year::where('current', true)->first()->id);
                        }])
                            ->where('afm', $teacher->afm)
                            ->get()
                            ->filter(function($p){
                                if($p->praxi != null) return true;
                            })->values()->all(),
                        'afm'           => $teacher->afm,
                        'placements'    => collect(),
                        'pleonazon'     => true
                    ]);

                    if(!empty($tempCollection["topothetisis"])){
                        foreach($tempCollection["topothetisis"] as $topothetisi){
                            $topothetisi->edit_cell = false;
                        }
                    }

                    $teachers->push($tempCollection);
                }
//                $teachers = $teachers->merge();
            }
        }

        return $teachers->sortBy('full_name')->values()->all();
//        return compact('eidikotites', 'schools');

    }

    public function jsonAllTeachersOfNewKladoi($klados_slug)
    {
        $this->denies('manage_placements');

        $eidikotites_list = $this->getNewEidikotitesMySchoolArray($klados_slug);

        $teacherCollection = MySchoolTeacher::whereIn('new_eidikotita', $eidikotites_list)->get();

        $teachers = collect();

        foreach($teacherCollection as $teacher){
            $eidikotita = NewEidikotita::find($teacher->new_eidikotita);

            $pasifaiTeacher = $teacher->teacher;

            if($teacher->am == ''){
                $tempCollection = collect([
                    'source'        => 'myschool',
                    'totalPlacements' => 0,
                    'totalTopothetisis' => 0,
                    'sxesi'         => 'ΑΝΑΠΛΗΡΩΤΗΣ',
                    'id'            => $teacher->afm,
                    'teacherable_type' => 'App\MySchool',
                    'full_name'     => $teacher->last_name . ' ' . $teacher->first_name,
                    'middle_name'   => $teacher->middle_name,
                    'moria'         => '-',
                    'organiki'      => 'ΑΝΑΠΛΗΡΩΤΗΣ',
                    'organiki_id'   => 3000,
                    'klados'        => $teacher->eidikotita,
                    'entopiotita'   => 'Χωρίς στοιχεία',
                    'sinipiretisi'  => 'Χωρίς στοιχεία',
                    'special'       => 'Χωρίς στοιχεία',
                    'ypoxreotiko'   => $teacher->ypoxreotiko,
                    'topothetisis'  => Placement::with(['praxi' => function($q){
                        $q->where('year_id', Year::where('current', true)->first()->id);
                    }])
                        ->where('afm', $teacher->afm)
                        ->get()
                        ->filter(function($p){
                            if($p->praxi != null) return true;
                        })->values()->all(),
                    'afm'           => $teacher->afm,
                    'placements'    => collect()
                ]);
            }else{
                $tempCollection = collect([
                    'source'        => 'myschool',
                    'totalPlacements' => 0,
                    'totalTopothetisis' => 0,
                    'sxesi'         => 'ΜΟΝΙΜΟΣ',
                    'id'            => $teacher->afm,
                    'teacherable_type' => 'App\MySchool',
                    'full_name'     => $teacher->last_name . ' ' . $teacher->first_name,
                    'middle_name'   => $teacher->middle_name,
                    'moria'         => $pasifaiTeacher != null ? $pasifaiTeacher->teacherable->moria : '-',
                    'organiki'      => $teacher->organiki_name,
                    'organiki_id'   => $teacher->organiki_id,
                    'klados'        => $eidikotita->eidikotita_slug . ' ('. $eidikotita->eidikotita_name .')',
                    'klados_id'     => $eidikotita->id,
                    'entopiotita'   => $pasifaiTeacher != null ? \Config::get('requests.dimos')[$pasifaiTeacher->dimos_entopiotitas]: 'Χωρίς στοιχεία',
                    'sinipiretisi'  => $pasifaiTeacher != null ? \Config::get('requests.dimos')[$pasifaiTeacher->dimos_sinipiretisis]: 'Χωρίς στοιχεία',
                    'special'       => $pasifaiTeacher != null ? $pasifaiTeacher->special_situation ? 'NAI' :'OXI' : 'Χωρίς στοιχεία',
                    'ypoxreotiko'   => $teacher->ypoxreotiko,
                    'topothetisis'  => Placement::with(['praxi' => function($q){
                        $q->where('year_id', Year::where('current', true)->first()->id);
                    }])
                        ->where('afm', $teacher->afm)
                        ->get()
                        ->filter(function($p){
                            if($p->praxi != null) return true;
                        })->values()->all(),
                    'afm'           => $teacher->afm,
                    'placements'    => collect()
                ]);
            }

            if(!empty($tempCollection["topothetisis"])){
                foreach($tempCollection["topothetisis"] as $topothetisi){
                    $topothetisi->edit_cell = false;
                }
            }

            $teachers->push($tempCollection);

        }

        return $teachers->sortBy('full_name')->values()->all();

    }

    public function jsonAllTeachersOfEidikotita($eidikotita)
    {
        $this->denies('manage_placements');

        $e = $this->getUnitedMySchoolEidikotitaArray($eidikotita);

        $teacherCollection = MySchoolTeacher::whereIn('eidikotita', $e)->get();

        $teachers = collect();

        foreach($teacherCollection as $teacher){
            if($teacher->am == ''){
                $tempCollection = collect([
                    'source'        => 'myschool',
                    'totalPlacements' => 0,
                    'totalTopothetisis' => 0,
                    'sxesi'         => 'ΑΝΑΠΛΗΡΩΤΗΣ',
                    'id'            => $teacher->afm,
                    'teacherable_type' => 'App\MySchool',
                    'full_name'     => $teacher->last_name . ' ' . $teacher->first_name,
                    'middle_name'   => $teacher->middle_name,
                    'moria'         => '-',
                    'organiki'      => 'ΑΝΑΠΛΗΡΩΤΗΣ',
                    'organiki_id'   => 3000,
                    'klados'        => $teacher->eidikotita,
                    'entopiotita'   => 'Χωρίς στοιχεία',
                    'sinipiretisi'  => 'Χωρίς στοιχεία',
                    'special'       => 'Χωρίς στοιχεία',
                    'ypoxreotiko'   => $teacher->ypoxreotiko,
                    'topothetisis'  => Placement::with(['praxi' => function($q){
                        $q->where('year_id', Year::where('current', true)->first()->id);
                    }])
                        ->where('afm', $teacher->afm)
                        ->get()
                        ->filter(function($p){
                            if($p->praxi != null) return true;
                        })->values()->all(),
                    'afm'           => $teacher->afm,
                    'placements'    => collect()
                ]);
            }else{
                $tempCollection = collect([
                    'source'        => 'myschool',
                    'totalPlacements' => 0,
                    'totalTopothetisis' => 0,
                    'sxesi'         => 'ΜΟΝΙΜΟΣ',
                    'id'            => $teacher->afm,
                    'teacherable_type' => 'App\MySchool',
                    'full_name'     => $teacher->last_name . ' ' . $teacher->first_name,
                    'middle_name'   => $teacher->middle_name,
                    'moria'         => '-',
                    'organiki'      => $teacher->organiki_name,
                    'organiki_id'   => $teacher->organiki_id,
                    'klados'        => $teacher->eidikotita,
                    'entopiotita'   => 'Χωρίς στοιχεία',
                    'sinipiretisi'  => 'Χωρίς στοιχεία',
                    'special'       => 'Χωρίς στοιχεία',
                    'ypoxreotiko'   => $teacher->ypoxreotiko,
                    'topothetisis'  => Placement::with(['praxi' => function($q){
                        $q->where('year_id', Year::where('current', true)->first()->id);
                    }])
                        ->where('afm', $teacher->afm)
                        ->get()
                        ->filter(function($p){
                            if($p->praxi != null) return true;
                        })->values()->all(),
                    'afm'           => $teacher->afm,
                    'placements'    => collect()
                ]);
            }

            if(!empty($tempCollection["topothetisis"])){
                foreach($tempCollection["topothetisis"] as $topothetisi){
                    $topothetisi->edit_cell = false;
                }
            }

            $teachers->push($tempCollection);

        }

        return $teachers->sortBy('full_name')->values()->all();

    }

    public function jsonAllPlacementsTypes()
    {
        return \Config::get('requests.placements_type');
    }

    public function softDelete($placement_id)
    {
        $this->denies('manage_placements');


        Placement::find($placement_id)->delete();

        flash()->error('Προσοχή', 'Η τοποθέτηση ανακλήθηκε');

        return redirect()->back();
    }

    public function restorePlacement($placement_id)
    {
        $this->denies('manage_placements');

        Placement::withTrashed()->find($placement_id)->restore();

        flash()->info('Συγχαρητήρια', 'Η τοποθέτηση ενεργοποιήθηκε');

        return redirect()->back();
    }

    public function postPermanentDelete(Request $request)
    {
        $this->denies('manage_placements');

        if($request->ajax()){
            Placement::withTrashed()->find($request->get('placement_id'))->forceDelete();

            flash()->success('','Η τοποθέτηση '.$request->get('placement_id').' διεγράφη με επιτυχία');

            return 'success';
        }
            flash()->error('Προσοχή!', 'αποτυχία διαγραφής...');
        abort(403);
    }

    public function ajaxDeletePlacement(Request $request)
    {
        $this->denies('manage_placements');

        if($request->ajax()){
            $id = $request->get('id');
            Placement::find($id)->delete();
            return 'ok';
        }
        abort(403);
    }


    public function ajaxTeacherDetails(Request $request)
    {
        $this->denies('manage_placements');

        if ($request->ajax()){
            $data = array();
            $id = $request->get('teacherAFM');
            $teacher  = MySchoolTeacher::find($id);

            $data['klados_full_name'] = $teacher->eidikotita . ' (' . $teacher->klados . ')';
            $data['klados_id'] = $teacher->klados_id;


            $data['organiki_name'] = $teacher->organiki_name;
            $data['organiki_id'] = $teacher->organiki_id;

            return $data;
        }
    }


    /**
     * @param $eidikotita
     * @return mixed
     */
    private function getUnitedEidikotitaArray($eidikotita)
    {
        $united = Eidikotita::find($eidikotita)->united;

        $e = Eidikotita::where('united', $united)->lists('id');
        return $e;
    }

    private function getNewEidikotitesMySchoolArray($klados_slug)
    {
        return NewEidikotita::where('klados_slug', $klados_slug)->lists('id');
    }

    private function getUnitedMySchoolEidikotitaArray($eidikotita)
    {
        $united = Eidikotita::find($eidikotita)->united;

        $e = Eidikotita::where('united', $united)->lists('slug_name');

        return $e;
    }

    private function getUnitedEidikotitaPleonasmaLists($eidikotita)
    {

    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(401);
        }
    }

    /**
     * @param $e
     * @return array
     */
    private function getSchoolsPleonazonta($e)
    {
        $schools = array_collapse($e->pluck('schools')->all());

        $list_schools = array();
        foreach ($schools as $school) {
            $list_schools[] = $school->id;
        }
        return $list_schools;
    }

}
