<?php

namespace App\Http\Controllers;

use App\Http\Controllers\traits\PlacementsTrait;
use App\Placement;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Gate;
use App\Teacher;
use App\Praxi;
use Carbon\Carbon;

class SchoolPlacementsController extends Controller
{
    use PlacementsTrait;

    public function  __construct()
    {
        $this->middleware('role:school');
    }

    public function getTeachersWithPlacements()
    {
        $this->denies('school_view_placements');

        $teachers =  Teacher::whereHas('placements', function($query){
            $query->withTrashed()
                    ->where('to_id', Auth::user()->userable->id);
        })->get();

        return view('school.placements.teachers_TO_school', compact('teachers'));
    }


    public function getTeachersOrganikiPlacements()
    {
        $this->denies('school_view_placements');

        $teachers =  Teacher::whereHas('placements', function($query){
            $query->withTrashed()
                ->where('from_id', Auth::user()->userable->id);
        })->get();

        return view('school.placements.teachers_Organiki', compact('teachers'));
    }

    public function viewTeacherPlacementsPDF($teacher_id)
    {
        $this->denies('school_view_placements');

        return $this->allPlacementsPDF($teacher_id);
    }

    public function viewTeacherDetails($teacher_id)
    {
        $this->denies('school_view_placements');

        return $this->getIndexPlacementsOfTeacher($teacher_id);

    }

    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }

}
