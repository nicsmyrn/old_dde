<?php

namespace App\Http\Controllers;

use App\Anaplirotis;
use App\Eidikotita;
use App\Events\TeacherChangeProfileNotification;
use App\FakeTeacher;
use App\Http\Requests\MonimosRequest;
use App\Monimos;
use App\Notification;
use App\School;
use App\Teacher;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TeacherRequest;
use App\User;
use App\Http\Requests\InfoRequest;
use Carbon\Carbon;
use App\Http\Requests\TeacherSmallRequest;
use App\FakeMonimos;
use App\FakeAnaplirotis;
use Smail;


class ProfileController extends Controller
{
    protected $user;

    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
        $this->user = $auth->user();
    }

    public function getInfo()
    {
        $user = User::with('userable')->find(Auth::id());

        return view('user.info', compact('user'));
    }

    public function postInfo(InfoRequest $request)
    {
        $user = User::find(Auth::id());

        $user->update($request->all());

        flash()->success('Συγχαρητήρια!', 'Οι αλλαγές αποθηκεύτηκαν με επιτυχία...');

        return redirect()->back();
    }

    public function getChangeMail()
    {
        return view('user.change_email');
    }

    public function postChangeMail(Request $request)
    {
        $this->validate($request, [
            'old_email' => 'required|email|exists:users,email',
            'new_email' => 'required|email'
        ],[
            'old_email.required' => 'Το υπάρχον E-mail είναι υποχρεωτικό',
            'old_email.email'    => 'Το υπάρχον E-mail δεν είναι έγκυρο',
            'old_email.exists'   => 'Το υπάρχον E-mail δεν υπάρχει στη βάση δεδομένων',
            'new_email.required' => 'Το νέο E-mail είναι υποχρεωτικό',
            'new_email.email'    => 'Το νέο E-mail δεν είναι έγκυρο'
        ]);

        if($this->user->email != $request->get('old_email')) {
            return redirect()->back()->withErrors(['old_email' => 'Το E-mail δεν αντιστοιχεί στον χρήστη']);
        }
        
        if($this->user->provider == 'google'){
            return redirect()->back()->withErrors(['provider'=> 'Το Email δεν αλλάζει διότι ανήκει στη Google.']);
        }

        $this->user->email = $request->get('new_email');
        $this->user->save();

        flash('Συγχαρητήρια!', 'Το E-mail σας άλλαξε με επιτυχία');

        return redirect('/');
    }

    public function getProfile(Request $request)
    {
        $action = $request->get('action');
        if($action != null){
            Notification::where('uniqueAction', $action)->update([
                'unread' => 'is_false'
            ]);
        }

        $kladoi = collect($this->kladoiList());
        $schools = collect($this->schoolsList());

        if ($this->user->userable_type == 'App\Teacher') {
            if ($this->user->userable == null) {
                $teacher = new Teacher();
            } else {
                $real_teacher = $this->user->userable;
                $new_klados = $real_teacher->myschool->new_klados . ' ('. $real_teacher->myschool->new_eidikotita_name . ')';

                if($real_teacher->fid != null){
                    $teacher = FakeTeacher::find($real_teacher->fid);
                    if($teacher == null){
                        $real_teacher->fid = null;
                        $real_teacher->save();
                        $this->getProfile($request);
                    }
                }else{
                    $teacher = $real_teacher;
                }
//                if($teacher->fmid != null){
//                    $misthodosia  = FakeMisthodosia::find($teacher->fmid);
//                }
            }
            return view('user.profile', compact('kladoi', 'schools' , 'teacher', 'real_teacher', 'new_klados'));
        }elseif($this->user->userable_type == 'App\School') {
            $school = $this->user->userable;

            return view('user.profile', compact('school'));
        }
    }

    public function postSchoolProfile(Request $request)
    {
        $valitator = \Validator::make($request->all(), [
            'work_phone' => 'alpha_num|size:10',
            'mobile_phone' => 'alpha_num|size:10',
            'sex'   => 'required',
            'area'  => 'required'
        ]);

        if ($valitator->fails()){
            return redirect()->back()
                ->withErrors($valitator)
                ->withInput();
        }

        $school = $this->user->userable;

        $this->user->sex = $request->get('sex');
        $this->user->save();

        $school->update($request->all());

        flash()->success('Συγχαρητήρια αλλάξατε με επιτυχία το προφίλ σας...');

        return redirect()->back();
    }

    public function postSmallProfile(TeacherSmallRequest $request)
    {
        $teacher = $this->user->userable;

        $teacher->update($request->all());

        flash()->success('Συγχαρητήρια '.$this->user->first_name,'Οι αλλαγές στο προφίλ σου αποθηκεύτηκαν με επιτυχία');

        return redirect()->back();
    }

    public function undoRequestToChange()
    {
        $teacher = $this->user->userable;

        $teacher->request_to_change = false;

        $teacher->save();

        flash()->success('Το αίτημα για αλλαγή στοιχείων αναιρέθηκε');

        return redirect()->back();
    }

    public function sentAttachementsProfile(Request $request)
    {
        $attachments = array();

        for($i=1; $i<=3; $i++){
            if($request->hasFile('attachment'.$i)){
                $attachments[] = [
                    'real_path' => $request->file('attachment'.$i)->getRealPath(),
                    'options' =>
                        [
                            'as'  => 'Δικαιολογητικό_No'.$i,
                            'mime' => $request->file('attachment'.$i)->getMimeType()
                        ]
                ];
            }
        }

        event(new TeacherChangeProfileNotification($this->user, [
            'title' => 'Αποστολή Δικαιολογητικών',
            'description' => "Ο Εκπαιδευτικός <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> του {$this->user->userable->middle_name} έχει στείλει δικαιολογητικά για αλλαγή των στοιχείων του.",
            'type' => 'warning',
            'teacherID' => $this->user->userable->fid != null ? $this->user->userable->teacher->id : $this->user->userable->id
        ]));

        if(\Config::get('requests.sent_mail') && count($attachments) > 0) {
            Smail::sentTeacherToPysdeOnlyAttachments($attachments, $this->user->full_name, $this->user->userable->afm, $this->user->userable->id);
        }

        flash()->success('Συγχαρητήρια '.$this->user->first_name,'Τα Δικαιολογητικά στάλθηκαν στο τμήμα Γ Προσωπικού');

        return redirect()->back();
    }

    public function postProfile(Request $request)
    {
        if($this->user->userable->profile_counter >=3){
            flash()->overlayE('Προσοχή!', 'Δεν έγινε ενημέρωση των στοιχείων σας. Μέχρι τον επόμενο έλεγχο, δεν μπορείτε να στείλετε νέα στοιχεία.');
            return redirect()->back();
        }
        /*
         * If user has No Profile
         */
        $attachments = array();

        for($i=1; $i<=3; $i++){
            if($request->hasFile('attachment'.$i)){
                $attachments[] = [
                    'real_path' => $request->file('attachment'.$i)->getRealPath(),
                    'options' =>
                        [
                            'as'  => 'Δικαιολογητικό_No'.$i,
                            'mime' => $request->file('attachment'.$i)->getMimeType()
                        ]
                ];
            }
        }
        /*
         * If user has No Profile
         */
        if ($this->user->userable == null) {
            if($request->get('teacher_type') == 0){ // ΜΟΝΙΜΟΣ
                $monimoi = Monimos::create($request->all());
                $teacherable_id = $monimoi->id;
                $teacherable_type = 'App\Monimos';
            }elseif($request->get('teacher_type') == 1) { //ΑΝΑΠΛΗΡΩΤΗΣ
                $anaplirotis = Anaplirotis::create($request->all());
                $teacherable_id = $anaplirotis->id;
                $teacherable_type = 'App\Anaplirotis';
            }

            $request_all = $request->all() + [
                    'teacherable_id' => $teacherable_id,
                    'teacherable_type' => $teacherable_type,
                    'activation_for_register' => true
                ];

            $teacher = Teacher::create($request_all);

            $teacher_id = $teacher->id;

            $this->user->update([
                'userable_id'=>$teacher->id,
                'sex' => $request->get('sex')
            ]);
        }else{
            $teacher = $this->user->userable;
            $teacher_id = $teacher->id;

            $teacher->user->update(['sex' => $request->get('sex')]);

            if($request->get('teacher_type') == 0){ // ΜΟΝΙΜΟΣ
                $teacherable_type_request = 'App\Monimos';
            }
            elseif($request->get('teacher_type') == 1){
                $teacherable_type_request = 'App\Anaplirotis';
            }

            if($teacher->fid != null){
                $teacher = FakeTeacher::find($teacher->fid);

                if($request->get('teacher_type') == 0){ // ΜΟΝΙΜΟΣ
                    $teacherable_type_request = 'App\FakeMonimos';
                }
                elseif($request->get('teacher_type') == 1){
                    $teacherable_type_request = 'App\FakeAnaplirotis';
                }
            }

            $request_all = $request->all() + [
//                    'is_checked'=> true,
                    'activation_for_register' => true
                ];

            if ($teacher->teacherable_type != $teacherable_type_request){
                $teacher->teacherable->delete();

                if($teacherable_type_request == 'App\Monimos'){ // ΜΟΝΙΜΟΣ
                        $monimoi = Monimos::create($request->all());
                        $teacherable_id = $monimoi->id;
                        $teacherable_type = 'App\Monimos';
                }elseif($teacherable_type_request == 'App\Anaplirotis') { //ΑΝΑΠΛΗΡΩΤΗΣ
                        $anaplirotis = Anaplirotis::create($request->all());
                        $teacherable_id = $anaplirotis->id;
                        $teacherable_type = 'App\Anaplirotis';
                }elseif($teacherable_type_request == 'App\FakeAnaplirotis') { //ΑΝΑΠΛΗΡΩΤΗΣ
                        $anaplirotis = FakeAnaplirotis::create($request->all() + ['is_checked' => false]);
                        $teacherable_id = $anaplirotis->id;
                        $teacherable_type = 'App\FakeAnaplirotis';
                }elseif($teacherable_type_request == 'App\FakeMonimos') { //ΑΝΑΠΛΗΡΩΤΗΣ
                        $monimoi = FakeMonimos::create($request->all() + ['is_checked' => false]);
                        $teacherable_id = $monimoi->id;
                        $teacherable_type = 'App\FakeMonimos';
                }

                $request_all = $request_all + [
                        'teacherable_id' => $teacherable_id,
                        'teacherable_type' => $teacherable_type
                    ];
            }else{
                $teacher_type = $teacher->teacherable;

                $teacher_type->update($request->all());

            }

            $teacher->update($request_all);

            $teacher->is_checked = 0;
            $teacher->save();


        }

        $this->user->userable->update([
            'profile_counter'   => $this->user->userable->profile_counter + 1
        ]);

        if($teacher->profile_counter > 0){
            event(new TeacherChangeProfileNotification($this->user, [
                'title' => 'Αίτημα για αλλαγή ΠΡΟΦΙΛ',
                'description' => "Ο Εκπαιδευτικός <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> του $teacher->middle_name αιτείται να αλλάξει τα στοιχεία στο Προφίλ του.",
                'type' => 'primary',
                'teacherID' => $teacher->fid != null ? $teacher->teacher->id : $teacher->id
            ]));
        }else{
            event(new TeacherChangeProfileNotification($this->user, [
                'title' => 'ΑΛΛΑΓΗ ΠΡΟΦΙΛ',
                'description' => "Ο Εκπαιδευτικός <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> του $teacher->middle_name έχει αλλάξει τα στοιχεία στο Προφίλ του.",
                'type' => 'warning',
                'teacherID' => $teacher->fid != null ? $teacher->teacher->id : $teacher->id
            ]));
        }

        if(\Config::get('requests.sent_mail') && count($attachments) > 0) {
            Smail::sentTeacherToPysdeRequest($attachments, $this->user->full_name, $this->user->userable->afm, $teacher_id);
        }

        flash()->success('Συγχαρητήρια '.$this->user->first_name,'Οι αλλαγές στο προφίλ σου στάλθηκαν για έλεγχο');

        return redirect()->back();
    }

    public function postFakeProfile(TeacherRequest $request)
    {

    }

    public function ajaxRequestChangeProfile(Request $request)
    {
        if($request->ajax()){

            $teacher = $this->user->userable;

            $teacher->request_to_change = true;

            $teacher->save();

            $data = array();

            flash()->success('','Το Αίτημά σας στάλθηκε με επιτυχία...');

            if(\Config::get('requests.sent_mail')) {

                Smail::sentTeacherProfile($teacher, 'Αίτημα αλλαγής Προφίλ', [
                    'type' => 'changeProfileRequest',
                    'full_name' => $teacher->user->full_name
                ]);
            }
            $data['return'] = 'query_success';

            event(new TeacherChangeProfileNotification($this->user, [
                'title' => 'Αίτημα αλλαγής στοιχείων',
                'description' => "Ο Εκπαιδευτικός <span class='notification_full_name'>&laquo;{$this->user->full_name}&raquo;</span> του $teacher->middle_name αιτείται να αλλάξει τα στοιχεία στο Προφίλ του.",
                'type' => 'danger',
                'teacherID' => $teacher->id
            ]));

            return $data;
        }else{
            abort(404);
        }
    }

    private function schoolsList()
    {
        $schools = School::all();

        $tempSchools = array();
        foreach($schools as $v){
            $tempSchools[$v->id] = $v->name;
        }

        foreach(\Config::get('requests.teacher_types') as $k=>$v){
            $tempSchools[$k] = $v;
        }
//        $tempSchools[1000] = 'Διάθεση ΠΥΣΔΕ';
//        $tempSchools[2000] = 'Απόσπαση από άλλο ΠΥΣΔΕ';

        return $tempSchools;
    }

    private function kladoiList()
    {
        $kladoi = Eidikotita::all();
        $tempKladoi = array();
        foreach($kladoi as $v){
            $tempKladoi[$v->id] = $v->full_name;
        }
        return $tempKladoi;
    }
}


