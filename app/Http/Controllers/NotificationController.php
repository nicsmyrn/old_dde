<?php

namespace App\Http\Controllers;

use App\Http\Requests\Request;
use App\Notification;

class NotificationController extends Controller
{
    private $user;

    public function __construct()
    {
        $this->middleware('auth');
        $this->user = \Auth::user();
    }

    public function index()
    {
        $this->resetNotifications();

        $notifications = $this->user->readedNotifications();
        return view('partials.nav.notifications.index', compact('notifications'));
    }

    public function ajaxDeleteAll()
    {
        if (\Request::ajax()){
            $this->deleteAllNotifications();
            flash()->success('','ΟΛΕΣ οι ειδοποιήσεις ΔΙΑΓΡΑΦΗΚΑΝ');
            return 'true';
        }else{
            abort(404);
        }
    }

    public function getUserId()
    {
        if(\Request::ajax()){
            return $this->user->id;
        }else{
            abort(404);
        }
    }

    public function fetchNotifications()
    {
        if(\Request::ajax()){
            return $this->user->notifications()->values();
        }else{
            abort(404);
        }
    }

    private function deleteAllNotifications()
    {
        if($this->user->isRole('ofa')){
            return Notification::where('forRole', $this->user->role->slug)
                ->delete();
        }else{
            return Notification::where('forRole', $this->user->role->slug)
                ->where('forUser', $this->user->id)
                ->delete();
        }
    }

    private function resetNotifications()
    {
        return Notification::where('forRole', $this->user->role->slug)
            ->where('unread', 'is_true')
            ->orWhere(function($query){
                $query->where('forRole','=','self')
                    ->where('user_id','=',$this->user->id);
            })
            ->update([
                'unread' => 'is_false'
            ]);
    }



}
