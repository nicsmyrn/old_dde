<?php

namespace App\Http\Controllers;

use App\Allteacher;
use App\MySchoolTeacher;
use App\OrganikiRequest;
use App\Praxi;
use App\RequestTeacher;
use App\Teacher;
use App\Year;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Gate;
use App\ExcelFiles;
use App\School;
use App\Eidikotita;
use App\Eidiki;


class ExcelController extends Controller
{
    private $prosorini_topothetisi = false;
    private $diathesi = false;
    private  $apospasi = false;
    private  $anaklisi = false;
    private  $protovathimia = false;

    private $sort = [
        'eidikotita'     => 'asc',
        'full_name' => 'asc',
        'middle_name'    => 'asc'
    ];

    private  $placementsStringTypes = [
        'topothetisi' => [1,6],
        'diathesi'      => [3,5],
        'apospasi'      => [2,7],
        'anaklisi'      => [9],
        'pvthmia'       => [8]
    ];

    private $excelPlacementsSheetHeaders = [
        'Προσωρ Τοποθέτηση & Συμπλήρωση' => 'Προσωρινές Τοποθετήσεις και Διάθεση εκπαιδευτικών - Πράξη: ',
        'Προσωρινή Τοποθέτηση' => 'Προσωρινές Τοποθετήσεις - Πράξη: ',
        'Συμπλήρωση Ωραρίου'   => 'Διάθεση Εκπαιδευτικών για συμπλήρωση ωραρίου - Πράξη: ',
        'Απόσπαση'  => 'Αποσπάσεις Εκπαιδευτικών - Πράξη: ',
        'Ανάκληση'  => 'Ανάκληση τοποθετήσεων εκπαιδευτικών - Πράξη: ',
        'Πθμια'    => 'Διάθεση εκπαιδευτικών στην Πρωτοβάθμια - Πράξη: '
    ];

    protected $style_Array = array(
        'borders' => array(
            'allborders' => array(
                'style' => \PHPExcel_Style_Border::BORDER_THIN
            )
        ),
        'alignment' => array(
            'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_CENTER
        )
    );
    
    public function excelPreparation()
    {
        $this->denies('manage_excels_kena_pleonasmata');
        
        $excel_files = ExcelFiles::orderBy('file_date')->get();

        return view('pysde.kena_pleonasmata.Excel.excelPrepare', compact('excel_files'));
    }  
    
    public function excelArchives()
    {
        $this->denies('archives_excel_files');
        
        $excel_files = ExcelFiles::orderBy('file_date')->get();
            
        return view('pysde.kena_pleonasmata.Excel.excelArchive', compact('excel_files'));
    } 
    
    public function excelDownloadKenaPleonasmata($file)
    {
        $this->denies('archives_excel_files');
        
        $file_path = storage_path('app/excel/').$file;
            
        if (file_exists($file_path)){
            return response()->download($file_path, $file, [
                'Content-Length: '. filesize($file_path)
            ]);
        }else{
            exit('Το συγκεκριμένο αρχείο ΔΕΝ υπάρχει!');        
        }  
    }

    public function excelParalliliEidiki(Request $request)
    {
        $this->denies('manage_excel_files');

        $date = $request->get('excel_date_parallili');
        $description = $request->get('description_parallili');

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical'   => \PHPExcel_Style_Alignment::VERTICAL_CENTER
            )
        );

        $schools = School::with(['eidikotites'=>function($query){
            return $query->where('sch_type', 'parallili');
        }])->orderBy('order')->get();

        $last_cell_row = 3;
        foreach($schools as $school){
            if($school->notZeroParalliliCell() != 'NULL'){
                $last_cell_row = $last_cell_row + 1;
            }
        }

        $eidikotites = Eidikotita::where('sch_type', 'parallili')->get();

        \Excel::create(str_replace('/','-',$date).'_PARALLILI', function($excel) use($schools, $eidikotites, $date, $description, $styleArray, $last_cell_row){

            $excel->sheet('Παράλληλη Στήριξη', function($sheet) use($schools, $eidikotites, $date, $styleArray, $last_cell_row){

                $sheet->setPageMargin(array(
                    0.55, 0.25, 0.25, 0.25
                ));
                $sheet->setOrientation('landscape')
                ->loadView('pysde.kena_pleonasmata.Excel.parallili_omada1')
                    ->setFontFamily('Arial')
                    ->setFontSize(16)
                    ->setFontBold(false)
                    ->with(compact('schools','eidikotites'));

                $sheet->setHeight(1, 50);
                $sheet->setHeight(2,30);

                $height_array = array();
                for($i=3; $i<=$last_cell_row; $i++){
                    $height_array[$i] = 50;
                }
                $sheet->setHeight($height_array);

                $sheet->cells('A1:S1', function($cells) {
                    $cells->setFontSize(34);
                    $cells->setFontWeight('bold');
                });

                $sheet->cells('B3:S'.$last_cell_row, function($cells) {
                    $cells->setFontSize(24);
                });

                $sheet->cells('A3:S'.($last_cell_row-1), function($cells) {
                    $cells->setFontSize(20);
                    $cells->setFontWeight('bold');
                });

                $sheet->cells('A'.$last_cell_row.':S'.$last_cell_row, function($cells) {
                    $cells->setFontSize(28);
                    $cells->setFontWeight('bold');
                    $cells->setBackground('#B0B0B0');
                });

                $sheet->getHeaderFooter()->setOddHeader(' ΠΙΝΑΚΑΣ ΠΑΡΑΛΛΗΛΗΣ ΣΤΗΡΙΞΗΣ: '. $date);
                $sheet->getStyle('A1:S'.$last_cell_row)->applyFromArray($styleArray);
                $sheet->getStyle('A1:A'.$last_cell_row)->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            });
            
            $excel_file = ExcelFiles::create([
                'file_name' => str_replace('/','-',$date).'_PARALLILI.xls',
                'file_date' => \Carbon\Carbon::createFromFormat('d/m/Y', $date),
                'description' => $description
            ]);

        })->store('xls', storage_path('app/excel'))
            ->download('xls');
    }
    
    public function createTmimataEdaksis(Request $request)
    {
        $this->denies('manage_excel_files'); 
        
        $date = $request->get('excel_date_eidiki');
        
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),

        );


        $schools = School::with('eidikis')->has('eidikis')->get();
        $eidikotites = Eidiki::with('schools')->where('sch_type','all')->orderBy('order')->get();
        
        $last_cell_row = 1;
        foreach($eidikotites as $eidikotita){
            if($eidikotita->notZeroCellValues() != 'NULL'){
                $last_cell_row = $last_cell_row + 1;
            }
        }

        $description = $request->get('description_eidiki');


        
        // return view('pysde.kena_pleonasmata.Excel.tmimataEntaksis', compact('schools', 'eidikotites'));
        
        
        \Excel::create(str_replace('/','-',$date).'_KENA-TMHMATA-ENTAKSIS', function($excel) use($schools, $eidikotites, $styleArray, $date, $description, $last_cell_row){

            $excel->sheet('ΤμήματαΈνταξης', function($sheet) use($schools, $eidikotites, $styleArray, $date, $last_cell_row){
                
                $sheet->loadView('pysde.kena_pleonasmata.Excel.tmimataEntaksis')
                      ->setFontFamily('Arial')
                      ->setFontSize(19)
                      ->setFontBold(false)
                      ->with(compact('schools','eidikotites'))
                      ->cells('A1:O1', function($cells){
                          $cells->setFontSize(16);
                      })

                      ->cells('A2:A'.$last_cell_row, function($cells){
                          $cells->setFontSize(14);
                      })
                      ->cells('B2:B'.$last_cell_row, function($cells){
                          $cells->setFontSize(9);
                      })
                      ->getStyle('A1:O1')->getAlignment()->setTextRotation(90)->setWrapText(true)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
                      
                  $sheet->cells('C1:O1', function($cells) {
                     $cells->setFontWeight('bold');
                     $cells->setFontSize(16);
                  });
                
                $sheet->setHeight(1, 123);
                $sheet->setPageMargin(array(
                    0.55, 0.25, 0.25, 0.25
                ));

                $sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΠΙΝΑΚΑΣ ΛΕΙΤΟΥΡΓΙΚΩΝ ΚΕΝΩΝ ΓΙΑ ΤΜΗΜΑΤΑ ΕΝΤΑΞΗΣ: '. $date);

                $sheet->getColumnDimension('A')->setWidth(6);
                $sheet->getColumnDimension('B')->setWidth(11);

                for($col='C'; $col !== 'P'; $col++){
                    $sheet->setSize($col.'1',5,95);
                }

                for($row=2; $row <=$last_cell_row ;$row++){
                    $sheet->getRowDimension($row)->setRowHeight(30);
                }

                $sheet->getStyle('A1:O'.$last_cell_row)->applyFromArray($styleArray);
                $sheet->getStyle('A2:O'.$last_cell_row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            });
//

            // Set the title
            $excel->setTitle('Pinakas Kenon EIDIKIS AGOGIS');

            // Chain the setters
            $excel->setCreator('PYSDE')
                  ->setCompany('DDE Chanion');

            // Call them separately
            $excel->setDescription('PYSDE CHANION');

            $excel_file = ExcelFiles::create([
                'file_name' => str_replace('/','-',$date).'_KENA-TMHMATA-ENTAKSIS.xls',
                'file_date' => \Carbon\Carbon::createFromFormat('d/m/Y', $date),
                'description' => $description
            ]);


        })->store('xls', storage_path('app/excel'))
        ->download('xls');        
        
    }

    public function excelHtmlCreate(Request $request)
    {
        $header1 = School::with(['eidikotites', 'mathimata'])->where('group',1)->where('type','<>', 'eidiko')->orderBy('order')->get();

        $eidikotites = Eidikotita::with('schools')->where('sch_type','all')->orderBy('order')->get();


        $last_cell_row = 1;
        foreach($eidikotites as $eidikotita){
            if($eidikotita->notZeroCellValues() != 'NULL'){
                $last_cell_row = $last_cell_row + 1;
            }
        }

        return view('pysde.kena_pleonasmata.Excel.omada1_orderBy_eidikotites', compact('header1','eidikotites'));
    }

    public function excelCreate(Request $request)
    {
//        $this->denies('manage_excel_files');
        
        $date = $request->get('excel_date');

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $header1 = School::with(['eidikotites', 'mathimata'])->where('group',1)->where('type','<>', 'eidiko')->orderBy('order')->get();
        $header2 = School::with(['eidikotites', 'mathimata'])->where('group', '<>',1)->where('type','<>', 'eidiko')->orderBy('order')->get();


        $eidikotites = Eidikotita::with('schools')->where('sch_type','all')->orderBy('order')->get();


//        foreach ($eidikotites as $eidikotita) {
//            foreach($header1 as $school){
//                if($school->type == 'Γυμνάσιο' && $eidikotita->united == 'pe04'){
//                    echo "<p>Ειδικότητα: {$eidikotita->name} School Id : {$school->name} - value: {$school->cellValue($eidikotita->id, $eidikotita->slug)}</p>";
//                }
//            }
//        }
//
//        return '';

        $last_cell_row = 1;
        foreach($eidikotites as $eidikotita){
            if($eidikotita->notZeroCellValues() != 'NULL'){
                $last_cell_row = $last_cell_row + 1;
            }
        }
        $last_cell_row = $last_cell_row + 4;

        $description = $request->get('description');

            \Excel::create(str_replace('/','-',$date).Carbon::now()->format('_Ymdhi_').'_KENA-PLEONASMATA', function($excel) use($header1,$header2, $eidikotites, $styleArray, $date, $description, $last_cell_row){

                $excel->sheet('Ομάδα1η', function($sheet) use($header1, $eidikotites, $styleArray, $date, $last_cell_row){
                    
                    $sheet->loadView('pysde.kena_pleonasmata.Excel.omada1_orderBy_eidikotites')
                          ->setFontFamily('Arial')
                          ->setFontSize(19)
                          ->setFontBold(false)
                          ->with(compact('header1','eidikotites'))
                          ->cells('A1:AF1', function($cells){
                              $cells->setFontSize(16);
                          })
 
                          ->cells('A2:A'.$last_cell_row, function($cells){
                              $cells->setFontSize(14);
                          })
                          ->cells('B2:B'.$last_cell_row, function($cells){
                              $cells->setFontSize(9);
                          })
                          ->getStyle('A1:AF1')->getAlignment()->setTextRotation(90)->setWrapText(true)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);
                          
                      $sheet->cells('C1:AF1', function($cells) {
                         $cells->setFontWeight('bold');
                         $cells->setFontSize(16);
                      });
                    
                    $sheet->setHeight(1, 123);
                    $sheet->setPageMargin(array(
                        0.55, 0.25, 0.25, 0.25
                    ));

                    $sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΟΜΑΔΑ 1 -  ΠΙΝΑΚΑΣ ΛΕΙΤΟΥΡΓΙΚΩΝ ΚΕΝΩΝ-ΠΛΕΟΝΑΣΜΑΤΩΝ: '. $date);
                    $sheet->getHeaderFooter()->setOddFooter('FILE:  '. str_replace('/','-',$date).Carbon::now()->format('_Ymdhi'));

                    $sheet->getColumnDimension('A')->setWidth(6);
                    $sheet->getColumnDimension('B')->setWidth(11);

                    for($col='C'; $col !== 'AG'; $col++){
                        $sheet->setSize($col.'1',5,150);
                    }

                    for($row=2; $row <=$last_cell_row ;$row++){
                        $sheet->getRowDimension($row)->setRowHeight(30);
                    }

                    $sheet->getStyle('A1:AF'.$last_cell_row)->applyFromArray($styleArray);
                    $sheet->getStyle('A2:AF'.$last_cell_row)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

                    $sheet->setFreeze('C2');

                });

                $excel->sheet('Ομάδες 2-3-4-5-6', function($sheet) use($header2, $eidikotites, $styleArray, $date, $last_cell_row){

                    $sheet->setHeight([
                        1 => 50,
                        2 => 150
                    ]);
                    $sheet->setPageMargin(array(
                        0.55, 0.25, 0.25, 0.25
                    ));

                    $sheet->loadView('pysde.kena_pleonasmata.Excel.omada23456_orderBy_eidikotites')
                          ->setFontFamily('Arial')
                          ->setFontSize(16)
                          ->setFontBold(false)
                          ->with(compact('header2','eidikotites'))
                          ->cells('C1:X1', function($cells) {
                             $cells->setFontWeight('bold');
                          })
                        ->cells('C1:X1', function($cells){
                            $cells->setFontSize(18);
                        })
                          ->cells('A2:X2', function($cells){
                              $cells->setFontSize(16);
                          })
                          ->cells('A3:A'.$last_cell_row, function($cells){
                              $cells->setFontSize(14);
                          })
                          ->cells('B3:B'.$last_cell_row, function($cells){
                              $cells->setFontSize(9);
                          });

                    $sheet->getStyle('A1:X1')->getAlignment()
                        ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    $sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΟΜΑΔΑ 2,3,4,5,6 -  ΠΙΝΑΚΑΣ ΛΕΙΤΟΥΡΓΙΚΩΝ ΚΕΝΩΝ-ΠΛΕΟΝΑΣΜΑΤΩΝ: '. $date);
                    $sheet->getHeaderFooter()->setOddFooter('FILE:  '. str_replace('/','-',$date).Carbon::now()->format('_Ymdhi'));

                    $sheet->getColumnDimension('A')->setWidth(6);
                    $sheet->getColumnDimension('B')->setWidth(11);

//                    for($col='C'; $col !== 'Y'; $col++){
//                        $sheet->setSize($col.'3',5,150);
//                    }

                    for($row=3; $row <=($last_cell_row+1) ;$row++){
                        $sheet->getRowDimension($row)->setRowHeight(30);
                    }

                    $sheet->getStyle('A1:X'.($last_cell_row+1))->applyFromArray($this->style_Array);
                    $sheet->getStyle('A3:X'.($last_cell_row+1))->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);

                    $sheet->getStyle('A1:B2')
                        ->getAlignment()
                        ->setTextRotation(90)
                        ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER)
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                    $sheet->getStyle('C2:X2')->getAlignment()
                        ->setTextRotation(90)
                        ->setWrapText(true)
                        ->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM)
                        ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $sheet->setFreeze('C3');

                });

                // Set the title
                $excel->setTitle('Pinakas Kenon Pleonasmatwn');

                // Chain the setters
                $excel->setCreator('PYSDE')
                      ->setCompany('DDE Chanion');

                // Call them separately
                $excel->setDescription('PYSDE CHANION');

                $excel_file = ExcelFiles::create([
                    'file_name' => str_replace('/','-',$date).Carbon::now()->format('_Ymdhi_').'_KENA-PLEONASMATA.xls',
                    'file_date' => \Carbon\Carbon::createFromFormat('d/m/Y', $date),
                    'description' => $description
                ]);


            })->store('xls', storage_path('app/excel'))
            ->download('xls');
    }

    public function excelCreateOrganika(Request $request)
    {
        $this->denies('manage_excel_files');

        $date = $request->get('excel_date_organika');

        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => \PHPExcel_Style_Border::BORDER_THIN
                )
            ),
            'alignment' => array(
                'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );

        $header1 = School::where('group',1)->where('type','<>', 'eidiko')->orderBy('order')->get();
        $header2 = School::where('group', '<>',1)->where('type','<>', 'eidiko')->orderBy('order')->get();

        $eidikotites = Eidikotita::where('sch_type','all')->orderBy('order')->orderBy('slug_name')->get();


        $last_cell_row_Omada1 = 1;
        $last_cell_row_Omada2345 = 1;
        foreach($eidikotites as $eidikotita){
            if($eidikotita->notZeroOrganikaOmada1() != 'NULL'){
                $last_cell_row_Omada1 = $last_cell_row_Omada1 + 1;
            }
            if($eidikotita->notZeroOrganikaOmada2345() != 'NULL'){
                $last_cell_row_Omada2345 = $last_cell_row_Omada2345 + 1;
            }
        }

        $description = $request->get('description');

        \Excel::create(str_replace('/','-',$date).'_ORGANIKA', function($excel) use($header1,$header2, $eidikotites, $styleArray, $date, $description, $last_cell_row_Omada1, $last_cell_row_Omada2345){

            $excel->sheet('Ομάδα1η', function($sheet) use($header1, $eidikotites, $styleArray, $date, $last_cell_row_Omada1){

                $sheet->loadView('pysde.kena_pleonasmata.Excel.ORGANIKA_omada1_orderBy_eidikotites')
                    ->setFreeze('C2')
                    ->setFontFamily('Arial')
                    ->setFontSize(19)
                    ->setFontBold(false)
                    ->with(compact('header1','eidikotites'))
                    ->cells('A1:AD1', function($cells){
                        $cells->setFontSize(16);
                    })

                    ->cells('A2:A'.$last_cell_row_Omada1, function($cells){
                        $cells->setFontSize(14);
                    })
                    ->cells('B2:B'.$last_cell_row_Omada1, function($cells){
                        $cells->setFontSize(9);
                    })
                    ->getStyle('A1:AD1')->getAlignment()->setTextRotation(90)->setWrapText(true)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);

                $sheet->cells('C1:AD1', function($cells) {
                    $cells->setFontWeight('bold');
                    $cells->setFontSize(16);
                });

                $sheet->setHeight(1, 123);
                $sheet->setPageMargin(array(
                    0.55, 0.25, 0.25, 0.25
                ));

                $sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΟΜΑΔΑ 1 - ΠΙΝΑΚΑΣ ΟΡΓΑΝΙΚΩΝ ΚΕΝΩΝ-ΠΛΕΟΝΑΣΜΑΤΩΝ: '. $date);
                $sheet->getHeaderFooter()->setOddFooter(Carbon::now()->format('d-m-Y H:i'));

                $sheet->getColumnDimension('A')->setWidth(6);
                $sheet->getColumnDimension('B')->setWidth(11);

                for($col='C'; $col !== 'AE'; $col++){
                    $sheet->setSize($col.'1',5,150);
                }

                for($row=2; $row <=$last_cell_row_Omada1 ;$row++){
                    $sheet->getRowDimension($row)->setRowHeight(30);
                }

                $sheet->getStyle('A1:AD'.$last_cell_row_Omada1)->applyFromArray($styleArray);
                $sheet->getStyle('A2:AD'.$last_cell_row_Omada1)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            });

            $excel->sheet('Ομάδες 2-3-4-5-6', function($sheet) use($header2, $eidikotites, $styleArray, $date, $last_cell_row_Omada2345){

                $sheet->loadView('pysde.kena_pleonasmata.Excel.ORGANIKA_omada2345_orderBy_eidikotites')
                    ->setFreeze('C2')
                    ->setFontFamily('Arial')
                    ->setFontSize(19)
                    ->setFontBold(false)
                    ->with(compact('header2','eidikotites'))
                    ->cells('A1:X1', function($cells){
                        $cells->setFontSize(16);
                    })

                    ->cells('A2:A'.$last_cell_row_Omada2345, function($cells){
                        $cells->setFontSize(14);
                    })
                    ->cells('B2:B'.$last_cell_row_Omada2345, function($cells){
                        $cells->setFontSize(9);
                    })
                    ->getStyle('A1:X1')->getAlignment()->setTextRotation(90)->setWrapText(true)->setVertical(\PHPExcel_Style_Alignment::VERTICAL_BOTTOM);

                $sheet->cells('C1:X1', function($cells) {
                    $cells->setFontWeight('bold');
                    $cells->setFontSize(16);
                });

                $sheet->setHeight(1, 123);
                $sheet->setPageMargin(array(
                    0.55, 0.25, 0.25, 0.25
                ));

                $sheet->getHeaderFooter()->setOddHeader('&C&H&25 ΟΜΑΔΑ 2,3,4,5,6 - ΠΙΝΑΚΑΣ ΟΡΓΑΝΙΚΩΝ ΚΕΝΩΝ-ΠΛΕΟΝΑΣΜΑΤΩΝ: '. $date);
                $sheet->getHeaderFooter()->setOddFooter(Carbon::now()->format('d-m-Y H:i'));

                $sheet->getColumnDimension('A')->setWidth(6);
                $sheet->getColumnDimension('B')->setWidth(11);

                for($col='C'; $col !== 'Y'; $col++){
                    $sheet->setSize($col.'1',5,150);
                }

                for($row=2; $row <=$last_cell_row_Omada2345 ;$row++){
                    $sheet->getRowDimension($row)->setRowHeight(30);
                }

                $sheet->getStyle('A1:X'.$last_cell_row_Omada2345)->applyFromArray($styleArray);
                $sheet->getStyle('A2:X'.$last_cell_row_Omada2345)->getAlignment()->setVertical(\PHPExcel_Style_Alignment::VERTICAL_CENTER);
            });

            // Set the title
            $excel->setTitle('pinakas me organika kena - yperarithmies');

            // Chain the setters
            $excel->setCreator('PYSDE')
                ->setCompany('DDE Chanion');

            // Call them separately
            $excel->setDescription('PYSDE CHANION');

            $excel_file = ExcelFiles::create([
                'file_name' => str_replace('/','-',$date).'_ORGANIKAKENA-YPERARITHMIES.xls',
                'file_date' => \Carbon\Carbon::createFromFormat('d/m/Y', $date),
                'description' => $description
            ]);


        })->store('xls', storage_path('app/excel'))
            ->download('xls');
    }

    public function excelRequestPreparation()
    {
        $this->denies('create_excel_teachers_requests');

        return view('pysde.teachers.excelPreparationRequests');
    }

    public function excelTeachersRequests(Request $request)
    {
        $this->denies('create_excel_teachers_requests');

        $type = \Config::get('requests.aitisis_type')[$request->get('typeOfRequest')];
        $file_name = $request->get('file_name');
        $from = Carbon::createFromFormat('d/m/Y', $request->get('excel_date_from'))->format('Y-m-d 00:00:00');
        $to = Carbon::createFromFormat('d/m/Y', $request->get('excel_date_to'))->format('Y-m-d 23:59:59');
        $order = 'date_request';
        $orderByMoria = true;

        $aitisi_type = [
            'Οργανικές Υπεραρίθμων' =>   'Ονομαστικά Υπεράριθμος',
            'Βελτιώσεις'            =>  'Βελτιώση - Οριστική τοποθέτηση'
        ];

        if($type == 'Οργανικές Υπεραρίθμων' || $type == 'Βελτιώσεις'){
            $requests = OrganikiRequest::with('prefrences')
                ->where('aitisi_type', $aitisi_type[$type])
                ->whereNotNull('protocol_number')
                ->whereBetween('date_request', [$from, $to])
                ->orderBy($order)
                ->get();

            if ($requests->isEmpty()){
                return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
            }

            $requests = $requests->each(function($item, $key)use ($type){
                $item->klados = $item->teacher->myschool->new_klados;
                $item->klados_united = $item->teacher->myschool->new_klados_name;
                $item->full_name = $item->teacher->user->full_name;
                $item->moria = $item->teacher->teacherable->moria;
                $item->group_name = $item->teacher->teacherable->group_name;
                $item->organiki = $item->teacher->teacherable->organiki_name;
                $item->middle_name = $item->teacher->middle_name;
                $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->dimos_entopiotitas];
                $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->dimos_sinipiretisis];
                $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ': 'ΟΧΙ';
            });

            if ($orderByMoria){
                $requests = $requests->sortByDesc('moria');
//            $requests =  $sorted->values()->all();
            }

            $kladoiArray =  $requests->groupBy('klados_united');


            \Excel::create($file_name, function($excel) use($kladoiArray, $type){

                $kladoiArray->each(function($requests, $key) use($excel, $type){
                    $excel->sheet($key, function($sheet) use ($requests, $type, $key){
                        $sheet->loadView('pysde.teachers.Excel.excelTeachersRequestsORGANIKES')
                            ->setFontFamily('Arial')
                            ->setFontSize(10)
                            ->setFontBold(false)
                            ->with(compact('requests', 'type'))
                        ;
                        $sheet->getColumnDimension('P')
                            ->setAutoSize(false)
                            ->setWidth('3.2');

                        $sheet->setHeight(1, 123);

                        $sheet->setPageMargin(array(
                            1.5, 0.25, 0.25, 0.25
                        ));

                        $this->createCellPrefrenceForORGANIKES($requests, $sheet);

                        $sheet->setOrientation('landscape');


                        $sheet->getHeaderFooter()->setOddHeader('&C&B&H&25Κλάδος '.strtoupper($key))
                            ->setOddFooter('&L Συντάχθηκε από Σμυρναίο Νικόλαο στις &D &T &RΣελίδα &P από &N');

                        $sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,1);

                        //you must find the last row Nick
                        $sheet->getStyle('A1:P'.$this->getLastRow($requests))->applyFromArray($this->style_Array);
                        $sheet->getStyle('P2:P'.$this->getLastRow($requests))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                        foreach($sheet->getRowDimensions() as $rd) {
                            $rd->setRowHeight(-1);
                        }
                    });
                });

            })->download('xls');

            return 'ok organikes';

        }else{
            $requests = RequestTeacher::with('prefrences')
                ->where('aitisi_type', $type)
                ->whereNotNull('protocol_number')
                ->whereBetween('date_request', [$from, $to])
                ->orderBy($order)
                ->get();
        }



        if ($requests->isEmpty()){
            return redirect()->back()->withErrors('Δεν υπάρχουν αιτήσεις με τα συγκεκριμένα κριτήρια');
        }

        $requests = $requests->each(function($item, $key)use ($type){
            $item->klados = $item->teacher->klados_name;
            $item->klados_united = $item->teacher->klados_united;
            $item->full_name = $item->teacher->user->full_name;
            if ($type == 'E3' || $type == 'E4'){
                $item->moria = $item->teacher->teacherable->moria_apospasis;
            }else{
                $item->moria = $item->teacher->teacherable->moria;
                $item->group_name = $item->teacher->teacherable->group_name;
            }
            $item->organiki = $item->teacher->teacherable->organiki_name;
            $item->middle_name = $item->teacher->middle_name;
            $item->dimos_entopiotitas = \Config::get('requests.dimos')[$item->teacher->dimos_entopiotitas];
            $item->dimos_sinipiretisis = \Config::get('requests.dimos')[$item->teacher->dimos_sinipiretisis];
            $item->special_situation = $item->teacher->special_situation ? 'ΝΑΙ': 'ΟΧΙ';
        });


        if ($orderByMoria){
            $requests = $requests->sortByDesc('moria');
//            $requests =  $sorted->values()->all();
        }

        $kladoiArray =  $requests->groupBy('klados_united');

        \Excel::create($file_name, function($excel) use($kladoiArray, $type){

            $kladoiArray->each(function($requests, $key) use($excel, $type){
                $excel->sheet($key, function($sheet) use ($requests, $type, $key){
                    $sheet->loadView('pysde.teachers.Excel.excelTeachersRequest')
                        ->setFontFamily('Arial')
                        ->setFontSize(10)
                        ->setFontBold(false)
                        ->with(compact('requests', 'type'))
                    ;
                    $sheet->getColumnDimension('M')
                        ->setAutoSize(false)
                        ->setWidth('3.2');

                    $sheet->setHeight(1, 123);

                    $sheet->setPageMargin(array(
                        0.55, 0.25, 0.25, 0.25
                    ));

                    $this->createCellPrefrence($requests, $sheet);

                    $sheet->setOrientation('landscape');


                    $sheet->getHeaderFooter()->setOddHeader('Κλάδος '.strtoupper($key));

                    $sheet->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1,1);

                    //you must find the last row Nick
                    $sheet->getStyle('A1:N'.$this->getLastRow($requests))->applyFromArray($this->style_Array);
                    $sheet->getStyle('L2:L'.$this->getLastRow($requests))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

                    foreach($sheet->getRowDimensions() as $rd) {
                        $rd->setRowHeight(-1);
                    }
                });
            });

        })->download('xls');

        return 'ok';
    }

    public function excelPraxiPlacements($id)
    {
//        $this->denies('create_excel_teachers_requests'); //must change

        $praxi = Praxi::where('decision_number', $id)
            ->where('year_id', Year::where('current', true)->first()->id)
            ->first(); //$request->get('praxi')

//        return $praxi->decision_number;
//        return Carbon::parse($praxi->decision_date)->format('d/m/Y');
        $kladoi =  $praxi->placements->groupBy('klados');

        foreach($kladoi as $teachers){
            foreach($teachers as $teacher){
                if ($teacher->teacherable_type == 'App\Teacher'){
                    $t = Teacher::find($teacher->teacherable_id);
                    $teacher->full_name = $t->user->full_name;
                    $teacher->moria = $t->teacherable->moria;
                    $teacher->ypoxreotiko = $t->teacherable->orario;
                    $teacher->entopiotita = \Config::get('requests.dimos')[$t->dimos_entopiotitas];
                    $teacher->sinipiretisi = \Config::get('requests.dimos')[$t->dimos_sinipiretisis];
                    $teacher->special = $t->special_situation == 0 ? 'ΟΧΙ': 'ΝΑΙ';
                    $teacher->middle_name = $t->middle_name;

                }else{
                    $myschool = MySchoolTeacher::find($teacher->afm);
                    $teacher->full_name = $myschool->full_name;
                    $teacher->moria = $myschool->teacher != null ? $myschool->teacher->teacherable->moria : 0;
                    $teacher->ypoxreotiko = $myschool->ypoxreotiko;
                    $teacher->entopiotita = $myschool->teacher != null ? \Config::get('requests.dimos')[$myschool->teacher->dimos_entopiotitas] : 0;
                    $teacher->sinipiretisi = $myschool->teacher != null ? \Config::get('requests.dimos')[$myschool->teacher->dimos_sinipiretisis] : 0;
                    $teacher->special = $myschool->teacher != null ?  $myschool->teacher->special_situation == 0 ? 'ΟΧΙ': 'ΝΑΙ' : 'ΟΧΙ';
                    $teacher->middle_name = $myschool->middle_name;
                }
            }
        }

        \Excel::create('TOPOTHETISIS', function($excel) use($kladoi){

            $kladoi->each(function($klados, $key) use($excel){
                $excel->sheet($key, function($sheet) use ($klados){
                    $sheet->loadView('pysde.placements.excelPraxiTopothetisis')
                        ->setFontFamily('Arial')
                        ->setFontSize(13)
                        ->setFontBold(false)
                        ->with(compact('klados'));

                    $sheet->setPageMargin(array(
                        0.55, 0.25, 0.25, 0.25
                    ));
                    $sheet->setOrientation('landscape')
                        ->getStyle('A1:K1')
                        ->getFill()
                        ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                        ->getStartColor()
                        ->setARGB('FF808080');

                    $sheet->cells('A1:K1', function($cells){
                            $cells->setFontSize(16);
                        });

                    $sheet->getHeaderFooter()->setOddHeader('&C&H&25  ΚΛΑΔΟΣ ');

                    $sheet->getStyle('A1:K'.$this->getLastRow($klados))->applyFromArray($this->style_Array);

                    $sheet->setHeight(1, 30);

                    $height_array = array();
                    for($i=2; $i<=$this->getLastRow($klados); $i++){
                        $sheet->getStyle("K$i")->getAlignment()->setWrapText(true);  // στήλη που μπαίνουν οι προτιμήσεις
                        $height_array[$i] = 40;
                    }
                    $sheet->setHeight($height_array);
//                    $sheet->getStyle('L2:L'.$this->getLastRow($klados))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
                });
            });

        })->download('xls');

        return 'ok';
    }

    public function excelPraxiPlacements2($id)
    {
        //        $this->denies('create_excel_teachers_requests'); //must change

        $placementSheets = collect([
            'Προσωρ Τοποθέτηση & Συμπλήρωση' => collect(),
            'Προσωρινή Τοποθέτηση' => collect(),
            'Συμπλήρωση Ωραρίου' => collect(),
            'Απόσπαση' => collect(),
            'Πθμια' => collect(),
            'Ανάκληση' => collect(),
            'Unknown' => collect()
        ]);

        $praxi = Praxi::with(['placements' => function($p){
            $p->withTrashed();
        }])->where('decision_number', $id)
            ->where('year_id', Year::where('current', true)->first()->id)
            ->first();

//        return $praxi->decision_number;
//        return Carbon::parse($praxi->decision_date)->format('d/m/Y');

        $teachers =  $praxi->placements->groupBy('afm');

        foreach($teachers as $afm=>$teacher_placements){
            $type_list = $teacher_placements->pluck('placements_type')->all();

            $this->initializeTypes();
            $this->checkPlacementTypeOfTeacher($type_list);

            $title = $this->putTeacherToRightSheet($placementSheets, $afm);

            $teacher = $placementSheets[$title]->where('afm', "$afm")->first();

            if($teacher != null){                                                       // check if need it
                foreach($teacher_placements as $placement){
                    $teacher['placements']->push($placement);
                }
            }
        }

        \Excel::create('ΠΡΑΞΗ_'.$praxi->decision_number.'_'.$praxi->decision_date.'_ΤΟΠΟΘΕΤΗΣΕΙΣ', function($excel) use($placementSheets, $praxi){

            $placementSheets->each(function($teachers, $key) use($excel, $praxi){
                if(!$teachers->isEmpty()){

                    $teachers = $teachers->sort($this->compareCollection())->values();

                    $excel->sheet($key, function($sheet) use ($teachers, $key, $praxi){
                        $sheet->loadView('pysde.placements.excelPraxiTopothetisis2')
                            ->setFontFamily('Arial')
                            ->setFontSize(13)
                            ->setFontBold(false)
                            ->with(compact('teachers', 'key'));

                        $sheet->setPageMargin(array(
                            0.55, 0.25, 0.25, 0.25
                        ));


                        if($key == 'Προσωρ Τοποθέτηση & Συμπλήρωση')
                            $lastColumn = 'L';
                        elseif($key == 'Προσωρινή Τοποθέτηση')
                            $lastColumn = 'K';
                        elseif($key == 'Συμπλήρωση Ωραρίου')
                            $lastColumn = 'K';
                        elseif($key == 'Απόσπαση')
                            $lastColumn = 'K';
                        elseif($key == 'Ανάκληση')
                            $lastColumn = 'K';
                        elseif($key == 'Πθμια')
                            $lastColumn = 'J';
                        else
                            $lastColumn = 'K';

                        $sheet->setOrientation('landscape')
                            ->getStyle('A1:'.$lastColumn.'1')
                            ->getFill()
                            ->setFillType(\PHPExcel_Style_Fill::FILL_SOLID)
                            ->getStartColor()
                            ->setARGB('FF808080');

                        $sheet->cells('A1:'.$lastColumn.'1', function($cells){
                            $cells->setFontSize(16);
                        });

                        $sheet->getHeaderFooter()->setOddHeader('&C&H&25 '. $this->excelPlacementsSheetHeaders[$key] . $praxi->decision_number.'/'.$praxi->decision_date)
                                                 ->setOddFooter('&CΣελίδα &P από &N');


                        $sheet->getStyle('A1:'.$lastColumn.$this->getLastRow($teachers))->applyFromArray($this->style_Array);

                        $sheet->setHeight(1, 30);

                        $this->createCellTopothetisis($teachers, $sheet, $key);

                        $height_array = array();
                        for($i=2; $i<=$this->getLastRow($teachers); $i++){
                            $sheet->getStyle("J$i")->getAlignment()->setWrapText(true);
                            $sheet->getStyle("K$i")->getAlignment()->setWrapText(true);
                            if($key == 'Προσωρ Τοποθέτηση & Συμπλήρωση')
                                $sheet->getStyle("L$i")->getAlignment()->setWrapText(true);
                            $height_array[$i] = 40;
                        }
                        $sheet->setHeight($height_array);

                        $sheet->getStyle('B2:B'.$this->getLastRow($teachers))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);
                        $sheet->getStyle('C2:C'.$this->getLastRow($teachers))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);
                        $sheet->getStyle('J2:J'.$this->getLastRow($teachers))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);
                        $sheet->getStyle('K2:K'.$this->getLastRow($teachers))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);
                        if($key == 'Προσωρ Τοποθέτηση & Συμπλήρωση')
                            $sheet->getStyle('L2:L'.$this->getLastRow($teachers))->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT)->setIndent(1);

                        $sheet->setFreeze('E2');

                    });
                }
            });

        })->download('xls');

        return 'ok';
    }

    public function displayExcelTeachersRequests()
    {
        return view('pysde.teachers.Excel.excelTeachersRequest', compact('kladoiArray'));
    }

    protected function getLastRow($requests)
    {
        $i = 1;
        foreach($requests as $request){
            $i = $i + 1;
        }
        return $i;
    }

    private function createCellTopothetisis($teachers, $sheet, $key)
    {
        if($key == 'Προσωρ Τοποθέτηση & Συμπλήρωση'){
            $i = 2;
            foreach($teachers as $teacher){
                $cellDiathesis = '';
                $cellTopothetisis = '';
                $cellDescription = '';

                foreach($teacher['placements'] as $index=>$placement){
                    $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                    if(in_array($placement->placements_type, $this->placementsStringTypes['topothetisi'])){
                        $cellTopothetisis = $placement->to . ' για '. $placement->hours . 'Ωρ' . $days;
                        $cellDescription = $cellDescription . "$placement->description\n";
                    }elseif(in_array($placement->placements_type, $this->placementsStringTypes['diathesi'])){
                        if($teacher['placements']->last()->id == $placement->id){
                            $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days";
                            $cellDescription = $cellDescription . "$placement->description";
                        }else{
                            $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days\n";
                            $cellDescription = $cellDescription . "$placement->description\n";
                        }
                    }
                }
                $sheet->setCellValue("J$i", $cellTopothetisis);
                $sheet->setCellValue("K$i", $cellDiathesis);
                $sheet->setCellValue("L$i", $cellDescription);

                $i = $i + 1;
            }
        }
        elseif($key == 'Προσωρινή Τοποθέτηση'){
            $i = 2;
            foreach($teachers as $teacher){
                $cellTopothetisis = '';
                $cellDescription = '';

                foreach($teacher['placements'] as $index=>$placement){
                    $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                    if(in_array($placement->placements_type, $this->placementsStringTypes['topothetisi'])){
                        $cellTopothetisis = $placement->to . ' για '. $placement->hours . 'Ωρ' . $days;
                        $cellDescription = $cellDescription . "$placement->description";
                    }
                }
                $sheet->setCellValue("J$i", $cellTopothetisis);
                $sheet->setCellValue("K$i", $cellDescription);

                $i = $i + 1;
            }
        }

        elseif($key == 'Συμπλήρωση Ωραρίου'){
            $i = 2;
            foreach($teachers as $teacher){
                $cellDiathesis = '';
                $cellDescription = '';

                foreach($teacher['placements'] as $index=>$placement){
                    $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                    if(in_array($placement->placements_type, $this->placementsStringTypes['diathesi'])){
                        if($teacher['placements']->last()->id == $placement->id){
                            $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days";
                            $cellDescription = $cellDescription . "$placement->description";
                        }else{
                            $cellDiathesis = $cellDiathesis . "- $placement->to για $placement->hours Ωρ $days\n";
                            $cellDescription = $cellDescription . "$placement->description\n";
                        }
                    }
                }
                $sheet->setCellValue("J$i", $cellDiathesis);
                $sheet->setCellValue("K$i", $cellDescription);

                $i = $i + 1;
            }
        }
        elseif($key == 'Απόσπαση'){
            $i = 2;
            foreach($teachers as $teacher){
                $cellApospasis = '';
                $cellDescription = '';

                foreach($teacher['placements'] as $index=>$placement){
                    $days = $placement->days > 0 ? '/'.$placement->days.'Ημ' : '';
                    if(in_array($placement->placements_type, $this->placementsStringTypes['apospasi'])){
                        $cellApospasis = $placement->to . ' για '. $placement->hours . 'Ωρ' . $days;
                        $cellDescription = $cellDescription . "$placement->description";
                    }
                }
                $sheet->setCellValue("J$i", $cellApospasis);
                $sheet->setCellValue("K$i", $cellDescription);

                $i = $i + 1;
            }
        }

        elseif($key == 'Ανάκληση'){
            $i = 2;
            foreach($teachers as $teacher){
                $cellAnaklisis = '';
                $cellDescription = '';

                foreach($teacher['placements'] as $index=>$placement){
                    if(in_array($placement->placements_type, $this->placementsStringTypes['anaklisi'])){
                        $cellAnaklisis = 'ανάκληση από ' . $placement->to;
                        $cellDescription = "$placement->description";
                    }
                }
                $sheet->setCellValue("J$i", $cellAnaklisis);
                $sheet->setCellValue("K$i", $cellDescription);

                $i = $i + 1;
            }
        }
//        elseif($key == 'Πθμια')
//            $lastColumn = 'J';
    }
    private function createCellPrefrenceForORGANIKES($requests, $sheet)
    {
        $i = 2;
        foreach($requests as $request){
            $cell = '';
            foreach($request->prefrences as $prefrence){
                $cell = $cell . "($prefrence->order_number). $prefrence->OrganikiName\n";
            }
            $sheet->setCellValue("P$i", $cell);
            $sheet->getStyle("P$i")->getAlignment()->setWrapText(true);  // στήλη που μπαίνουν οι προτιμήσεις
            $sheet->getStyle("P$i")->getAlignment()->setWrapText(true);  // στήλη που μπαίνουν οι παρατηρήσεις



            $i = $i + 1;
        }
    }

    private function createCellPrefrence($requests, $sheet)
    {
        $i = 2;
        foreach($requests as $request){
            $cell = '';
            foreach($request->prefrences as $prefrence){
                $cell = $cell . "($prefrence->order_number). $prefrence->school_name\n";
            }
            $sheet->setCellValue("M$i", $cell);
            $sheet->getStyle("M$i")->getAlignment()->setWrapText(true);  // στήλη που μπαίνουν οι προτιμήσεις
            $sheet->getStyle("N$i")->getAlignment()->setWrapText(true);  // στήλη που μπαίνουν οι παρατηρήσεις



            $i = $i + 1;
        }
    }
    
    private function denies($permision)
    {
        if (Gate::denies($permision)){
            abort(403);
        }
    }

    /**
     * @param $prosorini_topothetisi
     * @param $diathesi
     * @param $apospasi
     * @param $anaklisi
     * @param $protovathimia
     */
    private function initializeTypes()
    {
        $this->prosorini_topothetisi = false;
        $this->diathesi = false;
        $this->apospasi = false;
        $this->anaklisi = false;
        $this->protovathimia = false;
    }

    /**
     * @param $type_list
     */
    private function checkPlacementTypeOfTeacher($type_list)
    {
        foreach ($type_list as $type) {
            if (in_array($type, $this->placementsStringTypes['topothetisi'])) {
                $this->prosorini_topothetisi = true;
            } elseif (in_array($type, $this->placementsStringTypes['diathesi'])) {
                $this->diathesi = true;
            } elseif (in_array($type, $this->placementsStringTypes['apospasi'])) {
                $this->apospasi = true;
            } elseif (in_array($type, $this->placementsStringTypes['anaklisi'])) {
                $this->anaklisi = true;
            } elseif (in_array($type, $this->placementsStringTypes['pvthmia'])) {
                $this->protovathimia = true;
            }
        }
    }

    /**
     * @param $placementSheets
     * @param $teacher
     */
    private function putTeacherToRightSheet($placementSheets , $afm)
    {
        $title = 'a';
        if ($this->prosorini_topothetisi && $this->diathesi) {
            $placementSheets['Προσωρ Τοποθέτηση & Συμπλήρωση']->push($this->teacherAttributes($afm));
            $title = 'Προσωρ Τοποθέτηση & Συμπλήρωση';
        } elseif ($this->prosorini_topothetisi && !$this->diathesi) {
            $placementSheets['Προσωρινή Τοποθέτηση']->push($this->teacherAttributes($afm));
            $title = 'Προσωρινή Τοποθέτηση';
        } elseif (!$this->prosorini_topothetisi && $this->diathesi) {
            $placementSheets['Συμπλήρωση Ωραρίου']->push($this->teacherAttributes($afm));
            $title = 'Συμπλήρωση Ωραρίου';
        } elseif ($this->apospasi) {
            $placementSheets['Απόσπαση']->push($this->teacherAttributes($afm));
            $title = 'Απόσπαση';
        } elseif ($this->anaklisi) {
            $placementSheets['Ανάκληση']->push($this->teacherAttributes($afm));
            $title = 'Ανάκληση';
        } elseif ($this->protovathimia) {
            $placementSheets['Πθμια']->push($this->teacherAttributes($afm));
            $title = 'Πθμια';
        } else {
            $placementSheets['Unknown']->push($this->teacherAttributes($afm));
            $title = 'Unknown';
        }

        return $title;
    }

    /**
     * @param $teacher
     */
    private function teacherAttributes($afm)
    {
        $myschool = MySchoolTeacher::find($afm);

        return collect([
            'afm'       => $myschool->afm,
            'organiki'  => $myschool->organiki_name,
            'eidikotita'=> $myschool->eidikotita,
            'full_name' => $myschool->full_name,
            'moria'     => $myschool->teacher != null ? $myschool->teacher->teacherable->moria : 0,
            'ypoxreotiko'   => $myschool->ypoxreotiko,
            'entopiotita'   => $myschool->teacher != null ? \Config::get('requests.dimos')[$myschool->teacher->dimos_entopiotitas] : 0,
            'sinipiretisi'  => $myschool->teacher != null ? \Config::get('requests.dimos')[$myschool->teacher->dimos_sinipiretisis] : 0,
            'special'       => $myschool->teacher != null ? $myschool->teacher->special_situation == 0 ? 'ΟΧΙ' : 'ΝΑΙ' : 'ΟΧΙ',
            'middle_name'   => $myschool->middle_name,
            'placements'    => collect()
        ]);
    }

    private function compareCollection()
    {
        return function ($a, $b) {
            foreach($this->sort as $sortField=>$orderType){
                if($a[$sortField] < $b[$sortField]){
                    return $orderType === 'asc' ? -1 : 1;
                }elseif($a[$sortField] > $b[$sortField]){
                    return $orderType === 'asc' ? 1 : -1;
                };
            }
            return 0;
        };
    }
}

