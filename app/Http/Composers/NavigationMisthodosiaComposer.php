<?php

namespace App\Http\Composers;

use App\RequestTeacher;
use App\Teacher;
use Illuminate\Contracts\View\View;

class NavigationMisthodosiaComposer{

    protected $numberOfTeachersForCheckingMisthodosia;

    public function __construct()
    {
        $this->numberOfTeachersForCheckingMisthodosia = Teacher::where('misthodosia_is_checked', 0)->count();

    }

    public function compose(View $view)
    {
        $view->with('numberOfTeachersForCheckingMisthodosia', $this->numberOfTeachersForCheckingMisthodosia);
    }
}