<?php

namespace App\Http\Composers;

use App\RequestTeacher;
use App\Teacher;
use Illuminate\Contracts\View\View;

class NavigationComposer{

    protected $numberOfTeachers;
    protected $numberOfRequests;

    public function __construct()
    {
        $this->numberOfTeachers = Teacher::where('is_checked', 0)->count();
        $this->numberOfRequests = RequestTeacher::where('protocol_number', null)->count();
    }

    public function compose(View $view)
    {
        $view->with('numberOfTeachers', $this->numberOfTeachers)
            ->with('numberOfRequests', $this->numberOfRequests);
    }
}