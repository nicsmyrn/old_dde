<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'role' =>\App\Http\Middleware\CheckRole::class,
        'pysde'=> \App\Http\Middleware\isPysde::class,
        'teacher_activation_ckeck' => \App\Http\Middleware\ActivatedForRegister::class,
        'teacher_can_view_eidikotita' => \App\Http\Middleware\TeacherCanViewHisEidikotita::class,
        'misthodosia'   => \App\Http\Middleware\MisthodosiaAdmin::class,
        'isLykeio'  => \App\Http\Middleware\CheckSchoolType::class,
        'isPrimary' => \App\Http\Middleware\isPrimaryOrGymnasiumSchool::class,
        'ofaAdmin'  => \App\Http\Middleware\OfaAdmin::class
    ];
}
