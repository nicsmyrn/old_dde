<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class isPysde
{
    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()) {
            if(!$request->user()->role->can_access){
                abort(403);
            }
            if ($request->user()->isRole('pysde_secretary') || $request->user()->isRole('pysde_helper_secretary')) {
                return $next($request);
            }
            abort(403);
        }
        if ($request->ajax()) {
            abort(403);
        } else {
            return redirect()->guest('auth/login');
        }
    }
}
