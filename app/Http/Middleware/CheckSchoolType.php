<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class CheckSchoolType
{
    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()) {
            if ($request->user()->isRole('school')) {
                if($this->isLykeio($request->user()->userable)){
                    return $next($request);
                }
            }
            abort(403);
        }
        if ($request->ajax()) {
            abort(403);
        } else {
            return redirect()->guest('auth/login');
        }
    }

    private function isLykeio($school){
        if($school->type == 'Λύκειο' || $school->type == 'ΕΠΑΛ'){
            return true;
        }
        return false;
    }
}
