<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class CheckRole
{
    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        if ($this->auth->user()) {
            if ($request->user()->isRole($role)) {
                if(!$request->user()->role->can_access){
                    abort(403);
                }
                // if ($role == 'school'){
                //     abort(503);
                // }
                return $next($request);
            }
            abort(403);
        }
        if ($request->ajax()) {
            abort(403);
        } else {
            return redirect()->guest('auth/login');
        }
    }
}
