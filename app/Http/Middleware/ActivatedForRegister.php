<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;

class ActivatedForRegister
{

    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()) {
            if(!$request->user()->role->can_access){
                abort(403);
            }
            if ($request->user()->isRole('teacher')) {
                if ($request->user()->userable != null){
                    if($request->user()->userable->activation_for_register == 1){
                        return $next($request);
                    }
                }
            }
            flash()->overlayE('Προσοχή!', 'Για να μπεις στο σύστημα αιτήσεων πρέπει να συμπληρώσεις υποχρεωτικά τα στοιχεία του Προφίλ σου');
            return redirect()->route('User::getProfile');
        }
        if ($request->ajax()) {
            abort(403);
        } else {
            return redirect()->guest('auth/login');
        }
    }
}
