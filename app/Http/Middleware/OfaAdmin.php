<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class OfaAdmin
{
    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    public function handle($request, Closure $next)
    {
        if ($this->auth->user()) {
            if(!$request->user()->role->can_access){
                abort(403);
            }
            if ($request->user()->isRole('ofa')) {
                return $next($request);
            }
            abort(403);
        }
        if ($request->ajax()) {
            abort(403);
        } else {
            return redirect()->guest('auth/login');
        }
    }
}
