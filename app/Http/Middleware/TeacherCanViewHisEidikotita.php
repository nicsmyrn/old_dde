<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Guard;

class TeacherCanViewHisEidikotita
{
    protected $auth;

    public function __construct(Guard $auth){
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->user()) {
            if(!$request->user()->role->can_access){
                abort(403);
            }
            if ($request->user()->isRole('teacher')) {
                if ($request->user()->userable != null){
                    if($request->user()->userable->is_checked == 1){
                        return $next($request);
                    }
                }
            }
            flash()->overlayE('Προσοχή!', 'Για να δεις τα κενά της ειδικότητάς σου θα πρέπει η υπηρεσία να ελέγξει την ορθότητα της ειδικότητας');
            return redirect()->back();
        }
        if ($request->ajax()) {
            abort(403);
        } else {
            return redirect()->guest('auth/login');
        }
    }
}
