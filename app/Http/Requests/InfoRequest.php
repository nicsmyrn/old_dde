<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InfoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:40|min:3',
            'last_name' => 'required|max:45|min:3'
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => 'Το όνομα είναι υποχρεωτικό',
            'last_name.required' => 'Το Επώνυμο είναι υποχρεωτικό',
            'last_name.max'     =>'Το Επώνυμο δεν μπορεί να ξεπερνάει τους 45 χαρακτήρες',
            'first_name.max'     =>'Το Όνομα δεν μπορεί να ξεπερνάει τους 40 χαρακτήρες',
            'last_name.min'     =>'Το Επώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'first_name.min'     =>'Το Όνομα πρέπει να ειναι τουλάχιστον 3 χαρακτήρες',
        ];
    }
}
