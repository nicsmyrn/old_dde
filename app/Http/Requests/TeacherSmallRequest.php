<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TeacherSmallRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'digits:10',
            'mobile'=> 'digits:10|required',
            'address'=> 'required|min:3',
            'address_number'    => 'required',
            'city' => 'required|min:3',
            'tk' => 'required|digits:5'
        ];
    }

    public function messages()
    {
        return [
            'phone.digits'           =>  'Το σταθερό τηλέφωνο πρέπει να αποτελείται από 10 αριθμητικά ψηφία',
            'mobile.digits'          =>  'Το κινητό τηλέφωνο πρέπει να αποτελείται από 10 αριθμητικά ψηφία',
            'mobile.required'           =>  'Το κινητό τηλέφωνο είναι υποχρεωτικό',
            'address.required'          =>  'Η διεύθυνση είναι υποχρεωτική',
            'address.min'               =>  'Η διεύθυνση πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'city.required'             =>  'Η πόλη είναι υποχρεωτική',
            'city.min'                  =>  'Η πόλη πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'tk.required'                   =>  'Ο ΤΚ είναι υποχρεωτικός',
            'tk.digits'                   =>  'Ο ΤΚ πρέπει να είναι 5 ψηφίων',
            'address_number.required'   => 'Ο Αριθμός της Οδού είναι υποχρεωτικός'
        ];
    }
}
