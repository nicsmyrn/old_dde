<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Contracts\Auth\Guard;

class TeacherRequest extends Request
{
    private $user;
    private $teacherableId;

    public function __construct(Guard $auth)
    {
        $this->user = $auth->user();
        if(isset($this->user->userable->teacherable)){
            $this->teacherableId = $this->user->userable->teacherable->id;
        }else{
            $this->teacherableId = 0;
        }
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $teacher = array(
            'middle_name' => 'required|alpha',
            'klados_id' => 'required|integer',
//            'teacherable_id' => 'required|integer',
//            'teacherable_type' => 'required',
            'phone' => 'digits:10',
            'mobile'=> 'digits:10|required',
            'family_situation' => 'required|in:0,1,2,3,4,5',
            'childs' => 'integer|between:0,12',
            'special_situation' => 'required|in:0,1',
            'address'=> 'required|min:3',
            'city' => 'required|min:3',
            'tk' => 'size:5',
            'dimos_sinipiretisis' => 'required',
            'dimos_entopiotitas' => 'required',
            'years_experience' => 'numeric|between:0,40',
//            'attachment1'   => 'mimes:pdf,jpg,png|max:2048',
//            'attachment2'   => 'mimes:pdf,jpg,png|max:2048',
//            'attachment3'   => 'mimes:pdf,jpg,png|max:2048'
        );

        $monimos = array(
            'am'    => 'required|digits:6|unique:monimoi,am,'. $this->teacherableId,
//            'am'    => 'required|digits:6',//|unique:monimoi,am,'.$this->request->get('am'),
            'organiki' => 'required|integer',
            'county' => 'required|integer',
            'moria' =>  'numeric|between:0,400',
            'orario'    => 'required|integer|between:1,30'
        );

        $anaplirotis = array(
            'afm'   => 'required|digits:9',//|unique:anaplirotes:afm,'.$this->request->get('am'),
            'seira_topothetisis' => 'integer',
            'type' => 'integer',
            'moria'=> 'numeric|between:0,400'
        );

        $school = array();

        if ($this->user()->userable_type == 'App\Teacher'){
            if($this->request->get('teacher_type') == 0){   //ΜΟΝΙΜΟΣ
                return $teacher + $monimos;
            }elseif($this->request->get('teacher_type') == 1){ // ΑΝΑΠΛΗΡΩΤΗΣ
                return $teacher + $anaplirotis;
            }
//            if(isset($this->user()->userable)){
//                if($this->user()->userable->teacherable_type == 'App\Monimos'){
//                    return $teacher + $monimos;
//                }elseif($this->user()->userable->teacherable_type == 'App\Anaplirotis'){
//                    return $teacher + $anaplirotis;
//                }
//            }else{
//                return $teacher + $monimos;
//            }
        }elseif($this->user()->userable_type == 'App\School'){
            return [];
        }

    }

    public function messages()
    {
        return [
            'middle_name.required'      =>  'Το Πατρώνυμο είναι υποχρεωτικό',
            'middle_name.alpha'         =>  'Το Πατρώνυμο πρέπει να είναι μόνο αλφαβητικά γράμματα',
            'klados_id.required'        =>  'Ο Κλάδος είναι υποχρεωτικός',
            'klados_id.integer'         =>  'Ο Κλάδος δεν είναι σωστός',
            'phone.digits'           =>  'Το σταθερό τηλέφωνο πρέπει να αποτελείται από 10 αριθμητικά ψηφία',
            'mobile.digits'          =>  'Το κινητό τηλέφωνο πρέπει να αποτελείται από 10 αριθμητικά ψηφία',
            'mobile.required'           =>  'Το κινητό τηλέφωνο είναι υποχρεωτικό',
            'family_situation.required' =>  'Η οικογενειακή κατάσταση είναι υποχρεωτική',
            'family_situation.in'       =>  'Η οικογενειακή κατάσταση ΔΕΝ είναι σωστή',
            'childs.required'           =>  'Ο αριθμός των παιδιών είναι υποχρεωτικός',
            'childs.integer'            =>  'Ο αριθμός των παιδιών πρέπει να είναι αριθμός',
            'childs.between'            =>  'Ο αριθμός των παιδιών πρεπει να είναι αριθμός μεταξύ 0 και 12',
            'special_situation.required' => 'Η Ειδική Κατηγορία είναι υποχρεωτικό πεδίο',
            'special-situation.in'      =>  'Η Ειδική Κατηγορία ΔΕΝ είναι σωστή',
            'address.required'          =>  'Η διεύθυνση είναι υποχρεωτική',
            'address.min'               =>  'Η διεύθυνση πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'city.required'             =>  'Η πόλη είναι υποχρεωτική',
            'city.min'                  =>  'Η πόλη πρέπει να έχει τουλάχιστον 3 χαρακτήρες',
            'tk.size'                   =>  'Ο ΤΚ πρέπει να είναι 5 ψηφίων',
            'dimos_sinipiretis.required'=>  'Ο Δήμος Συνηπηρέτησης είναι υποχρεωτικός',
            'dimos_entopiotitas.required'=> 'Ο Δήμος Εντοπιότητας είναι υποχρεωτικός',
            'years_experience.numeric'  =>  'Τα χρόνια προϋπηρεσίας πρέπει να είναι αριθμός',
            'years_experience.between'  =>  'Τα χρόνια προϋπηρεσίας πρέπει να είναι μεταξύ 0 και 40',

            'attachment1.mimes'  => 'Επιτρέπονται ΜΟΝΟ αρχεία τύπου: PDF, JPG, PNG',
            'attachment1.max'  => 'Το μέγεθος δεν μπορεί να ξεπερνάει τα 2 MB',
            'attachment2.mimes'  => 'Επιτρέπονται ΜΟΝΟ αρχεία τύπου: PDF, JPG, PNG',
            'attachment2.max'  => 'Το μέγεθος δεν μπορεί να ξεπερνάει τα 2 MB',
            'attachment3.mimes'  => 'Επιτρέπονται ΜΟΝΟ αρχεία τύπου: PDF, JPG, PNG',
            'attachment3.max'  => 'Το μέγεθος δεν μπορεί να ξεπερνάει τα 2 MB',


            'am.required'   => 'Ο Αριθμός Μητρώου είναι υποχρεωτικός',
            'am.digits'       => 'Ο Αριθμός Μητρώου πρέπει να είναι έξι (6) αριθμητικά ψηφία',
            'am.unique'       => 'Ο Αριθμός Μητρώου υπάρχει ήδη από άλλον χρήστη',
            'organiki.required'      => 'Η οργανική θέση είναι υποχρεωτική',
            'organiki.integer'       => 'Η οργανική δεν είναι σωστή',
            'county.required'      => 'Ο Νομός είναι υποχρεωτικός',
            'county.integer'        => 'Ο Νομός δεν είναι σωστός',
            'moria.numeric'         => 'Τα μόρια πρέπει να είναι αριθμός',
            'moria.between'         => 'Τα μόρια πρέπει να είναι μεταξύ 0  και 400',
            'orario.required'       => 'Το υποχρεωτικό ωράριο είναι υποχρεωτικό',
            'orario.integer'        => 'Το υποχρεωτικό ωράριο πρέπει να είναι αριθμός',
            'orario.between'        => 'Το υποχρεωτικό ωράριο μπορεί να είναι μεταξύ 1 και 24 ώρες',

            'afm.required' => 'Το ΑΦΜ είναι υποχρεωτικό',
            'afm.digits'     => 'Το ΑΦΜ πρέπει να είναι εννέα (9) αριθμητικά ψηφία',
            'afm.unique'     => 'Το ΑΦΜ υπάρχει ήδη από άλλον χρήστη',
            'seira_topothetisis.integer' => 'Η σειρά τοποθέτησης δεν είναι σωστή',
            'type.integer'  => 'Ο τύπος δεν είναι σωστό',

        ];
    }
}

