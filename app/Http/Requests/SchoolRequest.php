<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SchoolRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'eidikot' => 'required|array',
            'eidikis' => 'array'
        ];
    }
    
    public function messages()
    {
        return [
            'eidikot.required' => 'Τα στοιχεία είναι υποχρεωτικά',
            'eidikot.array' => 'Τα στοιχεία είναι σε μορφή πίνακα'
            ];
    }
}
