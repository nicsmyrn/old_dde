<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6'
        ];
    }
    
    public function messages()
    {
        return [
            'first_name.required' => 'Το όνομα είναι υποχρεωτικό',
            'last_name.required' => 'Το Επώνυμο είναι υποχρεωτικό',
            'email.required' => 'Το E-mail είναι υποχρεωτικό',
            'email.email' => 'Δεν είναι σωστό το E-mail',
            'email.users' => 'Το E-mail υπάρχει',
            'password.required' => 'Ο κωδικός είναι υποχρεωτικός',
            'password.min' => 'Ο κωδικός πρέπει να είναι τουλάχιστον 6 χαρακτήρες',
            'password.confirmed' => 'Δεν ταιριάζουν οι κωδικοί',
        ];
    }
}
