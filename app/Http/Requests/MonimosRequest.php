<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MonimosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'am'    => 'required|alpha_num|size:6',
            'organiki' => 'required|integer',
            'county' => 'required|integer',
            'moria' =>  'numeric|between:0,400',
            'orario'    => 'required|integer|between:1,24'
        ];
    }

    public function messages()
    {
        return [
            'am.required'   => 'Ο Αριθμός Μητρώου είναι υποχρεωτικός',
            'am.alpha_num'  => 'Ο Αριθμός Μητρώου πρέπει να είναι αριθμητικά ψηφία',
            'am.size'       => 'Ο Αριθμός Μητρώου πρέπει να είναι έξι (6) αριθμητικά ψηφία',
            'organiki.required'      => 'Η οργανική θέση είναι υποχρεωτική',
            'organiki.integer'       => 'Η οργανική δεν είναι σωστή',
            'county.required'      => 'Ο Νομός είναι υποχρεωτικός',
            'county.integer'        => 'Ο Νομός δεν είναι σωστός',
            'orario.required'       => 'Το υποχρεωτικό ωράριο είναι υποχρεωτικό',
            'orario.integer'        => 'Το υποχρεωτικό ωράριο πρέπει να είναι αριθμός',
            'orario.between'        => 'Το υποχρεωτικό ωράριο μπορεί να είναι μεταξύ 1 και 24 ώρες'
        ];
    }
}
