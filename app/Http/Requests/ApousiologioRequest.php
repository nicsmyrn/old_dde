<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ApousiologioRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
        ];

        foreach ($this->request->get('absencesCollection') as $k=>$v){
            $rules['absencesCollection.'.$k.'.days_apousias'] = 'required|integer|between:0,31';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];

        foreach ($this->request->get('absencesCollection') as $k=>$v){
            $messages['absencesCollection.'.$k.'.days_apousias.required'] = 'Οι ημέρες των απουσιών  είναι υποχρεωτικό πεδίο';
            $messages['absencesCollection.'.$k.'.days_apousias.integer'] = 'Οι ημέρες των απουσιών πρέπει να είναι ακέραιος';
            $messages['absencesCollection.'.$k.'.days_apousias.between'] = 'Οι ημέρες των απουσιών πρέπει να είναι ακέραιος μεταξύ 0 και 31';
        }

        return $messages;
    }
}
