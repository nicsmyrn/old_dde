<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Year;

class StudentFormRequest extends Request
{
    protected  $current_year;
    protected $min_year;
    protected $max_year;

    public function __construct()
    {
        $this->current_year = (int)substr(Year::where('current', true)->first()->name, -4);

        $this->max_year = $this->current_year - 8;
        $this->min_year = $this->current_year - 18;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'am'            => 'required|integer',
            'last_name'     => 'required|min:3',
            'first_name'    => 'required|min:3',
            'middle_name'   => 'required|min:3',
            'mothers_name'  => 'required|min:3',
            'class'         => 'required|integer|between:1,6',
            'sex'           => 'required|integer|between:0,1',
            'year_birth'    => 'required|integer|between:'.$this->min_year.','.$this->max_year
        ];
    }

    public function messages()
    {
        return  [
            'am.required' => 'Ο αριθμός Μητρώου είναι υποχρεωτικός',
            'last_name.required' => 'Το Επώνυμο είναι υποχρεωτικό',
            'first_name.required' => 'Το Όνομα είναι υποχρεωτικό',
            'middle_name.required' => 'Το Πατρώνυμο είναι υποχρεωτικό',
            'mothers_name.required' => 'Το Μητρώνυμο είναι υποχρεωτικό',
            'class.required' => 'Η Τάξη είναι υποχρεωτική',
            'sex.required' => 'Το Φύλο είναι  υποχρεωτικό',
            'year_birth.required' => 'Το έτος γέννησης είναι υποχρεωτικό',

            'am.integer' => 'Ο αριθμός Μητρώου πρέπει να είναι ακέραιος αριθμός',
            'class.integer' => 'Η τάξη πρέπει να είναι ακέραιος αριθμός',
            'sex.integer' => 'Το φύλο πρέπει να είναι ακέραιος αριθμός',
            'year_birth.integer' => 'Το έτος γέννησης πρέπει να είναι ακέραιος αριθμός',

            'last_name.min' => 'Το Επώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'first_name.min' => 'Το Όνομα πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'middle_name.min' => 'Το Πατρώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',
            'mothers_name.min' => 'Το Μητρώνυμο πρέπει να είναι τουλάχιστον 3 χαρακτήρες',

            'class.between' => 'Η τάξη πρέπει να είναι ή Α ή Β ή Γ',
            'sex.between' => 'Το φύλο πρέπει να είναι ή κορίτσι ή αγόρι',
            'year_birth.between' => 'Το έτος γέννησης μπορεί να είναι μεταξύ '. $this->min_year . ' και '. $this->max_year,

        ];
    }
}
