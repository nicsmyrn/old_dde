<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TeacherRegistrationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name'             => 'required|min:3',
            'first_name'            => 'required|min:3',
            'email'                 => 'required|email|unique:users,email',
            'password'              => 'required|confirmed|min:6|regex:/((?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^()<>.,+?-_]).{6,20})/',
            'g-recaptcha-response'  => 'required|checkcaptcha'
        ];
    }

    public function messages()
    {
        return [
            'g-recaptcha-response.required' => 'Πρέπει να επιλέξετε το reCAPTCHA',
            'g-recaptcha-response.checkcaptcha'              => 'Το captcha δεν είναι έγκυρο',
            'last_name.required'        => 'Το Επώνυμο είναι υποχρεωτικό',
            'last_name.min'             => 'Το Επώνυμο πρέπει να έχει τουλάχιστον 3 γράμματα',
            'first_name.required'       => 'Το Όνομα είναι υποχρεωτικό',
            'first_name.min'            => 'Το Όνομα πρέπει να έχει τουλάχιστον 3 γράμματα',
            'email.required'            => 'Το E-mail είναι υποχρεωτικό',
            'email.email'               => 'Το E-mail δεν είναι σωστό',
            'email.unique'              => 'Το E-mail υπάρχει ήδη',
            'password.required'         => 'Ο κωδικός είναι υποχρεωτικός',
             'password.min'             => 'Ο κωδικός πρέπει να έχει τουλάχιστον 6 χαρακτήρες (γράμματα - αριθμούς - σύμβολα)',
            'password.confirmed'        => 'Οι κωδικοί δεν ταιριάζουν',
            'password.regex'            => 'Ο Κωδικός πρέπει να αποτελείται από λατινικούς χαρακτήρες, νούμερα  και τουλάχιστον ένα από τα σύμβολα !@#$%^()<>.,+?-_'

        ];
    }
}

