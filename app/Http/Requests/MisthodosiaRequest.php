<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MisthodosiaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'at' => 'required|alpha_num|between:6,10',
            'birth' => 'required|date_format:d/m/Y',
            'iban' => 'required|size:27',
            'bank' => 'required|integer',
            'amka' => 'required|digits:11',
            'doy' => 'required|integer',
            'am_tsmede' => 'integer',
            'am_ika' => 'required|integer',
            'mk' => 'integer|between:1,30',
            'tekna' => 'array',
            'teknaStartCollege' => 'array',
            'teknaEndCollege' => 'array',
            'description'   => 'string',
            'attachment1'   => 'mimes:pdf,jpg,png|max:2048',
            'attachment2'   => 'mimes:pdf,jpg,png|max:2048',
            'attachment3'   => 'mimes:pdf,jpg,png|max:2048'
        ];
    }

    public function messages()
    {
        return  [
            'at.required' => 'Ο αριθμός της Αστυνομικής Ταυτότητας είναι υποχρεωτικός',
            'birth.required' => 'Η ημερομηνία γέννησης είναι υποχρεωτική',
            'iban.required' => 'Το IBAN της Τράπεζας είναι υποχρεωτικό',
            'bank.required' => 'Το όνομα της Τράπεζας είναι υποχρεωτικό',
            'amka.required' => 'Το ΑΜΚΑ είναι υποχρεωτικό',
            'doy.required' => 'Η Δ.Ο.Υ. είναι υποχρεωτική',
            'at.alpha_num'  => 'Η ταυτότητα πρέπει να είναι αλφαριθμητική χαρακτήρες',
            'at.between'    => 'Η ταυτότητα πρέπει να είναι μεταξύ 6 και 10 χαρακτήρων',
            'birth.date_format'         => 'Η ημερομηνία γέννησης πρέπει να είναι ως 10/10/2000',
            'iban.size'     => 'Το IBAN πρέπει να έχει ακριβώς 27 χαρακτήρες',
            'bank.integer'    => 'Το πεδίο τράπεζα πρέπει να είναι αριθμός',
            'amka.digits'  => 'Το ΑΜΚΑ πρέπει να είναι αριθμός με 11 ψηφία',
            'doy.integer'           => 'Η ΔΟΥ πρέπει να είναι αριθμός',
            'am_tsmede.integer'     => 'Ο ΑΜ ΤΣΜΕΔΕ πρέπει να είναι αριθμός',
            'am_ika.required'     => 'Ο ΑΜ IKA είναι υποχρεωτικός',
            'am_ika.integer'     => 'Ο ΑΜ IKA πρέπει να είναι αριθμός',
            'mk.integer'     => 'Το ΜΚ πρέπει να είναι αριθμός',
            'mk.between'    => 'Το ΜΚ πρέπει να είναι μεταξύ 1 και 30 ψηφίων',
            'tekna.array'         => 'Τα τέκνα πρέπει να είναι τύπου πίνακα',
            'teknaStartCollege.array'         => 'Τα τέκνα2 πρέπει να είναι τύπου πίνακα',
            'teknaEndCollege.array'         => 'Τα τέκνα3 πρέπει να είναι τύπου πίνακα',
            'description.string'           => 'Η ειδική περίπτωση πρέπει να είναι τύπου κειμένου',
            'attachment1.mimes'  => 'Επιτρέπονται ΜΟΝΟ αρχεία τύπου: PDF, JPG, PNG',
            'attachment1.max'  => 'Το μέγεθος δεν μπορεί να ξεπερνάει τα 2 MB',
            'attachment2.mimes'  => 'Επιτρέπονται ΜΟΝΟ αρχεία τύπου: PDF, JPG, PNG',
            'attachment2.max'  => 'Το μέγεθος δεν μπορεί να ξεπερνάει τα 2 MB',
            'attachment3.mimes'  => 'Επιτρέπονται ΜΟΝΟ αρχεία τύπου: PDF, JPG, PNG',
            'attachment3.max'  => 'Το μέγεθος δεν μπορεί να ξεπερνάει τα 2 MB',
        ];
    }
}
