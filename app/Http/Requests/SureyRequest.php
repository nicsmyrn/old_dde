<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SureyRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'total' => 'required|integer',
            'studentpermeters' => 'required|numeric',

            'ergG_year' => 'integer|between:1900,2015',
            'ergG_status' => 'in:1,2,3',

            'ergP_year' => 'integer|between:1900,2015',
            'ergP_status' => 'in:1,2,3',

            'ergX_year' => 'integer|between:1900,2015',
            'ergX_status' => 'in:1,2,3',

            'ergL_year' => 'integer|between:1900,2015',
            'ergL_status' => 'in:1,2,3',

            'faucet' => 'required|numeric',
            'wc' => 'required|numeric',

            'double_hours' => 'in:1',
            'yard_check' => 'in:1',
            'fire_check' => 'in:1',
            'earthquake_check' => 'in:1',
        ];
    }

    public function messages()
    {
        return [
            'total.required' => 'Η δυναμικότητα είναι υποχρεωτική',
            'total.integer' => 'Η δυναμικότητα πρέπει να είναι ακέραιος αριθμός.',

            'studentpermeters.required' => 'Ο αριθμός μαθητής ανά τετραγωνικό μέτρο είναι υποχρεωτικός',
            'studentpermeters.numeric' => 'Ο  μαθητής ανά τετραγωνικό μέτρο πρέπει να είναι αριθμός. Για υποδιαστολή χρησιμοποιείστε την . και όχι το ,',

            'ergG_year.integer' => 'Η χρονολογία του Γυμναστηρίου πρέπειν να είναι αριθμός',
            'ergG_year.between' => 'Χρονολογία Γυμναστηρίου μεταξύ 1900 - 2015',
            'ergG_status.in'    => 'Status Γυμναστηρίου μεταξύ 1-3',


            'ergP_year.integer' => 'Η χρονολογία του εργαστηρίου Πληροφορικής πρέπειν να είναι αριθμός',
            'ergP_year.between' => 'Χρονολογία του εργαστηρίου Πληροφορικής μεταξύ 1900 - 2015',
            'ergP_status.in'    => 'Status του εργαστηρίου Πληροφορικής μεταξύ 1-3',

            'ergX_year.integer' => 'Η χρονολογία του εργαστηρίου Χημείας πρέπειν να είναι αριθμός',
            'ergX_year.between' => 'Χρονολογία του εργαστηρίου Χημείας μεταξύ 1900 - 2015',
            'ergX_status.in'    => 'Status του εργαστηρίου Χημείας μεταξύ 1-3',

            'ergL_year.integer' => 'Η χρονολογία της Βιβλιοθήκης πρέπειν να είναι αριθμός',
            'ergL_year.between' => 'Χρονολογία της Βιβλιθήκης μεταξύ 1900 - 2015',
            'ergL_status.in'    => 'Status βιβλιοθήκης μεταξύ 1-3',


            'faucet.required'   => 'Ο αριθμός βρύσες ανά μαθητή είναι υποχρεωτικός',
            'faucet.numeric'    => 'Οι βρύσες ανά μαθητή πρέπει να είναι αριθμός. Για υποδιαστολή χρησιμοποιείστε την . και όχι το ,',

            'wc.required'   => 'Ο αριθμός τουαλέτες ανά μαθητή είναι υποχρεωτικός',
            'wc.numeric'    => 'Οι τουαλέςτες ανά μαθητή πρέπει να είναι αριθμός. Για υποδιαστολή χρησιμοποιείστε την . και όχι το ,'
        ];
    }
}
