<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'middle_name'           => 'required',
            'klados_id'             => 'required',
            'years_experience'      => '',
            'am'                    => '',
            'orario'                => '',
            'organiki'              => '',
            'moria'                 => '',
            'family_situation'      => '',
            'childs'                => '',
            'special_situation'     => '',
            'dimos_sinipiretisis'   => '',
            'dimos_entopiotitas'    => '',
            'address'               => '',
            'mobile'                => '',
            'city'                  => '',
            'phone'                 => '',
            'tk'                    => ''
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}

