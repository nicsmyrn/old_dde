<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PeriodRouteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $rules = [
        ];


        foreach ($this->request->get('periodCollection') as $k=>$v){
            $rules['periodCollection.'.$k.'.kids_number'] = 'required|integer|between:1,55';
            $rules['periodCollection.'.$k.'.routes_number'] = 'required|integer|between:1,31';
            $rules['periodCollection.'.$k.'.description'] = 'min:3';
            $rules['periodCollection.'.$k.'.starts_at'] = 'required';
//            $rules['periodCollection.'.$k.'.new'] = 'required';
        }

        return $rules;
    }

    public function messages()
    {
        $messages = [];

        foreach ($this->request->get('periodCollection') as $k=>$v){
            $messages['periodCollection.'.$k.'.kids_number.required'] = 'Ο αριθμός των παιδιών είναι υποχρεωτικός';
            $messages['periodCollection.'.$k.'.kids_number.integer'] = 'Ο αριθμός των παιδιών πρέπει να είναι ακέραιος';
            $messages['periodCollection.'.$k.'.kids_number.between'] = 'Ο αριθμός των παιδιών πρέπει να είναι ακέραιος μεταξύ 1 και 55';
            $messages['periodCollection.'.$k.'.routes_number.required'] = 'Ο αριθμός των δρομολογίων είναι υποχρεωτικός';
            $messages['periodCollection.'.$k.'.routes_number.integer'] = 'Ο αριθμός των δρομολογίων πρέπει να είναι ακέραιος';
            $messages['periodCollection.'.$k.'.routes_number.between'] = 'Ο αριθμός των δρομολογίων πρέπει να είναι ακέραιος μεταξύ 1 και 31';
            $messages['periodCollection.'.$k.'.description.min'] = 'Οι παρατηρήσεις πρέπει να είναι τουλάχιστον 3 χαρακτήρες';

            $messages['periodCollection.'.$k.'.starts_at.required'] = 'Η ώρα είναι υποχρεωτική';
//            $messages['periodCollection.'.$k.'.new.required'] = 'Το Νέο δρομολόγιο είναι υποχρεωτικό';

        }

        return $messages;
    }
}
