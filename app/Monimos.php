<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Monimos extends Model
{
    protected $table = 'monimoi';

    protected $fillable = [
        'am',
        'organiki',
        'moria',
        'orario',
        'county',
        'moria_apospasis',
        'dieuthinsi'
    ];

    public function teacher()
    {
        return $this->morphOne(\App\Teacher::class, 'teacherable');
    }

    public function getOrganikiNameAttribute()
    {
        $school = School::find($this->organiki);

        if($school == null){
                                                                //MUST CHANGE WITH A LOOP FROM REQUESTS FILE CONFIG
            if ($this->organiki == 1000){
                return 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ';
            }elseif ($this->organiki == 2000){
                return 'Απόσπαση από Δ.Δ.Ε. '.\Config::get('requests.county')[$this->county];
            }elseif ($this->organiki == 3000){
                return 'Αναπληρωτής';
            }elseif ($this->organiki == 4000) {
                return 'Απόσπαση από Π/θμια';
            }
        }else {
                return $school->name;
            }
    }

    public function getOrganikiSlugAttribute()
    {
        $school = School::find($this->organiki);

        if($school == null){
            if ($this->organiki == 1000){
                return 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ';
            }elseif ($this->organiki == 2000){
                return 'Δ.Δ.Ε. '.\Config::get('requests.county')[$this->county];
            }elseif ($this->organiki == 4000){
                return 'Διεύθυνση Π';
            }
        }else{
            return $school->name . ' (οργανική)';
        }
    }

    public function getGroupNameAttribute()
    {
        $school = School::find($this->organiki);

        if ($school != null){
            return $school->group;
        }
    }
}
