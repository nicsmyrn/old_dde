<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganikiPrefrences extends Model
{
    //
    protected $table = 'organikes_prefrences';

    public  $timestamps = false;

    protected $fillable = [
        'request_id',
        'order_number',
        'school_id'
    ];


    protected $appends = [
        'OrganikiName',
        'OrganikiOrder'
    ];

    public function request()
    {
        return $this->belongsTo(\App\OrganikiRequest::class,'request_id','id');
    }

    public function getOrganikiNameAttribute()
    {
        return School::find($this->school_id)->name;
    }

    public function getOrganikiOrderAttribute()
    {
        return School::find($this->school_id)->order;
    }
}
