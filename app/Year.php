<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $table = 'years_placements';

    protected $fillable = [
        'name',
        'current'
    ];

    public function praxeis()
    {
        return $this->hasMany(\App\Praxi::class);
    }
}
