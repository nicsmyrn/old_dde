<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FakeAnaplirotis extends Model
{
    //
    protected $table = 'fake_anaplirotes';

    protected $fillable = [
        'afm',
        'moria',
        'seira_topothetisis',
        'type',
        'orario'
    ];

    public function teacher()
    {
        return $this->morphOne(\App\FakeTeacher::class, 'teacherable');
    }
}
