<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class RequestTeacherYperarithmia extends Model
{
    //
    protected $table = 'requests_yperarithmias';

    protected $fillable = [
        'protocol_number',
        'year',
        'unique_id',
        'teacher_id',
        'want_yperarithmia',
        'school_organiki',
        'file_name',
        'date_request',
        'description',
    ];

    public function teacher()
    {
        return $this->belongsTo(\App\Teacher::class, 'teacher_id', 'id');
    }

    public function getDateReqAttribute($date)
    {
        return Carbon::parse($this->date_request)->format('d/m/Y -  H:i:s');   // W H Y ?????
    }

    public function getDateAitisisAttribute($date)
    {
        return Carbon::parse($this->date_request)->format('d/m/Y');
    }

    public function getProtocolDateAttribute()
    {
        $protocol = ProtocolTeacher::find($this->protocol_number);

        if ($protocol != null){
            return str_replace('/','-',$protocol->p_date);
        }else{
            return '';
        }
    }

    public function getFNameAttribute()
    {
        $protocol = ProtocolTeacher::find($this->protocol_number);

        return $protocol->f_name;
    }

    public function getProtocolNameAttribute()
    {
        $year =  Carbon::parse($this->date_request)->format('Y');

        if ($year == Carbon::now()->format('Y')){
            $protocol = ProtocolTeacher::find($this->protocol_number);
        }else{
            $protocol = ProtocolTeacherArchives::where('id',$this->protocol_number)->first();
        }

        return $protocol->protocol_name;
    }
}
