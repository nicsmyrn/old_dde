<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Lesson extends Model
{
    //
    protected $table = 'lessons';

    protected $fillable = [
        'name',
        'lesson_type',
        'united'
    ];

    public function anatheseis()
    {
        return $this->belongsToMany(\App\Eidikotita::class, 'anatheseis', 'lessons_id', 'eidikotita_id');
    }

    public function schools()
    {
        return $this->belongsToMany('App\School', 'kena-mathimaton', 'lesson_id', 'sch_id')
            ->withPivot('value','last_user_login_id', 'description')
            ->withTimestamps();
    }

    public function getEidikotitesAttribute()
    {
        return implode(', ',json_decode($this->anatheseis->sortBy('slug_name')->lists('slug_name')));
    }
    
    public function getDateModifiedAttribute()
    {
        return Carbon::parse($this->pivot->updated_at)->format('d-m-Y h:i:s');
    }
    
    public function getLastUserAttribute()
    {
        $user = User::find($this->pivot->last_user_login_id);
        if ($user->userable_type == 'App\School'){
            return "$user->last_name  $user->first_name";    
        }else{
            return "ΠΥΣΔΕ";
        }
    }
}

