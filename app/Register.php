<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Register extends Model
{
    protected $table = 'registration';

    protected $fillable = [
        'last_name',
        'first_name',
        'email',
        'password',
        'token'
    ];
}
