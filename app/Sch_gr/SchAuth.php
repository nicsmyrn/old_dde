<?php

namespace App\Sch_gr;


use App\Repositories\UserRepository;
use Illuminate\Contracts\Auth\Guard;
use phpCAS;
use Illuminate\Support\Facades\Auth;

class SchAuth{

    private $provider = 'sch.gr';
    private $config;
    private $auth;
    protected $users;


    public function __construct(UserRepository $users, Guard $auth)
    {
        $this->config = config('school_gr');
        $this->users = $users;
        $this->auth = $auth;
        $this->init();
    }

    private function init()
    {
        phpCAS::client(SAML_VERSION_1_1,$this->config['cas_hostname'],$this->config['cas_port'],'');
        phpCAS::setNoCasServerValidation();
        phpCAS::handleLogoutRequests(array($this->config['cas_hostname']));
    }

    public function execute($listener)
    {
        if(!$this->check()){
            $this->login();
        }

        $user = $this->users->SCH_GRfindByUsernameOrCreate($this->getAttributes(), $this->provider);

        if($user == 'other'){
            return $listener->userHasPushedOut($this->getAttributes()['mail']);
        }

        $this->auth->login($user);

        return $listener->userHasLoggedIn($user);
    }

    public function login()
    {
        if(!phpCAS::checkAuthentication()){
            phpCAS::forceAuthentication();
        }
        $this->setUser();
    }

    public function check()
    {
        return phpCAS::isAuthenticated();
    }

    public function setUser()
    {
        $this->user = phpCAS::getUser();
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getAttributes()
    {
        return phpCAS::getAttributes();
    }

    public function logout()
    {

        if ($this->auth->check()) {
            $this->auth->logout();
        }
        if (phpCAS::isSessionAuthenticated()) {
            phpCAS::logoutWithRedirectService(config('school_gr.redirect_path_success'));
        }
    }

}