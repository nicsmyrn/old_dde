<?php

namespace App\Sch_gr\Facade;

use Illuminate\Support\Facades\Facade;

class Smail extends Facade{
    protected static function getFacadeAccessor(){
        return 'Smail';
    }
}