<?php

namespace App\Sch_gr\Facade;

use Illuminate\Support\Facades\Facade;

class Sch extends Facade{
    protected static function getFacadeAccessor(){
        return 'Sch';
    }
}