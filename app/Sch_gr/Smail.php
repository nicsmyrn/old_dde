<?php

namespace App\Sch_gr;

use Mail;

class Smail
{
    private function sentConfirmation($view, $subject, $data , $toMail, $toName)
    {
        Mail::send($view, $data, function($message) use ($subject, $toMail, $toName){
            $message->to($toMail, $toName)
                ->subject($subject);
        });
    }

    private function sentAitisi($view, $subject, $protocol, $request_teacher, $file_path, $toMail, $toName, $teacher_name, $new_eidikotita = null)
    {
        $data = array(
            'protocol_name' => $protocol->protocol_name,
            'type'          => $protocol->type,
            'date'          => $request_teacher->date_request,
            'fullname'      => $teacher_name,
            'new_eidikotita' => $new_eidikotita
        );
        Mail::send($view, $data, function($message) use ($subject, $file_path, $toMail, $toName, $protocol){
            $message->to($toMail, $toName)
                ->subject($subject)
                ->attach(storage_path('app/teachers/'.$file_path));
        });
    }

    private function sentMultiplyAttachments($view, $subject, $attachments, $toMail, $toName, $full_name = null, $afm = null, $id = null)
    {
        $data = array(
            'full_name'  => $full_name,
            'afm'       => $afm,
            'counter'   => count($attachments),
            'id'        => $id
        );
        Mail::send($view, $data, function($message) use ($subject, $toMail, $toName, $attachments){
            $message->to($toMail, $toName)
                ->subject($subject);
            if(count($attachments) > 0){
                foreach($attachments as $file){
                    $message->attach($file['real_path'], $file['options']);
                }
            }
        });
    }

    private function sentMultiplyAttachmentsMoriodotisi($view, $subject, $attachments, $toMail, $toName, $full_name = null, $afm = null, $am = null,  $id = null, $type)
    {
        $data = array(
            'full_name'  => $full_name,
            'afm'       => $afm,
            'counter'   => count($attachments),
            'id'        => $id,
            'type'      => $type,
            'am'        => $am
        );
        Mail::send($view, $data, function($message) use ($subject, $toMail, $toName, $attachments){
            $message->to($toMail, $toName)
                ->subject($subject);
            if(count($attachments) > 0){
                foreach($attachments as $file){
                    $message->attach($file['real_path'], $file['options']);
                }
            }
        });
    }

    private function sentMultiplyAttachementsToOfa($view, $subject, $attachments, $toMail, $toName)
    {
        $data = array();
        Mail::send($view, $data, function($message) use ($subject, $toMail, $toName, $attachments){
            $message->to($toMail, $toName)
                ->subject($subject);
            if(count($attachments) > 0){
                foreach($attachments as $file){
                    $message->attach($file);
                }
            }
        });
    }

    private function sentOfaAttachment($view, $subject, $attachment, $toMail, $toName)
    {
        $data = array();
        Mail::send($view, $data, function($message) use ($subject, $toMail, $toName, $attachment){
            $message->to($toMail, $toName)
                ->from(\Auth::user()->email, \Auth::user()->userable->name)
                ->subject($subject)
                ->attach($attachment);
        });
    }

    public function sentTeacherToMisthodosiaRequest($attachments, $full_name, $afm)
    {
        $subject = 'Αίτημα αλλαγής στοιχείων Μισθοδοσίας';
        $viewForTmimaMisthodosias = 'emails.sent_misthodosia_request';
        $mailMisthodosias = env('MAIL_MISTHODOSIA');
        $nameMisthodosias = env('NAME_MISTHODOSIA');

        $this->sentMultiplyAttachments($viewForTmimaMisthodosias, $subject, $attachments, $mailMisthodosias, $nameMisthodosias, $full_name, $afm);
    }

    public function sentSchoolToOfaList($attachment)
    {
        $subject = 'Λίστα Αγώνων';
        $viewForOfaDepartment = 'emails.school_to_ofa_mail';
        $mailOFA = env('MAIL_OFA');
        $nameOFA = env('NAME_OFA');

        $this->sentOfaAttachment($viewForOfaDepartment, $subject, $attachment, $mailOFA, $nameOFA);
    }

    public function sentSchoolToOfaSportEducation($attachment)
    {
        $subject = 'Αθλοπαιδεία';
        $viewForOfaDepartment = 'emails.school_to_ofa_mail_sport_education';
        $mailOFA = env('MAIL_OFA');
        $nameOFA = env('NAME_OFA');

        $this->sentOfaAttachment($viewForOfaDepartment, $subject, $attachment, $mailOFA, $nameOFA);
    }

    public function sentSchoolToOfaSportEducationTwoAttachments($attachments)
    {
        $subject = 'Αθλοπαιδεία Στίβος';
        $viewForOfaDepartment = 'emails.school_to_ofa_mail_sport_education';
        $mailOFA = env('MAIL_OFA');
        $nameOFA = env('NAME_OFA');

        $this->sentMultiplyAttachementsToOfa($viewForOfaDepartment, $subject, $attachments, $mailOFA, $nameOFA);
    }

    public function sentTeacherToPysdeRequest($attachments, $full_name, $afm, $id)
    {
        $subject = '['.$full_name.'] - Δικαιολογητικά';
        $viewForTmimaPysde = 'emails.sent_pysde_request';
        $mailPysde = env('MAIL_PYSDE');
        $namePysde = env('NAME_PYSDE');

        $this->sentMultiplyAttachments($viewForTmimaPysde, $subject, $attachments, $mailPysde, $namePysde, $full_name, $afm,$id);
    }

    public function sentTeacherToPysdeOnlyAttachments($attachments, $full_name, $afm, $id)
    {
        $subject = '['.$full_name.'] - ΜΟΝΟ Δικαιολογητικά';
        $viewForTmimaPysde = 'emails.sent_attachments';
        $mailPysde = env('MAIL_PYSDE');
        $namePysde = env('NAME_PYSDE');

        $this->sentMultiplyAttachments($viewForTmimaPysde, $subject, $attachments, $mailPysde, $namePysde, $full_name, $afm,$id);

    }

    public function sentTeacherToProswpikoForMoriodotisi($attachments, $full_name, $afm, $am,  $id, $type)
    {
        $subject = '['.$full_name.'] - ΑΙΤΗΣΗ ΜΟΡΙΟΔΟΤΗΣΗΣ '. $type;
        $viewForTmima = 'emails.sent_for_moriodotisi';
        $mail = env('MAIL_PROSWPIKOU');
        $name = env('NAME_PROSWPIKOU');

        $this->sentMultiplyAttachmentsMoriodotisi($viewForTmima, $subject, $attachments, $mail, $name, $full_name, $afm, $am, $id, $type);
    }

    public function sentAitisiToTeacher($protocol, $request_teacher, $file_path, $user)
    {
        $view = 'emails.sentTeacherRequest';
        $subject = 'Αίτηση ' . $protocol->type;
        $toMail = $user->email;
        $toName = $user->full_name;
        $teacher_name = $toName;

        $this->sentAitisi($view, $subject,  $protocol, $request_teacher, $file_path, $toMail, $toName, $teacher_name);
    }

    public function sentAitisiToPYSDE($protocol, $request_teacher, $file_path, $user)
    {
        $view = 'emails.sentTeacherRequestToPysde';
        $toMail = 'pysdechanion@gmail.com'; //pysde@dide.chan.sch.gr
        $toName = 'ΠΥΣΔΕ ΧΑΝΙΩΝ';
        $teacher_name = $user->last_name . ' '.  $user->first_name;

        $new_eidikotita = $user->userable->myschool->new_eidikotita_name . '(' . $user->userable->myschool->new_klados . ')';

        $subject = 'Αίτηση ' . $protocol->type . ' από τον εκπαιδευτικό '. $user->first_name . ' ' . $user->last_name . ' ' . $new_eidikotita;

        $this->sentAitisi($view, $subject,  $protocol, $request_teacher, $file_path, $toMail, $toName, $teacher_name, $new_eidikotita);
    }

    public function sentConfirmationToSchool($school)
    {
        $view = 'emails.from_pysde_confirmation';
        $subject = 'Επιτυχής παραλαβή στοιχείων';
        $toMail = $school->user->email;
        $toName = $school->user->full_name;

        $this->sentConfirmation($view, $subject, compact('school'), $toMail, $toName);
    }

    public function sentTeacherProfile($teacher, $subject, $data = null)
    {
        $view = 'emails.change_teacher_profile';
        $toMail = $teacher->user->email;
        $toName = $teacher->user->full_name;

        $this->sentConfirmation($view, $subject, $data, $toMail, $toName);
    }

    public function sentTeacherMisthodosia($teacher, $subject, $data = null)
    {
        $view = 'emails.change_teacher_misthodosia';
        $toMail = $teacher->user->email;
        $toName = $teacher->user->full_name;

        $this->sentConfirmation($view, $subject, $data, $toMail, $toName);

        $viewForTmimaMisthodosias = 'emails.change_teacher_misthodosia_To_Misthodosia';
        $mailMisthodosias = env('MAIL_MISTHODOSIA');
        $nameMisthodosias = env('NAME_MISTHODOSIA');

        $this->sentConfirmation($viewForTmimaMisthodosias, $subject, $data, $mailMisthodosias, $nameMisthodosias);
    }

    public function sentTeacherConfirmMisthodosia($teacher, $subject, $data = null)
    {
        $view = 'emails.change_teacher_misthodosia';
        $toMail = $teacher->user->email;
        $toName = $teacher->user->full_name;

        $this->sentConfirmation($view, $subject, $data, $toMail, $toName);
    }

    public function sentSchoolDescriptionToPysde($school, $old_description,  $new_description)
    {
        if ($new_description == ''){
            return true;
        }else{
            if ($old_description == $new_description){
                return true;
            }
        }

        $view = 'emails.school_kena';
        $subject = 'Παρατηρήσεις '. $school->user->full_name;
        $toMail = 'pysdechanion@gmail.com';
        $toName = 'ΠΥΣΔΕ Χανίων';

        $this->sentConfirmation($view, $subject, compact('school'), $toMail, $toName);

    }

}