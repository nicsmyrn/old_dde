<?php

namespace App\Policies;


use App\User;
use App\School;

class SchoolPolicy
{
    
    public function isMySchool(User $user, School $school)
    {
        return $user->userable_id == $school->id; 
    }
}
    