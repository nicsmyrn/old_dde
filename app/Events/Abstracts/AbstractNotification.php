<?php

namespace App\Events\Abstracts;

use Carbon\Carbon;

abstract class AbstractNotification{
    public $url;
    public $pearson;
    public $title;
    public $description;
    public $type;
    public $uniqueAction;
    public $time;
    public $hour_time;
    public $minute_time;

    protected function getUniqueAction()
    {
        $date = Carbon::now();
        return md5($this->pearson->id. date_timestamp_get($date));
    }

    protected function timeNow()
    {
        return Carbon::now();
    }

    protected function getTime()
    {
        return $this->timeNow()->toIso8601String();
    }

    protected function getHour()
    {
        return $this->timeNow()->format('H');
    }

    protected function getMinutes()
    {
        return $this->timeNow()->format('i');
    }
}