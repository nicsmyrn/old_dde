<?php

namespace App\Events;

use App\Events\Abstracts\AbstractNotification;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PysdeSendNotificationToSchool extends AbstractNotification implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($school, $attributes = [])
    {
        $this->pearson = $school;
        $this->uniqueAction = $this->getUniqueAction();
        $this->url = action($attributes['action'], [str_replace(' ', '-',$this->pearson->name), 'action'=>$this->uniqueAction]);
        $this->title = $attributes['title'];
        $this->description = $attributes['description'];
        $this->type = $attributes['type'];
        $this->time = $this->getTime();
        $this->hour_time = $this->getHour();
        $this->minute_time = $this->getMinutes();
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['school-' . $this->pearson->user->id];
    }
}
