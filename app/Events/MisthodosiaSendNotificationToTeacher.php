<?php

namespace App\Events;

use App\Events\Event;
use Carbon\Carbon;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Events\Abstracts\AbstractNotification;

class MisthodosiaSendNotificationToTeacher extends  AbstractNotification implements ShouldBroadcast
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($teacher, $attributes = [])
    {
        $this->pearson = $teacher;
        $this->uniqueAction = $this->getUniqueAction();
        $this->url = action($attributes['url'], ['action' => $this->uniqueAction]);
        $this->title = $attributes['title'];
        $this->description = $attributes['description'];
        $this->type = $attributes['type'];
        $this->time = $this->getTime();
        $this->hour_time = $this->getHour();
        $this->minute_time = $this->getMinutes();
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['user-' . $this->pearson->user->id];
    }
}
