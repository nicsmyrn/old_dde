<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Role;

class SchoolGrantEditPermission extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $permission;
    public $noPermission;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($permission)
    {
        $this->permission = $permission;
        $this->noPermission = $this->checkPermission($permission);
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return ['school-permission'];
    }

    private function checkPermission($permission)
    {
        $role_school = Role::where('slug', 'school')->first();
        if($role_school->hasPermission($permission)){
            return true;
        }else{
            return false;
        }
    }
}
