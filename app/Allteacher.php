<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allteacher extends Model
{
    //
    protected $table = 'all_teachers';

    protected $fillable = [
        'klados_id',
        'school_id',
        'school_name',
        'last_name',
        'first_name',
        'middle_name',
        'klados',
        'ypoxreotiko',
        'hours_at_organiki',
        'category',
        'situation'
    ];

    public function placements()
    {
        return $this->morphOne(\App\Placement::class,'teacherable');
    }

    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }
}
