<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FakeMonimos extends Model
{
    //
    protected $table = 'fake_monimoi';

    protected $fillable = [
        'am',
        'organiki',
        'moria',
        'orario',
        'county',
        'moria_apospasis'
    ];

    public function teacher()
    {
        return $this->morphOne(\App\FakeTeacher::class, 'teacherable');
    }

    public function getOrganikiNameAttribute()
    {
        $school = School::find($this->organiki);

        if($school == null){
            if ($this->organiki == 1000){
                return 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ';
            }elseif ($this->organiki == 2000){
                return 'Απόσπαση από Δ.Δ.Ε. '.\Config::get('requests.county')[$this->county];
            }
        }else{
            return $school->name;
        }
    }

    public function getOrganikiSlugAttribute()
    {
        $school = School::find($this->organiki);

        if($school == null){
            if ($this->organiki == 1000){
                return 'ΔΙΑΘΕΣΗ ΠΥΣΔΕ';
            }elseif ($this->organiki == 2000){
                return 'Δ.Δ.Ε. '.\Config::get('requests.county')[$this->county];
            }
        }else{
            return $school->name . ' (οργανική)';
        }
    }

    public function getGroupNameAttribute()
    {
        $school = School::find($this->organiki);

        if ($school != null){
            return $school->group;
        }
    }

}
