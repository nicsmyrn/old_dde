<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MySchoolTeacher extends Model
{
    protected $table = 'myschool_teachers';

    protected $primaryKey = 'afm';

    protected $fillable = [
        'afm',
        'am',
        'sex',
        'last_name',
        'first_name',
        'middle_name',
        'mothers_name',
        'phone',
        'mobile',
        'eidikotita',
        'vathmos',
        'ypoxreotiko',
        'organiki_prosorini',
        'topothetisi',
        'birth',
        'dieuthinsi',
        'new_eidikotita'
    ];

    public function getFullNameAttribute()
    {
        return $this->last_name . ' ' . $this->first_name;
    }

    public function teacher()
    {
        return $this->hasOne('App\Teacher','afm', 'afm');
    }

    public function misthodosia()
    {
        return $this->hasOne('App\Misthodosia', 'afm', 'afm');
    }

    public function rights()
    {
        return $this->hasOne('App\TeacherRights','afm', 'afm');
    }

    public function edata()
    {
        return $this->hasOne('App\Edata','am', 'am');
    }

    public function placements()
    {
        return $this->hasMany(Placement::class, 'afm', 'afm');
    }

    public function getNewKladosNameAttribute()
    {
        return NewEidikotita::where('id', $this->new_eidikotita)->first()->klados_slug;
    }

    public function getNewKladosAttribute()
    {
        return NewEidikotita::where('id', $this->new_eidikotita)->first()->eidikotita_slug;
    }

    public function getNewEidikotitaNameAttribute()
    {
        return NewEidikotita::where('id', $this->new_eidikotita)->first()->eidikotita_name;
    }

    public function getKladosAttribute()
    {
        return  Eidikotita::where('slug_name', $this->eidikotita)->first()->name;
    }

    public function getKladosIdAttribute()
    {
        return Eidikotita::where('slug_name', $this->eidikotita)->first()->id;
    }

    public function getDieuthinsiTitleAttribute()
    {
        return Dieuthinsi::where('identifier', $this->dieuthinsi)->first()->title;
    }

    public function getHasYperarithmiaAttribute()
    {
//        $eidikotita_id = $this->klados_id;
        $eidikotita_id = $this->new_eidikotita;
        $school_id = $this->organiki_id;

        return $school_id;
//        $yp = Yperarithmia::where('school_id', $school_id)->where('eidikotita_id',$eidikotita_id)->first();
        $yp = NewYperarithmia::where('school_id', $school_id)->where('eidikotita_id',$eidikotita_id)->first();

        return $yp;

        if ($yp == null){
            return false;
        }else{
            if($yp->number > 0){
                return true;
            }
            return false;
        }
    }

    public function getOrganikiNameAttribute()
    {
        if(\Config::get('requests.my_dieuthinsi') != $this->dieuthinsi){
            $dieuthinsi = Dieuthinsi::where('identifier', $this->dieuthinsi)->first();
            if ($dieuthinsi != null){
                if($dieuthinsi->myschool_id == 2){
                    return 'Απόσπαση από '. $this->dieuthinsi_title;
                }else{
                    return 'Διάθεση από '. $this->dieuthinsi_title;
                }
            }else{
                return 'ΑΝΑΠΛΗΡΩΤΗΣ';
            }

        }else{
            if(str_contains($this->topothetisi, 'Οργανικά')){
                $school = School::where('identifier', $this->organiki_prosorini)->first();
                if ($school != null){
                    return $school->name;
                }else{
                    return '-';
                }
            }else{
                if($this->am == ''){
                    return 'ΑΝΑΠΛΗΡΩΤΗΣ';
                }else{
                    return 'Διάθεση ΠΥΣΔΕ';
                }
            }
        }
    }

    public function getOrganikiIdAttribute()
    {
        if(\Config::get('requests.my_dieuthinsi') != $this->dieuthinsi){
            $dieuthinsi = Dieuthinsi::where('identifier', $this->dieuthinsi)->first();
            if ($dieuthinsi != null){
                if(str_contains('Δ.Ε.',Dieuthinsi::where('identifier', $this->dieuthinsi)->first()->name)){
                    return 2000;
                }else{
                    return 4000;
                }
            }else{
                return 3000;
            }
        }else{
            if(str_contains($this->topothetisi, 'Οργανικά')){
                $school = School::where('identifier', $this->organiki_prosorini)->first();
                if ($school != null){
                    return $school->id;
                }else{
                    return 1000;
                }
            }else{
                if($this->am == ''){
                    return 3000;
                }else{
                    return 1000;
                }
            }
        }
    }
}
