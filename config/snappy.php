<?php

return array(


    'pdf' => array(
        'enabled' => true,
        // 'binary' => '/usr/local/bin/wkhtmltopdf',
        'binary' => base_path('vendor/h4cc/wkhtmltopdf-i386/bin/wkhtmltopdf-i386'),
        'timeout' => false,
        'options' => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary' => base_path('vendor/h4cc/wkhtmltoimage-i386/bin/wkhtmltoimag-i386'),
        'timeout' => false,
        'options' => array(),
    ),


);
