<?php
return [

    'site_key'   => env('RECAPTCHA_KEY_SITE','site-key'),

    'secret_key' => env('RECAPTCHA_KEY_SECRET','secrete-key'),

    'url'        => 'https://www.google.com/recaptcha/api/siteverify?'

];
