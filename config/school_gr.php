<?php
return [


    'cas_hostname'   => env('SCH_SSO'),

    'cas_port'       => 443,

    'redirect_path_success'  => env('SCH_LOGOUT_SUCCESS'),

    'redirect_path_suspended'  => env('SCH_LOGOUT_SUSPENDED')

];
