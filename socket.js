var http = require('http').Server(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end('Hello World\n');
});
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();


io.on('connection', function(socket) {
    console.log('new connection');
});

redis.psubscribe('*');

redis.on('pmessage', function(subscribed, channel, message) {
    message = JSON.parse(message);

    console.log(channel+':'+message.event+':'+message.data);

    io.emit(channel + ':' + message.event, message.data);
});
http.listen(3000, function(){
    console.log('Server is running on port 3000!');
});
