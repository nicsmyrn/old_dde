<?php

use Illuminate\Database\Seeder;

class LessonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('lessons')->insert(['id' => '22','name' => 'english+', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'gym','united' =>'english']);
        DB::table('lessons')->insert(['id' => '23','name' => 'english', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'gym','united' =>'english']);
        DB::table('lessons')->insert(['id' => '24','name' => 'german+', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'gym','united' =>'german']);
        DB::table('lessons')->insert(['id' => '25','name' => 'german-', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'gym','united' =>'german']);
        DB::table('lessons')->insert(['id' => '26','name' => 'french+', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'gym','united' =>'french']);
        DB::table('lessons')->insert(['id' => '27','name' => 'french-', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'gym','united' =>'french']);
        DB::table('lessons')->insert(['id' => '28','name' => 'Project', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'all','united' =>NULL]);
        DB::table('lessons')->insert(['id' => '29','name' => 'Technology', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'gym','united' =>NULL]);
        DB::table('lessons')->insert(['id' => '30','name' => 'pe09-10-13', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'lyk','united' =>NULL]);
        DB::table('lessons')->insert(['id' => '31','name' => 'sxedio', 'created_at'=>'2015-10-01 00:00:00', 'updated_at'=>'2015-10-01 00:00:00','lesson_type' => 'lyk','united' =>NULL]);

    }
}
