<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    private  $roles = array(
        array('id' => '1','name' => 'Σχολείο','slug' => 'school','description' => NULL),
        array('id' => '2','name' => 'Γραμματεία ΠΥΣΔΕ','slug' => 'pysde_secretary','description' => NULL),
        array('id' => '3','name' => 'Συμβούλιο ΠΥΣΔΕ','slug' => 'pysde_council','description' => NULL),
        array('id' => '4','name' => 'Διευθυντής ΔΔΕ Χανίων','slug' => 'manager','description' => NULL),
        array('id' => '5','name' => 'Βοηθός γραμματέα ΠΥΣΔΕ','slug' => 'pysde_helper_secretary','description' => NULL),
        array('id' => '6','name' => 'Καθηγητής','slug' => 'teacher','description' => NULL),
        array('id' => '7','name' => 'Υπεύθυνος Εκπαιδευτικών Θεμάτων','slug' => 'ekpaideutikon_thematon','description' => NULL)
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert($this->roles);
    }
}
