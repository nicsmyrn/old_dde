<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProtocolArchivesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::beginTransaction();

        DB::statement('INSERT INTO `protocol-archives` (id, type, p_date, from_to, subject, f_id, description, printed, created_at, updated_at , pending)

                select id, type, p_date, from_to, subject, f_id, description, printed, created_at, updated_at , pending
                from protocols');

        DB::table('protocols')->truncate();
        DB::commit();
    }
}
