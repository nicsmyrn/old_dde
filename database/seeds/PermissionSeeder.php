<?php

use Illuminate\Database\Seeder;//

class PermissionSeeder extends Seeder
{
    private $permissions = array(
        array('id' => '2','name' => 'Ανάγνωση Κενών-Πλεονασμάτων του Σχολείο','slug' => 'read_school_kena','description' => 'Δικαίωμα που το έχει το κάθε Σχολείο για να μπορεί να διαβάσει τα κενά πλεονάσματά του'),
        array('id' => '3','name' => 'Τροποποίηση Κενών-Πλεονασμάτων','slug' => 'edit_school_kena','description' => NULL),
        array('id' => '4','name' => 'Προβολή όλων των αριθμών πρωτοκόλλου','slug' => 'read_all_protocols','description' => NULL),
        array('id' => '5','name' => 'Δημιουργία νέου πρωτόκολλου','slug' => 'create_protocol','description' => NULL),
        array('id' => '6','name' => 'Δημιουργία PDF Αρχείου Πρωτοκόλλων','slug' => 'create_pdf_protocols','description' => NULL),
        array('id' => '7','name' => 'Επεξεργασία Πρωτοκόλλου','slug' => 'edit_protocol','description' => NULL),
        array('id' => '8','name' => 'Διαγραφή Πρωτοκόλλου','slug' => 'delete_protocol','description' => NULL),
        array('id' => '9','name' => 'Ανάγνωση Κενών όλων των Σχολείων','slug' => 'read_All_Schools_Kena','description' => NULL),
        array('id' => '10','name' => 'Διαχείριση & δημιουργία Excel Αρχείων','slug' => 'manage_excel_files','description' => NULL),
        array('id' => '11','name' => 'Αρχειοθέτηση Excel Αρχείων','slug' => 'archives_excel_files','description' => NULL),
        array('id' => '12','name' => 'Δημιουργία αίτησης Ε1','slug' => 'create_E1_request','description' => 'E1'),
        array('id' => '13','name' => 'Δημιουργία αίτησης Ε2','slug' => 'create_E2_request','description' => 'E2'),
        array('id' => '14','name' => 'Δημιουργία αίτησης Ε3','slug' => 'create_E3_request','description' => 'E3'),
        array('id' => '15','name' => 'Δημιουργία αίτησης Ε4','slug' => 'create_E4_request','description' => 'E4'),
        array('id' => '16','name' => 'Δημιουργία αίτησης Ε5','slug' => 'create_E5_request','description' => 'E5'),
        array('id' => '17','name' => 'Δημιουργία αίτησης Ε6','slug' => 'create_E6_request','description' => 'E6'),
        array('id' => '18','name' => 'Δημιουργία αίτησης Ε7','slug' => 'create_E7_request','description' => 'E7'),
        array('id' => '19','name' => 'Δημιουργία απλής άιτησης ή ένστασης','slug' => 'create_ΑΠΛΗ_request','description' => 'ΑΠΛΗ')
    );
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert($this->permissions);
    }
}
