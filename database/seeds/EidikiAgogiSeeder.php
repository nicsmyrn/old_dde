<?php

use Illuminate\Database\Seeder;

class EidikiAgogiSeeder extends Seeder
{
    private $eidiki_agogi = array(
        array('id' => '1','name' => 'ΦΙΛΟΛΟΓΩΝ','slug' => 'pe02','slug_name' => 'ΠΕ02','order' => '1','sch_type' => 'all'),
        array('id' => '2','name' => 'ΜΑΘΗΜΑΤΙΚΩΝ','slug' => 'pe03','slug_name' => 'ΠΕ03','order' => '2','sch_type' => 'all'),
        array('id' => '3','name' => 'ΦΥΣΙΚΩΝ','slug' => 'pe040150','slug_name' => 'ΠΕ04.01.50','order' => '3','sch_type' => 'all'),
        array('id' => '4','name' => 'ΧΗΜΙΚΩΝ','slug' => 'pe040250','slug_name' => 'ΠΕ04.02.50','order' => '4','sch_type' => 'all'),
        array('id' => '5','name' => 'ΑΓΓΛΙΚΗΣ ΓΛ.','slug' => 'pe06','slug_name' => 'ΠΕ06','order' => '5','sch_type' => 'all'),
        array('id' => '6','name' => 'ΚΑΛΛΙΤΕΧΝΙΚΩΝ','slug' => 'pe08','slug_name' => 'ΠΕ08','order' => '6','sch_type' => 'all'),
        array('id' => '7','name' => 'ΚΟΙΝΩΝΙΟΛΟΓΩΝ','slug' => 'pe10','slug_name' => 'ΠΕ10','order' => '7','sch_type' => 'all'),
        array('id' => '8','name' => 'ΦΥΣΙΚΗΣ ΑΓΩΓΗΣ','slug' => 'pe11','slug_name' => 'ΠΕ11','order' => '8','sch_type' => 'all'),
        array('id' => '9','name' => 'ΓΕΩΠΟΝΟΙ','slug' => 'pe1404','slug_name' => 'ΠΕ14.04','order' => '9','sch_type' => 'all'),
        array('id' => '10','name' => 'ΤΟΥΡΙΣΤΙΚΩΝ ΕΠΙΧΕΙΡ.','slug' => 'pe1835','slug_name' => 'ΠΕ18.35','order' => '11','sch_type' => 'all'),
        array('id' => '11','name' => 'ΦΥΤΙΚΗΣ ΠΑΡΑΓΩΓΗΣ','slug' => 'pe1812','slug_name' => 'ΠΕ18.12','order' => '11','sch_type' => 'all'),
        array('id' => '12','name' => 'ΤΕΧΝΟΛΟΓΩΝ ΤΡΟΦΙΜΩΝ','slug' => 'pe1836','slug_name' => 'ΠΕ18.36','order' => '12','sch_type' => 'all'),
        array('id' => '13','name' => 'ΜΟΥΣΙΚΗΣ','slug' => 'te16','slug_name' => 'ΤΕ16','order' => '13','sch_type' => 'all'),
        array('id' => '14','name' => 'ΠΛΗΡΟΦΟΡΙΚΗΣ','slug' => 'pe19','slug_name' => 'ΠΕ19','order' => '14','sch_type' => 'all')
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('eidiki_agogi')->insert($this->eidiki_agogi);
    }
}

