<?php

use Illuminate\Database\Seeder;

class MySchoolTeachers extends Seeder
{

    public function run()
    {
        $columns_names = array();
        $record = array();

        if ( ($handle = fopen(database_path('seeds/data/myschool_teachers.csv'),'r')) !== FALSE){
            $headers = fgetcsv($handle, 0,';');

            foreach ($headers as $column){
                $columns_names[] = str_slug($column);
            }

            while(($data = fgetcsv($handle, 0,';')) !== FALSE){
                $i = 0;
               foreach($columns_names as $key){
                   $record[$key] = $data[$i++];
               }
            }

            fclose($handle);

        }else{
            echo 'nothing...';
        }
    }
}
