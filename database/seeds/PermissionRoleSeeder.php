<?php

use Illuminate\Database\Seeder;//

class PermissionRoleSeeder extends Seeder
{
    private $permissions_roles = array(
        array('permission_id' => '2','role_id' => '1'),
        array('permission_id' => '3','role_id' => '2'),
        array('permission_id' => '4','role_id' => '2'),
        array('permission_id' => '5','role_id' => '2'),
        array('permission_id' => '6','role_id' => '2'),
        array('permission_id' => '7','role_id' => '2'),
        array('permission_id' => '9','role_id' => '2'),
        array('permission_id' => '10','role_id' => '2'),
        array('permission_id' => '11','role_id' => '2'),
        array('permission_id' => '9','role_id' => '3'),
        array('permission_id' => '9','role_id' => '4'),
        array('permission_id' => '4','role_id' => '5'),
        array('permission_id' => '5','role_id' => '5'),
        array('permission_id' => '6','role_id' => '5'),
        array('permission_id' => '7','role_id' => '5'),
        array('permission_id' => '8','role_id' => '5'),
        array('permission_id' => '13','role_id' => '6'),
        array('permission_id' => '15','role_id' => '6'),
        array('permission_id' => '16','role_id' => '6'),
        array('permission_id' => '17','role_id' => '6'),
        array('permission_id' => '18','role_id' => '6'),
        array('permission_id' => '19','role_id' => '6'),
        array('permission_id' => '9','role_id' => '7'),
        array('permission_id' => '11','role_id' => '7')
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions_roles')->insert($this->permissions_roles);
    }
}
