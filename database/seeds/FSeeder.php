<?php

use Illuminate\Database\Seeder;//

class FSeeder extends Seeder
{
    private $f = array(
        array('id' => '1','name' => 'Φ.1','description' => 'Αιτήσεις - Ενστάσεις'),
        array('id' => '2','name' => 'Φ.2.1','description' => 'Αιτήσεις Οργανικές θέσεις Υπεράριθμων (Α φάση)'),
        array('id' => '3','name' => 'Φ.2.2','description' => 'Αιτήσεις - Οργανικές θέσεις Υπεράριθμων (Β φάση)'),
        array('id' => '4','name' => 'Φ.3.1','description' => 'Οργανικά Κενά Σχολείων (Α & Β φάση)'),
        array('id' => '5','name' => 'Φ.3.2','description' => 'Λειτουργικά Κενά Σχολείων'),
        array('id' => '6','name' => 'Φ.4','description' => 'Αιτήσεις Τοποθέτησεις μονίμων εκπαιδευτικών'),
        array('id' => '7','name' => 'Φ.5','description' => 'Αιτήσεις Τοποθέτησεις Αναπληρωτών εκπαιδευτικών Γενικής Παιδείας'),
        array('id' => '8','name' => 'Φ.6.1','description' => 'Αιτήσεις Ειδικής Αγωγής - Τμήματα Ένταξης'),
        array('id' => '9','name' => 'Φ.6.2','description' => 'Κενά Σχολείων Ειδικής Αγωγής - Τμήματα Ένταξης'),
        array('id' => '10','name' => 'Φ.7','description' => 'Παράλληλη Στήριξη'),
        array('id' => '11','name' => 'Φ.8','description' => 'Κατ οίκον Διδασκαλία'),
        array('id' => '12','name' => 'Φ.9','description' => 'Υπερωρίες'),
        array('id' => '13','name' => 'Φ.10','description' => 'Διευθυντές'),
        array('id' => '14','name' => 'Φ.11','description' => 'Αναπληρωτές Διευθυντές'),
        array('id' => '15','name' => 'Φ.12','description' => 'Υποδιευθυντές'),
        array('id' => '16','name' => 'Φ.13','description' => 'Ε.Κ. (ΣΕΚ)'),
        array('id' => '17','name' => 'Φ.14','description' => 'ΕΚΦΕ'),
        array('id' => '18','name' => 'Φ.15','description' => 'ΣΣΝ'),
        array('id' => '19','name' => 'Φ.16','description' => 'ΚΕΣΥΠ'),
        array('id' => '20','name' => 'Φ.17','description' => 'ΚΕΠΛΗΝΕΤ'),
        array('id' => '21','name' => 'Φ.18','description' => 'ΜΟΥΣΙΚΟ ΣΧΟΛΕΙΟ ΘΕΡΙΣΟΥ'),
        array('id' => '22','name' => 'Φ.19','description' => 'Ιδιωτική Εκπαίδευση'),
        array('id' => '23','name' => 'Φ.20','description' => 'Άδεια Άσκησης Ιδιωτικού Έργου'),
        array('id' => '24','name' => 'Φ.21','description' => 'Μεταπτυχιακά - Διδακτορικά'),
        array('id' => '25','name' => 'Φ.22','description' => 'Αναγνώριση προϋπηρεσίας'),
        array('id' => '26','name' => 'Φ.23','description' => 'Περιφερειακή Διεύθυνση Κρήτης'),
        array('id' => '27','name' => 'Φ.24','description' => 'Υπουργείο Παιδείας'),
        array('id' => '28','name' => 'Φ.25','description' => 'Γενικά Έγγραφα ΠΥΣΔΕ ΑΠΥΣΔΕ ΚΥΣΔΕ'),
        array('id' => '29','name' => 'Φ.26','description' => 'Διάφορα'),
        array('id' => '30','name' => 'κενό','description' => 'κενό πρωτόκολλο')
    );


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('f')->insert($this->f);
    }
}
