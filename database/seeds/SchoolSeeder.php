<?php

use Illuminate\Database\Seeder;

class SchoolSeeder extends Seeder
{
    private $schools = array(
        array('id' => '7','name' => '1 Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '1','description' => ''),
        array('id' => '8','name' => '2 Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '2','description' => ''),
        array('id' => '9','name' => '3 Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '3','description' => ''),
        array('id' => '10','name' => '4 Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '4','description' => ''),
        array('id' => '11','name' => '5 Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '5','description' => ''),
        array('id' => '12','name' => '6 Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '6','description' => ''),
        array('id' => '13','name' => '7 Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '7','description' => ''),
        array('id' => '14','name' => 'Εσπερινό Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '8','description' => ''),
        array('id' => '15','name' => '1 ΓΕΛ Χανίων','type' => 'lyk','group' => '1','order' => '9','description' => ''),
        array('id' => '16','name' => '2 ΓΕΛ Χανίων','type' => 'lyk','group' => '1','order' => '10','description' => ''),
        array('id' => '17','name' => '3 ΓΕΛ Χανίων','type' => 'lyk','group' => '1','order' => '11','description' => ''),
        array('id' => '18','name' => '4 ΓΕΛ Χανίων','type' => 'lyk','group' => '1','order' => '12','description' => ''),
        array('id' => '19','name' => 'Εσπερινό ΓΕΛ Χανίων','type' => 'lyk','group' => '1','order' => '13','description' => ''),
        array('id' => '20','name' => '1 ΕΠΑΛ Χανίων','type' => 'epal','group' => '1','order' => '14','description' => '1 ΕΠΑΛ Χανίων'),
        array('id' => '21','name' => '2 ΕΠΑΛ Χανίων','type' => 'epal','group' => '1','order' => '15','description' => ''),
        array('id' => '22','name' => 'Εσπερινό ΕΠΑΛ Χανίων','type' => 'epal','group' => '1','order' => '16','description' => ''),
        array('id' => '23','name' => 'Γυμνάσιο Κουνουπιδιανών','type' => 'gym','group' => '1','order' => '17','description' => ''),
        array('id' => '24','name' => 'ΓΕΛ Ακρωτηρίου','type' => 'lyk','group' => '1','order' => '18','description' => 'Για τα Γαλλικά το ένα δίωρο μόνο Τρίτη  6η και 7η ώρα λόγο περιορισμού του προγράμματος.'),
        array('id' => '25','name' => 'ΕΠΑΛ Ακρωτηρίου','type' => 'epal','group' => '1','order' => '19','description' => ''),
        array('id' => '26','name' => '1 Γυμνάσιο Ελ. Βενιζέλου','type' => 'gym','group' => '1','order' => '20','description' => ''),
        array('id' => '27','name' => '2 Γυμνάσιο Ελ. Βενιζέλου','type' => 'gym','group' => '1','order' => '21','description' => ''),
        array('id' => '28','name' => 'ΓΕΛ Ελ. Βενιζέλου','type' => 'lyk','group' => '1','order' => '22','description' => ''),
        array('id' => '29','name' => 'ΕΠΑΛ Ελ. Βενιζέλου','type' => 'epal','group' => '1','order' => '23','description' => ''),
        array('id' => '30','name' => 'Γυμνάσιο Σούδας','type' => 'gym','group' => '1','order' => '24','description' => ''),
        array('id' => '31','name' => 'ΓΕΛ Σούδας','type' => 'lyk','group' => '1','order' => '25','description' => 'Η καθηγήτρια Ξηρουχάκη Χρυσάνη κλάδου ΠΕ19 -Πληροφορικής θα λείψει με άδεια άνευ αποδοχών από 7-12-2015 για το υπόλοιπο του σχολικού έτους.'),
        array('id' => '32','name' => 'ΓΕΛ Κυδωνίας','type' => 'lyk','group' => '1','order' => '27','description' => ''),
        array('id' => '33','name' => 'Μουσικό','type' => 'lyk','group' => '1','order' => '28','description' => ''),
        array('id' => '34','name' => 'Ειδ Γυμνάσιο Χανίων','type' => 'gym','group' => '1','order' => '29','description' => ''),
        array('id' => '35','name' => 'Εκκλκο Γυμνάσιο Λύκειο','type' => 'gym','group' => '1','order' => '30','description' => ''),
        array('id' => '36','name' => 'Γυμνάσιο Αλικιανού  ','type' => 'gym','group' => '2','order' => '31','description' => 'Τα κενά των Γερμανικών (16 ώρες) και της Τεχνολογίας (8 ώρες) έχουν καλυφθεί με πράξεις του Διευθυντή Εκπαίδευσης.'),
        array('id' => '37','name' => 'ΓΕΛ Αλικιανού','type' => 'lyk','group' => '2','order' => '32','description' => 'Οι εκπ/κοί που εμφανίζονται με πλεόνασμα ωρών καλύπτουν τη διαφορά με γραμματειακή υποστήριξη σχολικής μονάδας.'),
        array('id' => '38','name' => 'Γυμνάσιο Πλατανιά ','type' => 'gym','group' => '2','order' => '33','description' => ''),
        array('id' => '39','name' => 'Εσπερινό ΕΠΑΛ Πλατανιά ','type' => 'epal','group' => '2','order' => '34','description' => ''),
        array('id' => '40','name' => 'Γυμνάσιο Βάμου ','type' => 'gym','group' => '4','order' => '35','description' => ''),
        array('id' => '41','name' => 'ΓΕΛ Βάμου','type' => 'lyk','group' => '4','order' => '36','description' => ''),
        array('id' => '42','name' => 'Γυμνάσιο Βρυσών','type' => 'gym','group' => '4','order' => '37','description' => ''),
        array('id' => '43','name' => 'ΕΠΑΛ  Βρυσών','type' => 'epal','group' => '4','order' => '38','description' => ''),
        array('id' => '44','name' => '1 Γυμνάσιο Κισάμου','type' => 'gym','group' => '3','order' => '39','description' => ''),
        array('id' => '45','name' => '2 Γυμνάσιο Κισάμου','type' => 'gym','group' => '3','order' => '40','description' => ''),
        array('id' => '46','name' => 'Γενικό Λύκειο Κισάμου','type' => 'lyk','group' => '3','order' => '41','description' => ''),
        array('id' => '47','name' => 'ΕΠΑΛ Κισάμου','type' => 'epal','group' => '3','order' => '42','description' => ''),
        array('id' => '48','name' => 'Γυμνάσιο Βουκολιών','type' => 'gym','group' => '2','order' => '43','description' => ''),
        array('id' => '49','name' => 'Γενικό Λύκειο Βουκολιών','type' => 'lyk','group' => '2','order' => '44','description' => 'Το κενό στον κλάδο ΠΕ08 αφορά το μάθημα του γραμμικού σχεδίου της Γ\' Λυκείου. Έχουν δοθεί 2 ώρες υπερωριακής απασχόλησης στην εκπαιδευτικό ΠΕ06 Τραμπακοπούλου Σοφία και παρακαλούμε να αλλάξετε την πράξη διάθεσής της στο σχολείο μας (από 8 σε 10 ώρες).'),
        array('id' => '50','name' => 'Γυμνάσιο Κολυμβαρίου','type' => 'gym','group' => '2','order' => '45','description' => ''),
        array('id' => '51','name' => 'ΓΕΛ Κολυμβαρίου','type' => 'lyk','group' => '2','order' => '46','description' => ''),
        array('id' => '52','name' => 'Γυμνάσιο Κανδάνου','type' => 'gym','group' => '5','order' => '47','description' => ''),
        array('id' => '53','name' => 'ΕΠΑΛ Κανδάνου','type' => 'epal','group' => '5','order' => '48','description' => '1 ΕΠΑΛ ΚΑΝΤΑΝΟΥ'),
        array('id' => '54','name' => 'Γυμνάσιο Παλαιόχωρας','type' => 'gym','group' => '5','order' => '49','description' => ''),
        array('id' => '55','name' => 'ΓΕΛ. Παλαιόχωρας','type' => 'lyk','group' => '5','order' => '50','description' => 'Αφήνω το κενό των 2 ωρών στα φιλολογικά, διότι δεν ξέρω αν το θέμα έχει λήξει τελικά με τις τοποθετήσεις των αναπληρωτών. Επί του παρόντος καλύπτεται με υπερωρία. Δείτε και προηγούμενο μαιλ.'),
        array('id' => '56','name' => 'Γυμνάσιο Χ.Σφακίων','type' => 'gym','group' => '6','order' => '51','description' => ''),
        array('id' => '57','name' => 'ΓΕΛ .Χ.Σφακίων ','type' => 'lyk','group' => '6','order' => '52','description' => ''),
        array('id' => '58','name' => 'Γυμνάσιο Ν. Κυδωνίας','type' => 'gym','group' => '1','order' => '26','description' => ''),
        array('id' => '59','name' => 'ΕΕΕΕΚ ΧΑΝΙΩΝ','type' => 'eidiko','group' => '1','order' => '100','description' => ''),
        array('id' => '60','name' => 'ΕΕΕΕΚ ΚΙΣΑΜΟΥ','type' => 'eidiko','group' => '4','order' => '101','description' => '')
    );

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schools')->insert($this->schools);
    }
}
