<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

//         $this->call(RoleSeeder::class);
//         $this->call(PermissionSeeder::class);
 //        $this->call(PermissionRoleSeeder::class);
//         $this->call(UserSeeder::class);
//         $this->call(SchoolSeeder::class);
//         $this->call(EidikotitaSeeder::class);
//         $this->call(KenaSeeder::class);
//         $this->call(FSeeder::class);
//         $this->call(ProtocolSeeder::class);

//         $this->call(LessonSeeder::class);
//         $this->call(KenaMathimatonSeeder::class);
//        // $this->call(SurveySeeder::class);
        
//         $this->call(EidikiAgogiSeeder::class);
//         $this->call(KenaEidikisSeeder::class);

//            $this->call(ProtocolArchivesSeeder::class);
        $this->call(MySchoolTeachers::class);

        Model::reguard();
    }
}
