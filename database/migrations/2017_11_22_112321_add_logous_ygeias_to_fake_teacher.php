<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogousYgeiasToFakeTeacher extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fake_teachers', function(Blueprint $table){
            $table->tinyInteger('logoi_ygeias_idiou')->default(0)->unsigned();
            $table->tinyInteger('logoi_ygeias_sizigou')->default(0)->unsigned();
            $table->tinyInteger('logoi_ygeias_paidiwn')->default(0)->unsigned();
            $table->tinyInteger('logoi_ygeias_goneon')->default(0)->unsigned();
            $table->tinyInteger('logoi_ygeias_aderfon')->default(0)->unsigned();
            $table->tinyInteger('logoi_ygeias_exosomatiki')->default(0)->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
