<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDieuthinsiToMonimoi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('monimoi', function(Blueprint $table){
            $table->string('dieuthinsi')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('monimoi', function(Blueprint $table){
            $table->dropColumn('dieuthinsi');
        });

    }
}
