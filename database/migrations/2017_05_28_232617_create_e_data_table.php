<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create edata table
        Schema::create('edata', function (Blueprint $table) {
            $table->string('am');
            $table->primary('am');

            $table->boolean('special_disease')->default(false);
            $table->boolean('special_many_children')->default(false);
            $table->boolean('special_judiciary')->default(false);

            $table->float('sum_moria', 6, 2);

            $table->boolean('entopiotita')->default(false);
            $table->boolean('sinipiretisi')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('edata');
    }
}
