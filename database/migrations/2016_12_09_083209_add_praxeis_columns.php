<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPraxeisColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('praxeis', function(Blueprint $table){
            $table->integer('praxi_type');
            $table->string('ada');
            $table->string('url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('praxeis', function(Blueprint $table){
            $table->dropColumn('praxi_type');
            $table->dropColumn('ada');
            $table->dropColumn('url');
        });
    }
}
