<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToAllteachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('all_teachers', function(Blueprint $table){
            $table->integer('klados_id')->unsigned();
            $table->integer('school_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('all_teachers', function(Blueprint $table){
            $table->removeColumn('klados_id')->unsigned();
            $table->removeColumn('school_id')->unsigned();
        });
    }
}
