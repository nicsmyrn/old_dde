<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RequestsSystemTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       //Create teachers table
        Schema::create('teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('middle_name');

            $table->integer('klados_id')->unsigned();
            $table->foreign('klados_id')->references('id')->on('eidikotita');

            $table->morphs('teacherable');

            $table->string('phone')->nullable();
            $table->string('mobile');
            $table->string('family_situation');
            $table->integer('childs')->default(0);
            $table->boolean('special_situation')->default(0);
            $table->string('address');
            $table->string('city');
            $table->string('tk')->nullable();
            $table->string('dimos_sinipiretisis')->nullable();
            $table->string('dimos_entopiotitas')->nullable();
            $table->float('years_experience')->nullable();

            $table->boolean('activation_for_register')->default(0);
            $table->boolean('is_checked')->default(0);

            $table->timestamps();
        });

        //Create monimoi table
        Schema::create('monimoi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('am');
            $table->integer('organiki');
            $table->integer('county');
            $table->float('moria')->nullable();
            $table->integer('orario')->nullable();
            $table->timestamps();
        });

        //Create anaplirotes table
        Schema::create('anaplirotes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('afm');
            $table->float('moria')->nullable();
            $table->integer('seira_topothetisis')->nullable();
            $table->string('type')->nullable();

            $table->timestamps();
        });

        //Create requests table
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');

            $table->string('unique_id')->unique();

            $table->integer('protocol_number')->nullable();

            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
            
            $table->string('schools_that_is')->nullable();
            $table->string('file_name')->nullable();


            $table->string('aitisi_type');
            $table->timestamp('date_request');

            $table->boolean('stay_to_organiki')->nullable();
            $table->integer('hours_for_request')->nullable();

            $table->string('subject')->nullable();
            $table->text('description')->nullable();

            $table->timestamps();
        });

        //Create prefrences table
        Schema::create('prefrences', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('request_id')->unsigned();
            $table->foreign('request_id')->references('id')->on('requests')->onDelete('cascade');

            $table->integer('order_number');
            $table->string('school_name');

            $table->timestamps();
        });

        //Create aitisi table
        Schema::create('aitisi', function (Blueprint $table) {
            $table->increments('id');

            $table->string('type');
            $table->text('description')->nullable();
            $table->string('for');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('aitisi');
        Schema::drop('prefrences');
        Schema::drop('anaplirotes');
        Schema::drop('monimoi');
        Schema::drop('requests');
        Schema::drop('teachers');
    }
}
