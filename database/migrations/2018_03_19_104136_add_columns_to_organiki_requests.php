<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrganikiRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organikes_requests', function(Blueprint $table){
            $table->float('proipiresia', 6, 3)->default(0)->unsigned();
            $table->integer('family')->default(0)->unsigned();
            $table->integer('kids')->default(0)->unsigned();
            $table->integer('ygeia_idiou')->default(0)->unsigned();
            $table->integer('ygeia_sizigou')->default(0)->unsigned();
            $table->integer('ygeia_teknon')->default(0)->unsigned();
            $table->integer('ygeia_goneon')->default(0)->unsigned();
            $table->integer('ygeia_aderfon')->default(0)->unsigned();
            $table->integer('exosomatiki')->default(0)->unsigned();
            $table->integer('entopiotita')->default(0)->unsigned();
            $table->integer('sinipiretisi')->default(0)->unsigned();
            $table->float('sum', 6, 3)->default(0)->unsigned();
            $table->float('moria_dieuthidi', 6,3);
            $table->string('groupType')->nullable();
            $table->boolean('stay_to_organiki')->default(false);
            $table->boolean('return_to_organiki')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
