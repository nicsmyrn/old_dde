<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNextMkToMisthodosia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('misthodosia', function(Blueprint $table){
            $table->date('next_mk')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('misthodosia', function(Blueprint $table){
            $table->removeColumn('next_mk');
        });
    }
}
