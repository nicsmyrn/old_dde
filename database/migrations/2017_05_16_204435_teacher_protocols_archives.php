<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TeacherProtocolsArchives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher-protocol-archives', function (Blueprint $table) {
            $table->increments('aa');

            $table->integer('id')->unsigned();
            $table->binary('type');
            $table->date('p_date');
            $table->string('from');
            $table->string('subject');

            $table->integer('f_id')->unsigned();
            $table->foreign('f_id')->references('id')->on('f');

            $table->longText('description');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
