<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesFakeProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //Create teachers table
        Schema::create('fake_teachers', function (Blueprint $table) {
            $table->increments('fid');
            $table->string('middle_name');

            $table->integer('klados_id')->unsigned();
            $table->foreign('klados_id')->references('id')->on('eidikotita');

            $table->morphs('teacherable');

            $table->string('phone')->nullable();
            $table->string('mobile');
            $table->string('family_situation');
            $table->integer('childs')->default(0);
            $table->boolean('special_situation')->default(0);
            $table->string('address');
            $table->string('city');
            $table->string('tk')->nullable();
            $table->string('dimos_sinipiretisis')->nullable();
            $table->string('dimos_entopiotitas')->nullable();
            $table->float('years_experience')->nullable();

            $table->boolean('activation_for_register')->default(0);
            $table->boolean('is_checked')->default(0);

            $table->integer('ex_years')->nullable();
            $table->integer('ex_months')->nullable();
            $table->integer('ex_days')->nullable();

            $table->boolean('sex');
            $table->boolean('request_to_change')->default(false);

            $table->timestamps();
        });

        //Create monimoi table
        Schema::create('fake_monimoi', function (Blueprint $table) {
            $table->increments('id');
            $table->string('am');
            $table->integer('organiki');
            $table->integer('county');
            $table->float('moria')->nullable();
            $table->float('moria_apospasis')->nullable();
            $table->integer('orario')->nullable();
            $table->timestamps();
        });

        //Create anaplirotes table
        Schema::create('fake_anaplirotes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('afm');
            $table->float('moria')->nullable();
            $table->integer('seira_topothetisis')->nullable();
            $table->string('type')->nullable();
            $table->integer('orario');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('fake_anaplirotes');
        Schema::drop('fake_monimoi');
        Schema::drop('fake_teachers');
    }
}
