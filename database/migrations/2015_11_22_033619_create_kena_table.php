<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKenaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kena', function(Blueprint $table){
            $table->integer('sch_id')->unsigned();
            $table->foreign('sch_id')->references('id')->on('schools')->onDelete('cascade');

            $table->integer('eid_id')->unsigned();
            $table->foreign('eid_id')->references('id')->on('eidikotita')->onDelete('cascade');

            $table->integer('value');

            $table->integer('last_user_login_id')->unsigned()->nullable();

            $table->string('description')->nullable();
            
            $table->primary(array('sch_id', 'eid_id'));

            $table->timestamps();
        });  
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kena');
    }
}

