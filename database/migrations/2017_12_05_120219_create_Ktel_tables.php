<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKtelTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create busroute table
        Schema::create('ktel_busroute', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->integer('municipality_id')->unsigned();
            $table->integer('routetype_id')->unsigned();

            $table->integer('aa_contract')->unique()->unsigned()->nullable();
            $table->string('name_type');

            $table->timestamps();
            $table->softDeletes();
        });


        //Create years table
        Schema::create('ktel_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('ktel_months', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('number');
            $table->string('name');
            $table->timestamps();
        });

        //Create period table
        Schema::create('ktel_period', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year_id')->unsigned();
            $table->integer('month_id')->unsigned();
            $table->boolean('disabled')->default(true);
            $table->unique(['year_id', 'month_id']);
            $table->timestamps();
        });

        //Create schoolroutes table
        Schema::create('ktel_schoolroutes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('school_id')->unsigned();

            $table->integer('route_id')->unsigned();

            $table->boolean('checked')->default(false);
            $table->boolean('locked')->default(false);


            $table->unique(['school_id', 'route_id']);

            $table->timestamps();
        });


        //Create periodroutes table
        Schema::create('ktel_period_routes', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('schoolroute_id')->unsigned();

            $table->integer('period_id')->unsigned();

            $table->integer('kids_number')->default(0);
            $table->integer('routes_number')->default(0);
            $table->text('description');
            $table->boolean('disabled')->default(false);

            $table->boolean('new');
            $table->time('starts_at')->nullable();

            $table->unique(['schoolroute_id', 'period_id']);

            $table->timestamps();
            $table->softDeletes();
        });

        //Create apousiologio table
        Schema::create('ktel_apousiologio', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('periodroute_id')->unsigned();

            $table->integer('days_apousias');
            $table->boolean('disabled')->default(false);


            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ktel_period_school_comments', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('school_id')->unsigned();

            $table->integer('period_id')->unsigned();

            $table->boolean('disabled');

            $table->string('comment',250);

            $table->timestamps();
            //some test
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
