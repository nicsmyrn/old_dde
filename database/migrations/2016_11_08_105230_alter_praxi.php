<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPraxi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('praxeis', function(Blueprint $table){
             $table->integer('dde_protocol')->unsigned()->nullable();
            $table->date('dde_protocol_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('praxeis', function(Blueprint $table){
            $table->dropColumn('dde_protocol');
            $table->dropColumn('dde_protocol_date');
        });
    }
}
