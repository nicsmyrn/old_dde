<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPlacements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('placements', function(Blueprint $table){
            $table->integer('placements_type')->unsigned();
            $table->integer('days')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('placements', function(Blueprint $table){
            $table->dropColumn('placements_type');
            $table->dropColumn('days');
        });
    }
}
