<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSchoolSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //Create survey table
        Schema::create('survey', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sch_id')->unsigned()->unique();
            $table->foreign('sch_id')->references('id')->on('schools');

            $table->boolean('double_hours')->default(0);
            $table->boolean('earthquake_check')->default(0);

            $table->boolean('ergP')->default(0);
            $table->integer('ergP_year')->nullable();
            $table->tinyInteger('ergP_status')->nullable();

            $table->boolean('ergX')->default(0);
            $table->integer('ergX_year')->nullable();
            $table->tinyInteger('ergX_status')->nullable();

            $table->boolean('ergL')->default(0);
            $table->integer('ergL_year')->nullable();
            $table->tinyInteger('ergL_status')->nullable();

            $table->boolean('ergG')->default(0);
            $table->integer('ergG_year')->nullable();
            $table->tinyInteger('ergG_status')->nullable();

            $table->boolean('ergE')->default(0);
            $table->integer('ergE_year')->nullable();
            $table->tinyInteger('ergE_status')->nullable();

            $table->float('faucet');
            $table->boolean('fire_check')->default(0);
            $table->float('studentpermeters');
            $table->integer('total');
            $table->float('wc');
            $table->boolean('yard_check')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey');
    }
}
