<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToEdata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('edata', function(Blueprint $table){
            $table->integer('ex_years')->default(0);
            $table->integer('ex_months')->default(0);
            $table->integer('ex_days')->default(0);
            $table->float('moria_ypiresias')->default(0);
            $table->float('moria_sinthikes')->default(0);
            $table->integer('moria_family_situation')->default(0);
            $table->integer('number_of_kids')->default(0);
            $table->integer('number_of_kids_spoudes')->default(0);

            $table->integer('sum_tekna')->default(0);
            $table->integer('moria_tekna')->default(0);
            $table->boolean('special_situation')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
