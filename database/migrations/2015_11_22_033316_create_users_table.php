<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();

            $table->morphs('userable');

            $table->string('password', 60);
            $table->integer('role_id')->unsigned();

            $table->rememberToken()->nullable();
            $table->timestamps();
            $table->string('provider');
            $table->string('provider_id')->unique()->nullable();
            $table->timestamp('login_at');

            $table->integer('counter')->default(0);
            $table->string('ip')->nullable();

            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
