<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePlacements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //Create praxeis table
        Schema::create('praxeis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('decision_number')->unique();
            $table->date('decision_date');
            $table->text('description');
            $table->timestamps();
        });

        //Create placements table
        Schema::create('placements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('klados');

            $table->integer('praxi_id')->unsigned();
            $table->foreign('praxi_id')->references('id')->on('praxeis')->onDelete('cascade');

            $table->string('teacher_name');
            $table->integer('hours')->unisgned()->nullable();
            $table->string('from');
            $table->string('to');
            $table->text('description')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('placements');
        Schema::drop('praxeis');        
    }
}

