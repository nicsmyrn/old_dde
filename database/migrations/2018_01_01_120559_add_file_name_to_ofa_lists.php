<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFileNameToOfaLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ofa_lists', function(Blueprint $table){
            $table->string('filename')->nullable();
            $table->boolean('locked')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ofa_lists', function(Blueprint $table){
            $table->dropColumn('filename');
            $table->dropColumn('locked');
        });    }
}
