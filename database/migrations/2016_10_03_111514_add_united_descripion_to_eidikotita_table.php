<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnitedDescripionToEidikotitaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('eidikotita', function (Blueprint $table){
            $table->string('united_description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('eidikotita', function (Blueprint $table){
            $table->removeColumn('united_description');
        });
    }
}
