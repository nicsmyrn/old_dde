<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OfaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create students table
        Schema::create('ofa_students', function (Blueprint $table) {

            $table->integer('am')->unsigned();
            $table->primary('am');

            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name')->nullable();

            $table->date('birth');
            $table->tinyInteger('class');
            $table->boolean('sex');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });

        //Create sports table
        Schema::create('ofa_sports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->boolean('individual');
            $table->smallInteger('list_limit');

            $table->softDeletes();
            $table->timestamps();
        });

        //Create special_sports table
        Schema::create('ofa_special_sports', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->integer('sport_id')->unsigned();
            $table->foreign('sport_id')->references('id')->on('ofa_sports')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });

        //Create group_lists table
        Schema::create('ofa_lists', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('year_id')->unsigned();
            $table->foreign('year_id')->references('id')->on('years_placements')->onDelete('cascade');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

            $table->integer('sport_id')->unsigned();
            $table->foreign('sport_id')->references('id')->on('ofa_sports')->onDelete('cascade');

            $table->smallInteger('gender');

            $table->unique(['year_id', 'school_id', 'sport_id', 'gender']);

            $table->string('teacher_name')->nullable();
        });

        //Create group_list_details table
        Schema::create('ofa_list_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('list_id')->unsigned();
            $table->foreign('list_id')->references('id')->on('ofa_lists')->onDelete('cascade');

            $table->integer('student_id')->unsigned();
            $table->foreign('student_id')->references('am')->on('ofa_students')->onDelete('cascade');

            $table->boolean('statement');

            $table->smallInteger('sport_id_special')->nullable();

            $table->timestamps();
        });

        //Create group_participations table
        Schema::create('ofa_participation_status', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('list_id')->unsigned();
            $table->foreign('list_id')->references('id')->on('ofa_lists')->onDelete('cascade');


            $table->string('description');
            $table->timestamps();
        });

        //Create ofa_p_status_details table
        Schema::create('ofa_p_status_details', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('p_status_id')->unsigned();
            $table->foreign('p_status_id')->references('id')->on('ofa_participation_status')->onDelete('cascade');

            $table->integer('list_details_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ofa_special_sports', function(Blueprint $table){
            $table->dropForeign('ofa_students_school_id_foreign');
        });
        Schema::table('ofa_students', function(Blueprint $table){
            $table->dropForeign('ofa_special_sports_sport_id_foreign');
        });
        Schema::table('ofa_lists', function(Blueprint $table){
            $table->dropForeign('ofa_lists_year_id_foreign');
            $table->dropForeign('ofa_lists_sport_id_foreign');
            $table->dropForeign('ofa_lists_school_id_foreign');
        });
        Schema::table('ofa_list_details', function(Blueprint $table){
            $table->dropForeign('ofa_list_details_list_id_foreign');
        });

        Schema::drop('ofa_p_status_details');
        Schema::drop('ofa_participation_status');
        Schema::drop('ofa_list_details');
        Schema::drop('ofa_lists');
        Schema::drop('ofa_special_sports');
        Schema::drop('ofa_sports');
        Schema::drop('ofa_students');
    }
}
