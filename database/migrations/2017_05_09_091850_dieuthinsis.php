<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dieuthinsis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create dieuthinsis table
        Schema::create('dieuthinsis', function (Blueprint $table) {
            $table->increments('id');

            $table->string('identifier')->unique()->nullable();
            $table->string('title');
            $table->string('nomos');
            $table->string('name');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dieuthinsis');
    }
}
