<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTeachersProtocol extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('teachers_protocol', function(Blueprint $table){
            $table->increments('id');
            $table->date('p_date');
            $table->string('from');
            $table->string('type');
            $table->string('subject');

            $table->integer('f_id')->unsigned();
            $table->foreign('f_id')->references('id')->on('f');

            $table->longText('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('teachers_protocol');
    }
}
