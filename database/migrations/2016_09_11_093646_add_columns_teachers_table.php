<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('teachers', function(Blueprint $table){
            $table->integer('ex_years')->nullable();
            $table->integer('ex_months')->nullable();
            $table->integer('ex_days')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('teachers', function(Blueprint $table){
            $table->dropColumn('ex_years');
            $table->dropColumn('ex_months');
            $table->dropColumn('ex_days');
        });
    }
}
