<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYearsOfPlacements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create years_placements table
        Schema::create('years_placements', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('current')->default(false);
            $table->timestamps();
        });

        Schema::table('praxeis', function(Blueprint $table){
            $table->integer('year_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
