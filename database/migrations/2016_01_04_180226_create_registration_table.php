<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create registration table
        Schema::create('registration', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('last_name');
            $table->string('first_name');
            $table->string('email')->unique();
            $table->string('password',60);
            $table->string('token',128);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('registration');
    }
}
