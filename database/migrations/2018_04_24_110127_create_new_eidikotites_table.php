<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewEidikotitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create eidikotites_new table
        Schema::create('eidikotites_new', function (Blueprint $table) {
            $table->increments('id');
            $table->string('klados_name');
            $table->string('klados_slug');
            $table->string('eidikotita_name');
            $table->string('eidikotita_slug');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eidikotites_new');
    }
}
