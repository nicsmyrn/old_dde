<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnNewEidikotitaTeachers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('myschool_teachers', function(Blueprint $table){
            $table->integer('new_eidikotita')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('myschool_teachers', function(Blueprint $table){
            $table->dropColumn('new_eidikotita');
        });
    }
}
