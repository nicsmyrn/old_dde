<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablesForOrganikes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organikes_requests', function (Blueprint $table) {
            $table->increments('id');

            $table->string('unique_id')->unique();

            $table->integer('protocol_number')->nullable();

            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');

            $table->string('school_organiki')->nullable();
            $table->string('file_name')->nullable();

            $table->string('aitisi_type');
            $table->timestamp('date_request');

            $table->string('situation');
            $table->text('description')->nullable();

            $table->timestamps();
        });

        Schema::create('organikes_prefrences', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('request_id')->unsigned();
            $table->foreign('request_id')->references('id')->on('organikes_requests')->onDelete('cascade');

            $table->integer('school_id')->unsigned();
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');

            $table->integer('order_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
