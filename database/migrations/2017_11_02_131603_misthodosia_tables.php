<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MisthodosiaTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('teachers', function(Blueprint $table){
//            $table->integer('fmid')->unsigned()->default(0);
//            $table->boolean('misthodosia_is_checked')->default(true);
//        });

        //Create misthodosia table
        Schema::create('misthodosia', function (Blueprint $table) {
            $table->increments('id');

            $table->string('at', 9)->nullable();
            $table->date('birth')->nullable();

            $table->integer('family_situation')->unsigned()->default(0);

            $table->string('iban',27);
            $table->integer('bank')->unsigned();

            $table->string('amka', 11)->unique();
            $table->integer('doy')->nullable();
            $table->string('am_tsmede', 15)->nullable();
            $table->string('am_ika', 15)->nullable();
            $table->boolean('new')->default(true);
            $table->integer('mk');

            $table->string('afm',9)->unique();
            $table->foreign('afm')->references('afm')->on('myschool_teachers')->onDelete('cascade');

            $table->timestamps();
        });

        //Create misthodosia_tekna table
        Schema::create('misthodosia_tekna', function (Blueprint $table) {
            $table->increments('id');

            $table->date('birth');
            $table->date('college_start_date')->nullable();
            $table->date('college_end_date')->nullable();

            $table->string('description')->nullable();

            $table->integer('misthodosia_id')->unsigned();
            $table->foreign('misthodosia_id')->references('id')->on('misthodosia')->onDelete('cascade');

            $table->timestamps();
        });

        //Create misthodosia table
        Schema::create('misthodosiaFake', function (Blueprint $table) {
            $table->increments('fmid');

            $table->string('at', 9)->nullable();
            $table->date('birth')->nullable();

            $table->integer('family_situation')->unsigned()->default(0);

            $table->string('iban',27);
            $table->integer('bank')->unsigned();

            $table->string('amka', 11)->unique();
            $table->integer('doy')->nullable();
            $table->string('am_tsmede', 15)->nullable();
            $table->string('am_ika', 15)->nullable();
            $table->boolean('new')->default(true);
            $table->integer('mk');

            $table->integer('teacher_id')->unsigned();
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');

            $table->timestamps();
        });

        //Create misthodosia_tekna table
        Schema::create('misthodosia_teknaFake', function (Blueprint $table) {
            $table->increments('id');

            $table->date('birth');
            $table->date('college_start_date')->nullable();
            $table->date('college_end_date')->nullable();

            $table->string('description')->nullable();

            $table->integer('misthodosia_id')->unsigned();
            $table->foreign('misthodosia_id')->references('fmid')->on('misthodosiaFake')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
