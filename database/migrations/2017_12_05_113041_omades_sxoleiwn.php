<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OmadesSxoleiwn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create omades table
        Schema::create('omades', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
        });

        Schema::table('schools', function(Blueprint $table){
            $table->integer('municipality_id')->unsigned();
            $table->string('work_phone',10)->nullable();
            $table->string('mobile_phone',10)->nullable();
            $table->tinyInteger('sex')->default(0);
            $table->smallInteger('area')->nullable();
            $table->boolean('low_functionality')->default(false);

            $table->integer('omada')->unsigned();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
