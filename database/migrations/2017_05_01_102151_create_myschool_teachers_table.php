<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMyschoolTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create myschool_teachers table
        Schema::create('myschool_teachers', function (Blueprint $table) {
            $table->string('afm', 9);
            $table->primary('afm');

            $table->string('am');

            $table->boolean('sex');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('mothers_name');
            $table->string('phone', 10);
            $table->string('mobile', 10);
            $table->string('eidikotita');
            $table->string('vathmos',2);

            $table->integer('ypoxreotiko');
            $table->integer('organiki_prosorini');
            $table->string('topothetisi');
            $table->date('birth');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('myschool_teachers');
    }
}
