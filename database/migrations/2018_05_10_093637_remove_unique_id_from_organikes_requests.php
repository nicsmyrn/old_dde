<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUniqueIdFromOrganikesRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('organikes_requests', function(Blueprint $table){
            $table->foreign('teacher_id')->references('id')->on('teachers')->onDelete('cascade');
//            $table->boolean('locked')->default(false);
//            $table->dropForeign('organikes_requests_teacher_id_foreign');
//            $table->dropUnique('organikes_requests_teacher_id_aitisi_type_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
