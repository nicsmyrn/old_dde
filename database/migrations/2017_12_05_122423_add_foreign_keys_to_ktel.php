<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysToKtel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function(Blueprint $table){
            $table->foreign('omada')->references('id')->on('omades');
        });

        Schema::table('ktel_period', function (Blueprint $table) {
            $table->foreign('year_id')->references('id')->on('ktel_years')->onDelete('cascade');
            $table->foreign('month_id')->references('id')->on('ktel_months')->onDelete('cascade');
        });

        Schema::table('ktel_schoolroutes', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->foreign('route_id')->references('id')->on('ktel_busroute')->onDelete('cascade');
        });

        Schema::table('ktel_period_routes', function (Blueprint $table) {
            $table->foreign('schoolroute_id')->references('id')->on('ktel_schoolroutes')->onDelete('cascade');
            $table->foreign('period_id')->references('id')->on('ktel_period')->onDelete('cascade');
        });

        Schema::table('ktel_apousiologio', function (Blueprint $table) {
            $table->foreign('periodroute_id')->references('id')->on('ktel_period_routes')->onDelete('cascade');
        });

        Schema::table('ktel_period_school_comments', function (Blueprint $table) {
            $table->foreign('school_id')->references('id')->on('schools')->onDelete('cascade');
            $table->foreign('period_id')->references('id')->on('ktel_period')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
