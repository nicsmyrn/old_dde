<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToPlacements extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('placements', function(Blueprint $table){
            $table->removeColumn('teacher_id');
            $table->morphs('teacherable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('placements', function(Blueprint $table){
            $table->integer('teacher_id')->unsigned();
            $table->removeColumn('teacherable_id');
            $table->removeColumn('teacherable_type');
        });

    }
}
