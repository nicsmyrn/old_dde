<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAllTeachers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create all_teachers table
        Schema::create('all_teachers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('school_name');
            $table->string('last_name');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('klados');
            $table->integer('ypoxreotiko');
            $table->integer('hours_at_organiki');
            $table->string('category');
            $table->string('situation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
