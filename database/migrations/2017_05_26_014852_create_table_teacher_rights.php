<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTeacherRights extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Create teacher_rights table
        Schema::create('teacher_rights', function (Blueprint $table) {
            $table->string('afm');
            $table->primary('afm');

            $table->integer('year');
            $table->boolean('onomastika_yperarithmos');
            $table->boolean('dikaiwma_veltiwsis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teacher_rights');
    }
}
