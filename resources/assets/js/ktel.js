
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';

import AjaxDelete from './components/AjaxDelete.vue';
import BusRoutes from './components/BusRoutes';
import ChooseRoute from './components/ChooseRoute';
import EditRoutePeriod from './components/EditRoutePeriod';
import ManagePeriod from './components/ManagePeriod';

//s

Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

var parent = new Vue ({
    el : '#app',

    components : {
        AjaxDelete , BusRoutes, ChooseRoute, EditRoutePeriod, ManagePeriod
    }
});
