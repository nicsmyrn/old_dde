
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

import Alert from './components/Alert.vue';

var parent = new Vue ({
    el : '#app',

    data : {
        schools : null,
        eidikotites : null,
        currentSchool : 0,
        alert : {
            message : 'Ειδοποίηση',
            type : 'info',
            important : false
        }
    },

    ready () {
        this.fetchSchools();
    },

    methods : {
        fetchSchools (){
            this.$http.get('/aj/organika/schools')
                .then(r => this.schools = r.data)
                .catch(r => console.log('error'))
        },

        fetchEidikotitesForSchool (){
            this.$http.post('/aj/organika/eidikotitesWithKenaPleonasmata', {
                schoolId : this.currentSchool
            })
                .then(r => this.eidikotites = r.data)
                .catch(r => console.log('error'))
        },

        saveEidikotita (id){
            let eidikotita = this.eidikotites.filter(obj => {
                if(obj.id == id) return obj;
            });

            let number = Number.parseInt(eidikotita[0].number);

            console.log('SchoolId:' + this.currentSchool + ' EidikotitaId:' + eidikotita[0].id + ' Number:'+ number);

            if(Number.isInteger(number)){
                this.$http.post('/aj/organika/saveEidikotaToSchool', {
                    schoolId : this.currentSchool,
                    eidikotitaId : eidikotita[0].id,
                    number : number
                })
                    .then(r => {
                        if(r.data != ''){
                            this.displayAlert(r.data, 'success')
                        }
                    })
                    .catch(r => this.displayAlert('ERROR: Δεν αποθηκεύτηκε', 'danger'))
            }else{
                console.log('Error Message. Number is not Integer');
            }
        },
        displayAlert(message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.$broadcast(`displayMsg`, this.alert)
        }
    },

    components : {
        Alert
    }
});
