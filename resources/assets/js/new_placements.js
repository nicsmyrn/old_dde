var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['value'].nodeValue : '';

import Alert from './components/Alert.vue';
import {dataAlert} from './helpers/AlertHelper.js';

Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});


var parent = new Vue ({
    el: '#placements',

    created: function () {
        $('#loader').hide();
    },

    data : {
        alert : dataAlert,
        loaderDisplay : false,

        showRight : false,
        eid_id : null,

        klados_slug : null,

        temporarySchools : [],
        temporaryTeachers : [],
        allTeachers : [],
        allSchools : [],
        allSchoolsList : [],
        pleonazontes : [],

        currentTeacher : {
            sxesi           : null,
            id              : null,
            full_name       : null,
            middle_name     : null,
            moria           : null,
            organiki        : null,
            organiki_id     : null,
            klados          : null,
            entopiotita     : null,
            sinipiretisi    : null,
            special         : null,
            ypoxreotiko     : null,
            placements      : [],
            topothetisis    : [],
            totalPlacements : 0,
            totalTopothetisis : 0
        },


        placements : [
        ],

        decision_number: null,
        decision_date: '',

        newPlacement : {
            to: null,
            hours: null,
            days: null,
            description : '',
            placementType : 0,
            aitisi : 1,
            ellimma : true
        },

        allPlacementsTypes : []
    },

    computed: {
        notZeroSchoolLength : {
            get (){
                if (this.temporarySchools === undefined){
                    return this.$set('temporarySchools', []);
                }else{
                    return this.temporarySchools.filter(function(school){
                        return school.pivot.value !=  0;
                    }.bind(this));
                }
            }
        },

        currentTeacherTotal () {
            let total = 0;

            for(let t of this.currentTeacher.topothetisis){
                total = total + Number.parseInt(t.hours);
            }

            return total;
            //this.currentTeacher.topothetisis.forEach(t => {
            //    total = total + Number.parseInt(t.hours);
            //}, total);
            //return total;
        },

        currentTeacherTotalPlacements (){
            let total = 0;
            this.currentTeacher.placements.forEach(t => {
                total = total + Number.parseInt(t.hours);
            }, total);
            return total;
        }
    },

    filters : {
        sxoleiaTopothetis (schools){
            return schools.filter(function(school){
                return school.pivot.value < 0 ? school:null;
            });
        },

        sxoleiaOrganikis (schools){
            return schools.filter(function(school){
                return school.pivot.value > 0 ? school:null;
            });
        },

        notZeroSchool (schools){
            return schools.filter(function(school){
                return school.pivot.value != 0 ? school:null;
            });
        }
    },

    methods: {

        initializeNewPlacement (){
            this.newPlacement.to = null;
            this.newPlacement.hours = null;
            this.newPlacement.days = null;
            this.newPlacement.description = '';
            this.newPlacement.placementType = 0;
            this.newPlacement.aitisi = 1;
            this.newPlacement.ellimma = true;
        },

        initializeCurrentTeacher(){
            this.currentTeacher.sxesi = null;
            this.currentTeacher.id = null;
            this.currentTeacher.full_name = null;
            this.currentTeacher.middle_name = null;
            this.currentTeacher.moria = null;
            this.currentTeacher.organiki = null;
            this.currentTeacher.organiki_id = null;
            this.currentTeacher.klados = null;
            this.currentTeacher.entopiotita = null;
            this.currentTeacher.sinipiretisi = null;
            this.currentTeacher.special = null;
            this.currentTeacher.ypoxreotiko = null;
            this.currentTeacher.placements = [];
            this.currentTeacher.totalPlacements = 0;
            this.totalTopothetisis = 0;
        },

        date2f (e){
            e.preventDefault();
        },

        saveChanges(e) {
            e.preventDefault();

            this.loaderDisplay = true;
            this.$http.post('/aj/placements/savePlacementWithNewEidikotita',{
                placements : this.placements,
                decision_date : this.decision_date,
                decision_number : Number.parseInt(this.decision_number),
                schools : this.temporarySchools
            })
                .then(r => this.loaderDisplay = false)
                .then(r => {
                    this.initiliseAllPage();
                    this.displayAlert('Συγχαρητήρια!', `Η Πράξη ${this.decision_number} με ημερομηνία ${this.decision_date} αποθηκεύτηκε με επιτυχία`,'success');
                })
                .catch(r=> console.log('Error'));
        },

        initiliseAllPage(){
            this.klados_slug = null;
            this.initializeCurrentTeacher();
            this.initializeNewPlacement();
            this.initializePlacements();
            this.temporarySchools = [];
            this.temporaryTeachers = [];
        },

        changeEidikotita(){
            this.fetchSchools(this.klados_slug);
            this.initializeCurrentTeacher();
        },

        initializePlacements (){
            this.placements = [];
        },

        addPlacement(e){
            e.preventDefault();

            let organiki = this.searchSchool(this.temporarySchools,this.currentTeacher.organiki_id);
            let to_school = this.searchSchool(this.temporarySchools, this.newPlacement.to);

            this.pushPlacement();
            this.placementUpdateFromSchool();
            this.placementUpdateToSchool();

            this.initializeNewPlacement();

        },

        searchSchool (schools, keyword){
            return schools.filter(school => {
                if (Number.parseInt(keyword) === Number.parseInt(school.id)){
                    return school;
                }
            },[keyword]);
        },

        placementUpdateFromSchool(){
            this.temporarySchools.filter(school => {
                if(this.currentTeacher.source == 'pleonazon'){
                    if (school.id == this.currentTeacher.organiki_id){
                        console.log(Number.parseInt(school.pivot.value) + '-' + Number.parseInt(this.newPlacement.hours));
                        school.pivot.value = Number.parseInt(school.pivot.value) - Number.parseInt(this.newPlacement.hours);
                        return school
                    }
                }else{
                    console.log('not changes to school source');
                }
                return school
            });
        },

        placementUpdateToSchool(){
            this.temporarySchools.filter(school => {
                if(this.newPlacement.ellimma){
                    if (school.id == this.newPlacement.to.id){
                        console.log(Number.parseInt(school.pivot.value) + '+' + Number.parseInt(this.newPlacement.hours));
                        school.pivot.value = Number.parseInt(school.pivot.value) + Number.parseInt(this.newPlacement.hours);
                        return school
                    }
                }

                return school
            });
        },

        placementHasOvertime (from_school, to_school){
            var from_hours = from_school[0]? Math.abs(from_school[0].pivot.value): 0;
            var to_hours = to_school[0]? Math.abs(to_school[0].pivot.value):0;

            this.NewRecord.overtime = (from_hours < to_hours) && from_hours != 0 ? (to_hours - from_hours) :0;
        },

        pushPlacement (){
            //let extra_description = this.NewRecord.overtime > 0 ? ' + '+this.NewRecord.overtime+' ωρες υπερωρία': '';

            //var placements_from_name = from_school[0] ? from_school[0].name : 'Διάθεση ΠΥΣΔΕ';
            //var placements_to_name = to_school[0] ? to_school[0].name : 'ΔΔΕ Χανιων';

            this.currentTeacher.placements.push({
                to : this.newPlacement.to,
                hours : this.newPlacement.hours,
                days : this.newPlacement.days,
                description : this.newPlacement.description,
                type : this.newPlacement.placementType,
                aitisi : this.newPlacement.aitisi,
                ellimma : this.newPlacement.ellimma
            });

            //let currentTotal = 0;
            //this.currentTeacher.placements.forEach(t => {
            //     currentTotal = currentTotal + Number.parseInt(t.hours)
            //},currentTotal);

            this.currentTeacher.totalPlacements = this.currentTeacherTotalPlacements;
            this.currentTeacher.totalTopothetisis = this.currentTeacherTotal;

            //this.currentTeacher.placements.push('totalPlacements', currentTotal);
            //console.log(this.currentTeacher.totalPlacements);

            this.placements.$remove(this.currentTeacher);
            this.placements.push(this.currentTeacher);
        },

        placementInitializeNewRecord (){
            this.NewRecord.name = '';
            this.NewRecord.from = {id:0, value: 'Something from'};
            this.NewRecord.t_o   = {id:0, value: 'Something to'};
            this.NewRecord.hours = null;
            this.NewRecord.description = '';
            this.NewRecord.overtime = 0;
        },

        placementIsApospasi (){
            return this.Apospasi.type == "1"
        },

        placementIsRegular (){
            return this.Apospasi.type == "0"
        },

        removeTeacher(teacher){
            teacher.placements.forEach(function(p){
                this.removePlacementUpdateSchools(p.to.id, p.hours, teacher.organiki_id, teacher.source, p.ellimma);
            }, this);
            this.placements.$remove(teacher);
            this.currentTeacher.placements = [];
        },

        removeTeacherPlacement (teacher, placement){
            this.placements.filter(teachers => {
                if (teachers.id === teacher.id){
                    if(teachers.placements.length == 1){
                        this.removeTeacher(teacher)
                    }else{
                        teachers.placements.$remove(placement);
                        teachers.totalPlacements = teachers.totalPlacements - Number.parseInt(placement.hours);
                        this.removePlacementUpdateSchools(placement.to.id, placement.hours, teacher.organiki_id, teacher.source, placement.ellimma);
                    }

                    return teacher;
                }
            }, [teacher, placement]);

            this.currentTeacher.placements.$remove(placement);
        },

        removePlacementUpdateSchools(schoolId_to, hours, organikiSchoolId, source, ellimma){
            this.temporarySchools.filter(school => {
                if(ellimma){
                    if (school.id == schoolId_to){
                        school.pivot.value = Number.parseInt(school.pivot.value) - Number.parseInt(hours);
                        return school
                    }
                }

                if(source == 'pleonazon'){
                    if (school.id == organikiSchoolId){
                        school.pivot.value = Number.parseInt(school.pivot.value) + Number.parseInt(hours);
                        return school
                    }
                }

                return school
            },[schoolId_to, hours, organikiSchoolId, source]);
        },

        editPlacement (placement){
            this.removePlacement(placement);
            this.NewRecord = placement;
            this.$$.focus.focus();
        },

        editTopothetisi (topothetisi){
            topothetisi.edit_cell = true;
        },
        deleteTopothetisi (topothetisi){
            this.$http.post(`aj/deletePlacement`,{
                id : topothetisi.id
            })
                .then(r => {
                    this.displayAlert('Σημείωση!',
                        `Η τοποθέτηση του καθηγητή ${topothetisi.teacher_name} ->
                         ${topothetisi.hours} ώρες στο ${topothetisi.to}  πράξη:
                         ${topothetisi.praxi_id}/${topothetisi.created_at} ΔΙΕΓΡΑΦΗ
                         . `, 'danger', true);
                    this.currentTeacher.topothetisis.$remove(topothetisi);
                    //console.log(this.$get('placements.'+this.currentTeacher));
                    this.placements.filter(function(teacher){
                        if (teacher.id == this.currentTeacher.id){
                            console.log('Id is the same'+ teacher.id);
                            teacher.totalTopothetisis = teacher.totalTopothetisis - Number.parseInt(topothetisi.hours);
                        }
                    }.bind(this));
                })
                .catch(r=> console.log('Error'));

        },
        saveTopothetisi (topothetisi){
            topothetisi.edit_cell = false;
            this.displayAlert('Συγχαρητήρια!',`Η πράξη τροποποιήθηκε με επιτυχία`, 'success')
        },

        fetchSchools  (klados_slug) {
            this.$http.get('jsonAllPlacementsTypes')
                .then(r=> this.$set('allPlacementsTypes', r.data))
                .catch(r=>console.log('Error231'));

            this.$http.get('jsonAllTeachersOfNewKladoi/'+klados_slug)
                .then(r => this.$set('allTeachers', r.data))
                .catch(r=> console.log('ErrorAllTeachers'));

            this.$http.get('jsonAllSchoolsList')
                .then(r => this.$set('allSchoolsList', r.data))
                .catch(r=> console.log('Error'));
        },

        displayAlert(header, message, type, important = false){
            this.alert.header = header;
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.$broadcast(`displayMsg`, this.alert)
        }

    },

    components : {
        Alert
    }

});
