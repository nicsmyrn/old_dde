var socket = io('http://srv-dide.chan.sch.gr:3000');
var TYPING_TIMER_LENGTH = 2000;

Vue.transition('bounce', {
    enterClass: 'fadeIn',
    leaveClass: 'fadeOut'
});

Vue.component('select2', {
    props : ['universities', 'value'],
    template : `<select>
                    <optgroup v-for="university in universities" label="{{university.city}}">
                        <option
                            v-for="department in university.departments"
                            v-bind:value="department.id"
                        >
                            {{ department.name }}
                        </option>
                    </optgroup>
                </select>`,
    ready : function(){
        var vm = this;
        $(this.$el)
            .val(this.value)
            .select2()
            .on('change', function(){
                vm.$emit('change', this.value);
            });
    },
    events : {
        change (value) {
            this.value  = value;
        }
    },
    watch : {
        value : function (value){
            $(this.$el).select2('val', value)
        }
    },
    destroyed : function(){
        $(this.$el).off().select2('destroy')
    }
});

var parent = new Vue ({
    el : '#chat_page',

    data : {
        room : null,
        peer : null,
        peerID : null,
        //username : null,
        message  : null,
        all_messages : [],
        loginScreen : true,
        typing : {
            username : null,
            body : 'πληκτρολογεί...'
        },
        selected : 0,
        universities : [
            { city : 'Κομοτηνή', departments : [
                { id : 1, name : 'Γλώσσας, Φιλολογίας Και Πολιτισμού Παρευξείνιων Χωρών - ΔΠΘ Κομοτηνή'},
                { id : 2, name : 'Ελληνικής Φιλολογίας - ΔΠΘ Κομοτηνή'},
                { id : 3, name : 'Hellenic Studies - ΔΠΘ Κομοτηνή'},
                { id : 4, name : 'Ιστορίας Και Εθνολογίας - ΔΠΘ Κομοτηνή'}
            ]},
            { city : 'Ξάνθη', departments : [
                { id : 5, name : 'Κλασικών Και Ανθρωπιστικών Σπουδών'},
                { id : 6, name : 'Κοινωνικών, Πολιτικών Και Οικονομικών Επιστημών '},
                { id : 7, name : 'Κοινωνικής Διοίκησης Και Πολιτικής Επιστήμης'},
                { id : 8, name : 'Νομικής'}
            ]}
        ]
    },

    ready : function (){

        this.getUsernameColor('temp');
        this.recieveMessage();
        this.userTyping();
        this.userStopTyping();
        this.startChat();
        this.endChat();

    },

    computed : {
        username (){
            for(let university of this.universities){
                for(let department of university.departments){
                    if (department.id === Number.parseInt(this.selected)){
                        return department.name;
                    }
                }
            }
            return null
        }
    },


    methods : {

        updateScrollBar(){
            // ###################    needs update  #######################
            let scrollHeight = this.$els.scroller.scrollHeight;
            let clientHeight = this.$els.scroller.clientHeight;
            this.$nextTick(() => {
                this.$els.scroller.scrollTop = 50000 ;  //  LEITOURGEI alla den einai dunamiko
                console.log('after');
            });
            // ###################    needs update  #######################
        },

        sentMessage (){
            socket.emit('new message', {
                username : this.username,
                message : this.message,
                room : this.room
            });
            this.all_messages.push({
                username : this.username,
                body     : this.message
            });
            this.message = null;

            this.updateScrollBar();
        },

        loginToChat (){
            if (this.username){    // εδώ πρέπει να γίνουν έλεγχοι
                this.loginScreen = false;
                socket.emit('login', this.username);
            }else{
                console.log('error');
            }
        },

        newConverstation(){
            console.log('new converstation');
            this.leaveChat();
            this.loginToChat();
        },

        getUsernameColor(username){
            let hash = '';
            for (let i=0; i<username.length; i++){
                hash = hash + username.charCodeAt(i);
            }
            return '#'+Number.parseInt(hash).toString(16).substr(0,6);
        },

        checkTyping(){
            if (this.room){
                socket.emit('typing', this.room);
                this.stopTyping();
            }
        },

        stopTyping(){
            let lastTypingTime = (new Date()).getTime();

            setTimeout(function () {
                var typingTimer = (new Date()).getTime();
                var timeDiff = typingTimer - lastTypingTime;
                if (timeDiff >= TYPING_TIMER_LENGTH) {
                    socket.emit('stop typing', this.room);
                }
            }.bind(this), TYPING_TIMER_LENGTH);
        },

        recieveMessage (){
            socket.on('new message', function (data) {
                this.all_messages.push(data);
                this.updateScrollBar();
            }.bind(this), this.room);
        },

        userTyping (){
            socket.on('typing', function(data){
                this.typing.username = data.username;
            }.bind(this));
        },

        userStopTyping(){
            socket.on('stop typing', function(data){
                this.typing.username = null;
            }.bind(this));
        },

        startChat(){
            socket.on('start chat', function(data){
                this.connected = true;
                this.room = data.room;
                this.peer = data.peername;
                this.peerID = data.peerID
                var object = this.$els.messageInput;
                console.log(object);
                Vue.nextTick(function(){
                    object.focus();
                }.bind(this));

            }.bind(this));
        },

        endChat(){
            socket.on('end chat', function(){
                //this.initData();
                this.all_messages.push({
                    username : 'me',
                    body : 'user is disconnected'
                });
            }.bind(this));
        },

        leaveChat(){
            this.loginScreen = true;

            socket.emit('leave room', {
                room : this.room,
                peerID : this.peerID,
                peer: this.peer,
                myID : socket.id
            });
            this.initData();
        },

        initData(){
            this.room = null;
            this.peer = null;
            this.peerID = null;
            this.all_messages = [];
        }
    }
});