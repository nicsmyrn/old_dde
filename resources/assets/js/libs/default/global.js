$.msg = function(subject, text, style, appendTo){

    var styles = {
        success : 'alert alert-block alert-success flyover flyover-centered',
        info: 'alert alert-info flyover flyover-centered',
        warning : 'alert alert-warning flyover flyover-centered',
        danger: 'alert alert-danger flyover flyover-centered'
    }

    var html = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
        '<strong>'+subject+':</strong>&MediumSpace;'+text;

    $('<div>')
        .attr('id','div_message_alert')
        .addClass(styles[style])
        .attr('role', 'alert')
        .html(html)
        .appendTo($('#'+appendTo))
        .fadeIn('slow', function(){
            $(this).addClass('in');
        })
        .animate({opacity: 1}, 1000,function(){
            $(this).removeClass('in');
        })
        .fadeOut(function(){
            $(this).remove();
        })
};

$.swalAlert = function (args){
    swal({
        title: args['title'],
        text: args['body'],
        type: args['level'],
        timer : 2500,
        showConfirmButton: false
    });
};
$.swalAlertValidatorError = function(data){
    if( data.status === 422 ) {
        errors = data.responseJSON;

        errorsHtml = '<div><ul>';

        $.each( errors, function( key, value ) {
            errorsHtml += '<li>' + value + '</li>'; //showing only the first error.
        });

        errorsHtml += '</ul></div>';
    }

    swal({
        title: 'Προσοχή',
        text: errorsHtml,
        type: 'error',
        confirmButtonText: 'Κλείσιμο',
        html : true
    });
};
