
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


import Alert from '../components/Alert.vue';
import {dataAlert} from '../helpers/AlertHelper.js';
import ModalStudent from './components/ModalNewStudent.vue';
import ModalAgreement from './components/ModalOfaAgreement.vue';


Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

var parent = new Vue ({
    el : '#app',

    data : {
        teacher_name : '',
        synodos : '',
        nullValue : null,
        sex : [
            'Κορίτσια', 'Αγόρια', 'Μικτή Ομάδα'
        ],
        alert: dataAlert,
        dataSet : {
            sports : null,
                    year : {
                        students : null,
                name : null,
                id : null
            }
        },
        current_sport : null,
        current_sex : null,
        sport_name : null,

        selectedStudent : null,
        allStudentsByGender : [],
        studentsList : [],
        loading : true,

        showModalStudent : false,
        showModalAgreement : false,
        hideLoader : true,

        loadingForExistence : true,
        notExistsPdfFile: true,
        existedListId : 0,

        showIndividualForm : false,
        loadingForExistenceIndividual : true,
        notExistsPdfFileIndividual : true,
        allStudentsIndividual : [],
        selectedStudentIndividual : null,
        studentsListIndividual : [],
        selectedIndividualSport : null,
        individualSports : []
    },

    computed :{
        showListForm (){
            return !!((this.current_sport != null) && (this.current_sex != null) && (this.dataSet.year.id != null));
        }
    },

    created (){
        this.fetchDataFromDatabase();
    },


    components : {
        Alert, ModalStudent, ModalAgreement
    },

    methods : {
        showModalNewStudentIndividual(){
            this.showModalStudent = true;
            this.current_sex = 3;
            console.log('must open... for Individual Sport');
        },

        temporarySaveIndividual(){
            parent.hideLoader = false;

            parent.$http.post('/aj/ofa/temporarySaveNewListIndividualPrimary',{
                current_sport : parent.current_sport,
                current_sex : 3,                       //individual sex not exists
                studentsListIndividual : parent.studentsListIndividual,
                year : parent.dataSet.year,
                synodos : parent.synodos,
                teacher_name : parent.teacher_name,
                existedListId : parent.existedListId
            })
                .then(r => {
                    this.existedListId = r.data;
                    this.displayAlert('Συγχαρητήρια!', 'Η Κατάσταση Συμμετοχής για τα ατομικά αθλήματα αποθηκεύτηκε με επιτυχία', 'success');
                    parent.hideLoader = true;
                })
                .catch(r => {
                    parent.hideLoader = true;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.245', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        fetchDataFromDatabaseIndividual (){
            this.$http.get('/aj/ofa/fetchDataForListsIndividual')
                .then(r => {
                    this.$set('dataSet', r.data);
                    this.loading = false;
                })
                .catch(r => {
                    this.loading = false;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.303', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        checkIfListAlreadyExistsIndividual(){
            this.existedListId = 0;

            this.$http.post('/aj/ofa/checkIfListAlreadyExistsForIndividual', {
                sport : this.current_sport,
                year_id : this.dataSet.year.id
            })
                .then(r => {
                    console.log(r.data);
                    this.individualSports = r.data["special_sports"].sort(this.sortSports);

                    this.individualSports = r.data["special_sports"];

                    if(r.data["response"] == 'canMakeSchoolListIndividual'){
                        if(this.synodos == ''){
                            this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον συνοδό του Σχολείου σας', 'warning');
                        }
                        this.notExistsPdfFileIndividual = true;

                    }else if(r.data["response"] == 'isLockedIndividual'){
                        console.log('synodos loaded');
                        parent.synodos = r.data["synodos"];
                        parent.current_sex = 3;
                        parent.notExistsPdfFileIndividual = false;
                    }else{
                        if(r.data["response"] == 'temporaryListExistsIndividual'){
                            parent.synodos = r.data["synodos"];
                            let existedStudents = r.data["list"];
                            parent.notExistsPdfFileIndividual = true;
                            parent.existedListId = r.data["list_id"];
                            let list_details = r.data['studentListIndividual'];
                            var i;
                            for (i = parent.allStudentsIndividual.length - 1; i >= 0; i -= 1) {
                                if (existedStudents.includes(parent.allStudentsIndividual[i]["am"])) {
                                    parent.studentsListIndividual.push({
                                        am : parent.allStudentsIndividual[i]["am"],
                                        birth : parent.allStudentsIndividual[i]["birth"],
                                        class : parent.allStudentsIndividual[i]["class"],
                                        first_name : parent.allStudentsIndividual[i]["first_name"],
                                        last_name : parent.allStudentsIndividual[i]["last_name"],
                                        loader : 0,
                                        middle_name : parent.allStudentsIndividual[i]["middle_name"],
                                        mothers_name : parent.allStudentsIndividual[i]["mothers_name"],
                                        school_id : parent.allStudentsIndividual[i]["school_id"],
                                        sex : parent.allStudentsIndividual[i]["sex"],
                                        year_birth : parent.allStudentsIndividual[i]["year_birth"],
                                        sport_id_special : list_details.find(function(obj){
                                            return obj.student_id === parent.allStudentsIndividual[i]["am"]
                                        })["sport_id_special"],
                                        sport_name_special : list_details.find(function(obj){
                                            return obj.student_id === parent.allStudentsIndividual[i]["am"]
                                        })["special_name"]
                                    });
                                    parent.allStudentsIndividual.splice(i, 1);
                                }
                            }

                            parent.studentsListIndividual.sort(parent.sortStudentsBySpecialName);

                        }
                    }
                    this.loadingForExistenceIndividual = false;
                    this.loadingForExistence = false;
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.782', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))

        },

        addStudentToIndividualList(student, sport_special){
            console.log(sport_special);

            parent.studentsListIndividual.push({
                am : student.am,
                birth : student.birth,
                class : student.class,
                first_name : student.first_name,
                last_name : student.last_name,
                loader : 0,
                middle_name : student.middle_name,
                mothers_name : student.mothers_name,
                school_id : student.school_id,
                sex : student.sex,
                year_birth : student.year_birth,
                sport_id_special : sport_special.id,
                sport_name_special : sport_special.name
            });

            this.studentsListIndividual.sort(this.sortStudentsBySpecialName);

            this.allStudentsIndividual.$remove(student);
            this.selectedStudentIndividual = null;
            console.log('add student & remove Individual ');
        },

        addNewStudentToAllStudents(student){
            parent.allStudentsIndividual.push(student);
            parent.allStudentsIndividual.sort(this.sortStudents);
        },

        deleteStudentIndividual(student){
            this.studentsListIndividual.$remove(student);
            this.allStudentsIndividual.push(student);
            this.allStudentsIndividual.sort(this.sortStudents);
        },

        sentRequestIndividual (){

            parent.hideLoader = false;

            parent.studentsListIndividual.sort(parent.sortStudentsBySpecialSport);

            parent.$http.post('/aj/ofa/insertNewListIndividualPrimary',{
                current_sport : parent.current_sport,
                current_sex : 3,
                studentsList : parent.studentsListIndividual,
                year : parent.dataSet.year,
                teacher_name : parent.teacher_name,
                synodos      : parent.synodos,
                existedListId : parent.existedListId
            })
                .then(r => {
                    location.href = r.data;
                    //console.log(r.data);
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.999', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },

        downloadList(){
            this.displayAlert('Παρακαλώ περιμένετε...', 'Το αρχείο είναι σε διαδικασία κατεβάσματος στον υπολογιστή σας', 'danger');
        },

        temporarySave (){
            parent.hideLoader = false;

            parent.$http.post('/aj/ofa/primary/temporarySaveNewList',{
                current_sport : parent.current_sport,
                current_sex : parent.current_sex,
                studentsList : parent.studentsList,
                year : parent.dataSet.year,
                teacher_name : parent.teacher_name,
                synodos : parent.synodos,
                existedListId : parent.existedListId
            })
                .then(r => {
                    this.existedListId = r.data;
                    this.displayAlert('Συγχαρητήρια!', 'Η κατάσταση συμμετοχής αποθηκεύτηκε με επιτυχία', 'success');
                    parent.hideLoader = true;
                })
                .catch(r => {
                    parent.hideLoader = true;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 200.133', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },


        fetchDataFromDatabase (){
            this.$http.get('/aj/ofa/primary/fetchDataForSportEducation')
                .then(r => {
                    this.$set('dataSet', r.data);
                    this.loading = false;
                })
                .catch(r => {
                    this.loading = false;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 200.134', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        fetchAllStudentsByGender (){
            this.loadingForExistence = true;
            this.studentsList = [];
            if(this.current_sex != 3){                                                  //case current sex is mixed
                this.allStudentsByGender = this.dataSet.students.filter(student => {
                    if (student.sex == this.current_sex) return student;
                });
            }else{
                this.allStudentsByGender = this.dataSet.students;
            }

            this.checkIfSportEducationAlreadyExists();
        },

        fetchAllStudentsForIndividualSport(){
            this.loadingForExistenceIndividual = true;
            this.studentsListIndividual = [];
            this.studentsList = [];
            this.allStudentsIndividual = this.dataSet.students;

            console.log('run stivos game');

            this.checkIfListAlreadyExistsIndividualPrimary();

        },

        checkIfListAlreadyExistsIndividualPrimary(){
            this.existedListId = 0;

            this.$http.post('/aj/ofa/checkIfListAlreadyExistsForIndividualPrimary', {
                sport : this.current_sport,
                year_id : this.dataSet.year.id
            })
                .then(r => {
                    console.log(r.data);
                    this.individualSports = r.data["special_sports"];

                    if(r.data["response"] == 'canMakeSchoolPrimaryIndividual'){
                        if(this.synodos == ''){
                            this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον συνοδό και τον γυμναστή του Σχολείου', 'warning');
                        }
                        this.notExistsPdfFileIndividual = true;

                    }else if(r.data["response"] == 'isLockedPrimaryIndividual'){
                        console.log('synodos loaded');
                        parent.synodos = r.data["synodos"];
                        parent.current_sex = 3;
                        parent.notExistsPdfFileIndividual = false;
                    }else{
                        if(r.data["response"] == 'temporaryPrimaryExistsIndividual'){
                            parent.synodos = r.data["synodos"];
                            parent.teacher_name = r.data["teacher_name"];
                            let existedStudents = r.data["list"];
                            parent.notExistsPdfFileIndividual = true;
                            parent.existedListId = r.data["list_id"];
                            let list_details = r.data['studentListIndividual'];
                            var i;
                            for (i = parent.allStudentsIndividual.length - 1; i >= 0; i -= 1) {
                                if (existedStudents.includes(parent.allStudentsIndividual[i]["am"])) {
                                    parent.studentsListIndividual.push({
                                        am : parent.allStudentsIndividual[i]["am"],
                                        birth : parent.allStudentsIndividual[i]["birth"],
                                        class : parent.allStudentsIndividual[i]["class"],
                                        first_name : parent.allStudentsIndividual[i]["first_name"],
                                        last_name : parent.allStudentsIndividual[i]["last_name"],
                                        loader : 0,
                                        middle_name : parent.allStudentsIndividual[i]["middle_name"],
                                        mothers_name : parent.allStudentsIndividual[i]["mothers_name"],
                                        school_id : parent.allStudentsIndividual[i]["school_id"],
                                        sex : parent.allStudentsIndividual[i]["sex"],
                                        year_birth : parent.allStudentsIndividual[i]["year_birth"],
                                        sport_id_special : list_details.find(function(obj){
                                            return obj.student_id === parent.allStudentsIndividual[i]["am"]
                                        })["sport_id_special"],
                                        sport_name_special : list_details.find(function(obj){
                                            return obj.student_id === parent.allStudentsIndividual[i]["am"]
                                        })["special_name"]
                                    });
                                    parent.allStudentsIndividual.splice(i, 1);
                                }
                            }
                        }
                    }
                    this.loadingForExistenceIndividual = false;
                    this.loadingForExistence = false;
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.782221', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))

        },

        checkIfSportEducationAlreadyExists(){
            this.existedListId = 0;

            this.$http.post('/aj/ofa/primary/checkIfSportEducationAlreadyExists', {
                sport : this.current_sport,
                year_id : this.dataSet.year.id,
                gender : this.current_sex
            })
                .then(r => {
                    console.log(r.data);
                    if(r.data["response"] == 'canMakeSchoolSportEducation'){
                        if(this.teacher_name == ''){
                            this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον υπεύθυνο Φυσικής Αγωγής και τον Συνοδό του αγωνίσματος.', 'warning');
                        }
                        this.notExistsPdfFile = true;
                    }else if(r.data["response"] == 'isLocked'){
                        console.log('teacher_name loaded');
                        this.teacher_name = r.data["teacher_name"];
                        this.synodos = r.data["synodos"];
                        this.notExistsPdfFile = false;
                    }else{
                        if(r.data["response"] == 'temporarySportEducationExists'){
                            this.teacher_name = r.data["teacher_name"];
                            this.synodos = r.data["synodos"];
                            let existedStudents = r.data["list"];
                            this.notExistsPdfFile = true;
                            this.existedListId = r.data["list_id"];

                            var i;
                            for (i = parent.allStudentsByGender.length - 1; i >= 0; i -= 1) {
                                if (existedStudents.includes(parent.allStudentsByGender[i]["am"])) {
                                    parent.studentsList.push(parent.allStudentsByGender[i]);
                                    parent.allStudentsByGender.splice(i, 1);
                                }
                            }
                        }
                    }
                    this.loadingForExistence = false;
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 200.135', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },


        displayGender(sport){
            this.current_sex = null;
            if(sport.individual == 1){
                this.showIndividualForm = true;
                this.fetchAllStudentsForIndividualSport();
            }else{
                this.showIndividualForm = false;
            }
        },

        addStudentToList (selected){
            this.studentsList.push(selected);
            this.studentsList.sort(this.sortStudents);
            this.allStudentsByGender.$remove(selected);
            this.selectedStudent = null;
            console.log('add student & remove ');
        },

        addNewStudentToList(student){
            parent.studentsList.push(student);
            parent.studentsList.sort(this.sortStudents);
        },

        deleteStudent(student){
            this.studentsList.$remove(student);
            this.allStudentsByGender.push(student);
            this.allStudentsByGender.sort(this.sortStudents);
        },

        showModalNewStudent(){
            this.showModalStudent = true;
            console.log('must open...');
        },

        displayAlert(m_header, message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.alert.header = m_header;
            this.$broadcast(`displayMsg`, this.alert)
        },

        sortStudents (a,b){
            if (a.last_name < b.last_name){
                return 1;
            }
            if (a.last_name > b.last_name){
                return -1;
            }
        },

        sentRequest (){

            parent.hideLoader = false;

            parent.$http.post('/aj/ofa/primary/insertNewList',{
                current_sport : parent.current_sport,
                current_sex : parent.current_sex,
                studentsList : parent.studentsList,
                year : parent.dataSet.year,
                teacher_name : parent.teacher_name,
                synodos : parent.synodos,
                existedListId : parent.existedListId
            })
                .then(r => {
                    location.href = r.data;
                    //console.log(r.data);
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 200.136', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },

        openModal(){
            this.showModalAgreement = true;
        },

        sortSports(a,b){
            if (a.name < b.name){
                return -1;
            }
            if (a.name > b.name){
                return 1;
            }
        },

        sortStudentsBySpecialSport(a, b){
            if (a.sport_name_special < b.sport_name_special){
                return -1;
            }
            if (a.sport_name_special > b.sport_name_special){
                return 1;
            }
        },


        sortStudentsBySpecialName(a, b) {
            if (a.sport_id_special < b.sport_id_special) {
                return -1;
            }
            if (a.sport_id_special > b.sport_id_special) {
                return 1;
            }
        }


    }

});
