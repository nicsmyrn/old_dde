var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


import Alert from '../components/Alert.vue';
import {dataAlert} from '../helpers/AlertHelper.js';
import ModalStudent from './components/ModalNewStudent.vue';
import ModalAgreement from './components/ModalOfaAgreement.vue';


Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

var ofaSchoolList = new Vue ({

    el : '#app',

    data : {
        teacher_name : '',
        synodos : '',
        nullValue : null,
        sex : [
            'Κορίτσια', 'Αγόρια'
        ],
        alert: dataAlert,
        dataSet : {
            sports : null,
            students : null,
            year : {
                name : null,
                id : null
            }
        },
        current_sport : null,
        current_sex : null,
        sport_name : null,

        selectedStudent : null,
        allStudentsByGender : [],
        studentsList : [],
        loading : true,

        showModalStudent : false,
        showModalAgreement : false,
        hideLoader : true,

        loadingForExistence : true,
        notExistsPdfFile: true,
        existedListId : 0,

        showIndividualForm : false,
        showListForm : true,
        notExistsPdfFileIndividual : true,
        allStudentsIndividual : [],
        selectedStudentIndividual : null,
        studentsListIndividual : [],
        selectedIndividualSport : null,
        individualSports : [],
        loadingForExistenceIndividual : true
    },

    computed :{
        showListForm (){
            return !!((this.current_sport != null) && (this.current_sex != null) && (this.dataSet.year.id != null));
        }
    },

    components : {
        Alert, ModalStudent, ModalAgreement
    },

    created (){
        if(window.location.pathname == '/%CE%91%CE%98%CE%9B%CE%97%CE%A4%CE%99%CE%9A%CE%9F%CE%99-%CE%91%CE%93%CE%A9%CE%9D%CE%95%CE%A3/%CE%9B%CE%AF%CF%83%CF%84%CE%B1-%CE%9F%CE%BC%CE%B1%CE%B4%CE%B9%CE%BA%CF%8E%CE%BD-%CE%91%CE%B8%CE%BB%CE%B7%CE%BC%CE%AC%CF%84%CF%89%CE%BD'){
            this.fetchDataFromDatabase();
        }else if(window.location.pathname == '/ΑΘΛΗΤΙΚΟΙ-ΑΓΩΝΕΣ/Λίστα-Ομαδικών-Αθλημάτων'){
            this.fetchDataFromDatabase();
        }
        else{
            this.fetchDataFromDatabaseIndividual();
            console.log('path2');
        }
    },

    methods: {
        downloadList(){
            this.displayAlert('Παρακαλώ περιμένετε...', 'Το αρχείο είναι σε διαδικασία κατεβάσματος στον υπολογιστή σας', 'danger');
        },

        temporarySave (){
            ofaSchoolList.hideLoader = false;

            ofaSchoolList.$http.post('/aj/ofa/temporarySaveNewList',{
                current_sport : ofaSchoolList.current_sport,
                current_sex : ofaSchoolList.current_sex,
                studentsList : ofaSchoolList.studentsList,
                year : ofaSchoolList.dataSet.year,
                teacher_name : ofaSchoolList.teacher_name,
                synodos : ofaSchoolList.synodos,
                existedListId : ofaSchoolList.existedListId
            })
                .then(r => {
                    this.existedListId = r.data;
                    this.displayAlert('Συγχαρητήρια!', 'Η Λίστα αποθηκεύτηκε με επιτυχία', 'success');
                    ofaSchoolList.hideLoader = true;
                })
                .catch(r => {
                    ofaSchoolList.hideLoader = true;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.133', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        temporarySaveIndividual(){
            ofaSchoolList.hideLoader = false;

            ofaSchoolList.$http.post('/aj/ofa/temporarySaveNewListIndividual',{
                current_sport : ofaSchoolList.current_sport,
                current_sex : 3,                       //individual sex not exists
                studentsListIndividual : ofaSchoolList.studentsListIndividual,
                year : ofaSchoolList.dataSet.year,
                synodos : ofaSchoolList.synodos,
                teacher_name : ofaSchoolList.teacher_name,
                existedListId : ofaSchoolList.existedListId
            })
                .then(r => {
                    this.existedListId = r.data;
                    this.displayAlert('Συγχαρητήρια!', 'Η Λίστα για ατομικά αθλήματα αποθηκεύτηκε με επιτυχία', 'success');
                    ofaSchoolList.hideLoader = true;
                })
                .catch(r => {
                    ofaSchoolList.hideLoader = true;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.245', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        fetchDataFromDatabase (){
            this.$http.get('/aj/ofa/fetchDataForLists')
                .then(r => {
                    this.$set('dataSet', r.data);
                    this.loading = false;
                })
                .catch(r => {
                    this.loading = false;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.134', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },

        fetchDataFromDatabaseIndividual (){
            this.$http.get('/aj/ofa/fetchDataForListsIndividual')
                .then(r => {
                    this.$set('dataSet', r.data);
                    this.loading = false;
                })
                .catch(r => {
                    this.loading = false;
                    this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.303', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true)
                })
        },


        fetchAllStudentsByGender (){
            this.loadingForExistence = true;
            this.studentsList = [];
            this.allStudentsByGender = this.dataSet.students.filter(student => {
                if (student.sex == this.current_sex) return student;
            });

            this.checkIfListAlreadyExists();
        },

        fetchAllStudentsForIndividualSport(){
            this.loadingForExistenceIndividual = true;
            this.studentsListIndividual = [];
            this.studentsList = [];
            this.allStudentsIndividual = this.dataSet.students;

            this.checkIfListAlreadyExistsIndividual();
            //this.notExistsPdfFileIndividual = true;
            //this.loadingForExistenceIndividual = false;
            //temporary


        },

        checkIfListAlreadyExists(){
            this.existedListId = 0;

            this.$http.post('/aj/ofa/checkIfListAlreadyExists', {
                sport : this.current_sport,
                year_id : this.dataSet.year.id,
                gender : this.current_sex
            })
                .then(r => {
                    console.log(r.data);
                    if(r.data["response"] == 'canMakeSchoolList'){
                        if(this.teacher_name == ''){
                            this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον υπεύθυνο Φυσικής Αγωγής του Σχολείου σας', 'warning');
                        }
                        this.notExistsPdfFile = true;
                    }else if(r.data["response"] == 'isLocked'){
                        console.log('teacher_name loaded');
                        this.teacher_name = r.data["teacher_name"];
                        this.notExistsPdfFile = false;
                    }else{
                        if(r.data["response"] == 'temporaryListExists'){
                            this.teacher_name = r.data["teacher_name"];
                            let existedStudents = r.data["list"];
                            this.notExistsPdfFile = true;
                            this.existedListId = r.data["list_id"];

                            var i;
                            for (i = ofaSchoolList.allStudentsByGender.length - 1; i >= 0; i -= 1) {
                                if (existedStudents.includes(ofaSchoolList.allStudentsByGender[i]["am"])) {
                                    ofaSchoolList.studentsList.push(ofaSchoolList.allStudentsByGender[i]);
                                    ofaSchoolList.allStudentsByGender.splice(i, 1);
                                }
                            }
                        }
                    }
                    this.loadingForExistence = false;
                    this.loadingForExistenceIndividual = false;
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.135', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },

        checkIfListAlreadyExistsIndividual(){
            this.existedListId = 0;

            this.$http.post('/aj/ofa/checkIfListAlreadyExistsForIndividual', {
                sport : this.current_sport,
                year_id : this.dataSet.year.id
            })
                .then(r => {
                    console.log(r.data);
                    this.individualSports = r.data["special_sports"].sort(this.sortSports);

                    //this.individualSports.sort(this.sortSports());

                    if(r.data["response"] == 'canMakeSchoolListIndividual'){
                        if(this.synodos == ''){
                            this.displayAlert('Σημείωση:', 'συμπληρώστε παρακαλώ τον συνοδό του Σχολείου σας', 'warning');
                        }
                        this.notExistsPdfFileIndividual = true;

                    }else if(r.data["response"] == 'isLockedIndividual'){
                        console.log('synodos loaded');
                        ofaSchoolList.synodos = r.data["synodos"];
                        ofaSchoolList.current_sex = 3;
                        ofaSchoolList.notExistsPdfFileIndividual = false;
                    }else{
                        if(r.data["response"] == 'temporaryListExistsIndividual'){
                            ofaSchoolList.synodos = r.data["synodos"];
                            let existedStudents = r.data["list"];
                            ofaSchoolList.notExistsPdfFileIndividual = true;
                            ofaSchoolList.existedListId = r.data["list_id"];
                            let list_details = r.data['studentListIndividual'];
                            var i;
                            for (i = ofaSchoolList.allStudentsIndividual.length - 1; i >= 0; i -= 1) {
                                if (existedStudents.includes(ofaSchoolList.allStudentsIndividual[i]["am"])) {
                                    ofaSchoolList.studentsListIndividual.push({
                                        am : ofaSchoolList.allStudentsIndividual[i]["am"],
                                        birth : ofaSchoolList.allStudentsIndividual[i]["birth"],
                                        class : ofaSchoolList.allStudentsIndividual[i]["class"],
                                        first_name : ofaSchoolList.allStudentsIndividual[i]["first_name"],
                                        last_name : ofaSchoolList.allStudentsIndividual[i]["last_name"],
                                        loader : 0,
                                        middle_name : ofaSchoolList.allStudentsIndividual[i]["middle_name"],
                                        mothers_name : ofaSchoolList.allStudentsIndividual[i]["mothers_name"],
                                        school_id : ofaSchoolList.allStudentsIndividual[i]["school_id"],
                                        sex : ofaSchoolList.allStudentsIndividual[i]["sex"],
                                        year_birth : ofaSchoolList.allStudentsIndividual[i]["year_birth"],
                                        sport_id_special : list_details.find(function(obj){
                                            return obj.student_id === ofaSchoolList.allStudentsIndividual[i]["am"]
                                        })["sport_id_special"],
                                        sport_name_special : list_details.find(function(obj){
                                            return obj.student_id === ofaSchoolList.allStudentsIndividual[i]["am"]
                                        })["special_name"]
                                    });
                                    ofaSchoolList.allStudentsIndividual.splice(i, 1);
                                }
                            }

                            ofaSchoolList.studentsListIndividual.sort(ofaSchoolList.sortStudentsBySpecialName);
                        }
                    }
                    this.loadingForExistenceIndividual = false;
                    this.loadingForExistence = false;
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.782', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))

        },

        displayGender(sport){
            this.current_sex = null;

            if(sport.individual == 1){
                this.showIndividualForm = true;
                this.fetchAllStudentsForIndividualSport();
            }else{
                this.showIndividualForm = false;
            }
        },

        addStudentToList (selected){
            this.studentsList.push(selected);
            this.studentsList.sort(this.sortStudents);
            this.allStudentsByGender.$remove(selected);
            this.selectedStudent = null;
            console.log('add student & remove ');
        },

        addStudentToIndividualList(student, sport_special){
            console.log(sport_special);

            ofaSchoolList.studentsListIndividual.push({
                am : student.am,
                birth : student.birth,
                class : student.class,
                first_name : student.first_name,
                last_name : student.last_name,
                loader : 0,
                middle_name : student.middle_name,
                mothers_name : student.mothers_name,
                school_id : student.school_id,
                sex : student.sex,
                year_birth : student.year_birth,
                sport_id_special : sport_special.id,
                sport_name_special : sport_special.name
            });

            this.studentsListIndividual.sort(this.sortStudentsBySpecialSport);
            this.allStudentsIndividual.$remove(student);
            this.selectedStudentIndividual = null;
            console.log('add student & remove Individual ');
        },

        addNewStudentToList(student){
            ofaSchoolList.studentsList.push(student);
            ofaSchoolList.studentsList.sort(this.sortStudents);
        },

        addNewStudentToAllStudents(student){
            ofaSchoolList.allStudentsIndividual.push(student);
            ofaSchoolList.allStudentsIndividual.sort(this.sortStudents);
        },

        deleteStudent(student){
            this.studentsList.$remove(student);
            this.allStudentsByGender.push(student);
            this.allStudentsByGender.sort(this.sortStudents);
        },

        deleteStudentIndividual(student){
            this.studentsListIndividual.$remove(student);
            this.allStudentsIndividual.push(student);
            this.allStudentsIndividual.sort(this.sortStudents);
        },

        showModalNewStudent(){
            this.showModalStudent = true;
            console.log('must open...');
        },

        showModalNewStudentIndividual(){
            this.showModalStudent = true;
            this.current_sex = 3;
            console.log('must open... for Individual Sport');
        },

        displayAlert(m_header, message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.alert.header = m_header;
            this.$broadcast(`displayMsg`, this.alert)
        },

        sortStudents (a,b){
            if (a.last_name < b.last_name){
                return 1;
            }
            if (a.last_name > b.last_name){
                return -1;
            }
        },

        sortSports(a,b){
            if (a.name < b.name){
                return -1;
            }
            if (a.name > b.name){
                return 1;
            }
        },

        sortStudentsBySpecialSport(a, b){
            if (a.sport_name_special < b.sport_name_special){
                return -1;
            }
            if (a.sport_name_special > b.sport_name_special){
                return 1;
            }
        },


        sortStudentsBySpecialName(a, b){
            if (a.sport_id_special < b.sport_id_special){
                return -1;
            }
            if (a.sport_id_special > b.sport_id_special){
                return 1;
            }
        },

        sentRequest (){

            ofaSchoolList.hideLoader = false;

            ofaSchoolList.$http.post('/aj/ofa/insertNewList',{
                current_sport : ofaSchoolList.current_sport,
                current_sex : ofaSchoolList.current_sex,
                studentsList : ofaSchoolList.studentsList,
                year : ofaSchoolList.dataSet.year,
                teacher_name : ofaSchoolList.teacher_name,
                synodos : ofaSchoolList.synodos,
                existedListId : ofaSchoolList.existedListId
            })
                .then(r => {
                    location.href = r.data;
                    //console.log(r.data);
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.136', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },

        sentRequestIndividual (){

            ofaSchoolList.hideLoader = false;

            ofaSchoolList.$http.post('/aj/ofa/insertNewListIndividual',{
                current_sport : ofaSchoolList.current_sport,
                current_sex : 3,
                studentsList : ofaSchoolList.studentsListIndividual,
                year : ofaSchoolList.dataSet.year,
                teacher_name : ofaSchoolList.teacher_name,
                synodos      : ofaSchoolList.synodos,
                existedListId : ofaSchoolList.existedListId
            })
                .then(r => {
                    location.href = r.data;
                    //console.log(r.data);
                })
                .catch(r => this.displayAlert('Προσοχή ΣΦΑΛΜΑ - 150.999', 'Παρακαλώ επικοινωνήστε με τον διαχειριστή του συστήματος (Σμυρναίο Νικόλαο) και αναφέρετε τον κωδικό Σφάλματος', 'danger',true))
        },

        openModal(){
            this.showModalAgreement = true;
        }
    }
});

