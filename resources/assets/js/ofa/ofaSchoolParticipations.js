
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


import Alert from '../components/Alert.vue';
import {dataAlert} from '../helpers/AlertHelper.js';
import ModalAgreement from './components/ModalOfaAgreement.vue';


Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

var parent = new Vue ({
    el : '#app',

    data : {
        synodos : '',

        nullValue : null,
        sex : [
            'Κορίτσια', 'Αγόρια'
        ],
        alert: dataAlert,
        dataSet : {
            sports : null,
            students : null,
            years : null
        },

        current_phase : null,
        current_sport : null,
        current_sex : null,
        current_year : null,

        current_list_id : null,

        phases : [
            '1ος αγώνας',
            '2ος αγώνας',
            '3ος αγώνας'
        ],

        listExists : false,


        allStudentsByGender : [],
        studentsInList : [],
        studentsInParticipationStatus : [],
        checkedStudents : [],
        p_status_id : null,

        loading : true,

        showModalAgreement : false,
        hideLoader : true,
        addRemoveLoader : false,

        canChangeParticipationStatus: true,
        loadingPhaseDiv : true,
        loadingParticipationStatusTable : true

    },

    computed :{
        showParticipationStatusDiv (){
            if(this.current_sport != null && this.current_sex != null && this.current_year != null && this.current_phase != null){
                return true;
            }
            return false;
        },

        showPhase(){
            if(this.current_sport != null && this.current_sex != null && this.current_year != null){
                return true;
            }
            return false;
        }
    },

    created (){
        this.fetchDataFromDatabase();
    },

    ready(){
        this.hideLoader = true;
    },

    components : {
        Alert, ModalAgreement
    },

    methods : {

        openPdfStream (){
            console.log('open method openPdfStream');
            //var newWindow = window.open(); //Open URL in new tab
            //
            //
            //this.$http.post('/aj/ofa/ParticipationStatus/pdfStream', {
            //    checkedStudents : this.checkedStudents,
            //    phase : this.current_phase,
            //    synodos : this.synodos
            //})
            //    .then(r => {
            //        newWindow.html(r.data);
            //    })
            //    .catch(r => {
            //        console.log('Error 999');
            //    });
        },

        toggleStudentInStatus(am, checked, index){
            this.studentsInList[index].loader = true;
            console.log(document.querySelector('#token').attributes['content'].nodeValue);
            if(checked){
                this.$http.post('/aj/ofa/RemoveFromStatus', {
                    am : am,
                    p_status_id : this.p_status_id,
                    list_id : this.current_list_id,
                    phase : this.current_phase
                })
                    .then(r => {
                        this.checkedStudents.$remove(am);
                        this.displayAlert('Προσοχή','Ο μαθητής ' + this.studentsInList[index].last_name + " " + this.studentsInList[index].first_name  +  '  με Α.Μ. :' + am + ' αφαιρέθηκε από την κατάσταση συμμετοχής!', 'danger');
                        this.studentsInList[index].loader = false;
                    })
                    .catch(r => {
                        this.studentsInList[index].loader = false;
                        this.displayAlert('Προσοχή!','Ο μαθητής '+ this.studentsInList[index].last_name + " " + this.studentsInList[index].first_name +' με Α.Μ. :' + am + ' δεν ενημερώθηκε λόγω τεχνικού προβλήματος, ξαναπροσπαθήστε!', 'danger', true)
                    });
            }else{
                this.$http.post('/aj/ofa/AddToStudents', {
                    am : am,
                    p_status_id : this.p_status_id,
                    list_id : this.current_list_id,
                    phase : this.current_phase
                })
                    .then(r => {
                        console.log(r.data);
                        this.checkedStudents.push(am);
                        this.p_status_id = r.data;
                        this.displayAlert('Συγχαρητήρια','Ο μαθητής' + this.studentsInList[index].last_name + " " + this.studentsInList[index].first_name  + ' με Α.Μ. :' + am + ' προστέθηκε με επιτυχία!', 'success');
                        this.studentsInList[index].loader = false;
                    })
                    .catch(r => {
                        this.studentsInList[index].loader = false;
                        this.displayAlert('Προσοχή!','Ο μαθητής '+ this.studentsInList[index].last_name + " " + this.studentsInList[index].first_name +' με Α.Μ. :' + am + ' δεν ενημερώθηκε λόγω τεχνικού προβλήματος, ξαναπροσπαθήστε!', 'danger', true)
                    });
            }
        },

        fetchStudentsFromList (){
            this.loadingParticipationStatusTable = true;
            this.canChangeParticipationStatus = false;

            this.$http.post('/aj/ofa/fetchStudentsFromParticipationStatus', {
                list_id : this.current_list_id,
                phase : this.current_phase
            })
                .then(r => {
                    console.log('fetchStudentsFromParticipationStatus');

                    console.log(r.data);

                    this.checkedStudents = r.data['p_status_students_ids'];
                    this.p_status_id = r.data['p_status_id'];

                    this.loadingParticipationStatusTable = false;

                    this.canChangeParticipationStatus = true;
                })
                .catch(r => console.log('error 24s25'))
        },

        fetchDataFromDatabase (){
            this.$http.get('/aj/ofa/fetchDataForParticipationStatus')
                .then(r => {
                    this.$set('dataSet', r.data);
                    this.loading = false;
                })
                .catch(r => console.log('error'))
        },

        fetchAllStudentsByGender (){
            if(this.showPhase) {
                console.log('fetchAllStudentsByGender');
                this.loadingForExistence = true;
                this.studentsList = [];
                this.allStudentsByGender = this.dataSet.students.filter(student => {
                    if (student.sex == this.current_sex) return student;
                });

                this.checkIfListAlreadyExists();
            }
        },

        checkIfListAlreadyExists(){
            this.loadingPhaseDiv = true;

            this.$http.post('/aj/ofa/checkIfListAlreadyExistsForParticipationStatus', {
                sport : this.current_sport,
                year_id : this.current_year,
                gender : this.current_sex
            })
                .then(r => {
                    console.log(r.data);
                    this.loadingPhaseDiv = false;


                    if(r.data['message'] === 'listExistsAndCanMakeParticipationStatus'){
                        //this.allStudentsByGender.forEach(student => {
                        //    let temp;
                        //    if(r.data['list'].includes(student.am)){
                        //        temp = Object.assign(student, {loader : "some value"});
                        //        this.studentsInList.push(temp);
                        //    }
                        //});
                        this.studentsInList = this.allStudentsByGender.filter(student => {
                            if (r.data['list'].includes(student.am)){
                                return student;
                            }
                        });


                        this.current_list_id = r.data['list_id'];
                        this.listExists = true;
                    }else{
                        this.listExists = false;
                    }
                    this.loadingForExistence = false;
                })
                .catch(r => console.log('error 242425'))
        },

        displayGender(sport){
            this.current_sex = null;
            this.fetchAllStudentsByGender();
        },

        addStudentToList (selected){
            this.studentsList.push(selected);
            this.studentsList.sort(this.sortStudents);
            this.allStudentsByGender.$remove(selected);
            this.selectedStudent = null;
            console.log('add student & remove ');
        },

        deleteStudent(student){
            this.studentsList.$remove(student);
            this.allStudentsByGender.push(student);
            this.allStudentsByGender.sort(this.sortStudents);
        },

        showModalNewStudent(){
            this.showModalStudent = true;
            console.log('must open...');
        },

        displayAlert(m_header, message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.alert.header = m_header;
            this.$broadcast(`displayMsg`, this.alert)
        },

        sortStudents (a,b){
            if (a.last_name < b.last_name){
                return 1;
            }
            if (a.last_name > b.last_name){
                return -1;
            }
        },

        sentRequest (){

            parent.hideLoader = false;

            parent.$http.post('/aj/ofa/insertNewList',{
                current_sport : parent.current_sport,
                current_sex : parent.current_sex,
                studentsList : parent.studentsList,
                year : parent.dataSet.year,
                synodos : parent.synodos
            })
                //.then(r => location.href = r.data)
                .then(r => console.log(r.data))
                .catch(r => console.log('Error'));
        },

        openModal(){
            this.showModalAgreement = true;
        },

        remove(array, element) {
            const index = array.indexOf(element);

            if (index !== -1) {
                array.splice(index, 1);
            }
        },

        add(array, element){

        }
    }

});
