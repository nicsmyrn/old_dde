
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

import Modal from './components/ModalOrganikes.vue';
import Alert from './components/Alert.vue';
import ModalDelete from './components/ModalDelete.vue';

var parent = new Vue ({
    el : '#app',

    data : {
        maxSelections : 20,
        requestInDatabase : false,
        loader : false,
        allSchools : null,
        schoolsChoices : null,
        selectedSchools : [],
        hideLoader : true,
        showModal : false,
        showDeleteModal : false,
        hasMadeRequest : false,
        alert : {
            message : 'Ειδοποίηση',
            type : 'info',
            important : false
        }
    },

    ready () {
        this.fetchSchools();
    },

    methods : {

        permanentlyDelete(){
            console.log('permanentlyDelete');
            parent.$http.post('/aj/organikes/permanentlyDelete',{
                type : 'Βελτιώση - Οριστική τοποθέτηση'
            })
                .then(r => location.href = r.data)
                //.then(r => console.log(r.data))
                .catch(r => console.log('Error'));
        },

        saveRequest(){
            parent.$http.post('/aj/organikes/saveRequestVeltiwsi',{
                selectedSchools : this.selectedSchools
            })
                .then(r => {
                    this.displayAlert(r.data, 'success');
                    this.requestInDatabase = true;
                })
                .catch(r => console.log('Error'));
        },
        sentRequest (){
            parent.$http.post('/aj/organikes/sentRequestForProtocolVeltiwsi',{
                selectedSchools : parent.selectedSchools
            })
                .then(r => location.href = '/')
                //.then(r => console.log(r.data))
                .catch(r => console.log('Error'));
        },

        openModal (){
            this.showModal = true;
        },

        openDeleteModal (){
            this.showDeleteModal = true;
        },

        fetchSchools (){
            this.loader = true;
            this.$http.get('/aj/organikes/fetchSelectedSchoolsVeltiwsi')
                .then(r => {
                    if(r.data == 'is_made'){
                        this.hasMadeRequest = true;
                    }else{
                        this.allSchools = r.data.allSchools;
                        this.selectedSchools = r.data.selectedSchools;
                        if(this.selectedSchools.length > 0){
                            this.requestInDatabase = true;
                        }
                    }
                    setTimeout(function(){
                        this.loader = false;
                    }.bind(this),1000);
                })
                .catch(r => console.log('error'))
        },

        fetchEidikotitesForSchool (){
            this.$http.post('/aj/organika/eidikotitesWithKenaPleonasmata', {
                schoolId : this.currentSchool
            })
                .then(r => this.eidikotites = r.data)
                .catch(r => console.log('error'))
        },

        deleteAllSelected (){
            this.selectedSchools.forEach(r => this.putBackSchool(r));
            this.selectedSchools = [];
        },

        saveEidikotita (id){
            let eidikotita = this.eidikotites.filter(obj => {
                if(obj.id == id) return obj;
            });

            let number = Number.parseInt(eidikotita[0].number);

            if(Number.isInteger(number)){
                this.$http.post('/aj/organika/saveEidikotaToSchool', {
                    schoolId : this.currentSchool,
                    eidikotitaId : eidikotita[0].id,
                    number : number
                })
                    .then(r => {
                        if(r.data != ''){
                            this.displayAlert(r.data, 'success')
                        }
                    })
                    .catch(r => this.displayAlert('ERROR: Δεν αποθηκεύτηκε', 'danger'))
            }else{
                console.log('Error Message. Number is not Integer');
            }
        },

        removeSchool(id){
            return this.allSchools.filter(obj => {
                if(obj.id == id) this.allSchools.$remove(obj);
            });
        },

        addSchoolToChoices (obj){
            this.selectedSchools.push(obj);
            this.allSchools.$remove(obj);
        },

        swapUp(index){
            this.swap(index-1, index)
        },

        swapDown(index){
            this.swap(index, index +1);
        },

        swap(a, b){
            let temp = this.selectedSchools[a];
            this.selectedSchools.splice(a, 1, this.selectedSchools[b]);
            this.selectedSchools.$set(b, temp);
        },

        deleteSchool(obj){
            this.selectedSchools.$remove(obj);
        },

        putBackSchool(obj){
            this.allSchools.push(obj);
            this.allSchools.sort(this.sortItems);
        },

        deleteSelectedSchool(obj){
            this.deleteSchool(obj);
            this.putBackSchool(obj);
        },

        displayAlert(message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.$broadcast(`displayMsg`, this.alert)
        },

        sortItems(a,b){
            if (a.order < b.order){
                return -1;
            }
            if (a.order > b.order){
                return 1;
            }
        }
    },

    components : {
        Alert, Modal, ModalDelete
    }
});
