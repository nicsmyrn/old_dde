var io = require('socket.io-client');
var notificationVue = require('vue');
var VueResource = require('vue-resource');

notificationVue.use(VueResource);

notificationVue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


notificationVue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

var socket = io();  //127.0.0.1:3000

import TimeCounter from '../components/TimeCounter.vue';

new notificationVue ({
    el : '#menubar',

    data : {
        notifications : [],
        user_id : 0
    },

    created () {
        this.fetchNotifications();
        this.getUserId();

    },

    ready () {

    },

    methods : {
        fetchNotifications (){
            this.$http.get('/aj/notifications/fetchNotifications')
                .then(r => this.$set('notifications', r.data))
                .catch(r => console.log('error'))
        },
        getUserId(){
            this.$http.get('/aj/notifications/getUserId')
                .then(r => {this.user_id = r.data;
                    socket.on('user-'+this.user_id+':App\\Events\\PysdeSendNotificationToTeacher', function (data) {
                        this.notifications.push(data);
                        this.notifications.sort(this.sortNotifications);

                        $('#notify_counter').removeClass('animated shake').addClass('animated shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                            $(this).removeClass('animated shake');
                        })
                    }.bind(this));

                    socket.on('user-'+this.user_id+':App\\Events\\MisthodosiaSendNotificationToTeacher', function (data) {
                        this.notifications.push(data);
                        this.notifications.sort(this.sortNotifications);

                        $('#notify_counter').removeClass('animated shake').addClass('animated shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                            $(this).removeClass('animated shake');
                        })
                    }.bind(this));
                })
                .catch(r => console.log('error'))
        },
        sortNotifications (a,b){
            if (a.time < b.time){
                return 1;
            }
            if (a.time > b.time){
                return -1;
            }
        }
    },

    components : {
        TimeCounter
    }
});
