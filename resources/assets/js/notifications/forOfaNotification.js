var io = require('socket.io-client');
var notificationVue = require('vue');
var VueResource = require('vue-resource');

notificationVue.use(VueResource);

notificationVue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


notificationVue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

var socket = io();  //127.0.0.1:3000

import TimeCounter from '../components/TimeCounter.vue';

new notificationVue ({
    el : '#menubar',

    data : {
        notifications : []
    },

    created () {
        this.fetchNotifications();
    },

    ready () {

        console.log('notifications:');
        console.log(this.notifications);

        socket.on('ofa:App\\Events\\SchoolToOfaTeamNotification', function (data) {
            this.notifications.push(data);
            this.notifications.sort(this.sortNotifications);

            $('#notify_counter').removeClass('animated shake').addClass('animated shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                $(this).removeClass('animated shake');
            })
        }.bind(this));

    },

    methods : {
        fetchNotifications (){
            this.$http.get('/aj/notifications/fetchNotifications')
                .then(r => {
                    this.$set('notifications', r.data);
                    console.log('notification added...');
                })
                .catch(r => console.log('error'))
        },
        sortNotifications (a,b){
            if (a.time < b.time){
                return 1;
            }
            if (a.time > b.time){
                return -1;
            }
        }
    },

    components : {
        TimeCounter
    }
});
