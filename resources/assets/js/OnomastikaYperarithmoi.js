
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

import Alert from './components/Alert.vue';

var parent = new Vue ({
    el : '#app',

    data : {
        schools : null,
        teachers : null,
        currentSchool : 0,
        alert : {
            message : 'Ειδοποίηση',
            type : 'info',
            important : false
        }
    },

    ready () {
        this.fetchSchools();
    },

    methods : {
        fetchSchools (){
            this.$http.get('/aj/organika/schools')
                .then(r => this.schools = r.data)
                .catch(r => console.log('error'))
        },

        toggleYperarithmiaValue(teacher){
            this.$http.post('/aj/organika/toggleYperarithmiaValueOfTeacher', {
                afm : teacher.afm,
                onomastika_yperarithmos : ! (teacher.onomastika_yperarithmos)
            })
                .then(r => this.displayAlert('Η Αλλαγή αποθηκεύτηκε με επιτυχία...', 'success'))
                .catch(r => console.log('error'))
        },

        fetchTeachersForSchool (){
            console.log('fetchTeachersForSchool');
            this.$http.post('/aj/organika/teachersSchoolFromMySchool', {
                schoolId : this.currentSchool
            })
                .then(r => this.teachers = r.data)
                .catch(r => console.log('error'))
        },

        saveTeacher (id){
            let eidikotita = this.teachers.filter(obj => {
                if(obj.id == id) return obj;
            });

            let number = Number.parseInt(eidikotita[0].number);

            if(Number.isInteger(number)){
                this.$http.post('/aj/organika/saveEidikotaToSchool', {
                    schoolId : this.currentSchool,
                    eidikotitaId : eidikotita[0].id,
                    number : number
                })
                    .then(r => {
                        if(r.data != ''){
                            this.displayAlert(r.data, 'success')
                        }
                    })
                    .catch(r => this.displayAlert('ERROR: Δεν αποθηκεύτηκε', 'danger'))
            }else{
                console.log('Error Message. Number is not Integer');
            }
        },
        displayAlert(message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.$broadcast(`displayMsg`, this.alert)
        }
    },

    components : {
        Alert
    }
});
