import Alert from './Alert.vue';
import {dataAlert} from '../helpers/AlertHelper.js';

export default {
    template : '#period-manage-template',

    created () {
        this.fetchPeriodCollection();
    },

    props : ['token'],

    data () {
        return {
            alert : dataAlert,
            periodCollection : null,
            year : {
                months : []
            }
        };
    },

    http ()  {
        return {
            headers : {
                'X-CSRF-TOKEN' : this.token
            }
        }
    },

    methods : {
        fetchPeriodCollection(){
            this.$http.get(`/ajax/ktel/getPeriodCollection`)
                .then(r => this.periodCollection = r.data)
                .catch(r=> console.log('Error'));
        },
        changePermission(month, index){
            this.$http.post(`/ajax/ktel/changePermission`,{
                id : month.pivot.id
            })
                .then(r => {
                    this.year.months[index].pivot = r.data;
                    if (r.data.disabled){
                        this.displayAlert('Προσοχή!',`Ο μήνας ${month.name} του σχολικού έτους ${this.year.name} κλειδώθηκε με επιτυχία`, 'danger')
                    }else{
                        this.displayAlert('Συγχαρητήρια!',`Ο μήνας ${month.name} του σχολικού έτους ${this.year.name} ενεργοποιήθηκε με επιτυχία`, 'success')
                    }
                })
                .catch(r=> console.log('Error'));
        },
        displayAlert(header, message, type, important = false){
            this.alert.header = header;
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.$broadcast(`displayMsg`, this.alert)
        }
    },

    components : {
        Alert
    }

}