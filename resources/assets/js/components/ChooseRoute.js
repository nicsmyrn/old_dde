export default {
    template : '#choose-route-template',

    created () {
        this.fetchMunicipalityCollection();

        if (this.municipalityId != 0){
            this.existingMunicipality = this.municipalityId;
            this.fetchExistingRoutesCollection();
        }
    },

    props : ['municipalityId'],

    data () {
        return {
            municipalityCollection : [],
            busroutesCollection : [],
            existingMunicipality : null
        };
    },

    methods : {
        fetchMunicipalityCollection () {
            this.$http.get('/ajax/ktel/school/municipalities')
                .then(r => this.municipalityCollection = r.data)
                .catch(response => console.log('Error'));
        },
        fetchBusRoutesCollection(){
            if (this.existingMunicipality == this.municipalityId){
                this.fetchExistingRoutesCollection();
            }else{
                this.$http.get(`/ajax/ktel/school/busroutes/${this.municipalityId}`)
                    .then(r => this.busroutesCollection =  r.data)
                    .catch(response => console.log('Error'));
            }
        },
        fetchExistingRoutesCollection (){
            this.$http.get(`/ajax/ktel/existingroutes`)
                .then(r => this.busroutesCollection =  r.data)
                .catch(response => console.log('Error'));
        }
    }
}