import Alert from './Alert.vue';
import Modal from './ModalAttendance.vue';
import Comments from './Comments.vue';


export default {
    template : '#edit-periods',

    created () {
        this.fetchYearCollection();
    },


    data () {
        return {
            timeTimeout : 2000,
            hideLoader : true,
            url : '',
            showCommentsSystem : false,
            yearCollection : [],
            months : [],
            period : {
                id : null,
                pivot : {
                    disabled : 0
                }
            },
            comments : [],
            PeriodCollection : [],
            periodErrors : {
                kids_number : [],
                routes_number : [],
                description : [],
                starts_at : [],
                new : []
            },
            valueUpdatePeriodRoutes : null,
            alert : {
                message : 'Ειδοποίηση',
                type : 'info',
                important : false
            },
            showModal: false,
            currentId : 0
        };
    },

    computed : {
        displayAlertEmpty (){
            return !this.PeriodCollection.length && this.months.length && this.period.pivot.id ? true : false;   //pivot.id or id?
        }
    },

    methods : {

        hideLoaderTimeout (){
            setTimeout(function(){
                this.hideLoader = true;
            }.bind(this), this.timeTimeout);
        },
        hideLoaderTimeoutWithMessage (message, type){
            setTimeout(function(){
                this.hideLoader = true;
                this.displayAlert(message, type)
            }.bind(this), this.timeTimeout);
        },
        fetchYearCollection (){
            this.$http.get('/ajax/ktel/getYears')
                .then(r => this.yearCollection = r.data)
                .catch(r=> console.log('Error'));
        },

        createPeriod(){
            this.fetchEnabledSchoolRoutes();
        },

        fetchEnabledSchoolRoutes(){
            this.$http.get('/ajax/ktel/existingroutesEnabled')
                .then(r => {
                    console.log(r.data);
                    this.PeriodCollection = [];
                    this.valueUpdatePeriodRoutes = 'no';
                    for (let record of r.data){
                        record = Object.assign(record, {
                            period_id : this.period.pivot.id,
                            kids_number : 0,
                            routes_number : 0,
                            description : '',
                            disabled : false
                        });
                        this.PeriodCollection.push(record);
                        this.showCommentsSystem = true;
                    }
                })
                .catch(r=> console.log('Error'));
        },

        fetchPeriodRoutes(){
            this.$http.get(`/ajax/ktel/getPeriodRoutes/${this.period.pivot.id}`)
                .then(r => {
                    this.PeriodCollection = r.data['periodRoutes'];
                    this.comments = r.data['comments'];
                    this.url = r.data['url'];
                    if (this.url != '') this.showCommentsSystem = false;
                    else {
                        if(this.PeriodCollection.length) this.showCommentsSystem = true;
                        else this.showCommentsSystem = false;
                    }
                    this.valueUpdatePeriodRoutes = 'yes';
                })
                .catch(r=> console.log('Error'));
        },

        changePeriodRoutes(){
            if (this.valueUpdatePeriodRoutes == 'yes'){
                this.updatePeriodRoutes();
            }else if(this.valueUpdatePeriodRoutes == 'no'){
                this.insertPeriodRoutes();
            }
        },

        updatePeriodRoutes(){
            this.hideLoader = false;
            this.emptyPeriodErrors();

            this.$http.post('/ajax/ktel/updatePeriodRoutes',{
                periodCollection : this.PeriodCollection,
                periodId : this.period.pivot.id,
                comments : this.comments
            })
                .then(r => this.hideLoaderTimeoutWithMessage('Η ενημέρωση των δρομολογίων έγινε με επιτυχία','success'))
                .catch(r=> {
                    this.hideLoader = true;
                    this.createErrorMessages(r.data);
                });
        },

        insertPeriodRoutes(){
            this.hideLoader = false;
            this.emptyPeriodErrors();

            this.$http.post('/ajax/ktel/insertPeriodRoutes',{
                periodCollection : this.PeriodCollection,
                comments : this.comments
            })
                .then(r => {
                    this.PeriodCollection = r.data['periodCollection'];
                    this.comments = r.data['comments'];
                    this.hideLoaderTimeoutWithMessage('Η Εισαγωγή των δρομολογίων έγινε με επιτυχία','warning');
                    this.valueUpdatePeriodRoutes = 'yes';
                })
                .catch(r => {
                    this.hideLoader = true;
                    this.createErrorMessages(r.data);
                });
        },

        sendPeriodRoutes (){
            this.hideLoader = false;
            this.PeriodCollection.forEach(period => period.disabled = true);
            this.comments.forEach(comment => comment.disabled = true);
            this.$http.post('/ajax/ktel/updatePeriodRoutes',{
                periodCollection : this.PeriodCollection,
                comments : this.comments,
                periodId : this.period.pivot.id,
                createPDF: 'canCreatePDF',
                monthId : this.period.pivot.month_id,
                yearId : this.period.pivot.year_id
            })
                .then(r =>  {
                    console.log(r.data);
                    this.hideLoaderTimeoutWithMessage('Τα δρομολόγια αποθηκεύτηκαν και στάλθηκαν στα γραφεία της Διεύθυνσης','success');
                    this.showCommentsSystem = false;
                    this.url = r.data;
                })
                .catch(r=> console.log('Error'));
        },

        getPeriodRoutes (){
            this.emptyPeriodErrors();
            this.fetchPeriodRoutes();
        },

        displayAlert(message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.$broadcast(`displayMsg`, this.alert)
        },

        getCurrentId(object){
            this.showModal = true;
            this.currentId = object.id;
        },

        emptyPeriodErrors(){
            this.periodErrors.kids_number = [];
            this.periodErrors.routes_number = [];
            this.periodErrors.description = [];
            this.periodErrors.starts_at = [];
            this.periodErrors.new = [];
        },

        createErrorMessages(collection){
            const exp = /(\d+)/;
            Object.keys(collection).forEach(key => {
                let k = key.match(exp);
                let attribute = key.substr(key.lastIndexOf(".")+1);
                this.periodErrors[attribute].$set(k[0], collection[key][0]);
            });
        }
    },

    components : {
        Alert, Modal, Comments
    }
}