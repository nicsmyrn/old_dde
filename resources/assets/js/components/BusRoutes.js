import Alert from './Alert.vue';

export default {
    template : '#busroutes-template',

    created () {
        this.fetchMunicipalityCollection();
        this.fetchRouteTypeCollection();
    },

    props : ['token'],

    http ()  {
        return {
            headers : {
                'X-CSRF-TOKEN' : this.token
            }
        }
    },

    data () {
        return {
            municipalityCollection : [],
            busroutesCollection : [],
            municipalityId : null,
            newRoute : {
                name : '',
                type : '',
                typeName : '',
                aa_contract : ''
            },
            routetypeCollection : [],
            alert : {
                message : 'Ειδοποίηση',
                type : 'info',
                important : false
            }
        };
    },


    methods : {
        fetchRouteTypeCollection (){
            this.$http.get('/ajax/ktel/routetypes')
                .then(r => this.routetypeCollection = r.data)
                .catch(response => this.displayAlert(`ΣΦΑΛΜΑ: ${response.data.message}`, 'danger', true));
        },
        fetchMunicipalityCollection () {
            this.$http.get('/ajax/ktel/municipalities')
                .then(r => this.municipalityCollection = r.data)
                .catch(response => this.displayAlert(`ΣΦΑΛΜΑ: ${response.data.message}`, 'danger', true));
        },

        //TODO fetch Routes Collection
        fetchBusRoutesCollection(){
            console.log(this.municipalityId);
            this.$http.get(`/ajax/ktel/busroutes/${this.municipalityId}`)
                .then(r => {
                    r.data.forEach (data => {
                        data = Object.assign(data, {editable : false});
                    });
                    this.busroutesCollection =  r.data;
                })
                .catch(response => this.displayAlert(`ΣΦΑΛΜΑ: ${response.data.message}`, 'danger', true));
        },

        makeRouteEditable (route){
            route.editable = true;
        },

        saveRoute(route){
            route.editable = false;
            this.updateRouteRecord(route);
        },

        deleteRoute(route){
            this.busroutesCollection.$remove(route);
            console.log('Route Id: ' + route.id);
            this.$http['delete'](`/ajax/ktel/busroutes/${route.id}`)
                .then(r => this.displayAlert('Η διαδρομή ΔΙΕΓΡΑΦΗ', 'danger'))
                .catch(response => this.displayAlert(`ΣΦΑΛΜΑ: ${response.data.message}`, 'danger', true));
        },

        updateRouteRecord(route){
            console.log(route.id);
            console.log(route.name);
            console.log(route.routetype_id);

            this.$http.post(`/ajax/ktel/busroutes`,{
                id : route.id,
                name : route.name,
                routetype_id : route.routetype_id,
                aa_contract : route.aa_contract
            })
                .then(r =>  this.displayAlert('Η διαδρομή αποθηκεύτηκε με επιτυχία', 'success'))
                .catch(response => this.displayAlert(`ΣΦΑΛΜΑ: ${response.data.message}`, 'danger', true));
        },

        addRoute (){
            this.$http.post(`/ajax/ktel/addroute`,{
                municipality_id : this.municipalityId,
                name : this.newRoute.name,
                routetype_id : this.newRoute.type,
                aa_contract : this.newRoute.aa_contract
            })
                .then(r => {
                    this.newRoute.type = '';
                    this.newRoute.name = '';
                    this.newRoute.aa_contract = '';
                    r.data = Object.assign(r.data, {editable : false});
                    this.busroutesCollection.push(r.data);
                    this.displayAlert('Το δρομολόγιο ΔΗΜΙΟΥΡΓΗΘΗΚΕ', 'success')
                })
                .catch(response => this.displayAlert(`ΣΦΑΛΜΑ: ${response.data.message}`, 'danger', true));
        },

        displayAlert(message, type, important = false){
            this.alert.message = message;
            this.alert.type = type;
            this.alert.important = important;
            this.$broadcast(`displayMsg`, this.alert)
        }
    },

    components : {
        Alert
    }
}