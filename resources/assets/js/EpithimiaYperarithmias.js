
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

import Modal from './components/ModalOrganikes.vue';
import Toggle from './components/toggle.vue';

var parent = new Vue ({
    el : '#app',

    data : {
        showModal : false,
        hideLoader : true,
        wants : true
    },

    methods : {
        openModal (){
            this.showModal = true;
        },

        sentRequest (){
            console.log('sent request');
            parent.$http.post('/aj/yperarithmia/save',{
                want_yperarithmia : parent.wants
            })
            .then(r => location.href = '/ΑΙΤΗΣΕΙΣ/Οργανικές/Αίτηση-Υπεραριθμίας')
            .catch(r => console.log('Error'));
        }
    },

    components : {
        Modal, Toggle
    }
});
