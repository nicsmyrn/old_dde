
var Vue = require('vue');
var VueResource = require('vue-resource');

Vue.use(VueResource);

Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token') ? document.querySelector('#token').attributes['content'].nodeValue : '';


//s

Vue.transition('fade', {
    enterClass : 'fadeIn',
    leaveClass: 'fadeOutRightBig'
});

var parent = new Vue ({
    el : '#app',

    components : {

    }
});
