@extends('app')

@section('header.style')
<meta id="token" name="csrf-token" content="{{csrf_token()}}" xmlns="http://www.w3.org/1999/html">

    <style>
        #periodRoutes{

        }
        #periodRoutes thead tr th{
            text-align: center;
        }

        .suggestion-fake{
            color: red;
            font-weight: bold;
        }
        .button-change{
            color : red;
        }

        .pointers {
            cursor: pointer;
        }

        .delete-button{
            padding-top: 15px !important;
            color: red;
            font-weight: bold;
        }

        .loading{
            width: 80px;
            height: 80px;
        }

        .school-header{
            color: #006666;
            font-size: 15pt;
            font-weight: 700;
            font-family: 'Merriweather', 'Helvetica Neue', Arial, sans-serif;
        }

    </style>
@endsection

@section('scripts.footer')
    <script src="{!! elixir('js/OfaSchoolGymnasiou.js') !!}"></script>
@endsection

@section('title')
    Δημιουργία Αθλοπαιδείας
@endsection

@section('content')

        <div class="row">
            <h3 class="page-heading">Κατάσταση συμμετοχής σε αγώνες ΑθλοΠαιδείας.</h3>

            <div class="panel panel-primary">
                <div v-if="loading" class="panel-body text-center">
                    <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                    <h5>παρακαλώ περιμένετε...</h5>
                </div>
                <div v-cloak v-else class="panel-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <span>Σχολικό Έτος: </span> <label v-cloak class="school-header"> @{{ dataSet.year.name }} </label>
                        </div>
                        <div class="form-group">
                            <label>Καθηγητής Φυσικής Αγωγής</label>
                            <input class="form-control" type="text" v-model="teacher_name" placeholder="γράψτε τον υπεύθυνο Φυσικής Αγωγής του Σχολείου"/>
                        </div>
                    </div>

                    <div class="col-md-4 text-center">
                        <div class="form-group form-inline">
                            <label>Άθλημα:</label>
                            <select class="form-control" v-cloak @change="displayGender(current_sport)" v-model="current_sport">
                                <option selected v-bind:value="nullValue">Επέλεξε Άθλημα</option>
                                <option v-for="sport in dataSet.sports" v-bind:value="sport">
                                    @{{ sport.name }}
                                </option>
                            </select>
                        </div>
                        <div class="form-group form-inline" v-if="current_sport != null">
                            <div v-if="current_sport.name != 'ΣΤΙΒΟΣ'">
                                <label>Φύλο:</label>
                                <select class="form-control" v-cloak @change="fetchAllStudentsByGender($value)" v-model="current_sex">
                                    <option selected v-bind:value="nullValue" style="background-color: #00a65a">Επέλεξε φύλο</option>
                                    <option value="0">Κορίτσια</option>
                                    <option value="1">Αγόρια</option>
                                    <option value="3">Μικτή Ομάδα</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-4">
                        <div class="form-group form-inline">
                            Σχολική Μονάδα: <label v-cloak class="school-header">{!! Auth::user()->userable->name !!}</label>
                        </div>
                        <div class="form-group">
                            <label>Συνοδός Εκπαιδευτικός</label>
                            <input class="form-control" type="text" v-model="synodos" placeholder="γράψτε τον συνοδό εκπαιδευτικό του Σχολείου"/>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div v-cloak v-if="showListForm && !showIndividualForm">
            <div v-if="loadingForExistence" class="row text-center">
                <img class="loading" src="{!! asset('images/Ripple.gif') !!}"/>
                <h5>παρακαλώ περιμένετε...</h5>
            </div>

            <div v-else class="row">

                <div v-if="notExistsPdfFile">
                    <div v-show="studentsList.length <= current_sport.list_limit">
                        <div v-if="allStudentsByGender.length > 0" class="col-md-3 col-md-offset-2">
                            <div class="panel panel-primary">
                                <div class="panel-heading text-center">
                                    ΕΠΙΛΟΓΗ ΜΑΘΗΤΩΝ
                                </div>
                                <div class="panel-body text-center">
                                    <select v-model="selectedStudent" style="margin-bottom: 10px; max-width: 80%">
                                        <option v-for="student in allStudentsByGender" v-bind:value="student">
                                            @{{ student.last_name }} @{{ student.first_name }} [@{{ student.am }}]
                                        </option>
                                    </select>

                                    <span class="pointers" @click="addStudentToList(selectedStudent)" v-show="selectedStudent!=null">
                                        <button class="btn btn-success btn-sm">
                                            προσθήκη στη λίστα
                                        </button>
                                    </span>
                                    <span v-else>
                                        <span class="label label-warning text-center">
                                            επέλεξε έναν μαθητή από τη λίστα μαθητών
                                        </span>
                                    </span>
                                </div>
                            </div>

                        </div>

                        <div v-cloak v-else class="col-md-3 col-md-offset-2">
                            <div class="alert alert-danger">
                                Δεν υπάρχουν μαθητές για επιλογή
                            </div>
                        </div>
                        <div class="col-md-2 text-center">
                            <h1>ή</h1>
                        </div>
                        <div class="col-md-3">
                            <div class="panel panel-primary">
                                <div class="panel-heading text-center">
                                    ΝΕΟΣ ΜΑΘΗΤΗΣ
                                </div>
                                <div class="panel-body text-center">
                                    <button @click="showModalNewStudent" class="btn btn-success btn-sm">
                                        Δημιουργία
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div v-else>
                        <div class="alert alert-danger col-md-6 col-md-offset-3">
                            <p><strong>Προσοχή!</strong></p>
                            <p>
                                Δεν μπορείτε να εισάγετε άλλους μαθητές στην κατάσταση συμμετοχής διότι φτάσατε το ανώτατο όριο.
                            </p>
                        </div>
                    </div>


                        <div class="col-md-12">

                            <table v-cloak v-if="studentsList.length > 0" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="5" class="text-center">
                                                <span v-cloak style="color: #0000ff;font-size: 14pt">@{{ studentsList.length }}
                                                    <span v-if="studentsList.length == 1">μαθητής</span>
                                                    <span v-else>μαθητές</span>
                                                </span> στην Κατάσταση Συμμετοχής
                                                <span v-cloak style="color: red;font-size: 9pt">
                                                    (@{{ (current_sport.list_limit - studentsList.length) }} διαθέσιμοι)
                                                </span>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">A/A</th>
                                            <th class="text-center">Επώνυμο</th>
                                            <th class="text-center">Όνομα</th>
                                            <th class="text-center">Πατρώνυμο</th>
                                            <th class="text-center">Ενέργειες</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr v-cloak v-for="student in studentsList">
                                                <td class="text-center" style="padding-top: 15px;">
                                                    @{{ $index + 1 }}
                                                </td>
                                                <td class="text-center">
                                                    @{{ student.last_name }}
                                                </td>
                                                <td class="text-center">
                                                    @{{ student.first_name }}
                                                </td>
                                                <td class="text-center">
                                                    @{{ student.middle_name }}
                                                </td>
                                                <td class="text-center delete-button">
                                                   <span @click="deleteStudent(student)" class="glyphicon glyphicon-trash pointers"></span>
                                                </td>
                                            </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5" class="text-center">
                                                <button @click="temporarySave" class="btn btn-danger">
                                                    Προσωρινή Αποθήκευση
                                                </button>
                                                 <button @click="openModal" class="btn btn-primary">
                                                    ΑΠΟΣΤΟΛΗ στην Ομάδα Φυσικής Αγωγής
                                                 </button>
                                            </td>
                                        </tr>
                                    </tfoot>
                            </table>

                            <div v-else>
                                <div class="col-md-4 col-md-offset-4 text-center">
                                    <div class="alert alert-warning">
                                        <p>
                                            <strong>ΔΕΝ</strong> υπάρχουν μαθητές στην κατάσταση συμμετοχής
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div v-else>
                    <div class="alert alert-danger col-md-4 col-md-offset-4 text-center">
                        <h4>Η συγκεκριμένη λίστα υπάρχει ήδη και είναι κλειδωμένη.</h4>
                        <a @click="downloadList" href="{!! url('downloadOfaPdfSportEducation/primary') !!}/@{{ dataSet.year.name }}/@{{ current_sport.name }}/@{{ current_sex }}" class="btn btn-success btn-lg" download>
                            Αποθήκευση στον Υπολογιστή
                        </a>
                    </div>
                </div>

            </div>

        <modal-student :sex="current_sex" :show.sync="showModalStudent" :school.sync="'primary_school'"></modal-student>

        <modal-agreement :show.sync="showModalAgreement" :loader.sync="hideLoaderAgreement"></modal-agreement>
    </div>

        <div v-cloak v-if="showIndividualForm">
                        <div v-if="loadingForExistenceIndividual" class="row text-center">
                            <img class="loading" src="{!! asset('images/under_construction.png') !!}"/>
                            <h5>παρακαλώ περιμένετε...</h5>
                        </div>

                        <div v-else class="row">
                            <div v-if="notExistsPdfFileIndividual">
                                <div v-show="studentsListIndividual.length <= current_sport.list_limit">
                                    <div v-if="allStudentsIndividual.length > 0" class="col-md-3 col-md-offset-2">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading text-center">
                                                ΕΠΙΛΟΓΗ ΜΑΘΗΤΩΝ
                                            </div>
                                            <div class="panel-body text-center">
                                                <div class="form-group">
                                                    <select v-model="selectedStudentIndividual" style="margin-bottom: 10px; max-width: 80%">
                                                        <option v-for="student in allStudentsIndividual" v-bind:value="student">
                                                            @{{ student.last_name }} @{{ student.first_name }} [@{{ student.am }}]
                                                        </option>
                                                    </select>
                                                </div>

                                                <div class="form-group" v-show="selectedStudentIndividual!=null">
                                                    <label>Αγώνισμα:</label>
                                                    <select v-model="selectedIndividualSport">
                                                        <option v-for="sport in individualSports" v-bind:value="sport">
                                                            @{{ sport.name }}
                                                        </option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <span class="pointers" @click="addStudentToIndividualList(selectedStudentIndividual, selectedIndividualSport)" v-show="selectedStudentIndividual!=null && selectedIndividualSport!=null">
                                                        <button class="btn btn-success btn-sm">
                                                            προσθήκη στη λίστα
                                                        </button>
                                                    </span>
                                                    <span v-else>
                                                        <div class="col-md-12 alert alert-warning text-center">
                                                            επέλεξε έναν μαθητή από τη λίστα και στη συνέχεια το αγώνισμά του.
                                                        </div>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div v-cloak v-else class="col-md-3 col-md-offset-2">
                                        <div class="alert alert-danger">
                                            Δεν υπάρχουν μαθητές για επιλογή
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-center">
                                        <h1>ή</h1>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="panel panel-primary">
                                            <div class="panel-heading text-center">
                                                ΝΕΟΣ ΜΑΘΗΤΗΣ
                                            </div>
                                            <div class="panel-body text-center">
                                                <button @click="showModalNewStudentIndividual" class="btn btn-success btn-sm">
                                                    Δημιουργία
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div v-else>
                                    <div class="alert alert-danger col-md-6 col-md-offset-3">
                                        <p><strong>Προσοχή!</strong></p>
                                        <p>
                                            Δεν μπορείτε να εισάγετε άλλους μαθητές στη Λίστα διότι φτάσατε το ανώτατο όριο.
                                        </p>
                                    </div>
                                </div>


                                    <div class="col-md-12">

                                        <table v-cloak v-if="studentsListIndividual.length > 0" class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th colspan="7" class="text-center">
                                                            <span v-cloak style="color: #0000ff;font-size: 14pt">@{{ studentsListIndividual.length }}
                                                                <span v-if="studentsListIndividual.length == 1">μαθητής</span>
                                                                <span v-else>μαθητές</span>
                                                            </span> στη Λίστα
                                                            <span v-cloak style="color: red;font-size: 9pt">
                                                                (@{{ (current_sport.list_limit - studentsListIndividual.length) }} διαθέσιμοι)
                                                            </span>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th class="text-center">A/A</th>
                                                        <th class="text-center">Επώνυμο</th>
                                                        <th class="text-center">Όνομα</th>
                                                        <th class="text-center">Πατρώνυμο</th>
                                                        <th class="text-center">Αγώνισμα</th>
                                                        <th class="text-center">Έτος Γέν.</th>
                                                        <th class="text-center">Ενέργειες</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                        <tr v-cloak v-for="student in studentsListIndividual">
                                                            <td class="text-center" style="padding-top: 15px;">
                                                                @{{ $index + 1 }}
                                                            </td>
                                                            <td class="text-center">
                                                                @{{ student.last_name }}
                                                            </td>
                                                            <td class="text-center">
                                                                @{{ student.first_name }}
                                                            </td>
                                                            <td class="text-center">
                                                                @{{ student.middle_name }}
                                                            </td>
                                                            <td class="text-center">
                                                                @{{ student.sport_name_special }}
                                                            </td>
                                                            <td class="text-center">
                                                                @{{ student.year_birth }}
                                                            </td>
                                                            <td class="text-center delete-button">
                                                               <span @click="deleteStudentIndividual(student)" class="glyphicon glyphicon-trash pointers"></span>
                                                            </td>
                                                        </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td colspan="7" class="text-center">
                                                            <button @click="temporarySaveIndividual" class="btn btn-danger">
                                                                Προσωρινή Αποθήκευση
                                                            </button>
                                                             <button @click="openModal" class="btn btn-primary">
                                                                ΑΠΟΣΤΟΛΗ στην Ομάδα Φυσικής Αγωγής
                                                             </button>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                        </table>

                                        <div v-else>
                                            <div class="col-md-4 col-md-offset-4 text-center">
                                                <div class="alert alert-warning">
                                                    <p>
                                                        <strong>ΔΕΝ</strong> υπάρχουν μαθητές στη Λίστα
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                            <div v-else>
                                <div class="alert alert-danger col-md-4 col-md-offset-4 text-center">
                                    <h4>Η συγκεκριμένη λίστα υπάρχει ήδη και είναι κλειδωμένη. 2β</h4>
                                    <a @click="downloadList" href="{!! url('downloadList') !!}/@{{ dataSet.year.name }}/@{{ current_sport.name }}/@{{ current_sex }}" class="btn btn-success btn-lg" download>
                                        Αποθήκευση στον Υπολογιστή
                                    </a>
                                </div>
                            </div>

                        </div>

            <modal-student :individual.sync="showIndividualForm" :sex="current_sex" :show.sync="showModalStudent" :school.sync="'primary_school'"></modal-student>

            <modal-agreement :individual.sync="showIndividualForm" :show.sync="showModalAgreement" :loader.sync="hideLoaderAgreement"></modal-agreement>
        </div>


    @include('KTEL.alert')

@endsection

@section('loader')
    @include('vueloader')
@endsection