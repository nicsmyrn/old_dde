        @if($students->isEmpty())
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <hr>
                        <div class="alert alert-info text-center" role="alert">Δεν υπάρχουν μαθητές</div>
                    </div>
                </div>
            </div>
        @else
            <div class="table-responsive">
                <table id="students" class="table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr class="active">
                            <th class="text-center">Αρ. Μητρώου</th>
                            <th class="text-center">Επώνυμο</th>
                            <th class="text-center">Όνομα</th>
                            <th class="text-center">Πατρώνυμο</th>
                            <th class="text-center">Μητρώνυμο</th>
                            <th class="text-center">Φύλο</th>
                            <th class="text-center">Τάξη</th>
                            <th class="text-center">Έτος Γέννησης</th>
                            <th class="text-center">Ενέργειες</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($students as $student)
                            <tr>
                                <td class="text-center">
                                    @if($type == 'Λύκειο' || $type == 'ΕΠΑΛ')
                                        <a href="{!! route('OFA::editStudent', $student->am) !!}">
                                        {!! $student->am !!}
                                    </a>
                                    @else
                                        <a href="{!! route('OFA::primary.editStudent', $student->am) !!}">
                                            {!! $student->am !!}
                                        </a>
                                    @endif

                                </td>
                                <td class="text-center">
                                    @if($type == 'Λύκειο' || $type == 'ΕΠΑΛ')
                                        <a href="{!! route('OFA::editStudent', $student->am) !!}">
                                            {!! $student->last_name !!}
                                        </a>
                                    @else
                                        <a href="{!! route('OFA::primary.editStudent', $student->am) !!}">
                                            {!! $student->last_name !!}
                                        </a>
                                    @endif
                                </td>
                                <td class="text-center">{!! $student->first_name !!}</td>
                                <td class="text-center">{!! $student->middle_name !!}</td>
                                <td class="text-center">{!! $student->mothers_name !!}</td>


                                <td class="text-center">
                                    @if($student->sex == 0)
                                        Κορίτσι
                                    @else
                                        Αγόρι
                                    @endif
                                </td>

                                <td class="text-center">
                                    {!! Config::get('requests.class')[$student->class] !!}
                                </td>

                                <td class="text-center">
                                    {!! $student->year_birth !!}
                                </td>

                                <td class="text-center delete-button">
                                    <button data-name="{!! $student->am !!}" class="deleteStudent btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>

                                </td>
                            </tr>
                        @endforeach


                    </tbody>
                </table>
            </div>

        @endif