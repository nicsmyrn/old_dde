@extends('app')

@section('title')
    Προβολή Κατάστασης Συμμετοχής
@endsection

@section('header.style')
    <style>
        .header-colored{
            color: blue;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <a href="{!! URL::previous() !!}" class="btn btn-primary">
                Επιστροφή
            </a>
            <h4 class="text-center">
                    ΚΑΤΑΣΤΑΣΗ ΣΥΜΜΕΤΟΧΗΣ {!! $participation->sport_list->gender == 0 ? 'ΜΑΘΗΤΡΙΩΝ' : $participation->sport_list->gender == 1 ? 'ΜΑΘΗΤΩΝ': 'ΜΙΚΤΗ' !!}
                    ΣΤΟ ΑΘΛΗΜΑ <span class="header-colored">«{!! $participation->sport_list->sport->name !!}»</span> Σχολικού Έτους <span class="header-colored">{!! $participation->sport_list->year->name !!}</span>
            </h4>

            @if($participation->details->isEmpty())
                <div class="alert alert-dander">
                    Η Κατάσταση Συμμετοχής ΔΕΝ υπάρχει
                </div>
            @else
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">Α/Α</th>
                                <th class="text-center">Επώνυμο</th>
                                <th class="text-center">Όνομα</th>
                                <th class="text-center">Όνομα Πατέρα</th>
                                <th class="text-center">Έτος Γέννησης</th>
                                <th class="text-center">Αρ. Μητρώου</th>
                                <th class="text-center">Τάξη</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($participation->details as $index=>$detail)
                                <tr>
                                    <td class="text-center">{!! $index + 1 !!}</td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td class="text-center">
                                    </td>
                                    <td class="text-center">
                                    </td>
                                    <td class="text-center">
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>


                <a href="{!! URL::previous() !!}" class="btn btn-primary">
                    Επιστροφή
                </a>
            @endif
        </div>
    </div>

@endsection

