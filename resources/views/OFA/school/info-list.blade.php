@extends('app')

@section('title')
    Προβολή Λίστα
@endsection

@section('header.style')
    <style>
        .header-colored{
            color: blue;
        }
    </style>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <a href="{!! URL::previous() !!}" class="btn btn-primary">
                Επιστροφή
            </a>
            <h4 class="text-center">
                @if($list->sport->individual)
                    ΚΑΤΑΣΤΑΣΗ ΣΥΜΜΕΤΟΧΗΣ
                @else
                    ΛΙΣΤΑ ΣΧΟΛΙΚΗΣ ΟΜΑΔΑΣ <span class="header-colored">{!! $list->gender == 0 ? 'ΜΑΘΗΤΡΙΩΝ' : $list->gender == 1 ? 'ΜΑΘΗΤΩΝ': 'ΜΙΚΤΗ' !!}</span>
                @endif
                    ΣΤΟ ΑΘΛΗΜΑ <span class="header-colored">«{!! $list->sport->name !!}»</span> Σχολικού Έτους <span class="header-colored">{!! $list->year->name !!}</span>
            </h4>

            @if($list == null)
                <div class="alert alert-dander">
                    Η λίστα ΔΕΝ υπάρχει
                </div>
            @else
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center">Α/Α</th>
                                <th class="text-center">Επώνυμο</th>
                                <th class="text-center">Όνομα</th>
                                <th class="text-center">Όνομα Πατέρα</th>
                                <th class="text-center">Έτος Γέννησης</th>
                                <th class="text-center">Αρ. Μητρώου</th>
                                <th class="text-center">Τάξη</th>
                                @if($list->sport->individual)
                                    <th class="text-center">
                                        Αγώνισμα
                                    </th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list->details as $index=>$detail)
                                <tr>
                                    <td class="text-center">{!! $index + 1 !!}</td>
                                    <td>
                                        {!! $detail->student->last_name !!}
                                    </td>
                                    <td>
                                        {!! $detail->student->first_name !!}
                                    </td>
                                    <td>
                                        {!! $detail->student->middle_name !!}
                                    </td>
                                    <td class="text-center">
                                        {!! $detail->student->year_birth !!}
                                    </td>
                                    <td class="text-center">
                                        {!! $detail->student->am !!}
                                    </td>
                                    <td class="text-center">
                                        {!! Config::get('requests.class')[$detail->student->class]  !!}
                                    </td>
                                    @if($list->sport->individual)
                                        <td class="text-center">
                                            {!! $detail->special->name !!}
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>


            <a href="{!! URL::previous() !!}" class="btn btn-primary">
                    Επιστροφή
                </a>
            @endif
        </div>
    </div>

@endsection

