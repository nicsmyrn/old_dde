<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-info">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12"></div>
                        <div class="checkbox text-center">
                            <label style="font-size: 1.5em">
                                <input id="checkCsvAttachment" type="checkbox">
                                <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                                Εισαγωγή ή ενημέρωση μαθητών από το MySchool
                            </label>
                        </div>
                </div>

                <div class="row" id="csv_myschool_attachments" style="display: none">
                    @if($type == 'Λύκειο' || $type == 'ΕΠΑΛ')
                        {!! Form::open(['method'=>'POST', 'action'=>'OFA\SchoolOfaController@insertStudentsFromMySchool', 'files' => true]) !!}
                    @else
                        {!! Form::open(['method'=>'POST', 'action'=>'OFA\SchoolOfaPrimaryController@insertStudentsFromMySchool', 'files' => true]) !!}
                    @endif

                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group input-file" name="csv_file">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-choose" type="button">Επιλογή αρχείου</button>
                                </span>
                                <input type="text" class="form-control" placeholder='Επιλέξτε το αρχείο csv του Myschool' />
                                <span class="input-group-btn">
                                     <button class="btn btn-warning btn-reset" type="button">Άκυρο</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="alert alert-warning">
                              <p>
                                  <strong>Προσοχή!</strong> το αρχείο πρέπει:
                              </p>
                              <p>
                                    <ul>
                                        <li> να είναι τύπου CSV</li>
                                        <li>να περιέχει τουλάχιστον μία τάξη</li>
                                        <li>να περιέχει οπωσδήποτε:</li>
                                        <li>
                                            <ol>
                                                <li>τον Αριθμό Μητρώου</li>
                                                <li>την Ημερομηνία Γέννησης</li>
                                                <li>το Επώνυμο</li>
                                                <li>το Όνομα</li>
                                                <li>το όνομα Πατέρα</li>
                                                <li>το όνομα Μητέρας</li>
                                                <li>το φύλο</li>
                                            </ol>
                                        </li>
                                    </ul>
                              </p>
                        </div>
                    </div>
                    {!! Form::submit('Ενημέρωση', ['class'=>'btn btn-primary', 'id' => 'sent']) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>