<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            /*font-family: "DejaVu Serif";*/
            margin: 0 40px 0 40px;
            font-size: 14pt;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        #header{
            margin-top: 30px;
            margin-bottom: 15px;
        }

        table .head-1{
            text-align: center;
            font-size: 17pt;
            font-weight: bold;
        }

        table .head-2{
            text-align: center;
            font-size: 13pt;
        }

        table .head-3{
            text-align: center;
            font-size: 14pt;
        }

        table .head-information{
            font-size: 12pt;
            text-align: left;
        }
        table .head-description{
            padding-top: 30px;
            font-size: 11pt;
            text-align: center;
        }

        #content{
            border: #000000 solid 2px;
        }

        #content .content-header{
            text-align: center;
            font-size: 13pt;
            font-weight: bold;
            height: 50px;
            word-wrap: break-word;
        }
        #content .content-header td{
            border: #000000 solid 1px;
        }

        #content .content-body{
            text-align: center;
            font-size: 13pt;
        }

        #content .content-body td{
            border: #000000 solid 1px;
            text-align: center;
        }

        .left-content{
            text-align: left !important;
            padding-left: 10px;
        }


        #footer{
            margin-bottom: 10px;
            width: 100%;
            font-size: 12pt;
            position: absolute;
            bottom: 200px;
        }

        .right_footer_date{
            text-align: left !important;
        }

        .header-signature{
             text-align: center;
             font-weight: bold;
        }

        .footer-signature{
            text-align: center;
            margin-top: 70px;
             font-weight: bold;
        }

        .bold-title{
            font-weight: bold;
        }

        .print_date{
            position: absolute;
            bottom:0;
            left: 0;
            font-size: 8pt;
        }

        .header-right{
            border: #000000 solid 1px;
            text-align: center;
            font-weight: bold;
        }


    </style>
</head>