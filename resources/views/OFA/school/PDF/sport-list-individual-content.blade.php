    <table id="header">
        <tr>
            <td class="header-left"></td>
            <td class="head-1">
                ΠΑΝΕΛΛΗΝΙΟΙ ΑΓΩΝΕΣ ΛΥΚΕΙΩΝ
            </td>
            <td class="header-right">
                <span id="header-right-content">
                    {!! $attr['sport']['identifier'] !!}
                </span>
            </td>
        </tr>

        <tr>
            <td class="head-3">
                <div>Τίτλος Σχολείου:</div>
                <div><span class="bold-title">{!! $attr['school']['name'] !!}</span></div>
            </td>
            <td>

            </td>
            <td class="head-3">
                <span class="bold-title">
                    Λυκείου - Ατομικά
                </span>
            </td>
        </tr>

        <tr>
            <td colspan="3" class="head-2">
                ΚΑΤΑΣΤΑΣΗ ΣΥΜΜΕΤΟΧΗΣ ΣΤΟ ΑΘΛΗΜΑ <b>&laquo;{!! $attr['sport']['name'] !!}&raquo; </b> ΣΧ. ΕΤΟΥΣ  <b>{!! $attr['year']['name'] !!}</b>
            </td>
        </tr>

    </table>

    <table id="content">
        <tr class="content-header">
            <td width="3%">α/α</td>
            <td width="20%">Επώνυμο</td>
            <td width="13%">Όνομα</td>
            <td width="13%">Όνομα Πατέρα</td>
            <td width="10%">Όνομα Μητέρας</td>
            <td width="10%">Έτος Γεννησ.</td>
            <td width="7%">Αρ. Μητρ.</td>
            <td width="4%">Τάξη</td>
            <td width="14%">Αγώνισμα</td>
            <td width="6%">Υπεύθυνη Δήλωση Γονέα</td>
        </tr>

        @for($i=1; $i<=$attr['sport']['list_limit']; $i++)
            @if(array_key_exists($i-1, $attr['studentsList']))
                <tr class="content-body">
                    <td>{!! ($i) !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['last_name'] !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['first_name'] !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['middle_name'] !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['mothers_name'] !!}</td>
                    <td>{!! $attr['studentsList'][$i-1]['year_birth'] !!}</td>
                    <td>{!! $attr['studentsList'][$i-1]['am'] !!}</td>
                    <td>{!! Config::get('requests.class')[$attr['studentsList'][$i-1]['class']] !!}</td>
                    <td>{!! $attr['studentsList'][$i-1]['sport_name_special'] !!}</td>
                    <td>ΝΑΙ</td>
                </tr>
            {{--@else--}}
                {{--<tr class="content-body">--}}
                    {{--<td>{!! ($i) !!}</td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                    {{--<td></td>--}}
                {{--</tr>--}}
            @endif
        @endfor

    </table>

    <table id="footer">
        <tr>
            <td width="30%">
                <div class="right_footer_date"></div>

                <div class="header-signature">
                    Συνοδός Καθηγητής ορίζεται
                </div>
                <div class="footer-signature">
                    {!! $attr['synodos'] !!}
                </div>
            </td>
            <td width="40%"></td>
            <td width="30%">
                <div class="right_footer_date">
                    Χανιά {!! \Carbon\Carbon::now()->format('d/m/Y') !!}
                </div>
                <div class="header-signature">
                    @if(Auth::user()->sex == 0)
                        Η Διευθύντρια
                    @else
                        Ο Διευθυντής
                    @endif
                </div>
                <div class="footer-signature">
                    {!! Auth::user()->first_name .' '. Auth::user()->last_name !!}
                </div>
            </td>
        </tr>
    </table>

