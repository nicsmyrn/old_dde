    <table id="header">
        <tr>
            <td class="header-left"></td>
            <td class="head-1">
            </td>
            <td class="header-right">
                <span id="header-right-content">
                </span>
            </td>
        </tr>

        <tr>
            <td class="head-3" colspan="3">
                Τίτλος Σχολείου: <span class="bold-title">{!! $attr['school']['name'] !!}</span>
            </td>
        </tr>

        <tr>
            <td colspan="3" class="head-2">
                <b>{!! $attr['gender'] == 0 ? 'ΚΟΡΙΤΣΙΑ': $attr['gender'] == 1 ? 'ΑΓΟΡΙΑ' :'ΜΙΚΤΗ' !!}</b> ΣΤΟ ΑΘΛΗΜΑ  <strong>&laquo;{!! $attr['sport']['name'] !!}&raquo;</strong> Σχ. Έτους <strong>{!! $attr['year']['name'] !!}</strong>
            </td>
        </tr>
    </table>

    <table id="content">
        <tr class="content-header">
            <td width="3%">α/α</td>
            <td width="22%">Επώνυμο</td>
            <td width="20%">Όνομα</td>
            <td width="12%">Όνομα Πατέρα</td>
            <td width="12%">Όνομα Μητέρας</td>
            <td width="10%">Έτος Γεννησ.</td>
            <td width="7%">Αρ. Μητρ.</td>
            <td width="4%">Τάξη</td>
            <td width="4%">ΑΓΩΝΙΣΜΑ</td>
            <td width="6%"></td>
        </tr>

        @for($i=1; $i<=$attr['sport']['list_limit']; $i++)
            @if(array_key_exists($i-1, $attr['studentsList']))
                <tr class="content-body">
                    <td>{!! ($i) !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['last_name'] !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['first_name'] !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['middle_name'] !!}</td>
                    <td class="left-content">{!! $attr['studentsList'][$i-1]['mothers_name'] !!}</td>
                    <td>{!! $attr['studentsList'][$i-1]['year_birth'] !!}</td>
                    <td>{!! $attr['studentsList'][$i-1]['am'] !!}</td>
                    <td>{!! Config::get('requests.class')[$attr['studentsList'][$i-1]['class']] !!}</td>
                    <td>
                        {!! $attr['studentsList'][$i-1]['sport_name_special'] !!}
                    </td>
                    <td></td>
                </tr>
            @else
                <tr class="content-body">
                    <td>{!! ($i) !!}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endif
        @endfor

    </table>



