<!DOCTYPE html>
<html lang="en">
    @include('OFA.school.PDF.sport-participation-status-header')
<body>
    @include('OFA.school.PDF.sport-participation-status-content')

    <div class="print_date">
        Ημερομηνία εκτύπωσης: {!! \Carbon\Carbon::now()->format('d-m-Y H:i:s') !!}
    </div>
</body>
</html>
