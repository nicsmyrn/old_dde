@extends('app')

@section('title')
    Προβολή Λίστας Συμμετοχής
@endsection

@section('scripts.footer')
	@include('OFA.admin._javascript')
@endsection

@section('content')
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Προβολή Λίστας Αγώνων
            </div>
            <div class="panel-body">
                <h3 class="text-center">

                <select size="30">
                    <option></option>
                    @foreach($schools as $school)
                        <option value="{!! action('OFA\AdminOfaController@showLists',str_replace(' ', '-', $school->name)) !!}"
                            {{--@if(str_replace(' ', '-', $name) == str_replace(' ', '-', $school))--}}
                                {{--selected--}}
                            {{--@endif--}}
                        >{!! $school->name !!}</option>
                    @endforeach
                </select>
                </h3>
            </div>
        </div>
    </div>
@endsection
