@extends('app')

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {

//
//            $('select').change(function(){
//                var url = $(this).val();
//                window.location = url;
//            })
//            .select2({
//                placeholder: "Επιλογή Σχολείου",
//                theme: "classic",
//                language: {
//                    noResults: function() {
//                        return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
//                    }
//                }
//            });


            var table = $('#index_lists').DataTable({
                    "order": [[ 6, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 4,5,7 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });

        });
    </script>
@endsection

@section('title')
    Λίστες όλων των Σχολείων
@endsection

@section('content')
    <h2>
        Λίστες
    </h2>

    <div class="col-md-12 col-lg-12">
        @include('OFA.admin._index-lists-by-filter')
    </div>
@endsection


