@extends('app')

@section('title')
    Λίστα Συμμετοχής για: {!! $name !!}
@endsection

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
	@include('OFA.admin._javascript')
@endsection

@section('content')
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Λίστα Συμμετοχής για: {!! $name !!}
            </div>
            <div class="panel-body">
                <h3 class="text-center">

                <select size="40">
                    <option></option>
                    @foreach($schools as $school)
                        <option value="{!! action('OFA\AdminOfaController@showLists',str_replace(' ', '-', $school->name)) !!}"
                            @if(str_replace(' ', '-', $name) == str_replace(' ', '-', $school->name))
                                selected
                            @endif
                        >{!! $school->name !!}</option>
                    @endforeach
                </select>
                </h3>
            </div>
        </div>
    </div>

    <div class="col-md-12 col-lg-12">
        @include('OFA.school.tables._index-lists-table', ['name' => $name, 'school_id' => $school_id])
    </div>
@endsection
