@extends('app')

@section('content')
<div class="container-fluid">
    <div class="col-md-12">
        <div class="alert alert-success text-center" role="alert">
            <b>Συγχαρητήρια</b>. Η εγγραφή σχεδόν ολοκληρώθηκε. Ελέγξτε το E-mail σας και κάντε ενεργοποίηση του λογαριασμού σας
        </div>
    </div>
</div>
@endsection
