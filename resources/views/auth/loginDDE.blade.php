<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> @yield('title')</title>

    <link rel="stylesheet" href="{!! elixir('css/app.css') !!}">

    <link rel="stylesheet" href="/build/css/dCxVpqJuO1R.css">


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body id="login_body">

	<div id="login_form" style="margin-top: 60px" class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">Σύνδεση στο Πληροφοριακό Σύστημα</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12">
								<a href="{!!url('/') !!}"><img src="/images/dide-logo.png"></a>
							</div>
						</div>
						<br>
						@include('errors.list')

                        <div class="col-md-12">
                            <div class="jumbotron">
                                <div class="form-group">
                                </div>
                                <form class="form-horizontal" role="form" method="POST" action="{!! route('postDdeLogin') !!}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                    <div class="form-group">
                                        <label class="control-label">E-Mail</label>
                                        <input type="email" class="form-control"  name="email" value="{{ old('email') }}">
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Κωδικός</label>
                                        <input type="password" class="form-control" name="password">
                                        <label><input type="checkbox" name="remember" > Να με θυμάσαι</label>
                                    </div>

                                     <button data-toggle="tooltip" data-placement="bottom" title="Σύνδεση με λογαριασμό της Ηλεκτρονικής Πλατφόρμας" type="submit" class="btn btn-primary">Σύνδεση</button>

                                </form>
                            </div>
                        </div>

					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<script>
	    $(function() {
	        $('input[name=email]').focus();

            $('[data-toggle="tooltip"]').tooltip()
		});
	</script>

</body>
</html>