<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> @yield('title')</title>

    <link rel="stylesheet" href="{!! elixir('css/app.css') !!}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.1.1/bootstrap-social.min.css" />

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body id="login_body">

	<div id="login_form" style="margin-top: 60px" class="container-fluid">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">Σύνδεση στο Πληροφοριακό Σύστημα</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-md-12 text-center">
								<a href="{!!url('/') !!}"><img src="/images/dide-logo.png"></a>
							</div>
						</div>
						<br>
						@include('errors.list')

                        <div class="row">
                            <div class="col-md-12 text-center">
                                    <h5>
                                        <span>
                                            <a href="{!! action('Auth\AuthController@schGrLogin') !!}" data-toggle="tooltip" title="υπάρχον λογαριασμό στο Πανελλήνιο Σχολικό Δίκτυο">
                                                Σύνδεση για <strong><u>καθηγητές</u></strong> μέσω:
                                                <img style="width:30%; height:30%;border-radius: 5px" src="/img/logo.jpg">
                                            </a>
                                        </span>
                                    </h5>
                            </div>

                            <div class="col-md-6">
                                <a href="{!! route('ddeLogin') !!}" class="btn btn-warning">Σύνδεση <b>ΜΟΝΟ</b> για Διοικητικούς Υπαλλήλους</a>
                            </div>
                        </div>

                        <br>
                        <div class="row">
                            <div class="col-md-10 text-center">
                                    <a href="{!! url('google/login/callback') !!}" class="btn btn-block btn-social btn-google">
                                      <span class="fa fa-google"></span>
                                      Σύνδεση με λογαριασμό Google
                                    </a>
                            </div>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Scripts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

	<script>
	    $(function() {
	        $('input[name=email]').focus();

            $('[data-toggle="tooltip"]').tooltip()
		});
	</script>

</body>
</html>