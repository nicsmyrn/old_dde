@extends('app')

@section('header.style')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">Εγγραφή Καθηγητή</div>
				<div class="panel-body">
				    @include('errors.list')

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <!-- Unique ID -->
                        <div class="form-group" id="">
                            {!! Form::label('relation', 'Σχέση:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::select('relation',['ΜΟΝΙΜΟΣ', 'ΑΝΑΠΛΗΡΩΤΗΣ'], null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <!-- AM or AFM -->
                        <div class="form-group" id="">
                            {!! Form::label('am', 'Αριθμός Μητρώου:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('am',null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <!-- Last_name  Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('last_name', 'Επώνυμο:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('last_name',null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <!-- First_name  Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('first_name', 'Όνομα:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('first_name',null, ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <!-- Email Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('email', 'E-mail:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::text('email',null, ['class'=>'form-control']) !!}
                            </div>
                        </div>


                        <!-- Password  Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('password', 'Κωδικός:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password', ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <!-- Password_confirmation  Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('password_confirmation', 'Επαλήθευση Κωδικού:', ['class'=>'col-md-4 control-label']) !!}
                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-6">
                                <div class="g-recaptcha" data-sitekey="{!! config('recaptcha.site_key') !!}"></div>
                            </div>
                        </div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4 text-center">
								<button type="submit" class="btn btn-primary">
									Εγγραφή
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

