@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Αλλαγή κωδικού χρήστη</div>
				<div class="panel-body">

				    @include('errors.list')

					<form class="form-horizontal" role="form" method="POST" action="{!! Auth::check()?route('User::postReset'):route('postResetLostPassword') !!}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{!! $reset->token !!}">

						<div class="form-group">
							<label class="col-md-4 control-label">Διεύθυνση E-Mail</label>
							<div class="col-md-6">
								<h4><label class="label label-default">{!! $reset->email !!}</label></h4>
								<input type="hidden" name="email" value="{!! $reset->email !!}"/>
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Κωδικός</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Επανάληψη κωδικού</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Αλλαγή κωδικού
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


