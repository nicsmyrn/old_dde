@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Ανάκτηση κωδικών</div>
				<div class="panel-body">

                    @include('errors.list')

                    {!! Form::open([
                        'method'=>'post',
                        'action'=>'Auth\LostController@postEmail',
                        'role'=>'form',
                        'class'=>'form-horizontal'
                     ]) !!}


						
						<!-- Email  Form Input -->
						<div class="form-group" id="">
						    {!! Form::label('email', 'Ηλεκτρονική Διεύθυνση Email:', ['class'=>'col-md-4 control-label']) !!}
						    <div class="col-md-6">
						        {!! Form::text('email',null, ['class'=>'form-control']) !!}
						    </div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Αποστολή
								</button>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

