@extends('app')

@section('content')
<div class="container-fluid">
    <div class="col-md-12">
        <div class="alert alert-success text-center" role="alert">
            <b>Συγχαρητήρια</b>. Η εγγραφή ολοκληρώθηκε. Πατήστε <a href="{!!url('auth/login')!!}" class="btn btn-success">Σύνδεση</a> για να εισέλθετε στο Πληροφοριακό Σύστημα
        </div>
    </div>
</div>
@endsection
