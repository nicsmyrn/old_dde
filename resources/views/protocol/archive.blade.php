@extends('app')

@section('title')
    Εισαγωγή Πρωτοκόλλου
@stop

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')
    <h1 class="page-heading">Αρχείο Πρωτοκόλλου</h1>
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <label>Επιλέξτε χρονολογία:</label>
            <select id="years" class="col-md-3">
                <option {!! Request::get('year') == \Carbon\Carbon::now()->format('Y')?'selected':'' !!} value="{!! route('Pysde::Protocol::archives', ['year'=>\Carbon\Carbon::now()->format('Y')]) !!}">{!! \Carbon\Carbon::now()->format('Y') !!}</option>
                @foreach($years as $year)
                <option {!! Request::get('year') == $year?'selected':'' !!} value="{!! route('Pysde::Protocol::archives', ['year'=>$year]) !!}">{!! $year !!}</option>
                @endforeach
            </select>

            <hr>
            <label>Έτος:  <span class="label label-primary">
                @if(Request::get('year')!= null)
                    {!! Request::get('year') !!}
                @else
                    {!! \Carbon\Carbon::now()->format('Y') !!}
                @endif
            </span> &#9755; </label>
            <select id="select_pdf" class="input-lg">
                <option></option>
                @foreach($group_array as $k=>$v)
                    @if(Request::get('year')== null || Request::get('year') == \Carbon\Carbon::now()->format('Y'))
                        <option value="{!! secure_url(route('Pysde::Protocol::toPDF')) !!}?page={!! $k+1 !!}">Σελίδα-{!!($k+1)!!} # {!! head($v) !!}-{!!  last($v) !!}</option>
                    @else
                        <option value="{!! secure_url(route('Pysde::Protocol::archivesToPDF')) !!}?page={!! $k+1 !!}">Σελίδα-{!!($k+1)!!} # {!! head($v) !!}-{!!  last($v) !!}</option>
                    @endif
                @endforeach
            </select> 
            <button class="btn btn-default">Εμφάνιση</button>            
        </div>
    </div>
    
@stop

@section('scripts.footer')
    <script>
        $(document).ready(function() {

            $('#years').change(function(){
                var url = $(this).val();
                window.location = url;
            })
            .select2({
                theme: "classic",
                language: {
                    noResults: function() {
                        return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                    }
                }
            });
         
            $('button').on('click', function(){
                var url = $('#select_pdf').val();
              window.open(url); 
            });
            
            $('#select_pdf').select2();
        
        });
    </script>            
@endsection


