@extends('app')


@section('content')
    <p>Nick Smyrnaios</p>
    <p>Νίκος Σμυρναίος</p>
    
    <a class="btnPrint btn btn-primary">Print!</a>
@endsection

@section('scripts.footer')
<script>

    
    $(document).ready(function() {
        $(".btnPrint").printPage({
            url: "{!! action('KenaController@all') !!}",
            attr: "href",
            message:"Εκτυπώνει"
     })
    });

</script>
@endsection