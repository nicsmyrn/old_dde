   <table class="list" style="width:97%; margin-top:1em;">
        <tbody>
            <tr class="primary_head">
                <td class="center" rowspan="2" style="width:5%">Αρ. Πρωτ.</td>
                <td class="center" rowspan="2" style="width: 6%">Ημερομηνία</td>
                <td class="center type" colspan="2">ΕΙΣΕΡΧΟΜΕΝΑ</td>
                <td class="center" rowspan="2" style="width: 3%">Φ</td>
                <td class="center type" colspan="2">ΕΞΕΡΧΟΜΕΝΑ</td>
                <td class="center" rowspan="2" style="width: 7%">Παρατηρήσεις</td>
            </tr>
            <tr class="head">
                <td class="center" style="width: 14%">Αποστολέας</td>
                <td class="center" style="width: 25%">Θέμα</td>
                <td class="center" style="width: 15%">Παραλήπτες</td>
                <td class="center" style="width: 25%">Θέμα</td>
            </tr>
            @foreach($protocols as $protocol)
                <tr class="list_row">
                    <td class="center protocol">{!! $protocol->id !!}</td>
                    <td class="center date">{!! $protocol->p_date !!}</td>
                    <td class="left">
                        @if($protocol->type == 0)
                            {!! $protocol->from_to !!}
                        @endif
                    </td>
                        @if($protocol->type == 0)
                            <td class="left"> {!! $protocol->subject !!} </td>
                        @elseif($protocol->type == 1)
                            <td class="center" style="letter-spacing: 0.5em"> ΕΞΕΡΧΟΜΕΝΟ</td>
                        @else
                            <td class="center"></td>
                        @endif
                    <td class="center">{!! $protocol->f_name !!}</td>
                    <td class="left">
                        @if($protocol->type == 1)
                            {!! $protocol->from_to !!}
                        @endif
                    </td>
                    <td class="left">
                        @if($protocol->type == 1)
                            {!! $protocol->subject !!}
                        @endif
                    </td>
                    <td class="left">{!! $protocol->description !!}</td>
                </tr>
            @endforeach

            {{--<tr class="foot">--}}
                {{--<td colspan="7" class="left">--}}
                    {{--<a href="{!! $protocols->previousPageUrl() !!}">&larr;</a>--}}
                {{--</td>--}}
                {{--<td class="right">--}}
                    {{--<a href="{!! $protocols->nextPageUrl()!!}">&rarr;</a>--}}
                {{--</td>--}}
            {{--</tr>--}}
        </tbody>
    </table>
