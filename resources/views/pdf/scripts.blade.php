    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datepicker.gr.js" charset="UTF-8"></script>
    <script>
        $(document).ready(function() {

            var endDate = new Date();
            $('.data_picker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
//                daysOfWeekDisabled: [0,6],
                endDate: endDate,
                language: 'gr'
            });


            $('#dateParallili').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                daysOfWeekDisabled: [0,6],
                endDate: endDate,
                language: 'gr'
            });

            $('#dateEidiki').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                daysOfWeekDisabled: [0,6],
                endDate: endDate,
                language: 'gr'
            });
         });
    </script>
    