<html>
    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <table id="kena_pleonasmata" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ΚΛΑΔΟΣ</th><th>Ειδικότητα</th><th
                    @foreach($header1 as $header)
                        <th class="header-rotate"><h6><small>{!! $header->name !!}</small></h6></th>
                    @endforeach
                </tr>
            </thead>
            
            <tbody>
                @foreach($eidikotites1 as $eidikotita)
                    <tr>
                        <td>{!! $eidikotita->slug_name !!}</td>
                        <td>{!! str_limit($eidikotita->name,15) !!}</td>
                        @foreach($eidikotita->schools as $school)
                            <td class='text-center'>{!! $school->pivot->value != 0 ? $school->pivot->value : ''!!}</td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        
        </table>   
        
</html>