<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <table id="kena_pleonasmata" class="table table-bordered table-hover">
       
        <thead>
            <tr>
                <th></th>
                @foreach($eidikotites2 as $eidikotita)
                    <th class="header-rotate"><h6><small>{!! $eidikotita->slug_name !!}</small></h6></th>
                @endforeach
            </tr>
        </thead>
        
        <tbody>
            @foreach($header2 as $school)
                <tr>
                    <td>{!! $school->name !!}</td>
                    @foreach($school->eidikotites as $eidikotita)
                        <td class='text-center'>{!! $eidikotita->pivot->value !!}</td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
        
    </table>
</html>
