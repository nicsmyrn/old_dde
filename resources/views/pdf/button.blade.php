<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<link href="/css/all.css" rel="stylesheet">
	<link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <h1>Hello Word!!! Νίκος Σμυρναίος</h1>

    <a class="btnPrint btn btn-success" href="http://library.gr/pdf">Μετατροπή σε PDF</a>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="/js/jquery.printPage.js"></script>
    <script>
        $(document).ready(function() {
            $(".btnPrint").printPage({
                message:"Εκτυπώνει"
             });
        });
    </script>
</body>
</html>
