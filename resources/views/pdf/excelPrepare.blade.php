@extends('app')

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="container">
        
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">Δημιουργία  πίνακα Κενών - Πλεονασμάτων</h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['action'=>'KenaController@excelCreate','method'=>'POST', 'class'=>'form-horizontal']) !!}
                                <div class="form-group">
                                    {!! Form::label('excel_date', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-5">
                                        {!! Form::text('excel_date',\Carbon\Carbon::now()->format('d/m/Y'), ['class'=>'form-control text-center', 'id'=>'date']) !!}
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                     {!! Form::submit('Δημιουργία Αρχείου Excel', ['class'=>'btn btn-primary']) !!}    
                                </div>
                        {!! Form::close() !!}
                    </div>
                </div>                
            </div>
        </div>  
        
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">Ηλεκτρονική Αρχειοθέτηση</h3>
                    </div>
                    <div class="panel-body text-center">
                        Υπό κατασκευή...
                    </div>
                </div>                 
            </div>
        </div>
    </div>
@endsection

@section('scripts.footer')
    @include('pdf.scripts')
@endsection
