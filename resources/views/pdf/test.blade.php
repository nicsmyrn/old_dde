<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style>
    /* General
    -----------------------------------------------------------------------*/
    body {
      background-color: #114C8D;
      color: #000033;
      font-family: DejaVu Sans, sans-serif;
      margin: 0;
      font-size: 1em;
      text-align: justify;

    }

        #header,
        #footer {
            position: fixed;
            left: 0;
        	right: 0;
        	color: #aaa;
        	font-size: 1em;
        	font-weight:bold;
        }

        #header {
            top: -14px;
            border-bottom: 0.1pt solid #aaa;
        }

        #footer {
          bottom: 0;
          border-top: 0.1pt solid #aaa;
        }

        #header table,
        #footer table {
        	width: 100%;
        	border-collapse: collapse;
        	border: none;
        }

        #header td,
        #footer td {
          padding: 0;
        	width: 50%;
        }

        .page-number {
          text-align: center;
        }

        .page-number:before {
          content: "-- {!! $protocols->currentPage() !!} --";
          font-family: DejaVu Sans, sans-serif;
        }

        hr {
          page-break-after: always;
          border: 0;
        }


    h1 {
      font-size: 1.1em;
      color: #114C8D;
      font-style: italic;
    }

    h2 {
      font-size: 1.05em;
      color: #114C8D;
    }

    h3 {
      font-size: 1em;
      color: #114C8D;
    }

    img {
      border: none;
    }

    img.border {
      border: 1px solid #114C8D;
    }

    p {
      font-size: 0.8em;
    }

    a:link,
    a:visited {
      text-decoration: none;
      color: #114C8D;
    }

    a:hover {
      text-decoration: underline;
      color: #860000;
    }

    .foot a:link,a:visited{
          text-decoration: none;
          color: #114C8D;
    }
    .foot a:hover {
      text-decoration: underline;
      color: #860000;
    }

    #page_header {
      position: relative; /* required to make the z-index work */
      z-index: 2;
    }

    #body {
      background-color: #F9F0E9;
      padding: 12px 0.5% 2em 3px;
      min-height: 20em;
      margin: 0px;
      width: 100%;
    }

    #body pre {
      color: #000033;
    }

    #left_column {
      width: 84%;
      /*height: auto;*/
      padding-right: 8px;
      padding-bottom: 30px;
    }

    #right_column {
    /*  position: absolute;
      right: 0.5%;*/
      padding-left: 16px;
      width: 15%;
      min-width: 160px;
    }


  




    /* Tables
    -----------------------------------------------------------------------*/
    table {
      empty-cells: show;
    }

    .head td {
      color: #666;
      background-color: #E5D9C3;
      font-weight: bold;
      font-size: 0.7em;
      
      padding: 2px;
    }

    .head input {
      font-weight: normal;
    }

    .sub_head td {
      border: none;
      white-space: nowrap;
      font-size: 10px;
    }

    .foot td {
      color: #666;
      background-color: #E5D9C3;
      font-size: 0.8em;
    }

    .label {
      color: #666;
      background-color: #F8F5F2;
      padding: 3px;
      font-size: 0.75em;
    }

    .label_right {
      color: #666;
      background-color: #F8F5F2;
      padding: 3px;
      font-size: 0.75em;
      text-align: right;
      padding-right: 1em;
    }

    .sublabel {
      color: #666;
      font-size: 0.6em;
      padding: 0px;
      text-align: center;
    }

    .field {
      color: #000033;
      background-color: #F9F0E9;
      padding: 3px;
      font-size: 0.75em;
    }

    .field_center {
      color: #000033;
      background-color: #F9F0E9;
      padding: 3px;
      font-size: 0.75em;
      text-align: center;
    }

    .field_nw {
      color: #000033;
      background-color: #F9F0E9;
      padding: 3px;
      font-size: 0.75em;
      white-space: nowrap;
    }

    .field_money {
      color: #000033;
      background-color: #F9F0E9;
      padding: 3px;
      font-size: 0.75em;
      white-space: nowrap;
      text-align: right;
    }

    .field_total {
      color: #000033;
      background-color: #F9F0E9;
      padding: 3px;
      font-size: 0.75em;
      white-space: nowrap;
      text-align: right;
      font-weight: bold;
      border-top: 1px solid black;
    }

    /* Table Data
    -----------------------------------------------------------------------*/
    .h_scrollable {
      overflow: -moz-scrollbars-horizontal;
    }

    .v_scrollable {
      overflow: -moz-scrollbars-vertical;
    }

    .scrollable {
      overflow: auto;/*-moz-scrollbars-horizontal;*/
    }

    tr.head>td.center,
    tr.list_row>td.center,
    .center {
      text-align: center;
    }

    .left,
    tr.head>td.left,
    tr.list_row>td.left {
      text-align: left;
      padding-left: 0.5em;
    }

    .total,
    .right,
    .list tr.head td.right,
    tr.list_row td.right,
    tr.foot td.right,
    tr.foot td.total {
      text-align: right;
      padding-right: 1em;
    }

    .list tr.foot td {
      font-weight: bold;
    }

    .no_wrap {
      white-space: nowrap;
    }

    .bar {
      border-top: 1px solid black;
    }

    .total {
      font-weight: bold;
    }

    .summary_spacer_row {
      line-height: 2px;
    }

    .light {
      color: #999999;
    }

 
    /* Lists
    -----------------------------------------------------------------------*/
    .list {
      border-collapse: collapse;
      border-spacing: 0px;
      border-top: 1px solid #666;
      border-bottom: 1px solid #666;
      width: 99%;
      margin-top: 3px;
    }

    .list tr.head td {
      font-size: 1em; /*nicsmyrn headers ΘΕΜΑ - Παραλήπτες */
      white-space: nowrap;
      padding-bottom: 0.4em;
      border: 1px solid #666;
    }

    .list table.sub_head td {
      border: none;
      white-space: nowrap;
      font-size: 10px;
    }

    .list tr.foot td {
      border-top: 1px solid #666;
      font-size: 0.7em;
    }
    
    tr.list_row>td {  /*nicsmyrn*/
      background-color: #EDF2F7;
      border-bottom: 1px solid #666;
      font-size: 10pt;
      /*height:45px;*/
      /*padding: 1px;*/
    }

    tr.list_row>td.protocol{
        font-size:1.2em;
        font-weight: bold;
        letter-spacing: 1px;
        height:32px;
    }

    tr.list_row>td.date{
        font-size:1em;
    }

    tr.list_row:hover td {
      background-color: #F8EEE4;
    }

    tr.problem_row>td {
      background-color: #FDCCCC;
      border-bottom: 1px solid #666;
      font-size: 0.65em;
      padding: 3px;
    }

    tr.problem_row:hover td {
      background-color: #F8EEE4;
    }

    .row_form td {
      font-size: 0.7em;
      padding: 3px;
      white-space: nowrap;
    /*  text-align: center; */
    }

    .row_form td.label {
      text-align: left;
      white-space: normal;
    }

    .inline_header td {
      color: #666;
      font-size: 0.6em;
      white-space: nowrap;
      text-align: center;
    }

 
    /* Print preview
    -----------------------------------------------------------------------*/
    .page {
      background-color: white;
      /*padding: 0px;*/
      border: 1px solid #eee;
    /*  font-size: 0.7em; */
      width: 95%;
      margin-bottom: 15px;
      margin-right: 5px;
    }

    .page table.header td {
      padding: 0px;
    }

    .page table.header td h1 {
      padding: 0px;
      margin: 0px;
    }

    .page h1 {
      color: black;
      font-style: normal;
      font-size: 1.3em;
    }

    .page h2 {
      color: black;
    }

    .page h3 {
      color: black;
      font-size: 1em;
    }

    .page p {
      text-align: justify;
      font-size: 0.8em;
    }

    .page table {
      font-size: 0.8em;
    }

    .page em {
      font-weight: bold;
      font-style: normal;
      text-decoration: underline;
      margin-left: 1%;
      margin-right: 1%;
    }

    .page table.money_table {
      font-size: 1.1em;
      border-collapse: collapse;
      width: 85%;
      margin-left: auto;
      margin-right: auto;
    }

    .page table.money_table tr.foot td {
      font-size: 1em;
      border-top: 0.4pt solid black;
      font-weight: bold;
      background-color: white;
      color: black;
    }

    .page table.money_table tr.foot td.right {
      padding-right: 1px;
    }

    .written_field {
      border-bottom: 1px solid black;
    }

    .page .written_field {
      border-bottom: 0.4pt solid black;
    }

    .page .indent * { margin-left: 4em; }

    .checkbox {
      border: 1px solid black;
      padding: 1px 2px;
      font-size: 7px;
      font-weight: bold;
    }


    table.signature_table {
      width: 80%;
      font-size: 0.7em;
      margin: 2em auto 2em auto;
    }

    table.signature_table tr td {
      padding-top: 1.5em;
      vertical-align: top;
      white-space: nowrap;
    }

    #special_conditions {
      font-size: 1.3em;
      font-style: italic;
      margin-left: 2em;
      font-weight: bold;
    }

    .sa_head p {
      font-size: 1em;
    }

    .page table.detail,
    .page table.fax_head {
      margin-left: auto;
      margin-right: auto;
    }

    .page .narrow,
    .page .fax_head {
      border: none;
    }

   /* TODO Primary Head nicsmyrn */
    .page tr.primary_head td{
        font-size:1em;
        color: black;
        border: 0.7pt solid #666;
    }

    .page tr.primary_head td.type{
        background: #eee;
        font-size: 1.6em;
        font-weight: bold;
        letter-spacing: 8px;
    }
    .page tr.head td {
      color: black;
      background-color: #eee;
    }

    .page td.label {
      color: black;
      background-color: white;
      width: 20%;
    }

    .page td.label_right {
      color: black;
      background-color: white;
    }

    .page td.field {
      background-color: white;
      font-weight: bold;
    }

    .page td.field_money {
      background-color: white;
    }

    .page td.field_total {
      font-weight: bold;
      background-color: white;
    }

    .page tr.detail_spacer_row td {
      background-color: white;
      border-top: 1px solid black;
    }

    .page .header {
      border-spacing: 0px;
      border-collapse: collapse;
      padding: 0px;
    }

    .page .header tr td {
      border-top: 1px solid #eee;
      border-bottom: 1px solid #eee;
      background-color: #eee;
    }
    /* Style definitions for printable pages */


 
    /* Tables
    -----------------------------------------------------------------------*/
    .head td {
      color: black;
      background-color: white;
    }

    .head input {
    }

    .foot td {
      color: black;
      background-color: white;
    }



    .label {
      color: black;
      background-color: white;
    }

    .sublabel {
      color: black;
    }

    .field {
      color: black;
      background-color: white;
    }

    .field_center {
      color: black;
      background-color: white;
    }

    .field_nw {
      color: black;
      background-color: white;
    }

    .field_money {
      color: black;
      background-color: white;
    }

    .field_total {
      color: black;
      background-color: white;
    }

  

    /* Lists
    -----------------------------------------------------------------------*/
    .list {
      border: 1px solid black;
    }

    .list tr.head>td {
      border-bottom: 0.7pt solid #666;
    }
    .list tr.foot td {
      border-top: 0.7pt solid #666;
    }

    tr.list_row>td {
      background-color: white;
      border-bottom: 1px solid #666;
    }

    tr.list_row:hover td {
      background-color: white;
    }


    /* Pages
    -----------------------------------------------------------------------*/
    .page>*>p, .page>p {
      font-size: 1.5em;
    }

    .written_field {
      font-size: 1em;
      border-bottom: 1px solid black;
    }

    .page h1 {
      font-size: 1em;
    }

    .page h2 {
      font-size: 0.9em;
    }

    @page {
      margin-bottom: 0.75in;
    }
 

    /* Lists by Nick Smyrnaios TODO lists
    -----------------------------------------------------------------------*/
    .list tr.head td {
      background-color: #666;
    }

    tr.list_row>td {
      background-color: white;
      border: 0.7pt solid #666;
    }

    .list tr.foot td {
      background-color: #eee;
    }

    /* Pages
    -----------------------------------------------------------------------*/
    .page {
      font-size: 1em;
      border: none;
      margin: none;
      width: auto;
      padding: 0px;
    }

    .foot td {
      font-size: 1em;
    }


    .page>*>p, .page>p {
      font-size: 0.8em;
    }


    table.signature_table {
      width: 88%;
      font-size: 0.6em;
    }

    #special_conditions {
      font-size: 1.5em;
    }

    .header h1 {
      font-size: 0.8em;
    }

    p.small {
      font-size: 0.8em;
    }

    .page td {
      padding: 1px;
    }

    td.label {
      font-size: 0.7em;
    }

    td.field {
      font-size: 0.7em;
    }

    td.field_money {
      font-size: 0.7em;
    }</style>
</head>
<body class="page" marginwidth="0" marginheight="0">
    <div id="header">
      <table>
        <tbody><tr>
          <td>Βιβλίο Πρωτοκόλλου ΠΥΣΔΕ Χανίων</td>
          <td class="center"></td>
          <td class="right">ΔΔΕ Χανίων</td>
        </tr>
      </tbody></table>
    </div>

    <div id="footer">
      <div class="page-number"></div>
    </div>

    @include('pdf.pdf_table')

</body>
</html>
