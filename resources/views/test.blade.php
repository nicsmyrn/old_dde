
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
	<meta name="author" content="The CSS Ninja" />
	<meta name="keywords" content="Css, CSS Ninja, The CSS Ninja, Javascript, Web, xhtml, html, browsers, javascript, jquery, cascading style sheets, extensible markup language" />
	<meta name="description" content="Using a combinantion of CSS3 to rotate text that works in IE5.5 and up" />
	<meta name="robots" content="all" />
	<meta name="copyright" content="The CSS Ninja" />
	
	<link rel="stylesheet" type="text/css" href="_styles.css" media="screen" />
	
	<title>Rotating text using CSS | The CSS Ninja</title>
	
	<style type="text/css">
	    /* Just some base styles not needed for example to function */
*, html { font-family: Verdana, Arial, Helvetica, sans-serif; }

body, form, ul, li, p, h1, h2, h3, h4, h5
{
	margin: 0;
	padding: 0;
}
body { background-color: #606061; color: #ffffff; }
img { border: none; }
p
{
	font-size: 1em;
	margin: 0 0 1em 0;
}
	
/* Text rotation exmaples styles */
.datebox
{
	float: left;
	background-color: #2c7ad0;
	border: 1px solid #000000;
	color: #ffffff;
	position: relative;
	margin: 20px 0 0 20px;
}
	.datebox .month
	{
		display: block;
		text-align: center;
		font-size: 30px;
		line-height: 30px;
		padding: 2px 0;
		background-color: #1e528c;
		text-transform: uppercase;
	}
	.datebox .day
	{
		display: block;
		text-align: center;
		font-size: 80px;
		line-height: 80px;
		font-weight: bold;
		padding: 2px 24px 2px 0;
	}
	.datebox .year
	{
		display: block;
		writing-mode: tb-rl;
		-webkit-transform: rotate(90deg);	
		-moz-transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		-o-transform: rotate(90deg);
		transform: rotate(90deg);
		position: absolute;
		right: 0;
		bottom: 9px;
		font-size: 24px;
	}
	*:first-child+html .datebox .year { right: -28px; } /* IE7 positions element differently to IE6 & 8 */
	.datebox span:nth-child(3)
	{
		right: -16px;
		bottom: 24px;
		writing-mode: lr-tb; 
	}
	
	
	/*################*/
	.rotate{
        -webkit-transform: rotate(-90deg);
        -moz-transform: rotate(-90deg);
        -ms-transform: rotate(-90deg);
        -o-transform: rotate(-90deg);
        transform: rotate(-90deg);
        mso-rotate:90;
        -webkit-transform-origin: 50% 50%;
        -moz-transform-origin: 50% 50%;
        -ms-transform-origin: 50% 50%;
        -o-transform-origin: 50% 50%; 
        transform-origin: 50% 50%;
        position: absolute;
        filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3); 
	}
	</style>

</head>
<body>
    
    <table>
        <tr>
            <th class="rotate"> 2009</th>
        </th>
    </table>
	
	
</body>
</html>

