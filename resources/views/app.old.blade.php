
<!DOCTYPE html>
<html lang="en">
<head>
    @include('meta')

	<title> @yield('title')</title>

    <link rel="stylesheet" href="{!! elixir('css/app.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/libs.css') !!}">


	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	@yield('header.style')

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="/css/html5shiv.min.js"></script>
		<script src="/css/respond.min.js"></script>
	<![endif]-->

</head>
<body id="app">
    @yield('loader')

	@include('partials.nav')
	<div class="container" style="margin-top: 60px;min-height: 350px">
		@yield('content')
	</div>

	@include('partials.footer')

    <script src="{!! elixir('js/libs-default.js') !!}"></script>

	@yield('scripts.footer')
	@include('flash')
    @include('partials.google_analytics')

    @include('partials.nav.notifications.notifications')

    @include('partials.countdown')

</body>
</html>
