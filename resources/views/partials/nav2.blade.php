 <div id="navbar-full">
    <div id="navbar">

        <nav class="navbar navbar-ct-blue navbar-fixed-top" role="navigation">

          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menubar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand navbar-brand-logo" href="{{ url('/') }}">
                    <div class="logo">
                        <img src="" height="60px" width="60px">
                    </div>
                    <div class="brand text-center">ΠΑΣΙΦΑΗ Χανίων</div>
              </a>
            </div>
            <div class="collapse navbar-collapse" id="menubar">

              <ul class="nav navbar-nav navbar-right">

                @if (Auth::guest())
                    @unless(Request::is('*/login'))
                        <li>
                            <a href="{!! url('auth/login') !!}">
                                <i class="pe-7s-user"></i>
                                <p>Σύνδεση</p>
                            </a>
                        </li>
                    @endunless
                @else
                    @include('partials.nav2.forPysde')
					@include('partials.nav2.forSchools')
					@include('partials.nav2.forDDE')
					@include('partials.nav2.forTeacher')
					@include('partials.nav2.forOikonomika')
					@include('partials.nav2.forAdmin')
					@include('partials.nav2.forMisthodosia')
					@include('partials.nav2.forOfa')

                    @include('partials.nav2.userDropdownPreferences')
                @endif
               </ul>

            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

    </div><!--  end navbar -->

</div> <!-- end menu-dropdown -->
