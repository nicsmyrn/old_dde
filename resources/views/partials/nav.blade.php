	<nav class="navbar  navbar-fixed-top" id="nav" role="navigation" style="background-color: #AEDEF4">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">ΔΔΕ Χανίων</a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav" id="navbar_menu">
					@include('partials.nav.forSchools')
					@include('partials.nav.forPysde')
					@include('partials.nav.forDDE')
					@include('partials.nav.forTeacher')
					@include('partials.nav.forOikonomika')
					@include('partials.nav.forAdmin')
					@include('partials.nav.forMisthodosia')
				</ul>

				<ul class="nav navbar-nav navbar-right">
					@if (Auth::guest())
						{{--<li><a href="#" data-toggle="modal" data-target="#loginModal">Σύνδεση</a></li>--}}
					@else
						@include('partials.nav.userDropdownPreferences')
					@endif
				</ul>
			</div>
		</div>
	</nav>

	@if(!(Auth::check()))
    <!--  #########################   L O G I N     M O D A L ####################################-->
               <div id="loginModal" class="modal fade" tabindex="-1" aria-labelledby="myModalLabel" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h2 class="text-center"><img src="{!! URL::asset('/img/default-avatar.gif') !!}" class="img-circle"><br>Σύνδεση</h2>
                            </div>
                            <div class="modal-body">
                                {!! Form::open(['action'=>'Auth\AuthController@postLogin', 'role'=>'form', 'method'=>'post', 'class'=> 'form col-md-12 center-block']) !!}
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i> </span>
                                            <input type="text" class="form-control input-lg" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key fa-fw"></i> </span>
                                            <input type="password" class="form-control input-lg" name="password" placeholder="Κωδικός">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">Σύνδεση</button>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                            <div class="modal-footer">
                                <div class="text-center"> <small><b>ή σύνδεση με:</b></small>
                                    <a data-toggle="tooltip" data-placement="top" title="Σύνδεση με την Google" class="btn  btn-social btn-google-plus" href="{!! url('google/login/callback') !!}">
                                        <i class="fa fa-google-plus"></i>Google
                                    </a>

                                    <a href="{!! action('Auth\AuthController@schGrLogin') !!}" data-toggle="tooltip" data-placement="left" title="Σύνδεση με το Πανελλήνιο Σχολικό Δίκτυο"><img src="/img/logo.jpg"></a>

                                    <small> ή κάντε
                                        <a href="{!!url('auth/register')!!}">Εγγραφή</a>
                                    </small>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Επιστροφή</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <!--  #########################   L O G I N     M O D A L ####################################-->
    @endif