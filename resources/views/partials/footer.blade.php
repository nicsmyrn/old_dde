		<footer class="footer-distributed">

			<div class="footer-left">

				<h3><span>ΠΑΣΙΦΑΗ</span></h3>

				<p class="footer-company-name">Δ.Δ.Ε. Χανίων &copy; 2016</p>

                <p>
                    <ul class="social-buttons colored-bg-on-hover round clearfix pull-left">
                        <li><a href="https://www.facebook.com/Διεύθυνση-Δευτεροβάθμιας-Εκπαίδευσης-Χανίων-1182809728398831/"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="http://didechan.blogspot.gr"><i class="fa fa-link"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    </ul>
                </p>
			</div>

			<div class="footer-center">

			    <div>
			        <i class="fa fa-user-secret"></i>
			        <p>
			            <span>
                            <a class="terms" href="#" type="button" data-toggle="modal" data-target="#termsModal">
                                Όροι Χρήσης - Εμπιστευτικότητας
                            </a>
			            </span>
                    </p>
			    </div>

			    <div>
			        <i class="fa fa-lock"></i>
			        <p> <span>
                        <a class="terms" href="#" type="button" data-toggle="modal" data-target="#privacyModal">
                            Προσωπικά Δεδομένα
                        </a>
			        </span> </p>
			    </div>

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>
					                        <a class="terms" href="#" type="button" data-toggle="modal" data-target="#cookiesModal">
                                                Cookies
                                            </a>
					</span></p>
				</div>



			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>Τεχνική Υποστήριξη</span>
					Για οποιοδήποτε τεχνικό πρόβλημα επικοινωνήστε στο : <a href="mailto://nicsmyrn@sch.gr">nicsmyrn@sch.gr</a>
				</p>

                <p>
                    Έκδοση: {!! Config::get('requests.version') !!}
                </p>

			</div>

		</footer>




		<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
                <strong><h4 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                    Όροι χρήσης - Εμπιστευτικότητας
                    </h4></strong>
              </div>
              <div class="modal-body">
                 <p>
                      Η Ηλεκτρονική Πλατφόρμα διέπεται από τις διατάξεις του Ν. 2472/1997,
                      όπως έχουν τροποποιηθεί και ισχύουν. Το σύστημα επεξεργάζεται προσωπικά
                      δεδομένα φυσικών προσώπων (εκπαιδευτικών, διοικητικού προσωπικού
                      Διευθύνσεων), τα οποία προκύπτουν αποκλειστικά
                      από το νομικό πλαίσιο που διέπει τις διαδικασίες διαχείρισης εκπαιδευτικού και
                       διοικητικού προσωπικού. Αποδέκτες των εν λόγω δεδομένων είναι οι εκ του νόμου
                        οριζόμενες δομές της Δ.Δ.Ε. Ν. Χανίων. Σκοπός της επεξεργασίας είναι η
                        μηχανογραφική υποστήριξη διοικητικών διαδικασιών της Διεύθυνσης Χανίων.
                        Πρόσβαση σε προσωπικά δεδομένα εκπαιδευτικών έχει το αρμόδιο τμήμα της Δ.Δ.Ε. Χανίων.
                 </p>

                 <p>
                     Οι λογαριασμοί χρήσης του συστήματος είναι αυστηρά προσωπικοί και εμπιστευτικοί.
                     Οι χρήστες του συστήματος αναλαμβάνουν την υποχρέωση να τηρούν απόλυτη
                     εμπιστευτικότητα και μυστικότητα ως προς τις πληροφορίες, τα έγγραφα,
                      τα προσωπικά στοιχεία ή/και δεδομένα, στα οποία έχουν πρόσβαση στο πλαίσιο
                       αξιοποίησης του συστήματος. Επίσης, οι χρήστες του συστήματος δεσμεύονται
                        να μην αποκαλύψουν σε οποιοδήποτε τρίτο πρόσωπο, για δικό τους όφελος ή
                        για λογαριασμό τρίτων, πληροφορίες, αρχεία, έγγραφα, συνθηματικά,
                        προσωπικά στοιχεία ή/και δεδομένα που θα περιέλθουν στη γνώση ή την
                        αντίληψή τους κατά την χρήση του συστήματος και αφορούν σε εκπαιδευτικούς
                         ή διοικητικό προσωπικό Διευθύνσεων.
                          Τέλος, οι χρήστες του συστήματος δεσμεύονται να γνωστοποιούν στους
                           διαχειριστές τις αλλαγές υπηρεσιακής αρμοδιότητας ή μονάδας/φορέα
                           υπηρέτησης και να τηρούν πιστά την πολιτική ασφάλειας του συστήματος
                            που περιλαμβάνει τη συχνή αλλαγή των μυστικών κωδικών.
                 </p>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Κλείσιμο</button>
              </div>
            </div>
          </div>
        </div>




		<div class="modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header modal-header-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
                <strong><h4 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                        Προσωπικά Δεδομένα
                    </h4></strong>
              </div>
              <div class="modal-body">
                 <p>
                    Η λειτουργία του συστήματος διέπεται από τους νόμους Ν. 2472/1997 και Ν. 3471/2006
                    περί προστασίας δεδομένων προσωπικού χαρακτήρα και της ιδιωτικής ζωής στον τομέα
                    των ηλεκτρονικών επικοινωνιών, όπως έχουν τροποποιηθεί και ισχύουν. Στο σύστημα έχει
                    ενεργοποιηθεί μηχανισμός καταγραφής των ενεργειών των χρηστών.
                 </p>

                 <p>
                    Ο λογαριασμός χρήστη που χρησιμοποιώ είναι αυστηρά προσωπικός
                    και δεν επιτρέπεται η χρήση του από άλλα πρόσωπα.
                 </p>

                 <p>
                    Η χρήση του συστήματος γίνεται αποκλειστικά στο πλαίσιο αρμοδιοτήτων
                     της εργασίας μου και για τον σκοπό αυτό δεσμεύομαι, πως δεν θα παραχωρήσω
                      κωδικούς πρόσβασης ή στοιχεία στα οποία έχω πρόσβαση σε τρίτους που δεν έχουν την αρμοδιότητα.
                 </p>


              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Κλείσιμο</button>
              </div>
            </div>
          </div>
        </div>

		<div class="modal fade" id="cookiesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header modal-header-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
                <strong><h4 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                        Cookies
                    </h4></strong>
              </div>
              <div class="modal-body">
                <script id="CookieDeclaration" src="https://consent.cookiebot.com/4377abba-fd5c-4a1f-8e45-8a8bf38293e6/cd.js" type="text/javascript" async></script>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Κλείσιμο</button>
              </div>
            </div>
          </div>
        </div>