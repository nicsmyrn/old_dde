	@if(Auth::user()->isRole('ofa'))
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="pe-7s-medal"></i>
                <p>Σχολικοί Αγώνες<b class="caret"></b></p>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{!! route('OFA::admin::adminShowLists') !!}">
                    	Αρχείο Λυκείων
                    	<span class="fa fa-cogs pull-right"></span>
                    </a>
                </li>

                <li>
                    <a href="{!! route('OFA::admin::adminShowGlobalLists') !!}">
                    	<span class="fa fa-book"></span>
                    	Λίστες ανά άθλημα
                    </a>
                </li>

                <li>
                    <a href="{!! route('OFA::admin::statistics') !!}">
                    	<span class="fa fa-book"></span>
                    	Στατιστικά
                    </a>
                </li>

            </ul>
        </li>

	@endif
