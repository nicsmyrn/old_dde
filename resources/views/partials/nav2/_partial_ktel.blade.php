    <li class="dropdown{!! \Request::is('Τοποθετήσεις*') ? ' active':'' !!}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="pe-7s-albums"></i>
            <p>ΚΤΕΛ <b class="caret"></b></p>
        </a>

        <ul class="dropdown-menu">
            <li>
                <a href="{!! route('BUS::Admin::routes') !!}">
                    Διαχείριση Δρομολογίων
                </a>
            </li>

            <li>
                <a href="{!! route('BUS::Admin::period') !!}">
                    Διαχείριση Έτους
                </a>
            </li>

            <li>
                <a href="{!! route('BUS::Admin::statistics') !!}">
                    Στατιστικά
                </a>
            </li>
        </ul>
    </li>