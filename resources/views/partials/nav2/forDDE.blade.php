	@if(Auth::user()->isRole('ekpaideutikon_thematon')||Auth::user()->isRole('manager'))
		<li {!! \Request::is('kena-pleonasmata*') ? ' class="active"':'' !!}>
			<a href="{!! action('KenaController@indexView') !!}">
                <i class="pe-7s-albums"></i>
                <p>Κενά - Πλεονάσματα <b class="caret"></b></p>
			</a>
		</li>
        <li>
            <a href="{!! action('ExcelController@excelArchives') !!}">
                <i class="pe-7s-albums"></i>
                <p>Αρχειοθέτηση</p>
            </a>
        </li>							
	@endif
