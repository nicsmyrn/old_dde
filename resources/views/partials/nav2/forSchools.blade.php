	@if(Auth::user()->isRole('school'))

        <li class="dropdown">
             <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 <i class="pe-7s-albums"></i>
                 <p>Κ.Τ.Ε.Λ. <b class="caret"></b></p>
             </a>
             <ul class="dropdown-menu">
                 <li>
                     <a href="{!! route('BUS::School::chooseRoutes') !!}">
                         Επιλογή Διαδρομών
                     </a>
                 </li>

                 <li>
                     <a href="{!! route('BUS::School::editPeriods') !!}">
                         Διαχείρση Δρομολογίων
                     </a>
                 </li>

                 <li>
                     <a href="{!! route('BUS::School::listPdf') !!}">
                         Αρχείο Βεβαιώσεων
                     </a>
                 </li>
             </ul>
         </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="pe-7s-albums"></i>
                <p>Κενά - Πλεονάσματα <b class="caret"></b></p>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{!! action('SchoolController@edit', str_replace(' ','-',Auth::user()->userable->name)) !!}">
                    	<span class="fa fa-cogs"></span>
                    	Επεξεργασία
                    </a>
                </li>

                <li>
                    <a href="{!! action('SchoolController@show', str_replace(' ','-',Auth::user()->userable->name)) !!}">
                    	<span class="fa fa-book"></span>
                    	Προβολή Πίνακα
                    </a>
                </li>
                
            </ul>
        </li>

        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="pe-7s-albums"></i>
                <p>Τοποθετήσεις<b class="caret"></b></p>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{!! action('SchoolPlacementsController@getTeachersOrganikiPlacements') !!}">
                        Καθηγητές με οργανική στο Σχολείο
                    </a>
                </li>

                <li>
                    <a href="{!! action('SchoolPlacementsController@getTeachersWithPlacements') !!}">
                        Καθηγητές με τοποθέτηση στο Σχολείο
                    </a>
                </li>

            </ul>
        </li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="pe-7s-albums"></i>
                        <p>Σχολικά Πρωταθλήματα<b class="caret"></b></p>
                    </a>
                    <ul class="dropdown-menu">
                        @if(Auth::user()->userable->type == 'Λύκειο' || Auth::user()->userable->type == 'ΕΠΑΛ')
                            <li>
                                <a href="{!! route('OFA::sportList') !!}">
                                    (1α,β,γ). Δημιουργία Λίστας Ομαδικών Αθλημάτων
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('OFA::sportListIndividual') !!}">
                                    (2). Δημιουργία Κατάστασης Συμμετοχής ΑΤΟΜΙΚΩΝ Αθλημάτων
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('OFA::sportParticipationStatus') !!}">
                                    (3). Δημιουργία Κατάστασης Συμμετοχής ΟΜΑΔΩΝ
                                </a>
                            </li>

                            <li class="divider"></li>
                            <li>
                                <a href="{!! route('OFA::archivesStatementsHighSchool') !!}">
                                    ΑΡΧΕΙΟ ΔΗΛΩΣΕΩΝ του Σχολείου
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('OFA::listOfStudents') !!}">
                                    ΑΛΛΑΓΗ ΣΤΟΙΧΕΙΩΝ ΜΑΘΗΤΩΝ
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{!! route('OFA::sportEducation') !!}">
                                    Δημιουργία Κατάστασης Συμμετοχής ΑθλοΠαιδεία
                                </a>
                            </li>
                            <li class="divider"></li>


                            <li>
                                <a href="{!! route('OFA::archivesStatements') !!}">
                                    ΑΡΧΕΙΟ ΔΗΛΩΣΕΩΝ
                                </a>
                            </li>
                            <li>
                                <a href="{!! route('OFA::listOfStudentsPrimary') !!}">
                                    <span class="fa fa-book"></span>
                                    Καταστάσεις Μαθητών
                                </a>
                            </li>
                        @endif
                    </ul>
                </li>

        {{--<li id="schools_read_only" class="animated flash">--}}
            {{--<a href="#">--}}
                {{--<span class="fa fa-lock"></span>--}}
                {{--<span class="label label-warning">Μόνο για Ανάγνωση</span>--}}
            {{--</a>--}}
        {{--</li>--}}
        {{--<li>--}}
             {{--<a id="" href="{!! action('SchoolController@confirmation', str_replace(' ','-',Auth::user()->userable->name)) !!}" class="btn btn-success btn-sm">--}}
                 {{--Επικαιροποίηση <span id="confirmation_date">{!! Auth::user()->userable->confirmation !!}</span>--}}
             {{--</a>--}}
        {{--</li>--}}



	@endif
