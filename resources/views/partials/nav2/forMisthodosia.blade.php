	@if(Auth::user()->isRole('misthodosia'))
        <li class="dropdown{!! \Request::is('Εργαζόμενοι*') ? ' active':'' !!}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="pe-7s-albums"></i>
                <p>Εργαζόμενοι<b class="caret"></b></p>
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="{!! route('Misthodosia::allErgazomenoi') !!}">
                        Όλοι
                    </a>
                </li>
                <li>
                    <a href="{!! route('Misthodosia::notChecked') !!}">
                        για Έλεγχο
                        @if($numberOfTeachersForCheckingMisthodosia > 0)
                            <span class="badge">{!! $numberOfTeachersForCheckingMisthodosia !!}</span>
                        @endif
                    </a>
                </li>
                <li>
                    <a href="{!! route('Misthodosia::allWithAccount') !!}">
                        με λογαριασμό στην Πασιφάη
                    </a>
                </li>
                <li>
                    <a href="{!! route('Misthodosia::allWithOutAccount') !!}">
                        χωρίς Λογαριασμό
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="{!! route('Misthodosia::getMisthodosiaExcel') !!}">
                <i class="pe-7s-albums"></i>
                <p>Εξαγωγή σε Excel</p>
            </a>
        </li>
@endif
