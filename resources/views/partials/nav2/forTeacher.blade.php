	@if(Auth::user()->isRole('teacher'))
		<li class="dropdown{!! \Request::is('aitisi*') ? ' active':'' !!}">
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="pe-7s-news-paper"></i>
                <p>Αιτήσεις<b class="caret"></b></p>
			</a>

            <ul class="dropdown-menu">
                <li>
                    <a href="{!! action('RequestController@createPreparation') !!}">
                    	Νέα Αίτηση
                    </a>
                </li>

                <li>
                    <a href="{!! action('RequestController@index') !!}">
                    	Αρχείο Αιτήσεων Ε1-Ε5, Μοριοδότησης
                    </a>
                </li>
                <li>
                    <a href="{!! action('RequestController@indexYperarithmia') !!}">
                        Αρχείο Αιτήσεων Υπεραριθμίας
                    </a>
                </li>
                <li>
                    <a href="{!! action('RequestController@indexOrganiki') !!}">
                        Αρχείο Αιτήσεων για Οργανική Θέση
                    </a>
                </li>
            </ul>
		</li>

        <li>
            <a href="{!! action('TeacherPlacementsController@index') !!}">
                <i class="pe-7s-albums"></i>
                <p>Τοποθετήσεις</p>
            </a>
        </li>

        {{--<li class="animated wobble">--}}
            {{--<a href="#">--}}
                {{--<span class="fa fa-exclamation-circle"></span>--}}
                {{--<span class="label label-warning">Οι αιτήσεις θα είναι διαθέσιμες σε λίγο...</span>--}}
            {{--</a>--}}
        {{--</li>--}}
        @if(\Auth::user()->userable_type == 'App\Teacher' && \Auth::user()->userable_id != '0')
            @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
                {{--<li class="animated wobble">--}}
                    {{--<a href="#">--}}
                        {{--<span class="fa fa-exclamation-circle"></span>--}}
                        {{--<span class="label label-success">Η Αίτηση <b>E2</b> θα είναι  διαθέσιμη για:  <span id="clock_teacher_monimos"></span> </span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            @elseif(\Auth::user()->userable->teacherable_type == 'App\Anaplirotis')
                {{--<li class="animated wobble">--}}
                    {{--<a href="#">--}}
                        {{--<span class="fa fa-exclamation-circle"></span>--}}
                        {{--<span class="label label-success">Η Αίτηση <b>Ε5</b>  θα είναι διαθέσιμη για: <span id="clock_teacher_anaplirotis"></span></span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            @endif
        @endif

        {{--<li>--}}
            {{--<a href="{!! route('kena_sxoleion') !!}">--}}
                {{--Κενά Σχολικών Μονάδων--}}
                {{--<i class="fa fa-skype"></i>--}}
            {{--</a>--}}
        {{--</li>--}}
	@endif
