	@if(Auth::user()->isRole('oikonomika'))
        <li class="dropdown{!! \Request::is('Τοποθετήσεις*') ? ' active':'' !!}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="pe-7s-albums"></i>
                <p>Τοποθετήσεις<b class="caret"></b></p>
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="{!! route('Dioikisi::Placements::allTeachers') !!}">
                        ΟΛΕΣ
                    </a>
                </li>
                <li>
                    <a href="{!! route('Dioikisi::Placements::placementsByPraxi') !!}">
                        ανά Πράξη
                    </a>
                </li>
            </ul>
        </li>
	@endif
