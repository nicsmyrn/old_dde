    socket.on('pysde_secretary:App\\Events\\SchoolSaveChanges', function(data){
        console.log(data);
        addNotification(data.url,data.type, data.title, data.description);
    });
    socket.on('pysde_secretary:App\\Events\\TeacherSendNotificationToPysde', function(data){
        console.log(data);
        addNotification(data.url,data.type, data.title, data.description);
    });

    socket.on('pysde_secretary:App\\Events\\SchoolConfirmToPysde', function(data){
        console.log(data);
        addNotification(data.url,data.type, data.title, data.description);
    });

    socket.on('pysde_secretary:App\\Events\\TeacherChangeProfileNotification', function(data){
        console.log(data);
        addNotification(data.url,data.type, data.title, data.description);
    });

    socket.on('pysde_secretary:App\\Events\\TeacherRequestForMoriodotisi', function(data){
        console.log(data);
        addNotification(data.url,data.type, data.title, data.description);
    });