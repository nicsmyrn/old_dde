var permissionLabel = $('#schools_read_only');


@can('edit_school_kena')
    permissionLabel.detach();
@endcannot

socket.on('school-permission:App\\Events\\SchoolGrantEditPermission', function(data){
    if(data.noPermission){
        permissionLabel.appendTo('#navbar_menu');
    }else{
        permissionLabel.detach();
    }
});

socket.on('school-{!! Auth::id() !!}:App\\Events\\PysdeSendNotificationToSchool', function(data){
   addNotification(data.url,data.type, data.title, data.description);
});