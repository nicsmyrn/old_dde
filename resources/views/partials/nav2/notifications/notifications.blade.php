    @if(Auth::check())

         @if(Auth::user()->isRole('pysde_secretary'))
             <script src="{!! elixir('js/forPysdeNotification.js') !!}"></script>
         @elseif(Auth::user()->isRole('school'))
             <script src="{!! elixir('js/forSchoolNotification.js') !!}"></script>
         @elseif(Auth::user()->isRole('teacher'))
              <script src="{!! elixir('js/forTeacherNotification.js') !!}"></script>
         @elseif(Auth::user()->isRole('misthodosia'))
              <script src="{!! elixir('js/forMisthodosiaNotification.js') !!}"></script>
         @elseif(Auth::user()->isRole('ofa'))
              <script src="{!! elixir('js/forOfaNotification.js') !!}"></script>
         @endif

     @endif