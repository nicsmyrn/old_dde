<li class="dropdown" id="notifications">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <i class="pe-7s-bell"></i>
            <span v-cloak v-if="notifications.length" class="badge" id="notify_counter" style="background-color: red">
                @{{ notifications.length }}
            </span>
        <p>Ειδοποιήσεις<b class="caret"></b></p>
    </a>

    <ul class="dropdown-menu" style="width: 300px">
            <div v-if="notifications.length == 0">
                <h3 class="text-center" id="null_notification">Δεν υπάρχουν νεες ειδοποιήσεις</h3>
            </div>
            <div v-else  v-bind:class="{
                            'notification_list' : true,
                            'scroller' : (notifications.length >= 4)
                        }" id="main-navbar-notifications">
                <div v-for="notification in notifications" class="notification_div">
                    <a href="@{{ notification.url }}">
                        <div v-cloak class="notification-title my-text-@{{ notification.type }}">@{{ notification.title }}</div>
                        <div v-cloak class="notification-description" v-html="notification.description"></div>

                        <div v-cloak class="notification-ago">
                            <time-counter :time="notification.time" :h="notification.hour_time" :m="notification.minute_time"></time-counter>
                        </div>
                        <div v-cloak class="notification-icon fa fa-hdd-o my-bg-@{{ notification.type }}"></div>
                    </a>
                </div>
            </div>
            <a href="{!! action('NotificationController@index') !!}" class="notification_link">ΟΛΕΣ οι ειδοποιήσεις...</a>

    </ul>
</li>

<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
        <i class="pe-7s-user"></i>
        <p>{{ Auth::user()->full_name }}<b class="caret"></b></p>
    </a>
    <ul class="dropdown-menu">
        <li>
            <a href="{!! route('User::getChangePwd') !!}">
                <i class="dropdown-icon fa fa-cog"></i>
                Προφίλ
            </a>
        </li>

        <li>
            <a href="{{ url('/auth/logout') }}">
                <i class="dropdown-icon fa fa-power-off"></i>
                Αποσύνδεση
            </a>
        </li>
    </ul>
</li>