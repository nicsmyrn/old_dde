    @if(Auth::check())
        @if(Auth::user()->isRole('teacher'))
            <script src="{!! asset('js/jquery.countdown.min.js') !!}"></script>
            <script>
                $(function() {
                    @if(\Auth::user()->userable_type == 'App\Teacher' && \Auth::user()->userable_id != '0')

                        @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
                            $('#clock_teacher_monimos').countdown('2017/06/04 13:00:00', function(event) {
                                $(this).html(event.strftime('<strong>%-D</strong> ημέρες <strong>%-H</strong> ώρες <strong>%-M</strong> λεπτά <strong>%-S</strong> δευτερόλεπτα'));
                            })
                            .on('finish.countdown', function(){
                                location.reload();
                            });
                        @elseif(\Auth::user()->userable->teacherable_type == 'App\Anaplirotis')
                            $('#clock_teacher_anaplirotis').countdown('2017/06/04 23:59:00', function(event) {
                                $(this).html(event.strftime('<strong>%-D</strong> ημέρες <strong>%-H</strong> ώρες <strong>%-M</strong> λεπτά <strong>%-S</strong> δευτερόλεπτα'));
                            })
                            .on('finish.countdown', function(){
                                location.reload();
                            });
                        @endif

                    @endif
                });
            </script>
        @endif
    @endif