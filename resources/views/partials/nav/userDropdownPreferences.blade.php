<li class="dropdown" id="notifications">
    <a href="#notifications" class="dropdown-toggle" data-toggle="dropdown">
        <i class="nav-icon fa fa-bullhorn"></i>
        <span class="badge" id="notify_counter" style="background-color: red">{!! Auth::user()->notifications()->count() > 0 ? Auth::user()->notifications()->count() : '' !!}</span>
    </a>

    <div class="dropdown-menu" style="width: 300px">
        <div class="notification_list" id="main-navbar-notifications">
            @if(Auth::user()->notifications()->isEmpty())
                <h3 class="text-center" id="null_notification">Δεν υπάρχουν νεες ειδοποιήσεις</h3>
            @else
                @foreach(Auth::user()->notifications() as $notification)
                    <div class="notification_div">
                        <a href="{!! $notification->url !!}">
                            <div class="notification-title my-text-{!! $notification->type !!}">{!! $notification->title !!}</div>
                            <div class="notification-description">{!! $notification->description !!}</div>

                            <?php \Carbon\Carbon::setLocale('el'); ?>

                            <div class="notification-ago">{!! $notification->created_at->diffForHumans() !!}</div>
                            <div class="notification-icon fa fa-hdd-o my-bg-{!! $notification->type !!}"></div>
                        </a>
                    </div>
                @endforeach
            @endif
        </div>
        <a href="{!! action('NotificationController@index') !!}" class="notification_link">ΟΛΕΣ οι ειδοποιήσεις...</a>
    </div>
</li>

<li class="dropdown">
	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->full_name }} <span class="caret"></span></a>
    <ul class="dropdown-menu">
        <li>
            <a href="{!! route('User::getChangePwd') !!}">
                <i class="dropdown-icon fa fa-cog"></i>
                Προφίλ
            </a>
        </li>

        <li>
            <a href="{{ url('/auth/logout') }}">
                <i class="dropdown-icon fa fa-power-off"></i>
                Αποσύνδεση
            </a>
        </li>
    </ul>
</li>