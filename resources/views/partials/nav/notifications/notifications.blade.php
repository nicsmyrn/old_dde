<script>
    (function(){
        var socket = io();
        var count = Number($('#notify_counter').text());

        $('#main-navbar-notifications').mCustomScrollbar({
            theme: "minimal-dark"
        });

        @if(Auth::check())

             @if(Auth::user()->isRole('pysde_secretary'))
                @include('partials.nav.notifications.for_pysde')
             @elseif(Auth::user()->isRole('school'))
                @include('partials.nav.notifications.for_school')
             @elseif(Auth::user()->isRole('teacher'))
                @include('partials.nav.notifications.for_teacher')
             @elseif(Auth::user()->isRole('misthodosia'))
                @include('partials.nav.notifications.for_misthodosia')
             @endif


         function addNotification(url, type, title, description){
                 if ($('#null_notification').length){
                     $('#null_notification').remove();
                 }
                 count++;
                 $('#notify_counter').removeClass('animated shake').addClass('animated shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
                     $(this).removeClass('animated shake');
                 }).text(count);

                 $('<div>')
                    .addClass('notification_div')
                    .append(
                        $('<a>').attr('href', url)
                            .append($('<div>').addClass('notification-title my-text-'+type).text(title))
                            .append($('<div>').addClass('notification-description').html(description))
                            .append($('<div>').addClass('notification-ago').text('μόλις τώρα...'))
                            .append($('<div>').addClass('notification-icon fa fa-hdd-o my-bg-'+type))
                    ).prependTo('#mCSB_1_container');
         }

         @endif

    })();
</script>