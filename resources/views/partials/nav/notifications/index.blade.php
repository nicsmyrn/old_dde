@extends('app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                    <h1 class="page-heading">Ειδοποιήσεις</h1>
            </div>
            <div class="col-md-3">
                @if(!$notifications->isEmpty())
                    <button id="deleteAllNotifications" class="btn btn-danger btn-lg"><i class="glyphicon glyphicon-trash"></i> Διαγραφή</button>
                @endif
            </div>
        </div>

        <div class="col-md-10 col-md-offset-1">
            @if(!$notifications->isEmpty())
                <table class="table table-hover">
                    <thead>
                        <th>Τίτλος</th>
                        <th>Ημερομηνία & ώρα</th>
                        <th>Περιγραφή</th>
                    </thead>
                    <tbody>
                        @foreach($notifications as $notification)
                            <tr class="{!! $notification->type !!} clickable-row" data-href="{!! $notification->url !!}?action={!! $notification->action !!}">
                                <td>{!! $notification->title !!}</td>
                                <td>{!! $notification->timestamp !!}</td>
                                <td>{!! $notification->description !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-warning text-center" role="alert"><h3><b>Δεν υπάρχουν ειδοποιήσεις</b></h3></div>
            @endif
        </div>
    </div>
@endsection

@section('scripts.footer')
    <script>
        jQuery(document).ready(function($) {
            $(".clickable-row").click(function() {
                window.document.location = $(this).data("href");
            })
            .on('mouseover', function(){
                $(this).css('cursor', 'pointer');
            });

            $('#deleteAllNotifications').on('click', swalAlertDelete)
        });

        function swalAlertDelete(e){
            e.preventDefault();

            swal({
                title: "Προσοχή!",
                text: "Με αυτήν την ενέργεια θα διαγραφούν ΟΛΕΣ οι ειδοποιήσεις",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Εντάξει",
                cancelButtonText: "Άκυρο",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                closeOnCancel: true },
                function(){
                    $.ajax({
                        type : 'get',
                        url : "{!! route('ajaxDeleteNotifications')!!}",
                        success : function(data){
                            location.reload(true);
                        },
                        error: function (request, status, error) {
                           $('body').html(request.responseText);
                       }
                    });
                }
            );
        }
    </script>
@endsection