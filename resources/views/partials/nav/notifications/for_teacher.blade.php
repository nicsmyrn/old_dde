    socket.on('user-{!! Auth::id() !!}:App\\Events\\PysdeSendNotificationToTeacher', function(data){
       addNotification(data.url,data.type, data.title, data.description);
    });


    socket.on('user-{!! Auth::id() !!}:App\\Events\\MisthodosiaSendNotificationToTeacher', function(data){
       addNotification(data.url,data.type, data.title, data.description);
    });