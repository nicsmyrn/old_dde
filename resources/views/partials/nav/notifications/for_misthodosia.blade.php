    socket.on('misthodosia:App\\Events\\TeacherChangeMisthodosiaNotification', function(data){
        addNotification(data.url,data.type, data.title, data.description);
    });


    socket.on('misthodosia:App\\Events\\TeacherSentNewMisthodosiaDetails', function(data){
        addNotification(data.url,data.type, data.title, data.description);
    });