@if(Auth::check())
	@if(Auth::user()->isRole('ekpaideutikon_thematon')||Auth::user()->isRole('manager'))
		<li {!! \Request::is('kena-pleonasmata*') ? ' class="active"':'' !!}>
			<a href="{!! action('KenaController@indexView') !!}">
				Κενά - Πλεονάσματα
			</a>
		</li>
        <li>
            <a href="{!! action('ExcelController@excelArchives') !!}">
            	Αρχειοθέτηση
            	<i class="fa fa-cloud"></i>
            </a>
        </li>							
	@endif
@endif