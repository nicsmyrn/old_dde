@if(Auth::check())
	@if(Auth::user()->isRole('misthodosia'))
        <li class="dropdown{!! \Request::is('Εργαζόμενοι*') ? ' active':'' !!}">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                Εργαζόμενοι
                <span class="fa fa-caret-down"></span> &MediumSpace;
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="{!! route('Misthodosia::allErgazomenoi') !!}">
                        Όλοι
                    </a>
                </li>
                <li>
                    <a href="{!! route('Misthodosia::notChecked') !!}">
                        για Έλεγχο
                        @if($numberOfTeachersForCheckingMisthodosia > 0)
                            <span class="badge">{!! $numberOfTeachersForCheckingMisthodosia !!}</span>
                        @endif
                    </a>
                </li>
                <li>
                    <a href="{!! route('Misthodosia::allWithAccount') !!}">
                        με λογαριασμό στην Πασιφάη
                    </a>
                </li>
                <li>
                    <a href="{!! route('Misthodosia::allWithOutAccount') !!}">
                        χωρίς Λογαριασμό
                    </a>
                </li>
            </ul>
        </li>

        <li>
            <a href="{!! route('Misthodosia::getMisthodosiaExcel') !!}">
                Εξαγωγή Excel
                <i class="fa fa-cloud"></i>
            </a>
        </li>
@endif
@endif
