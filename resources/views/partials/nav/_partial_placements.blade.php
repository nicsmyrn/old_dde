    <li class="dropdown{!! \Request::is('Τοποθετήσεις*') ? ' active':'' !!}">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="pe-7s-portfolio"></i>
            <p>Τοποθετήσεις <b class="caret"></b></p>
        </a>

        <ul class="dropdown-menu">
            <li>
                <a href="{!! route('Dioikisi::Placements::manualCreate') !!}">
                    Δημιουργία Τοποθέτησης
                </a>
            </li>

            <li>
                <a href="{!! route('Dioikisi::Placements::placementsByPraxi') !!}">
                    Εμφάνιση Πράξεων Τοποθέτησης
                </a>
            </li>
            <li>
                <a href="{!! route('Dioikisi::Placements::allTeachers') !!}">
                    Καθηγητές
                </a>
            </li>
        </ul>
    </li>