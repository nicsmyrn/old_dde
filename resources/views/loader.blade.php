    <div id="loader" class="loaderWrap invisible">
        <div class="bg"></div>
        <div id="spinner">
          <div class="double-bounce1"></div>
          <div class="double-bounce2"></div>
        </div>
        <div id="textLoading">
            Παρακαλώ περιμένετε...
        </div>
    </div>