<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">


/*body{*/

    /*margin-top:20px;*/

    /*background:#eee;*/

/*}*/

* {
    box-sizing: border-box;
}

html, body {
  height: 100%;
  margin: 0;
  padding: 0;
  overflow-x: hidden;
}

.box {

    position: relative;

   margin-top: 20px;
   border-radius: 10px;

    background: #ffffff;

    /*border-top: 3px solid #d2d6de;*/


    width: 100%;

    height: 100%;

    box-shadow: 0 5px 5px rgba(0, 0, 0, 0.1);

    border: 1px solid #838B8B;

}

.box.box-primary {

    /*border-top-color: #3c8dbc;*/

}

.box.box-info {

    border-top-color: #00c0ef;

}

.box.box-danger {

    border-top-color: #dd4b39;

}

.box.box-warning {

    border-top-color: #f39c12;

}

.box.box-success {

    border-top-color: #00a65a;

}

.box.box-default {

    border-top-color: #d2d6de;

}

.box.collapsed-box .box-body, .box.collapsed-box .box-footer {

    display: none;

}

.box .nav-stacked>li {

    border-bottom: 1px solid #f4f4f4;

    margin: 0;

}

.box .nav-stacked>li:last-of-type {

    border-bottom: none;

}

.box.height-control .box-body {

    /*max-height: 300px;*/
    /*max-height: 60%;*/
    /*overflow: auto;*/

}

.box .border-right {

    border-right: 1px solid #f4f4f4;

}

.box .border-left {

    border-left: 1px solid #f4f4f4;

}

.box.box-solid {

    border-top: 0;

}

.box.box-solid>.box-header .btn.btn-default {

    background: transparent;

}

.box.box-solid>.box-header .btn:hover, .box.box-solid>.box-header a:hover {

    background: rgba(0, 0, 0, 0.1);

}

.box.box-solid.box-default {

    border: 1px solid #d2d6de;

}

.box.box-solid.box-default>.box-header {

    color: #444;

    background: #d2d6de;

    background-color: #d2d6de;

}

.box.box-solid.box-default>.box-header a, .box.box-solid.box-default>.box-header .btn {

    color: #444;

}

.box.box-solid.box-primary {

    border: 1px solid #3c8dbc;

}

.box.box-solid.box-primary>.box-header {

    color: #fff;

    background: #3c8dbc;

    background-color: #3c8dbc;

}

.box.box-solid.box-primary>.box-header a, .box.box-solid.box-primary>.box-header .btn {

    color: #fff;

}

.box.box-solid.box-info {

    border: 1px solid #00c0ef;

}

.box.box-solid.box-info>.box-header {

    color: #fff;

    background: #00c0ef;

    background-color: #00c0ef;

}

.box.box-solid.box-info>.box-header a, .box.box-solid.box-info>.box-header .btn {

    color: #fff;

}

.box.box-solid.box-danger {

    border: 1px solid #dd4b39;

}

.box.box-solid.box-danger>.box-header {

    color: #fff;

    background: #dd4b39;

    background-color: #dd4b39;

}

.box.box-solid.box-danger>.box-header a, .box.box-solid.box-danger>.box-header .btn {

    color: #fff;

}

.box.box-solid.box-warning {

    border: 1px solid #f39c12;

}

.box.box-solid.box-warning>.box-header {

    color: #fff;

    background: #f39c12;

    background-color: #f39c12;

}

.box.box-solid.box-warning>.box-header a, .box.box-solid.box-warning>.box-header .btn {

    color: #fff;

}

.box.box-solid.box-success {

    border: 1px solid #00a65a;

}

.box.box-solid.box-success>.box-header {

    color: #fff;

    background: #00a65a;

    background-color: #00a65a;

}

.box.box-solid.box-success>.box-header a, .box.box-solid.box-success>.box-header .btn {

    color: #fff;

}

.box.box-solid>.box-header>.box-tools .btn {

    border: 0;

    box-shadow: none;

}

.box.box-solid[class*='bg']>.box-header {

    color: #fff;

}

.box .box-group>.box {

    margin-bottom: 5px;

}

.box .knob-label {

    text-align: center;

    color: #333;

    font-weight: 100;

    font-size: 12px;

    margin-bottom: 0.3em;

}

.box>.overlay, .overlay-wrapper>.overlay, .box>.loading-img, .overlay-wrapper>.loading-img {

    position: absolute;

    top: 0;

    left: 0;

    width: 100%;

    height: 100%}

.box .overlay, .overlay-wrapper .overlay {

    z-index: 50;

    background: rgba(255, 255, 255, 0.7);

    border-radius: 3px;

}

.box .overlay>.fa, .overlay-wrapper .overlay>.fa {

    position: absolute;

    top: 50%;

    left: 50%;

    margin-left: -15px;

    margin-top: -15px;

    color: #000;

    font-size: 30px;

}

.box .overlay.dark, .overlay-wrapper .overlay.dark {

    background: rgba(0, 0, 0, 0.5);

}

.box-header:before, .box-body:before, .box-footer:before, .box-header:after, .box-body:after, .box-footer:after {

    content: " ";

    display: table;

}

.box-header:after, .box-body:after, .box-footer:after {

    clear: both;

}

.box-header {

    color: #444;

    display: block;

    padding: 15px;


}

.box-header.with-border {

    border-bottom: 1px solid #f4f4f4;

}

.collapsed-box .box-header.with-border {

    border-bottom: none;

}

.box-header>.fa, .box-header>.glyphicon, .box-header>.ion, .box-header .box-title {

    display: inline-block;

    font-size: 18px;

    margin: 0;

    line-height: 1;

}

.box-header>.fa, .box-header>.glyphicon, .box-header>.ion {

    margin-right: 5px;

}

.box-header>.box-tools {

    position: absolute;

    right: 10px;

    top: 5px;

}

.box-header>.box-tools [data-toggle="tooltip"] {

    position: relative;

}

.box-header>.box-tools.pull-right .dropdown-menu {

    right: 0;

    left: auto;

}

.btn-box-tool {

    padding: 5px;

    font-size: 12px;

    background: transparent;

    color: #97a0b3;

}

.open .btn-box-tool, .btn-box-tool:hover {

    color: #606c84;

}

.btn-box-tool.btn:active {

    box-shadow: none;

}

.box-body {

    border-top-left-radius: 0;

    border-top-right-radius: 0;

    border-bottom-right-radius: 3px;

    border-bottom-left-radius: 3px;

    padding: 10px;

}

.no-header .box-body {

    border-top-right-radius: 3px;

    border-top-left-radius: 3px;

}

.box-body>.table {

    margin-bottom: 0;

}

.box-body .fc {

    margin-top: 5px;

}

.box-body .full-width-chart {

    margin: -19px;

}

.box-body.no-padding .full-width-chart {

    margin: -9px;

}

.box-body .box-pane {

    border-top-left-radius: 0;

    border-top-right-radius: 0;

    border-bottom-right-radius: 0;

    border-bottom-left-radius: 3px;

}

.box-body .box-pane-right {

    border-top-left-radius: 0;

    border-top-right-radius: 0;

    border-bottom-right-radius: 3px;

    border-bottom-left-radius: 0;

}

.box-footer {

    border-top-left-radius: 0;

    border-top-right-radius: 0;

    border-bottom-right-radius: 3px;

    border-bottom-left-radius: 3px;

    border-top: 1px solid #f4f4f4;

    position:relative;
    bottom: 0px;

}

.direct-chat .box-body {

    border-bottom-right-radius: 0;

    border-bottom-left-radius: 0;

    position: relative;

    overflow-x: hidden;

    padding: 0;

    /*max-height: 450px;*/
    /*min-height: 300px;*/
    height: calc(100vh - 150px);
}

.direct-chat.chat-pane-open .direct-chat-contacts {

    -webkit-transform: translate(0,  0);

    -ms-transform: translate(0,  0);

    -o-transform: translate(0,  0);

    transform: translate(0,  0);

}

.direct-chat-messages {

    -webkit-transform: translate(0,  0);

    -ms-transform: translate(0,  0);

    -o-transform: translate(0,  0);

    transform: translate(0,  0);

    padding: 10px;

    /*height: 280px;*/
    /*height: 60%;*/

    overflow: auto;

}

.direct-chat-msg, .direct-chat-text {

    display: block;
}

.direct-chat-msg {

    margin-bottom: 10px;
    width: 100%;
}

.direct-chat-msg:before, .direct-chat-msg:after {

    content: " ";

    display: table;

}

.direct-chat-msg:after {

    clear: both;

}

.direct-chat-messages, .direct-chat-contacts {

    -webkit-transition: -webkit-transform .5s ease-in-out;

    -moz-transition: -moz-transform .5s ease-in-out;

    -o-transition: -o-transform .5s ease-in-out;

    transition: transform .5s ease-in-out;

}

.right{
    float: right !important;
    width: 100%;
}

.direct-chat-text {

    border-radius: 5px;

    position: relative;

    padding: 5px 10px;

    background: #d2d6de;

    border: 1px solid #d2d6de;

    margin: 5px 0 0 50px;

    color: #444;

}

.direct-chat-text:after, .direct-chat-text:before {

    position: absolute;

    right: 100%;

    top: 15px;

    border: solid transparent;

    border-right-color: #d2d6de;

    content: ' ';

    height: 0;

    width: 0;

    pointer-events: none;

}

.direct-chat-text:after {

    border-width: 5px;

    margin-top: -5px;

}

.direct-chat-text:before {

    border-width: 6px;

    margin-top: -6px;

}

.right .direct-chat-text {

    margin-right: 50px;

    margin-left: 0;

}

.right .direct-chat-text:after, .right .direct-chat-text:before {

    right: auto;

    left: 100%;

    border-right-color: transparent;

    border-left-color: #d2d6de;

}

.direct-chat-img {

    border-radius: 50%;

    /*float: left;*/

    /*position: relative;*/
    /*left: 2px;*/

    width: 40px;

    height: 40px;
    /*height: 10%;*/
}


.direct-chat-info {

    display: block;

    margin-bottom: 2px;

    font-size: 12px;

}

.info-right{
    text-align: right;
}

.direct-chat-name {

    font-weight: 600;

}


.direct-chat-timestamp {

    color: #999;

}

.direct-chat-contacts-open .direct-chat-contacts {

    -webkit-transform: translate(0,  0);

    -ms-transform: translate(0,  0);

    -o-transform: translate(0,  0);

    transform: translate(0,  0);

}

.direct-chat-contacts {

    -webkit-transform: translate(101%,  0);

    -ms-transform: translate(101%,  0);

    -o-transform: translate(101%,  0);

    transform: translate(101%,  0);

    position: absolute;

    top: 0;

    bottom: 0;

    /*height: 250px;*/
    /*height: 60%;*/

    width: 100%;

    background: #222d32;

    color: #fff;

    overflow: auto;

}

.contacts-list>li {

    border-bottom: 1px solid rgba(0, 0, 0, 0.2);

    padding: 10px;

    margin: 0;

}

.contacts-list>li:before, .contacts-list>li:after {

    content: " ";

    display: table;

}

.contacts-list>li:after {

    clear: both;

}

.contacts-list>li:last-of-type {

    border-bottom: none;

}

.contacts-list-img {

    border-radius: 50%;

    width: 40px;

    float: left;

}

.contacts-list-info {

    margin-left: 45px;

    color: #fff;

}

.contacts-list-name, .contacts-list-status {

    display: block;

}

.contacts-list-name {

    font-weight: 600;

}

.contacts-list-status {

    font-size: 12px;

}

.contacts-list-date {

    color: #aaa;

    font-weight: normal;

}

.contacts-list-msg {

    color: #999;

}

.direct-chat-danger .right>.direct-chat-text {

    background: #dd4b39;

    border-color: #dd4b39;

    color: #fff;

}

.direct-chat-danger .right>.direct-chat-text:after, .direct-chat-danger .right>.direct-chat-text:before {

    border-left-color: #dd4b39;

}

.direct-chat-primary .right>.direct-chat-text {

    background: #3c8dbc;

    border-color: #3c8dbc;

    color: #fff;

}

.direct-chat-primary .right>.direct-chat-text:after, .direct-chat-primary .right>.direct-chat-text:before {

    border-left-color: #3c8dbc;

}

.direct-chat-warning .right>.direct-chat-text {

    background: #f39c12;

    border-color: #f39c12;

    color: #fff;

}

.direct-chat-warning .right>.direct-chat-text:after, .direct-chat-warning .right>.direct-chat-text:before {

    border-left-color: #f39c12;

}

.direct-chat-info .right>.direct-chat-text {

    background: #00c0ef;

    border-color: #00c0ef;

    color: #fff;

}

.direct-chat-info .right>.direct-chat-text:after, .direct-chat-info .right>.direct-chat-text:before {

    border-left-color: #00c0ef;

}

.direct-chat-success .right>.direct-chat-text {

    background: #00a65a;

    border-color: #00a65a;

    color: #fff;

}

.direct-chat-success .right>.direct-chat-text:after, .direct-chat-success .right>.direct-chat-text:before {

    border-left-color: #00a65a;

}


.form_next{
    width: 13%;
    font-size: 18px;
    color: red;
}


.form_input{
    width: 67%;
    padding: 0 20px;
    color: #286090;
    border: 0;
    border-left: 1px solid #838B8B;
    outline: 0;
    font-size: 18pt;
}
.form_send{
    width: 20%;
    border-radius: 0 0 10px;
    background-color: #31a361;
    border: 1px solid #31a361 !important;
    color: #fff
}


.form_next, .form_input, .form_send{
    border-top: 1px solid #838B8B;
    float: left;
    line-height: 45px;

    letter-spacing: normal;
    word-spacing: normal;
    text-transform: none;
    text-indent: 0px;
    text-shadow: none;
    display: inline-block;
    text-align: start;

}

.form_next, .form_send{
    cursor: pointer;
    text-align: center;
    font-size: 9pt;
}

.form_send:hover{
    font-weight: bold;
    background-color: #31a400;
    border: 1px solid #31a361 !important;
}

.form_next:hover{
    font-weight: bold;
}

.message-text-right{
    display: inline-block;
     border-radius: 3px;
     background-color: #3c8dbc;
     border-bottom-left-radius: 1.3em;
     border-top-left-radius: 1.3em;
     color: #fff;
     padding: 6px 12px;
     margin-right: 45px;
}

.message-text-right-first{
     border-top-right-radius: 1.3em;
}
.message-text-right-last{
     border-bottom-right-radius: 1.3em;
}
.message-text-right-only{
     border-top-right-radius: 1.3em;
     border-bottom-right-radius: 1.3em;
}
.div_message_text_right{
   margin: 2px 0;
   text-align:right;
   float: right;
   width: 55%;
}

.message-text-left{
    display: inline-block;
     border-radius: 3px;
     background-color: #d2d6de;
     border-bottom-right-radius: 1.3em;
     border-top-right-radius: 1.3em;
     color: #444;
     padding: 6px 12px;
     /*margin-left: 45px;*/
     /*position: relative;*/
     /*left: 45px;*/
}

.message-text-left-first{
     border-top-left-radius: 1.3em;
}
.message-text-left-last{
     border-bottom-left-radius: 1.3em;
}
.message-text-left-only{
     border-top-left-radius: 1.3em;
     border-bottom-left-radius: 1.3em;
}
.div_message_text_left{
   margin: 5px 0;
   text-align:left;
   position: relative;
   left: 42px;
   width: 55%;
}


@media(max-width:768px) {
    .box{
        margin-top: 0;
        border-radius: 0;
        border: 0;
        box-shadow: 0;
    }
    .box-header{

    }

    .box-body{
        height: calc(100vh - 100px) !important;
    }
    .direct-chat-messages{
    }

    .form_input{
        width: 80% !important;
        font-size: 15pt !important;
    }

    .form_send{
        border-radius: 0 !important;
    }
    .form-send-text {
        display: none
    }

    .form-send-icon {
        display: inline;
    }

    .form_next {
        display: none !important;
    }

    .form_input {
        width: 80%
    }

    .leave-group {
        display: inline-block;
    }
    .button-leave-group{
        display: none;
    }
}


@media(min-width:768px) {
    .form-send-icon {
        display: none !important;
    }

    .leave-group {
        display: none;
    }
}


</style>
</head>
<body>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

<div class="row">

    <div class="col-md-8 col-md-offset-2 col-xs-12 col-sm-10 col-sm-offset-1">

      <div class="box box-primary direct-chat direct-chat-primary">

            <div class="box-header with-border">

              <h3 class="box-title">Σπουδάζω στο: Πανεπιστήμιο Μακεδονίας</h3>

              <div class="box-tools pull-right">

                <button type="button" class="btn btn-danger btn-sm leave-group">
                    Αποχώρηση
                </button>

                <button type="button" class="btn btn-box-tool button-leave-group" title="Αποχώρηση από τη συζήτηση">
                    <i class="fa fa-times"></i>
                </button>

              </div>

            </div>

            <div id="box-body" class="box-body">

              <div class="direct-chat-messages">

                <div class="direct-chat-msg">

                  <div class="direct-chat-info clearfix">

                    <img class="direct-chat-img" src="http://bootdey.com/img/Content/user_3.jpg" alt="Message User Image">

                    <span class="direct-chat-name">Πάντειο Πανεπιστήμιο</span>

                  </div>

                  <div class="div_message_text_left">
                        <span class="message-text-left message-text-left-first">
                            Δοκιμή test
                        </span>
                  </div>
                  <div class="div_message_text_left">
                        <span class="message-text-left">
                            Δοκιμή 2 test. Μια μεγάλη παράγραφο Δοκιμή 2 test. Μια μεγάλη παράγρΔοκιμή 2 test. Μια μεγάλη παράγραφΔοκιμή 2 test. Μια μεγάλη παράγραφοΔοκιμή 2 test. Μια μεγάλη παράγραφο
                        </span>
                  </div>

                  <div class="div_message_text_left">
                        <span class="message-text-left message-text-left-last">
                            ? test
                        </span>
                  </div>
                  <div class="div_message_text_left">
                        <span class="message-text-left message-text-left-only">
                            A
                        </span>
                  </div>


                </div>

                <div class="right direct-chat-msg">

                  <div class="direct-chat-info info-right clearfix">
                        <span class="direct-chat-name">Εγώ</span>
                        <img class="direct-chat-img" src="http://bootdey.com/img/Content/avatar/avatar4.png" alt="Message User Image"><!-- /.direct-chat-img -->
                  </div>


                  <div class="div_message_text_right">
                        <span class="message-text-right message-text-right-first">
                            Δοκιμή dsgsdfgfdg dsafsd fsdfdas fsdf dsagasd asd
                        </span>
                  </div>
                  <div class="div_message_text_right">
                        <span class="message-text-right">
                            Δοκιμή 2 σε ένα μεγάλο κείμενο αι ακόμη πιο μεγάλο
                            Δοκιμή 2 σ
                            Δοκιμή 2 σε ένα μεγάλο κείμενο αι ακόμη πιο μεγάλο
                            Δοκιμή 2 σε ένα μεγάλο κείμενο αι ακόμη πιο μεγάλο
                        </span>
                  </div>

                  <div class="div_message_text_right">
                        <span class="message-text-right message-text-right-last">
                            ?
                        </span>
                  </div>
                  <div class="div_message_text_right">
                        <span class="message-text-right message-text-right-only">
                            A
                        </span>
                  </div>
              </div>

                <div class="direct-chat-msg">

                  <div class="direct-chat-info clearfix">

                    <img class="direct-chat-img" src="http://bootdey.com/img/Content/user_3.jpg" alt="Message User Image">

                    <span class="direct-chat-name">Πάντειο Πανεπιστήμιο</span>

                  </div>

                  <div class="div_message_text_left">
                        <span class="message-text-left message-text-left-first">
                            Δοκιμή test
                        </span>
                  </div>
                  <div class="div_message_text_left">
                        <span class="message-text-left">
                            Δοκιμή 2 test. Μια μεγάλη παράγραφο Δοκιμή 2 test. Μια μεγάλη παράγρΔοκιμή 2 test. Μια μεγάλη παράγραφΔοκιμή 2 test. Μια μεγάλη παράγραφοΔοκιμή 2 test. Μια μεγάλη παράγραφο
                        </span>
                  </div>

                  <div class="div_message_text_left">
                        <span class="message-text-left message-text-left-last">
                            ? test
                        </span>
                  </div>
                  <div class="div_message_text_left">
                        <span class="message-text-left message-text-left-only">
                            A
                        </span>
                  </div>


                </div>

                <div class="right direct-chat-msg">

                  <div class="direct-chat-info info-right clearfix">
                        <span class="direct-chat-name">Εγώ</span>
                        <img class="direct-chat-img" src="http://bootdey.com/img/Content/avatar/avatar4.png" alt="Message User Image"><!-- /.direct-chat-img -->
                  </div>


                  <div class="div_message_text_right">
                        <span class="message-text-right message-text-right-first">
                            Δοκιμή dsgsdfgfdg dsafsd fsdfdas fsdf dsagasd asd
                        </span>
                  </div>
                  <div class="div_message_text_right">
                        <span class="message-text-right">
                            Δοκιμή 2 σε ένα μεγάλο κείμενο αι ακόμη πιο μεγάλο
                            Δοκιμή 2 σ
                            Δοκιμή 2 σε ένα μεγάλο κείμενο αι ακόμη πιο μεγάλο
                            Δοκιμή 2 σε ένα μεγάλο κείμενο αι ακόμη πιο μεγάλο
                        </span>
                  </div>

                  <div class="div_message_text_right">
                        <span class="message-text-right message-text-right-last">
                            ?
                        </span>
                  </div>
                  <div class="div_message_text_right">
                        <span class="message-text-right message-text-right-only">
                            A
                        </span>
                  </div>
              </div>

            </div>


            </div>

            <div class="box-footer">
                <div class="form_next">
                    ΕΠΟΜΕΝΟΣ
                </div>
                <input class="form_input" placeholder="Γράψε το μήνυμά σου..." type="text"/>
                <div class="form_send">
                    <span class="form-send-text">ΑΠΟΣΤΟΛΗ</span>
                    <icon class="fa fa-paper-plane form-send-icon"></icon>
                </div>
            </div>

    </div>

</div>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
</body>
</html>