      <div id="loader"  v-cloak v-bind:class="{
            'loaderWrap' : true,
            'invisible' : hideLoader
        }"
    >
        <div class="bg"></div>
        <div id="spinner">
          <div class="double-bounce1"></div>
          <div class="double-bounce2"></div>
        </div>
        <div id="textLoading">
            Παρακαλώ περιμένετε...
        </div>
    </div>