<!doctype html>
<html>
  <head>
    <title>Socket.IO chat</title>
    <style>
/* Fix user-agent */

* {
  box-sizing: border-box;
}

html {
  font-weight: 300;
  -webkit-font-smoothing: antialiased;
}

html, input {
  font-family:
    "HelveticaNeue-Light",
    "Helvetica Neue Light",
    "Helvetica Neue",
    Helvetica,
    Arial,
    "Lucida Grande",
    sans-serif;
}

html, body {
  height: 100%;
  margin: 0;
  padding: 0;
}

ul {
  list-style: none;
  word-wrap: break-word;
}

/* Pages */

.pages {
  height: 100%;
  margin: 0;
  padding: 0;
  width: 100%;
}

.page {
  height: 100%;
  position: absolute;
  width: 100%;
}

/* Login Page */

.login.page {
  background-color: #000;
}

.login.page .form {
  height: 100px;
  margin-top: -100px;
  position: absolute;

  text-align: center;
  top: 50%;
  width: 100%;
}

.login.page .form .usernameInput {
  background-color: transparent;
  border: none;
  border-bottom: 2px solid #fff;
  outline: none;
  padding-bottom: 15px;
  text-align: center;
  width: 400px;
}

.login.page .title {
  font-size: 200%;
}

.login.page .usernameInput {
  font-size: 200%;
  letter-spacing: 3px;
}

.login.page .title, .login.page .usernameInput {
  color: #fff;
  font-weight: 100;
}


/* Font */

.messages {
  font-size: 150%;
}

.inputMessage {
  font-size: 100%;
}

.log {
  color: gray;
  font-size: 70%;
  margin: 5px;
  text-align: center;
}

/* Messages */

.chatArea {
  height: 100%;
  padding-bottom: 60px;
}

.messages {
  height: 100%;
  margin: 0;
  overflow-y: scroll;
  padding: 10px 20px 10px 20px;
}

.message.typing .messageBody {
  color: gray;
}

.username {
  font-weight: 700;
  overflow: hidden;
  padding-right: 15px;
  text-align: right;
}

/* Input */

.inputMessage {
  border: 10px solid #000;
  bottom: 0;
  height: 60px;
  left: 0;
  outline: none;
  padding-left: 10px;
  position: absolute;
  right: 0;
  width: 100%;
}

#leaveRoom{
    position: absolute;
    top: 5px;
    right: 30px;
}
#chat-loader{

}

    </style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
  </head>
  <body id="chat_page">
        <button v-if="room" id="leavRoom" @click.prevent="leaveChat">
            Αποχώρηση από τη συζήτηση
        </button>
        <button v-if="room" id="leavRoom" @click.prevent="newConverstation">
            Νέα συνομιλία
        </button>
      <ul class="pages">
        <li v-show="loginScreen" class="login page animated" transition="bounce">
          <div class="form">
            <h3 class="title">Δώσε ένα ψευδώνυμο</h3>
            <input
                v-model.trim="username"
                @keydown.enter="loginToChat"
                class="usernameInput"
                type="text"
                maxlength="14"
             />
          </div>
        </li>
        <li v-else class="chat page animated" transition="bounce">
            <div class="chatArea">
                <div v-if="!room" id="loader" class="loaderWrap invisible">
                    <div class="bg"></div>
                    <div id="textLoading">
                        Παρακαλώ περιμένετε...
                    </div>
                    {{--<div id="chat-loader">--}}
                        {{--<img src="images/39.gif"--}}
                    {{--</div>--}}
                </div>
                <ul v-else class="messages">
                    <li v-for="message in all_messages" class="message">
                        <span class="username"
                              v-bind:style="{
                                color : getUsernameColor(message.username)
                              }"
                        >
                            @{{ message.username }}
                        </span>
                        <span class="messageBody">@{{ message.body }}</span>

                    <li v-show="typing.username"
                        class="message typing animated"
                        transition="bounce"
                    >
                        <span class="username"
                              v-bind:style="{
                                color : getUsernameColor(message.username)
                              }"
                        >
                            @{{ peer }}
                        </span>
                        <span class="messageBody">Πληκτρολογεί...</span>
                    </li>
                </ul>
            </div>
            <input
                v-model="message"
                class="inputMessage"
                placeholder="γράψε το κείμενό σου..."
                @keydown.enter="sentMessage"
                @input="checkTyping"
            />
        </li>
      </ul>

      {{--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.min.js"></script>--}}
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.js"></script>
      <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
      <script src="/js/chatting.js"></script>

  </body>
</html>