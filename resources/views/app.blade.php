<!DOCTYPE html>
<html lang="en">

<head>
    @include('meta')

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{!! elixir('css/app.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/libs.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/app2.css') !!}">

    <link href='https://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
	@yield('header.style')

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    	<!--[if lt IE 9]>
    		<script src="/css/html5shiv.min.js"></script>
    		<script src="/css/respond.min.js"></script>
    	<![endif]-->

    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="4377abba-fd5c-4a1f-8e45-8a8bf38293e6" type="text/javascript" async></script>
</head>

<body>
    @include('partials.nav2')

    <div class="wrapper" id="app">
        @yield('loader')

        <div class="container" id="lobby-container">
            @yield('content')
        </div>


        @include('partials.footer')
    </div>


    <script src="{!! elixir('js/libs-default.js') !!}"></script>
    <script src="{!! elixir('js/app2.js') !!}"></script>

        @include('partials.nav2.notifications.notifications')


        @yield('scripts.footer')
        @include('flash')
        @include('partials.google_analytics')


{{--        @include('partials.countdown')--}}
</body>

</html>