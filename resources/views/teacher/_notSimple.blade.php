                <div class="row">
                    <form v-if="requests.length<60" v-on="submit:addRequest">
                        <div class="form-inline">
                            <label class="form-label">Επέλεξε Σχολείο:</label>
                            <input v-model="newRecord" class="form-control" placeholder="προτίμηση..."/>
                            <button class="btn btn-primary">Προσθήκη</button>
                        </div>
                    </form>
                </div> <!-- End of row -->

                <div class="row">
                    <div v-if="requests.length">
                        <h4 class="text-center"><span v-cloak v-if="requests.length">@{{ requests.length }} - Προτιμήσεις</span></h4>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">Α/Α</th>
                                    <th>Σχολείο</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr v-repeat="request: requests">
                                    <td v-cloak class="text-center">@{{ ($index+1) }}</td>
                                    <td v-cloak v-if="!request.edit_cell">@{{ request.school_name }}</td>
                                    <td v-cloak v-if="request.edit_cell"><input v-model="request.school_name" type="text" value="@{{ request }}"> </td>
                                    <td>
                                        <button v-if="!request.edit_cell" alt="Επεξεργασία εγγραφής" v-on="click:editRequest(request)" class="edit"><i class="fa fa-pencil-square-o"></i></button>
                                        <button v-if="!request.edit_cell" alt="Διαγραφή" class="delete" v-on="click:removeRequest(request)"><i class="fa fa-times"></i></button>
                                        <button v-if="request.edit_cell" alt="Αποθήκευση" class="save" v-on="click:saveRequest(request)"><i class="fa fa-floppy-o"></i></button>
                                    </td>
                               </tr>
                            </tbody>
                        </table>

                    </div>
                </div> <!-- End of row -->

                <div class="row">

                </div> <!-- End of row -->