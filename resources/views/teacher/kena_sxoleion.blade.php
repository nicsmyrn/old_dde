@extends('app')

@section('title')
    Κενά Σχολείων
@stop


@section('content')

    <h1 class="page-heading">Κενά Ειδικότητας {!! $e->full_name !!}</h1>

    <div class="col-md-6 col-md-offset-3">
        @if($schools->isEmpty())
            <div class="alert alert-warning col-md-8 col-md-offset-2 text-center" role="alert">
              <strong>ΔΕΝ</strong> υπάρχουν κενά - πλεονάσματα στα Σχολεία για την ειδικότητά σου
            </div>
        @else
            <table class="table table-bordered table-hover">
                <theader>
                    <tr class="active">
                        <th>Σχολείο</th>
                        <th>Ώρες</th>
                    </tr>
                </theader>
                <tbody>
                @foreach($schools as $school)
                    <tr>
                        @if($school->pivot->value < 0)
                            <td class="danger">{!! $school->name !!}</td>
                            <td class="text-center danger">{!! $school->pivot->value !!}</td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    </div>
@stop


