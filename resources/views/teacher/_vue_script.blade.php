    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.16/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.1.16/vue-resource.min.js"></script>
    <script src="https://cdn.jsdelivr.net/vue.router/0.5.2/vue-router.min.js"></script>
    <script src="/js/vue-strap.min.js"></script>

    <script>
        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

        var router = new VueRouter();
        new Vue ({
            el: '#requests',

            data: {
                newRecord : '',
                requests : [
                ],
                description : '',
                a_type : "{!! isset($type)?$type:'' !!}",
                hours_for_request : '',

                reason : null,
                stay : null,
                hasOrganiki : false,
                showHoursRemaining : false,
                showStay : false,
                showReason : true
            },

            ready: function(){
                var unique_request_id  = "{{ isset($unique_id)?$unique_id:0 }}";

                @if($type == 'E2')
                     this.showReason = false;
                     this.showStay = false;
                     this.showHoursRemaining = true;
                 @elseif($type == 'E3' || $type == 'E4'|| $type == 'E6' || $type == 'E5')
                    this.showReason = false;
                    this.showStay = false;
                    this.showHoursRemaining = false;
                @endif

                this.fetchRequest(unique_request_id);

            },

            computed : {
                showSubmit : function(){
                    if (this.requests.length){
                        if (this.reason >= 0  && this.reason <= 2){
                            if (this.reason == 0){
                                this.showHoursRemaining = true;
                                this.showStay = true;
                                if (this.stay == 0 || this.stay == 1){
                                    if (this.hours_for_request >0 && this.hours_for_request <=24){
                                        return true;
                                    }
                                }
                                return false;
                            }else{
                                @if($type == 'E1')
                                    this.showHoursRemaining = false;
                                    this.showStay = false;
                                    this.stay = null;
                                    this.hours_for_request = '';
                                    return true;
                                @elseif($type == 'E2')
                                    this.showHoursRemaining = true;
                                    this.showReason = false;
                                    this.showStay = false;
                                    if (Number.isInteger(parseInt(this.hours_for_request))){
                                        return true;
                                    }
                                    return false;
                                @elseif($type == 'E3' || $type == 'E4' || $type == 'E6' || $type == 'E5')
                                    return true;
                                @endif

                            }
                        }else{
                            @if($type == 'E2')
                                console.log('Error321');
                                this.showHoursRemaining = true;
                                this.showReason = false;
                                this.showStay = false;
                                if (this.hours_for_request.length){
                                    return true;
                                }
                                return false;
                            @elseif($type == 'E3' || $type == 'E4' || $type == 'E6' || $type == 'E5')
                                return true;
                            @else
                                return false;
                            @endif
                        }
                    }
                }
            },
            methods: {
                showSubFields: function(){
                    if (this.reason == 0){
                        this.showHoursRemaining = true;
                        this.showStay = true;
                    }else{
                        this.showHoursRemaining = false;
                        this.showStay = false;
                        this.stay = null;
                        this.hours_for_request = '';
                    }
                },
                addRequest:function(e){
                    e.preventDefault();

                    var school_name = this.newRecord;
                    if(school_name != ''){
                        this.requests.push({
                            school_name : school_name,
                            edit_cell : false,
                            order : null
                        });
                        this.newRecord = '';
                    }
                },

                removeRequest: function(request){
                    this.requests.$remove(request);
                },

                editRequest: function(request){
                    request['edit_cell'] = true;
                },

                saveRequest: function(request){
                    request['edit_cell'] = false;
                },

                saveChanges: function(e){
                    e.preventDefault();

                    this.$http.post('/aj/at/save',{
                        a_type : '{{$type}}',
                        description : this.description,
                        hours_for_request : this.hours_for_request,
                        prefrences : this.requests,
                        stay_to_organiki : this.stay,
                        reason : this.reason,
                        save_request : 'save'
                    })
                    .success(function(post){
                        location.href = '/ΑΙΤΗΣΕΙΣ/ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ';
//                        console.log(post);
                    })
                    .error(function(data, status, request){
                        console.log('error231');
                    });
                },
                saveAndProtocol: function(e){
                    e.preventDefault();

                    this.$http.post('/aj/at/save',{
                        a_type : '{{$type}}',
                        description : this.description,
                        hours_for_request : this.hours_for_request,
                        prefrences : this.requests,
                        stay_to_organiki : this.stay,
                        reason : this.reason,
                        sent_request : 'sent'
                    })
                    .success(function(post){
                        location.href = '/ΑΙΤΗΣΕΙΣ/ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ';
                    })
                    .error(function(data, status, request){
                        console.log('error232');
                    });
                },

                updateChanges : function(e){
                     e.preventDefault();

                     this.$http.post('/aj/at/update/{{isset($unique_id)?$unique_id:0}}',{
                         a_type : '{{$type}}',
                         description : this.description,
                         hours_for_request : this.hours_for_request,
                         prefrences : this.requests,
                         stay_to_organiki : this.stay,
                         reason : this.reason,
                         update_request : 'update'
                     })
                     .success(function(post){
                         location.href = '/ΑΙΤΗΣΕΙΣ/ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ';
                     })
                     .error(function(data, status, request){
                         console.log('error_on_update234');
                     });
                },

                updateAndProtocol : function(e){
                     e.preventDefault();

                     this.$http.post('/aj/at/update/{{isset($unique_id)?$unique_id:0}}',{
                         a_type : '{{$type}}',
                         description : this.description,
                         hours_for_request : this.hours_for_request,
                         prefrences : this.requests,
                         stay_to_organiki : this.stay,
                         reason : this.reason,
                         sent_request : 'sent'
                     })
                     .success(function(post){
                        location.href = '/ΑΙΤΗΣΕΙΣ/ΑΡΧΕΙΟ-ΑΙΤΗΣΕΩΝ';
                     })
                     .error(function(data, status, request){
                         console.log('error_on_sent_update235:'+ data);
                     });
                },

                continueButton : function(e){
                    e.preventDefault();
                    if($('.hiddenRequests').attr('name') == 'save_request'){
                        this.saveChanges(e);
                    }else if($('.hiddenRequests').attr('name') == 'update_request'){
                        this.updateChanges(e);
                    }else if($('.hiddenRequests').attr('name') == 'sent_request'){
                        if($('#continueButton').data('controller') == 'save'){
                            this.saveAndProtocol(e);
                        }else if ($('#continueButton').data('controller') == 'update'){
                            this.updateAndProtocol(e);
                        }
                    }
                },

                fetchRequest: function(unique_request_id){
                    this.$http.get('/aj/at/edit/'+unique_request_id, function(data){
                        this.$set('json_data', data);

                        this.description = this.json_data.description;

                        this.hours_for_request = this.json_data.hours_for_request;

                        this.stay = this.json_data.stay_to_organiki;

                        this.reason = this.json_data.reason;

                        this.a_type = this.json_data.aitisi_type;

//                        this.checkFields();

                        var record = this.requests;

                        for(var i=0; i < this.json_data.prefrences.length; i++){
                            var prefrences_record = this.json_data.prefrences[i];
                            this.requests.push({
                                school_name : prefrences_record.school_name,
                                edit_cell : false,
                                order : null
                            });
                        }

                        if(this.requests.length > 0 && this.hours_for_request > 0){
                            console.log('Hello Nick;');
                            this.showSubmit = true;
                        }else{
                            console.log('abra kata abra: ' + this.requests.length);
                        }

                    });
                }

            }
        })
    </script>