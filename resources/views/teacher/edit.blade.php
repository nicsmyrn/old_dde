@extends('app')

@section('header.style')
    <meta id="token" name="token" value="{!! csrf_token() !!}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <link href="/css/aitisi.css" rel="stylesheet"/>

    <link href="/css/loader.css" rel="stylesheet"/>
@endsection

@section('loader')
    @include('loader')
@endsection

@section('content')
<div id="requests">
    <h2 id="request_header_h2" class="text-center">
        <span class="label label-default">Φόρμα Αίτησης {!! $request->aitisi_type !!}</span>
        @if(in_array($request->aitisi_type,['E1','E2','E3','E4','E5','E6','E7']))
            @include('teacher._notSimpleHeader',['action'=> 'update_request','controller'=>'update'])
        @endif
    </h2>

    <div class="row">
        <div class="col-md-4">
            @include('teacher._profile')
        </div>

        @if($request->aitisi_type == 'ΑΠΛΗ')
            <div class="col-md-5">
                 @include('errors.list')

                {!! Form::model($request,['method'=>'PATCH', 'class'=>'form', 'id'=> 'aitisiForm']) !!}
                    @include('teacher._simple_form',['action'=> 'update_request'])
                    @include('teacher._modalConditions')
                {!! Form::close() !!}
            </div>
        @elseif(in_array($request->aitisi_type,['E1','E2','E3','E4','E5','E6','E7']))
            <div class="col-md-5">
                @include('errors.list')
                @include('teacher._notSimple')
                @include('teacher._modalConditions')
            </div> <!-- End of div col-md-6 -->

            <div class="col-md-3">
                @include('teacher._aitisiRight')
            </div>
        @endif
    </div>
</div>
@endsection

@section('scripts.footer')
    <script src="/js/global.js"></script>

    @if(in_array($request->aitisi_type,['E1','E2','E3','E4','E5','E6','E7']))
        @include('teacher._vue_script', ['type'=> $request->aitisi_type])
    @endif
    @include('teacher._modal_script', ['type'=> $request->aitisi_type])
@endsection