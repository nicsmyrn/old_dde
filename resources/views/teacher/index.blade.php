@extends('app')

@section('title')
    {!! $title !!}
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')
    <h1 class="page-heading">{!! $title !!}</h1>

    @if(!$requests->isEmpty())
        @include('teacher.tableIndex', ['type'=> $type])
    @else
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει καμία Αίτηση</div>
                </div>
            </div>
        </div>
    @endif
@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>

        (function($){
            var o = $({});
            $.subscribe = function(){
                o.on.apply(o, arguments);
            };
            $.unsubscribe = function(){
                o.off.apply(o, arguments);
            };
            $.publish = function(){
                o.trigger.apply (o, arguments);
            };
        }(jQuery));

        $(document).ready(function() {
            var table = $('#requests').DataTable({
                    "order": [[ 1, "desc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 4,5 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });

            $('button[data-click=delete]').on('click', swalAlertDelete);

        });

        function swalAlertDelete(e){
            e.preventDefault();
            var form = $(this).closest('form');
            var data = form.serialize();

            var method = form.find('input[name="_method"]').val()|| 'POST';

            swal({
                title: "Προσοχή!",
                text: "Η διαγραφή της αίτησης είναι οριστική",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ναι, διέγραψέ την",
                cancelButtonText: "Ακύρωση",
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                closeOnCancel: true },
                function(){
                    $.ajax({
                        type : method,
                        url : form.prop('action'),
                        data: data,
                        success : function(data){
                            location.reload(true);
                        },
                        error: function (request, status, error) {
                           $('body').html(request.responseText);
                       }
                    });
                }
            );
        }


    </script>
@endsection


