
            <div class="panel panel-info">
                <div class="panel-heading">
                    {!! $title !!}
                </div>
                <div class="panel-body  text-left">
                    <div class="checkbox text-center">
                        <label style="font-size: 1.5em">
                            <input id="checkAttachments" type="checkbox" v-model="attachmentsCheckBox">
                            <span class="cr"><i class="cr-icon fa fa-check"></i></span>
                            Επιθυμώ να αποστείλω ηλεκτρονικά τα δικαιολογητικά
                        </label>
                    </div>

                    <div class="col-md-7">
                        <div class="form-group" v-cloak v-show="attachmentsCheckBox">
                            <div class="input-group input-file" name="attachment1">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-choose" type="button">Επιλογή αρχείου</button>
                                </span>
                                <input type="text" class="form-control" placeholder='Επιλέξτε το 1ο δικαιολογητικό' />
                                <span class="input-group-btn">
                                     <button class="btn btn-warning btn-reset" type="button">Άκυρο</button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group" v-cloak v-show="attachmentsCheckBox">
                            <div class="input-group input-file" name="attachment2">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-choose" type="button">Επιλογή αρχείου</button>
                                </span>
                                <input type="text" class="form-control" placeholder='Επιλέξτε το 2ο δικαιολογητικό' />
                                <span class="input-group-btn">
                                     <button class="btn btn-warning btn-reset" type="button">Άκυρο</button>
                                </span>
                            </div>
                        </div>

                        <div class="form-group" v-cloak v-show="attachmentsCheckBox">
                            <div class="input-group input-file" name="attachment3">
                                <span class="input-group-btn">
                                    <button class="btn btn-default btn-choose" type="button">Επιλογή αρχείου</button>
                                </span>
                                <input type="text" class="form-control" placeholder='Επιλέξτε το 3ο δικαιολογητικό' />
                                <span class="input-group-btn">
                                     <button class="btn btn-warning btn-reset" type="button">Άκυρο</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="alert alert-warning" v-cloak v-show="attachmentsCheckBox">
                              <p>
                                  <strong>Προσοχή!</strong> Τα δικαιολογητικά θα πρέπει να είναι:
                              </p>
                              <p>
                                    <ul>
                                        <li>τύπου PDF (Acrobat Reader) ή εικόνας σε μορφή jpg, png</li>
                                        <li>το καθένα να έχει το πολύ <strong>2</strong> ΜΒ (megabyte) μέγεθος</li>
                                        <li>όλα τα έγγραφα που αποστέλλετε <strong>ΔΕΝ ΑΠΟΘΗΚΕΥΟΝΤΑΙ</strong>. Εκτυπώνονται και ενημερώνεται το Τμήμα Γ' Προσωπικού</li>
                                    </ul>
                              </p>
                        </div>
                    </div>

                </div>
            </div>
