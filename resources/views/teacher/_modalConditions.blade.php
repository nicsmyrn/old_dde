 <div class="modal fade" id="ConditionsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
        <strong><h4 class="modal-title" id="exampleModalLabel"></h4>Υποβολή Αίτησης</strong>
      </div>
      <div class="modal-body">
        <ul>
            <li>
                Για το σωστό υπολογισμό των μορίων όσοι καθηγητές δηλώνουν:
                    <ol>
                        <li>Εντοπιότητα</li>
                        <li>Συνυπηρέτηση</li>
                        <li>Οικογενειακή Κατάσταση</li>
                        <li>Αριθμό παιδιών</li>
                    </ol>
                        θα πρέπει μέχρι τη λήξη των αιτήσεων να προσκομίσουν και τα απαραίτητα <strong>δικαιολογητικά </strong> τους.
            </li>
            <li>
                Τα στοιχεία που δηλώνονται στην αίτηση απόσπασης έχουν την έννοια της <strong>υπεύθυνης δήλωσης</strong> ,
                με βάση τα οριζόμενα στο άρθρο του Ν. 1599/86 και <u>ψευδής δήλωση συνεπάγεται κυρώσεις</u>
                που προβλέπονται από την παράγραφο 6 του άρθρου 22 του ίδιου νόμου
            </li>
            <li>
                Με την αποστολή η αίτηση καταχωρείται με αύξων αριθμό και στέλνεται αντίγραφο στη γραμματεία
                του ΠΥΣΔΕ καθώς και στον ενδιαφερόμενο
            </li>
        </ul>
        <p>Αν συμφωνείτε με τους παραπάνω όρους πατήστε Συνέχεια</p>
      </div>
      <div class="modal-footer">
        <button type="button" id="closeConditionsModal" class="btn btn-default" data-dismiss="modal">Κλείσιμο</button>
        <input type="submit" value="Συνέχεια" class="btn btn-primary" id="continueButton" v-on="click: continueButton">
      </div>
    </div>
  </div>
</div>