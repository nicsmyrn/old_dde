    <div v-if="requests.length" style="margin-top: 10px">
        <form method="post" id="aitisiForm">
            <button type="button" value="{!! $action !!}" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#ConditionsModal">
              Προσωρινή αποθήκευση
            </button>
            <button v-if="showSubmit" type="button" value="sent_request" data-controller="{!! $controller !!}" class="btn btn-success btn-lg" data-toggle="modal" data-target="#ConditionsModal">
              Αποστολή
            </button>
        </form>
    </div>