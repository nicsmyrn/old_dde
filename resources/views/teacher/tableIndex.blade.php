<table id="requests" class="table table-bordered table-hover" cellspacing="0" width="100%">
    <thead>
        <tr class="active">
            <th class="text-center">Κωδικός Αίτησης</th>
            <th class="text-center">Ημ/νια</th>
            <th class="text-center">Αρ. Αίτησης</th>
            <th class="text-center">Είδος</th>
            <th class="text-center">Παρατηρήσεις</th>
            <th class="text-center">Ενέργειες</th>
        </tr>
    </thead>

    <tbody>
        @foreach($requests as $request)
        <tr>
            <td class="text-center">
                @can('edit_request')
                    <a href="#">{!! $request->unique_id !!}</a>
                @else
                    @if($request->protocol_number == null)
                        @if($type == 'organiki')
                            @if($request->aitisi_type == 'Βελτιώση - Οριστική τοποθέτηση')
                                <a href="{!! route('Aitisi::OrganikiType', 'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης') !!}">{!! $request->unique_id!!}</a>
                            @else
                                <a href="{!! route('Aitisi::OrganikiType', 'Αίτηση-Οργανικής-Υπεράριθμου') !!}">{!! $request->unique_id!!}</a>
                            @endif
                        @elseif($type == 'simple')
                            <a href="{!! route('Aitisi::edit',[$request->unique_id]) !!}">{!! $request->unique_id!!}</a>
                        @endif
                    @else
                        <span>{!! $request->unique_id!!}</span>
                    @endif
                @endcan
            </td>
            <td class="text-center">{!! $request->date_req !!}</td>
            <td class="text-center">
                @if($request->protocol_number != null)
                    {!! $request->protocol_name !!}
                @endif
            </td>
            @if(isset($request->aitisi_type))
                <td class="text-center">{!! $request->aitisi_type !!}</td>
            @else
                <td class="text-center">Unknown</td>
            @endif
            <td class="text-center">
                @if($request->protocol_number == null)
                    <span class="label label-warning">Σε εκκρεμότητα</span>
                @else
                    <span class="label label-success">Ολοκληρωμένη</span>
                @endif
            </td>
            <td class="text-center">
                @if($request->protocol_number == null)
                    {!! Form::open(['method'=>'POST', 'action'=> ['RequestController@deleteRowRequest', $request->unique_id, $type]]) !!}
                        <button type="button" title="Διαγραφή" data-click="delete" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i> </button>
                        {!!Form::hidden('unique_id', $request->unique_id)!!}
                    {!! Form::close() !!}
                @else
                    <a href="{!! route('Aitisi::downloadPDF', [$request->unique_id]) !!}"><img style="width: 30px; height: 30px" src="/img/download_icon.jpg"/> </a>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>
</table>