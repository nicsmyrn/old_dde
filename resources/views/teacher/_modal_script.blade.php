    <script>
        $(document).ready(function() {
            $('#ConditionsModal').on('show.bs.modal', showBsModal);
            $('#closeConditionsModal').on('click', buttonCloseModalDetails);
            $('#continueButton').on('click', buttonSaveModalDetails);

        }); // end of ready function

        function showBsModal(event){
            var button = $(event.relatedTarget);
            console.log(button.val());
            $('.hiddenRequests').remove();
          var modal = $(this);
            modal.find('#continueButton')
//                .attr('name',button.val())
                .attr('data-controller', button.data('controller')); // τα στοιχεία που προστήθεντε με το κλείσιμο ΔΕΝ σβήνουν
            $('<input>').attr('type', 'hidden')
                        .attr('name',button.val())
                        .addClass('hiddenRequests')
                        .val(button.val())
                        .appendTo('#aitisiForm');
        } //end of function showBsModal

        function buttonCloseModalDetails(){
            $.swalAlert({
                title : 'Ειδοποίηση!',
                body : 'Η αίτηση μπορεί να γίνει αποδεχτή μόνο αποδέχοντας τους όρους',
                level : 'warning'
            });
        }

        function buttonSaveModalDetails(e){
            e.preventDefault();
            $('#loader').removeClass('invisible')
                .addClass('loading');
            @if($type == 'ΑΠΛΗ')
                $('#aitisiForm').submit();
            @endif
        }

    </script>