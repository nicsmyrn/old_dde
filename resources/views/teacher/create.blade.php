@extends('app')

@section('header.style')
    <meta id="token" name="token" value="{!! csrf_token() !!}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <style>
            [v-cloak] {
              display: none;
            }
    </style>
@endsection

@section('loader')
    @include('loader')
@endsection

@section('content')
<div id="requests">
        <div class="row">
            <div class="alert alert-danger alert-dismissible col-md-6 col-md-offset-3" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <ul>
                    <li><h4>Για να σταλεί η Αίτηση στο ΠΥΣΔΕ θα πρέπει να: </h4>
                        <ol>
                            <li>συμφωνήσετε με τους όρους υποβολής αίτησης</li>
                            <li>πάρετε αριθμό αίτησης ο οποίος εμφανίζεται με ένα μήνυμα επιτυχίας αποστολής</li>
                            <li>λάβετε E-mail επιβεβαίωσης</li>
                        </ol>
                    <li><h5>Η προσωρινή αποθήκευση είναι για τη διευκόλυνση του εκπαιδευτικού. Σε αυτήν την περίπτωση το ΠΥΣΔΕ ΔΕΝ έχει λάβει γνώση της αίτησης</h5></li>
                </ul>
            </div>
            <div class="alert alert-warning alert-dismissible col-md-6 col-md-offset-3" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                @if($type == 'E1')
                    Συμπληρώνεται <strong><u>ΜΟΝΟ</u></strong> από εκπαιδευτικούς που είναι:
                    <ol>
                        <li>Λειτουργικά Υπεράριθμοι</li>
                        <li>Διάθεση ΠΥΣΔΕ</li>
                        <li>Μετατιθέμενοι</li>
                    </ol>
                @elseif($type == 'E2')
                    Συμπληρώνεται από εκπαιδευτικούς που θέλουν να συμπληρώσουν το ωράριό τους εντός ΠΥΣΔΕ
                @elseif($type == 'E3')
                    Συμπληρώνεται από εκπαιδευτικούς που επιθυμούν να αποσπαστούν εντός ΠΥΣΔΕ
                @elseif($type == 'E4')
                    Συμπληρώνεται από εκπαιδευτικούς που είναι <u>αποσπασμένοι από άλλο ΠΥΣΔΕ</u>
                @elseif($type == 'E5')
                    Συμπληρώνεται από τους διορισμένους αναπληρωτές - ωρομίσθιους εκπαιδευτικούς.
                @elseif($type == 'E6')
                    Συμπληρώνεται από εκπαιδευτικούς που θέλουν να συμπληρώσουν το ωράριό τους στην πρωτοβάθμια εκπαίδευση
                @elseif($type == 'ΑΠΛΗ')
                    Συμπληρώνεται από <strong>ΟΛΟΥΣ</strong> τους εκπαιδευτικούς που ενδιαφέρονται να υποβάλλουν οποιοδήποτε αίτημα ή ένσταση προς το ΠΥΣΔΕ
                @endif
            </div>
        </div>

    <h2 id="request_header_h2" class="text-center">
        <span class="label label-default">Φόρμα Αίτησης {{$type}}</span>
        @if(in_array($type,['E1','E2','E3','E4','E5','E6','E7']))
                @include('teacher._notSimpleHeader',['action'=>'save_request','controller'=>'save'])
        @endif
    </h2>

    <div class="row">
        <div class="col-md-4">
            @include('teacher._profile')
        </div>

        @if($type == 'ΑΠΛΗ')
            <div class="col-md-5">
                @include('errors.list')

                {!! Form::model($request = new \App\RequestTeacher(),['method'=>'POST', 'class'=>'form', 'action'=>'RequestController@store','id'=>'aitisiForm']) !!}
                    @include('teacher._simple_form',['action'=>'save_request'])
                    {!! Form::hidden('a_type',$type) !!}
                    @include('teacher._modalConditions')
                {!! Form::close() !!}
            </div>
        @elseif(in_array($type,['E1','E2','E3','E4','E5','E6','E7']))
            <div class="col-md-5">
                @include('errors.list')
                @include('teacher._notSimple',['action'=>'save_request'])
                @include('teacher._modalConditions')
            </div> <!-- End of div col-md-5 -->

            <div class="col-md-3">
                @include('teacher._aitisiRight')
            </div>
        @endif
    </div>
</div>

@endsection

@section('scripts.footer')
    <script src="/js/global.js"></script>


    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>

    @if(in_array($type,['E1','E2','E3','E4','E5','E6','E7']))
        @include('teacher._vue_script')
    @endif
    @include('teacher._modal_script', ['type'=> $type])
@endsection