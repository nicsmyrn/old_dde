<!DOCTYPE html>
<html lang="gr">
    @include('teacher.placements.template_head')
<body>
    @foreach($pages as $index=>$praxi)
        @include('teacher.placements.template3', [
            'new_placements' => $praxi->new_placements,
            'old_placements' => [],
            'base_placement'=> $praxi->last_BASE_placement,
            'myschool'      => $praxi->teacher,
        ])
        <div class="print_date">
            Ημερομηνία εκτύπωσης: {!! \Carbon\Carbon::now()->format('d-m-Y H:i:s') !!}
        </div>
        @if($pages->count() != ($index + 1))
            <div class="page-break"></div>
        @endif
    @endforeach

</body>
</html>
