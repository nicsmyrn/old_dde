    <table>
        <tr>
            <td class="logo_cell_name">
                <img src="img/ethnosimo.jpg" id="logo">
                <div>ΕΛΛΗΝΙΚΗ ΔΗΜΟΚΡΑΤΙΑ</div>
                <div>ΥΠΟΥΡΓΕΙΟ ΠΑΙΔΕΙΑΣ, ΕΡΕΥΝΑΣ ΚΑΙ ΘΡΗΣΚΕΥΜΑΤΩΝ</div>
                <div>ΠΕΡ. Δ/ΝΣΗ Α/ΘΜΙΑΣ & Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΚΡΗΤΗΣ</div>
                <div>Δ/ΝΣΗ Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΧΑΝΙΩΝ</div>
                <div>ΤΜΗΜΑ ΔΙΟΙΚΗΤΙΚΩΝ ΘΕΜΑΤΩΝ</div>
                <hr class="line">
                <div>Ταχ. Διεύθυνση: Γκερόλα 48 β</div>
                <div>Πληροφορίες: Σμυρναίος Νικόλαος</div>
                <div>Τηλέφωνο: 2821047152 Fax: 2821047137</div>
                <div>E-mail: mail&#64;dide.chan.sch.gr</div>
            </td>
            <td id="protocol">
                <div>ΧΑΝΙΑ {!! $praxi->dde_protocol_date !!}</div>
                <div>ΑΡΙΘΜ ΠΡΩΤ: Φ.10.3/{!! $praxi->dde_protocol !!} </div>
                <br>
                <div id="div_table_destinations">
                    <div class="div_row">
                        <div class="div_table_cell_title">
                            Προς:
                        </div>
                        <div class="div_table_cell">
                            <span class="span_bold">
                                {!! $teacher->user->full_name !!}
                             </span>
                             του
                             <span class="span_bold">
                                {!!  $teacher->middle_name !!}
                             </span> εκπαιδευτικό κλ. {!! $teacher->klados_name . '('.$teacher->eidikotita_name. ')' !!}
                        </div>
                    </div>
                    <div class="div_row">
                        <div class="div_table_cell_title">
                            Κοιν.:
                        </div>
                        <div class="div_table_cell">
                            <?php $counter = 1; ?>
                            @if($teacher->teacherable->organiki != 1000)
                                <div> {!! $counter . '. ' . $teacher->teacherable->organiki_slug !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @endif

                            @if($base_placement != null)
                                <div> {!! $counter . '. ' . $base_placement->to !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @endif
                            @foreach($new_placements as $placement)
                                <div> {!! $counter . '. ' .  $placement->to !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @endforeach
                            @foreach($old_placements as $placement)
                                <div> {!! $counter . '. ' .  $placement->to !!}</div>
                                <?php $counter = $counter + 1; ?>
                            @endforeach
                        </div>
                    </div>
                </div>
            </td>
        </tr>
    </table>

    <!-- TODO: ###### subject ####### -->
    <div id="subject">
            Θέμα: &laquo;Ανακοίνωση {!! $praxi->title !!}&raquo;
    </div>

    <div id="apofasis">
        <div class="line_apofasi">
            <p>Σας ανακοινώνουμε ότι με την υπ' αριθμ. Φ.10.3/{!! $praxi->dde_protocol !!}/{!! $praxi->dde_protocol_date !!}
            απόφαση του Δ/ντή της Δ/θμιας Εκπ/σης Χανίων (ΑΔΑ:<a class="url_links" href="{!! $praxi->url !!}">{!! $praxi->ada !!}</a>) και
            σύμφωνα με την υπ' αριθμ. {!! $praxi->decision_number !!} / {!! $praxi->decision_date !!}
            Πράξη του ΠΥΣΔΕ Χανίων,
            @if($base_placement != null)
                @if($praxi->decision_number == $base_placement->praxi->decision_number)
                    τοποθετηθήκατε προσωρινά {!! $praxi->base_aitisis['text'] !!} στο: </p>
                    <p>
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>
                        για
                        <span class="span_bold">
                            {!! $base_placement->hours !!} ώρες
                        </span>
                        @if($base_placement->days >=1 && $base_placement->days <= 5)
                            <span>
                                / {!! $base_placement->days !!}
                                @if($base_placement->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                            </span>
                        @endif
                        @if($base_placement->description != '')
                            [{!! $base_placement->description !!}]
                        @endif
                    </p>
                @endif
                @if($teacher->teacherable->organiki == 1000)
                    <br>
                    με προσωρινή τοποθέτηση στο
                    <span class="span_bold">
                        {!! $base_placement->to !!}
                    </span>
                    {{--(Πράξη Δ/ντή ΔΔΕ Χανίων {!! $base_placement->praxi->decision_number .'/'.$base_placement->praxi->decision_date !!})--}}
                    <br>
                @endif
            @else
                - με οργανική στο
                <span class="span_bold">
                    {!! $teacher->teacherable->organiki_name !!}
                </span> -
            @endif

            @if(!$new_placements->isEmpty())
                @if($base_placement != null)
                    @if($praxi->decision_number == $base_placement->praxi->decision_number)
                        <p> και
                    @endif
                @endif
                    διατεθήκατε
                    @if($praxi->aitisis['value'] != 'true&false')
                        {!! $praxi->aitisis['text'] !!}
                    @endif
                    @if(!$praxi->placements->where('placements_type', "8")->isEmpty())
                        στην Π/θμια Εκπαίδευση για
                        <span class="span_bold">
                            {!! $new_placements->first()->hours !!} ώρες
                        </span>
                        @if($new_placements->first()->days >=1 && $new_placements->first()->days <= 5)
                            <span>
                                / {!! $new_placements->first()->days !!}
                                @if($new_placements->first()->days == 1)
                                    ημέρα
                                @else
                                    ημέρες
                                @endif
                            </span>
                        @endif

                    @else
                        για συμπλήρωση του υποχρεωτικού σας ωραρίου στο: </p>
                        <ol>
                            @foreach($new_placements as $new_placement)
                                <li>
                                    <span class="span_bold">
                                        {!! $new_placement->to !!}
                                    </span>
                                    για
                                    <span class="span_bold">
                                        {!! $new_placement->hours !!} ώρες
                                    </span>
                                    @if($new_placement->days >=1 && $new_placement->days <= 5)
                                        <span>
                                            / {!! $new_placement->days !!}
                                            @if($new_placement->days == 1)
                                                ημέρα
                                            @else
                                                ημέρες
                                            @endif
                                        </span>
                                    @endif
                                    @if($praxi->aitisis['value'] == 'true&false')
                                        @if($new_placement->me_aitisi)
                                            με αίτηση
                                        @else
                                            χωρίς αίτηση
                                        @endif
                                    @endif
                                    @if($new_placement->description != '')
                                        [{!! $new_placement->description !!}]
                                    @endif
                                </li>
                            @endforeach
                        </ol>
                    @endif

            @endif
        </div>

        <div class="line_apofasi">
            μέχρι τη λήξη του διδακτικού έτους 2016 - 2017 (30/6/2016)
        </div>

        @if(!$old_placements->isEmpty())
            @if($praxi->decision_number != $base_placement->praxi->decision_number)
                <div class="line_apofasi">
                    Σε προηγούμενη απόφαση (ΑΔΑ:<a href="{!! $base_placement->praxi->url !!}" class="url_links">{!! $base_placement->praxi->ada !!}</a>) έχετε τοποθετηθεί προσωρινά {!! $praxi->base_aitisis['text'] !!} στο:
                    <p>
                        <span class="span_bold">
                            {!! $base_placement->to !!}
                        </span>
                        για
                        <span class="span_bold">
                            {!! $base_placement->hours !!} ώρες
                        </span>
                    @if($base_placement->days >=1 && $base_placement->days <= 5)
                        <span>
                            / {!! $base_placement->days !!}
                            @if($base_placement->days == 1)
                                ημέρα
                            @else
                                ημέρες
                            @endif
                        </span>
                    @endif
                    </p>
                </div>
            @endif
            <div class="line_apofasi">
                @if($praxi->decision_number != $base_placement->praxi->decision_number)
                    και
                @endif
                διατεθήκατε για τη συμπλήρωση του ωραρίου σας:
                <ol>
                    @foreach($old_placements as $old_placement)
                        <li>
                            <span class="span_bold">
                                {!! $old_placement->to !!}
                            </span>
                            για
                            <span class="span_bold">
                                {!! $old_placement->hours !!} ώρες
                            </span>
                            @if($old_placement->days >=1 && $old_placement->days <= 5)
                                <span>
                                    / {!! $old_placement->days !!}
                                    @if($old_placement->days == 1)
                                        ημέρα
                                    @else
                                        ημέρες
                                    @endif
                                </span>
                            @endif
                            @if($praxi->old_aitisis['value'] == 'true&false')
                                @if($old_placement->me_aitisi)
                                    με αίτηση
                                @else
                                    χωρίς αίτηση
                                @endif
                            @endif
                            ΑΔΑ:
                            <a href="{!! $old_placement->praxi->url !!}" class="url_links">
                                {!! $old_placement->praxi->ada !!}
                            </a>
                            @if($old_placement->description != '')
                                [{!! $old_placement->description !!}]
                            @endif
                        </li>
                    @endforeach
                </ol>
            </div>
        @endif

        <div class="line_apofasi">
            κατόπιν αυτού παρακαλούμε να παρουσιαστείτε στο Σχολείο
            τοποθέτησης σας, να αναλάβετε υπηρεσία και να αποστείλετε άμεσα την πράξη ανάληψή σας
        </div>
    </div>

    <table id="footer">
        <tr>
            <td id="left_footer">

            </td>
            <td id="center_footer"></td>
            <td id="right_footer">
                <div>Ο Διευθυντής της Δ.Δ.Ε. Χανίων</div>
                <div style="height: 55px"></div>
                <div style="height: 55px"></div>
                <div></div>
                <div>Ζερβάκης Γ. Στυλιανός</div>
                <div>Διοίκησης Επιχειρήσεων</div>
            </td>
        </tr>
    </table>