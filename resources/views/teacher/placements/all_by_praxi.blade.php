<!DOCTYPE html>
<html lang="gr">
    @include('teacher.placements.template_head')
<body>
    @foreach($teachers as $teacher)
        @include('teacher.placements.template', ['plc' => $teacher->placements])

        @if($teachers->last()->id != $teacher->id)
            <div class="page-break"></div>
        @endif
    @endforeach
</body>
</html>
