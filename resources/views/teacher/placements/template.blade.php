    <table>
        <tr>
            <td class="logo_cell_name">
                <img src="img/ethnosimo.jpg" id="logo">
                <div>ΕΛΛΗΝΙΚΗ ΔΗΜΟΚΡΑΤΙΑ</div>
                <div>ΥΠΟΥΡΓΕΙΟ ΠΑΙΔΕΙΑΣ, ΕΡΕΥΝΑΣ ΚΑΙ ΘΡΗΣΚΕΥΜΑΤΩΝ</div>
                <div>ΠΕΡ. Δ/ΝΣΗ Α/ΘΜΙΑΣ & Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΚΡΗΤΗΣ</div>
                <div>Δ/ΝΣΗ Β/ΘΜΙΑΣ ΕΚΠ/ΣΗΣ ΧΑΝΙΩΝ</div>
                <div>ΤΜΗΜΑ ΔΙΟΙΚΗΤΙΚΩΝ ΘΕΜΑΤΩΝ</div>
                <hr class="line">
            </td>
            <td></td>
            <td id="protocol">
                <div>ΧΑΝΙΑ {!! $praxi->dde_protocol_date !!}</div>
                <div>ΑΡΙΘΜ ΠΡΩΤ: Φ.10.3/{!! $praxi->dde_protocol !!}</div>
            </td>
        </tr>
        <tr>
            <td>
                <div>Ταχ. Διεύθυνση: Γκερόλα 48 β</div>
                <div>Πληροφορίες: Σμυρναίος Νικόλαος</div>
                <div>Τηλέφωνο: 2821047152 Fax: 2821047137</div>
                <div>E-mail: mail&#64;dide.chan.sch.gr</div>
            </td>
            <td></td>
            <td>
                <div id="title"><div>ΑΠΟΦΑΣΗ</div></div>
            </td>
        </tr>
    </table>

    <!-- TODO: ###### subject ####### -->
    <div id="subject">
        @if($teacher->teacherable->organiki == 2000)
            @if(!$plc->where('placements_type', "1")->isEmpty())
                Θέμα: &laquo;Τοποθέτηση αποσπασμένου Εκπαιδευτικού από άλλο ΠΥΣΔΕ&raquo;
            @elseif(!$plc->where('placements_type', "5")->isEmpty())
                Θέμα: &laquo;Τροποποίηση συμπλήρωσης ωραρίου αποσπασμένου Εκπαιδευτικού από άλλο ΠΥΣΔΕ&raquo;
            @else
                Θέμα: &laquo;Τροποποίηση τοποθέτησης αποσπασμένου Εκπαιδευτικού από άλλο ΠΥΣΔΕ&raquo;
            @endif
        @elseif(!$plc->where('placements_type', "1")->isEmpty())
            Θέμα: &laquo;Προσωρινή τοποθέτηση εκπαιδευτικών&raquo;
        @elseif(!$plc->where('placements_type', "2")->isEmpty())
            Θέμα: &laquo;Απόσπαση εκπαιδευτικού&raquo;
        @elseif(!$plc->where('placements_type', "6")->isEmpty())
            Θέμα: &laquo;Τροποποίηση προσωρινής τοποθέτησης εκπαιδευτικού&raquo;
        @elseif(!$plc->where('placements_type', "5")->isEmpty())
            Θέμα: &laquo;Τροποποίηση συμπλήρωσης ωραρίου εκπαιδευτικού&raquo;
        @elseif(!$plc->where('placements_type', "4")->isEmpty())
            Θέμα: &laquo;Ολική διάθεση εκπαιδευτικού&raquo;
        @else
            Θέμα: &laquo;Συμπλήρωση ωραρίου εκπαιδευτικού&raquo;
        @endif
    </div>

    <div id="apofasis">
        <div>
            <div>Έχοντας υπόψη:</div>
            <ol id="nomoi">
                <li>
                    Τις διατάξεις της παρ. 14 του άρθρου 14, παρ. 5 του άρθρου 54
                    @if($teacher->teacherable->organiki == 2000)
                        και του άρθρου 16 περ. Γ
                    @endif
                    του Ν. 1566/85 (ΦΕΚ 176/85τ.Γ') &laquo;Δομή και λειτουργία της Π/θμιας και Δ/θμιας Εκπ/σης και άλλες διατάξεις&raquo;
                </li>
                @if($teacher->teacherable->organiki == 2000)
                    <li>
                        Το άρθρο 31 του Ν. 3848/2010 (ΦΕΚ 71τ'Α) &laquo;Αναβάθμιση του ρόλου του εκπαιδευτικού
                        - καθιέρωση κανόνων αξιολόγησης και αξιοκρατίας στην εκπαίδευση και λοιπές διατάξεις&raquo;
                    </li>
                @endif
                <li>
                    Την αριθμ. Φ.353.1/324/105657/Δ1/8-10-2002 (ΦΕΚ 1340 τ Β') Υ.Α. άρθρο 18 &laquo;Καθορισμός των
                    ειδικότερων καθηκόντων και αρμοδιοτήτων των Προϊσταμένων των Περιφερειακών Υπηρεσιών
                    Α/θμιας και Β/θμιας εκπ/σης...&raquo;
                </li>
                <li>
                    Τις υπ' αριθμ. ΣΤ5/56/2000 και ΣΤ5/12/2001 Αποφάσεις ΥΠΕΠΘ, με τις οποίες μεταβιβάζεται το δικαίωμα
                    υπογραφής &laquo;με εντολή Υπουργού&raquo; στους Προϊσταμένους των Δ/νσεων και Γραφείων Α/θμιας και Β/θμιας Εκπ/σης
                </li>
                @if($teacher->teacherable->organiki == 2000)
                    <li>
                        Την αριθμ. 130462/Ε2/5-8-2016 Υ.Α. &laquo;Αποσπάσεις εκπαιδευτικών Δ.Ε. από ΠΥΣΔΕ σε ΠΥΣΔΕ
                        για το διδακτικό έτος 2016-2017&raquo;
                    </li>
                    <li>
                        Την αριθμ. 160490/Ε2/29-9-2016 Υ.Α. &laquo;Αποσπάσεις ανακλήσεις και τροποποιήσεις αποσπάσεων
                        εκπαιδευτικών Δ.Ε. από ΠΥΣΔΕ σε ΠΥΣΔΕ για το διδακτικό έτος 2016-2017&raquo;
                    </li>
                @endif
                    <li>
                        Τις ανάγκες των Σχολείων σε διδακτικό προσωπικό
                    </li>
                @if(in_array($plc->first()->placements_type,[1,3]))
                    <li>
                        Την αίτηση @if($teacher->user->sex) του ενδιαφερόμενου @else της ενδιαφερόμενης @endif εκπαιδευτικού
                    </li>
                @endif
                <li>
                    Την υπ' αριθμ. <b>{!! $praxi->decision_number !!} / {!! $praxi->decision_date !!}</b> Πράξη του ΠΥΣΔΕ Χανίων
                </li>
            </ol>
        </div>
        <div id="apofasi">αποφασίζουμε</div>
        <div class="line_apofasi">
            <!-- TODO: ###### κύρια αιτία τοποθέτησης ####### -->
            @if(!$plc->where('placements_type', "1")->isEmpty())
                Να τοποθετηθεί προσωρινά @if($teacher->user->sex) ο @else η @endif  εκπαιδευτικός
            @elseif(!$plc->where('placements_type', "2")->isEmpty())
                Να αποσπαστεί {!! $praxi->aitisis['text'] !!} @if($teacher->user->sex) του ο @else της η @endif εκπαιδευτικός
            @elseif(!$plc->where('placements_type', "6")->isEmpty())
                Να τοποθετηθεί προσωρινά @if($teacher->user->sex) ο @else η @endif εκπαιδευτικός
            @elseif(!$plc->where('placements_type', "5")->isEmpty())
                Να διατεθεί για συμπλήρωση ωραρίου {!! $praxi->aitisis['text'] !!} @if($teacher->user->sex) του ο @else της η @endif εκπαιδευτικός
            @else
                Να διατεθεί για συμπλήρωση ωραρίου {!! $praxi->aitisis['text'] !!} @if($teacher->user->sex) του ο @else της η @endif εκπαιδευτικός
            @endif
        </div>
        <div class="line_apofasi">
            <span class="span_bold">
                {!! $teacher->user->full_name !!}
            </span>
            του
            <span class="span_bold">
                {!! $teacher->middle_name !!}
            </span>
            κλάδου <span class="span_bold"> {!! $teacher->klados_name !!} </span> {!! $teacher->eidikotita_name !!}
        </div>
        <div class="line_apofasi">
            <!-- TODO: ###### οργανική θέση ####### -->
            @if($teacher->teacherable->organiki != 1000 && $teacher->teacherable->organiki != 2000)
                @if(!$plc->where('placements_type', "3")->isEmpty())
                    <div class="line_apofasi">
                        που υπηρετεί στο σχολείο : <span class="span_bold">{!! $teacher->teacherable->organiki_name !!}</span>
                    </div>
                @endif
                @if(!$plc->where('placements_type', "2")->isEmpty())
                    <div class="line_apofasi">
                        που ανήκει οργανικά στο Σχολείο: : <span class="span_bold">{!! $teacher->teacherable->organiki_name !!}</span>
                    </div>
                @endif
                @if(!$plc->where('placements_type', "5")->isEmpty())
                    <div class="line_apofasi">
                        που υπηρετεί στο σχολείο : <span class="span_bold">{!! $teacher->teacherable->organiki_name !!}</span>
                    </div>
                @endif
            @endif

            <!-- TODO: Σχολείο προσωρινής τοποθέτησης -->
            @if(!$plc->where('placements_type', "1")->isEmpty())
                    <div class="line_apofasi">
                        στο Σχολείο : <span class="span_bold">{!! $plc->where('placements_type', "1")->first()->to !!}</span>
                        @if($plc->first()->hours > 0)
                            για <span class="span_bold">{!! $plc->where('placements_type', "1")->first()->hours !!}</span>  ώρες
                            @if($plc->where('placements_type', "1")->first()->days >= 1 && $plc->where('placements_type', "1")->first()->days <= 5)
                                <span>
                                    {!! $plc->where('placements_type', "1")->first()->days !!}
                                    @if($plc->where('placements_type', "1")->first()->days == 1)
                                        (ημέρα)
                                    @else
                                        (ημέρες)
                                    @endif
                                </span>
                                    @if($plc->where('placements_type', "1")->first()->description != '')
                                          <span>
                                            [{!! $plc->where('placements_type', "1")->first()->description !!}]
                                          </span>
                                    @endif
                            @endif
                        @endif
                    </div>
                    @if($teacher->teacherable->organiki != 1000 && $teacher->teacherable->organiki != 2000)
                        @if($plc->where('placements_type', "1")->first()->hours >=12 && $plc->where('placements_type', "1")->first()->hours < $teacher->teacherable->orario)
                            <div class="line_apofasi">
                                και τις υπόλοιπες ημέρες διατίθεται για συμπλήρωση ωραρίου στην οργανική του <span class="span_bold">({!! $teacher->teacherable->organiki_name !!})</span>
                            </div>
                        @endif
                    @endif
            @elseif(!$plc->where('placements_type', "2")->isEmpty())
                    <div class="line_apofasi">
                        στο Σχολείο : <span class="span_bold">{!! $plc->where('placements_type', "2")->first()->to !!}</span>
                        @if($plc->first()->hours > 0)
                            για <span class="span_bold">{!! $plc->where('placements_type', "2")->first()->hours !!}</span>  ώρες
                            @if($plc->where('placements_type', "2")->first()->days >= 1 && $plc->where('placements_type', "2")->first()->days <= 5)
                                <span>
                                    {!! $plc->where('placements_type', "2")->first()->days !!}
                                    @if($plc->where('placements_type', "2")->first()->days == 1)
                                        (ημέρα)
                                    @else
                                        (ημέρες)
                                    @endif
                                </span>
                            @endif
                        @endif
                                            @if($plc->where('placements_type', "2")->first()->description != '')
                                                [{!! $plc->where('placements_type', "2")->first()->description !!}]
                                            @endif
                    </div>
            @elseif(!$plc->where('placements_type', "6")->isEmpty())
                    <div class="line_apofasi">
                        στο Σχολείο : <span class="span_bold">{!! $plc->where('placements_type', "6")->first()->to !!}</span>
                        @if($plc->first()->hours > 0)
                            για <span class="span_bold">{!! $plc->where('placements_type', "6")->first()->hours !!}</span>  ώρες
                            @if($plc->where('placements_type', "6")->first()->days >= 1 && $plc->where('placements_type', "6")->first()->days <= 5)
                                <span>
                                    {!! $plc->where('placements_type', "6")->first()->days !!}
                                    @if($plc->where('placements_type', "6")->first()->days == 1)
                                        (ημέρα)
                                    @else
                                        (ημέρες)
                                    @endif
                                </span>
                            @endif
                        @endif
                                            @if($plc->where('placements_type', "6")->first()->description != '')
                                                [{!! $plc->where('placements_type', "6")->first()->description !!}]
                                            @endif
                    </div>

            @endif
            <!-- ###################      E N D  ######################### -->
        </div>

        <!-- TODO: Σχολείο ή Σχολεία για συμπλήρωση ωραρίου -->

        @if(count($plc) > 1)
            <div class="line_apofasi">
                @if(!$plc->where('placements_type', "1")->isEmpty() || !$plc->where('placements_type', "6")->isEmpty())
                    <div>
                        και να διατεθεί για συμπλήρωση ωραρίου {!! $praxi->aitisis['text'] !!}@if($teacher->user->sex) του @else της @endif  στα Σχολεία:
                    </div>
                @else
                    <div>στα Σχολεία: </div>
                @endif
                <ol>
                    @foreach($plc as $k=>$v)
                        @if($v->placements_type != "1" && $v->placements_type != "6")
                            <li class="school_name">
                                <span class="schools">{!! $v->to !!}
                                @if($praxi->aitisis['value'] == 'true&false')
                                    &#40;
                                    @if($v->me_aitisi)
                                        με αίτηση
                                    @else
                                        χωρίς αίτηση
                                    @endif
                                    &#41;
                                @endif
                                </span>
                                <span>{!! $v->hours !!} ώρες</span>
                                @if($v->days >=1 && $v->days <=5)
                                    <span>
                                        {!! $v->days !!}
                                        @if($v->days == 1)
                                            (ημέρα)
                                        @else
                                            (ημέρες)
                                        @endif
                                     </span>
                                @endif
                                <span>
                                    @if($v->description != '')
                                        [{!! $v->description !!}]
                                    @endif
                                </span>
                            </li>
                        @endif
                    @endforeach
                </ol>
            </div>
        @elseif(count($plc) == 1 && (!$plc->where('placements_type', "3")->isEmpty() || (!$plc->where('placements_type', "5")->isEmpty())))
            <div class="line_apofasi">
                <div>
                    στο Σχολείο:
                    <span class="span_bold">{!! $plc->first()->to !!}</span>

                    <span> για <span class="span_bold">{!! $plc->first()->hours !!}</span> ώρες</span>
                    @if($plc->first()->days >=1 && $plc->first()->days <=5)
                        <span>
                            {!! $plc->first()->days !!}
                            @if($plc->first()->days == 1)
                                (ημέρα)
                            @else
                                (ημέρες)
                            @endif
                         </span>
                    @endif
                    <span>
                        @if($plc->first()->description != '')
                            [{!! $plc->first()->description !!}]
                        @endif
                    </span>
                </div>
        @endif
        <div class="end_of_apofasi">
            μέχρι τη λήξη του διδακτικού έτους 2016 - 2017 (30/6/2017)
        </div>
    </div>

    <table id="footer">
        <tr>
            <td id="left_footer">
                <div style="font-weight: 600;">Κοινοποίηση:</div>
                <div> @if($teacher->user->sex) Ενδιαφερόμενος @else Ενδιαφερόμενη @endif</div>
                @if($teacher->teacherable->organiki != 1000)
                    <div>{!! $teacher->teacherable->organiki_slug !!} </div>
                @endif
                @foreach($plc as $placement)
                    <div>{!! $placement->to !!}</div>
                @endforeach
            </td>
            <td id="center_footer"></td>
            <td id="right_footer">
                <div>Ε.Υ.</div>
                <div>Ο Διευθυντής της Δ.Δ.Ε. Χανίων</div>
                <div style="height: 55px"></div>
                <div style="height: 55px"></div>
                <div></div>
                <div>Ζερβάκης Γ. Στυλιανός</div>
                <div>Διοίκησης Επιχειρήσεων</div>
            </td>
        </tr>
    </table>