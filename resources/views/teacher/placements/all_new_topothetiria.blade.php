<!DOCTYPE html>
<html lang="gr">
    @include('teacher.placements.template_head')
<body>
    @foreach($praxeis as $praxi)
        @include('teacher.placements.template4', [
            'new_placements' => $praxi->new_placements,
            'old_placements' => $praxi->old_placements,
            'base_placement'=> $praxi->last_BASE_placement
        ])
        <div class="print_date">
            Ημερομηνία εκτύπωσης: {!! \Carbon\Carbon::now()->format('d-m-Y H:i:s') !!}
        </div>
        @if($praxeis->last()->id != $praxi->id)
            <div class="page-break"></div>
        @endif
    @endforeach

</body>
</html>
