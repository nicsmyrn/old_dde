<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            /*font-family: "DejaVu Serif";*/
            margin: 0 40px 0 40px;
            font-size: 13pt;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        #signature{
            background-image: url("http://srv-dide.chan.sch.gr/hidden/$2y$10$M7lTqmCckaPkU0uEd87RpulMkrDY95L2V8onvVodtDfxtQr24vUCG.jpg");
        }

        #logo{
            height: 80px;
            width:80px;
        }
        .logo_cell_name{
            text-align: center;
            width: 400px;
        }
        .line{
            width: 200px;
        }

        #subject{
            font-size: 15pt;
            margin-top: 20px;
            margin-bottom: 20px;
            margin-left: 20px;
            font-weight: bold;
        }

        #footer{
            position: absolute;
            bottom: 280px;
            font-size: 15pt;
        }
        #footer #left_footer{
            width: 30%;
        }
        #footer #right_footer{
            text-align: center;
            width: 40%
        }
        #protocol{
            padding-left: 100px;
        }

        #title{
            text-align: center;
            font-size: 15pt;
            letter-spacing: 4px;
            font-weight: bold;
        }
        #title div{
            text-decoration: none;
            border: 3px solid #000000;
            width: 170px;
            padding: 15px;
        }
        #apofasis{
            font-size: 11pt;
        }

        #apofasis #apofasi{
            text-align: center;
            font-size: 15pt;
            font-weight: bold;
            letter-spacing: 5px;
        }
        .span_bold{
            font-weight: bold;
            font-size: 15pt;
        }

        .schools{
            margin-right: 100px
        }

        .line_apofasi{
            padding: 0 0 12px 0;
            margin-right: 20px;
            font-size: 15pt;
        }

        .end_of_apofasi{
            font-size: 13pt;
            padding: 2px 0 5px 0;
        }

        .school_name{
            margin: 0 0 5px 0;;
        }

        .page-break {
            page-break-after: always;
        }

        #div_table_destinations{
            display: table;
            width: 100%;
        }

        .div_row{
            display: table-row;
            padding-bottom: 20px;
        }

        .div_table_cell_title{
            font-weight: bold;
            display: table-cell;
        }

        .div_table_cell{
            display: table-cell;
        }

        .url_links{
            text-decoration: none;
            color: red;
        }

        .print_date{
            position: absolute;
            bottom:0;
            left: 0;
            font-size: 8pt;
        }
    </style>
</head>