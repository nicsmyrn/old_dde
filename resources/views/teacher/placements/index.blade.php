@extends('app')

@section('title')
    Τοποθετήσεις Καθηγητή {!! $myschool->full_name !!}
@stop

@section('header.style')
<style>
    .table-user-information > tbody > tr {
        border-top: 1px solid rgb(221, 221, 221);
    }

    .table-user-information > tbody > tr:first-child {
        border-top: 0;
    }


    .table-user-information > tbody > tr > td {
        border-top: 0;
    }
</style>
@endsection

@section('content')
    <h1 class="page-heading">
        <a href="{!! URL::previous() !!}" class="btn btn-primary btn-s">
            Επιστροφή
        </a>
        Τοποθετήσεις από το ΠΥΣΔΕ
        {{--@if(Auth::user()->userable_type == 'App\Teacher')--}}
            {{--<a target="_blank"--}}
                {{--href="{!! route('allTeacherPlacements') !!}"--}}
                {{--class="btn btn-success btn-lg"--}}
            {{-->--}}
        {{--@elseif(Auth::user()->userable_type == 'App\School')--}}
            {{--<a target="_blank"--}}
                {{--href="{!! action('SchoolPlacementsController@viewTeacherPlacementsPDF', ['teacher_id' => $myschool->afm]) !!}"--}}
                {{--class="btn btn-success btn-lg"--}}
            {{-->--}}
        {{--@else--}}
            {{--<a target="_blank"--}}
                {{--href="#"--}}
                {{--class="btn btn-success btn-lg"--}}
            {{-->--}}
        {{--@endif--}}
            {{--Όλα τα τοποθετήρια--}}
        {{--</a>--}}
    </h1>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-info">
              <div class="panel-heading">
                <h3 class="panel-title">{!! $myschool->full_name !!}</h3>
              </div>
              <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-user-information">
                            <tbody>
                                <tr>
                                    <td>ΑΜ:</td>
                                    <td><b>{!! $myschool->am!!}</b></td>
                                </tr>
                                <tr>
                                    <td>Πατρώνυμο:</td>
                                    <td><b>{!! $myschool->middle_name!!}</b></td>
                                </tr>
                                <tr>
                                    <td>Κλάδος:</td>
                                    <td><b>{!! $myschool->eidikotita !!}</b></td>
                                </tr>

                                 <tr>
                                    <td>Οργανική</td>
                                    <td><b>{!! $myschool->organiki_name!!}</b></td>
                                </tr>
                                <tr>
                                    <td>Υποχρεωτικό Ωράριο:</td>
                                    <td><b>{!! $myschool->ypoxreotiko!!}</b></td>
                                </tr>
                                @if(Auth::user()->role->hasPermission('user_teacher_admin'))
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <button type="button"
                                                    data-toggle="modal"
                                                    data-target="#teacherDescModal"
                                                    data-id="{!! $myschool->afm !!}"
                                                    class="btn btn-warning btn-xs"
                                                    title="Επεξεργασία Καθηγητή"
                                            >
                                                <i class="glyphicon glyphicon-edit"></i>
                                                Επεξεργασία Προφίλ
                                            </button>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-9">
            <p>
                <span style="background-color: #dff0d8;padding: 5px; border-radius: 5px;margin: 5px"> Προσωρινή τοποθέτηση</span>
                <span style="background-color: #f2dede;padding: 5px; border-radius: 5px;margin: 5px"> Ανάκληση τοποθέτησης</span>
                <span style="background-color: #f1f1f1;padding: 5px; border-radius: 5px;margin: 5px"> Συμπλήρωση ωραρίου</span>
                <span style="background-color: #d9edf7;padding: 5px; border-radius: 5px;margin: 5px"> Απόσπαση με αίτηση</span>
                <span style="background-color: #f8edba;padding: 5px; border-radius: 5px;margin: 5px"> Ολική Διάθεση</span>
            </p>

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation">
                    <a href="#old_placements" aria-controls="old_placements" role="tab" data-toggle="tab" style="background-color: #f2f2f2;;">
                        Υπόλοιπα Έτη
                    </a>
                </li>
              <li role="presentation" class="active">
                    <a href="#current_placements" aria-controls="current_placements" role="tab" data-toggle="tab" style="background-color: #ccffcc">
                        {!! $current_year !!}
                    </a>
              </li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane fade" id="old_placements" style="background-color:#f2f2f2">
                    @if($old_placements->isEmpty())
                        <br><br>
                        <div class="col-md-8 col-md-offset-2 alert alert-info text-center" role="alert">
                            <strong>ΔΕΝ</strong> υπάρχουν τοποθετήσεις για τα υπόλοιπα έτη
                        </div>
                        <br><br><br><br><br><br><br>
                    @else
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Α/Α</th>
                                    <th class="text-center">Πράξη ΠΥΣΔΕ</th>
                                    <th class="text-center">Στο</th>
                                    <th class="text-center">Τύπος</th>
                                    <th class="text-center">Ημέρες</th>
                                    <th class="text-center">Ώρες</th>
                                    <th class="text-center">Με αίτηση</th>
                                    <th class="text-center">Παρατηρήσεις</th>
                                    @if(Auth::user()->role->hasPermission('manage_placements'))
                                        <th class="text-center">Ενέργειες</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1 ?>
                                @foreach($old_placements as $placement)
                                    @if($placement->deleted_at != null)
                                        <tr class="danger">
                                            <td class="text-center"><s>{{$index}}</s></td>
                                            <td class="text-center">
                                                @if($placement->praxi->praxi_type == 13)
                                                <s><a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                </a></s>
                                                @else
                                                <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                    <s>{{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}</s>
                                                </a>
                                                @endif
                                            </td>
                                            <td><s>{{$placement->to}}</s></td>
                                            <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                            <td class="text-center"><s>{!! $placement->days >0 ? $placement->days : '' !!}</s></td>
                                            <td class="text-center"><s>{!! $placement->hours >0 ? $placement->hours : '' !!}</s></td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif
                                            <td class="text-center"><s>{{$placement->description}}</s></td>
                                            @if(Auth::user()->role->hasPermission('manage_placements'))
                                                <td class="text-center">
                                                    {!! Form::open(['method'=>'POST', 'action'=>['PlacementsController@postPermanentDelete']]) !!}

                                                       <a href="{!! route('Dioikisi::Placements::update', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </a>

                                                        <a href="{!! route('Dioikisi::Placements::restorePlacement', [$placement->id]) !!}" title="Ενεργοποίηση τοποθέτησης" class="btn btn-success btn-xs">
                                                           <i class="fa fa-undo" aria-hidden="true"></i>
                                                        </a>

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            {!!Form::hidden('placement_id', $placement->id)!!}
                                                        {!! Form::close() !!}
                                                </td>
                                            @endif
                                        </tr>

                                    @else
                                        @if(isset($base_placement) && $base_placement->id == $placement->id)
                                            <tr class="success">
                                        @else
                                            @if(in_array($placement->placements_type, [3,5]))
                                                <tr class="active">
                                            @elseif(in_array($placement->placements_type, [2]))
                                                <tr class="info">
                                            @elseif(in_array($placement->placements_type, [4]))
                                                <tr class="warning">
                                            @else
                                                <tr>
                                            @endif
                                        @endif
                                                <td class="text-center">{{$index}}</td>
                                                <td class="text-center">
                                                    @if($placement->praxi->praxi_type == 13)
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                    </a>
                                                    @else
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        {{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}
                                                    </a>
                                                    @endif
                                                </td>
                                                <td>{{$placement->to}}</td>
                                                <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                                <td class="text-center">{!! $placement->days >0 ? $placement->days : '' !!}</td>
                                                <td class="text-center">{!! $placement->hours >0 ? $placement->hours : '' !!}</td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif

                                                <td class="text-center">{{$placement->description}}</td>
                                                @if(Auth::user()->role->hasPermission('manage_placements'))
                                                    <td class="text-center">
                                                        {!! Form::open(['method'=>'POST', 'action'=>['PlacementsController@postPermanentDelete']]) !!}

                                                            <a href="{!! route('Dioikisi::Placements::update', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </a>

                                                            <a href="{!! route('Dioikisi::Placements::softDelete', [$placement->id]) !!}" title="Ανάκληση τοποθέτησης" class="btn btn-primary btn-xs">
                                                               <i class="fa fa-undo" aria-hidden="true"></i>
                                                            </a>

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            {!!Form::hidden('placement_id', $placement->id)!!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                @endif
                                        </tr>
                                    @endif
                                    <?php $index = $index + 1 ?>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="8">
                                        @can('manage_placements')
                                                <a target="_blank"
                                                href="{!! route('Dioikisi::Placements::allPlacementsForYears', ['teacher_id' => $myschool->afm, 'year_name' => 'ΠΑΛΑΙΟΤΕΡΑ-ΕΤΗ']) !!}"
                                                    class="btn btn-warning btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @endcan
                                        @can('view_placements_by_dde')
                                                <a target="_blank"
                                                href="{!! route('Dioikisi::Placements::allPlacementsForYears', ['teacher_id' => $myschool->afm, 'year_name' => 'ΠΑΛΑΙΟΤΕΡΑ-ΕΤΗ']) !!}"
                                                    class="btn btn-warning btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @endcan
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    @endif
                </div>
                <div role="tabpanel" class="tab-pane fade in active" id="current_placements" style="background-color:#ccffcc">
                    @if($current_placements->isEmpty())
                                <br><br>
                                <div class="col-md-8 col-md-offset-2 alert alert-warning text-center" role="alert">
                                    <strong>ΔΕΝ</strong> υπάρχουν τοποθετήσεις για το τρέχον έτος
                                </div>
                                <br><br><br><br><br><br><br>
                    @else
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Α/Α</th>
                                    <th class="text-center">Πράξη ΠΥΣΔΕ</th>
                                    <th class="text-center">Στο</th>
                                    <th class="text-center">Τύπος</th>
                                    <th class="text-center">Ημέρες</th>
                                    <th class="text-center">Ώρες</th>
                                    <th class="text-center">Με αίτηση</th>
                                    <th class="text-center">Παρατηρήσεις</th>
                                    @if(Auth::user()->role->hasPermission('manage_placements'))
                                        <th class="text-center">Ενέργειες</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                <?php $index = 1 ?>
                                @foreach($current_placements as $placement)
                                    @if($placement->deleted_at != null)
                                        <tr class="danger">
                                            <td class="text-center"><s>{{$index}}</s></td>
                                            <td class="text-center">
                                                @if($placement->praxi->praxi_type == 13)
                                                <s><a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                </a></s>
                                                @else
                                                <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                    <s>{{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}</s>
                                                </a>
                                                @endif
                                            </td>
                                            <td><s>{{$placement->to}}</s></td>
                                            <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                            <td class="text-center"><s>{!! $placement->days >0 ? $placement->days : '' !!}</s></td>
                                            <td class="text-center"><s>{!! $placement->hours >0 ? $placement->hours : '' !!}</s></td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif
                                            <td class="text-center"><s>{{$placement->description}}</s></td>
                                            @if(Auth::user()->role->hasPermission('manage_placements'))
                                                <td class="text-center">
                                                    {!! Form::open(['method'=>'POST', 'action'=>['PlacementsController@postPermanentDelete']]) !!}

                                                       <a href="{!! route('Dioikisi::Placements::update', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                            <i class="glyphicon glyphicon-edit"></i>
                                                        </a>

                                                        <a href="{!! route('Dioikisi::Placements::restorePlacement', [$placement->id]) !!}" title="Ενεργοποίηση τοποθέτησης" class="btn btn-success btn-xs">
                                                           <i class="fa fa-undo" aria-hidden="true"></i>
                                                        </a>

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            {!!Form::hidden('placement_id', $placement->id)!!}
                                                        {!! Form::close() !!}
                                                </td>
                                            @endif
                                        </tr>

                                    @else
                                        @if(isset($base_placement) && $base_placement->id == $placement->id)
                                            <tr class="success">
                                        @else
                                            @if(in_array($placement->placements_type, [3,5]))
                                                <tr class="active">
                                            @elseif(in_array($placement->placements_type, [2]))
                                                <tr class="info">
                                            @elseif(in_array($placement->placements_type, [4]))
                                                <tr class="warning">
                                            @else
                                                <tr>
                                            @endif
                                        @endif
                                                <td class="text-center">{{$index}}</td>
                                                <td class="text-center">
                                                    @if($placement->praxi->praxi_type == 13)
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        ΑΠΟΦΑΣΗ ΠΕΡΙΦΕΙΑΡΧΗ ΕΝΑΡΞΗ ΑΠΟ: {!! $placement->starts_at !!} εως {!! $placement->ends_at !!}
                                                    </a>
                                                    @else
                                                    <a target="_blank" href="{!! $placement->praxi->url !!}">
                                                        {{$placement->praxi->decision_number}}η/{{$placement->praxi->decision_date}}
                                                    </a>
                                                    @endif
                                                </td>
                                                <td>{{$placement->to}}</td>
                                                <td>{!! Config::get('requests.placements_type')[$placement->placements_type] !!}</td>
                                                <td class="text-center">{!! $placement->days >0 ? $placement->days : '' !!}</td>
                                                <td class="text-center">{!! $placement->hours >0 ? $placement->hours : '' !!}</td>
                                                @if($placement->me_aitisi)
                                                    <td class="text-center" style="color: #008000">
                                                        <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    </td>
                                                @else
                                                    <td class="text-center" style="color: red">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </td>
                                                @endif

                                                <td class="text-center">{{$placement->description}}</td>
                                                @if(Auth::user()->role->hasPermission('manage_placements'))
                                                    <td class="text-center">
                                                        {!! Form::open(['method'=>'POST', 'action'=>['PlacementsController@postPermanentDelete']]) !!}

                                                            <a href="{!! action('PlacementsController@updatePlacement', [$myschool->afm, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                                                <i class="glyphicon glyphicon-edit"></i>
                                                            </a>

                                                            <a href="{!! route('Dioikisi::Placements::softDelete', [$placement->id]) !!}" title="Ανάκληση τοποθέτησης" class="btn btn-primary btn-xs">
                                                               <i class="fa fa-undo" aria-hidden="true"></i>
                                                            </a>

                                                            <button type="button" title="Διαγραφή Τοποθέτησης" data-click="delete" class="btn btn-danger btn-xs">
                                                                <i class="glyphicon glyphicon-trash"></i>
                                                            </button>
                                                            {!!Form::hidden('placement_id', $placement->id)!!}
                                                        {!! Form::close() !!}
                                                    </td>
                                                @endif
                                        </tr>
                                    @endif
                                    <?php $index = $index + 1 ?>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4" class="text-right"><h3 style="font-weight: bold;">Σύνολο Ωρών:</h3></td>
                                    <td colspan="2" class="text-left"><h3 style="padding-left: 15px;color: #0000ff;font-weight: bold">{!! $total !!}</h3></td>
                                    <td colspan="2">
                                        @can('manage_placements')
                                                <a target="_blank"
                                                    href="{!! route('Dioikisi::Placements::allPlacementsForYears', ['teacher_id' => $myschool->afm, 'year_name' => $current_year]) !!}"
                                                    class="btn btn-success btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @endcan
                                        @can('view_placements_by_dde')
                                                <a target="_blank"
                                                    href="{!! route('Dioikisi::Placements::allPlacementsForYears', ['teacher_id' => $myschool->afm, 'year_name' => $current_year]) !!}"
                                                    class="btn btn-success btn-lg"
                                                >
                                                    Τοποθετήρια
                                                </a>
                                        @endcan
                                    </td>
                                </tr>

                            </tfoot>
                        </table>
                    @endif
                </div>
            </div>

        </div>
    </div>
    @if(Auth::user()->role->hasPermission('user_teacher_admin'))
        @include('pysde.user.teacherLittleModal', $schools)
    @endif
@stop

@section('scripts.footer')
    @if(Auth::user()->role->hasPermission('user_teacher_admin'))

        <script src="/js/global.js"></script>

        <script>
            $(document).ready(function() {
                $('button[data-click=delete]').on('click', swalAlertDelete);

                $('#closeTeacherDetailsModal').on('click', buttonCloseModalDetails);
                $('#saveTeacherDetailsModal').on('click', buttonSaveModalDetails);
            });

            function swalAlertDelete(e){
                e.preventDefault();
                var form = $(this).closest('form');
                var data = form.serialize();

                var method = form.find('input[name="_method"]').val()|| 'POST';

                swal({
                    title: "Προσοχή!",
                    text: "Η διαγραφή της τοποθέτησης είναι οριστική",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ναι, διέγραψέ την",
                    cancelButtonText: "Ακύρωση",
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    closeOnCancel: true },
                    function(){
                        console.log('deleted');
                            $.ajax({
                                type : method,
                                url : form.prop('action'),
                                data: data,
                                success : function(data){
                                    location.reload(true);
                                },
                                error: function (request, status, error) {
                                   $('body').html(request.responseText);
                               }
                            });
                    }
                );
            }

            function buttonSaveModalDetails(){
                var form = $('#formTeacherDetails');
                var modal = $('#teacherDescModal');
                $.ajax({
                    url : "{!! route('ajaxTeacherModalLittleDataSave') !!}",
                    type : 'post',
                    data : form.serialize(),
                    success : function(data){
                        location.reload(true);
                    },
                    error: function(data){
                        $.swalAlertValidatorError(data)
                    }
                });
            }

            function buttonCloseModalDetails(){
                $.swalAlert({
                    title : 'Ειδοποίηση!',
                    body : 'Kαμία αλλαγή στα στοιχεία του καθηγητή',
                    level : 'warning'
                });
            }
        </script>
    @endif
@endsection


