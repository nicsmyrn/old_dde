    <!-- Subject Form Input -->
    <div class="form-group" id="">
        {!! Form::label('subject', 'Θέμα:', ['class'=>'control-label']) !!}
        {!! Form::text('subject',null, ['class'=>'form-control']) !!}
    </div>
    <!-- Schools_that_is Form Input -->
    <div class="form-group" id="">
        {!! Form::label('schools_that_is', 'Σχολείο/α που υπηρετείς:', ['class'=>'control-label']) !!}
        {!! Form::text('schools_that_is',null, ['class'=>'form-control']) !!}
    </div>
    <!-- Description Form Input -->
    <div class="form-group">
        {!! Form::label('description', 'Παρακαλώ:', ['class'=>'control-label']) !!}
        {!! Form::textarea('description',null, ['class'=>'form-control', 'placeholder'=>'περιγράψτε το αίτημά σας...']) !!}
    </div>

    <div class="form-group text-center">
        <button type="button" value="{!! $action !!}" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#ConditionsModal">
          Προσωρινή αποθήκευση
        </button>
        <button type="button" value="sent_request" class="btn btn-success btn-lg" data-toggle="modal" data-target="#ConditionsModal">
          Αποστολή
        </button>
    </div>