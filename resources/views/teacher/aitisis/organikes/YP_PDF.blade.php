<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            margin: 0;
            font-size: 1em;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-left: 50px;
            margin-right: 50px;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        .withBorders td{
            border: 1px solid #000000;
        }

        .t1c1{
            font-size: 22pt;
            font-weight: bold;
            text-align: center;
            letter-spacing: 5px;
        }


        #t1c3s1{
            padding-left: 15px;
            font-size: 16pt;
        }

        #t1c3s2{
            font-size: 18pt;
            font-weight: bold;
        }

        .t1c4{
            /*text-align: center;*/
            font-size: 16pt;
            font-weight: 800;
            padding-left: 30px;
        }

        #description{
            padding-left: 20px;
            line-height: 200%;
            text-align: justify;
            font-size: 14pt;
        }

        .line-data{
            font-size: 14pt;
            text-align: center;
        }

        .data{
            font-size: 15pt;
            font-weight: bold;
        }

        .text-center {
            text-align: center;
        }

        .header{
            font-size: 20pt;
            font-weight: bold;
            text-align: center;
        }

        .little-header{
            font-size: 14pt;
            text-align: center;
        }

        #content-body{
            margin-top: 50px;
            margin-bottom: 50px;
            padding-top: 30px;
            padding-bottom: 50px;
            margin-left: 10px;
            margin-right: 10px;
            font-size: 16pt;
            line-height: 30px;
            text-align: justify;
        }

        .dilosi{
            width: 70%;
            margin: auto;
            padding-top: 70px;
            padding-bottom: 30px;
        }

        .to{
            width: 70%;
            padding-top: 30px;
            margin: auto;
            padding-bottom: 50px;
        }
    </style>

</head>
<body class="page" marginwidth="0" marginheight="0">

    <table>
        <tbody>
            <tr>
                <td width="50%"></td>
                <td width="50%">
                    <div>
                        <span id="t1c3s1">ΑΡ. ΑΙΤΗΣΗΣ:</span>
                        <span id="t1c3s2">{!! $protocol->protocol_name !!}</span>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2" width="100%">
                    <div class="dilosi">
                        <div class="header">
                            ΔΗΛΩΣΗ ΕΠΙΘΥΜΙΑΣ ΓΙΑ ΧΑΡΑΚΤΗΡΙΣΜΟ ΩΣ ΥΠΕΡΑΡΙΘΜΟΣ
                        </div>
                        <div class="little-header">
                            (επέχει θέση Υπεύθυνης Δήλωσης του Ν.1599/86)
                        </div>
                    </div>

                </td>
            </tr>

            <tr>
                <td colspan="2" width="100%">
                    <div class="to">
                       <div class="little-header">
                            Προς
                       </div>
                        <div class="header">
                            ΠΥΣΔΕ ΧΑΝΙΩΝ
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td colspan="2" width="100%" id="content-body">
                    <div>
                        <p>
                            {!!$request_teacher->teacher->myschool->sex == 0 ? 'Η':'Ο' !!}
                            <b>{!! $request_teacher->teacher->myschool->full_name !!}</b>  του
                            {!! $request_teacher->teacher->myschool->middle_name !!} εκπαιδευτικός ΚΛΑΔΟΥ
                            {!! $request_teacher->teacher->myschool->new_klados !!}
                            ({!! $request_teacher->teacher->myschool->new_eidikotita_name !!})
                            <b>{!! $request_teacher->want_yperarithmia == 1 ? 'επιθυμώ' : 'ΔΕΝ επιθυμώ' !!}</b>
                            να χαρακτηριστώ ως υπεράριθμ{!!$request_teacher->teacher->myschool->sex == 0 ? 'η':'ος' !!}
                            στο
                            <b>{!! $request_teacher->teacher->myschool->organiki_name !!} </b>
                            της περιοχής του Ν. Χανίων.
                        </p>
                    </div>
                </td>
            </tr>


            <tr>
                <td colspan="2" width="50%">
                    <div class="line-data">
                        <span class="data">{!! $request_teacher->date_request !!}</span>
                    </div>
                    <div class="line-data">
                        <span style="border-top: 2px solid #000000">ΗΜΕΡΟΜΗΝΙΑ ΚΑΙ ΩΡΑ ΥΠΟΒΟΛΗΣ</span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

</body>
</html>
