@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">
@endsection

@section('loader')
    @include('vueloader')
@endsection

@section('content')


<?php
    $current_year = \Carbon\Carbon::now()->year;
?>

@if(Auth::user()->userable->yperarithmia_request->where('year', $current_year)->first() != null)
     <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 150px;">
            <h2 class="text-center">
                <p>
                    Έχετε στείλει αίτημα για να χαρακτηριστείτε ως Οργανικά Υπεράριθμος στο ΠΥΣΔΕ.
                </p>
                <p>

                </p>
                    Μπορείτε να ανατρέξετε στο <a href="{!! action('RequestController@indexYperarithmia') !!}">Ιστορικό Αιτήσεων</a>  για να δείτε το αίτημά σας.
            </h2>
     </div>
@else
    @if(Auth::user()->userable->myschool->has_yperarithmia)
        <div id="requests">
                <div class="row">
                    <div class="alert alert-danger alert-dismissible col-md-6 col-md-offset-3" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <ul>
                            <li><h4>Για να σταλεί η Αίτηση στο ΠΥΣΔΕ θα πρέπει να: </h4>
                                <ol>
                                    <li>συμφωνείτε με τους όρους υποβολής αίτησης</li>
                                    <li>πάρετε αριθμό αίτησης ο οποίος εμφανίζεται με ένα μήνυμα επιτυχίας αποστολής</li>
                                    <li>λάβετε E-mail επιβεβαίωσης</li>
                                </ol>
                        </ul>
                    </div>
                </div>

            <h2 id="request_header_h2" class="text-center">
                <span class="label label-default">ΔΗΛΩΣΗ ΕΠΙΘΥΜΙΑΣ ΓΙΑ ΧΑΡΑΚΤΗΡΙΣΜΟ ΩΣ ΥΠΕΡΑΡΙΘΜΟΣ</span>
            </h2>

            <div class="row">
                <div class="col-md-4">
                    @include('teacher._profile')
                </div>
                <h2 class="text-center">ΠΡΟΣ ΤΟ Π.Υ.Σ.Δ.Ε. Ν. ΧΑΝΙΩΝ</h2>
                <h3 style="padding: 5px; line-height: 40px;margin: 0 50px 0 50px;text-align: justify">

                    <input type="checkbox" id="checkYperarithmos" v-model="wants"/>
                    <label for="checkYperarithmos" v-if="wants" class="label label-success">ΕΠΙΘΥΜΩ</label>
                    <label for="checkYperarithmos" v-else class="label label-danger">ΔΕΝ ΕΠΙΘΥΜΩ</label>

                    <br>
                    να χαρακτηριστώ ως υπεράριθμος στο
                    <span style="font-weight: bold;font-size: 15pt">
                         {!! $organiki !!}
                    </span>
                    της περιοχής του Νομού Χανίων.
                </h3>
                <h1 class="text-center">
                <button @click="openModal" class="btn btn-primary btn-lg">
                    ΑΠΟΣΤΟΛΗ
                </button>
                </h1>
            </div>
        </div>

        <modal :show.sync="showModal" :loader.sync="hideLoader"></modal>
    @else
         <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 150px;">
                <h1 class="text-center">
                    Δεν έχει βγει υπεραριθμία στο Σχολείο σας για την ειδικότητά σας.
                </h1>
         </div>
    @endif
@endif
@endsection

@section('scripts.footer')
    <script src="{!! elixir('js/EpithimiaYperarithmias.js') !!}"></script>

    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()

            $('#toggle-one').bootstrapToggle();
        });
    </script>

@endsection