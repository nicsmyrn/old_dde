@extends('app')

@section('title')
    Αίτηση - Δήλωση προτιμήσεων Σχολικών Μονάδων για βελτιώση με τη διαδικασία της ρύθμισης υπεραριθμίας
@endsection

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">
    <style>
        [v-cloak] {
          display: none;
        }

        .listAvailableSchools{
            margin-bottom:3px;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
            background-color: #D3D3D3;
            border-radius: 5px;
            color:#000000;
            cursor:pointer;
              -o-transition:color .1s ease-out, background 0.5s ease-in;
              -ms-transition:color .1s ease-out, background 0.5s ease-in;
              -moz-transition:color .1s ease-out, background 0.5s ease-in;
              -webkit-transition:color 1s ease-out, background 0.5s ease-in;
              transition:color .1s ease-out, background 0.5s ease-in;
        }
        .listAvailableSchools:hover{
            background-color: darkgrey;
            color: #0000ff;
            font-weight: 800;
        }

        .slideLabel{
            font-size: 12pt;
        }



        input[type=checkbox] {
        	visibility: hidden;
        }


        /* SLIDE THREE */
        .slideThree {
        	width: 80px;
        	height: 26px;
        	background: #cccccc;
        	margin-top: 10px;

        	-webkit-border-radius: 50px;
        	-moz-border-radius: 50px;
        	border-radius: 50px;
        	position: relative;


            -webkit-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
            -moz-box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
            box-shadow: inset 0px 1px 1px rgba(0,0,0,0.5), 0px 1px 0px rgba(255,255,255,0.2);
        }

        .slideThree:after {
        	content: 'ΌΧΙ';
        	font: 12px/26px Arial, sans-serif;
        	color: #FF0000;
        	position: absolute;
        	right: 10px;
        	z-index: 0;
        	font-weight: bold;
        	text-shadow: 1px 1px 0px rgba(255,255,255,.15);
        }

        .slideThree:before {
        	content: 'ΝΑΙ';
        	font: 14px/26px Arial, sans-serif;
        	color: #00bf00;
        	position: absolute;
        	left: 10px;
        	z-index: 0;
        	font-weight: bold;
        }

        .slideThree label {
        	display: block;
        	width: 34px;
        	height: 20px;

        	-webkit-border-radius: 50px;
        	-moz-border-radius: 50px;
        	border-radius: 50px;

        	-webkit-transition: all .4s ease;
        	-moz-transition: all .4s ease;
        	-o-transition: all .4s ease;
        	-ms-transition: all .4s ease;
        	transition: all .4s ease;
        	cursor: pointer;
        	position: absolute;
        	top: 3px;
        	left: 3px;
        	z-index: 1;



            -webkit-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
            -moz-box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
            box-shadow: 0px 2px 5px 0px rgba(0,0,0,0.3);
            background: #fcfff4;

        	background: -webkit-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: -moz-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: -o-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: -ms-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	background: linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
        	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fcfff4', endColorstr='#b3bead',GradientType=0 );
        }

        .slideThree input[type=checkbox]:checked + label {
        	left: 43px;
        }



    </style>
@endsection


@section('loader')
    @include('vueloader')
@endsection

@section('content')
                <input type="hidden" v-model="type_of_aitisi" value="{!! $type !!}"/>

                <div class="row">
                    <div class="col-md-4">
                        @include('teacher._profile')
                    </div>

                    <div v-if="loader" class="col-md-8">

                        <h1 class="text-center" id="loader">
                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                            <p>
                                <b><i>Φορτώνει τα δεδομένα...</i></b>
                            </p>
                        </h1>
                    </div>
                    <div v-cloak v-else class="col-md-8">

                        <div v-cloak v-if="hasMadeRequest">
                             <h2 class="text-center">ΑΙΤΗΣΗ  ΥΠΕΡΑΡΙΘΜΟΥ</h2>
                             <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 20px;">
                                    <h3 class="text-center">
                                        Η αίτηση - δήλωση προτιμήσεων έχει πραγματοποιηθεί. Δεν μπορείτε
                                        να υποβάλλετε εκ νέου αίτηση. Πηγαίνετε στο ιστορικό αρχείων για εμφάνιση της αίτησής σας.
                                    </h3>
                             </div>
                        </div>

                        <div v-cloak v-else>
                                <h4 class="text-center"><u>Δήλωση Προτιμήσεων Σχολικών Μονάδων σε Ονομαστικά Υπεράριθμους για Οργανική Θέση</u></h4>

                                <div v-if="selectedSchoolsIdiasOmadas.length || selectedSchoolsOmoron.length || selectedSchools.length" class="row">
                                    <div class="col-md-12 text-center">
                                        <button @click="saveRequest('{!! $type !!}')" class="btn btn-warning btn-lg">
                                            Προσωρινή Αποθήκευση
                                        </button>
                                        <button @click="openModal" class="btn btn-success btn-lg">
                                            Αποστολή
                                        </button>
                                        <button v-if="requestInDatabase" @click="openDeleteModal" class="btn btn-danger btn-lg">
                                            ΟΡΙΣΤΙΚΗ ΔΙΑΓΡΑΦΗ
                                        </button>
                                    </div>

                                </div>

                                <hr>

                                <!-- ΙΔΙΑ ΟΜΑΔΑ  -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <div class="slideThree">
                                                <input @change="initIdiaOmada" type="checkbox" v-model="idia_omada_checked"  id="idia_omada_checked" name="check" />
                                                <label for="idia_omada_checked"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="slideLabel">
                                                 1. Επιθυμώ να τοποθετηθώ σε Σχολεία
                                                  <span class="label label-primary">της ίδιας ομάδας</span>
                                                  <a href="#" data-toggle="tooltip" title="@{{ idiaomada.description }}">
                                                    [ @{{ idiaomada.name }} ]
                                                    <i style="color: #ff4433" class="fa fa-info-circle" aria-hidden="true"></i>
                                                </a>
                                                  με το Σχολείο που ανήκω οργανικά; (σύγκριση με μόρια μετάθεσης)
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12" v-if="idia_omada_checked">
                                        <div class="panel panel-primary">
                                              <div class="panel-heading">
                                                Δήλωση προτίμησης Σχολείων Ίδιας Ομάδας
                                              </div>
                                              <div class="panel-body">
                                                <!-- ##########################   PANEL BODY  -->
                                                   <div class="col-md-5"
                                                        style="border : solid 2px #000000; border-radius: 4px; padding: 5px; background : #ffffff; color : #000000;  width : 240px; height : 250px; overflow : auto;"
                                                        v-if="selectedSchoolsIdiasOmadas.length < maxSelections"
                                                    >
                                                        <div v-for="school in allSchoolsIdiasOmadas"
                                                            class="listAvailableSchools"
                                                            @click="addSchoolToChoicesIdiaOmada(school)"
                                                        >
                                                            <span v-cloak>@{{ school.name }}</span>
                                                            <span style="float: right">
                                                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div v-else class="col-md-5">
                                                        <div class="alert alert-danger" role="alert">
                                                            <h3 v-cloak class="text-center">
                                                                Έχετε τη δυνατότητα να επιλέξετε μέγιστο @{{ maxSelections }} Σχολικές Μονάδες
                                                            </h3>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-7 text-center">
                                                        <div v-if="!selectedSchoolsIdiasOmadas.length">
                                                             <div class="alert alert-warning col-md-6 col-md-offset-3" role="alert">
                                                                    <h3 class="text-center">
                                                                        ΔΕΝ υπάρχουν προτιμήσεις Σχολείων
                                                                    </h3>
                                                             </div>
                                                        </div>
                                                        <div v-else>
                                                            <h2 v-cloak>
                                                                @{{ selectedSchoolsIdiasOmadas.length }} -
                                                                    <span v-if="selectedSchoolsIdiasOmadas.length == 1">Επιλογή</span>
                                                                    <span v-else>Επιλογές</span>
                                                                    <button @click="deleteAllSelectedIdiaOmada" class="btn btn-danger btn-xs">
                                                                        <i class="glyphicon glyphicon-trash"></i>
                                                                        Διαγραφή όλων των επιλογών
                                                                    </button>
                                                            </h2>

                                                            <table v-else class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">A/A</th>
                                                                        <th class="text-center">Σχολείο</th>
                                                                        <th class="text-center" colspan="2">Ενέργειες</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr v-for="school in selectedSchoolsIdiasOmadas" track-by="$index">
                                                                        <td v-cloak>@{{ $index + 1 }}</td>
                                                                        <td v-cloak>@{{ school.name  }}</td>
                                                                        <td>
                                                                            <i style="color: red;cursor: pointer"
                                                                                title="Διαγραφή προτίμησης"
                                                                                @click="deleteSelectedSchoolIdiaOmada(school)"
                                                                                class="fa fa-times-circle"
                                                                                aria-hidden="true"></i>
                                                                        </td>
                                                                        <td>
                                                                            <span v-if="$index > 0" style="cursor: pointer"  @click="swapUpIdiaOmada($index)">
                                                                                <i class="fa fa-caret-square-o-up" title="Μετακίνηση μία θέση προς τα επάνω" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span v-if="selectedSchoolsIdiasOmadas.length > $index+1" style="cursor: pointer" @click="swapDownIdiaOmada($index)">
                                                                                <i class="fa fa-caret-square-o-down" title="Μετακίνηση μία θέση προς τα κάτω" aria-hidden="true"></i>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>


                                <!-- ΟΜΟΡΕΣ ΟΜΑΔΕΣ  -->

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-2">
                                            <div class="slideThree">
                                                <input @change="initOmoriOmada" type="checkbox" v-model="omori_omada_checked" id="omori_omada_checked" name="check" />
                                                <label for="omori_omada_checked"></label>
                                            </div>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="slideLabel">
                                                 2. Επιθυμώ να τοποθετηθώ σε Σχολεία
                                               <span class="label label-warning">όμορων ομάδων</span>
                                               <a href="#" title="@{{ om.description }}" data-toggle="tooltip" v-for="om in omoresArray">
                                                    <span style="color: #f0ad4e;font-weight: bold">
                                                        [@{{ om.name }}]
                                                    </span>
                                                    <i style="color: #f06e57;" class="fa fa-info-circle" aria-hidden="true"></i>
                                               </a>
                                                 με το Σχολείο που ανήκω οργανικά; (σύγκριση με μόρια μετάθεσης)
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12" v-if="omori_omada_checked">
                                        <div class="panel panel-warning">
                                              <div class="panel-heading">
                                                Δήλωση προτίμησης Σχολείων Όμορων Ομάδων
                                              </div>
                                              <div class="panel-body">
                                                <!-- ##########################   PANEL BODY  -->
                                                   <div class="col-md-5"
                                                        style="border : solid 2px #000000; border-radius: 4px; padding: 5px; background : #ffffff; color : #000000;  width : 240px; height : 250px; overflow : auto;"
                                                        v-if="selectedSchoolsOmoron.length < maxSelections"
                                                    >
                                                        <div v-for="school in allSchoolsOmoron"
                                                            class="listAvailableSchools"
                                                            @click="addSchoolToChoicesOmoriOmada(school)"
                                                        >
                                                            <span v-cloak>@{{ school.name }}</span>
                                                            <span style="float: right">
                                                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                                            </span>
                                                        </div>

                                                    </div>

                                                    <div v-else class="col-md-5">
                                                        <div class="alert alert-danger" role="alert">
                                                            <h3 v-cloak class="text-center">
                                                                Έχετε τη δυνατότητα να επιλέξετε μέγιστο @{{ maxSelections }} Σχολικές Μονάδες
                                                            </h3>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-7 text-center">
                                                        <div v-if="!selectedSchoolsOmoron.length">
                                                             <div class="alert alert-warning col-md-6 col-md-offset-3" role="alert">
                                                                    <h3 class="text-center">
                                                                        ΔΕΝ υπάρχουν προτιμήσεις Σχολείων
                                                                    </h3>
                                                             </div>
                                                        </div>
                                                        <div v-else>
                                                            <h2 v-cloak>
                                                                @{{ selectedSchoolsOmoron.length }} -
                                                                    <span v-if="selectedSchoolsOmoron.length == 1">Επιλογή</span>
                                                                    <span v-else>Επιλογές</span>
                                                                    <button @click="deleteAllSelectedOmoriOmada" class="btn btn-danger btn-xs">
                                                                        <i class="glyphicon glyphicon-trash"></i>
                                                                        Διαγραφή όλων των επιλογών
                                                                    </button>
                                                            </h2>

                                                            <table v-else class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="text-center">A/A</th>
                                                                        <th class="text-center">Σχολείο</th>
                                                                        <th class="text-center" colspan="2">Ενέργειες</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr v-for="school in selectedSchoolsOmoron" track-by="$index">
                                                                        <td v-cloak>@{{ $index + 1 }}</td>
                                                                        <td v-cloak>@{{ school.name  }}</td>
                                                                        <td>
                                                                            <i style="color: red;cursor: pointer"
                                                                                title="Διαγραφή προτίμησης"
                                                                                @click="deleteSelectedSchoolOmoriOmada(school)"
                                                                                class="fa fa-times-circle"
                                                                                aria-hidden="true"></i>
                                                                        </td>
                                                                        <td>
                                                                            <span v-if="$index > 0" style="cursor: pointer"  @click="swapUpOmoriOmada($index)">
                                                                                <i class="fa fa-caret-square-o-up" title="Μετακίνηση μία θέση προς τα επάνω" aria-hidden="true"></i>
                                                                            </span>
                                                                            <span v-if="selectedSchoolsOmoron.length > $index+1" style="cursor: pointer" @click="swapDownOmoriOmada($index)">
                                                                                <i class="fa fa-caret-square-o-down" title="Μετακίνηση μία θέση προς τα κάτω" aria-hidden="true"></i>
                                                                            </span>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>

                                                        </div>
                                                    </div>
                                              </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>


                                </div>
                        </div>

                    </div>
                </div>

@endsection

@section('scripts.footer')
    <script src="{!! elixir('js/DilosiSxoleionYperarithmou.js') !!}"></script>
@endsection