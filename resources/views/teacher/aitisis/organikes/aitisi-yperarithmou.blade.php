@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">
    <style>
        [v-cloak] {
          display: none;
        }

        .listAvailableSchools{
            margin-bottom:3px;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
            background-color: #D3D3D3;
            border-radius: 5px;
            color:#000000;
            cursor:pointer;
              -o-transition:color .1s ease-out, background 0.5s ease-in;
              -ms-transition:color .1s ease-out, background 0.5s ease-in;
              -moz-transition:color .1s ease-out, background 0.5s ease-in;
              -webkit-transition:color 1s ease-out, background 0.5s ease-in;
              transition:color .1s ease-out, background 0.5s ease-in;
        }
        .listAvailableSchools:hover{
            background-color: darkgrey;
            color: #0000ff;
            font-weight: 800;
        }
    </style>
@endsection


@section('loader')
    @include('vueloader')
@endsection

@section('content')
    @if(Auth::user()->userable->myschool->rights != null)
        @if(Auth::user()->userable->myschool->rights->onomastika_yperarithmos)
                <div class="row">
                    <div class="col-md-4">
                        @include('teacher._profile')
                    </div>

                    <div v-if="loader" class="col-md-8">

                        <h1 class="text-center" id="loader">
                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                            <p>
                                <b><i>Φορτώνει τα δεδομένα...</i></b>
                            </p>
                        </h1>
                    </div>
                    <div v-cloak v-else class="col-md-8">
                        <div v-cloak v-if="hasMadeRequest">
                             <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 150px;">
                                    <h3 class="text-center">
                                        Έχετε υποβάλει αίτηση για Οργανική ως Ονομαστικά Υπεράριθμος. Πηγαίνετε στο ιστορικό αρχείων για εμφάνιση
                                    </h3>
                             </div>
                        </div>

                        <div v-cloak v-else>
                                <h2 class="text-center">Δήλωση Προτιμήσεων Σχολικών Μονάδων σε Ονομαστικά Υπεράριθμους για Οργανική Θέση</h2>

                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <p>
                                            <strong>Σημείωση!</strong>
                                            <ol>
                                                <li>Επιλέξτε από την παρακάτω λίστα τα Σχολεία με τη σειρά που επιθυμείτε για την αίτησή σας.</li>
                                                <li>Μπορείτε να κάνετε <b>ΜΟΝΟ μια</b>  φορά αίτηση.</li>
                                                <li>Η αίτηση παραλαμβάνεται <b>ΜΟΝΟ</b> με το κουμπί <b>ΑΠΟΣΤΟΛΗ</b>.</li>
                                            </ol>

                                        </p>
                                        <p>

                                        </p>
                                    </div>

                                    <div class="col-md-5"
                                        style="border : solid 2px #000000; border-radius: 4px; padding: 5px; background : #ffffff; color : #000000;  width : 240px; height : 450px; overflow : auto;"
                                        v-if="selectedSchools.length < maxSelections"
                                    >
                                        <div v-for="school in allSchools"
                                            class="listAvailableSchools"
                                            @click="addSchoolToChoices(school)"
                                        >
                                            <span v-cloak>@{{ school.name }}</span>
                                            <span style="float: right">
                                                <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
                                            </span>
                                        </div>

                                    </div>

                                    <div v-else class="col-md-5">
                                        <div class="alert alert-danger" role="alert">
                                            <h3 v-cloak class="text-center">
                                                Έχετε τη δυνατότητα να επιλέξετε μέγιστο @{{ maxSelections }} Σχολικές Μονάδες
                                            </h3>
                                        </div>
                                    </div>

                                    <div class="col-md-7 text-center">
                                        <div v-if="!selectedSchools.length">
                                             <div class="alert alert-warning col-md-6 col-md-offset-3" role="alert">
                                                    <h3 class="text-center">
                                                        ΔΕΝ υπάρχουν προτιμήσεις Σχολείων
                                                    </h3>
                                             </div>
                                        </div>
                                        <div v-else>
                                            <div class="col-md-12 btn-group text-center">
                                                <button @click="saveRequest" class="btn btn-warning btn-lg">
                                                    Προσωρινή Αποθήκευση
                                                </button>
                                                <button @click="openModal" class="btn btn-success btn-lg">
                                                    Αποστολή
                                                </button>
                                            </div>
                                            <h2 v-cloak>
                                                @{{ selectedSchools.length }} -
                                                    <span v-if="selectedSchools.length == 1">Επιλογή</span>
                                                    <span v-else>Επιλογές</span>
                                                    <button @click="deleteAllSelected" class="btn btn-danger btn-xs">
                                                        <i class="glyphicon glyphicon-trash"></i>
                                                        Διαγραφή όλων των επιλογών
                                                    </button>
                                            </h2>

                                            <table v-else class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">A/A</th>
                                                        <th class="text-center">Σχολείο</th>
                                                        <th class="text-center" colspan="2">Ενέργειες</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr v-for="school in selectedSchools" track-by="$index">
                                                        <td v-cloak>@{{ $index + 1 }}</td>
                                                        <td v-cloak>@{{ school.name  }}</td>
                                                        <td>
                                                            <i style="color: red;cursor: pointer"
                                                                title="Διαγραφή προτίμησης"
                                                                @click="deleteSelectedSchool(school)"
                                                                class="fa fa-times-circle"
                                                                aria-hidden="true"></i>
                                                        </td>
                                                        <td>
                                                            <span style="cursor: pointer"  @click="swapUp($index)">
                                                                <i class="fa fa-caret-square-o-up" title="Μετακίνηση μία θέση προς τα επάνω" aria-hidden="true"></i>
                                                            </span>
                                                            <span style="cursor: pointer" @click="swapDown($index)">
                                                                <i class="fa fa-caret-square-o-down" title="Μετακίνηση μία θέση προς τα κάτω" aria-hidden="true"></i>
                                                            </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>

                                        </div>
                                    </div>
                                                    <br>
                                                    <div v-if="requestInDatabase" class="col-md-12 text-center">
                                                        <button @click="openDeleteModal" class="btn btn-danger btn-lg">
                                                            ΟΡΙΣΤΙΚΗ ΔΙΑΓΡΑΦΗ ΑΙΤΗΣΗΣ
                                                        </button>
                                                    </div>
                        </div>

                    </div>

                </div>

                <modal :show.sync="showModal" :loader.sync="hideLoader"></modal>

                <modal-delete v-if="requestInDatabase" :show.sync="showDeleteModal" :loader.sync="hideLoader"></modal-delete>

                    <alert :type="alert.type" :important="alert.important">
                        <div slot="alert-header">
                            @{{ alert.header }}
                        </div>
                        <div slot="alert-body">
                            @{{ alert.message }}
                        </div>
                    </alert>
        @else
             <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 150px;">
                    <h1 class="text-center">
                        Δεν έχετε χαρακτηριστεί ως Οργανικά Υπεράριθμος
                    </h1>
             </div>
        @endif
    @else
         <div class="alert alert-danger col-md-6 col-md-offset-3" role="alert" style="margin-bottom: 400px; margin-top: 150px;">
                <h1 class="text-center">
                    Δεν έχετε χαρακτηριστεί ως Οργανικά Υπεράριθμος
                </h1>
         </div>
    @endif

@endsection

@section('scripts.footer')
    <script src="{!! elixir('js/DilosiSxoleion.js') !!}"></script>
@endsection