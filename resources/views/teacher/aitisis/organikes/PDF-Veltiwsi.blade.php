<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            margin: 0;
            font-size: 1em;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        .withBorders td{
            border: 1px solid #000000;
        }

        .t1c1{
            font-size: 20pt;
            font-weight: bold;
            text-align: center;
        }
        .t1c2{
            text-align: center;
        }

        #t1c3s1{
            padding-left: 15px;
            font-size: 12pt;
        }

        #t1c3s2{
            font-size: 14pt;
            font-weight: bold;
        }

        .t1c4{
            text-align: center;
            font-size: 16pt;
            font-weight: 800;
        }

        #aitisi_type{
            font-size: 20pt;
            font-weight: bold;
            text-align: center;
            color: #006400;
        }

        .greekHeader{
            font-size: 13pt;
            font-weight: 800;
            margin-left: 25px;
            border-bottom: 2px solid #000000;
            width: 200px;
        }

        .greekHeader3{
            font-size: 13pt;
            font-weight: 800;
            margin-left: 25px;
            border-bottom: 2px solid #000000;
            width: 270px;
        }

        .tableLabel{

        }

        .userTableData{
            font-weight: bold;
        }

        .table2Label{
            text-align: center;
        }
        .userTable2Data{
            font-weight: bold;
            text-align: center;
        }

        .ypiresia{
            text-align: center;
            font-size: 9pt;
        }

        #moria{
            font-size: 15pt;
            font-weight: bold;
        }

        #organikiLabel{
            text-align: center;
            font-size: 10pt;
        }

        #organiki{
            font-size: 15pt;
            font-weight: bold;
            text-align: center;
        }

        .userDataNumbers{
            font-weight: bold;
        }

        .odigies{
            padding-left: 15px;
        }

        .table3Label{
            text-align: center;
        }
        .userTable3Data{
            padding-left: 15px;
        }
        #dilosi{
            text-align: center;
            font-weight: bold;
            font-size: 13pt;
        }
        .date_signature{
            text-align: center;
        }
        #date_value{
            font-weight: bold;
            font-size: 14pt;
        }

        #descriptions{

        }

        .dilosi{
            font-weight: bold;
        }
    </style>

</head>
<body class="page" marginwidth="0" marginheight="0">
        <table>
            <tbody>
                <tr>
                    <td width="45%">
                        <div class="t1c1">ΑΙΤΗΣΗ - ΔΗΛΩΣΗ</div>
                        <div class="t1c2">ΓΙΑ ΒΕΛΤΙΩΣΗ - ΟΡΙΣΤΙΚΗ ΤΟΠΟΘΕΤΗΣΗ</div>
                    </td>
                    <td width="10%">
                        <div id="aitisi_type">
                        </div>
                    </td>
                    <td width="45%">
                        <div><span id="t1c3s1">Αρ. Αίτησης:</span><span id="t1c3s2">{!! $protocol->protocol_name !!}</span> </div>
                        <div class="t1c4">Προς το</div>
                        <div class="t1c4">Π.Υ.Σ.Δ.Ε. Χανίων</div>
                    </td>
                </tr>
            </tbody>
        </table>

        <br>
        <div class="greekHeader">Α. ΓΕΝΙΚΑ ΣΤΟΙΧΕΙΑ</div>

        <table cellpadding="8" class="withBorders">
            <tbody>
                <tr>
                    <td width="20%" class="tableLabel">ΕΠΩΝΥΜΟ:</td>
                    <td width="30%" class="userTableData">{!! $request_teacher->teacher->user->last_name !!}</td>
                    <td width="20%" class="tableLabel">ΟΡΓΑΝΙΚΗ ΘΕΣΗ:</td>
                    <td width="30%" class="userTableData">{!! $request_teacher->teacher->teacherable->organiki_name !!}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΟΝΟΜΑ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->user->first_name !!}</td>
                    <td class="tableLabel">ΔΙΕΥΘΥΝΣΗ ΚΑΤΟΙΚΙΑΣ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->address !!}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΠΑΤΡΩΝΥΜΟ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->middle_name !!}</td>
                    <td class="tableLabel">ΠΟΛΗ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->city !!}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΚΛΑΔΟΣ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->myschool->new_klados !!}</td>
                    <td class="tableLabel">ΤΚ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->tk !!}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΕΙΔΙΚΟΤΗΤΑ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->myschool->new_eidikotita_name  !!}</td>
                    <td class="tableLabel">ΤΗΛΕΦΩΝΟ:</td>
                    <td class="userTableData">{!! $request_teacher->teacher->phone !!}</td>
                </tr>
                <tr>
                    <td class="tableLabel">ΑΡ. ΜΗΤΡΩΟΥ</td>
                    <td class="userTableData">{!! $request_teacher->teacher->teacherable->am !!}</td>
                    <td class="tableLabel">ΚΙΝΗΤΟ</td>
                    <td class="userTableData">{!! $request_teacher->teacher->mobile !!}</td>
                </tr>
            </tbody>
        </table>

        <div class="greekHeader">Β. ΕΙΔΙΚΑ ΣΤΟΙΧΕΙΑ</div>

        <table class="withBorders">
            <tbody>
                <tr>
                    <td class="table2Label">ΧΡΟΝΙΑ ΥΠΗΡΕΣΙΑΣ:</td>
                    <td class="userTable2Data">
                        @if($request_teacher->teacher->myschool->edata->aitisi_veltiwsis)
                            {!! $request_teacher->teacher->myschool->edata->ex_years !!}Χ | {!! $request_teacher->teacher->myschool->edata->ex_months !!}M | {!! $request_teacher->teacher->myschool->edata->ex_days!!}H
                        @else
                            Υπολογίζονται από το Τμήμα Γ' Προσωπικού
                        @endif
                    </td>
                    <td class="table2Label">
                        <div>ΔΗΜΟΣ</div>
                        <div>ΣΥΝΥΠΥΡΕΤΗΣΗΣ * </div>
                    </td>
                    <td class="userTable2Data">{!! \Config::get('requests.dimos')[$request_teacher->teacher->dimos_sinipiretisis] !!}</td>
                </tr>
                <tr>
                    <td class="table2Label">
                        <div>ΟΙΚΟΓΕΝΕΙΑΚΗ</div>
                        <div>ΚΑΤΑΣΤΑΣΗ * </div>
                    </td>
                    <td class="userTable2Data">{!! \Config::get('requests.family_situation')[$request_teacher->teacher->family_situation] !!}</td>
                    <td class="table2Label">
                        <div>ΔΗΜΟΣ</div>
                        <div>ΕΝΤΟΠΙΟΤΗΤΑΣ * </div>
                        <div>(τελευταία διετία)</div>
                    </td>
                    <td class="userTable2Data">{!! \Config::get('requests.dimos')[$request_teacher->teacher->dimos_entopiotitas] !!}</td>
                </tr>
                <tr>
                    <td width="20%"  rowspan="2" class="table2Label">
                        <div>ΑΡΙΘΜΟΣ ΠΑΙΔΙΩΝ <=</div>
                        <div>18 ή ΣΠΟΥΔΑΣΤΕΣ</div>
                        <div>ΑΕΙ, ΤΕΙ, ΙΕΚ * </div>
                    </td>
                    <td width="20%" rowspan="2" class="userTable2Data">{!! $request_teacher->teacher->childs !!}</td>
                    <td width="20%">
                        <div class="table2Label">ΣΥΝΟΛΟ ΣΤΑΘΕΡΩΝ ΜΟΡΙΩΝ</div>
                        <div class="table2Label">
                        </div>
                    </td>
                    <td class="userTable2Data" width="20%">
                        @if($request_teacher->teacher->myschool->edata->aitisi_veltiwsis)
                            {!! ($request_teacher->teacher->myschool->edata->moria_ypiresias +  $request_teacher->teacher->myschool->edata->moria_sinthikes) !!}
                        @else

                        @endif
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="table2Label">
                            ΓΕΝΙΚΟ ΣΥΝΟΛΟ ΜΟΡΙΩΝ
                        </div>
                        <div class="ypiresia">
                            συμπληρώνεται από την Υπηρεσία
                        </div>
                    </td>
                    <td class="userTable2Data">
                        @if($request_teacher->teacher->myschool->edata->aitisi_veltiwsis)
                            {!! $request_teacher->teacher->myschool->edata->sum_moria !!}
                        @else

                        @endif
                    </td>
                </tr>

                <tr>
                    <td class="table2Label">
                        <div>ΕΙΔΙΚΗ ΚΑΤΗΓΟΡΙΑ:</div>
                    </td>
                    <td class="userTable2Data">
                        @if($request_teacher->teacher->myschool->edata->aitisi_veltiwsis)
                            @if($request_teacher->teacher->myschool->edata->special_situation)
                                <span style="color: red; font-size: 15pt">ΝΑΙ</span>
                            @else
                                ΟΧΙ
                            @endif
                        @else

                        @endif
                    </td>
                    <td class="table2Label">

                    </td>
                    <td class="userTable2Data"></td>
                </tr>

            </tbody>
        </table>

        <p class="greekHeader3">Γ. ΔΗΛΩΣΗ ΠΡΟΤΙΜΗΣΕΩΝ</p>
        <div class="dilosi">Δηλώνω ότι επιθυμώ να τοποθετηθώ οργανικά σε ένα από τα παρακάτω, κατά σειρά προτίμησης, σχολεία της Δ.Δ.Ε. Χανίων:</div>

        <table class="withBorders">
            <thead>
                <tr>
                    <th width="5%">α/α</th>
                    <th width="45%">Ονομασία Σχολείου</th>
                    <th width="5%">α/α</th>
                    <th width="45%">Ονομασία Σχολείου</th>
                </tr>
            </thead>
            <tbody>
                @foreach($request_teacher->prefrences->chunk(2) as $chunk)
                    <tr>
                        @foreach($chunk as $prefrence)
                            @if($chunk->count() == 1)
                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                                <td class="table3Label"></td>
                                <td class="userTable3Data"></td>
                            @else
                                <td class="table3Label">{!! $prefrence->order_number !!}</td>
                                <td class="userTable3Data">{!! $prefrence->organiki_name !!}</td>
                            @endif
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>

        <table>
            <tr>
                <td width="40%">

                </td>
                <td width="60%">
                    <div id="dilosi">Δηλώνω την ακρίβεια όλων των παραπάνω στοιχείων</div>
                    <div class="date_signature">Ημερομηνία: <span id="date_value">{!! \Carbon\Carbon::now()->format('d/m/Y')!!} </span></div>
                    <br>
                    <div class="date_signature">
                        @if($request_teacher->teacher->myschool->sex == 1)
                            Ο ΔΗΛΩΝ
                        @else
                            Η ΔΗΛΟΥΣΑ
                        @endif
                    </div>
                    <br>
                    <br>
                    <div class="date_signature">Υπογραφή</div>
                </td>
            </tr>
        </table>
</body>
</html>
