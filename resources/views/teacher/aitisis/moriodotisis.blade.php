@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">

    <style>
            [v-cloak] {
              display: none;
            }
    </style>

    @include('user._style')

@endsection

@section('loader')
    @include('loader')
@endsection

@section('content')
<div id="requests">

    <div class="row">
        <div class="col-md-4">
            @include('teacher._profile')
        </div>

        <div class="col-md-8">

            {!! Form::open(['method'=>'POST', 'action'=>'RequestController@sentAttachementsMoriodotisi', 'files' => true]) !!}

                <input name="type" type="hidden" value="{!! $type !!}"/>

                <div class="row">
                    <div class="col-md-12">
                        @include('teacher.attachments.upload', [
                            'title' => 'Σε περίπτωση που δεν υποβληθούν ηλεκτρονικά τα δικαιολογητικά, θα πρέπει να τα προσκομίσετε στη Δ.Δ.Ε. Χανίων εγγράφως',
                            'teacher' => Auth::user()->teacher
                            ])
                    </div>
                </div>

                <h3 style="padding: 5px; line-height: 40px;margin: 0 50px 0 50px;text-align: justify">
                    @if($type == "Αίτηση-Μοριοδότησης-Απόσπασης")
                        Αιτούμαι να υπολογιστούν τα <strong>μόρια απόσπασης.</strong>
                    @elseif($type == "Αίτηση-Μοριοδότησης-Μετάθεσης")
                        Αιτούμαι να υπολογιστούν τα <strong>μόρια μετάθεσης για τις λειτουργικές τοποθετήσεις τον Σεπτέμβριο του 2018.</strong>
                    @endif

                </h3>

                <h1 class="text-center">
                    {!! Form::submit('ΑΠΟΣΤΟΛΗ', ['class' => 'btn btn-primary btn-lg', 'id'=> 'sent1']) !!}
                </h1>

            {!! Form::close() !!}
        </div>
    </div>
</div>

@endsection

@section('scripts.footer')
        @include('teacher._vue_script_moriodotisi')


            <script>
                $(document).ready(function() {

                    $('#sent1').on('click', function(e){
                        $('#loader').removeClass('invisible')
                            .addClass('loading');
                    });
                });

                function bs_input_file() {
                    $(".input-file").before(
                        function() {
                            if ( ! $(this).prev().hasClass('input-ghost') ) {
                                var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                                element.attr("name",$(this).attr("name"));
                                element.change(function(){
                                    element.next(element).find('input').val((element.val()).split('\\').pop());
                                });
                                $(this).find("button.btn-choose").click(function(){
                                    element.click();
                                });
                                $(this).find("button.btn-reset").click(function(){
                                    element.val(null);
                                    $(this).parents(".input-file").find('input').val('');
                                });
                                $(this).find('input').css("cursor","pointer");
                                $(this).find('input').mousedown(function() {
                                    $(this).parents('.input-file').prev().click();
                                    return false;
                                });
                                return element;
                            }
                        }
                    );
                }
                $(function() {
                    bs_input_file();
                });
            </script>

@endsection