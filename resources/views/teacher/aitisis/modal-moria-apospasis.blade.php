		<div class="modal fade" id="moriaApospasis" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header modal-header-success">
                <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
                <strong><h4 class="modal-title" id="exampleModalLabel">
                    <i class="fa fa-check-square-o" aria-hidden="true"></i>
                       {!! $teacher->user->full_name !!} -  Αναλυτικά μόρια απόσπασης
                    </h4></strong>
              </div>
              <div class="modal-body">
                    <div class="col-md-12">
                        <table class="table table-hover table-bordered table-condensed table-responsive">
                            <thead>
                                <tr class="active">
                                    <th>ΚΡΙΤΗΡΙΑ ΓΙΑ ΑΠΟΣΠΑΣΗ</th>
                                    <th class="text-center">ΜΟΡΙΑ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($analytika as $record)
                                    @if($record['value'] > 0)
                                        @if($record['name'] == 'proipiresia')
                                            <tr>
                                                <td>{!! $record['title'] !!}</td>
                                                <td class="text-center">{!! number_format($record['value'], 3, ',', '') !!}</td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td>{!! $record['title'] !!}</td>
                                                <td class="text-center">{!! $record['value'] !!}</td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr class="danger" style="font-size: 22pt;font-weight: bold">
                                    <td>ΣΥΝΟΛΟ ΜΟΡΙΩΝ</td>
                                    <td class="text-center">{!! number_format($analytika->sum('value'), 3, ',', '') !!}</td>
                                </tr>
                            </tfoot>
                        </table>

                        @if($entopiotita > 0 || $sinipiretisi > 0)
                            <p>
                                <label for="dimos_moria">Επιπλέον Μόρια:</label>
                                    <ul>
                                        @if($entopiotita > 0)
                                            <li>Εντοπιότητα: +{!! $entopiotita !!} μόρια για τον Δήμο {!! Config::get('requests.dimos')[$teacher->dimos_entopiotitas] !!}</li>
                                        @endif
                                        @if($sinipiretisi > 0)
                                            <li>Συνυπηρέτηση: +{!! $sinipiretisi !!} μόρια για τον Δήμο {!! Config::get('requests.dimos')[$teacher->dimos_sinipiretisis] !!}</li>
                                        @endif
                                    </ul>
                            </p>
                        @endif
                    </div>

              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Κλείσιμο</button>
              </div>
            </div>
          </div>
        </div>