            <a href="{!! route('User::getProfile') !!}" class="btn btn-warning">Επεξεργασία Στοιχείων</a>
            @if(Auth::user()->userable->is_checked)
                <span class="label label-success">Ελεγχμένα</span>
            @else
                <span class="label label-danger">Υπό Έλεγχο</span>
            @endif
        @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
             <p>Αριθμός Μητρώου:  <label>{{\Auth::user()->userable->teacherable->am}}</label></p>
        @elseif(\Auth::user()->userable->teacherable_type == 'App\Anaplirotis')
             <p>ΑΦΜ:  <label>{{\Auth::user()->userable->teacherable->afm}}</label></p>
        @endif
            <p>Ονοματεπώνυμο:  <label><a href="{!! route('User::getInfo') !!}" style="font-weight: bold">{{\Auth::user()->full_name}}</a></label></p>
            <p>Πατρώνυμο:  <label>{{\Auth::user()->userable->middle_name}}</label></p>
            <p>Κλάδος:  <label>{{ $eidikotita }}</label></p>
        @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
            @if(Config::get('requests.show_moria'))
                @if(Auth::user()->userable->myschool->edata->aitisi_veltiwsis)
                    <p>
                        <label>Μόρια Βελτιώσης </label>
                        <span data-toggle="tooltip" title="Τα Μόρια υπολογίζονται από το άθροισμα της συνολικής υπηρεσίας, τον αριθμό των παιδιών και την οικογενειακή κατάσταση." data-placement="right" style="color: red;font-weight:bold;font-size: large">
                            {!! Auth::user()->userable->myschool->edata->sum_moria !!}
                        </span>
                    </p>
                @else
                    <p><span data-placement="right" style="color: red;font-size: large"> Τα μόρια υπολογίζονται από το τμήμα Γ' Προσωπικού. </span> </p>
                @endif
            @endif
           <p>Οργανική:  <label>{{ $organiki }}</label></p>
            <p>Υποχρεωτικό Ωράριο:  <label>{{\Auth::user()->userable->teacherable->orario}}</label></p>
        @endif



            <p>Οικογενειακή Κατάσταση:  <label>{{ \Config::get('requests.family_situation')[\Auth::user()->userable->family_situation]}}</label></p>
            <p>Δήμος Συνυπηρέτησης:  <label>{{ \Config::get('requests.dimos')[\Auth::user()->userable->dimos_sinipiretisis]}}</label></p>
            <p>Δήμος Εντοπιότητας:  <label>{{ \Config::get('requests.dimos')[\Auth::user()->userable->dimos_entopiotitas]}}</label></p>
            <p>Σταθερό Τηλέφωνο:  <label>{{\Auth::user()->userable->phone}}</label></p>
            <p>Κινητό Τηλέφωνο:  <label>{{\Auth::user()->userable->mobile}}</label></p>
            <p>Αριθμός Παιδιών:  <label>{{\Auth::user()->userable->childs}}</label></p>
            <p>Ειδική Κατηγορία:  <label>{{\Auth::user()->userable->special_situation?'ΝΑΙ':'ΟΧΙ'}}</label></p>
            <p>Διεύθυνση:  <label>{{\Auth::user()->userable->address}}</label></p>
            <p>Πόλη:  <label>{{\Auth::user()->userable->city}}</label></p>
            <p>ΤΚ:  <label>{{\Auth::user()->userable->tk}}</label></p>

