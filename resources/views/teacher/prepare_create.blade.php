@extends('app')

@section('header.style')
    	<style>
            #teacher-instructions{
                position: fixed;
                top: 260px;
                left:0px;
            }
    	</style>
@endsection

@section('content')

    <h1 class="page-heading"> ΝΕΑ ΑΙΤΗΣΗ</h1>
    {{--<div id="teacher-instructions">--}}

        <div class="alert alert-warning col-md-8 col-md-offset-2" role="alert">
            <ol>
                {{--<li>--}}
                    {{--<a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViMThvTlh3QWx3Yk5fa1hjRlhPV2ZLa0JGUmww/view?usp=sharing">--}}
                     {{--Πρόταση τοποθέτησης ονομαστικά υπεράριθμων εκπαιδευτικών σε κενές--}}
                     {{--οργανικές θέσεις με τη διαδικασία της ρύθμισης υπεραριθμίας και--}}
                     {{--υποβολή Δηλώσεων προτίμησης οργανικών κενών σχολικών μονάδων,--}}
                     {{--με τη διαδικασία των Βελτιώσεων-Οριστικών τοποθετήσεων--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgVidndzMDA2Yk1CQ0U/view?usp=sharing">--}}
                        {{--ΟΝΟΜΑΣΤΙΚΑ ΥΠΕΡΑΡΙΘΜΟΙ 2017--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li>--}}
                    {{--<a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViQjV3UmxQaVdTX1U/view?usp=sharing">--}}
                        {{--Πίνακας οργανικών κενών θέσεων και θέσεων υπεραριθμίας - ΠΡΑΞΗ ΠΥΣΔΕ 17η/30-05-2017--}}
                    {{--</a>--}}
                {{--</li>--}}

                {{--<li>--}}
                    {{--<a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgVidnZjaHpycEhCNHFvY2lHZWhjREhRa1lYc0VR/view?usp=sharing">--}}
                        {{--Ρύθμιση υπεραριθμίας εκπαιδευτικών--}}
                    {{--</a>--}}
                {{--</li>--}}
                {{--<li><a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViMG5kVlJxY05JX00/view">--}}
                    {{--ΑΝΑΚΟΙΝΩΣΗ--}}
                {{--</a></li>--}}
                {{--<li><a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViMzdQMWJXNE5kX3o5TnV3SlFnekxXZFF2Q2tV/view?usp=sharing">--}}
                    {{--ΑΝΑΚΟΙΝΩΣΗ ΓΙΑ ΤΗΝ 2η ΑΝΑΚΟΙΝΟΠΟΙΗΣΗ--}}
                {{--</a></li>--}}
                {{--<li><a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViOFMtekZscDR4RWh2MGN4NWg0RjdSby1oOGpN/view?usp=sharing">--}}
                    {{--2η ΑΝΑΚΟΙΝΟΠΟΙΗΣΗ - ΠΙΝΑΚΑΣ ΛΕΙΤΟΥΡΓΙΚΩΝ ΚΕΝΩΝ 05-09-2017--}}
                {{--</a></li>--}}
                {{--<li><a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViZko2eWdFc0laekk/view">--}}
                    {{--ΔΙΑΔΙΚΑΣΙΑ ΤΟΠΟΘΕΤΗΣΕΩΝ ΣΕ ΛΕΙΤΟΥΡΓΙΚΑ ΚΕΝΑ--}}
                {{--</a></li>--}}

                <li><a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViYTZSaXltaVdFZzg/view?usp=sharing">Σχολεία κατά Δήμους - Μόρια Σχολείων</a></li>
                <li><a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViQUI2RUl0dElTUGc/view?usp=sharing">Ομάδες Σχολείων Ν. Χανίων</a></li>
            </ol>
        </div>
    {{--</div>--}}



    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <table width="100%" cellpadding="10" cellspacing="10">
                @can('create_Οργ_request')
                    @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
                        <tr>
                            <td>
                                @can('create_Μοριοδ_request')
                                    @include('teacher.aitisis._available_veltiwseis', [
                                        'title' => 'Αίτηση μοριοδότησης μετάθεσης (συμπλήρωση ωραρίου)',
                                        'type'=>'Αίτηση-Μοριοδότησης-Μετάθεσης',
                                        'description'=>'Αίτηση επαναυπολογισμού μορίων μετάθεσης (για συμπλήρωση ωραρίου)',
                                        'file'=>'Μοριοδ'
                                    ])
                                @else
                                    @include('teacher.aitisis._not_available-veltiwseis', [
                                        'type'=>'Αίτηση-Μοριοδότησης-Μετάθεσης',
                                        'file'=>'Μοριοδ'
                                    ])
                                @endcan
                            </td>
                            <td>
                                @can('create_ΜοριοδΑ_request')
                                    @include('teacher.aitisis._available_veltiwseis', [
                                        'title' => 'Αίτηση μοριοδότησης απόσπασης',
                                        'type'=>'Αίτηση-Μοριοδότησης-Απόσπασης',
                                        'description'=>'Αίτηση υπολογισμού μορίων απόσπασης (εντός ΠΥΣΔΕ)',
                                        'file'=>'ΜοριοδΑ'
                                    ])
                                @else
                                    @include('teacher.aitisis._not_available-veltiwseis', [
                                        'type'=>'Αίτηση-Μοριοδότησης-Απόσπασης',
                                        'file'=>'ΜοριοδΑ'
                                    ])
                                @endcan
                            </td>
                        </tr>
                        <tr>
                            <td>
                                @can('create_Υπεραρ_request')
                                    @include('teacher.aitisis._available_veltiwseis', [
                                        'title' => 'Αίτηση για Υπεραριθμία',
                                        'type'=>'Αίτηση-Υπεραριθμίας',
                                        'description'=>'Αίτηση επιθυμίας για χαρακτηρισμό ως ονομαστικά Υπεράριθμος',
                                        'file'=>'Υπεραρ'
                                    ])
                                @else
                                    @include('teacher.aitisis._not_available-veltiwseis', [
                                        'type'=>'Αίτηση-Υπεραριθμίας',
                                        'file'=>'Υπεραρ'
                                    ])
                                @endcan

                            </td>
                            <td>
                                @can('create_ΟργανικήΥπ_request')
                                    @include('teacher.aitisis._available_veltiwseis', [
                                        'title' => 'Αίτηση για Οργανική σε Υπεράριθμο',
                                        'type'=>'Αίτηση-Οργανικής-Υπεράριθμου',
                                        'description'=>'Αίτηση - Δήλωση προτίμησης Σχολείου - Σχολείων ΜΟΝΟ για Οργανικά Υπεράριθμους',
                                        'file'=>'ΟργανικήΥπ'
                                    ])
                                @else
                                    @include('teacher.aitisis._not_available-veltiwseis', [
                                        'type'=>'Αίτηση-Οργανικής-Υπεράριθμου',
                                        'file'=>'ΟργανικήΥπ'
                                    ])
                                @endcan
                            </td>
                            <td>
                                @can('create_Βελτίωση_request')
                                    @include('teacher.aitisis._available_veltiwseis', [
                                        'title' => 'Αίτηση για Βελτίωση - Οριστική Τοποθέτηση',
                                        'type'=>'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης',
                                        'description'=>'Αίτηση - Δήλωση πρότιμησης Σχολείου - Σχολείων για βελτίωση - οριστική τοποθέτηση',
                                        'file'=>'Βελτίωση'
                                    ])
                                @else
                                    @include('teacher.aitisis._not_available-veltiwseis', [
                                        'type'=>'Αίτηση-Βελτίωσης-Οριστικής-Τοποθέτησης',
                                        'file'=>'Βελτίωση'
                                    ])
                                @endcan
                            </td>
                        </tr>
                    @endif
                @endcan
                {{--<tr>--}}
                    {{--@if(\Auth::user()->userable->teacherable_type == 'App\Anaplirotis')--}}
                    {{--<td>--}}
                        {{--@can('create_E5_request')--}}
                            {{--@include('teacher.aitisis._available',['type'=>'E5', 'description'=> 'Δήλωση αναπληρωτή'])--}}
                        {{--@else--}}
                            {{--@include('teacher.aitisis._not_available',['type'=>'E5'])--}}
                        {{--@endcan--}}
                    {{--</td>--}}
                    {{--@endif--}}
                    {{--<td width="32%">--}}
                        {{--<div class="alert alert-danger alert-dismissible" role="alert">--}}
                          {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                          {{--<strong>Προσοχή!</strong>--}}
                          {{--<p>--}}
                                {{--Για το σωστό υπολογισμό των μορίων, όσοι καθηγητές <strong>ΔΕΝ</strong> κατέθεσαν αίτηση Μετάθεσης/Βελτίωσης το Δεκέμβρη του 2015 και δηλώνουν:--}}
                                {{--<ul>--}}
                                    {{--<li>Εντοπιότητα</li>--}}
                                    {{--<li>Συνηπηρέτηση</li>--}}
                                    {{--<li>Οικογενειακή Κατάσταση (για τους φοιτητές απαιτείται βεβαίωση Σχολής)</li>--}}
                                    {{--<li>Αριθμό παιδιών</li>--}}
                                    {{--<li>Ειδική Κατηγορία (σοβαροί λόγοι Υγείας)</li>--}}
                                {{--</ul>--}}
                                {{--<strong>--}}
                                    {{--θα πρέπει,  (σύμφωνα με τις υπ αριθμ 200986/Ε2/9-12-2015 εγκύκλιο Μεταθέσεων ΔΕ 2015-2016 - 67505/Ε1/20-04-2016 εγκύκλιο Αποσπάσεων ΔΕ 2015-2016) ,μέχρι τη λήξη των αιτήσεων να προσκομίσουν  τα απαραίτητα δικαιολογητικά.--}}
                                {{--</strong>--}}
                          {{--</p>--}}
                        {{--</div>--}}
                    {{--</td>--}}
                    {{--<td width="32%" align="center">--}}
                        {{--@can('create_ΑΠΛΗ_request')--}}
                            {{--@include('teacher.aitisis._available',['type'=>'ΑΠΛΗ', 'description'=> 'ή Ένσταση'])--}}
                        {{--@else--}}
                            {{--@include('teacher.aitisis._not_available',['type'=>'ΑΠΛΗ'])--}}
                        {{--@endcan--}}
                    {{--</td>--}}
                    {{--<td width="31%">--}}
                        {{--<div class="alert alert-info alert-dismissible" role="alert">--}}
                          {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                          {{--<strong>Σημείωση!</strong>--}}
                          {{--<p>--}}
                                {{--Οι αιτήσεις των εκπαιδευτικών προς το ΠΥΣΔΕ γίνονται μόνο ηλεκτρονικά μέσα από την Ηλεκτρονική Πλατφόρμα.--}}
                                {{--Αν για οποιοδήποτε λόγο αντιμετωπίζεται πρόβλημα ή υπάρχουν απορίες στον τρόπο υποβολής παρακαλούμε--}}
                                {{--επικοινωνήστε με τη Δ.Δ.Ε. Χανίων.--}}
                          {{--</p>--}}
                        {{--</div>--}}
                    {{--</td>--}}

                {{--</tr>--}}


                    @if(\Auth::user()->userable->teacherable_type == 'App\Monimos')
                        <tr align="center">
                            <td>
                            @if(\Auth::user()->userable->teacherable->county == \Config::get('requests.default_county'))
                                @can('create_E1_request')
                                    @include('teacher.aitisis._available',['type'=>'E1', 'description'=> 'Αίτηση - Δήλωση Λειτουργικά υπεράριθμων, Διάθεση ΠΥΣΔΕ, μετάθεση'])
                                @else
                                    @include('teacher.aitisis._not_available',['type'=>'E1'])
                                @endcan
                            @endif
                            </td>

                            <td>
                                @can('create_E2_request')
                                    @include('teacher.aitisis._available',['type'=>'E2', 'description'=> 'Αίτηση - Δήλωση για συμπλήρωση ωραρίου'])
                                @else
                                    @include('teacher.aitisis._not_available',['type'=>'E2'])
                                @endcan
                            </td>


                            <td>
                                @if(\Auth::user()->userable->teacherable->county == \Config::get('requests.default_county'))
                                    @can('create_E3_request')
                                        @include('teacher.aitisis._available',['type'=>'E3', 'description'=> 'Δήλωση απόσπασης εντός ΠΥΣΔΕ'])
                                    @else
                                        @include('teacher.aitisis._not_available',['type'=>'E3'])
                                    @endcan
                                @endif
                            </td>

                        </tr>
                        <tr align="center">
                            <td>
                                @if(\Auth::user()->userable->teacherable->county != \Config::get('requests.default_county'))
                                    @can('create_E4_request')
                                        @include('teacher.aitisis._available',['type'=>'E4', 'description'=> 'Δήλωση προτίμησης Αποσπασμένου από άλλο ΠΥΣΔΕ'])
                                    @else
                                        @include('teacher.aitisis._not_available',['type'=>'E4'])
                                    @endcan
                                @endif
                            </td>
                            <td>
                                @can('create_E6_request')
                                    @include('teacher.aitisis._available',['type'=>'E6', 'description'=> 'για συμπλήρωση στην Πρωτοβάθμια'])
                                @else
                                    @include('teacher.aitisis._not_available',['type'=>'E6'])
                                @endcan
                            </td>
                        </tr>
                    @endif

            </table>
        </div>
    </div>

@endsection