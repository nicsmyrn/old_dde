   <div v-show="showReason" class="form-group">
        <label>Καταθέτω την αίτηση γιατί:</label>
        <select v-model="reason" v-on="change:showSubFields" name="reason">
            <option value="null"></option>
            @foreach(Config::get('requests.reason') as $k=>$v)
                <option value="{{$k}}">{{$v}}</option>
            @endforeach
        </select>
   </div>

   <div v-show="showStay" class="form-group">
        <label>Επιθυμώ να διατηρήσω το ωράριο της οργανικής μου:</label>
        <select v-model="stay" name="stay_to_organiki">
            <option value="null"></option>
           <option value="0">ΟΧΙ</option>
           <option value="1">ΝΑΙ</option>
        </select>
   </div>
   <div v-show="showHoursRemaining"   class="form-group">
        <label for="hours_for_request">
            Ώρες προς συμπλήρωση σε άλλο σχολείο:
            <input v-model="hours_for_request" type="text" name="hours_for_request"/>
        </label>
    </div>
     <h5>Παρατηρήσεις - Σχόλια</h5>
     <textarea v-model="description" name="description"></textarea>