@extends('app')

@section('title')
    {!! $school->name !!}
@endsection

@section('header.style')
    <link href="/css/loader.css" rel="stylesheet"/>
@endsection

@section('loader')
    @include('loader')
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						Καταχώρηση Κενών - Πλεονασμάτων
						<div class="pull-right"><div id="clock"></div></div>
					</div>
                    <div class='panel-body'>
                        @include('school.edit_panel')
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('scripts.footer')
	@include('school.edit_footer')
@endsection

