<br>
<div class="row">

    <div class="col-md-4">
        <table class="table table-bordered">
            <thead>
                <tr style="background-color: rgba(0, 0, 0, 0.62); color: #ffffff">
                    <th></th>
                    <th class="text-center">Πλεόνασμα</th>
                    <th class="text-center">Έλλειμμα</th>
                </tr>
            </thead>
            <tbody>

                @foreach($school->pe04() as $index => $eidikotita)
                    @if($eidikotita->id == 34)  <!-- $index == 3 -->
                        <tr>
                            <td>{!! $eidikotita->full_name !!}</td>
                            <td>

                                <input type="text" class="@if($eidikotita->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                                " name="eidikot[{!! $eidikotita->id !!}]" value="{!! $eidikotita->pivot->value < 0 ? 0 : $eidikotita->pivot->value !!}"

                                @if(\Auth::user()->isRole('pysde_secretary'))
                                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita->date_modified !!} από {!! $eidikotita->last_user!!}"
                                @endif
                                >
                            </td>
                            <td rowspan="5">
                                <input  class="@if($eidikotita->pivot->value<0)form-control negative text-center @else form-control text-center @endif"
                                        type="text"
                                        name="eidikotitaPE04"
                                        value="{!! $eidikotita->pivot->value > 0 ? 0 : $eidikotita->pivot->value !!}"
                                        style="height: 160px; font-size: 18pt"
                                /> </td>
                        </tr>
                    @else
                        <tr>
                            <td>{!! $eidikotita->full_name !!}</td>
                            <td>
                                <input type="text" class="@if($eidikotita->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                                " name="eidikot[{!! $eidikotita->id !!}]" value="{!! $eidikotita->pivot->value !!}"

                                @if(\Auth::user()->isRole('pysde_secretary'))
                                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita->date_modified !!} από {!! $eidikotita->last_user!!}"
                                @endif
                                >
                            </td>
                        </tr>
                    @endif
                @endforeach

            </tbody>
        </table>
    </div>

    <div class="col-md-8">
        @foreach($school->gymnasiumEidikotites() as $eidikotita)
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-7 control-label">{!! $eidikotita->full_name !!}</label>
                    <div class="col-md-3">
                        <input type="text" class="@if($eidikotita->pivot->value<0)form-control negative text-center @elseif($eidikotita->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                        " name="eidikot[{!! $eidikotita->id !!}]" value="{!! $eidikotita->pivot->value !!}"

                        @if(\Auth::user()->isRole('pysde_secretary'))
                            data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita->date_modified !!} από {!! $eidikotita->last_user!!}"
                        @endif
                        >
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!--    ΕΙΔΙΚΟΤΗΤΑ ΠΕ04 !-->

</div>


<div class="row">
    @foreach($school->eeeek() as $eidikotita)
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-7 control-label">{!! $eidikotita->full_name !!}</label>
                        <div class="col-md-3">
                            <input type="text" class="@if($eidikotita->pivot->value<0)form-control negative text-center @elseif($eidikotita->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                            " name="eidikot[{!! $eidikotita->id !!}]" value="{!! $eidikotita->pivot->value !!}"
                            @if(\Auth::user()->isRole('pysde_secretary'))
                                data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita->date_modified !!} από {!! $eidikotita->last_user!!}"
                            @endif
                            >
                        </div>
                    </div>
                </div>
    @endforeach
</div>

<div class="row">
    <div class="col-md-10 col-md-offset-1">
        <div class="checkbox">
            <h3><label>{!! Form::checkbox('texnologia', 1, false,['id'=>'checkbox_tech']) !!}<span class="label label-default"> Εμφάνιση επιπλέον κλάδων (μόνο πλεόνασμα.)</span></label></h3>
        </div>
        <div id="tech_eidikotites" style="display: none">
            @foreach($school->kanonikes_eidikotites()->where('plus', 'extra') as $eidikotita)
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-7 control-label">{!! $eidikotita->full_name !!}</label>
                        <div class="col-md-3">
                            <input type="text" class="@if($eidikotita->pivot->value<0)form-control negative text-center @elseif($eidikotita->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                            " name="eidikot[{!! $eidikotita->id !!}]" value="{!! $eidikotita->pivot->value !!}"
                            @if(\Auth::user()->isRole('pysde_secretary'))
                                data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita->date_modified !!} από {!! $eidikotita->last_user!!}"
                            @endif
                            >
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
                <br>