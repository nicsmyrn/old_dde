<div class="col-md-3">
    <div class="form-group">
        <label class="col-md-7 control-label">Project</label>
        <div class="col-md-4">
            <input type="text" class="form-control @if($school->mathimata->where('name', 'Project')->first()->pivot->value < 0) negative @endif text-center" name="lesson[{!! $school->mathimata->where('name', 'Project')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'Project')->first()->pivot->value !!}"
            @if(\Auth::user()->isRole('pysde_secretary'))
                data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'Project')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'Project')->first()->last_user!!}"
            @endif
            >
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-7 control-label">ΠΕ09-10-13</label>
        <div class="col-md-4">
            <input type="text" class="form-control @if($school->mathimata->where('name', 'pe09-10-13')->first()->pivot->value < 0) negative @endif text-center" name="lesson[{!! $school->mathimata->where('name', 'pe09-10-13')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'pe09-10-13')->first()->pivot->value !!}"
            @if(\Auth::user()->isRole('pysde_secretary'))
                data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'Project')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'Project')->first()->last_user!!}"
            @endif            
            >
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-7 control-label">Γραμμικό Σχέδιο</label>
        <div class="col-md-4">
            <input type="text" class="form-control @if($school->mathimata->where('name', 'sxedio')->first()->pivot->value < 0) negative @endif text-center" name="lesson[{!! $school->mathimata->where('name', 'sxedio')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'sxedio')->first()->pivot->value !!}"
                @if(\Auth::user()->isRole('pysde_secretary'))
                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'Project')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'Project')->first()->last_user!!}"
                @endif            
            >
        </div>
    </div>
</div>

<div class="col-md-7">
    <div class="alert alert-warning" role="alert">
        <h4 class="text-center">Οδηγίες:</h4>
        <p><strong>1. </strong> Στα μαθήματα <strong>Project</strong> και <strong>Γραμμικό Σχέδιο</strong> απλά γράφετε το κενό που υπάρχει στο Σχολείο (η ειδικότητα δεν χρειάζεται να αναφέρεται). </p>
        <p><strong>2. </strong> Το <u>ΠΕ09-10-13</u> είναι το άθροισμα των κενών ωρών που υπάρχουν στα μαθήματα:
            <ul>
                <li><strong>Πολιτική Παιδεία</strong></li>
                <li><strong>Βασικές Αρχές Κοινωνικών Επιστημών</strong></li>
            </ul>
        </p>
    </div>
</div>

