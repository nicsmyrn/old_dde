                <br>
                <div class="row">
                    @foreach($school->kanonikes_eidikotites() as $eidikotita)

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-7 control-label">{!! $eidikotita->full_name !!}</label>
                                <div class="col-md-2">
                                    <input type="text" class="@if($eidikotita->pivot->value<0)form-control negative text-center @elseif($eidikotita->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                                    " name="eidikot[{!! $eidikotita->id !!}]" value="{!! $eidikotita->pivot->value !!}"
                                    @if(\Auth::user()->isRole('pysde_secretary'))
                                        data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita->date_modified !!} από {!! $eidikotita->last_user!!}"
                                    @endif
                                    >
                                </div>

                            </div>
                        </div>

                    @endforeach

                    @foreach($school->eeeek() as $eidikotita)
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-7 control-label">{!! $eidikotita->full_name !!}</label>
                                        <div class="col-md-3">
                                            <input type="text" class="@if($eidikotita->pivot->value<0)form-control negative text-center @elseif($eidikotita->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                                            " name="eidikot[{!! $eidikotita->id !!}]" value="{!! $eidikotita->pivot->value !!}"
                                            @if(\Auth::user()->isRole('pysde_secretary'))
                                                data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita->date_modified !!} από {!! $eidikotita->last_user!!}"
                                            @endif
                                            >
                                        </div>
                                    </div>
                                </div>
                    @endforeach
                </div>

                <br>