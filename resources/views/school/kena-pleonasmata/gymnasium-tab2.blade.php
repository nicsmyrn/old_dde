<div class="col-md-4">

    <table class="table">
        <thead>
            <tr class="text-center">
                <td></td><td><strong>Πλεόνασμα</strong></td><td><strong>Κενό</strong></td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><strong>Αγγλικά</strong></td>
                <td><input class="form-control @if($school->mathimata->where('name', 'english+')->first()->pivot->value > 0) positive @endif text-center" type="text" name="lesson[{!! $school->mathimata->where('name', 'english+')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'english+')->first()->pivot->value !!}"
                @if(\Auth::user()->isRole('pysde_secretary'))
                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'english+')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'english+')->first()->last_user!!}"
                @endif
                ></td>
                <td><input class="form-control @if($school->mathimata->where('name', 'english-')->first()->pivot->value < 0) negative @endif text-center" type="text" name="lesson[{!! $school->mathimata->where('name', 'english-')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'english-')->first()->pivot->value !!}"
                @if(\Auth::user()->isRole('pysde_secretary'))
                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'english-')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'english-')->first()->last_user!!}"
                @endif
                ></td>
            </tr>
            <tr>
                <td><strong>Γαλλικά</strong></td>
                <td><input class="form-control @if($school->mathimata->where('name', 'french+')->first()->pivot->value > 0 ) positive @endif text-center" type="text" name="lesson[{!! $school->mathimata->where('name', 'french+')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'french+')->first()->pivot->value !!}"
                @if(\Auth::user()->isRole('pysde_secretary'))
                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'french+')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'french+')->first()->last_user!!}"
                @endif
                ></td>
                <td><input class="form-control @if($school->mathimata->where('name', 'french-')->first()->pivot->value < 0 ) negative @endif text-center" type="text" name="lesson[{!! $school->mathimata->where('name', 'french-')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'french-')->first()->pivot->value !!}"
                @if(\Auth::user()->isRole('pysde_secretary'))
                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'french-')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'french-')->first()->last_user!!}"
                @endif
                ></td>
            </tr>
            <tr>
                <td><strong>Γερμανικά</strong></td>
                <td><input class="form-control @if($school->mathimata->where('name', 'german+')->first()->pivot->value > 0) positive @endif text-center" type="text" name="lesson[{!! $school->mathimata->where('name', 'german+')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'german+')->first()->pivot->value !!}"
                @if(\Auth::user()->isRole('pysde_secretary'))
                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'german+')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'german+')->first()->last_user!!}"
                @endif
                ></td>
                <td><input class="form-control @if($school->mathimata->where('name', 'german-')->first()->pivot->value < 0) negative @endif text-center" type="text" name="lesson[{!! $school->mathimata->where('name', 'german-')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'german-')->first()->pivot->value !!}"
                @if(\Auth::user()->isRole('pysde_secretary'))
                    data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'german-')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'german-')->first()->last_user!!}"
                @endif
                ></td>
            </tr>
        </tbody>
    </table>

</div>

<div class="col-md-3">
    <div class="form-group" style="display: none">
        <label class="col-md-7 control-label">Project</label>
        <div class="col-md-4">
            <input type="text" class="form-control @if($school->mathimata->where('name', 'Project')->first()->pivot->value < 0) negative @endif text-center" name="lesson[{!! $school->mathimata->where('name', 'Project')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'Project')->first()->pivot->value !!}"
            @if(\Auth::user()->isRole('pysde_secretary'))
                data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: aa {!! $school->mathimata->where('name', 'Project')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'Project')->first()->last_user!!}"
            @endif
            >
        </div>
    </div>
    <div class="form-group">
        <label class="col-md-7 control-label">Τεχνολογία</label>
        <div class="col-md-4">
           <input type="text" class="form-control @if($school->mathimata->where('name', 'Technology')->first()->pivot->value < 0) negative @endif text-center" name="lesson[{!! $school->mathimata->where('name', 'Technology')->first()->id !!}]" value="{!! $school->mathimata->where('name', 'Technology')->first()->pivot->value !!}"
            @if(\Auth::user()->isRole('pysde_secretary'))
                data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $school->mathimata->where('name', 'Technology')->first()->date_modified !!} από {!! $school->mathimata->where('name', 'Technology')->first()->last_user!!}"
            @endif
           >
        </div>
    </div>
</div>

<div class="col-md-4">
    <div class="alert alert-warning" role="alert">
        <h4 class="text-center">Οδηγίες:</h4>
        <p><strong>1. </strong> Ο πίνακας των ξενόγλωσσων μαθημάτων δίνει τη δυνατότητα στο Γυμνάσιο να δηλώσει εαν υπάρχουν κενά & πλεονάσματα όπως στη περίπτωση που το μάθημα χωρίζεται σε Αρχάριους - Προχωρημένους και χρειάζονται δύο (2) καθηγητές.</p>
        <p><strong>2 </strong> Στο μάθημα της τεχνολογίας γράφετε το κενό που υπάρχει στο Σχολείο (η ειδικότητα δεν χρειάζεται να αναφέρεται). </p>
    </div>
</div>
