
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Υπάρχει κάποιο πρόβλημα με τα στοιχεία.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
    @if(\Auth::user()->isRole('school'))
        <div class="row">
            <div class="col-md-">
                <h2 class="text-center">{!! $school->name !!}</h2>
            </div>
            <div class="col-md-3">
                <!--<h4><div id="clock"></div></h4>-->
            </div>
        </div>
    @endif



    <h6 class="text-center">Τελευταία τροποποίηση: <strong>{!! $school->last_update !!}</strong></h6>

@if(\Auth::user()->isRole('school'))
    {!! Form::open(['action'=>['SchoolController@update',$school->slug_name], 'method'=>'PATCH', 'class'=>'form-horizontal']) !!}
@elseif(\Auth::user()->isRole('pysde_secretary'))
    {!! Form::open(['action'=>['KenaController@update',$school->slug_name], 'method'=>'PATCH', 'class'=>'form-horizontal']) !!}
@endif
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                {!! Form::submit('Αποθήκευση', ['class'=>'btn btn-primary btn-lg button-submit']) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('description', 'Σημειώσεις:', ['class'=>'form-label']) !!}
                {!! Form::textarea('description', $school->description, [
                        'class'=>'form-control',
                        'rows'=>'2',
                        'style'=>'background:#FFF5EE',
                        'data-toggle'=>'tooltip',
                        'data-placement'=>'top',
                        'title' => 'Οποιαδήποτε επιπλέον πληροφορία ή σημείωση για το συμβούλιο του ΠΥΣΔΕ παρακαλώ σημειώστε το εδώ.'
                        ]) !!}
            </div>
        </div>
        <br>
        <div>
          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="#kena_pleonasmata" aria-controls="kena_pleonasmata" role="tab" data-toggle="tab" style="background-color: #FFE4C4">Κενά - Πλεονάσματα</a></li>
              @if($school->type != 'ΕΠΑΛ' && $school->type != 'Ειδικό')
                <li role="presentation"><a href="#lessons" aria-controls="lessons" role="tab" data-toggle="tab" style="background-color:#BC8F8F">Κενά Μαθημάτων</a></li>
              @endif
              @if ($school->type != 'Ειδικό')
                <li role="presentation"><a href="#parallili" aria-controls="parallili" role="tab" data-toggle="tab" style="background-color:#B0C4DE">Παράλληλη Στήριξη</a></li>
              @endif
              @if(!$school->eidikis->isEmpty())
                    <li role="presentation"><a href="#eidikis" aria-controls="eidikis" role="tab" data-toggle="tab" style="background-color:#989898">Ειδική Αγωγή (τμήματα ένταξης)</a></li>
              @endif
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active" id="kena_pleonasmata" style="background-color:#FFE4C4">
                @if($school->type == 'Γυμνάσιο')
                    @include('school.kena-pleonasmata.gymnasium-tab1')
                @elseif($school->type == 'Λύκειο')
                    @include('school.kena-pleonasmata.lykeio-tab1')
                @elseif($school->type == 'ΕΠΑΛ')
                    @include('school.kena-pleonasmata.epal-tab1')
                @elseif($school->type == 'Ειδικό')
                    @include('school.kena-pleonasmata.eidiko-tab1')
                @endif
            </div>
            @if($school->type != 'ΕΠΑΛ')
                <div role="tabpanel" class="tab-pane fade" id="lessons" style="background-color:#BC8F8F">
                    <br>
                    <div class="container">
                        <div class="row">
                            @if($school->type == 'Γυμνάσιο')
                                @include('school.kena-pleonasmata.gymnasium-tab2')
                            @elseif($school->type == 'Λύκειο')
                                @include('school.kena-pleonasmata.lykeio-tab2')
                            @endif
                        </div>
                    </div>
                </div>
            @endif


            @if($school->type != 'Ειδικό')
                <div role="tabpanel" class="tab-pane fade" id="parallili" style="background-color:#B0C4DE">
                 <br>
                 <div class="container">
                     <div class="row">
                        <div class="col-md-6">
                            <table class="table">
                                <thead>
                                    <tr class="text-center">
                                        <td></td><td><strong>Ειδική Αγωγή</strong></td><td><strong>Braile</strong></td><td><strong>Νοηματική</strong></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><strong>Φιλόλογοι [ΠΕ02.50]</strong></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'fi-eidikis')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'fi-eidikis')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'fi-eidikis')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'fi-eidikis')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'fi-braile')->first()->pivot->value > 0) positive @elseif($school->eidikotites->where('name', 'fi-braile')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'fi-braile')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'fi-braile')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'fi-noimatiki')->first()->pivot->value > 0) positive @elseif($school->eidikotites->where('name', 'fi-noimatiki')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'fi-noimatiki')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'fi-noimatiki')->first()->pivot->value !!}"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Μαθηματικοί [ΠΕ03.50]</strong></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'm-eidikis')->first()->pivot->value > 0) positive @elseif($school->eidikotites->where('name', 'm-eidikis')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'm-eidikis')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'm-eidikis')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'm-braile')->first()->pivot->value > 0) positive @elseif($school->eidikotites->where('name', 'm-braile')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'm-braile')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'm-braile')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'm-noimatiki')->first()->pivot->value > 0) positive @elseif($school->eidikotites->where('name', 'm-noimatiki')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'm-noimatiki')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'm-noimatiki')->first()->pivot->value !!}"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Φυσικοί [ΠΕ04.01.50]</strong></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'fu-eidikis')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'fu-eidikis')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'fu-eidikis')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'fu-eidikis')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'fu-braile')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'fu-braile')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'fu-braile')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'fu-braile')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'fu-noimatiki')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'fu-noimatiki')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'fu-noimatiki')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'fu-noimatiki')->first()->pivot->value !!}"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Χημικοί [ΠΕ04.02.50]</strong></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'xi-eidikis')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'xi-eidikis')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'xi-eidikis')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'xi-eidikis')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'xi-braile')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'xi-braile')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'xi-braile')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'xi-braile')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'xi-noimatiki')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'xi-noimatiki')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'xi-noimatiki')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'xi-noimatiki')->first()->pivot->value !!}"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Βιολόγοι [ΠΕ04.04.50]</strong></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'bio-eidikis')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'bio-eidikis')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'bio-eidikis')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'bio-eidikis')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'bio-braile')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'bio-braile')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'bio-braile')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'bio-braile')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'bio-noimatiki')->first()->pivot->value > 0 )positive @elseif($school->eidikotites->where('name', 'bio-noimatiki')->first()->pivot->value < 0 ) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'bio-noimatiki')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'bio-noimatiki')->first()->pivot->value !!}"></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Γεωλόγοι [ΠΕ04.05.50]</strong></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'geo-eidikis')->first()->pivot->value  > 0)positive @elseif($school->eidikotites->where('name', 'geo-eidikis')->first()->pivot->value  < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'geo-eidikis')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'geo-eidikis')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'geo-braile')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'geo-braile')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'geo-braile')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'geo-braile')->first()->pivot->value !!}"></td>
                                        <td><input class="form-control @if($school->eidikotites->where('name', 'geo-noimatiki')->first()->pivot->value > 0)positive @elseif($school->eidikotites->where('name', 'geo-noimatiki')->first()->pivot->value < 0) negative @endif text-center" type="text" name="eidikot[{!! $school->eidikotites->where('name', 'geo-noimatiki')->first()->id !!}]" value="{!! $school->eidikotites->where('name', 'geo-noimatiki')->first()->pivot->value !!}"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-4">
                            <div class="alert alert-warning" role="alert">
                                <h4 class="text-center">Οδηγίες:</h4>
                                <p>
                                    Σε περίπτωση που το Σχολείο έχει παράλληλη στήριξη δηλώστε την Ειδικότητα
                                    που υπάρχει κενό ανάλογα τον τύπο της παράλληλης:
                                    <ol>
                                        <li><strong>Ειδική Αγωγή</strong></li>
                                        <li><strong>Braile</strong></li>
                                        <li><strong>Νοηματική</strong></li>
                                    </ol>
                                </p>
                            </div>
                        </div>
                     </div>
                 </div>
             </div>
            @endif

           @if(!$school->eidikis->isEmpty())
               <div role="tabpanel" class="tab-pane fade" id="eidikis" style="background-color:#989898">
                      <br>
                      <div class="container">
                          <div class="row">
                                @foreach($school->eidikis as $eidikotita_eidikis)
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-7 control-label">{!! $eidikotita_eidikis->full_name !!}</label>
                                            <div class="col-md-3">
                                                <input type="text" class="@if($eidikotita_eidikis->pivot->value<0)form-control negative text-center @elseif($eidikotita_eidikis->pivot->value>0)form-control positive text-center @else form-control text-center @endif
                                                " name="eidikis[{!! $eidikotita_eidikis->id !!}]" value="{!! $eidikotita_eidikis->pivot->value !!}"
                                                    @if(\Auth::user()->isRole('pysde_secretary'))
                                                        data-toggle="tooltip" data-placement="top" title="Τελευταία τροποποίηση: {!! $eidikotita_eidikis->date_modified !!} από {!! $eidikotita_eidikis->last_user!!}"
                                                    @endif
                                                >
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                          </div>
                      </div>
               </div>
           @endif

      </div>
        </div>
        <br>
        <div class="row text-center">
            {!! Form::submit('Αποθήκευση', ['class'=>'btn btn-primary btn-lg button-submit']) !!}
        </div>
    {!! Form::close() !!}
