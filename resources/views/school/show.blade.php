@extends('app')

@section('title')
    {!! $name !!}
@endsection

@section('content')

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Εμφάνιση Κενών - Πλεονασμάτων</div>
					<div class="panel-body">
						@can('read_All_Schools_Kena')
							<div class="row">
								<div class="col-md-3">

								</div>
								<div class="col-md-6">
									<h3 class="text-center">
							        <select class="col-md-12">
							        	<option></option>
							            @foreach($schools as $school)
							            	<option value="{!! action('KenaController@show',str_replace(' ', '-', $school)) !!}"
			       			                	@if(str_replace(' ', '-', $name) == str_replace(' ', '-', $school))
					                				selected
					                			@endif     	
							            	>{!! $school !!}</option>	
							            @endforeach                      	
							        </select>
							        </h3>	
								</div>
								<div class="col-md-3">
									@can('edit_school_kena')
										<a href="{!! action('KenaController@edit', str_replace(' ', '-', $name)) !!}" class="btn btn-warning">Επεξεργασία</a>
									@endcan
								</div>
							</div>
						@else
	                    	<h2 class="text-center">{!! $name !!}</h2>
                    	@endcan
                    	<h4 class="text-center"><a class="btnPrint btn btn-success">Εκτύπωση</a></h4>
	                    <h6 class="text-center">Τελευταία τροποποίηση: <strong>{!! $last_update !!}</strong></h6>



	                            	@if($eidikotites->isEmpty())
										<div class="alert alert-warning text-center" role="alert">
										  <strong>ΔΕΝ</strong> υπάρχουν κενά - πλεονάσματα στο Σχολείο
										</div>
	                            	@else
    			                        <table class="table table-bordered">
				                            <theader>
				                                <tr class="active">
			    	                                <th>Ειδικότητα</th>
			    	                                <th>Ώρες</th>
				                                </tr>
				                            </theader>
			                            	<tbody>
		                                    @foreach($eidikotites as $eidikotita)
			                                    <tr>
			                                            @if($eidikotita->pivot->value > 0)
			                                                <td class="success">{!! $eidikotita->full_name !!}</td>
			                                                <td class="text-center success">{!! $eidikotita->pivot->value !!}</td>                                            
			                                            @elseif($eidikotita->pivot->value < 0)
			                                                <td class="danger">{!! $eidikotita->full_name !!}</td>
			                                                <td class="text-center danger">{!! $eidikotita->pivot->value !!}</td>  
			                                            @else
			                                                <td>{!! $eidikotita->full_name !!}</td>
			                                                <td class="text-center">{!! $eidikotita->pivot->value !!}</td>  
			                                            @endif
			
			                                    </tr>
		                                    @endforeach	  
            	                            </tbody>
	                        			</table>

                                        @if($parallili->isEmpty())
                                            <div class="alert alert-warning text-center" role="alert">
                                              <strong>ΔΕΝ</strong> υπάρχουν κενά στην Παράλληλη Στήριξη
                                            </div>
                                        @else
	                        			<div><h4>Παράλληλη Στήριξη</h4></div>
    			                        <table class="table table-bordered">
				                            <theader>
				                                <tr class="active">
			    	                                <th>Ειδικότητα</th>
			    	                                <th>Ώρες</th>
				                                </tr>
				                            </theader>
			                            	<tbody>
		                                    @foreach($parallili as $par)
			                                    <tr>
			                                            @if($par->pivot->value > 0)
			                                                <td class="success">{!! $par->description !!}</td>
			                                                <td class="text-center success">{!! $par->pivot->value !!}</td>
			                                            @elseif($par->pivot->value < 0)
			                                                <td class="danger">{!! $par->description !!}</td>
			                                                <td class="text-center danger">{!! $par->pivot->value !!}</td>
			                                            @else
			                                                <td>{!! $par->description !!}</td>
			                                                <td class="text-center">{!! $par->pivot->value !!}</td>
			                                            @endif

			                                    </tr>
		                                    @endforeach
            	                            </tbody>
	                        			</table>
	                        			@endif
	                            	@endif

	                            	@if($mathimata->isEmpty())
										<div class="alert alert-warning text-center" role="alert">
										  <strong>ΔΕΝ</strong> υπάρχουν κενά - πλεονάσματα στα ειδικά μαθήματα
										</div>
	                            	@else
	                            	    <h4>Ειδικά Μαθήματα</h4>
    			                        <table class="table table-bordered">
				                            <theader>
				                                <tr class="active">
			    	                                <th>Μάθημα</th>
			    	                                <th>Ώρες</th>
				                                </tr>
				                            </theader>
			                            	<tbody>
		                                    @foreach($mathimata as $mathima)
			                                    <tr>
			                                            @if($mathima->pivot->value > 0)
			                                                <td class="success">{!! $mathima->description !!}</td>
			                                                <td class="text-center success">{!! $mathima->pivot->value !!}</td>
			                                            @elseif($mathima->pivot->value < 0)
			                                                <td class="danger">{!! $mathima->description !!}</td>
			                                                <td class="text-center danger">{!! $mathima->pivot->value !!}</td>
			                                            @else
			                                                <td>{!! $mathima->description !!}</td>
			                                                <td class="text-center">{!! $mathima->pivot->value !!}</td>
			                                            @endif

			                                    </tr>
		                                    @endforeach
            	                            </tbody>
	                        			</table>
	                            	@endif

	                            	@if($eidikis->isEmpty())
										<div class="alert alert-warning text-center" role="alert">
										  <strong>ΔΕΝ</strong> υπάρχουν κενά  στα Τμήματα Ένταξης
										</div>
	                            	@else
	                            	    <h4>Τμήματα Ένταξης</h4>
    			                        <table class="table table-bordered">
				                            <theader>
				                                <tr class="active">
			    	                                <th>Ειδικότητα</th>
			    	                                <th>Ώρες</th>
				                                </tr>
				                            </theader>
			                            	<tbody>
		                                    @foreach($eidikis as $eidiki)
			                                    <tr>
			                                            @if($eidiki->pivot->value > 0)
			                                                <td class="success">{!! $eidiki->name !!}</td>
			                                                <td class="text-center success">{!! $eidiki->pivot->value !!}</td>
			                                            @elseif($eidiki->pivot->value < 0)
			                                                <td class="danger">{!! $eidiki->name !!}</td>
			                                                <td class="text-center danger">{!! $eidiki->pivot->value !!}</td>
			                                            @else
			                                                <td>{!! $eidiki->name !!}</td>
			                                                <td class="text-center">{!! $eidiki->pivot->value !!}</td>
			                                            @endif

			                                    </tr>
		                                    @endforeach
            	                            </tbody>
	                        			</table>
	                            	@endif

                        <h4 class="text-center"><a class="btnPrint btn btn-success">Εκτύπωση</a></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@can('read_All_Schools_Kena')
	@section('scripts.footer')
		@include('pysde.kena_pleonasmata.edit_footer')
	@endsection

@endcan

@can('read_school_kena')
	@section('scripts.footer')
        <script>
            $(document).ready(function() {
                $(".btnPrint").printPage({
                    url: "{!! action('SchoolController@printShow', str_replace(' ', '-', $name)) !!}",
                    attr: "href",
                    message:"Εκτυπώνει"
                 });
            });
        </script>
    @endsection
@endcan