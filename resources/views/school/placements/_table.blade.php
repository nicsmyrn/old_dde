        <table id="teachers" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr class="active">
                    <th class="text-center">Ονοματεπώνυμο</th>
                    <th class="text-center">Πατρώνυμο</th>
                    @if(!$organiki)
                        <th class="text-center">Οργανική</th>
                    @endif
                    <th class="text-center">Κλάδος</th>
                    <th class="text-center">Ενέργειες</th>
                </tr>
            </thead>

            <tbody>
                @foreach($teachers as $teacher)
                    <tr>
                        <td>{!! $teacher->user->full_name !!}</td>
                        <td>{!! $teacher->middle_name !!}</td>
                        @if(!$organiki)
                            <td>{!! $teacher->teacherable->organiki_name !!}</td>
                        @endif
                        <td class="text-center">{!! $teacher->klados_name!!}</td>
                        <td class="text-center">
                            <a href="{!! action('SchoolPlacementsController@viewTeacherDetails', [$teacher->afm]) !!}"
                                class="btn btn-default btn-sm"
                                data-toggle="tooltip"
                                title="Λεπτομέρειες τοποθετήσεων..."
                            >
                                <i class="fa fa-info" aria-hidden="true"></i>
                            </a>
                            {{--<a  target="_blank"--}}
                                {{--data-toggle="tooltip"--}}
                                {{--title="όλα τα τοποθετήρια"--}}
                                {{--class="btn btn-primary btn-sm"--}}
                                {{--href="{!! action('SchoolPlacementsController@viewTeacherPlacementsPDF', [$teacher->id]) !!}"--}}
                            {{-->--}}
                                {{--<i class="fa fa-search" aria-hidden="true"></i>--}}
                            {{--</a>--}}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>