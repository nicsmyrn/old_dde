@extends('app')

@section('title')
    Τοποθετήσεις καθηγητών
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')

    <h1 class="page-heading">
        Καθηγητές που τοποθετήθηκαν στο Σχολείο μας
    </h1>

    @if($teachers->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">
                        Δεν υπάρχει κανένας καθηγητής που να τοποθετήθηκε από το ΠΥΣΔΕ
                    </div>
                </div>
            </div>
        </div>
    @else
        @include('school.placements._table',['organiki' => false])
    @endif

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();

            var table = $('#teachers').DataTable({
                    "order": [[ 0, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 4 ] } ],
                     "lengthMenu": [ [25, 50, -1], [25, 50, "Όλοι"] ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


