<script src="/js/jquery.countdown.min.js"></script>
<script>
    $(document).ready(function() {
        
        $('[data-toggle="tooltip"]').tooltip()

        $('input.form-control').on('click focus', function(e){
            $(this).addClass('editing')
                .val('');    
        });
        
        $('#clock').countdown('2016/09/20 12:00:00', function(event) {
            $(this).html(event.strftime('απομένουν <strong>%-H</strong> ώρες <strong>%-M</strong> λεπτά <strong>%-S</strong> δευτερόλεπτα'));
        })
        .on('finish.countdown', function(){
            location.reload();
        });

        $('.button-submit').on('click', function(){
            $('#loader').removeClass('invisible')
                .addClass('loading');
        });
        
        $("#checkbox_tech").on('change', function(){
            if (this.checked) $("#tech_eidikotites").show(1000);
            if (!this.checked) $("#tech_eidikotites").hide(1000);
        });
        
    });
</script>