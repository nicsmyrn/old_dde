@extends('app')

@section('header.style')

    <style type="text/css">
        /*  bhoechie tab */
        div.bhoechie-tab-container{
          z-index: 10;
          background-color: #ffffff;
          width: 95%;
          padding: 0 !important;
          border-radius: 4px;
          -moz-border-radius: 4px;
          border:1px solid #ddd;
          margin-top: 20px;
          margin-left: 50px;
          -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
          box-shadow: 0 6px 12px rgba(0,0,0,.175);
          -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
          background-clip: padding-box;
          opacity: 0.97;
          filter: alpha(opacity=97);
        }
        div.bhoechie-tab-menu{
          padding-right: 0;
          padding-left: 0;
          padding-bottom: 0;
        }
        div.bhoechie-tab-menu div.list-group{
          margin-bottom: 0;
        }
        div.bhoechie-tab-menu div.list-group>a{
          margin-bottom: 0;
        }
        div.bhoechie-tab-menu div.list-group>a .glyphicon,
        div.bhoechie-tab-menu div.list-group>a .fa {
          color: #5A55A3;
        }
        div.bhoechie-tab-menu div.list-group>a:first-child{
          border-top-right-radius: 0;
          -moz-border-top-right-radius: 0;
        }
        div.bhoechie-tab-menu div.list-group>a:last-child{
          border-bottom-right-radius: 0;
          -moz-border-bottom-right-radius: 0;
        }
        div.bhoechie-tab-menu div.list-group>a.active,
        div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
        div.bhoechie-tab-menu div.list-group>a.active .fa{
          background-color: #5A55A3;
          background-image: #5A55A3;
          color: #ffffff;
        }
        div.bhoechie-tab-menu div.list-group>a.active:after{
          content: '';
          position: absolute;
          left: 100%;
          top: 50%;
          margin-top: -13px;
          border-left: 0;
          border-bottom: 13px solid transparent;
          border-top: 13px solid transparent;
          border-left: 10px solid #5A55A3;
        }

        div.bhoechie-tab-content{
          background-color: #ffffff;
          /* border: 1px solid #eeeeee; */
          padding-left: 20px;
          padding-top: 10px;
        }

        div.bhoechie-tab div.bhoechie-tab-content:not(.active){
          display: none;
        }
    </style>
@endsection

@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item text-center">
                  <i class="fa fa-male fa-2x"></i><br/>Στοιχεία Λογαριασμού
                </a>
                <a href="{!! route('User::getChangePwd') !!}" class="list-group-item text-center">
                  <i class="fa fa-key fa-2x"></i><br/>Αλλαγή Κωδικού
                </a>
                <a href="{!! route('User::getChangeEmail') !!}" class="list-group-item text-center">
                  <i class="fa fa-paper-plane fa-2x"></i><br/>Αλλαγή E-mail
                </a>
                <a href="{!! route('User::getProfile') !!}" class="list-group-item active text-center">
                  <h4 class="glyphicon glyphicon-home"></h4><br/>Προφίλ
                </a>
              </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <div class="bhoechie-tab-content active">
                    <center>
                    <h2 style="margin-top: 0;color:#55518a">Προφίλ Χρήστη</h2>

                    @include('errors.list')

                    </center>
                </div>
            </div>
        </div>
  </div>
</div>
@endsection
