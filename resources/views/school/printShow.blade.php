    <center>
        <h4>Ηλεκτρονική Πλατφόρμα Δ.Δ.Ε. Χανίων</h4>

        <h2 class="text-center">{!! $name !!}</h2>

        <h6 class="text-center">Τελευταία τροποποίηση: <strong>{!! $last_update !!}</strong></h6>

    @if($eidikotites->isEmpty())
        <h6>
          <strong>ΔΕΝ</strong> υπάρχουν κενά - πλεονάσματα στο Σχολείο
        </h6>
    @else
        <table border="1" class="table table-bordered">
            <theader>
                <tr class="active">
                    <th>Ειδικότητα</th>
                    <th>Ώρες</th>
                </tr>
            </theader>
            <tbody>
            @foreach($eidikotites as $eidikotita)
                <tr>
                    <td>{!! $eidikotita->full_name !!}</td>
                    <td style="text-align: center">{!! $eidikotita->pivot->value !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        @if($parallili->isEmpty())
            <h6>
              <strong>ΔΕΝ</strong> υπάρχουν κενά στην Παράλληλη Στήριξη
            </h6>
        @else
            <div><h4>Παράλληλη Στήριξη</h4></div>
            <table border="1" class="table table-bordered">
                <theader>
                    <tr class="active">
                        <th>Ειδικότητα</th>
                        <th>Ώρες</th>
                    </tr>
                </theader>
                <tbody>
                @foreach($parallili as $par)
                    <tr>
                        <td>{!! $par->description !!}</td>
                        <td style="text-align: center">{!! $par->pivot->value !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @endif
    @endif


    @if($mathimata->isEmpty())
        <h6>
          <strong>ΔΕΝ</strong> υπάρχουν κενά - πλεονάσματα στα ειδικά μαθήματα
        </h6>
    @else
        <h4>Ειδικά Μαθήματα</h4>
        <table border="1" class="table table-bordered">
            <theader>
                <tr class="active">
                    <th>Μάθημα</th>
                    <th>Ώρες</th>
                </tr>
            </theader>
            <tbody>
            @foreach($mathimata as $mathima)
                <tr>
                    <td>{!! $mathima->description !!}</td>
                    <td style="text-align: center">{!! $mathima->pivot->value !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif

    @if($eidikis->isEmpty())
        <h6>
          <strong>ΔΕΝ</strong> υπάρχουν κενά  στα Τμήματα Ένταξης
        </h6>
    @else
        <h4>Τμήματα Ένταξης</h4>
        <table border="1" class="table table-bordered">
            <theader>
                <tr class="active">
                    <th>Ειδικότητα</th>
                    <th>Ώρες</th>
                </tr>
            </theader>
            <tbody>
            @foreach($eidikis as $eidiki)
                <tr>
                    <td>{!! $eidiki->name !!}</td>
                    <td style="text-align: center">{!! $eidiki->pivot->value !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @endif
    </center>
