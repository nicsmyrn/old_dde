@extends('app')

@section('title')
    Ευχαριστούμε
@endsection

@section('content')
    <div class="container">
        <h3 class="col-md-8 col-md-offset-2"><div class="alert alert-success" role="alert"> Τα στοιχεία καταχωρήθηκαν με επιτυχία. Ευχαριστούμε...</div> </h3>
    </div>
@endsection
