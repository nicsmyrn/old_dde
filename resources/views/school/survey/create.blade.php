@extends('app')

@section('title')
    Στοιχεία Σχολικής Μονάδας
@endsection

@section('header.style')

@endsection

@section('content')
<div class="container col-md-6 col-md-offset-3">
    <h2 class='text-center'>{!! $sch_name !!}</h2>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Υπάρχει κάποιο πρόβλημα με τα στοιχεία.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Στοιχεία Έρευνας</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['method'=>'POST', 'action'=>'SurveyController@postStoreSurvey','class'=>'form']) !!}

                <div class="row">
                    <div class="col-md-3">
                        <!-- Total Form Input -->
                         <div class="form-group">
                             {!! Form::label('total', 'Δυναμικότητα', ['class'=>'control-label']) !!}
                             {!! Form::text('total',Request::old('total'), ['class'=>'form-control', 'data-toggle'=>'tooltip', 'title'=>'Το σύνολο των μαθητών του Σχολείου']) !!}
                         </div>
                    </div>

                    <div class="col-md-3">
                         <div class="form-group">
                             {!! Form::label('studentpermeters', 'μαθητής/τ.μ.', ['class'=>'control-label']) !!}
                             {!! Form::text('studentpermeters',Request::old('studentpermeters'), ['class'=>'form-control', 'data-toggle'=>'tooltip', 'title'=>'Ο Μέσος Όρος των τμημάτων για μαθητές ανά τετραγωνικό μέτρο. Το νούμερο μπορεί να είναι και δεκαδικό']) !!}
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="ergP" id="ergP" {!! Request::old('ergP')?' checked ': ' ' !!} class="form" value="1">
                                         Υπάρχει Εργαστήριο Πληροφορικής;
                                     </label>
                                 </div>
                             </div>

                             <div class="panel-body" {!! Request::old('ergP')?'':'style="display: none"' !!} id="ergPbody">
                                 <div class="col-md-4">
                                      {!! Form::label('ergP_year', 'Έτος ανέγερσης:', ['class'=>'control-label']) !!}
                                      {!! Form::text('ergP_year',Request::old('ergP_year'), ['class'=>'form-control']) !!}
                                 </div>

                                 <div class="col-md-2"></div>
                                 <div class="col-md-5">
                                     <label class="label-warning">Κατάσταση:</label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergP_status')==1?' checked ':'' !!}  name="ergP_status" id="1" value="1"> Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergP_status')==2?' checked ':'' !!}  name="ergP_status" id="2" value="2"> ΔΕΝ Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergP_status')==3?' checked ':'' !!}  name="ergP_status" id="3" value="3"> Υπό κατασκευή
                                     </label>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" {!! Request::old('ergX')?' checked ': ' ' !!}  name="ergX" id="ergX" class="form" value="1">
                                         Υπάρχει Εργαστήριο Χημείας;
                                     </label>
                                 </div>
                             </div>

                             <div class="panel-body" {!! Request::old('ergX')?'':'style="display: none"' !!}  id="ergXbody">
                                 <div class="col-md-4">
                                      {!! Form::label('ergX_year', 'Έτος ανέγερσης:', ['class'=>'control-label']) !!}
                                      {!! Form::text('ergX_year',null, ['class'=>'form-control']) !!}
                                 </div>

                                 <div class="col-md-2"></div>
                                 <div class="col-md-5">
                                     <label class="label-warning">Κατάσταση:</label>
                                     <label class="radio">
                                         <input type="radio"  {!! Request::old('ergX_status')==1?' checked ':'' !!}  name="ergX_status" id="1" value="1"> Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio"  {!! Request::old('ergX_status')==2?' checked ':'' !!}  name="ergX_status" id="2" value="2"> ΔΕΝ Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio"  {!! Request::old('ergX_status')==3?' checked ':'' !!}  name="ergX_status" id="3" value="3"> Υπό κατασκευή
                                     </label>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="ergL" {!! Request::old('ergL')?' checked ': ' ' !!}  id="ergL" class="form" value="1">
                                         Υπάρχει Βιβλιοθήκη;
                                     </label>
                                 </div>
                             </div>

                             <div class="panel-body" {!! Request::old('ergL')?'':'style="display: none"' !!}  id="ergLbody">
                                 <div class="col-md-4">
                                      {!! Form::label('ergL_year', 'Έτος ανέγερσης:', ['class'=>'control-label']) !!}
                                      {!! Form::text('ergL_year',null, ['class'=>'form-control']) !!}
                                 </div>

                                 <div class="col-md-2"></div>
                                 <div class="col-md-5">
                                     <label class="label-warning">Κατάσταση:</label>
                                     <label class="radio">
                                         <input type="radio"  {!! Request::old('ergL_status')==1?' checked ':'' !!}  name="ergL_status" id="1" value="1"> Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergL_status')==2?' checked ':'' !!}  name="ergL_status" id="2" value="2"> ΔΕΝ Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergL_status')==3?' checked ':'' !!}  name="ergL_status" id="3" value="3"> Υπό κατασκευή
                                     </label>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" {!! Request::old('ergG')?' checked ': ' ' !!}  name="ergG" id="ergG" class="form" value="1">
                                         Υπάρχει Γυμναστήριο;
                                     </label>
                                 </div>
                             </div>

                             <div class="panel-body" {!! Request::old('ergX')?'':'style="display: none"' !!}  id="ergGbody">
                                 <div class="col-md-4">
                                      {!! Form::label('ergG_year', 'Έτος ανέγερσης:', ['class'=>'control-label']) !!}
                                      {!! Form::text('ergG_year',null, ['class'=>'form-control']) !!}
                                 </div>

                                 <div class="col-md-2"></div>
                                 <div class="col-md-5">
                                     <label class="label-warning">Κατάσταση:</label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergG_status')==1?' checked ':'' !!}  name="ergG_status" id="1" value="1"> Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergG_status')==2?' checked ':'' !!}  name="ergG_status" id="2" value="2"> ΔΕΝ Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergG_status')==3?' checked ':'' !!}  name="ergG_status" id="3" value="3"> Υπό κατασκευή
                                     </label>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" {!! Request::old('ergE')?' checked ': ' ' !!}  name="ergE" id="ergE" class="form" value="1">
                                         Υπάρχει αίθουσα πολλαπλών χρήσεων;
                                     </label>
                                 </div>
                             </div>

                             <div class="panel-body" {!! Request::old('ergE')?'':'style="display: none"' !!}  id="ergEbody">
                                 <div class="col-md-4">
                                      {!! Form::label('ergE_year', 'Έτος ανέγερσης:', ['class'=>'control-label']) !!}
                                      {!! Form::text('ergE_year',null, ['class'=>'form-control']) !!}
                                 </div>

                                 <div class="col-md-2"></div>
                                 <div class="col-md-5">
                                     <label class="label-warning">Κατάσταση:</label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergE_status')==1?' checked ':'' !!}  name="ergE_status" id="1" value="1"> Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergE_status')==2?' checked ':'' !!}  name="ergE_status" id="2" value="2"> ΔΕΝ Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergE_status')==3?' checked ':'' !!}  name="ergE_status" id="3" value="3"> Υπό κατασκευή
                                     </label>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" {!! Request::old('ergT')?' checked ': ' ' !!}  name="ergT" id="ergT" class="form" value="1">
                                         Υπάρχει Τραπεζαρία;
                                     </label>
                                 </div>
                             </div>

                             <div class="panel-body" {!! Request::old('ergT')?'':'style="display: none"' !!}  id="ergTbody">
                                 <div class="col-md-4">
                                      {!! Form::label('ergT_year', 'Έτος ανέγερσης:', ['class'=>'control-label']) !!}
                                      {!! Form::text('ergT_year',null, ['class'=>'form-control']) !!}
                                 </div>

                                 <div class="col-md-2"></div>
                                 <div class="col-md-5">
                                     <label class="label-warning">Κατάσταση:</label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergT_status')==1?' checked ':'' !!}  name="ergT_status" id="1" value="1"> Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergT_status')==2?' checked ':'' !!}  name="ergT_status" id="2" value="2"> ΔΕΝ Λειτουργεί
                                     </label>
                                     <label class="radio">
                                         <input type="radio" {!! Request::old('ergT_status')==3?' checked ':'' !!}  name="ergT_status" id="3" value="3"> Υπό κατασκευή
                                     </label>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>                
    

                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                 Ασφάλεια κτηρίων
                             </div>

                             <div class="panel-body">
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="earthquake_check" {!! Request::old('earthquake_check')? ' checked ':'' !!} class="form" value="1">
                                         Έχει γίνει αντισεισμικός έλεγχος.
                                     </label>
                                 </div>
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="fire_check" {!! Request::old('fire_check')?' checked ': '' !!} class="form" value="1">
                                         Υπάρχει πυρασφάλεια.
                                     </label>
                                 </div>
                                 <div class="checkbox">
                                     <label>
                                     <input type="checkbox" name="yard_check" {!! Request::old('yard_check')?' checked ':'' !!} class="form" value="1">
                                         Οι αύλειοι χώροι είναι ασφαλείς.
                                     </label>
                                 </div>
                             </div>
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                         <div class="panel panel-default">

                             <div class="panel-heading">
                                  Υγιεινή
                             </div>

                             <div class="panel-body">
                                <div class="col-md-4">
                                    <!-- Total Form Input -->
                                     <div class="form-group">
                                         {!! Form::label('wc', 'τουαλέτες/μαθητή', ['class'=>'control-label']) !!}
                                         {!! Form::text('wc',null, ['class'=>'form-control','data-toggle'=>'tooltip', 'title'=>'Το νούμερο μπορεί να είναι και δεκαδικό.']) !!}
                                     </div>
                                </div>

                                <div class="col-md-4">
                                     <div class="form-group">
                                         {!! Form::label('faucet', 'βρύσες/μαθητή', ['class'=>'control-label']) !!}
                                         {!! Form::text('faucet',null, ['class'=>'form-control','data-toggle'=>'tooltip', 'title'=>'Το νούμερο μπορεί να είναι και δεκαδικό.']) !!}
                                     </div>
                                </div>
                             </div>
                         </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                         <div class="checkbox">
                             <label>
                             <input type="checkbox" name="double_hours" {!! Request::old('double_hours')?' checked ':'' !!} class="form" value="1">
                                 Το Σχολείο σας έχει εναλλασσόμενο ωράριο - διπλοβάρδια;
                             </label>
                         </div>
                    </div>
                </div>

                <input type="hidden" name="sch_id" value="{!! $sch_id !!}"/>

                <div class="form-group text-center">
                    {!! Form::submit('Αποθήκευση', ['class'=>'btn btn-success btn-lg']) !!}
                </div>

            {!! Form::close() !!}
        </div>

    </div>
</div>
@endsection

@section('scripts.footer')
    <script>
        $(document).ready(function() {
            $("#ergP").on('change', function(){
                if (this.checked) $("#ergPbody").show(1000);
                if (!this.checked) $("#ergPbody").hide(1000);
            });

            $("#ergX").on('change', function(){
                if (this.checked) $("#ergXbody").show(1000);
                if (!this.checked) $("#ergXbody").hide(1000);
            });
            $("#ergL").on('change', function(){
                if (this.checked) $("#ergLbody").show(1000);
                if (!this.checked) $("#ergLPbody").hide(1000);
            });
            $("#ergG").on('change', function(){
                if (this.checked) $("#ergGbody").show(1000);
                if (!this.checked) $("#ergGbody").hide(1000);
            });
            $("#ergE").on('change', function(){
                if (this.checked) $("#ergEbody").show(1000);
                if (!this.checked) $("#ergEbody").hide(1000);
            });
            $("#ergT").on('change', function(){
                if (this.checked) $("#ergTbody").show(1000);
                if (!this.checked) $("#ergTbody").hide(1000);
            });
            
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection