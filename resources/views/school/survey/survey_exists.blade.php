@extends('app')

@section('title')
    Ευχαριστούμε
@endsection

@section('content')
    <div class="container">
        <h3 class="col-md-8 col-md-offset-2"><div class="alert alert-success" role="alert"> Το Σχολείο σας μας έχει αποστείλει τα απαραίτητα στοιχεία, ευχαριστούμε</div> </h3>
    </div>
@endsection
