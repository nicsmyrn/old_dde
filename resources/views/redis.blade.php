<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

    </head>
    <body>

        <h1> New Users</h1>

        <ul>
            <li v-repeat="user: users">@{{ user }} </li>
        </ul>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.16/vue.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.4.4/socket.io.min.js"></script>
        <script>
            var socket = io('http://srv-dide.chan.sch.gr:3000');
            new Vue({
                el: 'body',


                data : {
                    users : []
                },

                ready : function(){
                    socket.on('test:App\\Events\\TestEvent', function(data){
                          console.log(data);
                        this.users.push(data.username);
                    }.bind(this));
                }
            })
        </script>
    </body>
</html>
