<body>
    <table>
        <tbody>
            <tr>
                <td>
                    <a href="https://srv-dide.chan.sch.gr"><img src="{!! asset('images/pasifai.png') !!}"></a>
                    <a href="http://didechan.blogspot.gr"><img src="https://srv-dide.chan.sch.gr/images/dide-logo.png"></a>
                </td>
            </tr>

            <tr>
                <td>
                        <p>
                            Ο εκπαιδευτικός  <b>{!! $full_name !!}</b> με ΑΦΜ: <b>{!! $afm !!}</b> και Αρ. Μητρώου: <b>{!! $am !!}</b>  έχει στείλει ΑΙΤΗΜΑ<strong> ΓΙΑ ΜΟΡΙΟΔΟΤΗΣΗ {!! $type !!} </strong>.
                        </p>

                        <p>
                            Παρακαλώ για τις δικές σας ενέργειες
                        </p>

                            @if($counter > 0)
                                <p>
                                    το μήνυμα περιλαμβάνει <strong> {!! $counter !!} συννημμένα</strong>
                                </p>
                            @endif
                </td>
            </tr>
            <tr>
                <td><hr></td>
            </tr>
            <tr>
                <p><b>Σημείωση:</b> Αυτό είναι ένα αυτοματοποιημένο μήνυμα, παρακαλούμε μην απαντήσετε.</p>
            </tr>
        </tbody>
    </table>
</body>



