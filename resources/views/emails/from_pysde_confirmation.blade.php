<body>
    <table>
        <tbody>
            <tr>
                <td><a href="http://didechan.blogspot.gr"><img src="https://srv-dide.chan.sch.gr/images/dide-logo.png"></a> </td>
            </tr>
            <tr>
                <td>
                    <p>Τα Κενά - Πλεονάσματα του Σχολείου: {!! $school['name'] !!}  παρελήφθησαν με επιτυχία από τη Δ.Δ.Ε. Χανίων.</p>
                    <p>Σημειώσεις Σχολείου: {!! $school['description'] !!}</p>
                    <p>Ευχαριστούμε για τη συνεργασία</p>
                </td>
            </tr>
            <tr>
                <p><b>Σημείωση:</b> Αυτό είναι ένα αυτοματοποιημένο μήνυμα, παρακαλούμε μην απαντήσετε.</p>
            </tr>
        </tbody>
    </table>
</body>
