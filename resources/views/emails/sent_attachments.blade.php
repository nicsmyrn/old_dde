<body>
    <table>
        <tbody>
            <tr>
                <td>
                    <a href="https://srv-dide.chan.sch.gr"><img src="{!! asset('images/pasifai.png') !!}"></a>
                    <a href="http://didechan.blogspot.gr"><img src="https://srv-dide.chan.sch.gr/images/dide-logo.png"></a>
                </td>
            </tr>

            <tr>
                <td>
                        <p>
                            Ο εκπαιδευτικός  <b>{!! $full_name !!}</b> με ΑΦΜ: <b>{!! $afm !!}</b> έχει στείλει <strong>ΔΙΚΑΙΟΛΟΓΗΤΙΚΑ</strong> για αλλαγή των στοιχείων
                            του Προφίλ του.
                        </p>

                        <p>
                            Για επεξεργασία  πατήστε <a href="{!! route('Pysde::Teachers::check', $id) !!}">ΕΔΩ</a>
                        </p>

                            @if($counter > 0)
                                <p>
                                    το μήνυμα περιλαμβάνει <strong> {!! $counter !!} συννημμένα</strong>
                                </p>
                            @endif
                </td>
            </tr>
            <tr>
                <td><hr></td>
            </tr>
            <tr>
                <p><b>Σημείωση:</b> Αυτό είναι ένα αυτοματοποιημένο μήνυμα, παρακαλούμε μην απαντήσετε.</p>
            </tr>
        </tbody>
    </table>
</body>



