<table>

<tbody>
    <tr>
        <td><img src="https://srv-dide.chan.sch.gr/images/dide-logo.png"> </td>
    </tr>
    <tr>
        <td>
          <h2>Αλλαγή Κωδικού!</h2>

          <p>Πατήστε στο παρακάτω κουμπί για να αλλάξετε τον κωδικό σας</p>
          <p><a href="{!! Auth::check()?route('User::getReset',[$token]): route('getResetLostPassword', [$token]) !!}" style="text-align:center;text-decoration:none;color:#fff;font-size:12px;line-height:16px;display:inline-block;border-radius:3px;padding-top:8px;padding-right:18px;padding-bottom:10px;padding-left:18px;background-color:#c4161c">Αλλαγή κωδικού</a></p>
          <p>Σε περίπτωση που δεν ανοίγει το κουμπί ανοίξτε σε έναν φυλλομετρητή (browser) τον παρακάτω σύνδεσμο:</p>
          <p>{!! Auth::check()?route('User::getReset',[$token]): route('getResetLostPassword', [$token]) !!}</p>
        </td>
    </tr>
</tbody>

</table>

