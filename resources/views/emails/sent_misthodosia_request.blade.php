<body>
    <table>
        <tbody>
            <tr>
                <td><a href="http://didechan.blogspot.gr"><img src="https://srv-dide.chan.sch.gr/images/dide-logo.png"></a> </td>
            </tr>

            <tr>
                <td>
                        <p>
                            Ο εκπαιδευτικός  <b>{!! $full_name !!}</b> με ΑΦΜ: <b>{!! $afm !!}</b> έχει στείλει <strong>ΝΕΑ ΣΤΟΙΧΕΙΑ</strong> στην ΠΑΣΙΦΑΗ.
                        </p>

                        <p>
                            Για έλεγχο των νέων πληροφοριών πατήστε <a href="{!! route('Misthodosia::checkMisthodosia', $afm) !!}">ΕΔΩ</a>
                        </p>

                            @if($counter > 0)
                                <p>
                                    το μήνυμα περιλαμβάνει <strong> {!! $counter !!} συννημμένα</strong>
                                </p>
                            @endif
                </td>
            </tr>
            <tr>
                <td><hr></td>
            </tr>
            <tr>
                <p><b>Σημείωση:</b> Αυτό είναι ένα αυτοματοποιημένο μήνυμα, παρακαλούμε μην απαντήσετε.
                Εάν επιθυμείτε να επικοινωνήσετε παρακαλούμε τηλεφωνήστε στο τμήμα Μισθοδοσίας - 2821047141</p>
            </tr>
        </tbody>
    </table>
</body>



