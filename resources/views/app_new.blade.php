<!DOCTYPE html>
<html lang="en">

<head>
    @include('meta')

    <title>@yield('title')</title>

    <link rel="stylesheet" href="{!! elixir('css/app.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/libs.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/app2.css') !!}">

    <link href='https://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
	@yield('header.style')

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    	<!--[if lt IE 9]>
    		<script src="/css/html5shiv.min.js"></script>
    		<script src="/css/respond.min.js"></script>
    	<![endif]-->
</head>

<body id="app">
    @yield('loader')

    @include('partials.nav2')

    <div class="container" id="lobby-container">
        @yield('content')
    </div>

    @include('partials.footer')

    <script src="{!! elixir('js/libs-default.js') !!}"></script>
    <script src="{!! elixir('js/app2.js') !!}"></script>

        @yield('scripts.footer')
        @include('flash')
        @include('partials.google_analytics')

        @include('partials.nav2.notifications.notifications')

{{--        @include('partials.countdown')--}}
</body>

</html>