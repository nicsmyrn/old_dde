@extends('errors.layout')

@section('title')
    Σφάλμα
@stop

@section('content')
		<div>
			<h1 class="error-code">403</h1>
			<p class="error-message">
				Η ΣΕΛΙΔΑ ΔΕΝ ΕΙΝΑΙ ΔΙΑΘΕΣΙΜΗ ΑΥΤΗΝ ΤΗ ΣΤΙΓΜΗ<br>
				Go Back <a href="{!! url('/') !!}" title="Home"><i class="icon-home"></i> Home</a>
			</p>
		</div>
@stop

