
<!DOCTYPE html>
<html lang="en">
<head>
    @include('meta')

	<title> @yield('title')</title>

    <link rel="stylesheet" href="{!! elixir('css/app.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/libs.css') !!}">

	<!-- Fonts -->
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Tahoma:+400,700" type="text/css">

  <style>
  body {
    background: #f5f5f5;
  }
  body #error-page {
    background: url("http://clanbfg.org/wp-content/uploads/2013/03/bfg_members_only.png") no-repeat 100% 0;
    min-height: 400px;
    max-width: 650px;
    padding: 19px 29px 29px;
    margin: 100px auto 20px;
  }
  body #error-page >div {
    float: right;
    margin-right: 320px;
    text-align: right;
  }
  body #error-page h1.error-code {
    font-family: Impact;
    font-size: 180px;
    line-height: 180px;
    color: #f5acac;
    text-shadow: 0 1px #944040;
  }
  body #error-page p.error-message {
    font-size: 24px;
    line-height: 24px;
    text-transform: uppercase;
    color: #999;
    text-shadow: 0 1px #fff;
    margin-right: 10px;
  }
  body #error-page a {
    text-decoration: none;
    color: #44c5ef;
  }
  body #error-page a:hover {
    text-decoration: none;
    color: #62d3f8;
  }

  </style>

</head>
<body class="article homepage  ltr preset2 menu-home-arxiki responsive bg clearfix">
	<div id="error-page" class="container">
	    @yield('content')
	</div>
</body>
</html>
