@extends('app')

@section('title')
    Προσοχή!! Σφάλμα!!!
@stop

@section('content')
    <div class="container">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="panel panel-default" id="error_panel">
              <div class="panel-body">
                  <div class="row">
                      <div class="col-md-5">
                        <h3>Σφάλμα Συστήματος!</h3>
                        <h5>παρακαλούμε επικοινωνήστε με τον διαχειριστή του συστήματος </h5>
                        <h5>Σμυρναίο Νικόλαο για να λυθεί το πρόβλημα. </h5>
                        <p>
                            Στοιχεία Επικοινωνίας:
                            <ul>
                                <li>E-mail nicsmyrn@gmail.com</li>
                                <li>Τηλ γραφείου ΠΥΣΔΕ: 28210 47154</li>
                                <li>Κινητό: 6973009154</li>
                            </ul>
                        </p>
                         <a class="btn btn-success" href="{!!URL::to('/')!!}">Αρχική</a>
                      </div>
                      <div class="col-md-7 text-center">
                          <img src="http://www.google.com/images/errors/robot.png" width="50%" height="50%"/>
                      </div>
                  </div>
              </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
@stop
