@extends('app')

@section('header.style')
    <meta id="token" name="token" value="{!! csrf_token() !!}">

    <style type="text/css">
        td button.delete{
            font-weight: bold;
            font-size: larger;
            background: none;
            border: 0;
            color:red;
            padding: 0px;
            margin: 1px;
        }

        td button.edit{
            font-weight: bold;
            font-size: larger;
            background: none;
            border: 0;
            color:#ffcc00;
            padding: 0px;
            margin:1px;
        }
        .panel-label-placements{
            font-weight: normal;
        }
        .panel-value-placements{
            font-weight: bold;
        }
    </style>
    <style>
        [v-cloak] {
          display:none;
        }
    </style>
@endsection

@section('content')

    <div class="container" id="placements" v-cloak>
        <div v-bind:class="{
                'loaderWrap' : true,
                'invisible' : !loaderDisplay,
                'loading'   : loaderDisplay
            }"
        >
            <div class="bg"></div>
            <div id="spinner">
              <div class="double-bounce1"></div>
              <div class="double-bounce2"></div>
            </div>
            <div id="textLoading">
                Παρακαλώ περιμένετε...
            </div>
        </div>
        {{--<input  v-model="date2" type="text" name="date2" id="date2" value="{!!\Carbon\Carbon::now()->format('d/m/Y') !!}">--}}
        {{--<button v-on="click:date2f" class="btn btn-default">Click</button>--}}

        <div id="loader" class="progress">
            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                0%
            </div>
         </div>


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                   <div class="panel-heading">
                     Δημιουργία νέας πράξης Τοποθετήσεων
                   </div>
                   <div class="panel-body">
                         {!!Form::model($eidikotites,['method'=>'post', 'class'=>'form'] )!!}
                             <!-- Praxi Form Input -->

                             <div class="form-group">
                                <div class="col-md-1">
                                    {!! Form::label('decision_number', 'Πράξη:', ['class'=>'control-label']) !!}
                                     <input v-model="decision_number" name="decision_number"  type="text" class="form-control" placeholder="No"/>
                                </div>

                             </div>
                             <!-- Decision_date Form Input -->
                             <div class="form-group">
                                <div class="col-md-2">
                                 {!! Form::label('decision_date', 'Ημερομηνία:', ['class'=>'control-label']) !!}
                                 <input v-model="decision_date" name="decision_date" type="text" class="form-control" value="{!!\Carbon\Carbon::now()->format('d/m/Y') !!}"/>
                                </div>
                             </div>
                             <div class="form-group">
                                <div class="col-md-3">
                                    {!! Form::label('decision_date', 'Κλάδος - Ειδικότητα:', ['class'=>'control-label']) !!}
                                    <select @change="changeEidikotita" v-model="eid_id" name="selectEidikotita" id="selectEidikotita" class="form-control">
                                      @foreach($eidikotites as $eidikotita)
                                          <option value="{!! $eidikotita->id !!}">{!! $eidikotita->name !!} [{!! $eidikotita->united !!}]</option>
                                      @endforeach
                                    </select>
                                </div>
                             </div>

                            <div class="form-group">
                                <div class="col-md-4">
                                    {!! Form::label('teacher_select', 'Επέλεξε καθηγητή (χρήστες ΠΑΣΙΦΑΗ):', ['class'=>'control-label']) !!}
                                    <select v-model="currentTeacher" class="form-control col-md-2">
                                        <option></option>
                                        <option v-for="teacher in temporaryTeachers" v-bind:value="teacher">
                                            @{{ teacher.full_name }} [@{{ teacher.organiki }}]
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    {!! Form::label('all_teachers_select', 'Ολοι οι καθηγητές του κλάδου: (MySchool)', ['class'=>'control-label']) !!}
                                    <select v-model="currentTeacher" class="form-control col-md-2">
                                        <option></option>
                                        <option v-for="teacher in allTeachers" v-bind:value="teacher">
                                            @{{ teacher.full_name }} [@{{ teacher.organiki }}]
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4">
                                    {!! Form::label('pleonazontes', 'Πλεονάζοντες', ['class'=>'control-label']) !!}
                                    <select v-model="currentTeacher" class="form-control col-md-2">
                                        <option></option>
                                        <option v-for="teacher in pleonazontes" v-bind:value="teacher">
                                            @{{ teacher.full_name }} [@{{ teacher.organiki }}]
                                        </option>
                                    </select>
                                </div>
                            </div>
                             <div v-if="placements.length && decision_number"  class="btn-group">
                                <div class="col-md-2">
                                  <button @click="saveChanges" class="btn btn-primary btn-lg">Αποθήκευση</button>
                                </div>
                             </div>
                         {!!Form::close()!!}
                   </div>
                 </div>
            </div>
        </div> <!--  class ROW -->

        <div class="row">
            <div v-if="currentTeacher.id != null" class="panel panel-default">
                <div class="panel-heading">
                    Καθηγητής: <b> @{{ currentTeacher.full_name }} του @{{ currentTeacher.middle_name }}</b>
                </div>
                <div class="panel-body">
                    <div class="col-md-3">
                        <div>
                            <label class="panel-label-placements">
                                Σχέση Εργασίας: <span class="panel-value-placements">@{{ currentTeacher.sxesi }}</span>
                            </label>
                        </div>
                        <div v-show="currentTeacher.sxesi == 'ΜΟΝΙΜΟΣ'">
                            <label class="panel-label-placements">
                                Οργανική: <span class="panel-value-placements">@{{ currentTeacher.organiki }}</span>
                            </label>
                        </div>

                        <div v-show="currentTeacher.sxesi == 'ΜΟΝΙΜΟΣ'">
                            <label class="panel-label-placements">
                                ΟργανικήMySchool: <span class="panel-value-placements">@{{ currentTeacher.organikiMySchool }}</span>
                            </label>
                        </div>

                        <div>
                            <label class="panel-label-placements">
                                Μόρια: <span class="panel-value-placements">@{{ currentTeacher.moria }}</span>
                            </label>
                        </div>
                        <div>
                            <label class="panel-label-placements">
                                Κλάδος: <span class="panel-value-placements">@{{ currentTeacher.klados }}</span>
                            </label>
                        </div>
                        <div>
                            <label class="panel-label-placements">
                                Εντοπιότητα: <span class="panel-value-placements">@{{ currentTeacher.entopiotita }}</span>
                            </label>
                        </div>
                        <div>
                            <label class="panel-label-placements">
                                Συνυπηρέτηση: <span class="panel-value-placements">@{{ currentTeacher.sinipiretisi }}</span>
                            </label>
                        </div>
                        <div>
                            <label class="panel-label-placements">
                                Ειδική Κατηγορία: <span class="panel-value-placements">@{{ currentTeacher.special }}</span>
                            </label>
                        </div>
                        <div>
                            <label class="panel-label-placements">
                                Υποχρεωτικό Ωράριο: <span class="panel-value-placements" style="color: red; font-size: 13pt;">@{{ currentTeacher.ypoxreotiko }}</span>
                            </label>
                        </div>

                        <div v-show="currentTeacher.sxesi == 'ΜΟΝΙΜΟΣ'">
                            <label class="panel-label-placements">
                                Υποχρεωτικό MySchool: <span class="panel-value-placements" style="color: red; font-size: 13pt;">@{{ currentTeacher.ypoxreotikoMySchool }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="5">Προηγούμενες Πράξεις</th>
                                </tr>
                                <tr>
                                    <th>Πράξη</th>
                                    <th>Τοποθέτηση</th>
                                    <th>Είδος</th>
                                    <th>Ώρες/Ημ</th>
                                    <th>Παρατηρήσεις</th>
                                    <th>Ενέργειες</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr v-for="topothetisi_praxis in currentTeacher.topothetisis">
                                    <td v-if="!topothetisi_praxis.edit_cell">
                                        @{{ topothetisi_praxis.to }}
                                    </td>
                                    <td v-if="!topothetisi_praxis.edit_cell">@{{ topothetisi_praxis.praxi.decision_number }}η/@{{ topothetisi_praxis.praxi.decision_date }}</td>
                                    <td class="text-center">@{{ allPlacementsTypes[topothetisi_praxis.placements_type] }}</td>
                                    <td class="text-center" v-if="!topothetisi_praxis.edit_cell">@{{ topothetisi_praxis.hours }}Ω/@{{ topothetisi_praxis.days }}Η</td>
                                    <td v-if="!topothetisi_praxis.edit_cell">@{{ topothetisi_praxis.description }}</td>
                                    <td class="text-center" v-if="!topothetisi_praxis.edit_cell">
                                        <button @click="editTopothetisi(topothetisi_praxis)" alt="Επεξεργασία εγγραφής" class="edit"><i class="fa fa-pencil-square-o"></i></button>
                                        <button @click="deleteTopothetisi(topothetisi_praxis)" alt="Διαγραφή" class="delete"><i class="fa fa-times"></i></button>
                                    </td>

                                    <td v-if="topothetisi_praxis.edit_cell">
                                        <input type="text" value="@{{ topothetisi_praxis.to }}"
                                    </td>
                                    <td v-if="topothetisi_praxis.edit_cell">
                                        @{{ topothetisi_praxis.praxi_id }}η/@{{ topothetisi_praxis.created_at }}
                                    </td>
                                    <td v-if="topothetisi_praxis.edit_cell">
                                        <input type="text" value="@{{ topothetisi_praxis.hours }}" maxlength="2" size="2"
                                    </td>
                                    <td v-if="topothetisi_praxis.edit_cell">
                                        <input type="text" value="@{{ topothetisi_praxis.description }}"
                                    </td>
                                    <td v-if="topothetisi_praxis.edit_cell">
                                        <button @click="saveTopothetisi(topothetisi_praxis)"  alt="Αποθήκευση" class="save"><i class="fa fa-floppy-o"></i></button>
                                    </td>

                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3">Σύνολο</th>
                                    <th class="text-center" style="color: #006400; font-size: 13pt;">@{{ currentTeacherTotal }}</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="col-md-2">
                        <div v-if="notZeroSchoolLength || placements.length">
                            <form @submit="addPlacement" class="form">
                                <div>
                                    <label>Είδος τοποθέτησης:</label>
                                    <select v-model="newPlacement.placementType" class="form-control">
                                        <option value="0" selected></option>
                                        @foreach(Config::get('requests.placements_type') as $k=>$v)
                                            <option value="{!! $k !!}">
                                                {!! $v !!}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div>
                                    <input type="checkbox" v-model="newPlacement.ellimma"/>
                                    <div v-show="newPlacement.ellimma" class="form-group">
                                        <label>Έλλειμμα:</label>
                                        <select v-model="newPlacement.to" class="form-control">
                                            <option selected value="0"></option>
                                            <option v-for="school in temporarySchools|sxoleiaTopothetis" v-bind:value="school">
                                                @{{ school.name }}
                                            </option>
                                        </select>
                                    </div>

                                    <div v-else class="form-group">
                                        <label>ΌΛΑ ΤΑ ΣΧΟΛΕΙΑ:</label>
                                        <select v-model="newPlacement.to" class="form-control">
                                            <option selected value="0"></option>
                                            @foreach(Config::get('requests.special_placements') as $k=>$v)
                                                <option v-bind:value="{id:{!! $k !!},name:'{!! $v !!}'}">{!! $v !!}</option>
                                            @endforeach
                                            <option v-for="school in allSchoolsList" v-bind:value="school">
                                                @{{ school.name }}
                                            </option>
                                        </select>
                                    </div>

                                </div>

                                <br>
                                <div>
                                    <label>Ώρ:</label>
                                    <input v-el="focus" v-model="newPlacement.hours" placeholder="Ωρες" size="3"/>

                                    <label>Ημ:</label>
                                    <select v-model="newPlacement.days">
                                        <option value="0">-</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>

                                <div>
                                    <label>Παρατηρήσεις:</label>
                                    <input v-model="newPlacement.description" type="text" placeholder="Παρατηρήσεις"/>
                                    <label for="aitisi">
                                        με Αίτηση:
                                    </label>
                                    <input name="aitisi" type="checkbox" v-model="newPlacement.aitisi">
                                </div>

                                <br>
                                <button v-show="newPlacement.hours" class="btn btn-warning ">Προσθήκη</button>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div v-if="notZeroSchoolLength" class="col-md-3">

                <h1 v-if="notZeroSchoolLength.length==1" class="text-center">@{{ notZeroSchoolLength.length }} Σχολείο</h1>
                <h1 v-if="notZeroSchoolLength.length>1" class="text-center">@{{ notZeroSchoolLength.length }} Σχολεία</h1>

                <div v-if="notZeroSchoolLength.length==0" class="alert alert-success" role="alert">
                    Συμπληρώθηκαν ΟΛΑ τα κενά και πλεονάσματα του κλάδου
                </div>

                <table v-else class="table table-bordered">
                    <thead>
                        <tr class="active">
                            <th>Σχολείο</th>
                            <th class="text-center">Ώρες</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr
                            v-bind:class="{
                                'success' : school.pivot.value > 0,
                                'danger'  : school.pivot.value < 0
                            }"
                            v-for="school in notZeroSchoolLength"
                        >
                            <td>@{{ school.name }}</td>
                            <td class="text-center">@{{ school.pivot.value }}</td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <div class="col-md-9">
                <div class="col-md-12">
                <br>
                <div  v-if="placements.length" >
                    <h3 class="text-center"><span v-if="placements.length">@{{ placements.length }} - </span>Τοποθετήσεις</h3>

                     <table class="table table-bordered">
                         <thead>
                             <tr>
                                 <th class="text-center">Ονοματεπώνυμο</th>
                                 <th class="text-center">Από</th>
                                 <th class="text-center">Τοποθέτηση</th>
                                 <th class="text-center">Παρατηρήσεις</th>
                                 <th class="text-center">Ώρες</th>
                                 <th class="text-center">Διαφορά</th>
                             </tr>
                         </thead>

                         <tbody>
                             <tr v-for="teacher in placements" track-by="id">
                                   <td>
                                        @{{ teacher.full_name }}
                                        <button alt="Διαγραφή" class="delete" @click="removeTeacher(teacher)"><i class="fa fa-times"></i></button>
                                   </td>
                                   <td>@{{ teacher.organiki}}</td>
                                   <td>
                                       <table>
                                           <tr v-for="p in teacher.placements">
                                               <td>
                                                    @{{ ($index + 1) }}.
                                                    [@{{ allPlacementsTypes[p.type] }}]
                                                    @{{ p.hours }}Ωρ/@{{ p.days }}Ημ  στο @{{ p.to.name }}
                                                    <button alt="Διαγραφή" class="delete" @click="removeTeacherPlacement(teacher, p)"><i class="fa fa-times"></i></button>
                                               </td>
                                           </tr>
                                       </table>
                                   </td>
                                   <td>
                                       <table>
                                           <tr v-for="p in teacher.placements">
                                               <td>@{{ p.description }}</td>
                                           </tr>
                                       </table>
                                   </td>
                                   <td class="text-center" style="color: #006400; font-weight: bold; font-size: 13pt;">
                                        @{{ teacher.totalPlacements }}
                                   </td>
                                   <td class="text-center">
                                        <span v-if="teacher.ypoxreotiko - teacher.totalPlacements - teacher.totalTopothetisis == 0" style="color: #008000">
                                            <i class="fa fa-check-circle-o fa-2x" aria-hidden="true"></i>
                                        </span>
                                        <span v-else  style="color: #0000ff; font-weight: bold; font-size: 16pt;">
                                            @{{teacher.ypoxreotiko - teacher.totalPlacements - teacher.totalTopothetisis }}
                                        </span>

                                   </td>
                             </tr>
                         </tbody>
                     </table>
                </div>
            </div>
            </div>
        </div>

        <div class="row">

        </div>

    @include('components.alert')

    </div>


@endsection

@section('scripts.footer')
        <script src="{!! elixir('js/placements.js') !!}"></script>
@endsection