@extends('app')

@section('title')
    Τοποθετήσεις Καθηγητή
@endsection

@section('header.style')

@endsection

@section('content')
        <div class="col-md-1">
            <a href="{!! route('Dioikisi::Placements::placementsByPraxi',['αριθμός' => $praxi->id]) !!}" class="btn btn-primary brn-lg">Επιστροφή</a>
        </div>
        <h2 class="page-heading col-md-10">
            {!! $teacher->user->full_name !!} του {!! $teacher->middle_name !!} (Πράξη: {!! $praxi->decision_number !!}/{!! $praxi->decision_date !!})
            <a target="_blank" href="{!! route('Dioikisi::Placements::createTopothetirio',[$teacher->id, $praxi->id]) !!}">
                <i class="fa fa-search" aria-hidden="true"></i>
            </a>
        </h2>

        <table class="table table-hover">
            <thead>
                <tr>
                    <th>Τύπος τοποθέτησης</th>
                    <th>Τοποθέτηση</th>
                    <th class="text-center">Ημέρες</th>
                    <th class="text-center">Ώρες</th>
                    <th class="text-center">Παρατηρήσεις</th>
                    <th class="text-center">Με αίτηση</th>
                    <th class="text-center">Ενέργειες</th>
                </tr>
            </thead>
            <tbody>
                @foreach($teacher->placements as $placement)
                    <tr @if($placement->deleted_at != '')class="danger"@endif>
                        <td>{!! isset(Config::get('requests.placements_type')[$placement->placements_type])?Config::get('requests.placements_type')[$placement->placements_type] : '-'  !!}</td>
                        <td>
                            @if($placement->deleted_at != '')
                                <s>
                            @endif
                            {!! $placement->to !!}
                            @if($placement->deleted_at != '')
                                </s>
                            @endif
                        </td>
                        <td class="text-center">{!! $placement->days !!}</td>
                        <td class="text-center">{!! $placement->hours !!}</td>
                        <td>{!! $placement->description !!}</td>
                        <td class="text-center">
                            @if($placement->me_aitisi)
                                <span style="color: #008000">
                                    <i class="glyphicon glyphicon-ok-circle"></i>
                                </span>
                            @else
                                <span style="color: #FF0000">
                                    <i class="glyphicon glyphicon-remove-circle"></i>
                                </span>
                            @endif

                        </td>
                        <td class="text-center">
                            <a href="{!! route('Dioikisi::Placements::update', [$teacher->id, $placement->id]) !!}" title="Επεξεργασία" class="btn btn-warning btn-xs">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>

                            <button title="Διαγραφή" class="btn btn-danger btn-xs">
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

@endsection

@section('scripts.footer')

@endsection