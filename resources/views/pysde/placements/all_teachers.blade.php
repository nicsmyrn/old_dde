@extends('app')

@section('title')
    Καθηγητές
@stop

@section('header.style')
    <link href="/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')
    @include('pysde.placements.template_for_placements')
@stop

@section('scripts.footer')
    <script src="/js/datatables.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#teachers').DataTable({
                    "order": [[ 0, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 4 ] } ],
                     "lengthMenu": [ [25, 50, -1], [25, 50, "Όλοι"] ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


