@extends('app')

@section('header.style')

@endsection

@section('content')

        <h1 class="page-heading">Χειροκίνητη Τοποθέτηση Εκπαιδευτικού</h1>

        <div class="row">
{{--{!! Form::model($protocol = new \App\Protocol(), ['method'=>'POST', 'class'=>'form-horizontal', 'action'=>'ProtocolController@store']) !!}--}}

            {!! Form::model($placement = new \App\Placement(), ['method'=> 'POST', 'action' => 'PlacementsController@saveManualCreate']) !!}
                <div class="col-md-10 col-md-offset-2">
                    <div class="row" id="">
                        <div class="col-md-6">
                            <div class="form-group" id="">
                                {!! Form::label('teachers', 'ΟΛΟΙ ΟΙ ΚΑΘΗΓΗΤΕΣ:', ['class'=>'control-label']) !!}
                                <select name="teachers" id="teachers">
                                    <option></option>
                                    @foreach($myschool as $teacher)
                                        <option value="{!! $teacher->afm !!}">{!! $teacher->full_name !!} του {!! $teacher->middle_name !!} [{!! $teacher->eidikotita !!}]</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                @include('pysde.placements._form',  ['submitButton' => 'Δημιουργία Τοποθέτησης', 'creating' => true])
            {!! Form::close() !!}
        </div>

@endsection

@section('scripts.footer')

<script>
        $('#teachers').select2({
            placeholder: "Επιλογή Καθηγητή",
            theme: "classic",
            language: {
                noResults: function() {
                    return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                }
            }
        }).on('change', changeTeacherProfile);

        $('.school_select').select2({
            placeholder: "Επιλογή Σχολείου",
            theme: "classic",
            language: {
                noResults: function() {
                    return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                }
            }
        });

        function changeTeacherProfile(){
            $.ajax({
                url : "{!! route('Dioikisi::Placements::ajaxPlacementShowTeacherDetails') !!}",
                type : 'get',
                data : {teacherAFM : $(this).val()},
                success : function(data){
                    $('#organikiName').text(data['organiki_name']);
                    $('#kladosName').text(data['klados_full_name']);
                    $('#hiddenOrganiki').val(data['organiki_id']);
                    $('#hiddenKlados').val(data['klados_id']);
                    $('#placementDetails').show(200);
                    console.log(data);
                },
                error: function(data){
//                    $.swalAlertValidatorError(data)
                    console.log('Error');
                }
            });
        }

</script>

@endsection