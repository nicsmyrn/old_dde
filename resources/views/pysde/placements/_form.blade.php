            <div class="col-md-10 col-md-offset-2">

                <div class="row" id="placementDetails" @if($creating)style="display: none"@endif>
                    <div class="col-md-6">
                        <!-- Klados Full Name -->
                        <div class="form-group" id="divKlados">
                            {!! Form::label('klados_id', 'Κλάδος:', ['class'=>'control-label']) !!}
                            <span id="kladosName">{!! $placement->klados !!} ({!! $teacher->klados !!})</span>
                            <input type="hidden" value="{!! $placement->klados_id !!}" name="klados_id" id="hiddenKlados"/>
                        </div>

                        <!-- Praxi_id  Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('praxi_id', 'Αρ. Πράξης:', ['class'=>'control-label']) !!}
                            <select name="praxi_id">
                                <option value="0">Επέλεξε Πράξη</option>
                                @foreach($praxeis as $praxi)
                                    <option @if($placement->praxi_id == $praxi->id) selected @endif value="{!! $praxi->id !!}">{!! Config::get('requests.praxi_type')[$praxi->praxi_type] !!}: {!! $praxi->decision_number !!}η / {!! $praxi->decision_date !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <!-- From_id  Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('to_id', 'Προς:', ['class'=>'control-label']) !!}
        {{--                    {!! Form::select('to_id',Config::get('requests.specieal_placements') + $schools, null,['class' => 'school_select']) !!}--}}
                            <select name="to_id" class="school_select2">
                                <option></option>
                                @foreach(Config::get('requests.special_placements') as $k=>$v)
                                    <option @if($placement->to_id == $k) selected @endif value="{{$k}}">{{$v}}</option>
                                @endforeach
                                @foreach($schools as $school)
                                    <option @if($placement->to_id == $school->id) selected @endif value="{!! $school->id !!}">{!! $school->name !!}</option>
                                @endforeach
                            </select>
                        </div>

                        <!-- From_id  Form Input -->
                        <div class="form-group" class="col-md-6 col-md-offset-3">
                            {!! Form::label('me_aitisi', 'Έχει υποβάλλει αίτηση', ['class'=>'text-left control-label']) !!}
                            {!! Form::checkbox('me_aitisi') !!}
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Organiki -->
                        <div class="form-group" id="divOrganiki">
                            {!! Form::label('from_id', 'Οργανική:', ['class'=>'control-label']) !!}
                            <span id="organikiName">{!! $placement->from !!}</span>
                            <input type="hidden" name="from_id" id="hiddenOrganiki" value="{!! $placement->from_id !!}"/>
                        </div>
                        <!-- From_id  Form Input -->
                        <div class="form-group" id="">
                            {!! Form::label('from_id', 'Τύπος τοποθέτησης:', ['class'=>'control-label']) !!}
                            {!! Form::select('placements_type', [0 => 'Επέλεξε τύπο'] + Config::get('requests.placements_type')) !!}
                        </div>
                        <!-- From_id  Form Input -->
                        <div class="form-group" class="col-md-6 col-md-offset-3">
                            {!! Form::label('hours', 'Ώρες τοποθέτησης:', ['class'=>'text-left control-label']) !!}
                            {!! Form::text('hours',null, ['size'=> 2, 'maxlength'=>2]) !!}
                        </div>

                        <!-- From_id  Form Input -->
                        <div class="form-group" class="col-md-6 col-md-offset-3">
                            {!! Form::label('days', 'Ημέρες τοποθέτησης [προαιρετικό]:', ['class'=>'text-left control-label']) !!}
                            {!! Form::select('days',[
                                0 => '-',
                                1 => 1,
                                2 => 2,
                                3 => 3,
                                4 => 4,
                                5 => 5
                            ]) !!}
                        </div>
                    </div>

                <!-- From_id  Form Input -->
                <div class="form-group" id="" class="col-md-6 col-md-offset-3">
                    {!! Form::label('description', 'Παρατηρήσεις:', ['class'=>'control-label']) !!}
                    {!! Form::text('description',null, ['class'=>'form-control']) !!}
                </div>

                <!-- Αποθήκευση Form Input -->
                <div class="form-group col-md-4 col-md-offset-4">
                    {!! Form::submit($submitButton, ['class'=>'btn btn-primary form-control ']) !!}
                </div>

            </div>

        </div>