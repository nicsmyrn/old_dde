<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


               <table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>A/A</th>
                            <th>Ονοματεπώνυμο</th>
                            <th>Πατρώνυμο</th>
                            <th>Ειδικότητα</th>
                            <th>Οργανική</th>
                            <th>Υποχρεωτικό</th>
                            <th>Εντοπιότητα</th>
                            <th>Συνυπηρέτηση</th>
                            <th>Ειδική Κατηγορία</th>
                            @if($key == 'Προσωρ Τοποθέτηση & Συμπλήρωση')
                                <th>Προσωρινή Τοποθέτηση</th>
                                <th>Διάθεση για Συμπλήρωση Ωραρίου</th>
                            @elseif($key == 'Προσωρινή Τοποθέτηση')
                                <th>Προσωρινή Τοποθέτηση</th>
                            @elseif($key == 'Συμπλήρωση Ωραρίου')
                                <th>Διάθεση για Συμπλήρωση Ωραρίου</th>
                            @elseif($key == 'Απόσπαση')
                                <th>Απόσπαση</th>
                            @elseif($key == 'Ανάκληση')
                                <th>Ανάκληση</th>
                            @elseif($key == 'Πθμια')

                            @endif
                            <th>Παρατηρήσεις</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($teachers as $index=>$teacher)
                            <tr>
                                <td>{!! ($index + 1) !!}</td>
                                <td>{!! $teacher['full_name'] !!}</td>
                                <td>{!! $teacher['middle_name'] !!}</td>
                                <td>{!! $teacher['eidikotita'] !!}</td>
                                <td>{!! $teacher['organiki'] !!}</td>
                                <td>{!! $teacher['ypoxreotiko'] !!}</td>
                                <td>{!! $teacher['entopiotita'] !!}</td>
                                <td>{!! $teacher['sinipiretisi'] !!}</td>
                                <td>{!! $teacher['special'] !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

</html>
