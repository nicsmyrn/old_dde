<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


               <table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Ονοματεπώνυμο</th>
                            <th>Πατρώνυμο</th>
                            <th>Μόρια</th>
                            <th>Οργανική</th>
                            <th>Υποχρεωτικό</th>
                            <th>Εντοπιότητα</th>
                            <th>Συνυπηρέτηση</th>
                            <th>Ειδική Κατηγορία</th>
                            <th>Τύπος τοποθ.</th>
                            <th>Τοποθέτηση</th>
                            <th>Παρατηρήσεις</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($klados as $teacher)
                            <tr>
                                <td>{!! $teacher->full_name !!}</td>
                                <td>{!! $teacher->middle_name !!}</td>
                                <td>{!! $teacher->moria !!}</td>
                                <td>{!! $teacher->from !!}</td>
                                <td>{!! $teacher->ypoxreotiko !!}</td>
                                <td>{!! $teacher->entopiotita !!}</td>
                                <td>{!! $teacher->sinipiretisi !!}</td>
                                <td>{!! $teacher->special !!}</td>
                                <td>{!! Config::get('requests.placements_type')[$teacher->placements_type] !!}</td>
                                <td>{!! $teacher->to !!} για {!! $teacher->hours !!} ώρες</td>
                                <td>{!! $teacher->description !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

</html>
