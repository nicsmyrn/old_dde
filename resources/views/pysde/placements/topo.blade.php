@extends('app')

@section('header.style')
    <meta id="token" name="token" value="{!! csrf_token() !!}">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <style type="text/css">
        td button.delete{
            font-weight: bold;
            font-size: larger;
            background: none;
            border: 0;
            color:red;
            padding: 0px;
            margin: 1px;
        }

        td button.edit{
            font-weight: bold;
            font-size: larger;
            background: none;
            border: 0;
            color:#ffcc00;
            padding: 0px;
            margin:1px;
        }
    </style>
@endsection

@section('content')

    <div class="container" id="placements">

        {{--<input  v-model="date2" type="text" name="date2" id="date2" value="{!!\Carbon\Carbon::now()->format('d/m/Y') !!}">--}}
        {{--<button v-on="click:date2f" class="btn btn-default">Click</button>--}}

    <div id="loader" class="progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
            0%
        </div>
     </div>


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                   <div class="panel-heading">
                     Δημιουργία νέας πράξης Τοποθετήσεων
                   </div>
                   <div class="panel-body">
                         {!!Form::model($eidikotites,['method'=>'post', 'class'=>'form'] )!!}
                             <!-- Praxi Form Input -->

                             <div class="form-group">
                                <div class="col-md-1">
                                    {!! Form::label('decision_number', 'Πράξη:', ['class'=>'control-label']) !!}
                                     <input v-model="decision_number" name="decision_number"  type="text" class="form-control" placeholder="No"/>
                                </div>

                             </div>
                             <!-- Decision_date Form Input -->
                             <div class="form-group">
                                <div class="col-md-2">
                                 {!! Form::label('decision_date', 'Ημερομηνία:', ['class'=>'control-label']) !!}
                                 <input v-model="decision_date" name="decision_date" type="text" class="form-control" value="{!!\Carbon\Carbon::now()->format('d/m/Y') !!}"/>
                                </div>
                             </div>
                             <div class="form-group">
                                <div class="col-md-7">
                                    {!! Form::label('decision_date', 'Κλάδος - Ειδικότητα:', ['class'=>'control-label']) !!}
                                    <select v-on="change:changeEidikotita" v-model="eid_id" name="selectEidikotita" id="selectEidikotita" class="form-control">
                                      @foreach($eidikotites as $k=>$v)
                                          <option value="{!! $k !!}">{!! $v !!}</option>
                                      @endforeach
                                    </select>
                                </div>
                             </div>
                             <div v-if="placements.length"  class="btn-group">
                                <div class="col-md-2">
                                  <button  v-on="click: saveChanges" class="btn btn-primary btn-lg">Αποθήκευση</button>
                                </div>
                             </div>
                         {!!Form::close()!!}
                   </div>
                 </div>
            </div>

            <br>
            <div v-if="notZeroSchoolLength.length || placements.length"  class="form-group">

                <form v-on="submit:addPlacement">
                    <div class="form-inline">
                        <input v-model="NewRecord.name" class="form-control col-md-2" placeholder="Ονοματεπώνυμο"/>
                        <select v-model="Apospasi.type" class="form-control col-md-1">
                            <option selected value="0">Κανονική</option>
                            <option value="1">Απόσπαση εντός ΠΥΣΔΕ</option>
                        </select>
                        <select v-if="Apospasi.type==1" v-model="Apospasi.from" class="form-control col-md-1">
                            <option value="0">Επέλεξε Σχολείο</option>
                            <option v-repeat="school : allSchools" value="@{{ school.id }}">
                                @{{ school.name }}
                            </option>
                        </select>
                        <select v-if="Apospasi.type==1" v-model="Apospasi.t_o" class="form-control col-md-1">
                            <option value="0">Τοποθέτηση</option>
                            <option value="-1">ΔΔΕ Χανίων</option>
                            <option value="-2">ΙΕΚ</option>
                            <option value="-3">Άλλο</option>
                            <option v-repeat="school : allSchools" value="@{{ school.id }}">
                                @{{ school.name }}
                            </option>
                        </select>

                        <select v-if="Apospasi.type==0" v-model="NewRecord.from" id="selectFrom" class="form-control col-md-3">
                            <option selected value="0">Από</option>
                            <option value="0">Διάθεση ΠΥΣΔΕ</option>
                            <option value="0">Αναπληρωτής</option>
                            <option v-repeat="school: schoolis|sxoleiaOrganikis" value="@{{ school.id }}">
                                    @{{ school.name }}
                            </option>
                        </select>

                        <select v-if="Apospasi.type==0" v-model="NewRecord.t_o" class="form-control col-md-2">
                            <option selected value="0">Τοποθέτηση</option>
                            <option value="0">ΔΔΕ Χανίων</option>
                            <option v-repeat="school: schoolis|sxoleiaTopothetis" value="@{{ school.id }}">
                                @{{ school.name }}
                            </option>
                        </select>
                        <input v-el="focus" v-model="NewRecord.hours" class="form-control col-md-1" placeholder="Ωρες"/>
                        <input v-model="NewRecord.description" class="form-control col-md-2" type="text" placeholder="Παρατηρήσεις"/>
                        <button class="btn btn-warning">Προσθήκη</button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">

                <h1 v-if="notZeroSchoolLength.length==1" class="text-center">@{{ notZeroSchoolLength.length }} Σχολείο</h1>
                <h1 v-if="notZeroSchoolLength.length>1" class="text-center">@{{ notZeroSchoolLength.length }} Σχολεία</h1>

                <table v-if="notZeroSchoolLength.length" class="table table-bordered">
                    <thead>
                        <tr class="active">
                            <th>Σχολείο</th>
                            <th class="text-center">Ώρες</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr v-class="success: school.pivot.value>0, danger: school.pivot.value<0"  v-repeat="school: schoolis |notZeroSchool">
                            <td>@{{ school.name }}</td>
                            <td class="text-center">@{{ school.pivot.value }}</td>
                        </tr>
                    </tbody>
                </table>

                <div v-if="notZeroSchoolLength.length==0" class="alert alert-success" role="alert">
                    Συμπληρώθηκαν ΟΛΑ τα κενά και πλεονάσματα του κλάδου
                </div>

            </div>

            <div class="col-md-8">
                <br>
                <div  v-if="placements.length" >
                    <h3 class="text-center"><span v-if="placements.length">@{{ placements.length }} - </span>Τοποθετήσεις</h3>

                     <table class="table table-bordered">
                         <thead>
                             <tr>
                                 <th class="text-center">Ονοματεπώνυμο</th>
                                 <th class="text-center">Ώρες</th>
                                 <th class="text-center">Από</th>
                                 <th class="text-center">Τοποθέτηση</th>
                                 <th class="text-center">Παρατηρήσεις</th>
                                 <th class="text-center">Ενέργειες</th>
                             </tr>
                         </thead>

                         <tbody>
                             <tr v-repeat="placement: placements">
                                   <td>@{{ placement.name }} </td>
                                   <td class="text-center">@{{ placement.hours }}</td>
                                   <td>@{{ placement.from.name }}</td>
                                   <td>@{{ placement.to.name }}</td>
                                   <td>@{{ placement.description }}</td>
                                   <td class="text-center">
                                        <button alt="Επεξεργασία εγγραφής" v-on="click:editPlacement(placement)" class="edit"><i class="fa fa-pencil-square-o"></i></button>
                                        <button alt="Διαγραφή" class="delete" v-on="click:removePlacement(placement)"><i class="fa fa-times"></i></button>
                                   </td>
                             </tr>
                         </tbody>
                     </table>
                </div>
            </div>

        </div>
        <pre>
            @{{ $data | json }}
        </pre>
    </div>

@endsection

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/0.12.16/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.1.16/vue-resource.min.js"></script>
    <script src="/js/vue-strap.min.js"></script>

    <script>
//        $(document).ready(function() {
//            $('#date2').datepicker({
//                format: "dd/mm/yyyy",
//                autoclose: true,
//                daysOfWeekDisabled: [0,6]
//            });
//        });


        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

//        var alert = VueStrap.alert;

        new Vue({
            el: '#placements',

            components : {
            },

            data : {
                showRight : false,
                eid_id : null,

                placements : [
                ],

                decision_number: null,
                decision_date: '',

                NewRecord : {
                    name: '',
                    from: {id:0, value: ''},
                    t_o: {id:0, value: ''},
                    hours: null,
                    description : '',
                    overtime : 0
                },

                Apospasi : {
                    type : 0,
                    from : null,
                    t_o   : null
                }
            },

            computed: {

              notZeroSchoolLength: function () {
                  return this.schoolis.filter(function(school){
                      return school.pivot.value !=  0;
                  }.bind(this));
              }
            },

            filters : {
                sxoleiaTopothetis: function(schools){
                    return schools.filter(function(school){
                          return school.pivot.value < 0 ? school:null;
                    });
                },
                sxoleiaOrganikis: function(schools){
                     return schools.filter(function(school){
                           return school.pivot.value > 0 ? school:null;
                     });
                },
                notZeroSchool : function(schools){
                    return schools.filter(function(school){
                        return school.pivot.value != 0 ? school:null;
                    });
                }
            },

            ready: function () {
                $('#loader').hide();
            },

            methods: {
                date2f : function(e){
                    e.preventDefault();
                    console.log(this.date2);
                },
                saveChanges: function(e){
                    e.preventDefault();
                    $('#loader').show();
                    this.$http.post('placements2', {
                            placements : this.placements,
                            schools: this.schoolis,
                            eid_id:this.eid_id,
                            decision_date : this.decision_date,
                            decision_number : parseInt(this.decision_number)
                        }, function(data, status, request){
                        $('#loader').hide();
                        console.log(data);
//                        setTimeout(function(){
//                            location.reload(true);
//                         }, 200);
                    },{beforeSend: function(request){
                        request.upload.addEventListener("progress", function(e){
                            if(e.lengthComputable){
                                var persentComplete = (e.loaded / e.total) * 100;
                                $('div.progress > div.progress-bar').css({
                                    "width": persentComplete + "%"
                                }).html(persentComplete+'%');
                            }
                        }, false);
                        return request;
                    }})
                    .error(function(data, status, request){
                        console.log('error');
                    });
                },
                changeEidikotita: function(){
                    this.fetchSchools(this.eid_id);
                },

                initializePlacements: function(){
                    this.placements = [];
                },

                searchSchool : function(schools, keyword){
                    return schools.filter(function(school){
                        if (parseInt(keyword) === school.id){
                            return school;
                        }
                    },[keyword]);
                },

                addPlacement:function(e){
                    e.preventDefault();
                    var type = this.Apospasi.type;

                    if (this.Apospasi.type == 0){
                        var from_name = this.NewRecord.from;
                        var to_name = this.NewRecord.t_o;
                        var from_school = this.searchSchool(this.schoolis,from_name);
                        var to_school = this.searchSchool(this.schoolis, to_name);

                        var from_hours = from_school[0]? Math.abs(from_school[0].pivot.value): 0;
                        var to_hours = to_school[0]? Math.abs(to_school[0].pivot.value):0;

                        this.NewRecord.overtime = (from_hours < to_hours) && from_hours != 0 ? (to_hours - from_hours) :0;
                        var extra_description = this.NewRecord.overtime > 0 ? ' + '+this.NewRecord.overtime+' ωρες υπερωρία': '';

                        var placements_from_name = from_school[0] ? from_school[0].name : 'Διάθεση ΠΥΣΔΕ';
                        var placements_to_name = to_school[0] ? to_school[0].name : 'ΔΔΕ Χανιων';

                        this.placements.push({
                             type : type,
                             name : this.NewRecord.name,
                             from : {id: this.NewRecord.from, name: placements_from_name},
                             to   : {id: this.NewRecord.t_o, name: placements_to_name},
                             hours: parseInt(this.NewRecord.hours),
                             description : this.NewRecord.description + extra_description,
                             eid_id : this.eid_id,
                             overtime : this.NewRecord.overtime
                        });

                        if (from_school[0]){
                            if (this.NewRecord.overtime > 0){
                                from_school[0].pivot.value = 0;
                            }else{
                                from_school[0].pivot.value  = from_school[0].pivot.value - parseInt(this.NewRecord.hours);
                            }
                        }
                        if(to_school[0]){
                            to_school[0].pivot.value = to_school[0].pivot.value + parseInt(this.NewRecord.hours);
                        }

                        this.NewRecord.name = '';
                        this.NewRecord.from = {id:0, value: 'Something from'};
                        this.NewRecord.t_o   = {id:0, value: 'Something to'};
                        this.NewRecord.hours = null;
                        this.NewRecord.description = '';
                        this.NewRecord.overtime = 0;
                    } // end if Apospasi type == 0
                    if (this.Apospasi.type == 1){

                        var apospasi_from_name = this.Apospasi.from;
                        var apospasi_to_name = this.Apospasi.t_o;
                        var apospasi_from_school = this.searchSchool(this.schoolis,apospasi_from_name);
                        var apospasi_to_school = this.searchSchool(this.schoolis, apospasi_to_name);
                        var apospasi_from_AllSchool = this.searchSchool(this.allSchools,apospasi_from_name);
                        var apospasi_to_AllSchool = this.searchSchool(this.allSchools, apospasi_to_name);

                        var apospasi_from_hours = apospasi_from_school[0]? Math.abs(apospasi_from_school[0].pivot.value): 0;
                        var apospasi_to_hours = apospasi_to_school[0]? Math.abs(apospasi_to_school[0].pivot.value):0;

                        this.NewRecord.overtime = (apospasi_from_hours < apospasi_to_hours) && apospasi_from_hours != 0 ? (apospasi_to_hours - apospasi_from_hours) :0;
                        var extra_description = this.NewRecord.overtime > 0 ? ' + '+this.NewRecord.overtime+' ωρες υπερωρία': '';

                        var apospasi_placements_from_name = apospasi_from_school[0] ? apospasi_from_school[0].name : apospasi_from_AllSchool[0].name;
                        var apospasi_placements_to_name = apospasi_to_school[0] ? apospasi_to_school[0].name : apospasi_to_AllSchool[0].name;

                        this.placements.push({
                             type : type,
                             name : this.NewRecord.name,
                             from : {id: this.Apospasi.from, name: apospasi_placements_from_name},
                             to   : {id: this.Apospasi.t_o, name: apospasi_placements_to_name},
                             hours: parseInt(this.NewRecord.hours),
                             description : this.NewRecord.description,
                             eid_id : this.eid_id,
                             overtime : this.NewRecord.overtime
                        });
//
                        if (apospasi_from_school[0]){
                            if (this.NewRecord.overtime > 0){
                                apospase_from_school[0].pivot.value = 0;
                            }else{
                                apospasi_from_school[0].pivot.value  = apospasi_from_school[0].pivot.value - this.NewRecord.hours;
                            }
                        }else{
                            apospasi_from_school = this.searchSchool(this.allSchools,apospasi_from_name);
                            this.schoolis.push({
                                id: parseInt(apospasi_from_school[0].id),
                                name: apospasi_from_school[0].name,
                                pivot: {
                                    value: parseInt(this.NewRecord.hours)*(-1)
                                }
                            });
                        }
                        if(apospasi_to_school[0]){
                            apospasi_to_school[0].pivot.value = apospasi_to_school[0].pivot.value + parseInt(this.NewRecord.hours);
                        }else{
                            apospasi_to_school = this.searchSchool(this.allSchools, apospasi_to_name);
                             this.schoolis.push({
                                 id: parseInt(apospasi_to_school[0].id),
                                 name: apospasi_to_school[0].name,
                                 pivot: {
                                     value: parseInt(this.NewRecord.hours)
                                 }
                             });
                        }
//
                        this.NewRecord.name = '';
                        this.NewRecord.from = {id:0, value: 'Something from'};
                        this.NewRecord.t_o   = {id:0, value: 'Something to'};
                        this.NewRecord.hours = null;
                        this.NewRecord.description = '';
                        this.Apospasi.type = 0;
                        this.Apospasi.from = null;
                        this.Apospasi.t_o = null;
                    }
                },

                removePlacement: function(placement){
                    var from_id = placement.from.id;
                    var to_id = placement.to.id;
                    var from_school = this.searchSchool(this.schoolis,from_id);
                    var to_school = this.searchSchool(this.schoolis, to_id);

                    if (from_school[0]){
                        from_school[0].pivot.value  = from_school[0].pivot.value + parseInt(placement.hours) - placement.overtime;
                    }
                    if(to_school[0]){
                        to_school[0].pivot.value = to_school[0].pivot.value - parseInt(placement.hours);
                    }
                    this.placements.$remove(placement);
                },

                editPlacement: function(placement){
                    this.removePlacement(placement);
                    this.NewRecord = placement;
                    this.$$.focus.focus();
                },

                fetchSchools: function (united_eidikotita) {
                    this.$http.get('jsonAllSchools/'+united_eidikotita, function (allSchools){
                        this.$set('allSchools', allSchools);
                    });
                    this.$http.get('jsonSchools/'+united_eidikotita, function (schools) {
                        this.$set('schoolis', schools);
                        this.initializePlacements();
                        //this.users.push(this.single);
                    });
                },

                setSchoolName: function(e){
                    console.log(this.scho);
                    this.NewRecord.from.school_name = 'asdfh';
                }
            }
        })
    </script>
@endsection