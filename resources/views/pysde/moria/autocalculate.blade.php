@extends('app')

@section('title')
    Αυτόματος Υπολογισμός Μορίων Αποσπάσεων
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('loader')
    @include('loader')
@endsection

@section('content')
    <h1 class="page-heading">Καθηγητές που είναι σε διαδικασία απόσπασης</h1>

    @if($requests->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει καθηγητής που έχει ζητήσει απόσπαση</div>
                </div>
            </div>
        </div>
    @else
        <div class="col-md-12 text-center">
            {!! Form::open(['id'=>'aitisiForm']) !!}
                {!! Form::submit('Υπολογισμός Μορίων',['class'=> 'btn btn-primary btn-lg' ,'id'=>'submitCalculate']) !!}
            {!! Form::close() !!}
        </div>

        <table id="requests" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr class="active">
                    <th>Αρ. Μητρώου</th>
                    <th>Ονοματεπώνυμο</th>
                    <th>Πατρώνυμο</th>
                    <th>Ειδικότητα</th>
                    <th>Οργανική</th>
                    <th>Μόρια Απόσπασης</th>
                </tr>
            </thead>

            <tbody>
                @foreach($requests as $request)
                    <tr>
                        <td>{!! $request->teacher->teacherable->am !!}</td>
                        <td>{!! $request->teacher->user->full_name !!}</td>
                        <td>{!! $request->teacher->middle_name !!}</td>
                        <td>{!! $request->teacher->klados_name !!}</td>
                        <td>{!! $request->teacher->teacherable->organiki_name !!}</td>
                        <td>{!! $request->teacher->teacherable->moria_apospasis !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {

        $('#submitCalculate').on('click', function(e){
            e.preventDefault();
            $('#loader').removeClass('invisible')
                .addClass('loading');
            setTimeout(function(){
                $('#aitisiForm').submit();
            }, 6000);
        });

            var table = $('#requests').DataTable({
                    "order": [[ 1, "asc" ]],
                    "pageLength": 50,
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 2 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


