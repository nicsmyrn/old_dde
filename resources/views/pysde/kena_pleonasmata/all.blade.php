@extends('app')


@section('content')
        <a class="btnPrint btn btn-success">Μετατροπή σε PDF</a>        
        <hr>
        <div class="row">
            <div class="col-md-12">
                @include('pysde.kena_pleonasmata.all_table')                
            </div>
        </div>
@endsection


@section('scripts.footer')
    @include('pysde.kena_pleonasmata.edit_footer')
@endsection