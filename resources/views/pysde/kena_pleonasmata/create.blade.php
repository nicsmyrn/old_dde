@extends('pysde.kena_pleonasmata.panel')

@section('panel-body')
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> Υπάρχει κάποιο πρόβλημα με τα στοιχεία.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				
					<h3 class="text-center">
			        <select class="col-md-12">
			        	<option></option>
			            @foreach($schools as $k=>$v)
			            	<option value="{!! action('KenaController@edit',str_replace(' ', '-', $v)) !!}">{!! $v !!}</option>	
			            @endforeach                      	
			        </select>
			        </h3>
			</div>
			<div class="col-md-3">
				<a href="{!! action('KenaController@changeSchoolAccess') !!}" class="btn btn-{!! $schools_can_edit?'danger':'success' !!}">Αλλαγή Κατάστασης</a>
			</div>
		</div>	
@endsection