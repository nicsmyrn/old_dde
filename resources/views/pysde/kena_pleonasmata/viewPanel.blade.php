@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">
					Εμφάνιση Κενών - Πλεονασμάτων
				</div>
				<div class="panel-body">
                    @yield('panel-body')
				</div>
			</div>
		</div>

	</div>
</div>
@endsection

@section('scripts.footer')
	@include('pysde.kena_pleonasmata.edit_footer', ['name'=> ''])
@endsection