@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">
    <style>
        .eidikotita {
            text-align: center;
            font-size: 15pt;
        }
        .keno{
            background-color: #008000;
            color: #ffff00;
            font-weight: bold;
        }
        .pleonasma {
            background-color: red;
            color: #0000ff;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center">
            <select v-model="currentSchool" v-on:change="fetchEidikotitesForSchool" style="font-size: large">
                <option v-for="(key, val) in schools" v-bind:value="key">@{{ val }}</option>
            </select>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 form-group" v-for="eidikotita in eidikotites" style="padding-bottom10px;border-bottom: 2px solid #000000;height: 50px">
                <input type="text"
                        size="2"
                        maxlength="2"
                        v-model="eidikotita.number"
                        v-bind:class="{
                            'eidikotita' : true,
                            'keno'  : eidikotita.number < 0,
                            'pleonasma' : eidikotita.number > 0
                        }"
                        value="@{{ eidikotita.number }}"
                        @keyup.enter="saveEidikotita(eidikotita.id)"
                        @keydown.tab="saveEidikotita(eidikotita.id)"
                />
                <label for="@{{ eidikotita.id }}">@{{ eidikotita.name }}</label>
        </div>
    </div>


    <alert :type="alert.type" :important="alert.important">
        <div slot="alert-header">
            @{{ alert.header }}
        </div>
        <div slot="alert-body">
            @{{ alert.message }}
        </div>
    </alert>
@endsection


@section('scripts.footer')
    <script src="{!! elixir('js/OrganikaKenaPleonasmata.js') !!}"></script>
@endsection