<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ΚΛΑΔΟΣ</th><th>Ειδικότητα</th>
                    @foreach($schools as $school)
                        <th class="header-rotate">{!! $school->name !!}</th>
                    @endforeach
                </tr>
            </thead>
            
            <tbody>
                @foreach($eidikotites as $eidikotita)
                    @if($eidikotita->notZeroCellValues() != 'NULL')
                        <tr>
                            <td>{!! $eidikotita->slug_name !!}</td>
                            <td>{!! str_limit($eidikotita->name,15) !!}</td>
                            @foreach($schools as $school)
                                <td class='text-center'>
                                    {!! $school->cellValueEidikis($eidikotita->id, $eidikotita->slug) !!}
                                </td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
</html>
