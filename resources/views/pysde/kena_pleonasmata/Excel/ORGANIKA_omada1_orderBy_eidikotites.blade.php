<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <table border="1" id="organika" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ΚΛΑΔΟΣ</th><th>Ειδικότητα</th>
                    @foreach($header1 as $header)
                        <th class="header-rotate">{!! $header->name !!}</th>
                    @endforeach
                </tr>
            </thead>

            <tbody>
                @foreach($eidikotites as $eidikotita)
                    @if($eidikotita->notZeroOrganikaOmada1() != 'NULL')
                        <tr>
                            <td>{!! $eidikotita->slug_name !!}</td>
                            <td>{!! str_limit($eidikotita->name,15) !!}</td>
                            @foreach($header1 as $school)
                                    @if($school->cellValueOrganika($eidikotita->id, $eidikotita->slug) > 0)
                                        <td class='text-center' style="color: #ff0000">
                                            {!! $school->cellValueOrganika($eidikotita->id, $eidikotita->slug) !!}
                                        </td>
                                    @else
                                        <td class='text-center' style="color: #008000">
                                            {!! $school->cellValueOrganika($eidikotita->id, $eidikotita->slug) !!}
                                        </td>
                                    @endif
                            @endforeach
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
</html>
