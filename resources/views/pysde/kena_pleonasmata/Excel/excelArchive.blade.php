@extends('app')

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">Ηλεκτρονική Αρχειοθέτηση</h3>
                    </div>
                    <div class="panel-body text-center">
                        <div>
                            <table class="table table-hover">
                                <tbody>
                                    @foreach($excel_files as $file)
                                        <tr>
                                            <td><h4>
                                                <a href="{!! action('ExcelController@excelDownloadKenaPleonasmata',$file->file_name) !!}"><i class="fa fa-file-excel-o"></i> {!! $file->file_name !!}</a>
                                            </h4></td>
                                            <td>
                                                <h4>{!! $file->description !!}</h4>
                                            </td>
                                        </tr>
                                    @endforeach                                
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                </div>                 
            </div>
        </div>
    </div>
@endsection

@section('scripts.footer')
    @include('pdf.scripts')
@endsection        
