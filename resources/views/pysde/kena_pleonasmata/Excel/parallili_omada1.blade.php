<html>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <table border="1" id="parallili" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th></th>
                    <th colspan="3" style="text-align: center">Φιλόλογοι</th>
                    <th colspan="3" style="text-align: center">Μαθηματικοί</th>
                    <th colspan="3" style="text-align: center">Φυσικοί</th>
                    <th colspan="3" style="text-align: center">Χημικοί</th>
                    <th colspan="3" style="text-align: center">Βιολόγοι</th>
                    <th colspan="3" style="text-align: center">Γεωλόγοι</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td></td>
                    @for($i=1; $i<=6; $i++)
                        <td>Ειδικής Αγωγής</td>
                        <td>Braile</td>
                        <td>Νοηματικής</td>
                    @endfor
                </tr>
                @foreach($schools as $school)
                    @if($school->notZeroParalliliCell() != 'NULL')
                        <tr>
                            <td>{!! $school->name !!}</td>
                            @foreach($school->eidikotites as $eidikotita)
                                <td>{!! $eidikotita->pivot->value !!}</td>
                            @endforeach
                        </tr>
                    @endif
                @endforeach
            </tbody>

            <tfoot>
                <tr>
                    <td>Σύνολα:</td>
                        @foreach($eidikotites as $eidikotita)
                            <td>{!! $eidikotita->schools->sum('pivot.value') !!}</td>
                        @endforeach
                </tr>
            </tfoot>
        </table>

</html>
