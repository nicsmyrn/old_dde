<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th rowspan="2">ΚΛΑΔΟΣ</th>
                    <th rowspan="2">Ειδικότητα</th>
                    <th colspan="8">
                        ΟΜΑΔΑ 2
                    </th>
                    <th colspan="4">
                        ΟΜΑΔΑ 3
                    </th>
                    <th colspan="4">
                        ΟΜΑΔΑ 4
                    </th>
                    <th colspan="4">
                        ΟΜΑΔΑ 5
                    </th>
                    <th colspan="2">
                        ΟΜΑΔΑ 6
                    </th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    @foreach($header2 as $header)
                        <th class="header-rotate">{!! $header->name!!}</th>
                    @endforeach
                </tr>
            </thead>
            
            <tbody>
            <?php
                $counterPE04 = \App\Eidikotita::countPE04();
             ?>
                @foreach($eidikotites as $eidikotita)
                    @if($eidikotita->notZeroCellValues() != 'NULL')
                        <tr>
                            <td>{!! $eidikotita->slug_name !!}</td>
                            <td>{!! str_limit($eidikotita->name,15) !!}</td>
                            @foreach($header2 as $school)
                                @if($school->type == 'Γυμνάσιο' && $eidikotita->united == 'pe04')
                                    @if($eidikotita->slug == 'pe0401')
                                        @if($school->cellValue($eidikotita->id, $eidikotita->slug) < 0 )
                                            <td rowspan="{{$counterPE04}}" class="text-center">
                                                {!! $school->cellValue($eidikotita->id, $eidikotita->slug) !!}
                                            </td>
                                            <!-- PE04 IS NEGATIVE -->
                                        @else
                                             <td class="text-center">
                                                 {!! $school->cellValue($eidikotita->id, $eidikotita->slug) !!}
                                             </td>
                                            <!-- PE04 IS POSITIVE -->
                                        @endif
                                    @else
                                        @if($school->cellValue($eidikotita->id, 'pe0401') < 0 )
                                            <td></td>
                                        @else
                                              <td class="text-center">
                                                 {!! $school->cellValue($eidikotita->id, $eidikotita->slug) !!}
                                              </td>
                                        @endif

                                    @endif

                                @else
                                    <td class='text-center'>
                                        {!! $school->cellValue($eidikotita->id, $eidikotita->slug) !!}
                                    </td>
                                @endif
                            @endforeach
                        </tr>
                    @endif
                @endforeach
                    <tr><td colspan="2">Project</td>
                        @foreach($header2 as $school)
                            <td>
                                {!! $school->has_project !!}
                            </td>
                        @endforeach
                    </tr>
                    <tr><td colspan="2">Τεχνολογία</td>
                        @foreach($header2 as $school)
                            <td>
                                {!! $school->has_technology !!}
                            </td>
                        @endforeach
                    </tr>
                    <tr><td colspan="2">Γραμμικό Σχέδιο</td>
                        @foreach($header2 as $school)
                            <td>
                                {!! $school->has_sxedio !!}
                            </td>
                        @endforeach
                    </tr>
                    <tr><td colspan="2">ΠΕ09-10-13</td>
                        @foreach($header2 as $school)
                            <td>
                                {!! $school->has_pe091013 !!}
                            </td>
                        @endforeach
                    </tr>
            </tbody>
        </table>
</html>

