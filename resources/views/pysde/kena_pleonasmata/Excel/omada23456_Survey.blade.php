<html>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
        <table id="kena_pleonasmata" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>ΚΛΑΔΟΣ</th>
                    <th>Ειδικότητα</th>
                    @foreach($schools2 as $school)
                        <th>{!! $school->name !!}</th>
                    @endforeach
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td colspan="2">Δυναμικότητα</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->total !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">ΜΟ μαθητών ανά τετραγωνικό μέτρο</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->studentpermeters !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td rowspan="3">Βιβλιοθήκη</td>
                    <td>Ανέγερση</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergL == 0 ? 'ΟΧΙ':'ΝΑΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Έτος</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergL_year == 0 ? '': $school->survey->ergL_year !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Κατάσταση</td>
                    @foreach($schools2 as $school)
                        <td>@if($school->survey->ergL_status == 1) καλή @elseif($school->survey->ergL_status == 2) Δεν Λειτουργεί @elseif($school->survey->ergL_status == 3) Υπό κατασκευή @endif</td>
                    @endforeach
                </tr>
                <tr>
                    <td rowspan="3">Εργαστήριο Πληροφορικής</td>
                    <td>Ανέγερση</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergP == 0 ? 'ΟΧΙ':'ΝΑΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Έτος</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergP_year == 0 ? '': $school->survey->ergP_year !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Κατάσταση</td>
                    @foreach($schools2 as $school)
                        <td>@if($school->survey->ergP_status == 1) καλή @elseif($school->survey->ergP_status == 2) Δεν Λειτουργεί @elseif($school->survey->ergP_status == 3) Υπό κατασκευή @endif</td>
                    @endforeach
                </tr>
                <tr>
                    <td rowspan="3">Εργαστήριο Χημείας</td>
                    <td>Ανέγερση</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergX == 0 ? 'ΟΧΙ':'ΝΑΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Έτος</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergX_year == 0 ? '': $school->survey->ergX_year !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Κατάσταση</td>
                    @foreach($schools2 as $school)
                        <td>@if($school->survey->ergX_status == 1) καλή @elseif($school->survey->ergX_status == 2) Δεν Λειτουργεί @elseif($school->survey->ergX_status == 3) Υπό κατασκευή @endif</td>
                    @endforeach
                </tr> 
                <tr>
                    <td rowspan="3">Γυμναστήριο</td>
                    <td>Ανέγερση</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergG == 0 ? 'ΟΧΙ':'ΝΑΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Έτος</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergG_year == 0 ? '': $school->survey->ergG_year !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Κατάσταση</td>
                    @foreach($schools2 as $school)
                        <td>@if($school->survey->ergG_status == 1) καλή @elseif($school->survey->ergG_status == 2) Δεν Λειτουργεί @elseif($school->survey->ergG_status == 3) Υπό κατασκευή @endif</td>
                    @endforeach
                </tr>
                <tr>
                    <td rowspan="3">Αίθουσα πολλαπλών χρήσεων</td>
                    <td>Ανέγερση</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergE == 0 ? 'ΟΧΙ':'ΝΑΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Έτος</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergE_year == 0 ? '': $school->survey->ergE_year !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Κατάσταση</td>
                    @foreach($schools2 as $school)
                        <td>@if($school->survey->ergE_status == 1) καλή @elseif($school->survey->ergE_status == 2) Δεν Λειτουργεί @elseif($school->survey->ergE_status == 3) Υπό κατασκευή @endif</td>
                    @endforeach
                </tr>
                <tr>
                    <td rowspan="3">Τραπεζαρία</td>
                    <td>Ανέγερση</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergT == 0 ? 'ΟΧΙ':'ΝΑΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Έτος</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->ergT_year == 0 ? '': $school->survey->ergT_year !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td></td>
                    <td>Κατάσταση</td>
                    @foreach($schools2 as $school)
                        <td>@if($school->survey->ergT_status == 1) Λειτουργεί @elseif($school->survey->ergT_status == 2) Δεν Λειτουργεί @elseif($school->survey->ergT_status == 3) Υπό κατασκευή @endif</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">Αρ. WC/μαθητή</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->wc !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">Αρ. βρυσών/μαθητή</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->faucet !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">Αντισεισμική προστασία</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->earthquake_check == 1 ? 'ΝΑΙ':'ΟΧΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">Ασφάλεια αύλειου χώρου</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->yard_check == 1 ? 'ΝΑΙ':'ΟΧΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">Πυρασφάλεια</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->fire_check == 1 ? 'ΝΑΙ':'ΟΧΙ' !!}</td>
                    @endforeach
                </tr>
                <tr>
                    <td colspan="2">διπλοβάρδια</td>
                    @foreach($schools2 as $school)
                        <td>{!! $school->survey->double_hours == 1 ? 'ΝΑΙ':'ΟΧΙ' !!}</td>
                    @endforeach
                </tr>

            </tbody>

        </table>
</html>