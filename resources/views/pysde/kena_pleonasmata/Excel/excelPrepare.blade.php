@extends('app')

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="container">
        
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">Δημιουργία  πίνακα Κενών - Πλεονασμάτων</h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['action'=>'ExcelController@excelCreate','method'=>'POST', 'class'=>'form-horizontal']) !!}
                                <div class="form-group">
                                    {!! Form::label('excel_date', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-5">
                                        {!! Form::text('excel_date',\Carbon\Carbon::now()->format('d/m/Y'), ['class'=>'form-control text-center data_picker', 'id'=>'date']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description', 'Παρατηρήσεις', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('description', null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                     {!! Form::submit('Δημιουργία Αρχείου Excel', ['class'=>'btn btn-primary']) !!}
                                </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
               <div class="panel panel-success">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">Δημιουργία  πίνακα Παράλληλης Στήριξης</h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['action'=>'ExcelController@excelParalliliEidiki','method'=>'POST', 'class'=>'form-horizontal']) !!}
                                <div class="form-group">
                                    {!! Form::label('excel_date_parallili', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-5">
                                        {!! Form::text('excel_date_parallili',\Carbon\Carbon::now()->format('d/m/Y'), ['class'=>'form-control text-center', 'id'=>'dateParallili']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description_parallili', 'Παρατηρήσεις', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('description_parallili', null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                     {!! Form::submit('Δημιουργία...', ['class'=>'btn btn-success']) !!}
                                </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
           <div class="col-md-6">
               <div class="panel panel-warning">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">Δημιουργία  πίνακα Ειδικής Αγωγής</h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['action'=>'ExcelController@createTmimataEdaksis','method'=>'POST', 'class'=>'form-horizontal']) !!}
                                <div class="form-group">
                                    {!! Form::label('excel_date_eidiki', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-5">
                                        {!! Form::text('excel_date_eidiki',\Carbon\Carbon::now()->format('d/m/Y'), ['class'=>'form-control text-center', 'id'=>'dateEidiki']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description_eidiki', 'Παρατηρήσεις', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('description_eidiki', null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                     {!! Form::submit('Δημιουργία...', ['class'=>'btn btn-warning']) !!}
                                </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-danger">
                    <div class="panel-heading text-center">
                        <h3 class="panel-title">Δημιουργία  πίνακα ΟΡΓΑΝΙΚΩΝ Κενών - Πλεονασμάτων</h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['action'=>'ExcelController@excelCreateOrganika','method'=>'POST', 'class'=>'form-horizontal']) !!}
                                <div class="form-group">
                                    {!! Form::label('excel_date_organika', 'Ημερομηνία:', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-5">
                                        {!! Form::text('excel_date_organika',\Carbon\Carbon::now()->format('d/m/Y'), ['class'=>'form-control text-center data_picker', 'id'=>'dateOrganika']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    {!! Form::label('description', 'Παρατηρήσεις', ['class'=>'col-md-3 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('description', null, ['class'=>'form-control']) !!}
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                     {!! Form::submit('Δημιουργία...', ['class'=>'btn btn-primary']) !!}
                                </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('scripts.footer')
    @include('pdf.scripts')
@endsection

