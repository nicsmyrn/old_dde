@extends('pysde.kena_pleonasmata.viewPanel')

@section('panel-body')
	<div class="row">
		<div class="col-md-3"></div>
		<div class="col-md-6">
				<h3 class="text-center">
		        <select class="col-md-12">
		        	<option></option>
		            @foreach($schools as $school)
		            	<option value="{!! action('KenaController@show',str_replace(' ', '-', $school)) !!}">{!! $school !!}</option>	
		            @endforeach                      	
		        </select>
		        </h3>
		</div>
		<div class="col-md-3"></div>
	</div>	
@endsection