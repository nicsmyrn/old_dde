@extends('pysde.kena_pleonasmata.panel')


@section('panel-body')
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
					<h3>
			        <select class="col-md-12">
			            @foreach($schools_list as $k=>$v)
			            	<option value="{!! action('KenaController@edit', str_replace(' ', '-', $v)) !!}"
			                	@if(str_replace(' ', '-', $school->name) == str_replace(' ', '-', $v))
			                		selected
			                	@endif
			                >
			            	{!! $v !!}</option>
			            @endforeach
			        </select>
			        </h3>
			</div>
			<div class="col-md-3">
				<a href="{!! action('KenaController@changeSchoolAccess') !!}" class="btn btn-{!! $schools_can_edit?'danger':'success' !!}">Αλλαγή Κατάστασης</a>
			</div>
		</div>

	@include('school.edit_panel')
@endsection