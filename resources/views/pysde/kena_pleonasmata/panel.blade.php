@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Καταχώρηση Κενών - Πλεονασμάτων
						@if($schools_can_edit)
							<span class="pull-right">
								<h5>Κατάσταση: <i class="fa fa-unlock"></i>
								<span class="label label-success">
									Ξεκλείδωτο
								</span></h5>
							</span>
						@else
							<span class="pull-right">
								<h5>Κατάσταση: <i class="fa fa-lock"></i>
								<span class="label label-danger">
									Κλειδωμένο
								</span></h5>
							</span>
						@endif
				</div>
				<div class="panel-body">
                    @yield('panel-body')
				</div>
			</div>
		</div>

	</div>
</div>
@endsection

@section('scripts.footer')
	@include('pysde.kena_pleonasmata.edit_footer', ['name'=>''])
@endsection