<script>
    $(document).ready(function() {
        $(".btnPrint").printPage({
            url: "{!! action('KenaController@printShow', str_replace(' ', '-', $name)) !!}",
            attr: "href",
            message:"Εκτυπώνει"
         });
         
         
        $('select').change(function(){
            var url = $(this).val();
            window.location = url;
        })
        .select2({
            placeholder: "Επιλογή Σχολείου",
            theme: "classic",
            language: {
                noResults: function() {
                    return "Δεν υπάρχει αποτέλεσμα αναζήτησης";
                }
            }
        });
        

        $('input.form-control').on('click focus', function(e){
            $(this).addClass('editing')
                .val('');    
        });
        
        $('#owners').on('click', function(e){
            e.preventDefault();
            $('.div_ownsers').toggleClass('display_class');
        });
        
        $('[data-toggle="tooltip"]').tooltip();
        
        $("#checkbox_tech").on('change', function(){
            if (this.checked) $("#tech_eidikotites").show(1000);
            if (!this.checked) $("#tech_eidikotites").hide(1000);
        });
        
    });
</script>