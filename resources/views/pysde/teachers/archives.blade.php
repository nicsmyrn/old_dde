@extends('app')

@section('title')
    Αρχείο Πρωτοκολλημένων Αιτήσεων Καθηγητών
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')
    <h1 class="page-heading">Πρωτοκολλημένες ΑΙΤΗΣΕΙΣ ΚΑΘΗΓΗΤΩΝ</h1>

    @if($requests->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει καμία Αίτηση πρωτοκολλημένη</div>
                </div>
            </div>
        </div>
    @else
        <table id="requests" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr class="active">
                    <th class="text-center">Κωδικός Αίτησης</th>
                    <th class="text-center">Είδος</th>
                    <th class="text-center">Ημ/νια Πρωτοκόλλου</th>
                    <th class="text-center">Αρ. Πρωτοκόλλου</th>
                    <th class="text-center">Φάκελος</th>
                    <th>Ονοματεπώνυμο</th>
                    <th>Πατρώνυμο</th>
                    <th>Ειδικότητα</th>
                    <th>Σχέση</th>
                    <th class="text-center">Ενέργειες</th>
                </tr>
            </thead>

            <tbody>
                @foreach($requests as $request)
                    <tr>
                        <td>{!! $request->unique_id !!}</td>
                        <td class="text-center">{!! $request->aitisi_type !!}</td>
                        <td class="text-center">{!! $request->protocol_date !!}</td>
                        <td class="text-center">{!! $request->protocol_number !!}</td>
                        <td class="text-center">{!! $request->f_name !!}</td>
                        <td>{!! $request->teacher->user->full_name !!}</td>
                        <td>{!! $request->teacher->middle_name !!}</td>
                        <td>{!! $request->teacher->klados_name !!}</td>
                        <td>{!! $request->teacher->teacherable_type == 'App\Monimos' ? 'Μόνιμος': 'Αναπληρωτής' !!}</td>
                        <td class="text-center">
                            <a href="{!! route('Pysde::Teachers::downloadPDF',[str_replace(' ','-',$request->teacher->user->full_name),$request->unique_id]) !!}"><img style="width: 30px; height: 30px" src="/img/download_icon.jpg"/> </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#requests').DataTable({
                    "order": [[ 3, "desc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0,6,8,9 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


