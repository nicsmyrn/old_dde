@extends('app')

@section('title')
    Διαχείριση Αιτήσεων Καθηγητών
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')

    <h1 class="page-heading">Διαχείριση ΑΙΤΗΣΕΩΝ ΚΑΘΗΓΗΤΩΝ</h1>

    @if($requests->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει καμία Αίτηση καθηγητή σε εκκρεμότητα</div>
                </div>
            </div>
        </div>
    @else
        <table id="requests" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr class="active">
                    <th>Ονοματεπώνυμο</th>
                    <th>Πατρώνυμο</th>
                    <th>Ειδικότητα</th>
                    <th>Σχέση</th>
                    <th class="text-center">Ημ/νια</th>
                    <th class="text-center">Κωδικός Αίτησης</th>
                    <th class="text-center">Είδος</th>
                    <th class="text-center">Παρατηρήσεις</th>
                </tr>
            </thead>

            <tbody>
                @foreach($requests as $request)
                    <tr>
                        <td>
                            <a href="{!! route('Pysde::Teachers::check', [$request->teacher->id]) !!}">
                                {!! $request->teacher->user->full_name !!}
                            </a>
                        </td>
                        <td>{!! $request->teacher->middle_name !!}</td>
                        <td>{!! $request->teacher->klados_name !!}</td>
                        <td>{!! $request->teacher->teacherable_type == 'App\Monimos' ? 'Μόνιμος': 'Αναπληρωτής' !!}</td>
                        <td>{!! $request->date_request !!}</td>
                        <td>{!! $request->unique_id !!}</td>
                        <td>{!! $request->aitisi_type !!}</td>
                        <td class="text-center">
                            @if($request->teacher->is_checked)
                                @if($request->protocol_number != null && $request->file_name != null)
                                    <a href="#">PDF ARXEIO</a>
                                @else
                                    <a href="{!! route('Pysde::Teachers::giveProtocol',[$request->unique_id]) !!}">
                                       <span class="label label-info">Δώσε Πρωτόκολλο</span>
                                    </a>
                                @endif
                            @else
                                <a href="{!! route('Pysde::Teachers::check', [$request->teacher->id]) !!}">
                                    <span class="label label-danger">Υπό έλεγχο</span>
                                </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#requests').DataTable({
                    "order": [[ 4, "desc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 1,3,5,7 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


