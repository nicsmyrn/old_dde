<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


               <table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Αίτηση</th>
                            <th>Ονοματεπώνυμο</th>
                            <th>Πατρώνυμο</th>
                            <th>Κλάδος</th>
                            @if ($type == 'E3' || $type == 'E4')
                                <td>Μόρια Αποσπασης</td>
                            @else
                                <td>Μόρια</td>
                            @endif
                            <th>Οργανική</th>
                            @if($type == 'E1' || $type == 'E2' || $type == 'E6')
                                <th>Ώρες Συμπλήρωσης</th>
                                <th>Ομάδα</th>
                                <th>Υποχρεωτικό</th>
                            @endif
                            <th>Εντοπιότητα</th>
                            <th>Συνυπηρέτηση</th>
                            <th>Ειδ. Κατηγορία</th>
                            <th>Προτιμήσεις</th>
                            <th>Παρατηρήσεις</th>
                            <th>Date</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($requests as $request)
                            <tr>
                                <td>{!! $request->aitisi_type !!}</td>
                                <td>{!! $request->teacher->user->full_name !!}</td>
                                <td>{!! $request->teacher->middle_name !!}</td>
                                <td>{!! $request->teacher->klados_name !!}</td>
                                @if ($type == 'E3' || $type == 'E4')
                                    <td>{!! $request->teacher->teacherable->moria_apospasis !!}</td>
                                @else
                                    <td>{!! $request->teacher->teacherable->moria !!}</td>
                                @endif
                                <td>{!! $request->teacher->teacherable->organiki_name; !!}</td>
                                @if($type == 'E1' || $type == 'E2' || $type == 'E6')
                                    <td>{!! $request->hours_for_request !!}</td>
                                    <td>{!! $request->teacher->teacherable->group_name !!}</td>
                                    <td>{!! $request->teacher->teacherable->orario !!}</td>
                                @endif
                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_entopiotitas] !!}</td>
                                <td>{!! \Config::get('requests.dimos')[$request->teacher->dimos_sinipiretisis] !!}</td>
                                <td>{!! $request->teacher->special_situation ? 'ΝΑΙ': 'ΟΧΙ' !!}</td>
                                <td></td>
                                <td>{!! $request->description !!}</td>
                                <td>{!! $request->date_request !!}</td>
                            </tr>

                        @endforeach
                    </tbody>
                </table>

</html>
