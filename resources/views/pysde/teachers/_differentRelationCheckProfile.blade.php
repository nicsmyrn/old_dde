    <div class="row">
        <div class="col-md-10 col-md-offset-1 text-center">
            @if($teacher->teacherable_type == 'App\Monimos')
                <h3><span style="letter-spacing: 0.4em" class="label label-default">ΜΟΝΙΜΟΣ</span></h3>
            @elseif($teacher->teacherable_type == 'App\Anaplirotis')
                <h3><span style="letter-spacing: 0.4em" class="label label-default">ΑΝΑΠΛΗΡΩΤΗΣ</span></h3>
            @endif
                <h3>
                    Ο καθηγητής ζήτησε την Αλλαγή της Σχέσης με το Δημόσιο σε:
                    <span id="fake_am" class="suggestion-fake">
                        @if($teacher->fake->teacherable_type == 'App\FakeMonimos')
                            ΜΟΝΙΜΟΣ
                        @elseif($teacher->fake->teacherable_type == 'App\FakeAnaplirotis')
                            ΑΝΑΠΛΗΡΩΤΗΣ
                        @endif
                    </span>
                </h3>
                <br>
                    <div class="btn-group">
                            <a href="{!! action('TeacherController@confirmRequest', $teacher->id) !!}" class="btn btn-primary button_sxesi">
                                <i class="fa fa-exchange" aria-hidden="true"></i>
                                 πραγματοποίηση του αιτήματος
                            </a>

                            <a  href="{!! action('TeacherController@cancelRequest', $teacher->id) !!}" class="btn btn-warning button_cancel_sxesi">
                                <i class="fa fa-times" aria-hidden="true"></i>
                                Ακύρωση του αιτήματος
                            </a>
                    </div>
        </div>
    </div>