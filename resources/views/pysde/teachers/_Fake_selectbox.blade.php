@if(isset($var))
<h5>
    <small>πρόταση για αλλαγή:</small>
    <span id="fake_am" class="suggestion-fake">
        {!! $selectCollection[$var] !!}
    </span>
    <input type="hidden" class="suggestion-fake" value="{!! $var !!}"/>
    <button class="btn btn-default btn-xs button_select_box">
        <i class="fa fa-exchange button-change" aria-hidden="true"></i>
        αλλαγή
    </button>
     <button class="btn btn-default btn-xs button_select_box_cancel">
        <i class="fa fa-remove" aria-hidden="true"></i>
        απόρριψη
     </button>
</h5>
@endif