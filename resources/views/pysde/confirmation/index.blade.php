@extends('app')


@section('content')
<div class="container-fluid">


	<div class="row">
		<div class="col-md-8 col-md-offset-2">
            <p class="text-center">
                <a class="btnPrint btn btn-success">Εκτύπωση όλων των παρατηρήσεων</a>
            </p>

			<div class="panel panel-default">
				<div class="panel-heading">Πίνακας επικαιροποίησης Σχολικών Μονάδων</div>
				<div class="panel-body">
				    <table class="table table-hover">
				        <thead>
				            <th>Σχολική Μονάδα</th>
				            <th>Ημερομηνία Επικαιροποίησης</th>
				            <th>Παρατηρήσεις Σχολείου</th>
				        </thead>
				        <tbody>
				            @foreach($schools as $school)
				                <tr>
                                    <td>{!! $school->name !!}</td>
                                    <td>{!! $school->confirmation !!}</td>
                                    <td>{!! $school->description !!}</td>
				                </tr>
				            @endforeach
				        </tbody>
				    </table>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts.footer')
    @include('pysde.confirmation.index_footer')
@endsection