<script>
    $(document).ready(function() {
        $(".btnPrint").printPage({
            url: "{!! action('KenaController@printDescriptions') !!}",
            attr: "href",
            message:"Εκτυπώνει"
         });
    });
</script>