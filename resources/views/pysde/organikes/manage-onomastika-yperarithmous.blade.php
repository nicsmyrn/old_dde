@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">
    <style>
            [v-cloak] {
              display: none;
            }

            .is_yperarithmos{
                background-color: #90EE90;
                font-weight: bold;
            }

    </style>
@endsection

@section('content')

    <h1 class="text-center">Ονομαστικά Υπεράριθμοι</h1>
    <div class="row">
        <div class="col-md-6 col-md-offset-3 text-center">
            <select v-model="currentSchool" v-on:change="fetchTeachersForSchool" style="font-size: large">
                <option v-for="(key, val) in schools" v-bind:value="key">@{{ val }}</option>
            </select>
        </div>
    </div>

    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>A/A</th>
                    <th>Ειδικότητα</th>
                    <th>Ονοματεπώνυμο</th>
                    <th>Πατρώνυμο</th>
                    <th>Σχέση</th>
                    <th>Υπεράριθμος Ονομαστικά</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="teacher in teachers" v-bind:class="{
                    'is_yperarithmos' : teacher.onomastika_yperarithmos
                }">
                    <td class="table-danger">@{{ $index + 1 }}</td>
                    <td>@{{ teacher.eidikotita_klados }}</td>
                    <td>@{{ teacher.full_name }}</td>
                    <td>@{{ teacher.middle_name }}</td>
                    <td>@{{ teacher.sxesi }}</td>
                    <td>
                        <input type="checkbox" v-model="teacher.onomastika_yperarithmos" @click="toggleYperarithmiaValue(teacher)">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>


    <alert :type="alert.type" :important="alert.important">
        <div slot="alert-header">
            @{{ alert.header }}
        </div>
        <div slot="alert-body">
            @{{ alert.message }}
        </div>
    </alert>
@endsection


@section('scripts.footer')
    <script src="{!! elixir('js/OnomastikaYperarithmoi.js') !!}"></script>
@endsection