<div class="modal fade" id="schoolDescModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-success">
        <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
        <strong><h4 class="modal-title" id="exampleModalLabel"></h4></strong>
      </div>
      <div class="modal-body">
        {!! Form::open(['id'=>'formSchoolDetails']) !!}
            {!! Form::hidden('schoolID',null, ['id'=>'schoolID']) !!}
                <!-- Last Name  Form Input -->
                <div class="form-group">
                    <label for="admin-last-name" class="control-label">Επώνυμο Διευθυντή:</label>
                    <input type="text" class="form-control" id="admin-last-name" name="last_name">
                </div>

                <!-- First Name  Form Input -->
                <div class="form-group">
                    <label for="admin-first-name" class="control-label">Όνομα Διευθυντή:</label>
                    <input type="text" class="form-control" id="admin-first-name" name="first_name">
                </div>

                <!-- School E-mail  Form Input -->
                <div class="form-group">
                    {!! Form::label('email', 'E-mail:', ['class'=>'control-label']) !!}
                    {!! Form::text('email',null, ['class'=>'form-control', 'id'=> 'email']) !!}
                </div>

                <!-- School Type  Form Input -->
                <div class="form-group">
                    {!! Form::label('type', 'Τύπος:', ['class'=>'control-label']) !!}
                    {!! Form::select('type',[
                            'Λύκειο' => 'Λύκειο',
                            'Γυμνάσιο' => 'Γυμνάσιο',
                            'ΕΠΑΛ' => 'ΕΠΑΛ',
                            'Ειδικό' => 'Ειδικό'
                        ],null, ['class'=>'form-control', 'id'=> 'type']) !!}
                </div>

                <!-- School Group  Form Input -->
                <div class="form-group">
                    {!! Form::label('group', 'Ομάδα Σχολείων:', ['class'=>'control-label']) !!}
                    {!! Form::select('group',[
                            'Ομάδα 1' => 'Ομάδα 1',
                            'Ομάδα 2' => 'Ομάδα 2',
                            'Ομάδα 3' => 'Ομάδα 3',
                            'Ομάδα 4' => 'Ομάδα 4',
                            'Ομάδα 5' => 'Ομάδα 5',
                            'Ομάδα 6' => 'Ομάδα 6',
                        ],null, ['class'=>'form-control', 'id'=> 'group']) !!}
                </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" id="closeSchoolDetailsModal" class="btn btn-default" data-dismiss="modal">Κλείσιμο</button>
        <button type="button" id="saveSchoolDetailsModal" class="btn btn-primary" data-dismiss="modal">Αποθήκευση</button>
      </div>
    </div>
  </div>
</div>