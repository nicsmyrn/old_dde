<div class="modal fade" id="teacherDescModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
        <strong><h4 class="modal-title" id="exampleModalLabel"></h4></strong>
      </div>
      <div class="modal-body">
        {!! Form::open(['id'=>'formTeacherDetails']) !!}
            {!! Form::hidden('teacherID',$myschool->afm, ['id'=>'teacherID']) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            Επώνυμο:
                            <input type="text" id="modal-last-name" name="last_name" value="{!! $myschool->last_name !!}">
                        </div>
                        <div class="form-group">
                            Όνομα:
                            <input type="text" id="modal-first-name" name="first_name" value="{!! $myschool->first_name !!}">
                        </div>

                        <div class="form-group">
                            Υποχρεωτικό Ωράριο:
                            <input maxlength="2" size="2"  type="text" name="orario" id="modal-hours" value="{!! $myschool->ypoxreotiko !!}">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            Πατρώνυμο:
                            <input type="text" name="middle-name" id="modal-middle-name" value="{!! $myschool->middle_name !!}">
                        </div>
                        <div class="form-group">
                            Κλάδος:
                            {!! Form::select('klados', $kladoi, $myschool->eidikotita, ['id'=>'modal-klados-name']) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            Αριθμός Μητρώου:
                            <input maxlength="6" size="6"  type="text" name="am" id="modal-am" value="{!! $myschool->am !!}">
                        </div>
                        <div class="form-group">
                            Οργανική:
                            {!! Form::select('organiki',$schools,$myschool->organiki_name,['id'=>'modal-school-name']) !!}
                        </div>
                    </div>
                </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" id="closeTeacherDetailsModal" class="btn btn-default" data-dismiss="modal">Κλείσιμο</button>
        <button type="button" id="saveTeacherDetailsModal" class="btn btn-primary" data-dismiss="modal">Αποθήκευση</button>
      </div>
    </div>
  </div>
</div>