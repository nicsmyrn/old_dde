<div class="modal fade" id="teacherDescModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Κλείσιμο"><span aria-hidden="true">&times;</span></button>
        <strong><h4 class="modal-title" id="exampleModalLabel"></h4></strong>
      </div>
      <div class="modal-body">
        {!! Form::open(['id'=>'formTeacherDetails']) !!}
            {!! Form::hidden('teacherID',null, ['id'=>'teacherID']) !!}

                <div class="row">
                    <div class="col-md-4 text-center">
                        <div class="form-group">
                            <label for="moria-metathesis" class="control-label">Μόρια Μετάθεσης (προαιρετικό):</label>

                            <div style="width: 100px; margin: auto">
                                <input type="text" class="form-control" id="moria-metathesis" name="moria">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div class="form-group">
                            <label for="moria-metathesis" class="control-label">Μόρια Απόσπασης (προαιρετικό):</label>

                            <div style="width: 100px; margin: auto">
                                <input type="text" class="form-control" id="moria-apospasis" name="moria_apospasis">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 text-center">
                        <div>
                             <label class="control-label">Προϋπηρεσία:</label>
                        </div>
                        <div class="form-inline">
                            <label for="ex_years" class="control-label">Χρόνια:</label>
                            <input type="text"  id="ex_years" name="ex_years" style="width: 25px">
                            <label for="ex_months" class="control-label" >Μήνες:</label>
                            <input type="text" id="ex_months" name="ex_months" style="width: 25px">
                            <label for="ex_months" class="control-label">Ημέρες:</label>
                            <input type="text" id="ex_days" name="ex_days" style="width: 25px">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            Επώνυμο:
                            <input type="text" id="modal-last-name" name="last_name">
                        </div>
                        <div class="form-group">
                            Όνομα:
                            <input type="text" id="modal-first-name" name="first_name">
                        </div>
                        <div class="form-group">
                            Σχέση:
                            <strong id="modal-relation"></strong>
                        </div>
                        <div class="form-group">
                            Υποχρεωτικό Ωράριο:
                            <input maxlength="2" size="2"  type="text" name="orario" id="modal-hours">
                        </div>
                        <div class="form-group">
                            Διεύθυνση:
                            <strong id="modal-address"></strong>
                        </div>
                        <div class="form-group">
                            Δήμος Εντοπιότητας:
                            {!! Form::select('dimos_entopiotitas',Config::get('requests.dimos'),0,['id'=>'modal-municipality-home']) !!}
                        </div>
                        <div class="form-group">
                            Σταθερό:
                            <strong id="modal-phone"></strong>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            Πατρώνυμο:
                            <input type="text" name="middle-name" id="modal-middle-name">
                        </div>
                        <div class="form-group">
                            Κλάδος:
                            <strong id="modal-category"></strong>
                        </div>
                        <div class="form-group">
                            Οικογενειακή Κατάσταση:
                            {!! Form::select('family_situation',Config::get('requests.family_situation'),0,['id'=>'modal-family-situation']) !!}
                            <strong id="modal-family-situation"></strong>
                        </div>
                        <div class="form-group">
                            Πόλη:
                            <strong id="modal-city"></strong>
                        </div>
                        <div class="form-group">
                            Δήμος Συνυπηρέτησης:
                            {!! Form::select('dimos_sinipiretisis',Config::get('requests.dimos'),0,['id'=>'modal-municipality-wife']) !!}
                        </div>
                        <div class="form-group">
                            Κινητό:
                            <strong id="modal-mobile"></strong>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            Αριθμός Μητρώου:
                            <input maxlength="6" size="6"  type="text" name="am" id="modal-am">
                        </div>
                        <div class="form-group">
                            Οργανική:
                            {!! Form::select('organiki',$schools,0,['id'=>'modal-school-name']) !!}
                        </div>
                        <div class="form-group">
                            Αρ. Παιδιών:
                            <input maxlength="2" size="2" type="text" name="children" id="modal-children">
                        </div>
                        <div class="form-group">
                            Ειδική Κατηγορία:
                            {!! Form::select('special_situation',['ΟΧΙ','ΝΑΙ'],0,['id'=>'modal-special-category']) !!}
                        </div>
                        <div class="form-group">
                            Φύλο:
                            {!! Form::select('sex',['ΘΗΛΥ','ΑΡΡΕΝ'],0,['id'=>'modal-sex']) !!}
                        </div>
                    </div>

                </div>

        {!! Form::close() !!}
      </div>
      <div class="modal-footer">
        <button type="button" id="closeTeacherDetailsModal" class="btn btn-default" data-dismiss="modal">Κλείσιμο</button>
        <button type="button" id="saveTeacherDetailsModal" class="btn btn-primary" data-dismiss="modal">Αποθήκευση</button>
      </div>
    </div>
  </div>
</div>