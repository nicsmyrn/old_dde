@extends('app')

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">

@endsection

@section('content')
    <div class="container">
        <h1 class="page-heading">Διαχείριση Σχολικών Μονάδων</h1>

        <div class="row">
                <table id="tableSchools" class="table table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr class="active">
                            <th>Όνομα Σχολικής Μονάδος</th>
                            <th class="text-center">Τύπος</th>
                            <th class="text-center">E-mail</th>
                            <th class="text-center">Ομάδα Σχολείων</th>
                            <th class="text-center">Ονοματεπώνυμο Διευθυντή</th>
                            <th class="text-center">Τελευταία Σύνδεση</th>
                            <th class="text-center">Ενέργειες</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schools as $school)
                            <tr data-school-id="{!! $school->id !!}">
                                <td class="school-name">{!! $school->name !!}</td>
                                <td class="school-type">{!! $school->type !!}</td>
                                <td class="school-email">{!! $school->user->email !!}</td>
                                <td class="school-group">{!! $school->group !!}</td>
                                <td class="school-admin-full-name">{!! $school->user->full_name !!}</td>
                                <td>{!! $school->user->login_at !!}</td>
                                <td>
                                    <button type="button" data-toggle="modal" data-target="#schoolDescModal" data-id="{!! $school->id !!}" class="btn btn-warning btn-xs" title="Επεξεργασία Σχολικής Μονάδας">
                                        <i class="glyphicon glyphicon-edit"></i> Επεξεργασία
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
        </div>
    </div>

    @include('pysde.user.schoolDescriptionModal')
@endsection

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script src="/js/global.js"></script>

    <script>
        $(document).ready(function() {
            $('#schoolDescModal').on('show.bs.modal', showBsModal);
            $('#closeSchoolDetailsModal').on('click', buttonCloseModalDetails);
            $('#saveSchoolDetailsModal').on('click', buttonSaveModalDetails);

            $('#tableSchools').DataTable({
                "order": [[ 5, "desc" ]],
                "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 6 ] } ],
                "language": {
                            "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                            "emptyTable": "Δεν υπάρχουν δεδομένα στον πίνακα",
                            "loadingRecords" : "Φόρτωση δεδομένων...",
                            "processing":     "Επεξεργασία",
                            "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                            "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                            "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                            "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                            "search": "Αναζήτηση:",
                            "paginate": {
                                  "previous": "Προηγούμενη",
                                  "next" : "Επόμενη"
                            }
                }
            });
        }); // end of ready function

        function showBsModal(event){
            var button = $(event.relatedTarget);
            var modal = $(this);
            modal.data('dataModalId',button.data('id'));
            $.ajax({
                type : 'get',
                data : {id : button.data('id')},
                url : "{!! route('ajaxSchoolModalData')!!}",
                success : function(data){
                    successShowBsModal(modal, data)
                },
                 error: function(data){
                     $.swalAlertValidatorError(data)
                 }
            });
        } //end of function showBsModal

        function successShowBsModal(modal, data){
            modal.find('#schoolID').val(data.id);
            modal.find('.modal-title').text(data.name);
            modal.find('#admin-last-name').val(data.user.last_name);
            modal.find('#admin-first-name').val(data.user.first_name);
            modal.find('#type').val(data.type);
            modal.find('#email').val(data.user.email);
            modal.find('#group').val(data.group);
        }

        function successSaveModalSchoolDetails(modal, data){
            var row = $("tr[data-school-id="+modal.data('dataModalId')+"]");
            if (data.return == 'query_success'){
                row.find(".school-name").text(data.school.name);
                row.find(".school-type").text(data.school.type);
                row.find(".school-email").text(data.school.email);
                row.find(".school-group").text(data.school.group);
                row.find(".school-admin-full-name").text(data.school.full_name);
                $.swalAlert({
                    title: 'Συγχαρητήρια!',
                    body: 'Αποθηκεύτηκαν με επιτυχία οι αλλαγές των στοιχείων της Σχολικής Μονάδας',
                    level: 'success'
                });
            }else{
                $.swalAlert({
                    title: 'Προσοχή!!',
                    body: 'Δεν αποθηκεύτηκαν οι αλλαγές...',
                    level: 'danger'
                });
            }
        }

        function buttonSaveModalDetails(){
            var form = $('#formSchoolDetails');
            var modal = $('#schoolDescModal');
            $.ajax({
                url : "{!! route('ajaxSchoolModalDataSave') !!}",
                type : 'post',
                data : form.serialize(),
                success : function(data){
                    successSaveModalSchoolDetails(modal, data)
                },
                error: function(data){
                    $.swalAlertValidatorError(data)
                }
            });
        }

        function buttonCloseModalDetails(){
            $.swalAlert({
                title : 'Ειδοποίηση!',
                body : 'Kαμία αλλαγή στα στοιχεία του σχολείου',
                level : 'warning'
            });
        }

    </script>
@endsection