@extends('app')

@section('title')
    Διαχείριση Λογαριασμών - ΕΚΠΑΙΔΕΥΤΙΚΩΝ
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <style>
            /* CSS used here will be applied after bootstrap.css */
            .modal-header-success {
                color:#fff;
                padding:9px 15px;
                border-bottom:1px solid #eee;
                background-color: #5cb85c;
                -webkit-border-top-left-radius: 5px;
                -webkit-border-top-right-radius: 5px;
                -moz-border-radius-topleft: 5px;
                -moz-border-radius-topright: 5px;
                 border-top-left-radius: 5px;
                 border-top-right-radius: 5px;
            }
            .modal-header-warning {
                color:#fff;
                padding:9px 15px;
                border-bottom:1px solid #eee;
                background-color: #f0ad4e;
                -webkit-border-top-left-radius: 5px;
                -webkit-border-top-right-radius: 5px;
                -moz-border-radius-topleft: 5px;
                -moz-border-radius-topright: 5px;
                 border-top-left-radius: 5px;
                 border-top-right-radius: 5px;
            }
            .modal-header-danger {
                color:#fff;
                padding:9px 15px;
                border-bottom:1px solid #eee;
                background-color: #d9534f;
                -webkit-border-top-left-radius: 5px;
                -webkit-border-top-right-radius: 5px;
                -moz-border-radius-topleft: 5px;
                -moz-border-radius-topright: 5px;
                 border-top-left-radius: 5px;
                 border-top-right-radius: 5px;
            }
            .modal-header-info {
                color:#fff;
                padding:9px 15px;
                border-bottom:1px solid #eee;
                background-color: #5bc0de;
                -webkit-border-top-left-radius: 5px;
                -webkit-border-top-right-radius: 5px;
                -moz-border-radius-topleft: 5px;
                -moz-border-radius-topright: 5px;
                 border-top-left-radius: 5px;
                 border-top-right-radius: 5px;
            }
            .modal-header-primary {
                color:#fff;
                padding:9px 15px;
                border-bottom:1px solid #eee;
                background-color: #428bca;
                -webkit-border-top-left-radius: 5px;
                -webkit-border-top-right-radius: 5px;
                -moz-border-radius-topleft: 5px;
                -moz-border-radius-topright: 5px;
                 border-top-left-radius: 5px;
                 border-top-right-radius: 5px;
            }
    </style>

@endsection

@section('content')

    <h1 class="page-heading">Διαχείριση ΛΟΓΑΡΙΑΣΜΩΝ -  ΚΑΘΗΓΗΤΩΝ</h1>

    @if($teachers->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχει κανένας εγγεγραμένος καθηγητής</div>
                </div>
            </div>
        </div>
    @else
        <table id="teachers" class="table table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr class="active">
                    <th class="text-center">Α.Μ.</th>
                    <th class="text-center">Ονοματεπώνυμο</th>
                    <th class="text-center">Ειδικότητα</th>
                    <th class="text-center">Σχέση</th>
                    <th class="text-center">Οργανική</th>
                    <th class="text-center">Μόρια Μετάθεσης</th>
                    <th class="text-center">Ενεργός Χρήστης</th>
                    <th class="text-center">Ενέργειες</th>
                </tr>
            </thead>

            <tbody>
                @foreach($teachers as $teacher)
                    <tr data-teacher-id="{!! $teacher->id !!}">
                        <td>{!! $teacher->teacherable->am !!}</td>
                        <td>{!! $teacher['user']['full_name'] !!}</td>
                        <td>{!! $teacher->klados_name !!}</td>
                        <td>{!! $teacher->teacherable_type == 'App\Monimos' ? 'Μόνιμος': 'Αναπληρωτής' !!}</td>
                        <td>{!! $teacher->teacherable->organiki_name  !!}</td>
                        <td class="moria-metathesis text-center">{!! $teacher->teacherable->moria !!}</td>
                        <td class="text-center">
                            <span style="color: #008000">
                                <i class="glyphicon glyphicon-ok-circle"></i>
                            </span>
                        </td>
                        <td>
                            <button title="Ενεργοποίηση χρήστη" class="btn btn-default btn-xs">
                                    <i class="glyphicon glyphicon-thumbs-up"></i>
                            </button>

                            <button type="button" data-toggle="modal" data-target="#teacherDescModal" data-id="{!! $teacher->id !!}"  class="btn btn-warning btn-xs" title="Επεξεργασία Καθηγητή">
                                <i class="glyphicon glyphicon-edit"></i>
                                Λεπτομέρειες
                            </button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        @include('pysde.user.teacherDescriptionModal', $schools)

    @endif

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script src="/js/global.js"></script>

    <script>
        $('#teacherDescModal').on('show.bs.modal', showBsModal);
        $('#closeTeacherDetailsModal').on('click', buttonCloseModalDetails);
        $('#saveTeacherDetailsModal').on('click', buttonSaveModalDetails);

        function showBsModal(event){
            var button = $(event.relatedTarget);
            var modal = $(this);
            modal.data('dataModalId',button.data('id'));

            $.ajax({
                type : 'get',
                data : {id : button.data('id')},
                url : "{!! route('ajaxTeacherModalData')!!}",
                success : function(data){
                    successShowBsModal(modal, data)
                },
                 error: function(data){
                     $.swalAlertValidatorError(data)
                 }
            });
        } //end of function showBsModal

        function successShowBsModal(modal, data){
            modal.find('#teacherID').val(data.id);
            modal.find('.modal-title').text(data.user.last_name + ' ' + data.user.first_name);
            modal.find('#moria-metathesis').val(data.teacherable.moria);
            modal.find('#ex_years').val(data.ex_years);
            modal.find('#ex_months').val(data.ex_months);
            modal.find('#ex_days').val(data.ex_days);
            modal.find('#moria-apospasis').val(data.teacherable.moria_apospasis);
            modal.find('#modal-first-name').val(data.user.first_name);
            modal.find('#modal-last-name').val(data.user.last_name);
            modal.find('#modal-middle-name').val(data.middle_name);
            modal.find('#modal-am').val(data.teacherable.am);
            modal.find('#modal-relation').text(data.relation);
            modal.find('#modal-category').text(data.klados);
            modal.find('#modal-school-name').val(data.teacherable.organiki);
            modal.find('#modal-hours').val(data.teacherable.orario);
            modal.find('#modal-family-situation').val(data.family_s);
            modal.find('#modal-special-category').val(data.special_s);
            modal.find('#modal-address').text(data.address);
            modal.find('#modal-city').text(data.city);
            modal.find('#modal-municipality-home').val(data.dimos_e);
            modal.find('#modal-municipality-wife').val(data.dimos_s);
            modal.find('#modal-phone').text(data.phone);
            modal.find('#modal-mobile').text(data.mobile);
            modal.find('#modal-children').val(data.childs);
            modal.find('#modal-sex').val(data.user.sex);

        }

        function successSaveModalSchoolDetails(modal, data){
            var row = $("tr[data-teacher-id="+modal.data('dataModalId')+"]");
            if (data.return == 'query_success'){
                row.find(".moria-metathesis").text(data.moria);
                $.swalAlert({
                    title: 'Συγχαρητήρια!',
                    body: 'Αποθηκεύτηκαν με επιτυχία τα στοιχεία του καθηγητή',
                    level: 'success'
                });
            }else{
                $.swalAlert({
                    title: 'Προσοχή!!',
                    body: 'Δεν αποθηκεύτηκαν οι αλλαγές...',
                    level: 'danger'
                });
            }
        }

        function buttonSaveModalDetails(){
            var form = $('#formTeacherDetails');
            var modal = $('#teacherDescModal');
            $.ajax({
                url : "{!! route('ajaxTeacherModalDataSave') !!}",
                type : 'post',
                data : form.serialize(),
                success : function(data){
                    successSaveModalSchoolDetails(modal, data)
                },
                error: function(data){
                    $.swalAlertValidatorError(data)
                }
            });
        }

        function buttonCloseModalDetails(){
            $.swalAlert({
                title : 'Ειδοποίηση!',
                body : 'Kαμία αλλαγή στα στοιχεία του καθηγητή',
                level : 'warning'
            });
        }

        $(document).ready(function() {
            var table = $('#teachers').DataTable({
                    "order": [[ 1, "asc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 5,6 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


