@extends('app')


@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Νέος χρήστης</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> Υπάρχει κάποιο πρόβλημα με τα στοιχεία.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					{!! Form::open(['action'=>'UserController@store', 'method'=>'POST', 'class'=>'form-horizontal']) !!}

						<div class="form-group">
							<label class="col-md-4 control-label">Επώνυμο</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Όνομα</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail:</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Κωδικός</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Επιβεβαιωση Κωδικού</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						{{--<div class="form-group">--}}
							{{--<label class="col-md-4 control-label">Είδος Χρήστη</label>--}}
							{{--<div class="col-md-6">--}}
								{{--{!! Form::select('role_id', $role_list) !!}--}}
							{{--</div>--}}
						{{--</div>		--}}
						
						{{--<div class="form-group">--}}
							{{--<label class="col-md-4 control-label">Σχολείο:</label>--}}
							{{--<div class="col-md-6">--}}
								{{--{!! Form::select('sch_id', $school_list) !!}--}}
							{{--</div>--}}
						{{--</div>	--}}
						

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Δημιουργία
								</button>
							</div>
						</div>
					{!! Form::close() !!}	
					<!--</form>-->
				</div>
			</div>
		</div>
	</div>
</div>
@endsection