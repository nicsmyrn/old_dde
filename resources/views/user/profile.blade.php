@extends('app')

@section('header.style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.9.3/css/bootstrap-select.min.css">
    @include('user._style')
@endsection

@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab-container">
            @include('user._menu', ['active' => 'profile'])
            <div id="profile_vue" class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <div class="bhoechie-tab-content active">
                    <center>
                    <h2 style="margin-top: 0;color:#55518a">Προφίλ Χρήστη</h2>

                    @include('errors.list')

                    @if(isset($real_teacher))
                        @if($real_teacher->exists)
                            @if($real_teacher->is_checked)
                                @include('user.checkedTeacherProfile', ['teacher' => $real_teacher])
                            @else
                                @if($real_teacher->profile_counter > 0)
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Τα στοιχεία ελέγχονται από τη  Δ.Δ.Ε. Χανίων </strong>
                                    </div>
                                    @if($real_teacher->profile_counter >= 3)
                                        <div class="alert alert-warning" role="alert">
                                            <strong> Έχετε στείλει πολλές φορές νέα στοιχεία στην Υπηρεσία. Μέχρι να γίνει ο έλεγχος τους, δεν δίνεται η δυνατότητα επιπλέον αποστολής στοιχείων </strong>
                                        </div>
                                    @endif
                                @else
                                    <div class="alert alert-success" role="alert">
                                        <strong>Τα στοιχεία ξεκλειδώθηκαν και μπορείτε να τα επεξεργαστείτε. </strong>
                                    </div>
                                @endif


                                @include('user.teacherProfile')
                            @endif
                        @else
                            @include('user.teacherProfile')
                        @endif
                    @else
                        @include('user.schoolProfile')
                    @endif

                    <br>
                    </center>
                </div>
            </div>
        </div>
  </div>
</div>

<!-- Modal Request change profile -->
<div class="modal fade" id="changeProfile" tabindex="-1" role="dialog" aria-labelledby="changeProfileLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Αλλαγή στοιχείων εκπαιδευτικού</h4>
            </div>
            <div class="modal-body">
                <p>
                    Σε περίπτωση που διαφωνείτε - με τα ελεγμένα από την υπηρεσία -  προσωπικά σας στοιχεία, μπορείτε να αιτηθείτε την αλλαγή τους.
                    Για να σας επιτραπεί αλλαγή των στοιχείων θα πρέπει το αρμόδιο τμήμα της Διεύθυνσης να σας δώσει το δικαίωμα (ξεκλείδωμα) στην
                    υπάρχουσα φόρμα.
                </p>
                <p>
                    Οποιαδήποτε αλλαγή στα προσωπικά σας στοιχεία  θα πρέπει εκ νέου να εγκριθεί από αρμόδιο τμήμα της Διεύθυνσης.
                </p>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Άκυρο</button>
            <button id="changeProfileRequestButton" type="button" class="btn btn-primary" data-dismiss="modal">Αποστολή αιτήματος αλλαγής στοιχείων</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts.footer')
    @include('loader')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/js/bootstrap-select.min.js"></script>

    @include('user._vue_profile')

    <script>
        $(document).ready(function() {

            $('[data-toggle="tooltip"]').tooltip();

            $('#undoRequestToChangeProfile').on('click', function(e){
                e.preventDefault();
                $('#loader').removeClass('invisible')
                    .addClass('loading');
                location.href= "{!!  action('ProfileController@undoRequestToChange') !!}";

            });

            $('#sent1').on('click', function(e){
                $('#loader').removeClass('invisible')
                    .addClass('loading');
            });

            $('#sent2').on('click', function(e){
                $('#loader').removeClass('invisible')
                    .addClass('loading');
            });

        @if(Auth::user()->userable_type == 'App\Teacher')

            @if(isset(Auth::user()->userable))
                @if(in_array($teacher->teacherable_type,['App\Anaplirotis','App\FakeAnaplirotis']))
                    @if(Request::old('teacher_type'))
                        anaplirotis = $('#anaplirotis').detach();
                    @else
                        monimos = $('#monimos').detach();
                    @endif
                @elseif(in_array($teacher->teacherable_type,['App\Monimos','App\FakeMonimos']))
                    @if(Request::old('teacher_type'))
                        monimos = $('#monimos').detach();
                    @else
                        anaplirotis = $('#anaplirotis').detach();
                    @endif
                @endif

                //####################  Modal ##########################
                    $('#changeProfileRequestButton').on('click', buttonSaveRequestChangeProfile);
                //####################  Modal ##########################
            @else
                anaplirotis = $('#anaplirotis').detach();
            @endif

        @endif

            $('#select_for_attach').on('change', function(){
                if($(this).val() == 0){
                    anaplirotis = $('#anaplirotis').detach();
                    if (monimos){
                        monimos.appendTo('#attach');
                        monimos = null;
                    }
                }else if($(this).val() == 1){
                    monimos = $('#monimos').detach();
                    if(anaplirotis){
                        anaplirotis.appendTo('#attach');
                        anaplirotis = null;
                    }
                }
            });

            $('#organiki').on('change', function(){
                if($(this).val() == 2000){
                    $('#county').show();
                }else{
                    $('#select_county').val({!! \Config::get('requests.default_county') !!});
                    $('#county').hide();
                }
            });

            $('#show_logoi_ygeias').on('change', function(){
                if($(this).val() == '1'){
                    $('#content_logoi_ygeias').show();
                }else{
                    $('#content_logoi_ygeias').hide();
                }
            });

            $('#show_proipiresia').on('change', function(){
                if($(this).val() == '1'){
                    $('#content_proipiresia').show();
                }else{
                    $('#content_proipiresia').hide();
                }
            });

        });

        function bs_input_file() {
            $(".input-file").before(
                function() {
                    if ( ! $(this).prev().hasClass('input-ghost') ) {
                        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                        element.attr("name",$(this).attr("name"));
                        element.change(function(){
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function(){
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function(){
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor","pointer");
                        $(this).find('input').mousedown(function() {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
        $(function() {
            bs_input_file();
        });

        function buttonSaveRequestChangeProfile(){
            $('#loader').removeClass('invisible')
                .addClass('loading');
            $.ajax({
                url : "{!! route('ajaxTeacherModalChangeProfileRequest') !!}",
                type : 'get',
                success : function(data){
                    successSaveModalChangeProfile( data)
                },
                error: function(data){
                    $.swalAlertValidatorError(data)
                }
            });
        }

        function successSaveModalChangeProfile(data){
            if (data.return == 'query_success'){
                location.reload(true);
            }else{
                console.log('Error 234');
            }
        }
    </script>
@endsection
