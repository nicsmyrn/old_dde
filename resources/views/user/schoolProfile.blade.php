    @if(Auth::user()->isRole('school'))

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title"><b>{!! $school->name !!}</b></h3>
            </div>
            <div class="panel-body">
                <div class="col-md-5">
                    <p>
                        <h5 class="text-center">
                            <b>{!! Auth::user()->full_name !!}</b>
                        </h5>
                        @if($school->user->sex == 0)
                            <img src="{!! asset('img/female2.png') !!}" class="img-circle" alt="ΘΗΛΥ" title="ΘΗΛΥ" width="60px" height="60px">
                            <p><span>Θήλυ</span></p>
                        @else
                            <img src="{!! asset('img/male1.png') !!}" class="img-circle" alt="ΑΡΡΕΝ" title="ΑΡΡΕΝ" width="60px" height="60px">
                            <p><span>Άρρεν</span></p>
                        @endif
                    <p>
                    <h3><span class="label label-default">{!! $school->name !!}</span></h3>
                    <hr>
                    <h5 class="text-left">
                        <span>E-mail:</span>
                        <b>{!! Auth::user()->email !!}</b>
                    </h5>
                    <h5 class="text-left">
                        <span>Ομάδα:</span>
                        <b>{!! $school->group !!}</b>
                    </h5>
                </div>
                <div class="col-md-7">
                    {!! Form::model($school,['method'=>'POST', 'action'=>'ProfileController@postSchoolProfile']) !!}
                    <table class="table table-user-information">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('work_phone', 'Τηλέφωνο Σχολείου:', ['class'=>'control-label']) !!}
                                        {!! Form::text('work_phone',null, ['class'=>'form-control']) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('mobile_phone', 'Τηλέφωνο επικοινωνίας:', ['class'=>'control-label']) !!}
                                        {!! Form::text('mobile_phone',null, ['class'=>'form-control']) !!}
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('sex', 'Φύλο Διευθυντή:', ['class'=>'control-label']) !!}
                                        <select name="sex" class="form-control">
                                            <option>Επέλεξε Φύλο</option>
                                            @foreach(['Θήλυ', 'Άρρεν'] as $k=>$v)
                                                <option @if($school->user->sex == $k) selected @endif value="{!! $k !!}">{!! $v !!}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </td>
                                <td>
                                    <div class="form-group">
                                        {!! Form::label('area', 'Περιοχή Σχολείου:', ['class'=>'control-label']) !!}
                                        <select name="area" class="form-control">
                                            <option>Επέλεξε Περιοχή</option>
                                            @foreach(Config::get('requests.areas') as $k=>$v)
                                                <option @if($school->area == $k) selected @endif value="{!! $k !!}">{!! $v !!}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                    {!! Form::submit('Αποθήκευση', ['class'=>'btn btn-success']) !!}
                {!! Form::close() !!}
                </div>
            </div>
        </div>

    @endif
