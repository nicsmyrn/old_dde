    <!-- ####################  EDIT PROFILE FORM #######################   -->
        {!! Form::model($teacher,[
            'method'=>'POST', 'action'=>'ProfileController@postProfile', 'files' => true]) !!}
                {!! Form::submit('ΑΠΟΣΤΟΛΗ νέων στοιχείων', ['class'=>'btn btn-primary', 'id' => 'sent1']) !!}
    <hr>
        <div class="col-md-12">
            @include('misthodosia.attachments', ['title' => 'Απαραίτητα Δικαιολογητικά για την αλλαγή των στοιχείων σας.'])
        </div>
    <hr>

        <div class="row">
            <div class="col-md-6">
                <!-- Middle_name Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('middle_name', 'Πατρώνυμο:', ['class'=>'control-label']) !!}
                    {!! Form::text('middle_name',null, ['class'=>'form-control']) !!}
                </div>
                <div style="font-weight: bold">
                    (<span style="color: red">*</span>) <span style="text-decoration: underline">υποχρεωτικά στοιχεία</span>
                </div>
            </div>
            <div class="col-md-6">
                <!-- Klados_id Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('klados_id', 'Κλάδος:', ['class'=>'control-label']) !!}
                        <b style="color: #00008b; font-size: 15pt">{!! $new_klados !!}</b>
{{--                    {!! Form::select('klados_id', $kladoi, null, ['class' => 'form-control', 'name' => 'klados_id'] ) !!}--}}
                </div>

                <!-- Years_experience Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('sex', 'Φύλο:', ['class'=>'control-label']) !!}
                    {!! Form::select('sex',['ΘΗΛΥ','ΑΡΡΕΝ'],isset($teacher->user->sex)?$teacher->user->sex:null, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div> <!-- End of Row -->


            <div class="row">
                <div class="col-md-10">
                <div id="attach" class="panel panel-info">
                    <div class="panel-heading">
                        {!! Form::select('teacher_type',['ΜΟΝΙΜΟΣ', 'ΑΝΑΠΛΗΡΩΤΗΣ'],(isset($teacher)&& in_array($teacher->teacherable_type,['App\Anaplirotis','App\FakeAnaplirotis']))?1:0, ['class'=>'selectpicker','data-style'=>'btn-primary', 'id'=>'select_for_attach']) !!}
                    </div>
                    <div id="anaplirotis" class="panel-body">
                        <div class="col-md-3">
                            <!-- Am Form Input -->
                            <div class="form-group required" id="">
                                {!! Form::label('afm', 'ΑΦΜ:', ['class'=>'control-label']) !!}
                                {!! Form::text('afm',isset($teacher->teacherable->afm)?$teacher->teacherable->afm:null, ['class'=>'form-control','data-validation'=>'alphanumeric']) !!}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Orario Form Input -->
                            <div class="form-group" id="">
                                {!! Form::label('orario', 'Υποχρεωτικό Ωράριο:', ['class'=>'control-label']) !!}
                                {!! Form::text('orario',isset($teacher->teacherable->orario)?$teacher->teacherable->orario:null, ['class'=>'form-control','data-validation'=>'number', 'data-validation-allowing'=>'range[1;24]']) !!}
                                <input type="hidden" name="seira_topothetisis" value="100"/>
{{--                                                {!! Form::label('seira_topothetisis', 'Σειρά τοποθέτησης:', ['class'=>'control-label']) !!}--}}
{{--                                                {!! Form::text('seira_topothetisis',isset($teacher->teacherable->seira_topothetisis)?$teacher->teacherable->seira_topothetisis:null, ['class'=>'form-control','data-validation'=>'number', 'data-validation-allowing'=>'range[1;24]']) !!}--}}
                            </div>
                        </div>
                        <div class="col-md-3">
                            <!-- Organiki Form Input -->
                            <input type="hidden" name="type" value="1"/>

                            {{--<div class="form-group" id="">--}}
                                {{--{!! Form::label('type', 'Διαδικασία πρόσληψης:', ['class'=>'control-label']) !!}--}}
                                {{--<select id="type" name="type" class="form-control">--}}
                                    {{--<option>1</option>--}}
                                    {{--<option>2</option>--}}
                                    {{--<option>3</option>--}}
                                    {{--<option>4</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-md-3">
                            <!-- Moria Form Input -->
                            <div class="form-group" id="">
                                <label class="control-label">
                                    Μόρια:
                                </label>
                                <label>{!! isset($teacher->teacherable->moria)?$teacher->teacherable->moria:'-' !!}</label>
                            </div>
                        </div>
                    </div>
                    <div id="monimos" class="panel-body">
                        <div class="col-md-4">
                            <!-- Am Form Input -->
                            <div class="form-group required" id="">
                                {!! Form::label('am', 'ΑΜ:', ['class'=>'control-label']) !!}
                                {!! Form::text('am',isset($teacher->teacherable->am)?$teacher->teacherable->am:null, ['class'=>'form-control','data-validation'=>'alphanumeric']) !!}
                            </div>
                            {{--<!-- Moria Form Input -->--}}
                            {{--<div class="form-group" id="">--}}
                                {{--{!! Form::label('moria', 'Μόρια Μετάθεσης: '. isset($teacher->teacherable->moria)?$teacher->teacherable->moria:'', ['class'=>'control-label']) !!}--}}
                            {{--</div>--}}
                            {{--<!-- Moria Form Input -->--}}
                            {{--<div class="form-group" id="">--}}
                                {{--{!! Form::label('moria_apospasis', 'Μόρια Απόσπασης: '. isset($teacher->teacherable->moria_apospasis)?$teacher->teacherable->moria_apospasis:'', ['class'=>'control-label']) !!}--}}
                            {{--</div>--}}
                        </div>
                        <div class="col-md-2">
                            <!-- Orario Form Input -->
                            <div class="form-group required" id="">
                                {!! Form::label('orario', 'Υποχρεωτικό Ωράριο:', ['class'=>'control-label']) !!}
                                {!! Form::text('orario',isset($teacher->teacherable->orario)?$teacher->teacherable->orario:null, ['class'=>'form-control','data-validation'=>'number', 'data-validation-allowing'=>'range[1;24]']) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Organiki Form Input -->
                            <div class="form-group required" id="">
                                {!! Form::label('organiki', 'Οργανική:', ['class'=>'control-label']) !!}
                                {!! Form::select('organiki', $schools, isset($teacher->teacherable->organiki)?$teacher->teacherable->organiki:null, ['class'=>'form-control', 'id'=>'organiki']) !!}

                                <div id="county" style="display:{!! (isset($teacher->teacherable->organiki) && $teacher->teacherable->organiki == 2000) ? 'block':'none' !!}">
                                    {!! Form::label('county', 'Δ.Δ.Ε.:', ['class'=>isset($teacher->teacherable->organiki) && $teacher->teacherable->organiki == 2000?'control-label':'control-label']) !!}
                                    {!! Form::select('county',
                                        \Config::get('requests.county'),
                                        [isset($teacher->teacherable->county)?$teacher->teacherable->county:49],
                                        [
                                            'class'=> isset($teacher->teacherable->organiki) && $teacher->teacherable->organiki == 2000
                                                ?'form-control':'form-control',
                                                'id'=>'select_county'
                                        ])
                                    !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div> <!-- End fo Row -->
        {{--@else--}}

        {{--@endif--}}

        <div class="row">
            <div class="col-md-4">
                <!-- Family_situation Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('family_situation', 'Οικογενειακή Κατάσταση:', ['class'=>'control-label']) !!}
                    {!! Form::select('family_situation',\Config::get('requests.family_situation'),[isset($teacher->family_situation)?$teacher->family_situation:0] ,['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <!-- Childs Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('childs', 'Αρ. Παιδιών:', ['class'=>'control-label']) !!}
                    {!! Form::text('childs',null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <!-- Special_situation Form Input -->
                <div class="form-group" id="">
                    {!! Form::label('special_situation', 'Ειδική κατηγορία:', ['class'=>'control-label']) !!}
                    {!! Form::select('special_situation',['ΟΧΙ','ΝΑΙ'],isset($teacher->special_situation)?$teacher->special_situation:null, ['class'=>'form-control']) !!}
                </div>
            </div>
        </div> <!-- End  of Row -->


        <div class="row">
            <div class="col-md-8">
                <div class="panel panel-danger">
                  <div class="panel-heading">
                    <h3 class="panel-title">
                       {!! Form::label('show_logoi_ygeias', 'Υπάρχουν Σοβαροί Λόγοι Υγείας; (για μόρια αποσπάσεων):') !!}
                    </h3>
                  </div>
                  <div class="panel-body">
                       {!! Form::select('show_logoi_ygeias',['ΟΧΙ','ΝΑΙ'],0, ['id' => 'show_logoi_ygeias']) !!}

                       <div id="content_logoi_ygeias" style="display: none;margin-top: 10px">
                            @foreach(Config::get('requests.logoi_ygeias') as $ygeia_type)
                                <div class="form-group">
                                   {!! Form::label($ygeia_type['name'], $ygeia_type['label']) !!}
                                   {!! Form::select($ygeia_type['name'],$ygeia_type['data'],null, ['id' => $ygeia_type['name']]) !!}
                                </div>
                            @endforeach
                       </div>
                  </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-warning">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                        {!! Form::label('show_proipiresia', 'Γνωρίζεις την προϋπηρεσία σου;') !!}
                    </h4>
                  </div>
                  <div class="panel-body">
                        {!! Form::select('show_proipiresia',['ΟΧΙ','ΝΑΙ'],0, ['id' => 'show_proipiresia']) !!}
                        <div id="content_proipiresia" style="display: none;margin-top: 10px">
                            <div class="form-group">
                                {!! Form::label('ex_years', 'Χρόνια Προϋπηρεσίας') !!}
                                {!! Form::text('ex_years',null, ['class'=> 'text-center','id'=> 'xronia_proipiresias', 'size' => 2, 'maxlength'=>2]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ex_months', 'Μήνες Προϋπηρεσίας') !!}
                                {!! Form::text('ex_months',null, ['class'=> 'text-center','id'=> 'mines_proipiresias', 'size' => 2, 'maxlength'=>2]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('ex_days', 'Ημέρες Προϋπηρεσίας') !!}
                                {!! Form::text('ex_days',null, ['class'=> 'text-center','id'=> 'imeres_proipiresias', 'size' => 2, 'maxlength'=>2]) !!}
                            </div>
                            <div class="form-group" style="color: red;font-weight: bold">
                                ισχύει μέχρι ....
                            </div>
                        </div>
                  </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-md-6">
                <!-- Dimos_sinipiretisis Form Input -->
                <div class="form-group" id="">
                    {!! Form::label('dimos_sinipiretisis', 'Δήμος Συνυπηρέτησης:', ['class'=>'control-label']) !!}
                    {!! Form::select('dimos_sinipiretisis',\Config::get('requests.dimos'),isset($teacher->dimos_sinipiretisis)?$teacher->dimos_sinipiretisis:null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-6">
                    <!-- dimos_entopiotitas Form Input -->
                    <div class="form-group" id="">
                        {!! Form::label('dimos_entopiotitas', 'Δήμος Εντοπιότητας:', ['class'=>'control-label']) !!}
                        {!! Form::select('dimos_entopiotitas',\Config::get('requests.dimos'),isset($teacher->dimos_entopiotitas)?$teacher->dimos_entopiotitas:null, ['class'=>'form-control']) !!}
                    </div>
            </div>
        </div> <!-- End  of Row -->

        <div class="row">
            <div class="col-md-4">
                <!-- Address Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('address', 'Διεύθυνση:', ['class'=>'control-label']) !!}
                    {!! Form::text('address',null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group required" id="">
                    {!! Form::label('address_number', 'Αρ. Διεύθυνσης:', ['class'=>'control-label']) !!}
                    {!! Form::text('address_number',null, ['class'=>'form-control']) !!}
                </div>
                <!-- Mobile Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('mobile', 'Κινητό:', ['class'=>'control-label']) !!}
                    {!! Form::text('mobile',null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <!-- City Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('city', 'Πόλη:', ['class'=>'control-label']) !!}
                    {!! Form::text('city',null, ['class'=>'form-control']) !!}
                </div>
                <!-- Phone Form Input -->
                <div class="form-group" id="">
                    {!! Form::label('phone', 'Σταθερό:', ['class'=>'control-label']) !!}
                    {!! Form::text('phone',null, ['class'=>'form-control']) !!}
                </div>
            </div>
            <div class="col-md-4">
                <!-- Tk Form Input -->
                <div class="form-group required" id="">
                    {!! Form::label('tk', 'TK:', ['class'=>'control-label']) !!}
                    {!! Form::text('tk',null, ['class'=>'form-control']) !!}
                </div>

                {!! Form::submit('ΑΠΟΣΤΟΛΗ νέων στοιχείων', ['class'=>'btn btn-primary', 'id' => 'sent2']) !!}

            </div>
        </div> <!-- End of Row -->

        {!! Form::close() !!}

        <!-- ####################  EDIT PROFILE FORM #######################   -->