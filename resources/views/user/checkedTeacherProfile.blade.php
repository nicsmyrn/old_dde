    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--@include('teacher.attachments.upload', ['title' => 'Αν υπάρχει οποιαδήποτε μεταβολή στα στοιχεία σας, μπορείτε να στείλετε ηλεκρονικά τα δικαιολογητικά, για να περαστούν οι αλλαγές.'])--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="row">
        <div class="col-md-8">
            @if(!$teacher->request_to_change)
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                     Σε περίπτωση που επιθυμείτε αλλαγή των ελεγμένων στοιχείων πατήστε
                     <button type="button" class="btn btn-primary btn btn-xs" data-toggle="modal" data-target="#changeProfile">
                       εδώ
                     </button>
                </div>
            @else
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                     <p>
                         <strong>Προσοχή:</strong> Έχει σταλεί αίτημα αλλαγής των στοιχείων σας. Μόλις ενεργοποιηθεί από το αρμόδιο τμήμα
                         θά έχετε τη δυνατότητα επεξεργασίας <b>ΌΛΩΝ</b> των στοιχείων σας.
                     </p>
                     <p>
                         Αν επιθυμείτε να αναιρέστε την τροποποίηση των στοιχείων σας (να παραμείνουν ως έχουν)
                         κάντε κλικ
                         <a id="undoRequestToChangeProfile" href="{!! action('ProfileController@undoRequestToChange') !!}" class="btn btn-primary btn btn-xs">εδώ</a>
                     </p>
                </div>
            @endif
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><b>{!! $teacher->user->full_name !!}</b> του <b>{!! $teacher->middle_name !!}</b></h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <p>
                                @if($teacher->sex == 0)
                                    <img src="{!! asset('img/female2.png') !!}" class="img-circle" alt="ΘΗΛΥ" title="ΘΗΛΥ" width="60px" height="60px">
                                    <p><span>Θήλυ</span></p>
                                @else
                                    <img src="{!! asset('img/male1.png') !!}" class="img-circle" alt="ΑΡΡΕΝ" title="ΑΡΡΕΝ" width="60px" height="60px">
                                    <p><span>Άρρεν</span></p>
                                @endif
                            <p>
                                <i class="fa fa-check-circle fa-5x" style="color: #228B22" aria-hidden="true"></i>
                            </p>
                            <h4>
                                <span class="label label-success">Ελεγχμένα</span>
                            </h4>

                        </div>
                        <div class="col-md-9">
                            <table class="table table-user-information">
                                <tbody>
                                    <tr>
                                        <td>Σχέση Εργασίας:</td>
                                        <td><b>
                                            @if($teacher->teacherable_type == 'App\Monimos')
                                                Μόνιμος
                                            @elseif($teacher->teacherable_type == 'App\Anaplirotis')
                                                Αναπληρωτής
                                            @endif
                                        </b></td>
                                    </tr>
                                    @if($teacher->teacherable_type == 'App\Monimos')
                                        <tr>
                                            <td>ΑΜ:</td>
                                            <td><b>{!! $teacher->teacherable->am!!}</b></td>
                                        </tr>
                                    @endif

                                    <tr>
                                        <td>ΑΦΜ:</td>
                                        <td><b>{!! $teacher->myschool->afm!!}</b></td>
                                    </tr>

                                    <tr>
                                        <td>Παλαιός Κλάδος:</td>
                                        <td><b>{!! $teacher->klados_name !!} ({!! $teacher->eidikotita_name !!})</b></td>
                                    </tr>

                                    <tr>
                                        <td>Νέος Κλάδος:</td>
                                        <td><b style="color: #00008b">{!! $teacher->myschool->new_klados !!} ({!! $teacher->myschool->new_eidikotita_name !!})</b></td>
                                    </tr>


                                     <tr>
                                        @if(str_contains($teacher->myschool->topothetisi, 'Οργανικά'))
                                            <td>Οργανική</td>
                                        @else
                                            <td>
                                                Προσωρινή Τοποθέτηση
                                                @if(!str_contains($teacher->myschool->topothetisi, 'Οργανικά'))
                                                    <label class="label label-danger">{!! $teacher->myschool->topothetisi !!}</label>
                                                @endif
                                            </td>
                                        @endif
                                        <td><b>{!! $teacher->myschool->organiki_name!!}</b></td>
                                    </tr>

                                    <tr>
                                        <td>Υποχρεωτικό Ωράριο:</td>
                                        <td><b>{!! $teacher->teacherable->orario!!}</b></td>
                                    </tr>

                                    @if(Config::get('requests.show_moria'))


                                        @if($teacher->teacherable->moria > 0)
                                            <tr>
                                                <td>Μόρια Βελτιώσης</td>
                                                <td><b style="color: red; font-size: large">{!! $teacher->teacherable->moria !!}</b></td>
                                            </tr>
                                        @endif
                                        @if($teacher->teacherable->moria_apospasis > 0)
                                            <tr>
                                                <td>Μόρια Απόσπασης</td>
                                                <td><b style="color: red; font-size: large">{!! $teacher->teacherable->moria_apospasis !!}</b></td>
                                            </tr>
                                        @endif

                                    @endif
                                    <tr>
                                        <td>Προϋπηρεσία:</td>
                                        <td><b>
                                            {!! $teacher->ex_years.' χρόνια, '.$teacher->ex_months.' μήνες και '.$teacher->ex_days.' ημέρες' !!}
                                        </b></td>
                                    </tr>
                                    <tr>
                                        <td>Οικογενειακή κατάσταση:</td>
                                        <td><b>
                                            {!! \Config::get('requests.family_situation')[$teacher->family_situation] !!}
                                        </b></td>
                                    </tr>
                                    <tr>
                                        <td>Αριθμός παιδιών:</td>
                                        <td>
                                            <div class="alert alert-warning" role="alert">
                                                <strong> {!! $teacher->childs !!} </strong>
                                                τέκνα μοριοδοτούνται για τις αιτήσεις προς το ΠΥΣΔΕ
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Δήμος εντοπιότητας:</td>
                                        <td><b>
                                            {!! \Config::get('requests.dimos')[$teacher->dimos_entopiotitas] !!}
                                        </b></td>
                                    </tr>
                                    <tr>
                                        <td>Δήμος συνυπηρέτησης</td>
                                        <td><b>
                                            {!! \Config::get('requests.dimos')[$teacher->dimos_sinipiretisis] !!}
                                        </b></td>
                                    </tr>
                                    <tr>
                                        <td>Ειδική κατηγορία</td>
                                        <td><b>
                                            @if($teacher->special_situation)
                                                ΝΑΙ
                                            @else
                                                ΟΧΙ
                                            @endif
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td>E-mail:</td>
                                        <td><b>
                                            {!! $teacher->user->email !!}
                                        </b></td>
                                    </tr>

                                    <tr>
                                        <td>Δεύτερο E-mail:</td>
                                        <td><b>
                                            {!! $teacher->user->second_mail !!}
                                        </b></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h3>
                <span class="label label-default">Πρόσθετα Στοιχεία</span>
            </h3>

            {!! Form::model($teacher,['method'=>'POST', 'action'=>'ProfileController@postSmallProfile']) !!}
                    <div class="form-group required" id="">
                        {!! Form::label('address', 'Διεύθυνση:', ['class'=>'control-label']) !!}
                        {!! Form::text('address',null, ['class'=>'form-control']) !!}
                    </div>
                    <div class="form-group required" id="">
                        {!! Form::label('address_number', 'Αριθμός Διεύθ.:', ['class'=>'control-label']) !!}
                        {!! Form::text('address_number',null, ['class'=>'form-control', 'size'=> 4, 'maxlength'=> 4]) !!}
                    </div>
                    <div class="form-group required" id="">
                        {!! Form::label('mobile', 'Κινητό:', ['class'=>'control-label']) !!}
                        {!! Form::text('mobile',null, ['class'=>'form-control']) !!}
                    </div>
                    <!-- City Form Input -->
                    <div class="form-group required" id="">
                        {!! Form::label('city', 'Πόλη:', ['class'=>'control-label']) !!}
                        {!! Form::text('city',null, ['class'=>'form-control']) !!}
                    </div>
                    <!-- Phone Form Input -->
                    <div class="form-group" id="">
                        {!! Form::label('phone', 'Σταθερό:', ['class'=>'control-label']) !!}
                        {!! Form::text('phone',null, ['class'=>'form-control']) !!}
                    </div>
                    <!-- Tk Form Input -->
                    <div class="form-group required" id="">
                        {!! Form::label('tk', 'TK:', ['class'=>'control-label']) !!}
                        {!! Form::text('tk',null, ['class'=>'form-control']) !!}
                    </div>
                    {!! Form::submit('Αποθήκευση', ['class'=>'btn btn-success']) !!}
            {!! Form::close() !!}
        </div>
    </div>