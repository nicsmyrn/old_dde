<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
  <div class="list-group">
    <a href="{!! route('User::getInfo') !!}" class="list-group-item text-center {!! $active == 'name' ? 'active' : '' !!}">
      <i class="fa fa-male fa-2x"></i><br/>Στοιχεία Λογαριασμού
    </a>
    <a href="{!! route('User::getChangePwd') !!}" class="list-group-item text-center {!! $active == 'password' ? 'active' : '' !!}">
      <i class="fa fa-key fa-2x"></i><br/>Αλλαγή Κωδικού
    </a>
    <a href="{!! route('User::getChangeEmail') !!}" class="list-group-item text-center {!! $active == 'email' ? 'active' : '' !!}">
      <i class="fa fa-paper-plane fa-2x"></i><br/>Αλλαγή E-mail
    </a>
    <a href="{!! route('User::getProfile') !!}" class="list-group-item text-center {!! $active == 'profile' ? 'active' : '' !!}">
      <h4 class="glyphicon glyphicon-home"></h4><br/>Προφίλ
    </a>

    @if(Auth::user()->isRole('teacher'))
        <a href="{!! route('User::getMisthodosia') !!}" class="list-group-item text-center {!! $active == 'misthodosia' ? 'active' : '' !!}">
            <h4 class="glyphicon glyphicon-home"></h4><br/>Μισθολογικά
        </a>
    @endif

  </div>
</div>