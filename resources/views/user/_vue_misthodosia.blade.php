    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.js"></script>
    <script>

        new Vue ({
            el: '#misthodosia_vue',

            data: {
                attachmentsCheckBox : false,
                tekna : {!! $tekna !!}
            },

            methods : {
                deleteTekno:function(tekno){
                    this.tekna.$remove(tekno);
                },

                addTekno : function(){
                    this.tekna.push({
                        "birth" : null,
                        "college_end_date" : null,
                        "college_start_date" : null,
                        "description"   : null,
                        "misthodosia_id" : {!! $misthodosia_id !!}
                    })
                }
            }
        })
    </script>