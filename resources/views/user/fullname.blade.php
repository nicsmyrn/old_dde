
    {!! Form::model($user, ['method'=>'post', 'class'=>'form']) !!}
        <div class="form-group">
            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon1"><img src="{!! asset('img/last_name.png') !!}" width="20em"/> </span>
                {!! Form::text('last_name', null, ['class'=>'form-control', 'placeholder'=>'Επώνυμο...','aria-describeby'=>'sizing-addon1']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="input-group input-group-lg">
                <span class="input-group-addon" id="sizing-addon2"><img src="{!! asset('img/first_name.png') !!}" width="20em"/></span>
                {!! Form::text('first_name', null, ['class'=>'form-control', 'placeholder'=>'Όνομα...','aria-describeby'=>'sizing-addon2']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Αποθήκευση', ['class'=>'btn btn-primary btn-lg']) !!}
        </div>

    {!! Form::close() !!}