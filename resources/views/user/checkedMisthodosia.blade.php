    <!-- ####################  EDIT PROFILE FORM #######################   -->

            <div class="row">
                <div class="col-md-12">
                    @if(!$teacher->request_to_change_misthodosia)
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                             Σε περίπτωση που επιθυμείτε αλλαγή των στοιχείων Μισθοδοσίας σας πατήστε
                             <button type="button" class="btn btn-primary btn btn-xs" data-toggle="modal" data-target="#changeMisthodosia">
                               εδώ
                             </button>
                        </div>
                    @else
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                             <p>
                                 <strong>Προσοχή:</strong> Έχει σταλεί αίτημα αλλαγής των στοιχείων Μισθοδοσίας. Μόλις ενεργοποιηθεί από το αρμόδιο τμήμα
                                 θά έχετε τη δυνατότητα επεξεργασίας <b>ΌΛΩΝ</b> των στοιχείων σας.
                             </p>
                             <p>
                                 Αν επιθυμείτε να αναιρέστε την τροποποίηση των στοιχείων σας (να παραμείνουν ως έχουν)
                                 κάντε κλικ
                                 <a href="{!! route('User::undoMisthodosia') !!}" class="btn btn-primary btn btn-xs">εδώ</a>
                             </p>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div id="attach" class="panel panel-info">
                        <div class="panel-heading">
                            Προσωπικά στοιχεία
                        </div>
                        <div class="panel-body  text-left">
                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    Αρ. Ταυτότητας:
                                </label>
                                <span style="font-weight: bold">
                                    {!! $misthodosia->at !!}
                                </span>
                            </div>

                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    Ημ. Γέννησης:
                                </label>
                                <span style="font-weight: bold">
                                    {!! $misthodosia->birth !!}
                                </span>
                            </div>

                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    Οικ. Κατάσταση:
                                </label>
                                <span style="font-weight: bold">
                                    {!! Config::get('misthodosia.family_situation_list')[$misthodosia->family_situation] !!}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-8">
                    <div id="attach" class="panel panel-info">
                        <div class="panel-heading">
                            Τραπεζικός Λογαριασμός
                        </div>
                        <div class="panel-body text-left">
                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    IBAN:
                                </label>
                                <span style="font-weight: bold">
                                    {!! $misthodosia->iban !!}
                                </span>
                            </div>

                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    Τράπεζα:
                                </label>
                                <span style="font-weight: bold">
                                    {!! Config::get('misthodosia.banks')[$misthodosia->bank]  !!}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Στοιχεία ασφάλισης
                        </div>
                        <div class="panel-body text-left">
                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    ΑΜΚΑ:
                                </label>
                                <span style="font-weight: bold">
                                    {!! $misthodosia->amka  !!}
                                </span>
                            </div>

                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    ΔΟΥ:
                                </label>
                                <span style="font-weight: bold">
                                    @if(array_key_exists($misthodosia->doy, Config::get('misthodosia.list_doy')))
                                        {!! Config::get('misthodosia.list_doy')[$misthodosia->doy]   !!}
                                    @else
                                        <span style="color: red">
                                            μη καταχωρημένη
                                        </span>
                                    @endif
                                </span>
                            </div>

                            @if($misthodosia->am_tsmede != null)
                                <div class="form-group form-inline">
                                    <label style="font-weight: normal">
                                        ΑΜ ΤΣΜΕΔΕ:
                                    </label>
                                    <span style="font-weight: bold">
                                        {!! $misthodosia->am_tsmede  !!}
                                    </span>
                                </div>
                            @endif

                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    AM IKA:
                                </label>
                                <span style="font-weight: bold">
                                    {!! $misthodosia->am_ika  !!}
                                </span>
                            </div>

                            <div class="form-group form-inline">
                                <span style="font-weight: bold">
                                    {!! $misthodosia->new ? 'Νέος Ασφαλισμένος' : 'Παλιός Ασφαλισμένος'  !!}
                                </span>
                            </div>

                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    ΜΚ:
                                </label>
                                <span style="font-weight: bold">
                                    {!! $misthodosia->mk  !!}
                                </span>
                            </div>

                            <div class="form-group">
                                <label style="font-weight: normal">
                                    Επόμενο ΜΚ στις:
                                </label>
                                <span style="font-weight: bold;color: red">
                                    {!! $misthodosia->next_mk  !!}
                                </span>
                            </div>

                            <div class="form-group form-inline">
                                <label style="font-weight: normal">
                                    ΑΦΜ:
                                </label>
                                <span style="font-weight: bold">
                                    {!! $misthodosia->afm  !!}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div  class="panel panel-info">
                        <div class="panel-heading">
                            Τέκνα
                        </div>
                        <div class="panel-body text-left">
                            @if($misthodosia->tekna->isEmpty())
                                <div class="alert alert-warning text-center" role="alert">
                                     Δεν υπάρχουν
                                </div>
                            @else
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="text-center">A/A</th>
                                            <th colspan="3" class="text-center">Ημερομηνίες</th>
                                            <th rowspan="2" class="text-center">Ειδ. Περίπτωση</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">Γέν</th>
                                            <th class="text-center">Εγγρφ</th>
                                            <th class="text-center">Αποφ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($misthodosia->tekna as $key=>$tekna)
                                            <tr>
                                                <td class="text-center">{!! $key + 1 !!}</td>
                                                <td class="text-center">{!! $tekna->birth !!}</td>
                                                <td class="text-center">{!! $tekna->college_start_date !!}</td>
                                                <td class="text-center">{!! $tekna->college_end_date !!}</td>
                                                <td>{!! $tekna->description !!}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            @endif

                        </div>
                    </div>
                </div>
            </div>

        <!-- ####################  EDIT PROFILE FORM #######################   -->