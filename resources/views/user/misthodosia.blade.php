@extends('app')

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

    @include('user._style')
@endsection

@section('content')

<div class="container">
	<div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab-container">
            @include('user._menu', ['active' => 'misthodosia'])
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <div id="misthodosia_vue" class="bhoechie-tab-content active">
                    <center>
                    <h2 style="margin-top: 0;color:#55518a">Στοιχεία Μισθοδοσίας</h2>

                    @include('errors.list')

                    @if(isset($teacher))
                            @if($teacher->misthodosia_is_checked)
                                @include('user.checkedMisthodosia')
                            @else
                                @if($teacher->misthodosia_counter > 0)
                                    <div class="alert alert-danger" role="alert">
                                        <strong> Τα στοιχεία ελέγχονται από το τμήμα Μισθοδοσίας. </strong>
                                    </div>
                                    @if($teacher->misthodosia_counter >= 3)
                                        <div class="alert alert-warning" role="alert">
                                            <strong> Έχετε στείλει πολλές φορές νέα στοιχεία στο Τμήμα Μισθοδοσίας. Μέχρι να γίνει ο έλεγχος των στοιχείων σας δεν δίνεται η δυνατότητα επιπλέον αποστολής στοιχείων </strong>
                                        </div>
                                    @endif
                                @else
                                    <div class="alert alert-success" role="alert">
                                        <strong> Τα στοιχεία ξεκλειδώθηκαν από το τμήμα Μισθοδοσίας και μπορείτε να τα επεξεργαστείτε. </strong>
                                    </div>
                                @endif

                                @include('user.teacherMisthodosia')
                            @endif
                    @endif

                    <br>
                    </center>
                </div>
            </div>
        </div>
  </div>
</div>

<!-- Modal Request change profile -->
<div class="modal fade" id="changeMisthodosia" tabindex="-1" role="dialog" aria-labelledby="changeMisthodosiaLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Αλλαγή στοιχείων ΜΙΣΘΟΔΟΣΙΑΣ</h4>
            </div>
            <div class="modal-body">
                <p>
                    Σε περίπτωση που διαφωνείτε - με τα ελεγμένα από την υπηρεσία - στοιχεία ΜΙΣΘΟΔΟΣΙΑΣ, μπορείτε να αιτηθείτε την αλλαγή τους.
                    Για να σας επιτραπεί αλλαγή των στοιχείων θα πρέπει το τμήμα Μισθοδοσίας της Διεύθυνσης να σας δώσει το δικαίωμα (ξεκλείδωμα) στην
                    υπάρχουσα φόρμα.
                </p>
                <p>
                    Οποιαδήποτε αλλαγή στα στοιχεία Μισθοδοσίας, θα πρέπει εκ νέου να εγκριθεί από αρμόδιο τμήμα της Διεύθυνσης.
                </p>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Άκυρο</button>
            <button id="changeMisthodosiaRequestButton" type="button" class="btn btn-primary" data-dismiss="modal">Αποστολή αιτήματος αλλαγής στοιχείων ΜΙΣΘΟΔΟΣΙΑΣ</button>
            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts.footer')
    @include('loader')

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.2.8/jquery.form-validator.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

    {{--<script src="/js/global.js"></script>--}}

    @include('misthodosia._vue_misthodosia', ['tekna' => $misthodosia->tekna, 'misthodosia_id' => isset($misthodosia->id)? $misthodosia->id : $misthodosia->fmid])



    <script>
        $(document).ready(function() {

            @if(isset(Auth::user()->userable))
            //####################  Modal ##########################
                $('#changeMisthodosiaRequestButton').on('click', buttonSaveRequestChangeMisthodosia);
            //####################  Modal ##########################
                $('#sent1').on('click', function(e){
                    $('#loader').removeClass('invisible')
                        .addClass('loading');
                });

                $('#sent2').on('click', function(e){
                    $('#loader').removeClass('invisible')
                        .addClass('loading');
                });

                function buttonSaveRequestChangeMisthodosia(e){
                    e.preventDefault();
                    $('#loader').removeClass('invisible')
                        .addClass('loading');

                    $.ajax({
                        url : "{!! route('ajaxTeacherModalChangeMisthodosiaRequest') !!}",
                        type : 'get',
                        success : function(data){
                            successSaveModalChangeMisthodosia( data)
                        },
                        error: function(data){
                            $.swalAlertValidatorError(data)
                        }
                    });
                }

                function successSaveModalChangeMisthodosia(data){
                    if (data.return == 'query_success'){
                        location.reload(true);
                    }else{
                        console.log('Error 23452 misthodosia');
                    }
                }
            @endif

            $.fn.selectpicker.defaults = {
                noneSelectedText: 'Τίποτα δεν επιλέχθηκε',
                noneResultsText: 'Δεν υπάρχουν αποτελέσματα {0}',
                countSelectedText: function (numSelected, numTotal) {
                  return (numSelected == 1) ? "{0} στοιχείο επιλέχθηκε" : "{0} στοιχεία επιλέχθηκαν";
                },
                maxOptionsText: function (numAll, numGroup) {
                  return [
                    (numAll == 1) ? 'Limit reached ({n} item max)' : 'Limit reached ({n} items max)',
                    (numGroup == 1) ? 'Group limit reached ({n} item max)' : 'Group limit reached ({n} items max)'
                  ];
                },
                selectAllText: 'Επιλογή όλων',
                deselectAllText: 'αφαίρεση όλων',
                multipleSeparator: ', '
            };

            var endDate = new Date();
            $('.date-format').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true,
                endDate: endDate,
                language: 'gr'
            });

            $('[data-toggle="tooltip"]').tooltip();

         });


        function bs_input_file() {
            $(".input-file").before(
                function() {
                    if ( ! $(this).prev().hasClass('input-ghost') ) {
                        var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                        element.attr("name",$(this).attr("name"));
                        element.change(function(){
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function(){
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function(){
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor","pointer");
                        $(this).find('input').mousedown(function() {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
        $(function() {
            bs_input_file();
        });
    </script>
@endsection
