@extends('app')

@section('header.style')
    @include('user._style')
@endsection

@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab-container">
            @include('user._menu', ['active' => 'name'])
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <div class="bhoechie-tab-content active">
                    <center>
                    <h2 style="margin-top: 0;color:#55518a">Αλλαγή Πληροφοριών Λογαριασμού</h2>

                    @include('errors.list')

                    @if(isset($user->userable))
                        @if($user->userable->exists)
                            @if($user->userable->is_checked)
                                <p>Ονοματεπώνυμο: <b>{!! $user->full_name !!}</b></p>
                                <p><span class="label label-success">Ελεγμένο</span>   </p>
                            @else
                                @include('user.fullname')
                            @endif
                        @endif
                    @else
                            @include('user.fullname')
                    @endif
                    <br>
                    </center>
                </div>
            </div>
        </div>
  </div>
</div>
@endsection

