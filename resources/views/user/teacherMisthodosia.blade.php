<!-- ####################  EDIT PROFILE FORM #######################   -->
{!! Form::model($misthodosia,[
    'method'=>'POST', 'action'=>'MisthodosiaController@postMisthodosia', 'files' => true]) !!}


            <button id="sent1" class="btn btn-primary">
                ΑΠΟΣΤΟΛΗ νέων στοιχείων Μισθοδοσίας
            </button>

    <hr>
        <div class="col-md-12">
            @include('misthodosia.attachments', ['title' => 'Απαραίτητα Δικαιολογητικά για την αλλαγή των στοιχείων Μισθοδοσίας'])
        </div>
    <hr>
            <div class="col-md-6">
                <div id="attach" class="panel panel-info">
                    <div class="panel-heading">
                        Προσωπικά στοιχεία
                    </div>
                    <div class="panel-body  text-left">
                        <div class="form-group form-inline">
                            {!! Form::label('at', 'Αρ. Ταυτ.:', ['class'=>'control-label']) !!}
                            {!! Form::text('at',null, ['class'=>'form-control', 'maxlength'=> '8', 'size'=>'9']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('birth', 'Ημερομηνία:', ['class'=>'control-label']) !!}
                            {!! Form::text('birth',null, ['class'=>'form-control date-format', 'maxlength'=> '10', 'size'=>'10']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('family_situation', 'Οικ. κατάσταση:', ['class'=>'control-label']) !!}
                            {!! Form::select('family_situation', Config::get('misthodosia.family_situation_list'), null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                </div>

                <div id="attach" class="panel panel-info">
                    <div class="panel-heading">
                        Τραπεζικός Λογαριασμός
                    </div>
                    <div class="panel-body text-left">
                        <div class="form-group form-inline">
                            {!! Form::label('iban', 'IBAN:', ['class'=>'control-label']) !!}
                            {!! Form::text('iban',null, ['class'=>'form-control', 'maxlength'=> '27', 'size'=>'30']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('bank', 'Τράπεζα:', ['class'=>'control-label']) !!}
                            {!! Form::select('bank', Config::get('misthodosia.banks'), null, [
                                 'class' => 'selectpicker',
                                 'data-live-search' => 'true',
                                 'data-size' => '6',
                                 'title' => 'ΤΡΑΠΕΖΕΣ'
                            ]) !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        Στοιχεία ασφάλισης
                    </div>
                    <div class="panel-body text-left">
                        <div class="form-group form-inline">
                            {!! Form::label('amka', 'ΑΜΚΑ:', ['class'=>'control-label']) !!}
                            {!! Form::text('amka',null, ['class'=>'form-control', 'maxlength'=> '27', 'size'=>'30']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('doy', 'ΔΟΥ:', ['class'=>'control-label']) !!}
                            {!! Form::select('doy', Config::get('misthodosia.list_doy'), null, [
                                'class' => 'selectpicker',
                                'data-live-search' => 'true',
                                'data-size' => '6',
                                'title' => 'ΔΟΥ'
                            ]) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('am_tsmede', 'ΑΜ ΤΣΕΔΕ:', ['class'=>'control-label']) !!}
                            {!! Form::text('am_tsmede',null, ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('am_ika', 'AM ΙΚΑ:', ['class'=>'control-label']) !!}
                            {!! Form::text('am_ika',null, ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('new', 'ΝΕΟΣ Ασφαλισμένος:', ['class'=>'control-label']) !!}
                            {!! Form::select('new', ['ΟΧΙ', 'ΝΑΙ'], null, ['class' => 'form-control']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('mk', 'MK:', ['class'=>'control-label']) !!}
                            {!! Form::text('mk',null, ['class'=>'form-control']) !!}
                        </div>

                        <div class="form-group form-inline">
                            {!! Form::label('afm', 'ΑΦΜ:', ['class'=>'control-label']) !!}
                            <span>
                                {!! $teacher->afm !!}
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div  class="panel panel-warning">
                    <div class="panel-heading">
                        Τέκνα
                    </div>
                    <div class="panel-body text-left"  id="tekna">
                        <table v-if="tekna.length > 0" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th rowspan="2" class="text-center">A/A</th>
                                        <th colspan="3" class="text-center">Ημερομηνίες</th>
                                        <th rowspan="2" class="text-center">Ειδ. Περίπτωση</th>
                                        <th rowspan="2" class="text-center">Ενέργειες</th>
                                    </tr>
                                    <tr>
                                        <th class="text-center">Γέν</th>
                                        <th class="text-center">Εγγρφ</th>
                                        <th class="text-center">Αποφ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr v-for="tekno in tekna">
                                            <td class="text-center" style="padding-top: 15px;">
                                                @{{ $index + 1 }}
                                            </td>
                                            <td class="text-center">
                                                <input class="form-control date-format" type="text" name="tekna[@{{ $index }}][birth]" value="@{{ tekno.birth }}"/>
                                            </td>
                                            <td class="text-center">
                                                <input class="form-control date-format" type="text" name="tekna[@{{ $index }}][StartCollege]" value="@{{ tekno.college_end_date }}"/>
                                            </td>
                                            <td class="text-center">
                                                <input class="form-control date-format" type="text" name="tekna[@{{ $index }}][EndCollege]" value="@{{ tekno.college_start_date }}"/>
                                            </td>
                                            <td>
                                                <input type="text" value="@{{ tekno.description }}" class="form-control" name="tekna[@{{ $index }}][description]"/>
                                            </td>
                                            <td class="text-center delete-button">
                                               <span @click="deleteTekno(tekno)" class="glyphicon glyphicon-trash pointers"></span>
                                            </td>
                                        </tr>
                                </tbody>
                                <tfoot>
                                    <tr style="background-color: #99e699;">
                                        <td @click="addTekno" colspan="6" class="text-center pointers">
                                             Προσθήκη νέου παιδιού
                                        </td>
                                    </tr>
                                </tfoot>
                        </table>

                        <div v-else>
                            <div class="text-center">
                                <p>
                                    <strong>Δεν υπάρχουν</strong>
                                </p>
                                <p>
                                    <button class="btn btn-primary btn-sm text-center" @click="addTekno">
                                        προσθήκη νέου τέκνου
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <button id="sent2" class="btn btn-primary">
                    ΑΠΟΣΤΟΛΗ νέων στοιχείων Μισθοδοσίας
                </button>
                <hr>
            </div>

{!! Form::close() !!}

        <!-- ####################  EDIT PROFILE FORM #######################   -->