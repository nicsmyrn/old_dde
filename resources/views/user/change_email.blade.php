@extends('app')

@section('header.style')
    @include('user._style')
@endsection

@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab-container">
            @include('user._menu', ['active' => 'email'])
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <div class="bhoechie-tab-content active">
                    <center>
                    <h2 style="margin-top: 0;color:#55518a">E-mail</h2>

                    @include('errors.list')

{{--                    {!! Form::open(['method'=>'post', 'class'=>'form']) !!}--}}

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon" id="sizing-addon1">@</span>
                                <input name="old_email" type="text" value="{!! Auth::user()->email !!}" class="form-control" aria-describedby="sizing-addon1">
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<div class="input-group input-group-lg">--}}
                                {{--<span class="input-group-addon" id="sizing-addon1">@</span>--}}
                                {{--<input name="old_email" type="text" value="{!! Input::old('old_email') !!}" class="form-control" placeholder="γράψτε το υπάρχον E-mail σας" aria-describedby="sizing-addon1">--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--<div class="input-group input-group-lg">--}}
                                {{--<span class="input-group-addon" id="sizing-addon2">@</span>--}}
                                {{--<input name="new_email" type="text" value="{!! Input::old('new_email') !!}" class="form-control" placeholder="γράψτε το ΝΕΟ E-mail σας" aria-describedby="sizing-addon2">--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="form-group">--}}
                            {{--{!! Form::submit('Αλλαγή', ['class'=>'btn btn-primary btn-lg']) !!}--}}
                        {{--</div>--}}

                    {{--{!! Form::close() !!}--}}

                    <br>
                    </center>
                </div>
            </div>
        </div>
  </div>
</div>
@endsection

