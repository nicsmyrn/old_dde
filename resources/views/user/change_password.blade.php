@extends('app')

@section('header.style')
    @include('user._style')
@endsection

@section('content')
<div class="container">
	<div class="row">
        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab-container">
            @include('user._menu', ['active' => 'password'])
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <div class="bhoechie-tab-content active">
                    <center>
                    <img src="{!! asset('img/password.png') !!}" style="width: 5em"/>
                    <h2 style="margin-top: 0;color:#55518a">Αλλαγή κωδικού</h2>

                    @include('errors.list')

                    {!! Form::open(['method'=>'post', 'class'=>'form']) !!}

                        <div class="form-group">
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon" id="sizing-addon1">@</span>
                                <input name="email" type="text" value="{!! Input::old('email') !!}" class="form-control" placeholder="γράψτε το E-mail σας" aria-describedby="sizing-addon1">
                            </div>
                        </div>

                        <div class="form-group">
                            {!! Form::submit('Αποστολή αιτήματος', ['class'=>'btn btn-primary btn-lg']) !!}
                        </div>

                    {!! Form::close() !!}

                    <br>
                    </center>
                </div>
            </div>
        </div>
  </div>
</div>
@endsection

