    <alert :type="alert.type" :important="alert.important">
        <div slot="alert-header">
            @{{ alert.header }}
        </div>
        <div slot="alert-body">
            @{{ alert.message }}
        </div>
    </alert>