    <br>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">

                @foreach(Config::get('requests.logoi_ygeias') as $ygeia_type)
                    <div class="form-group">
                        <p>
                            <label>
                                {!! $ygeia_type['label'] !!}
                            </label>
                            <span>
                                {!! $ygeia_type['data'][$teacher[$ygeia_type['name']]] !!}
                            </span>
                        </p>
                    </div>
                @endforeach

        </div>
        <div class="col-md-3"></div>
    </div>

