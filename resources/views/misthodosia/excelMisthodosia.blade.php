<html>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


               <table border="1" id="kena_pleonasmata" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>A/A</th>
                            <th>ΑΦΜ</th>
                            <th>Επώνυμο</th>
                            <th>Όνομα</th>
                            <th>Πατρώνυμο</th>
                            <th>ΔΟΥ</th>
                            <th>Αρ. Μητρώου</th>
                            <th>Ειδικότητα</th>
                            <th>E-mail</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($employees as $index=>$employe)
                            <tr>
                                <td>{!! ($index + 1) !!}</td>
                                <td>{!! $employe->afm !!}</td>
                                <td>{!! $employe->last_name !!}</td>
                                <td>{!! $employe->first_name !!}</td>
                                <td>{!! $employe->middle_name !!}</td>
                                <td>{!! array_key_exists($employe->misthodosia->doy, Config::get('misthodosia.list_doy')) ?  Config::get('misthodosia.list_doy')[$employe->misthodosia->doy] : '' !!}</td>
                                <td>{!! $employe->am !!}</td>
                                <td>{!! $employe->eidikotita !!}</td>
                                <td>{!! $employe->teacher != null ?  $employe->teacher->user->email : '' !!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

</html>
