@extends('app')

@section('title')
    Όλοι οι εργαζόμενοι
@stop

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection

@section('content')

    <h1 class="page-heading">{!! $header !!} ({!! $sum !!})</h1>

    @if($employees->isEmpty())
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <hr>
                    <div class="alert alert-info text-center" role="alert">Δεν υπάρχουν εργαζόμενοι</div>
                </div>
            </div>
        </div>
    @else

    <div class="row">
        <table id="requests" class="table table-bordered table-hover" width="100%">
            <thead>
                <tr>
                    <th>ΑΦΜ</th>
                    <th>Ονοματεπώνυμο</th>
                    <th>Πατρώνυμο</th>
                    <th>ΑΜ</th>
                    <th>Ειδικότητα</th>
                    <th>Σχέση</th>
                    <th>Οργανική</th>
                </tr>
            </thead>

            <tbody>
                @foreach($employees as $employe)
                    <tr>
                        <td>
                            <a href="{!! action('MisthodosiaAdminController@checkMisthodosia', $employe->afm) !!}">
                                {!! $employe->afm !!}
                            </a>
                        </td>
                        <td>
                            <a href="{!! action('MisthodosiaAdminController@checkMisthodosia', $employe->afm) !!}">
                                {!! $employe->full_name !!}
                            </a>
                        </td>
                        <td>{!! $employe->middle_name !!}</td>
                        <td>{!! $employe->am !!}</td>
                        <td>{!! $employe->eidikotita !!}</td>
                        <td>{!! $employe->am != null ? 'Μόνιμος': 'Αναπληρωτής' !!}</td>
                        <td>{!! $employe->organiki_name !!}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    @endif

@stop

@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('#requests').DataTable({
                    "order": [[ 1, "asc" ]],
                    "pageLength": 100,
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0,2,3 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection


