
<br>

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-2">
            @if($teacher->sex == 0)
                <img src="{!! asset('img/female2.png') !!}" class="img-circle" alt="ΘΗΛΥ" title="ΘΗΛΥ" width="60px" height="60px">
                <p><span>Θήλυ</span></p>
            @else
                <img src="{!! asset('img/male1.png') !!}" class="img-circle" alt="ΑΡΡΕΝ" title="ΑΡΡΕΝ" width="60px" height="60px">
                <p><span>Άρρεν</span></p>
            @endif
        <h4>
            @if($teacher->is_checked)
                <p>
                    <i class="fa fa-check-circle fa-5x" style="color: #228B22" aria-hidden="true"></i>
                </p>
                <p>
                    <span class="label label-success">Ελεγχμένα</span>
                </p>
            @else
                <p>
                    <i class="fa fa-check-circle fa-5x" style="color: #228B22" aria-hidden="true"></i>
                </p>
                <p>
                    <span class="label label-danger">Υπό έλεγχο, από το ΠΥΣΔΕ</span>
                </p>
            @endif
        </h4>

    </div>

    <div class="col-md-6">
        <table class="table table-user-information">
            <tbody>
                <tr>
                    <td>Σχέση Εργασίας:</td>
                    <td><b>
                        @if($teacher->teacherable_type == 'App\Monimos')
                            Μόνιμος
                        @elseif($teacher->teacherable_type == 'App\Anaplirotis')
                            Αναπληρωτής
                        @endif
                    </b></td>
                </tr>
                @if($teacher->teacherable_type == 'App\Monimos')
                    <tr>
                        <td>ΑΜ:</td>
                        <td><b>{!! $teacher->teacherable->am!!}</b></td>
                    </tr>
                @endif

                <tr>
                    <td>ΑΦΜ:</td>
                    <td><b>{!! $teacher->myschool->afm!!}</b></td>
                </tr>

                <tr>
                    <td>Κλάδος:</td>
                    <td><b>{!! $teacher->klados_name !!} ({!! $teacher->eidikotita_name !!})</b></td>
                </tr>

                 <tr>
                    @if(str_contains($teacher->myschool->topothetisi, 'Οργανικά'))
                        <td>Οργανική</td>
                    @else
                        <td>
                            Προσωρινή Τοποθέτηση
                            @if(!str_contains($teacher->myschool->topothetisi, 'Οργανικά'))
                                <label class="label label-danger">{!! $teacher->myschool->topothetisi !!}</label>
                            @endif
                        </td>
                    @endif
                    <td><b>{!! $teacher->myschool->organiki_name!!}</b></td>
                </tr>

                <tr>
                    <td>Υποχρεωτικό Ωράριο:σσσ</td>
                    <td><b>{!! $teacher->teacherable->orario!!}</b></td>
                </tr>

                @if($teacher->teacherable->moria > 0)
                    <tr>
                        <td>Μόρια Βελτιώσης</td>
                        <td><b style="color: red; font-size: large">{!! $teacher->teacherable->moria !!}</b></td>
                    </tr>
                @endif
                @if($teacher->teacherable->moria_apospasis > 0)
                    <tr>
                        <td>Μόρια Απόσπασης</td>
                        <td><b style="color: red; font-size: large">{!! $teacher->teacherable->moria_apospasis !!}</b></td>
                    </tr>
                @endif
                <tr>
                    <td>Προϋπηρεσία:</td>
                    <td><b>
                        {!! $teacher->ex_years.' χρόνια, '.$teacher->ex_months.' μήνες και '.$teacher->ex_days.' ημέρες' !!}
                    </b></td>
                </tr>
                <tr>
                    <td>Οικογενειακή κατάσταση:</td>
                    <td><b>
                        {!! \Config::get('requests.family_situation')[$teacher->family_situation] !!}
                    </b></td>
                </tr>
                <tr>
                    <td>Αριθμός παιδιών:</td>
                    <td>
                        <div class="alert alert-warning" role="alert">
                            <strong> {!! $teacher->childs !!} </strong>
                            τέκνα μοριοδοτούνται για τις αιτήσεις προς το ΠΥΣΔΕ
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Δήμος εντοπιότητας:</td>
                    <td><b>
                        {!! \Config::get('requests.dimos')[$teacher->dimos_entopiotitas] !!}
                    </b></td>
                </tr>
                <tr>
                    <td>Δήμος συνυπηρέτησης</td>
                    <td><b>
                        {!! \Config::get('requests.dimos')[$teacher->dimos_sinipiretisis] !!}
                    </b></td>
                </tr>
                <tr>
                    <td>Ειδική κατηγορία</td>
                    <td><b>
                        @if($teacher->special_situation)
                            ΝΑΙ
                        @else
                            ΟΧΙ
                        @endif
                    </b></td>
                </tr>

                <tr>
                    <td>E-mail:</td>
                    <td><b>
                        {!! $teacher->user->email !!}
                    </b></td>
                </tr>

                <tr>
                    <td>Δεύτερο E-mail:</td>
                    <td><b>
                        {!! $teacher->user->second_mail !!}
                    </b></td>
                </tr>
            </tbody>
        </table>
    </div>

    <div class="col-md-3"></div>

</div>
