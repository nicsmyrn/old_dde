@extends('app')

@section('title')
    Έλεγχος στοιχείων Μισθοδοσίας
@endsection

@section('header.style')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker.min.css" rel="stylesheet">

    <style>
        .suggestion-fake{
            color: red;
            font-weight: bold;
        }
        .button-change{
            color : red;
        }

        .pointers {
            cursor: pointer;
        }

        .delete-button{
            padding-top: 15px !important;
            color: red;
            font-weight: bold;
        }
    </style>
@endsection

@section('content')
        <h3 class="page-heading">
            {!! $myschoolProfile->full_name !!} του {!! $myschoolProfile->middle_name !!} ΑΦΜ: {!! $myschoolProfile->afm !!}
        </h3>

            @if ($teacher == null)
                <div class="alert alert-danger text-center" role="alert">
                    <strong>Προσοχή!!</strong>
                    <p>Ο εργαζόμενος <strong>ΔΕΝ</strong> έχει εισέλθει ποτέ στην ΠΑΣΙΦΑΗ.</p>
                </div>
            @else
                @if($teacher->misthodosia_is_checked)
                    <div class="alert alert-success text-center" role="alert"><strong>Τα στοιχεία του καθηγητή είναι ελεγμένα.</strong></div>
                @endif

                @if($teacher->request_to_change_misthodosia)
                     <div class="alert alert-danger text-center" role="alert">
                        <strong>
                            Ο καθηγητής αιτήθηκε <u>ΑΛΛΑΓΗ</u> των στοιχείων της Μισθοδοσίας του. Πατήστε
                            <a id="unlock" href="{!! route('Misthodosia::unlock', $myschoolProfile->afm) !!}" class="btn btn-success btn-sm">ΞΕΚΛΕΙΔΩΜΑ</a>
                            για ξεκλείδωμα του δικαιώματος αλλαγής στοιχείων ή
                            <a href="{!! route('Misthodosia::noChanges', $myschoolProfile->afm) !!}" class="btn btn-danger btn-sm">ΚΑΜΙΑ ΑΛΛΑΓΗ</a>
                            για διατήρηση των στοιχείων ως έχουν
                        </strong>
                    </div>
                 @endif
            @endif

        <div class="row">
            <div class="col-md-12">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#misthodosia" aria-controls="misthodosia" role="tab" data-toggle="tab" style="background-color: #f2f2f2;;">
                                    Μισθοδοσία
                                </a>
                            </li>
                          <li role="presentation">
                                <a href="#generalProfile" aria-controls="generalProfile" role="tab" data-toggle="tab" style="background-color: #ccffcc">
                                    Γενικά Στοιχεία
                                </a>
                          </li>
                          <li role="presentation">
                                <a href="#health" aria-controls="health" role="tab" data-toggle="tab" style="background-color: #ffb3b3">
                                    Προσωπικά Στοιχεία Υγείας
                                </a>
                          </li>
                        </ul>

                        <div class="tab-content" id="misthodosia_vue">
                            <div role="tabpanel" class="tab-pane fade in active" id="misthodosia" style="background-color:#f2f2f2">
                                @include('errors.list')
                                @include('misthodosia._form_misthodosias')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="generalProfile" style="background-color:#ccffcc">
                                @include('misthodosia._general_content')
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="health" style="background-color:#ffb3b3">
                                @include('misthodosia.sovaroi-logoi-ygeias')
                            </div>
                        </div>
            </div>
        </div>

    @include('loader')
@endsection

@section('scripts.footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.3/js/bootstrap-select.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="/js/bootstrap-datepicker.gr.js" charset="UTF-8"></script>

    @include('misthodosia._vue_misthodosia', ['tekna' => $myschoolProfile->misthodosia->tekna, 'misthodosia_id' => $myschoolProfile->misthodosia->id])

    <script>
        $(document).ready(function() {

                    $('#unlock').on('click', function(e){
                        $('#loader').removeClass('invisible')
                            .addClass('loading');
                    });

                    $('#confirm').on('click', function(e){
                        $('#loader').removeClass('invisible')
                            .addClass('loading');
                    });

                    $('#recheck').on('click', function(e){
                        $('#loader').removeClass('invisible')
                            .addClass('loading');
                    });

                    $.fn.selectpicker.defaults = {
                        noneSelectedText: 'Τίποτα δεν επιλέχθηκε',
                        noneResultsText: 'Δεν υπάρχουν αποτελέσματα {0}',
                        countSelectedText: function (numSelected, numTotal) {
                          return (numSelected == 1) ? "{0} στοιχείο επιλέχθηκε" : "{0} στοιχεία επιλέχθηκαν";
                        },
                        maxOptionsText: function (numAll, numGroup) {
                          return [
                            (numAll == 1) ? 'Limit reached ({n} item max)' : 'Limit reached ({n} items max)',
                            (numGroup == 1) ? 'Group limit reached ({n} item max)' : 'Group limit reached ({n} items max)'
                          ];
                        },
                        selectAllText: 'Επιλογή όλων',
                        deselectAllText: 'αφαίρεση όλων',
                        multipleSeparator: ', '
                    };

                    var endDate = new Date();
                    $('.date-format').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true,
                        endDate: endDate,
                        language: 'gr'
                    });

            $('.button_text_field').on('click', function(e){
                e.preventDefault();
                $(this).parent()
                    .fadeOut(500, function(){
                        $(this).remove();
                    });
                $(this).parent().prev().find('.text_real_value').val($(this).prev('.suggestion-fake').html());
            });

            $('.button_text_field_cancel').on('click', function(e){
                e.preventDefault();
                $(this).parent()
                    .fadeOut(500, function(){
                        $(this).remove();
                    });
            });

            $('.button_select_box_cancel').on('click', function(e){
                e.preventDefault();
                $(this).parent()
                    .fadeOut(500, function(){
                        $(this).remove();
                    });
            });

           $('.button_select_box').on('click', function(e){
               e.preventDefault();
               $(this).parent()
                   .fadeOut(500, function(){
                       $(this).remove();
                   });
               var selectBox = $(this).parent().prev().find('.text_real_value');
               var selectNewValue = $(this).prev('.suggestion-fake').val();

               selectBox.find('option:selected').removeAttr('selected');
               selectBox.val($(this).prev('.suggestion-fake').val());
               selectBox.find('option[value='+selectNewValue+']').attr('selected', 'selected');
           })
        })
    </script>
@endsection