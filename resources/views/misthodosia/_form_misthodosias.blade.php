<br>

{!! Form::model($myschoolProfile->misthodosia, ['class'=>'form', 'method'=>'PATCH', 'action'=>['MisthodosiaAdminController@updateMisthodosiaDetails', $myschoolProfile->afm]]) !!}

<input type="hidden" name="afm" value="{!! $myschoolProfile->afm !!}" xmlns="http://www.w3.org/1999/html"/>
    <div class="container">
        @if($teacher != null)
            @if($teacher->misthodosia_counter > 0)
                    <div class="col-md-10 col-md-offset-1">
                        <div class="alert alert-info alert-dismissible text-center" role="alert">
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                             Ο εργαζόμενος έχει πραγματοποιήσει <strong> {!! $teacher->misthodosia_counter  !!} {!! $teacher->misthodosia_counter == 1 ?  ' φορά αλλαγή' : ' φορές αλλαγές' !!}  </strong> στα στοιχεία του.
                        </div>
                    </div>
            @else
                    <div class="col-md-10 col-md-offset-1">
                        <div class="alert alert-info text-center" role="alert">
                             Ο εργαζόμενος δεν έχει πραγματοποιήσει καμία αλλαγή στα στοιχεία του.
                        </div>
                    </div>
            @endif
        @endif

        <br>
        <div class="col-md-4 text-center">
            <button class="btn btn-primary">Αποθήκευση</button>
        </div>

        @if($teacher != null)
            <div class="col-md-4 text-center">
                @if(!$teacher->misthodosia_is_checked)
                    <button id="confirm" type="submit" class="btn btn-success" formaction="{!! route('Misthodosia::confirm',[$teacher->afm]) !!}">
                        Αποθήκευση & Κλείδωμα
                    </button>
                @endif
            </div>
            <div class="col-md-4 text-center">
                @if(!$teacher->misthodosia_is_checked)
                    <a id="recheck" href="{!! route('Misthodosia::lockWithOutChanges',[$teacher->afm]) !!}" class="btn btn-danger"> Καμία αλλαγή & κλείδωμα</a>
                @endif
            </div>
        @endif
        <br>
        <br>

    </div>

    <div class="container">
            <div class="col-md-4">
                <div id="attach" class="panel panel-info">
                    <div class="panel-heading">
                        Προσωπικά στοιχεία
                    </div>
                    <div class="panel-body  text-left">
                        <div class="form-group form-inline">
                            <div>
                                {!! Form::label('at', 'Αρ. Ταυτ.:', ['class'=>'control-label']) !!}
                                {!! Form::text('at',null, ['class'=>'form-control text_real_value', 'maxlength'=> '8', 'size'=>'9']) !!}
                            </div>
                            @if($hasFakeMisthodosia)
                                @if($myschoolProfile->misthodosia->at != $teacher->fake_misthodosia->at)
                                     @include('pysde.teachers._Fake_textbox',['var'=> $teacher->fake_misthodosia->at])
                                @endif
                            @endif
                        </div>

                        <div class="form-group form-inline">
                            <div>
                                {!! Form::label('birth', 'Ημ. Γέννσης:', ['class'=>'control-label']) !!}
                                {!! Form::text('birth',null, ['class'=>'form-control date-format text_real_value', 'maxlength'=> '10', 'size'=>'10']) !!}
                            </div>
                            @if($hasFakeMisthodosia)
                                @if($myschoolProfile->misthodosia->birth != $teacher->fake_misthodosia->birth)
                                     @include('pysde.teachers._Fake_textbox',['var'=> $teacher->fake_misthodosia->birth])
                                @endif
                            @endif
                        </div>

                        <div class="form-group form-inline">
                            <div>
                                {!! Form::label('family_situation', 'Οικ. κατάσταση:', ['class'=>'control-label']) !!}
                                {!! Form::select('family_situation', Config::get('misthodosia.family_situation_list'), null, ['class'=>'form-control text_real_value']) !!}
                            </div>
                            @if($hasFakeMisthodosia)
                                @if($myschoolProfile->misthodosia->family_situation != $teacher->fake_misthodosia->family_situation)
                                     @include('pysde.teachers._Fake_selectbox',['var'=> $teacher->fake_misthodosia->family_situation, 'selectCollection'=>Config::get('misthodosia.family_situation_list')])
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-7">
                <div id="attach" class="panel panel-info">
                    <div class="panel-heading">
                        Τραπεζικός Λογαριασμός
                    </div>
                    <div class="panel-body text-left">
                        <div class="form-group form-inline">
                            <div>
                                {!! Form::label('iban', 'IBAN:', ['class'=>'control-label']) !!}
                                {!! Form::text('iban',null, ['class'=>'form-control text_real_value', 'maxlength'=> '27', 'size'=>'30']) !!}
                            </div>

                            @if($hasFakeMisthodosia)
                                @if($myschoolProfile->misthodosia->iban != $teacher->fake_misthodosia->iban)
                                     @include('pysde.teachers._Fake_textbox',['var'=> $teacher->fake_misthodosia->iban])
                                @endif
                            @endif
                        </div>

                        <div class="form-group form-inline">
                            <div>
                                @if($hasFakeMisthodosia)
                                    @if($myschoolProfile->misthodosia->bank != $teacher->fake_misthodosia->bank)
                                        {!! Form::label('bank', 'Τράπεζα:', ['class'=>'control-label']) !!}
                                        {!! Form::select('bank', Config::get('misthodosia.banks'), null, [
                                             'class' => 'form-control text_real_value'
                                        ]) !!}
                                    @else
                                        {!! Form::label('bank', 'Τράπεζα:', ['class'=>'control-label']) !!}
                                        {!! Form::select('bank', Config::get('misthodosia.banks'), null, [
                                             'class' => 'selectpicker text_real_value',
                                             'data-live-search' => 'true',
                                             'data-size' => '6',
                                             'title' => 'ΤΡΑΠΕΖΕΣ'
                                        ]) !!}
                                    @endif
                                @else
                                    {!! Form::label('bank', 'Τράπεζα:', ['class'=>'control-label']) !!}
                                    {!! Form::select('bank', Config::get('misthodosia.banks'), null, [
                                         'class' => 'selectpicker text_real_value',
                                         'data-live-search' => 'true',
                                         'data-size' => '6',
                                         'title' => 'ΤΡΑΠΕΖΕΣ'
                                    ]) !!}
                                @endif
                            </div>

                            @if($hasFakeMisthodosia)
                                @if($myschoolProfile->misthodosia->bank != $teacher->fake_misthodosia->bank)
                                     @include('pysde.teachers._Fake_selectbox',['var'=> $teacher->fake_misthodosia->bank, 'selectCollection'=>Config::get('misthodosia.banks')])
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
    </div>

    <div class="container">
                <div class="col-md-4">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            Στοιχεία ασφάλισης
                        </div>
                        <div class="panel-body text-left">
                            <div class="form-group form-inline">
                                <div>
                                    {!! Form::label('amka', 'ΑΜΚΑ:', ['class'=>'control-label']) !!}
                                    {!! Form::text('amka',null, ['class'=>'form-control text_real_value', 'maxlength'=> '27', 'size'=>'30']) !!}
                                </div>

                                @if($hasFakeMisthodosia)
                                    @if($myschoolProfile->misthodosia->amka != $teacher->fake_misthodosia->amka)
                                         @include('pysde.teachers._Fake_textbox',['var'=> $teacher->fake_misthodosia->amka])
                                    @endif
                                @endif
                            </div>

                            <div class="form-group form-inline">
                                <div>
                                    @if($hasFakeMisthodosia)
                                        @if($myschoolProfile->misthodosia->doy != $teacher->fake_misthodosia->doy)
                                             {!! Form::label('doy', 'ΔΟΥ:', ['class'=>'control-label']) !!}
                                             {!! Form::select('doy', Config::get('misthodosia.list_doy'), null, [
                                                 'class' => 'form-control text_real_value'
                                             ]) !!}
                                        @else
                                            {!! Form::label('doy', 'ΔΟΥ:', ['class'=>'control-label']) !!}
                                            {!! Form::select('doy', Config::get('misthodosia.list_doy'), null, [
                                                'class' => 'selectpicker text_real_value',
                                                'data-live-search' => 'true',
                                                'data-size' => '6',
                                                'title' => 'ΔΟΥ'
                                            ]) !!}
                                        @endif
                                    @else
                                        {!! Form::label('doy', 'ΔΟΥ:', ['class'=>'control-label']) !!}
                                        {!! Form::select('doy', Config::get('misthodosia.list_doy'), null, [
                                            'class' => 'selectpicker text_real_value',
                                            'data-live-search' => 'true',
                                            'data-size' => '6',
                                            'title' => 'ΔΟΥ'
                                        ]) !!}
                                    @endif
                                </div>

                                @if($hasFakeMisthodosia)
                                    @if($myschoolProfile->misthodosia->doy != $teacher->fake_misthodosia->doy)
                                         @include('pysde.teachers._Fake_selectbox',['var'=> $teacher->fake_misthodosia->doy, 'selectCollection'=>Config::get('misthodosia.list_doy')])
                                    @endif
                                @endif
                            </div>

                            <div class="form-group form-inline">
                                <div>
                                    {!! Form::label('am_tsmede', 'ΑΜ ΤΣΕΔΕ:', ['class'=>'control-label']) !!}
                                    {!! Form::text('am_tsmede',null, ['class'=>'form-control text_real_value']) !!}
                                </div>

                                @if($hasFakeMisthodosia)
                                    @if($myschoolProfile->misthodosia->am_tsmede != $teacher->fake_misthodosia->am_tsmede)
                                         @include('pysde.teachers._Fake_textbox',['var'=> $teacher->fake_misthodosia->am_tsmede])
                                    @endif
                                @endif
                            </div>

                            <div class="form-group form-inline">
                                <div>
                                    {!! Form::label('am_ika', 'AM ΙΚΑ:', ['class'=>'control-label']) !!}
                                    {!! Form::text('am_ika',null, ['class'=>'form-control text_real_value']) !!}
                                </div>

                                @if($hasFakeMisthodosia)
                                    @if($myschoolProfile->misthodosia->am_ika != $teacher->fake_misthodosia->am_ika)
                                         @include('pysde.teachers._Fake_textbox',['var'=> $teacher->fake_misthodosia->am_ika])
                                    @endif
                                @endif
                            </div>

                            <div class="form-group form-inline">
                                <div>
                                    {!! Form::label('new', 'ΝΕΟΣ Ασφαλισμένος:', ['class'=>'control-label']) !!}
                                    {!! Form::select('new', ['ΟΧΙ', 'ΝΑΙ'], null, ['class' => 'form-control text_real_value ']) !!}
                                </div>

                                @if($hasFakeMisthodosia)
                                    @if($myschoolProfile->misthodosia->new != $teacher->fake_misthodosia->new)
                                         @include('pysde.teachers._Fake_selectbox',['var'=> $teacher->fake_misthodosia->new, 'selectCollection'=>['ΟΧΙ', 'ΝΑΙ']])
                                    @endif
                                @endif
                            </div>

                            <div class="form-group form-inline">
                                <div>
                                    {!! Form::label('mk', 'MK:', ['class'=>'control-label']) !!}
                                    {!! Form::text('mk',null, ['class'=>'form-control text_real_value']) !!}
                                </div>

                                @if($hasFakeMisthodosia)
                                    @if($myschoolProfile->misthodosia->mk != $teacher->fake_misthodosia->mk)
                                         @include('pysde.teachers._Fake_textbox',['var'=> $teacher->fake_misthodosia->mk])
                                    @endif
                                @endif
                            </div>

                            <div class="form-group form-inline">
                                {!! Form::label('next_mk', 'Ημ/νία χορήγησης επόμενου ΜΚ:') !!}
                                {!! Form::text('next_mk',null, ['class'=>'form-control date-format', 'maxlength'=> '10', 'size'=>'10']) !!}
                            </div>

                            <div class="form-group form-inline">
                                {!! Form::label('afm', 'ΑΦΜ:', ['class'=>'control-label']) !!}
                                <span>
                                    {!! $myschoolProfile->afm !!}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-7">
                    <div  class="panel panel-warning">
                        <div class="panel-heading">
                            Τέκνα
                        </div>
                        <div class="panel-body text-left"  id="tekna">
                            <table v-if="tekna.length > 0" class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" class="text-center">A/A</th>
                                            <th colspan="3" class="text-center">Ημερομηνίες</th>
                                            <th rowspan="2" class="text-center">Ειδ. Περίπτωση</th>
                                            <th rowspan="2" class="text-center">Ενέργειες</th>
                                        </tr>
                                        <tr>
                                            <th class="text-center">Γέν</th>
                                            <th class="text-center">Εγγρφ</th>
                                            <th class="text-center">Αποφ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr v-for="tekno in tekna">
                                                <td class="text-center" style="padding-top: 15px;">
                                                    @{{ $index + 1 }}
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control date-format" type="text" name="tekna[@{{ $index }}][birth]" value="@{{ tekno.birth }}"/>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control date-format" type="text" name="tekna[@{{ $index }}][StartCollege]" value="@{{ tekno.college_end_date }}"/>
                                                </td>
                                                <td class="text-center">
                                                    <input class="form-control date-format" type="text" name="tekna[@{{ $index }}][EndCollege]" value="@{{ tekno.college_start_date }}"/>
                                                </td>
                                                <td>
                                                    <input type="text" value="@{{ tekno.description }}" class="form-control" name="tekna[@{{ $index }}][description]"/>
                                                </td>
                                                <td class="text-center delete-button">
                                                   <span @click="deleteTekno(tekno)" class="glyphicon glyphicon-trash pointers"></span>
                                                </td>
                                            </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr style="background-color: #99e699;">
                                            <td @click="addTekno" colspan="6" class="text-center pointers">
                                                 Προσθήκη νέου παιδιού
                                            </td>
                                        </tr>
                                    </tfoot>
                            </table>

                            <div v-else>
                                <div class="text-center">
                                    <p>
                                        <strong>Δεν υπάρχουν</strong>
                                    </p>
                                    <p>
                                        <button class="btn btn-primary btn-sm text-center" @click="addTekno">
                                            προσθήκη νέου τέκνου
                                        </button>
                                    </p>
                                </div>
                            </div>

                            @if($hasFakeMisthodosia && $teacher->misthodosia_counter >0)
                                @if(!$teacher->fake_misthodosia->tekna->isEmpty())
                                    <table class="table table-bordered table-hover" style="background-color: #D3D3D3">
                                        <thead>
                                            <tr>
                                                <th colspan="5" style="color: #0000ff;text-align: center;font-weight: bold">
                                                    Πρόταση αλλαγής Εργαζομένου
                                                </th>
                                            </tr>
                                            <tr>
                                                <th rowspan="2" class="text-center">A/A</th>
                                                <th colspan="3" class="text-center">Ημερομηνίες</th>
                                                <th rowspan="2" class="text-center">Ειδ. Περίπτωση</th>
                                            </tr>
                                            <tr>
                                                <th class="text-center">Γέν</th>
                                                <th class="text-center">Εγγρφ</th>
                                                <th class="text-center">Αποφ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($teacher->fake_misthodosia->tekna as $key=>$tekna)
                                                <tr>
                                                    <td class="text-center">{!! $key + 1 !!}</td>
                                                    <td class="text-center">{!! $tekna->birth !!}</td>
                                                    <td class="text-center">{!! $tekna->college_start_date !!}</td>
                                                    <td class="text-center">{!! $tekna->college_end_date !!}</td>
                                                    <td>{!! $tekna->description !!}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>


{!! Form::close() !!}