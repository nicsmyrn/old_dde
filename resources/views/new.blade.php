@extends('app2')

@section('title')
    Ηλεκτρονική Πλατφόρμα Δ.Δ.Ε. Χανίων
@endsection

@section('header.style')

        {{--<link rel="stylesheet" href="/css/carousel.css">--}}

        @if(Auth::check())
            {{--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>--}}
            {{--<script type="text/javascript">--}}
              {{--google.charts.load("current", {packages:["corechart"]});--}}
              {{--google.charts.setOnLoadCallback(drawChart);--}}
              {{--function drawChart() {--}}
                {{--var data = google.visualization.arrayToDataTable([--}}
                  {{--['Φορέας', 'Αριθμός'],--}}
                  {{--['Πανελλήνιο Σχολικό Δίκτυο', {!! $number_sch !!}],--}}
                  {{--['G-Mail', {!! $number_google !!}],--}}
                  {{--['Εγγραφή στην Πλατφόρμα',  {!! $number_registered !!}]--}}
                {{--]);--}}

                {{--var options = {--}}
                  {{--title: 'Σύνδεση Εκπαιδευτικών με:',--}}
                  {{--is3D: true,--}}
                  {{--backgroundColor : 'transparent',--}}
                  {{--chartArea:{left:0,top:40,width:'75%',height:'75%'},--}}
                  {{--titleTextStyle : {--}}
                    {{--color: 'blue',--}}
                    {{--fontSize: 18,--}}
                    {{--bold: true--}}
                  {{--}--}}
                {{--};--}}

                {{--var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));--}}
                {{--chart.draw(data, options);--}}
              {{--}--}}
            {{--</script>--}}
        @endif
@endsection

@section('content')
    <pre>
        Length: @{{ notifications.length }}
    </pre>
    <pre>
        @{{ $data | json }}
    </pre>
    {{--<div class="alert alert-success alert-dismissible" role="alert">--}}
      {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
      {{--<strong>ΑΙΤΗΣΕΙΣ Ε2 για συμπλήρωση ωραρίου από Παρασκευή 9/9/2016 μέχρι Τρίτη 13/9/2016 και ώρα 14:00 (δόθηκε παράταση 2 ωρών από τον Διευθυντή Δευτεροβάθμιας Εκπαίδευσης </strong>--}}
      {{--<p>--}}
            {{--<ul>--}}
                {{--<li>Ο πίνακας με τα Κενά - Πλεονάσματα έχει αναρτηθεί στην ιστοσελίδα της Δ.Δ.Ε. Χανίων, στην Ηλεκτρονική Πλατφόρμα και έχει σταλεί E-mail στις Σχολικές Μονάδες.</li>--}}
            {{--</ul>--}}
      {{--</p>--}}
    {{--</div>--}}
{{--            <iframe width="100%" height="300px" src="{!! url('announcement') !!}"></iframe>--}}

    <div class="jumbotron">
        @if(Auth::check())
              <div class="row">
                <div class="col-md-6">
                      <h1>Καλώς ορίσατε</h1>

                      <p>
                      Η Ηλεκτρονική Πλατφόρμα <span style="color: red">&laquo;ΠΑΣΙΦΑΗ&raquo;</span> είναι ένα πληροφοριακό σύστημα που σκοπό έχει
                                        την ενημέρωση και διευκόλυνση των εκπαιδευτικών καθώς και τη Διοικητική
                                        υποστήριξη των Υπηρεσιών Εκπαίδευσης της Διεύθυνσης Δευτεροβάθμιας Εκπαίδευσης Ν. Χανίων
                      </p>
                        Για τη χρήση της εφαρμογής ο χρήστης είναι απαραίτητο να συμφωνεί με τους παρακάτω όρους:
                            <ul>
                                <li>
                                    <a class="terms" href="#" type="button" data-toggle="modal" data-target="#termsModal">
                                        Όροι Χρήσης - Εμπιστευτικότητας
                                    </a>
                                </li>
                                <li>
                                    <a class="terms" href="#" type="button" data-toggle="modal" data-target="#privacyModal">
                                        Προσωπικά Δεδομένα
                                    </a>
                                </li>
                            </ul>
                      </p>

                        <p>
                          <a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViZ1N3OUlROVBidEU/view?usp=sharing">Αναλυτικές Οδηγίες χρήσης για την υποβολή των Αιτήσεων.</a>
                        </p>
                        @if(Auth::user()->userable_type == 'App\School')
                          <p>
                            <a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViQVFLbUFscFd0MVk/view?usp=sharing">Αναλυτικές Οδηγίες χρήσης για τα Κενά - Πλεονάσματα των Σχολικών Μονάδων.</a>
                          </p>
                        @endif
                </div>
                <div class="col-md-6">
                    {{--<p>--}}
                      {{--<a target="_blank" href="https://drive.google.com/file/d/0B2eusu2_P6RsbzNIT3ZSY3ctblU/view">Ανακοίνωση Λειτουργικών Κενών Πράξης 32ης 14/9/2016</a>--}}
                    {{--</p>--}}
                    {{--<p>--}}
                      {{--<a target="_blank" href="https://drive.google.com/file/d/0B2eusu2_P6RsaXMtbXUwdWpXUUU/view">Πίνακας Λειτουργικών τοποθετήσεων Πράξη 32η ΠΥΣΔΕ 14-9-2016</a>--}}
                    {{--</p>--}}
                    {{--<p>--}}
                      {{--<a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViLUJHbFJYdzNCd3c/view">2η Ανακοινοποίηση (μόνο ως προς κενό Τεχνολογίας 2ου Γυμν.Χαν.-Πίνακα Κενών - Πλεον. μετά την Πράξη 32η 14.9.2016</a>--}}
                    {{--</p>--}}
                    {{--<div id="piechart_3d" style="width: 600px; height: 400px"></div>--}}

                                                  {{--<div class="alert alert-danger alert-dismissible" role="alert">--}}
                                                        {{--<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                                                        {{--<strong>ΑΠΟΡΙΕΣ - ΤΕΧΝΙΚΑ ΠΡΟΒΛΗΜΑΤΑ!</strong>--}}
                                                        {{--<p>--}}
                                                              {{--Οποιοδήποτε πρόβλημα αντιμετωπίζετε παρακαλώ στείλτε ένα σύντομο ηλεκτρονικό--}}
                                                              {{--μήνυμα (E-mail) στη διεύθυνση nicsmyrn@gmail.com ή nicsmyrn@sch.gr και θα λάβετε άμεση απάντηση.--}}
                                                        {{--</p>--}}
                                                        {{--<p>--}}
                                                              {{--Από την Γραμματεία του ΠΥΣΔΕ και Υπεύθυνος της Ηλεκτρονικής Πλατφόρμας ΠΑΣΙΦΑΗ--}}
                                                        {{--</p>--}}
                                                        {{--<p>--}}
                                                              {{--Νίκος Σμυρναίος--}}
                                                        {{--</p>--}}
                                                  {{--</div>--}}

                                  <div class="alert alert-danger" role="alert">
                                        <p style="background-color: #3D8FC9">
                                            <img src="{!! asset('images/eDataCenter-logo.png') !!}"
                                        </p>
                                        <p>
                                              Για τις αιτήσεις μετάθεσης - βελτίωσης παρακαλούμε ακολουθήστε τον παρακάτω σύνδεσμο
                                        </p>
                                        <p>
                                            <a href="https://teachers.minedu.gov.gr" class="btn btn-success btn-lg">
                                                ΑΙΤΗΣΕΙΣ ΜΕΤΑΘΕΣΗΣ - ΒΕΛΤΙΩΣΗΣ
                                            </a>
                                        </p>
                                        <p>
                                              Τμήμα Μεταθέσεων: Φιωτάκη Βούλα (2821047152) - Τωμαδάκη Μαρία (2821047165)
                                        </p>
                                  </div>
                </div>
              </div>
        @else
                                          <div class="alert alert-danger alert-dismissible" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <strong>ΑΠΟΡΙΕΣ - ΤΕΧΝΙΚΑ ΠΡΟΒΛΗΜΑΤΑ!</strong>
                                                <p>
                                                      Οποιοδήποτε πρόβλημα αντιμετωπίζετε , παρακαλώ στείλτε ένα σύντομο ηλεκτρονικό
                                                      μήνυμα (E-mail) στη διεύθυνση nicsmyrn@gmail.com ή nicsmyrn@sch.gr και θα λάβετε άμεση απάντηση.
                                                </p>
                                                <p>
                                                      Από την Γραμματεία του ΠΥΣΔΕ και Υπεύθυνος της Ηλεκτρονικής Πλατφόρμας ΠΑΣΙΦΑΗ
                                                </p>
                                                <p>
                                                      Νίκος Σμυρναίος
                                                </p>
                                          </div>
              <h1>Καλώς ορίσατε</h1>
              <p>
                  Η Ηλεκτρονική Πλατφόρμα <span style="color: red">&laquo;ΠΑΣΙΦΑΗ&raquo;</span> είναι ένα πληροφοριακό σύστημα που σκοπό έχει
                  την ενημέρωση και διευκόλυνση των εκπαιδευτικών καθώς και τη Διοικητική
                  υποστήριξη των Υπηρεσιών Εκπαίδευσης της Διεύθυνσης Δευτεροβάθμιας Εκπαίδευσης Ν. Χανίων
              </p>
                Για τη χρήση της εφαρμογής ο χρήστης είναι απαραίτητο να συμφωνεί με τους παρακάτω όρους:
                    <ul>
                        <li>
                            <a class="terms" href="#" type="button" data-toggle="modal" data-target="#termsModal">
                                Όροι Χρήσης - Εμπιστευτικότητας
                            </a>
                        </li>
                        <li>
                            <a class="terms" href="#" type="button" data-toggle="modal" data-target="#privacyModal">
                                Προσωπικά Δεδομένα
                            </a>
                        </li>
                    </ul>
              </p>
                  <p>
                        Για την πρόσβαση στην εφαρμογή απαιτείται
                        <a href="{!!url('auth/login')!!}" class="btn btn-primary">Σύνδεση</a>
                  </p>

                  <p>
                    <a target="_blank" href="https://drive.google.com/file/d/0B4h9TtIxRgViZ1N3OUlROVBidEU/view?usp=sharing">Αναλυτικές Οδηγίες χρήσης για την υποβολή των Αιτήσεων.</a>
                  </p>
        @endif



    </div>

@endsection

