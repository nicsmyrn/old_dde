@extends('app')

@section('title')
    Διαχείριση Δήμων - Δρομολογίων
@endsection

@section('scripts.footer')
    <script src="{{elixir('js/ktel.js')}}"></script>
@endsection


@section('content')

<bus-routes token="{{csrf_token()}}"></bus-routes>

<template id="busroutes-template">
    <div class="row">
        <h2 class="text-center">Διαχείριση Δήμων - Δρομολογίων</h2>
        <div class="col-md-5">
            <select v-model="municipalityId" @change="fetchBusRoutesCollection" name="municipality">
                <option value="@{{ $index }}" v-for="m in municipalityCollection">
                    @{{ m }}
                </option>
            </select>
            <hr>

            <div v-if="municipalityId" class="panel panel-primary">
                <div class="panel-heading">
                    Δημιουργία δρομολογίου για τον Δήμο @{{ municipalityCollection[municipalityId] }}
                </div>
                <div class="panel-body">
                    <form>
                        <div class="form-group">
                             <div class="form-inline">
                                 <label class="control-label">Όνομα δρομολογίου:</label>
                                 <input v-model="newRoute.name" type="text" class="form-control" name="routeName">
                             </div>
                        </div>

                        <div class="form-group">
                            <div class="form-inline">
                                <label class="control-label">Τύπος δρομολογίου:</label>
                                <select v-model="newRoute.type">
                                    <option value="@{{ $index }}" v-for="type in routetypeCollection">
                                        @{{ type }}
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-inline">
                                <label class="control-label">Αριθμός Δρομολογίου</label>
                                <input type="text" v-model="newRoute.aa_contract" class="form-control" name="routeContract"/>
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <button @click.prevent="addRoute" type="submit" class="btn btn-success">Δημιουργία</button>
                        </div>

                    </form>
                </div>
            </div>


        </div>
        <div class="col-md-1"></div>
        <div class="col-md-6">
            <ul class="list-group" v-show="busroutesCollection.length">
                <li class="list-group-item" v-for="route in busroutesCollection">
                    <span v-if="route.editable">
                        <form method="post">
                            <input type="text" size="4" maxlength="4" v-model="route.aa_contract" value="@{{ route.aa_contract }}">
                            <input type="text" v-model="route.name" value="@{{ route.name }}">
                            <select v-model="route.routetype_id" name="routetype">
                                <option v-for="type in routetypeCollection">
                                    @{{ type }}
                                </option>
                            </select>
                            <button @click.prevent="saveRoute(route)" type="submit" alt="Αποθήκευση"><i class="fa fa-floppy-o"></i></button>
                            <button @click.prevent="route.editable = false" alt="Ακύρωση"><i class="fa fa-times"></i></button>
                        </form>
                    </span>
                    <span v-else>
                        <form method="post">
                            <span v-if="route.aa_contract!=null">
                                [@{{ route.aa_contract }}].
                            </span>
                            <span v-else>
                                [-]
                            </span>
                            @{{ route.name }}
                            <span class="label label-info">
                                @{{ route.routetype_id}}
                            </span>
                            <input type="hidden" name="_method" value="delete">
                            <button @click.prevent="makeRouteEditable(route)" alt="Επεξεργασία" class="edit"><i class="fa fa-pencil-square-o"></i></button>
                            <button type="submit" @click.prevent="deleteRoute(route)" alt="Διαγραφή" class="delete"><i class="fa fa-trash-o"></i></button>
                        </form>
                    </span>
                </li>
            </ul>
            <div v-else class="alert alert-danger">
                Παρακαλώ επιλέξτε Δήμο για εμφάνιση δρομολογίων
            </div>
        </div>
    </div>

    @include('KTEL.alert')


</template>


@endsection
