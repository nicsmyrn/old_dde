@extends('app')

@section('title')
    Στατιστικά
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12 text-center">
            <a href="#" class="btn btn-success btn-lg">
                Εξαγωγή σε EXCEL
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <canvas id="myChart"></canvas>
        </div>
        <div class="col-md-4">
            <canvas id="myChart2"></canvas>
        </div>
    </div>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>


    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'bar',
            data: {
              labels: ["BUS", "Mini Bus", "Taxi"],
              datasets: [
                {
                  label: "Ευρώ",
                  backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                  data: [2478,5267,734]
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Κόστος με βάση το Όχημα'
              }
            }
        });

            var ctx2 = document.getElementById('myChart2').getContext('2d');
            var chart2 = new Chart(ctx2, {
                type: 'pie',
                data: {
                  labels: ["Δ. Χανίων", "Δ. Πλατανιά", "Δ. Κισάμου", "Δ. Αποκόρωνα", "Δ. Καντάνου","Δ. Σφακίων"],
                  datasets: [{
                    label: "#",
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850", "#000000"],
                    data: [300,100,45,40,15, 10]
                  }]
                },
                options: {
                  title: {
                    display: true,
                    text: 'Αριθμός Δρομολογίων'
                  }
                }
            });
    </script>

@endsection
