@extends('app')

@section('title')
    Διαχείριση Περιόδου
@endsection

@section('scripts.footer')
    <script src="{{elixir('js/ktel.js')}}"></script>
@endsection

@section('content')

<manage-period token="{{csrf_token()}}"></manage-period>

<template id="period-manage-template">
    <div class="row">
        <h2>Διαχείριση Περιόδου</h2>
        <div class="col-md-10">
            <div class="form-group">
                <label class="control-label">
                    Σχολική Χρονιά:
                </label>
                <select v-model="year">
                    <option v-for="year in periodCollection" v-bind:value="year">
                        @{{ year.name }}
                    </option>
                </select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group" v-show="year.months.length">
                <ul class="list-group">
                    <li class="list-group-item" v-for="month in year.months">
                            <div class="row">
                                <div class="col-md-8 text-left">
                                    <span v-if="month.pivot.disabled" style="color: red">
                                        <i class="fa fa-lock fa-2x" aria-hidden="true"></i>
                                    </span>
                                    <span v-else style="color: green">
                                        <i class="fa fa-unlock fa-2x" aria-hidden="true"></i>
                                    </span>

                                    <span style="font-size: large">
                                        @{{ month.name }}
                                    </span>
                                </div>
                                <div class="col-md-4 text-right">
                                    <button @click="changePermission(month, $index)" class="btn btn-default btn-sm">
                                        <i class="fa fa-exchange" aria-hidden="true"></i>
                                        Αλλαγή
                                    </button>
                                </div>
                            </div>

                    </li>
                </ul>
            </div>
            <div v-else>
                <div class="alert alert-danger">
                    Δεν υπάρχουν μήνες για αυτό το έτος
                </div>
            </div>
        </div>
    </div>

    @include('KTEL.alert')

</template>

@endsection
