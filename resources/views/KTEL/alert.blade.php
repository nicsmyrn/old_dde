    <alert :type="alert.type" :important="alert.important" :timeout="3000">
        <div v-cloak slot="alert-header">
            @{{ alert.header }}
        </div>
        <div v-cloak slot="alert-body">
            @{{ alert.message }}
        </div>
    </alert>