@extends('app')

@section('title')
    Επιλογή Δήμου - Δρομολογίων
@endsection


@section('scripts.footer')
    <script src="{{elixir('js/ktel.js')}}"></script>
    <script src="/js/libs/bootstrap-select.min.js"></script>
@endsection

@section('content')

<choose-route municipality-id="{{$municipalityId}}"></choose-route>

<template id="choose-route-template">
    <div class="row">
    <form method="POST">

        <h2 class="text-center">Επιλογή Δρομολογίων</h2>
        <div class="col-md-3 text-center">
            <div class="panel panel-primary">
              <div class="panel-body">
                    <div class="alert alert-warning">
                        Παρακαλώ επιλέξτε το Δήμο στον οποίο ανήκει η Σχολική σας Μονάδα
                    </div>
                    <select
                        class="selectpicker show-menu-arrow show-tick"
                        data-style="btn-primary"
                        name="municipality"
                        v-model="municipalityId"
                        @change="fetchBusRoutesCollection"
                    >
                        <option
                            value="@{{ $index }}"
                            v-for="m in municipalityCollection"
                            style="font-size: 18pt"
                        >
                            @{{ m }}
                        </option>
                    </select>
              </div>
            </div>

            <input
                v-if="!busroutesCollection[0].locked"
                type="submit"
                value="Αποθήκευση Δρομολογίων"
                class="btn btn-primary"
                style="position: fixed;top: 50%;left:8%"
            >
        </div>
        <div class="col-md-9">
                <input type="hidden" name="_token" value="{{csrf_token()}}">

                <div class="col-md-12 text-center">
                    <input v-if="!busroutesCollection[0].locked" type="submit" value="Αποθήκευση Δρομολογίων" class="btn btn-primary">
                </div>
                <br>

                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>επιλογή</th>
                            <th>Κωδικός Σύμβασης</th>
                            <th>Όνομα δρομολογίου</th>
                            <th>Τύπος</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr v-for="route in busroutesCollection">
                            <td class="text-center" v-if="!route.locked">
                                <input name="routes[@{{ route.id }}]" type="checkbox" v-model="route.checked">
                            </td>
                            <td v-else class="text-center">
                                <span v-if="route.checked">
                                    <i class="fa fa-check-square" style="color: #008000" aria-hidden="true"></i>
                                </span>
                            </td>
                            <td class="text-center">@{{ route.aa_contract }}</td>
                            <td>@{{ route.name }}</td>
                            <td v-bind:class="{
                                'text-center' : true,
                                'taxi' : route.routetype_id === 'Ταξί',
                                'truck' : route.routetype_id === 'Mini Bus',
                                'bus'  : route.routetype_id === 'Bus'
                            }">
                                <i v-bind:class="{
                                    'fa' : true,
                                    'fa-taxi' : route.routetype_id === 'Ταξί',
                                    'fa-truck' : route.routetype_id === 'Mini Bus',
                                    'fa-bus'  : route.routetype_id === 'Bus'
                                }" aria-hidden="true"></i>
                                @{{ route.routetype_id }}
                            </td>
                        </tr>
                    </tbody>

                </table>
                {{--<ul class="list-group">--}}
                    {{--<li v-for="route in busroutesCollection" class="list-group-item">--}}
                        {{--<div v-if="!route.locked">--}}
                            {{--<label>@{{ route.name }} </label>--}}
                            {{--<input name="routes[@{{ route.id }}]" type="checkbox" v-model="route.checked">--}}
                        {{--</div>--}}
                        {{--<div v-else>--}}
                            {{--<label> @{{ route.name }} <span v-if="route.checked">Επιλεγμένο</span></label>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                {{--</ul>--}}
                <div class="col-md-12 text-center">
                    <input v-if="!busroutesCollection[0].locked" type="submit" value="Αποθήκευση Δρομολογίων" class="btn btn-primary">
                </div>
        </div>
    </form>
    </div>
</template>


@endsection
