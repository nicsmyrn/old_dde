<table id="requests" class="table table-bordered table-hover" cellspacing="0" width="100%">
    <thead>
        <tr class="active">
            <th class="text-center">Αρχείο Βεβαίωσης</th>
            <th></th>
        </tr>
    </thead>

    <tbody>
        @foreach($fileCollection as $file)
        <tr>
            <td>
                {!! $file !!}
            </td>
            <td class="text-center">
                <a href="{!! route('BUS::School::downloadPDF', [$file]) !!}"><img style="width: 30px; height: 30px" src="/img/download_icon.jpg"/> </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>