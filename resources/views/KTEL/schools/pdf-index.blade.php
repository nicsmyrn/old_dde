@extends('app')

@section('title')
    Προβολή Βεβαιώσεων Μεταφερόμενων Μαθητών
@endsection

@section('header.style')
    <link href="https://cdn.datatables.net/1.10.9/css/dataTables.bootstrap.min.css" rel="stylesheet">
@endsection


@section('content')
        <h1 class="page-heading">Βεβαιώσεις για το Σχολικό Έτος: 2016 - 2017</h1>

        @if(!$fileCollection->isEmpty())
            @include('KTEL.schools.pdf-table-index')
        @else
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <hr>
                        <div class="alert alert-info text-center" role="alert">Δεν υπάρχει καμία Βεβαίωση </div>
                    </div>
                </div>
            </div>
        @endif

@endsection


@section('scripts.footer')
    <script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.9/js/dataTables.bootstrap.min.js"></script>
    <script>

        $(document).ready(function() {
            var table = $('#requests').DataTable({
                    "order": [[ 1, "desc" ]],
                    "aoColumnDefs": [ { "bSortable": false, "aTargets": [ 0, 4,5 ] } ],
                    "language": {
                                "lengthMenu": "Προβολή _MENU_ εγγραφών ανά σελίδα",
                                "zeroRecords": "Δεν βρέθηκε καμία εγγραφή",
                                "info": "Προβολή σελίδας _PAGE_ από _PAGES_",
                                "infoEmpty": "Καμία εγγραφή διαθέσιμη",
                                "infoFiltered": "(φιλτράρισμα  από  _MAX_ συνολικές εγγραφές)",
                                "search": "Αναζήτηση:",
                                "paginate": {
                                      "previous": "Προηγούμενη",
                                      "next" : "Επόμενη"
                                    }
                            }
            });
        });
    </script>
@endsection