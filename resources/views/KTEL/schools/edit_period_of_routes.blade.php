@extends('app')

@section('header.style')
    <meta id="token" name="csrf-token" content="{{csrf_token()}}">

    <style>
        #periodRoutes{

        }
        #periodRoutes thead tr th{
            text-align: center;
        }
    </style>
@endsection

@section('scripts.footer')
    <script src="{{elixir('js/ktel.js')}}"></script>
@endsection

@section('title')
    Επεξεργασία δρομολογίων για συγκεκριμένη χρονική περίοδο
@endsection

@section('content')

<edit-route-period></edit-route-period>

<template id="edit-periods">
    <div class="row">
        <h2 class="text-center">Διαχείριση περιόδου δρομολογίων</h2>

        <div class="col-md-4">
            <label>Σχολικό Έτος:</label>
            <select v-model="months" class="form-control">
                <option v-for="year in yearCollection" v-bind:value="year.months">@{{ year.name }}</option>
            </select>
        </div>
        <div v-if="months.length" class="col-md-4">
             <label>Μήνας:</label>
             <select v-model="period" class="form-control" @change="getPeriodRoutes">
                 <option v-for="month in months" v-bind:value="month">@{{ month.name }}</option>
             </select>
        </div>
        <div class="col-md-4">
            <div v-if="PeriodCollection.length && !PeriodCollection[0].disabled" class="btn-group">
                <button @click="changePeriodRoutes" class="btn btn-warning">Προσωρινή Αποθήκευση</button>
                <button v-if="valueUpdatePeriodRoutes == 'yes'" @click="sendPeriodRoutes" class="btn btn-success">Αποστολή</button>
            </div>

            {{--<a v-if="url != ''" href="@{{ url }}" target="_blank" class="btn btn-primary btn-lg">--}}
                {{--Προβολή PDF--}}
            {{--</a>--}}
        </div>
    </div>
    <br>
    <div v-if="period.pivot.disabled" class="alert alert-danger col-md-5">
        Η συγεκριμένη περίοδος είναι κλειδωμένη
    </div>
    <div v-else class="row">
        <table id="periodRoutes" v-if="PeriodCollection.length" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>Α/Α</th>
                    <th>Κωδ. Σύμβασης</th>
                    <th>Τύπος</th>
                    <th>Δρομολόγιο</th>
                    <th>Αρ. Παιδιών</th>
                    <th>Αρ. Δρομολογίων</th>
                    <th>Ώρα Εκκίνησης</th>
                    <th>Νέο Δρομολόγιο</th>
                    <th>Παρατηρήσεις</th>
                    {{--<th v-if="valueUpdatePeriodRoutes=='yes'">Απουσιολόγιο</th>--}}
                </tr>
            </thead>
            <tbody>
                <tr v-for="record in PeriodCollection">
                    <td class="text-center">@{{ $index + 1 }}</td>
                    <td class="text-center">@{{ record.contract }}</td>
                    <td v-bind:class="{
                        'text-center' : true,
                        'taxi' : record.type === 'Ταξί',
                        'truck' : record.type === 'Mini Bus',
                        'bus'  : record.type === 'Bus'
                    }">
                        <i v-bind:class="{
                            'fa' : true,
                            'fa-taxi' : record.type === 'Ταξί',
                            'fa-truck' : record.type === 'Mini Bus',
                            'fa-bus'  : record.type === 'Bus'
                        }" aria-hidden="true"></i>
                        @{{ record.type }}
                    </td>
                    <td>@{{ record.name }}</td>
                    <td class="text-center">
                        <label v-if="record.disabled">@{{ record.kids_number }}</label>

                        <div v-else class="form-group" :class="{'has-error has-feedback' : periodErrors['kids_number'][$index]}">
                            <input class="text-center" maxlength="2" size="2" type="text"  v-model="record.kids_number" value="@{{ record.kids_number }}" placeholder="γράψτε τον αριθμό παιδιών">
                            {{--<i class="fa fa-times form-control-feedback" aria-hidden="true"></i>--}}
                            <span class="help-block">
                                @{{ periodErrors['kids_number'][$index] }}
                            </span>
                        </div>
                    </td>
                    <td class="text-center">
                        <label v-if="record.disabled">@{{ record.routes_number }}</label>

                        <div v-else class="form-group" :class="{'has-error has-feedback' : periodErrors['routes_number'][$index]}">
                            <input class="text-center" maxlength="2" size="2" type="text" v-model="record.routes_number" value="@{{ record.routes_number }}" placeholder="γράψτε τον αριθμό δρομολογίων">
                            {{--<i class="fa fa-times form-control-feedback" aria-hidden="true"></i>--}}
                            <span class="help-block">
                                @{{ periodErrors['routes_number'][$index] }}
                            </span>
                        </div>
                    </td>
                    <td class="text-center">
                        <label v-if="record.disabled">@{{ record.starts_at }}</label>

                        <div v-else class="form-group" :class="{'has-error has-feedback' : periodErrors['starts_at'][$index]}">
                            {{--<input type="text" id="starts_at" v-model="record.starts_at" value="@{{ record.starts_at }}">--}}
                            <select v-model="record.starts_at">
                                <option>07:00</option>
                                <option>07:05</option>
                                <option>07:10</option>
                                <option>07:15</option>
                                <option>07:20</option>
                                <option>07:25</option>
                                <option>07:30</option>
                                <option>07:40</option>
                                <option>07:50</option>
                                <option>08:00</option>
                                <option>08:05</option>
                                <option>08:10</option>
                                <option>08:15</option>
                                <option>08:20</option>
                                <option>08:30</option>
                                <option>13:00</option>
                                <option>13:10</option>
                                <option>13:15</option>
                                <option>13:20</option>
                                <option>13:30</option>
                                <option>13:40</option>
                                <option>13:50</option>
                                <option>14:00</option>
                                <option>14:30</option>
                                <option>15:00</option>
                                <option>15:30</option>
                                <option>16:00</option>
                            </select>
                            {{--<i class="fa fa-times form-control-feedback" aria-hidden="true"></i>--}}
                            <span class="help-block">
                                @{{ periodErrors['starts_at'][$index] }}
                            </span>
                        </div>
                    </td>
                    <td class="text-center">
                        <label v-if="record.disabled">
                            <h4><i v-if="record.new" class="fa fa-check-circle" aria-hidden="true" style="color: #00dd00"></i></h4>
                        </label>

                        <div v-else class="form-group" :class="{'has-error has-feedback' : periodErrors['new'][$index]}">
                            <input type="checkbox" v-model="record.new" value="@{{ record.new }}"/>
                            {{--<i class="fa fa-times form-control-feedback" aria-hidden="true"></i>--}}
                            <span class="help-block">
                                @{{ periodErrors['new'][$index] }}
                            </span>
                        </div>
                    </td>
                    <td class="text-center">
                        <label v-if="record.disabled">@{{ record.description }}</label>

                        <div v-else class="form-group" :class="{'has-error has-feedback' : periodErrors['description'][$index]}">
                            <input type="text" v-model="record.description" value="@{{ record.description }}">
                            {{--<i class="fa fa-times form-control-feedback" aria-hidden="true"></i>--}}
                            <span class="help-block">
                                @{{ periodErrors['description'][$index] }}
                            </span>
                        </div>
                    </td>
                    {{--<td class="text-center" v-if="valueUpdatePeriodRoutes=='yes'">--}}
                        {{--<button @click="getCurrentId(record)" class="btn btn-primary btn-sm">--}}
                            {{--Απουσιολόγιο--}}
                        {{--</button>--}}
                    {{--</td>--}}
                </tr>
            </tbody>
        </table>
        <div v-if="displayAlertEmpty" class="alert alert-warning col-md-5">
            Δεν υπάρχουν δρομολόγια για τη συγκεκριμένη περίοδο
            <button @click="createPeriod" class="btn btn-primary">
                Δημιουργία
            </button>
        </div>
    </div>

    @include('KTEL.alert')

    <div class="row">
        <div class="col-md-12 text-center">
            <div v-if="PeriodCollection.length && !PeriodCollection[0].disabled" class="btn-group">
                <button @click="changePeriodRoutes" class="btn btn-warning">Προσωρινή Αποθήκευση</button>
                <button v-if="valueUpdatePeriodRoutes == 'yes'" @click="sendPeriodRoutes" class="btn btn-success">Αποστολή</button>
            </div>

            {{--<a v-if="url != ''" href="@{{ url }}" target="_blank" class="btn btn-primary btn-lg">--}}
                {{--Προβολή PDF--}}
            {{--</a>--}}
        </div>
    </div>

    <comments :show.sync="showCommentsSystem" :comments.sync="comments" :period_id="period.pivot.id" school_id="{!! Auth::user()->userable->id !!}"></comments>

    <modal :period-route-id="currentId" :show.sync="showModal"></modal>

    @include('vueloader')

</template>

@endsection
