    <table id="header">
        <tr>
            <td class="head-1" colspan="3">
                ΒΕΒΑΙΩΣΗ ΜΕΤΑΦΕΡΟΜΕΝΩΝ ΜΑΘΗΤΩΝ {!! $attr['month'] !!} {!! $attr['year'] !!}
            </td>
        </tr>
        <tr>
            <td class="head-2" colspan="3">
                που πληρούν την προϋπόθεση μεταφοράς σύμφωνα με τη χιλ. απόσταση της ΚΥΑ 24001/14-6-13
            </td>
        </tr>
        <tr>
            <td class="head-1" colspan="3">
                ΣΧΟΛΕΙΟ: {!! $attr['school']->name !!}
            </td>
        </tr>
        <tr>
            <td class="head-information">ΥΠΕΥΘ. ΕΠΙΚΟΙΝ: {!! $attr['full_name'] !!}</td>
            <td class="head-information">τηλ. Διευθυντή:{!! $attr['mobile_phone'] != null ? $attr['mobile_phone'] : '-' !!}</td>
            <td class="head-information">τηλ. Σχολείου:{!! $attr['work_phone'] != null ? $attr['work_phone'] : '-' !!}</td>
        </tr>
        <tr>
            <td colspan="3" class="head-description">
                Βεβαιώνεται ότι για το μήνα <b> {!! substr($attr['month'],0,-1) !!} </b> πραγματοποήθηκαν
                <b> {!! $attr['periodRouteCollection']->max('routes_number') !!} </b> δρομολόγια
                μεταφοράς μαθητών από τον τόπο κατοικίας τους στο σχολείο και αντίστροφα από τον
                ανάδοχο ΚΤΕΛ ΧΑΝΙΩΝ - ΡΕΘΥΜΝΟΥ Α.Ε., σύμφωνα με την υπ. αρ. 2/20-02-2016
                Σύμβαση Μεταφοράς Μαθητών, σύμφωνα με τις παρακάτω διαδρομές:
            </td>
        </tr>
    </table>

    <table id="content">
        <tr class="content-header">
            <td>A/A</td>
            <td>A/A ΣΥΜΒΑΣΗΣ ΚΤΕΛ</td>
            <td>ΔΡΟΜΟΛΟΓΙΟ</td>
            <td>ΩΡΑ ΕΚΚΙΝΗΣΗΣ ΔΡΟΜΟΛΟΓΙΟΥ</td>
            <td>ΑΡΙΘΜΟΣ ΜΑΘΗΤΩΝ</td>
            <td>ΜΕΤΑΦΟΡΙΚΟ ΜΕΣΟ</td>
            <td>ΑΡΙΘΜΟΣ ΔΡΟΜΟΛΟΓΙΩΝ</td>
            <td>ΝΕΟ ΔΡΟΜΟΛΟΓΙΟ</td>
            <td>ΣΥΝΟΛΟ</td>
            <td>ΠΑΡΑΤΗΡΗΣΕΙΣ</td>
        </tr>
        <?php $index = 1; ?>
        @foreach($attr['periodRouteCollection'] as $periodroute)
                <tr class="content-body">
                    <td> {!! $index !!} </td>
                    <td> {!! $periodroute->schoolroute->contract !!} </td>
                    <td> {!! $periodroute->schoolroute->route_name !!} </td>
                    <td> {!! $periodroute->starts_at !!} </td>
                    <td> {!! $periodroute->kids_number !!}  </td>
                    <td> {!! $periodroute->route_type !!} </td>
                    <td> {!! $periodroute->routes_number !!} </td>
                    <td> {!! $periodroute->new ? 'ΝΑΙ':'' !!}</td>
                    <td> {!! $periodroute->routes_number !!} </td>
                    <td> {!! $periodroute->description !!}</td>
                </tr>
                <?php $index += 1; ?>
        @endforeach
    </table>

    <div id="footer">
        <div id="left_footer">
            <div class="footer-description-header">ΠΑΡΑΤΗΡΗΣΕΙΣ:</div>
            <ul class="footer-description">
                @foreach($attr['commentsCollection'] as $comment)
                    <li> {!! $comment->comment !!} </li>
                @endforeach
            </ul>
        </div>
        <div id="right_footer">
            <div class="right_footer_date">{!! Config::get('requests.areas')[Auth::user()->userable->area] !!}, {!! $FirstDate !!}</div>
            <div class="header-signature">
                @if($attr['school']->type == 'nipiagogeio')
                    @if(Auth::user()->userable->sex == 0)
                        Η ΠΡΟΪΣΤΑΜΕΝΗ
                    @elseif(Auth::user()->userable->sex == 1)
                        Ο ΠΡΟΪΣΤΑΜΕΝΟΣ
                    @endif
                @else
                    @if(Auth::user()->userable->sex == 0)
                        @if(Auth::user()->userable->low_functionality)
                            Η ΠΡΟΪΣΤΑΜΕΝΗ
                        @else
                            Η ΔΙΕΥΘΥΝΤΡΙΑ
                        @endif
                    @elseif(Auth::user()->userable->sex == 1)
                        @if(Auth::user()->userable->low_functionality)
                            Ο ΠΡΟΪΣΤΑΜΕΝΟΣ
                        @else
                            Ο ΔΙΕΥΘΥΝΤΗΣ
                        @endif
                    @endif
                @endif
            </div>
            <div class="footer-signature">{!! $attr['full_name'] !!}</div>
        </div>
    </div>

