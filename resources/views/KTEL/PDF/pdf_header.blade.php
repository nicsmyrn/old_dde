<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        body{
            font-family: DejaVu Sans, sans-serif;
            /*font-family: "DejaVu Serif";*/
            margin: 0 40px 0 40px;
            font-size: 13pt;
            text-align: justify;
        }

        table{
            width: 100%;
            margin-top: 3px;
            margin-bottom: 15px;
            border-collapse: collapse;
        }

        #header{

        }

        table .head-1{
            text-align: center;
            font-size: 16pt;
            font-weight: bold;
        }

        table .head-2{
            text-align: center;
            font-size: 11pt;
        }

        table .head-information{
            font-size: 12pt;
            text-align: left;
        }
        table .head-description{
            padding-top: 30px;
            font-size: 11pt;
            text-align: center;
        }

        #content{
            border: #000000 solid 2px;
        }

        #content .content-header{
            text-align: center;
            font-size: 11pt;
            font-weight: bold;
        }
        #content .content-header td{
            border: #000000 solid 1px;
        }

        #content .content-body{
            text-align: center;
            font-size: 11pt;
        }

        #content .content-body td{
            border: #000000 solid 1px;
            text-align: center;
        }


        #footer{
            margin-top: 50px;
            width: 100%;
            font-size: 15pt;
        }
        #footer #left_footer{
            width: 70%;
            float:left;
        }
        #footer #right_footer{
            text-align: center;
            float: left;
            width: 30%;
            font-size: 12pt;

        }
        #footer #left_footer .footer-description-header{
            font-weight: bold;
            font-size: 12pt;
        }

        #footer #left_footer .footer-description{
            font-size: 11pt;
            text-align: left;
            padding-left: 30px;
        }
        #footer #right_footer .right_footer_date{

        }
        #footer #right_footer .header-signature{

        }
        #footer #right_footer .footer-signature{
            margin-top: 70px;
        }


    </style>
</head>